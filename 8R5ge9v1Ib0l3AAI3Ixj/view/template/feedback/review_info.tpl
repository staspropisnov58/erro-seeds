<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-review').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title_info; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid" id="discussion">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_discussion; ?></h3>
      </div>
      <div class="panel-body">
<ul>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar">

  </div></div>
</div></li>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar"></div></div>
</div>
<ul>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar"></div></div>
</div></li>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar"></div></div>
</div>
<ul>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar">
    <button type="button" class="btn btn-info"><i class="fa fa-comment"></i> Answer</button>
    <button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button>
    <button type="button" class="btn btn-success"><i class="fa fa-check"></i> Enable</button>
    <button type="button" class="btn btn-warning"><i class="fa fa-times"></i> Disable</button>
    <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
  </div></div>
</div></li>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar">

  </div></div>
</div>
          </li>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar">

  </div></div>
</div>
          </li>
        </ul>
          </li>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar">

  </div></div>
</div>
          </li>
        </ul>
          </li>
          <li>
            <div class="panel panel-default">
              <div class="panel-heading">Panel heading without title</div>
  <div class="panel-body">
    Cras sit
  </div>
  <div class="panel-footer"><div class="btn-toolbar">

  </div></div>
</div>
          </li>
        </ul>

      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
