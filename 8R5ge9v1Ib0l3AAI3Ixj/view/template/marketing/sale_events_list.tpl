<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>

      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              <?php if($sale_event_types){ ?>
              <div class="form-group">
                <label class="control-label" for="input-type-name"><?php echo $entry_type_name; ?></label>
                <select name="filter_type_name" id="input-type-name" class="form-control">
                  <option value=""></option>
                  <?php foreach ($sale_event_types as $sale_event_type){?>
                    <?php if($filter_type_name  === $sale_event_type['name']){ ?>
                      <option selected value = "<?php echo $sale_event_type['name']; ?>"> <?php echo $sale_event_type['name']; ?></option>
                    <?php }else{ ?>
                      <option value = "<?php echo $sale_event_type['name']; ?>"> <?php echo $sale_event_type['name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
            <?php } ?>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-datetime-start"><?php echo $entry_datetime_start; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_datetime_start" value="<?php echo $filter_datetime_start; ?>" placeholder="<?php echo $entry_datetime_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-datetime-end"><?php echo $entry_datetime_end; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_datetime_end" value="<?php echo $filter_datetime_end; ?>" placeholder="<?php echo $entry_datetime_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-sale_events">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'st.name') { ?>
                    <a href="<?php echo $sort_type_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_type_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_type_name; ?>"><?php echo $column_type_name; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'sed.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'se.datetime_start') { ?>
                    <a href="<?php echo $sort_datetime_start; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_datetime_start; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_datetime_start; ?>"><?php echo $column_datetime_start; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'se.datetime_end') { ?>
                    <a href="<?php echo $sort_datetime_end; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_datetime_end; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_datetime_end; ?>"><?php echo $column_datetime_end; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($sale_events) { ?>
                <?php foreach ($sale_events as $sale_event) { ?>
                <tr class="<?php echo $sale_event['class']; ?>">
                  <td class="text-center"><?php if (in_array($sale_event['sale_event_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $sale_event['sale_event_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $sale_event['sale_event_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $sale_event['type_name']; ?></td>
                  <td class="text-left"><?php echo $sale_event['name']; ?></td>
                  <td class="text-right"><?php echo $sale_event['datetime_start']; ?></td>
                  <td class="text-right"><?php echo $sale_event['datetime_end']; ?></td>
                  <td class="text-right">
                    <a href="<?php echo $sale_event['info']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                    <a href="<?php echo $sale_event['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <a href="<?php echo $sale_event['delete']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-sale_events').submit() : false;"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--

$('#button-filter').on('click', function() {
	url = 'index.php?route=marketing/sale_events&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_type_name = $('select[name=\'filter_type_name\']').val();

	if (filter_type_name) {
		url += '&filter_type_name=' + encodeURIComponent(filter_type_name);
	}

	var filter_datetime_start = $('input[name=\'filter_datetime_start\']').val();

	if (filter_datetime_start) {
		url += '&filter_datetime_start=' + encodeURIComponent(filter_datetime_start);
	}

  var filter_datetime_end = $('input[name=\'filter_datetime_end\']').val();

	if (filter_datetime_end) {
		url += '&filter_datetime_end=' + encodeURIComponent(filter_datetime_end);
	}

	location = url;
});


$('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=marketing/sale_events/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['sale_event_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name=\'filter_name\']').val(item['label']);
  }
});

//--></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>
