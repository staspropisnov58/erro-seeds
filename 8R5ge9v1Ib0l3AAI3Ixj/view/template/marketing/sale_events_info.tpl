<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
          <i class="fa fa-pencil"></i>
        </a>
        <a href="<?php echo $delete; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger">
          <i class = "fa fa-minus-circle"></i>
        </a>

        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default">
          <i class="fa fa-reply"></i>
        </a>
      </div>
      <h1><?php echo $heading_title_info; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="col-md-6">
      <div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cog"></i> <?php echo $text_sale_event_setting; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 30%; font-weight: bold;"> <?php echo $text_sale_event_type; ?></td>
                <td>
                  <?php if (isset($sale_event['sale_event_type_name']) && $sale_event['sale_event_type_name']){ ?>
                    <?php echo $sale_event['sale_event_type_name']; ?>
                  <?php } ?>
                </td>
              </tr>
              <tr>
                <td style="width: 30%; font-weight: bold;" ><?php echo $text_sale_events_time; ?> </td>
                <td><?php echo $sale_event['datetime_start'] . ' - ' . $sale_event['datetime_end']; ?> </td>
              </tr>
              <tr>
                <td style="width: 30%; font-weight: bold;" ><?php echo $text_enebled_coupons; ?> </td>
                <td>
                  <?php if($coupons){ ?>
                  <ul>
                      <?php foreach ($coupons as $coupon){ ?>
                        <li style = "list-style-type:none;"><?php echo $coupon['name']; ?> </li>
                      <?php } ?>
                  </ul>
                <?php }else{ ?>
                    <?php echo $text_disabled_coupons; ?>
                  <?php } ?>

                </td>
              </tr>

              <?php foreach ($languages as $language){ ?>
                <tr>
                  <td style="width: 30%; font-weight: bold;"><?php echo $text_sale_event_name; ?> <img src="view/image/flags/<?php echo $language['image']; ?>"></td>
                  <td> <?php echo $sale_event_description[$language['language_id']]['name']; ?>  </td>

                </tr>
                <tr>
                  <td style="width: 30%; font-weight: bold;"><?php echo $text_sale_event_description; ?> <img src="view/image/flags/<?php echo $language['image']; ?>"></td>
                  <td> <?php echo $sale_event_description[$language['language_id']]['description']; ?>  </td>
                </tr>

              <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
      <div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-picture-o"></i> <?php echo $text_sale_event_banner_and_news; ?></h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 30%; font-weight: bold;"> <?php echo $text_banner_image; ?></td>
                <td><img src="<?php echo $banner['banner_image']; ?> "</td>
              </tr>
              <tr>
                <td style="width: 30%; font-weight: bold;"> <?php echo $text_banner_status; ?></td>
                <td>
                  <?php if((int)$banner['status'] === 1 ){?>
                    <?php echo $text_enebled; ?>
                  <?php }elseif($banner['banner_image']){ ?>
                    <?php echo $text_disabled; ?>
                  <?php } ?>
                </td>
              </tr>

                <tr>
                  <td style="width: 30%; font-weight: bold;"><?php echo $text_news_name; ?></td>
                  <td>
                    <?php if(isset($news_description['title']) && $news_description['title']){ ?>
                     <a href = "<?php echo $news_description['href']; ?>"><?php echo $news_description['title']; ?></a>
                   <?php } ?>
                    </td>

                </tr>
                <tr>
                  <td style="width: 30%; font-weight: bold;"><?php echo $text_news_short_description; ?></td>
                  <td> <?php if(isset($news_description['short_description']) && $news_description['short_description']){ ?>
                        <?php echo $news_description['short_description']; ?>
                      <?php } ?>
                  </td>

                </tr>


              <tr>
                <td style="width: 30%; font-weight: bold;"> <?php echo $text_news_status; ?></td>
                <td>
                  <?php if((int)$news_status === 1 ){?>
                    <?php echo $text_enebled; ?>
                  <?php }else{ ?>
                    <?php echo $text_disabled; ?>
                  <?php } ?>
                </td>
              </tr>


            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-hand-o-down"></i> <?php echo $text_sale_events; ?></h3>
        </div>
      <ul class="nav nav-tabs" id="percent">
        <?php foreach ($products as $key => $product){ ?>
        <li><a href="#percent<?php echo $key; ?>" data-toggle="tab"><?php echo $product['tab_name']; ?></a></li>
        <?php } ?>
      </ul>
      <div class="tab-content">
        <?php foreach ($products as $key => $product) { ?>
          <div class="tab-pane" id="percent<?php echo $key; ?>">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i> <?php echo $text_customer_groups; ?></h3>
            </div>
            <table class="table">
              <tbody>
                <?php foreach ($product['customers_groups'] as $value){ ?>
                  <tr>
                    <td>
                    <?php echo $value; ?>
                  </td>
                  </tr>
              <?php } ?>
            </table>
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-cubes" aria-hidden="true"></i> <?php echo $text_categories; ?></h3>
            </div>
            <table class="table">
              <tbody>
              <?php if(isset($product['category_name'])){ ?>
                <?php foreach ($product['category_name'] as $value){ ?>
                  <tr>
                    <td>
                    <?php echo $value; ?>
                  </td>
                  </tr>
              <?php } ?>
            <?php } ?>
            </table>
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-barcode" aria-hidden="true"></i> <?php echo $text_manufacturers; ?></h3>
            </div>
            <table class="table">
              <tbody>
              <?php if(isset($product['manufacturer_name'])){ ?>
                <?php foreach ($product['manufacturer_name'] as $value){ ?>
                  <tr>
                    <td>
                    <?php echo $value; ?>
                  </td>
                  </tr>
              <?php } ?>
            <?php } ?>

            </table>
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo $text_products; ?></h3>
            </div>
            <table class="table">
              <thead>
                <tr>
                  <td><?php echo $text_product_name; ?> </td>
                  <td><?php echo $text_product_model; ?> </td>
                  <td><?php echo $text_product_price; ?> </td>
                  <td><?php echo $text_options_name; ?> </td>
                  <td><?php echo $text_options_price; ?> </td>
                </tr>
              </thead>
              <tbody>
                  <?php foreach ($product['products'] as $value){ ?>
                    <tr>
                      <td>
                      <a href = "<?php echo $value['href']; ?>"><?php echo $value['name']; ?></a>
                    </td>
                    <td>
                      <?php echo $value['model']; ?>
                    </td>
                    <td>
                      <strike><?php echo $value['price']; ?></strike><br>
                      <?php echo $value['special_price']; ?>
                    </td>

                    <td>
                      <?php foreach($value['options'] as $option){ ?>
                      <p>  <?php echo $option['name']; ?> </p><br>
                      <?php } ?>
                    </td>
                    <td>
                      <?php foreach($value['options'] as $option){ ?>
                      <p>
                          <strike><?php echo $option['old_price']; ?></strike>
                            <br>
                            <?php echo $option['new_price']; ?>
                       </p>
                      <?php } ?>
                    </td>

                    </tr>
                <?php } ?>

              </tbody>
            </table>
          </div>

        <?php } ?>
    </div>
    </div>



  </div>


<script type="text/javascript"><!--
$('#percent a:first').tab('show');
//--></script></div>


<?php echo $footer; ?>
