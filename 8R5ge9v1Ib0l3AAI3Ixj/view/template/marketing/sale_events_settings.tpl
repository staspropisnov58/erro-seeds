
<?php // $setting_row = 0; ?>
<?php // foreach ($sale_event_settings as $sale_event_setting) { ?>
  <div class="content"  id="settings_sale<?php echo $setting_row;?>">
<div class="panel panel-default">
  <div class="panel-heading"><?php echo $text_new_heading_title; ?></div>
  <div class="panel-body">

    <div class="text-right">
      <div class="form-group required btn-group right">
        <button type="button" onclick="removeSaleEventSetting('<?php echo $setting_row; ?>', '<?php echo $sale_event_id; ?>')" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-sale-event-persent<?php echo $setting_row; ?>"><?php echo $entry_persent; ?></label>
      <div class="col-sm-10">
        <input type="text" name="sale_event_settings[<?php echo $setting_row; ?>][persent]" value="<?php echo $sale_event_setting['persent']; ?>" placeholder="<?php echo $entry_persent; ?>" id="input-sale-event-persent<?php echo $setting_row; ?>" class="form-control" />
        <?php if (isset($error_persent[$setting_row])) { ?>
          <div class="text-danger"><?php echo $error_persent[$setting_row]; ?></div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-sale-event-sort-order<?php echo $setting_row; ?>"><?php echo $entry_priority; ?></label>
      <div class="col-sm-10">
        <input type="text" name="sale_event_settings[<?php echo $setting_row; ?>][priority]" value="<?php echo $sale_event_setting['priority'];?>" placeholder="<?php echo $entry_priority; ?>" id="input-sale-event-sort-order<?php echo $setting_row; ?>" class="form-control" />
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-sale-event-customer-groups<?php echo $setting_row; ?> "><span data-toggle="tooltip" title="<?php echo $help_customer_group; ?>"><?php echo $entry_customer_group; ?></span></label>
      <div class="col-sm-10">
        <div class="well well-sm" style="height: 150px; overflow: auto;">
          <?php foreach ($customer_groups as $customer_group) { ?>
          <div class="checkbox">
            <label>
              <?php if (in_array($customer_group['customer_group_id'], $config_customer_groups)) { ?>
              <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][customer_group][]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
              <?php echo $customer_group['name']; ?>
              <?php } else { ?>
              <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][customer_group][]" value="<?php echo $customer_group['customer_group_id']; ?>" />
              <?php echo $customer_group['name']; ?>
              <?php } ?>
            </label>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-sale-event-option<?php echo $setting_row; ?>"><span data-toggle="tooltip" title="<?php echo $help_option; ?>"><?php echo $entry_option; ?></span></label>
      <div class="col-sm-10">
        <div class="well well-sm" style="height: 150px; overflow: auto;">
          <?php foreach ($options as $option_id => $option) { ?>
            <?php foreach($option as $values){?>
              <?foreach($values as $option_value_id => $name){?>

              <div class="checkbox">
                <label>
                  <?php if (in_array($option['option_value_id'], $config_options_value)) { ?>
                  <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][option][]" value="<?php echo $option_value_id; ?>" checked="checked" />
                  <?php echo $name; ?>
                  <?php } else { ?>
                  <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][option][]" value="<?php echo $option_value_id; ?>" />
                  <?php echo $name; ?>
                  <?php } ?>
                </label>
              </div>
            <?php } ?>
            <?php } ?>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-sale-event-product"><span data-toggle="tooltip" title="<?php echo $help_product; ?>"><?php echo $entry_product; ?></span></label>
      <div class="col-sm-10">
        <input type="text" name="sale_event_settings_<?php echo $setting_row; ?>_product[]" onfocus="completeProduct('sale_event_settings[<?php echo $setting_row; ?>][product][]', '<?php echo $setting_row; ?>')" value="" placeholder="<?php echo $entry_product; ?>" id="input-product<?php echo $setting_row; ?>" class="form-control" />
        <div id="sale-event-product<?php echo $setting_row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">

        </div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-sale-event-category<?php echo $setting_row; ?>"><span data-toggle="tooltip" title="<?php echo $help_category; ?>"><?php echo $entry_category; ?></span></label>
      <div class="col-sm-10">
        <input type="text" name="sale_event_settings_<?php echo $setting_row; ?>_category[]" onfocus="completeCategory('sale_event_settings[<?php echo $setting_row; ?>][category][]', '<?php echo $setting_row; ?>')" value="" placeholder="<?php echo $entry_category; ?>" id="input-category<?php echo $setting_row; ?>" class="form-control" />
        <div id="sale-event-category<?php echo $setting_row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">

        </div>
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-sale-event-manufacturer<?php echo $setting_row; ?>"><span data-toggle="tooltip" title="<?php echo $help_manufacturer; ?>"><?php echo $entry_manufacturer; ?></span></label>
      <div class="col-sm-10">
        <input type="text" name="sale_event_settings_<?php echo $setting_row; ?>_manufacturer[]" onfocus="completeManufacturer('sale_event_settings[<?php echo $setting_row; ?>][manufacturer][]', '<?php echo $setting_row; ?>')" value="" placeholder="<?php echo $entry_manufacturer; ?>" id="input-manufacturer<?php echo $setting_row; ?>" class="form-control" />
        <div id="sale-event-manufacturer<?php echo $setting_row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
        </div>
      </div>
    </div>
  </div>
</div>
</div>
