<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-sale-event" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sale-event" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-banners" data-toggle="tab"><?php echo $tab_banners; ?></a></li>
            <li><a href="#tab-news" data-toggle="tab"><?php echo $tab_news; ?></a></li>
            <li><a href="#tab-settings" data-toggle="tab"><?php echo $tab_special; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class = "tab-content">
                <?php foreach ($languages as $language) { ?>
                  <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                    <div class="form-group required">
                      <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                      <div class="col-sm-10">
                        <input type="text" name="sale_event_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($sale_event_description[$language['language_id']]) ? $sale_event_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                        <?php if (isset($error_name[$language['language_id']])) { ?>
                        <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                        <?php } ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                      <div class="col-sm-10">
                        <textarea name="sale_event_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($sale_event_description[$language['language_id']]) ? $sale_event_description[$language['language_id']]['description'] : ''; ?></textarea>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-type"><?php echo $entry_sale_event_type; ?></label>
                  <div class="col-sm-10">
                    <select name="type" id="input-type" class="form-control">
                      <option value="0"></option>
                      <?php foreach ($types as $type){ ?>
                        <?php if($sale_event_type_id == $type['sale_event_type_id']){ ?>
                          <option selected value = "<?php echo $type['sale_event_type_id']; ?>"> <?php echo $type['name']; ?></option>
                        <?php }else{ ?>
                          <option value = "<?php echo $type['sale_event_type_id']; ?>"> <?php echo $type['name']; ?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-datetime-start"><?php echo $entry_datetime_start; ?></label>
                  <div class="col-sm-3">
                    <div class="input-group date">
                      <input type="text" readonly name="datetime_start" value="<?php echo $datetime_start; ?>" placeholder="<?php echo $datetime_start; ?>" data-date-format="YYYY-MM-DD HH:mm:ss"   class="form-control" />
                      <?php if ($error_datetime_start) { ?>
                        <div class="text-danger text-center"><?php echo $error_datetime_start; ?></div>
                      <?php } ?>
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                      </span>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-datetime-end"><?php echo $entry_datetime_end; ?></label>
                  <div class="col-sm-3">
                    <div class="input-group date">
                        <input type="text" readonly name="datetime_end" value="<?php echo $datetime_end; ?>" placeholder="<?php echo $datetime_end; ?>" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control" />
                        <?php if ($error_datetime_end) { ?>
                        <div class="text-danger text-center"><?php echo $error_datetime_end; ?></div>
                        <?php } ?>
                        <span class="input-group-btn">
                          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                        </span>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-disable-coupon"><span data-toggle="tooltip" title="<?php echo $help_turn_coupons; ?>"><?php echo $entry_turn_coupons; ?></span></label>
                  <div class="col-sm-10">
                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                      <?php foreach ($coupons as $coupon) { ?>
                      <div class="checkbox">
                        <label>

                          <?php if (in_array($coupon['coupon_id'], $disable_coupon)) { ?>
                            <input type="checkbox" name="disable_coupon[]" value="<?php echo $coupon['coupon_id']; ?>" checked="checked" />
                          <?php echo $coupon['name'] . ' - '; ?>
                          <?php if($coupon['status']){ ?>
                            <?php echo $text_enebled; ?>
                          <?php }else{ ?>
                            <?php echo $text_disabled; ?>
                           <?php } ?>
                          <?php } else { ?>
                            <input type="checkbox" name="disable_coupon[]" value="<?php echo $coupon['coupon_id']; ?>" />
                            <?php echo $coupon['name'] . ' - '; ?>
                            <?php if($coupon['status']){ ?>
                              <?php echo $text_enebled; ?>
                            <?php }else{ ?>
                              <?php echo $text_disabled; ?>
                             <?php } ?>
                          <?php } ?>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tab-banners">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-banner"><?php echo $entry_banner_group; ?></label>
                  <div class="col-sm-10">
                    <select name="banner_id" id="input-banner" class="form-control">
                      <?php foreach ($banners as $banner){ ?>
                        <?php if($banner_group_id === $banner['banner_id']){ ?>
                          <option selected value = "<?php echo $banner['banner_id']; ?>"> <?php echo $banner['name']; ?></option>
                        <?php }else{ ?>
                          <option value = "<?php echo $banner['banner_id']; ?>"> <?php echo $banner['name']; ?></option>
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-12" id ="banner-images">
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tab-news">

                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-center"><?php echo $column_on_off; ?></td>
                      <td class="text-left"><?php echo $column_name_news; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($sale_events_news) { ?>
                    <?php foreach ($sale_events_news as $sale_event_news) { ?>
                    <tr>
                      <td class="text-center">
                        <?php if ($sale_event_news['news_id'] === $sale_event_news_id) { ?>
                        <input type="radio" name = "sale_event_news" id="radio-news" value = "<?php echo $sale_event_news['news_id']; ?>" checked="checked" />
                      <?php }else{ ?>
                        <input type="radio" name = "sale_event_news" id="radio-news" value = "<?php echo $sale_event_news['news_id']; ?>" />
                      <?php } ?>
                      </td>
                      <td class="text-left"><a href = "<?php echo $sale_event_news['href']; ?>" target="_blank"><?php echo $sale_event_news['name']; ?></a></td>

                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>

              </div>
              <div class="tab-pane fade" id="tab-settings">
                <div class="panel-group" id= "settings_sale" role="tablist" aria-multiselectable="true">

                <?php $setting_row = 0; ?>
                <?php if($sale_event_settings){ ?>
                <?php foreach ($sale_event_settings as $sale_event_setting) { ?>

                  <div class="content"  id="settings_sale<?php echo $setting_row;?>">
                  <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="heading<?php echo $setting_row ?>">

                        <a role="button" data-toggle="collapse" data-parent="#settings_sale" href="#collapse<?php echo $setting_row; ?>" class="<?php if(isset($error_special[$setting_row])){echo 'text-danger';}else{echo 'panel-title';} ?>" aria-expanded="true" aria-controls="collapse<?php echo $setting_row; ?>">
                          <?php echo $text_heading_title . ' ' . ($sale_event_setting['persent']) .'%' ; ?>
                        </a>
                    </div>
                    <div id="collapse<?php echo $setting_row; ?>" class="panel-collapse collapse <?php if(isset($error_special[$setting_row])){echo 'in';} ?>" role="tabpanel" aria-labelledby="heading<?php echo $setting_row; ?>">

                    <div class="panel-body">

                      <div class="text-right">
                        <div class="form-group required btn-group right">
                          <button type="button" onclick="removeSaleEventSetting('<?php echo $setting_row; ?>', '<?php echo $sale_event_id;?>')" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                        </div>
                      </div>

                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-sale-event-persent<?php echo $setting_row; ?>"><?php echo $entry_persent; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="sale_event_settings[<?php echo $setting_row; ?>][persent]" value="<?php echo $sale_event_setting['persent']; ?>" placeholder="<?php echo $entry_persent; ?>" id="input-sale-event-persent<?php echo $setting_row; ?>" class="form-control" />
                          <?php if (isset($error_special[$setting_row]['persent'])) { ?>
                            <div class="text-danger"><?php echo $error_special[$setting_row]['persent']; ?></div>
                          <?php } ?>
                        </div>
                      </div>

                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-sale-event-sort-order<?php echo $setting_row; ?>"><?php echo $entry_priority; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="sale_event_settings[<?php echo $setting_row; ?>][priority]" value="<?php echo $sale_event_setting['priority'];?>" placeholder="<?php echo $entry_priority; ?>" id="input-sale-event-sort-order<?php echo $setting_row; ?>" class="form-control" />
                          <?php if (isset($error_special[$setting_row]['priority'])) { ?>
                            <div class="text-danger"><?php echo $error_special[$setting_row]['priority']; ?></div>
                          <?php } ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sale-event-customer-groups<?php echo $setting_row; ?> "><span data-toggle="tooltip" title="<?php echo $help_customer_group; ?>"><?php echo $entry_customer_group; ?></span></label>
                        <div class="col-sm-10">
                          <div class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php foreach ($customer_groups as $customer_group) { ?>
                            <div class="checkbox">
                              <label>
                                <?php if (isset($sale_event_setting['customer_group']) && in_array($customer_group['customer_group_id'], $sale_event_setting['customer_group'])) { ?>
                                <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][customer_group][]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                                <?php echo $customer_group['name']; ?>
                                <?php } else { ?>
                                <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][customer_group][]" value="<?php echo $customer_group['customer_group_id']; ?>" />
                                <?php echo $customer_group['name']; ?>
                                <?php } ?>
                              </label>
                            </div>
                            <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sale-event-option<?php echo $setting_row; ?>"><span data-toggle="tooltip" title="<?php echo $help_option; ?>"><?php echo $entry_option; ?></span></label>
                        <div class="col-sm-10">
                          <div class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php foreach ($options as $option_id => $option) { ?>
                              <?php foreach($option as $values){?>
                                <?foreach($values as $option_value_id => $name){?>
                                <div class="checkbox">
                                  <label>
                                    <?php if (isset($sale_event_setting['option']) && in_array($option_value_id, $sale_event_setting['option'])) { ?>
                                    <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][option][]" value="<?php echo $option_value_id; ?>" checked="checked" />
                                    <?php echo $name; ?>
                                    <?php } else { ?>
                                    <input type="checkbox" name="sale_event_settings[<?php echo $setting_row; ?>][option][]" value="<?php echo $option_value_id; ?>" />
                                    <?php echo $name; ?>
                                    <?php } ?>
                                  </label>
                                </div>
                              <?php } ?>
                              <?php } ?>
                            <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sale-event-product"><span data-toggle="tooltip" title="<?php echo $help_product; ?>"><?php echo $entry_product; ?></span></label>
                        <div class="col-sm-10">
                          <input type="text" name="sale_event_settings_<?php echo $setting_row; ?>_product[]" onfocus="completeProduct('sale_event_settings[<?php echo $setting_row; ?>][product][]', '<?php echo $setting_row; ?>')" value="" placeholder="<?php echo $entry_product; ?>" id="input-product<?php echo $setting_row; ?>" class="form-control" />
                          <div id="sale-event-product<?php echo $setting_row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php if(isset($sale_event_setting['product']) && $sale_event_setting['product']!== NULL){ ?>
                              <?php foreach ($sale_event_setting['product'] as $sale_event_product) { ?>
                              <div id="sale-event-product<?php echo $setting_row; ?><?php echo $sale_event_product['product_id']; ?>"><i class="fa fa-minus-circle" onclick="deleteThis('sale-event-product', '<?php echo $setting_row; ?>', '<?php echo $sale_event_product['product_id']; ?>')"></i> <?php echo $sale_event_product['name']; ?>
                                <input type="hidden" name="sale_event_settings[<?php echo $setting_row; ?>][product][]" value="<?php echo $sale_event_product['product_id']; ?>" />
                              </div>
                              <?php } ?>
                            <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sale-event-category<?php echo $setting_row; ?>"><span data-toggle="tooltip" title="<?php echo $help_category; ?>"><?php echo $entry_category; ?></span></label>
                        <div class="col-sm-10">
                          <input type="text" name="sale_event_settings_<?php echo $setting_row; ?>_category[]" onfocus="completeCategory('sale_event_settings[<?php echo $setting_row; ?>][category][]', '<?php echo $setting_row; ?>')" value="" placeholder="<?php echo $entry_category; ?>" id="input-category<?php echo $setting_row; ?>" class="form-control" />
                          <div id="sale-event-category<?php echo $setting_row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php if(isset($sale_event_setting['category']) && $sale_event_setting['category']!== NULL){ ?>
                              <?php foreach ($sale_event_setting['category'] as $sale_event_category) { ?>
                              <div id="sale-event-category<?php echo $setting_row; ?><?php echo $sale_event_category['category_id']; ?>"><i class="fa fa-minus-circle" onclick="deleteThis('sale-event-category', '<?php echo $setting_row; ?>', '<?php echo $sale_event_category['category_id']; ?>')"></i> <?php echo $sale_event_category['name']; ?>
                                <input type="hidden" name="sale_event_settings[<?php echo $setting_row; ?>][category][]" value="<?php echo $sale_event_category['category_id']; ?>" />
                              </div>
                              <?php } ?>
                            <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sale-event-manufacturer<?php echo $setting_row; ?>"><span data-toggle="tooltip" title="<?php echo $help_manufacturer; ?>"><?php echo $entry_manufacturer; ?></span></label>
                        <div class="col-sm-10">
                          <input type="text" name="sale_event_settings_<?php echo $setting_row; ?>_manufacturer[]" onfocus="completeManufacturer('sale_event_settings[<?php echo $setting_row; ?>][manufacturer][]', '<?php echo $setting_row; ?>')" value="" placeholder="<?php echo $entry_manufacturer; ?>" id="input-manufacturer<?php echo $setting_row; ?>" class="form-control" />
                          <div id="sale-event-manufacturer<?php echo $setting_row; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                          <?php if(isset($sale_event_setting['manufacturer']) && $sale_event_setting['manufacturer']!== NULL){ ?>
                            <?php foreach ($sale_event_setting['manufacturer'] as $sale_event_manufacturer) { ?>
                            <div id="sale-event-manufacturer<?php echo $setting_row; ?><?php echo $sale_event_manufacturer['manufacturer_id']; ?>"><i class="fa fa-minus-circle" onclick="deleteThis('sale-event-manufacturer', '<?php echo $setting_row; ?>', '<?php echo $sale_event_manufacturer['manufacturer_id']; ?>')" ></i> <?php echo $sale_event_manufacturer['name']; ?>
                              <input type="hidden" name="sale_event_settings[<?php echo $setting_row; ?>][manufacturer][]" value="<?php echo $sale_event_manufacturer['manufacturer_id']; ?>"/>
                            </div>
                            <?php } ?>
                          <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>

                  <?php $setting_row++; ?>
              <?php } ?>
            <?php } ?>
            </div>
            <div class="panel">
              <div class="text-right">
                <div class="form-group required btn-group right">
                  <button type="button" onclick="addSetting();" data-toggle="tooltip" title="<?php echo $button_settings_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<div>
  <?php echo $footer; ?>

  <script type="text/javascript"><!--
  <?php foreach ($languages as $language) { ?>
  $('#input-description<?php echo $language['language_id']; ?>').summernote({
  height: 300
  });
  <?php } ?>
  </script>

  <script>
  // var setting_row = <?php // echo $setting_row; ?>;
  var setting_row = <?php echo $setting_row; ?>;

  function addSetting() {
    var html = '';
    html = '<div id= "settings_sale' + setting_row + '">';
    html += '</div>';
    $('#settings_sale').append(html);


    $('#settings_sale' + setting_row).load('index.php?route=marketing/sale_events/getSettings&token=<?php echo $token; ?>&setting_row=' + setting_row + '&sale_event_id=<?php echo $sale_event_id; ?>');

    setting_row++;
    // document.getElementById("demo").innerHTML = "YOU CLICKED ME!";
  }

  function removeSaleEventSetting(setting_row, sale_event_id){
    $('#settings_sale' + setting_row ).remove();
    $.ajax({
      url: 'index.php?route=marketing/sale_events/deleteSale&token=<?php echo $token; ?>&sale_event_id=' +  sale_event_id,
    });
  }

  </script>

  <script type="text/javascript"><!--
  function completeProduct(name, setting_row){
    $('input[name="sale_event_settings_' + setting_row + '_product[]"]').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          json.unshift({
            product_id: 0,
            name: '<?php echo $text_none; ?>'
          });

          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['product_id']
            }
          }));
        }
      });
    },
  	'select': function(item) {
  		$('input[name="sale_event_settings_' + setting_row + '_product[]"]').val('');

      $('#sale-event-product'+ setting_row + item['value']).remove();

  		$('#sale-event-product'+ setting_row).append('<div id="sale-event-product' + setting_row + item['value'] + '"><i class="fa fa-minus-circle" onclick="deleteThis(\'sale-event-product\', \'' + setting_row + '\', \'' + item['value'] + '\')"></i> ' + item['label'] + '<input type="hidden" name="' + name + '" value="' + item['value'] +'"/></div>');
  	}
  });
  }

  function deleteThis(name, setting_row, value){
        	$('#'+ name + setting_row + value).remove();

  }
  </script>

  <script type="text/javascript"><!--
  function completeCategory(name, setting_row){
    $('input[name="sale_event_settings_' + setting_row + '_category[]"]').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: 'index.php?route=marketing/sale_events/autocompleteCategory&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          json.unshift({
            category_id: 0,
            name: '<?php echo $text_none; ?>'
          });

          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['category_id']
            }
          }));
        }
      });
    },
  	'select': function(item) {
  		$('input[name="sale_event_settings_' + setting_row + '_category[]"]').val('');

      $('#sale-event-category'+ setting_row + item['value']).remove();

  		$('#sale-event-category'+ setting_row).append('<div id="sale-event-category' + setting_row + item['value'] + '"><i class="fa fa-minus-circle" onclick="deleteThis(\'sale-event-category\',\'' + setting_row + '\', \'' + item['value'] + '\')"></i> ' + item['label'] + '<input type="hidden" name="' + name + '" value="' + item['value'] +'"/></div>');
  	}
  });

  $('#sale-event-category'+ setting_row).delegate('.fa-minus-circle', 'click', function() {
  	$(this).parent().remove();
  });
  }
  </script>

  <script type="text/javascript"><!--
  function completeManufacturer(name, setting_row){
    $('input[name="sale_event_settings_' + setting_row + '_manufacturer[]"]').autocomplete({
    'source': function(request, response) {
      $.ajax({
        url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          json.unshift({
            manufacturer_id: 0,
            name: '<?php echo $text_none; ?>'
          });

          response($.map(json, function(item) {
            return {
              label: item['name'],
              value: item['manufacturer_id']
            }
          }));
        }
      });
    },
  	'select': function(item) {
  		$('input[name="sale_event_settings_' + setting_row + '_manufacturer[]"]').val('');

      $('#sale-event-manufacturer'+ setting_row + item['value']).remove();

  		$('#sale-event-manufacturer'+ setting_row).append('<div id="sale-event-manufacturer' + setting_row + item['value'] + '"><i class="fa fa-minus-circle" onclick="deleteThis(\'sale-event-manufacturer\',\'' + setting_row + '\', \'' + item['value'] + '\')" ></i> ' + item['label'] + '<input type="hidden" name="' + name + '" value="' + item['value'] +'"/></div>');
  	}
  });

  $('#sale-event-manufacturer'+ setting_row).delegate('.fa-minus-circle', 'click', function() {
  	$(this).parent().remove();
  });
  }
  </script>

  <script>
    $(document).ready(function() {
      $('#input-banner').change(function(){
        var value = this.value;
        $('#banner-images').load('index.php?route=marketing/sale_events/getBannerImages&token=<?php echo $token; ?>&banner_id=' + value + '&sale_event_id=<?php echo $sale_event_id; ?>');
      });

      $('#input-banner').trigger('change');
    });

  </script>

  <script type="text/javascript"><!--
  $('#language a:first').tab('show');

  // <script type="text/javascript">
  //       $(function () {
  //           $('#datetimepicker2').datetimepicker({
  //               locale: 'ru'
  //           });
  //       });
  //   </script>

$('.date').datetimepicker({

      locale: 'ru',
      pick12HourFormat: false

	// pickTime: false
});
//--></script>
