<div class="row">
  <p><br><br></p>


    <?php foreach ($banner_images as $key => $banner_image1) { ?>
      <?php foreach($banner_image1['banner_image_description'] as $language_id => $banner_image){ ?>

        <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
            <?php if($banner_image_radio === $banner_image1['banner_image_id']){ ?>
              <input id="banner-image" value = "<?php echo $banner_image1['banner_image_id']; ?>  " checked class="" type="radio" name="banner_image_radio">
            <?php }else{ ?>
              <input id="banner-image"  value = "<?php echo $banner_image1['banner_image_id']; ?>" class="" type="radio" name="banner_image_radio">
            <?php } ?>
            <img src="<?php echo $banner_images[$key]['banner_image_description'][$config_language_id]['image']; ?>" alt="">
            <div class="caption">
              <h3 class = "text-center"><?php echo $banner_images[$key]['banner_image_description'][$config_language_id]['title']; ?></h3>
            </div>
          </div>
        </div>
      <?php } ?>
    <?php } ?>

  </div>
<script type="text/javascript"><!--
$('#language a:first').tab('show');
</script>
