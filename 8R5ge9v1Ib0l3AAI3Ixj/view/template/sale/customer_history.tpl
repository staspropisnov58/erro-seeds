<?php if ($error_warning) { ?>
<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<?php if ($success) { ?>
<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<table class="table table-bordered">
  <thead>
    <tr>
      <td class="text-left"><?php echo $column_date_added; ?></td>
      <td class="text-left"><?php echo $column_comment; ?></td>
    </tr>
  </thead>
  <tbody>
    <?php if ($histories) { ?>
    <?php foreach ($histories as $history) { ?>
    <tr>
      <td class="text-left"><?php echo $history['date_added']; ?></td>
      <td class="text-left"><?php echo $history['comment']; ?></td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td class="text-center" colspan="2"><?php echo $text_no_results; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<div class="row">
  <div data-paginate="history" class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
<br />
<div class="form-group">
  <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
  <div class="col-sm-10">
    <textarea name="comment" rows="8" placeholder="<?php echo $entry_comment; ?>" id="input-comment" class="form-control"></textarea>
  </div>
</div>
<div class="text-right">
  <button type= "button" id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?></button>
</div>
<script type="text/javascript"><!--

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#history').load(this.href);
});

$('#button-history').on('click', function(e) {
e.preventDefault();
$.ajax({
  url: 'index.php?route=sale/customer/addHistories&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
  type: 'post',
  dataType: 'html',
  data: 'comment=' + encodeURIComponent($('#tab-history textarea[name=\'comment\']').val()),
  beforeSend: function() {
    $('#button-history').button('loading');
  },
  complete: function() {
    $('#button-history').button('reset');
  },
  success: function(html) {
    $('.alert').remove();

    $.each(JSON.parse(html), function(tab, value) {
      $('#' + tab).html(value);
    });

  //  $('#history').html(html);

    $('#tab-history textarea[name=\'comment\']').val('');
  }
});
});
</script>
