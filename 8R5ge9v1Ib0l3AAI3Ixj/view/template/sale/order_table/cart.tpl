<div class="panel-body" id="cart-table">
  <div class="alert alert-danger" style="display: none;"></div>
  <div class="alert alert-warning" style="display: none;"></div>
  <div class="alert alert-success" style="display: none;"></div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <td class="text-left"><?php echo $column_product; ?></td>
        <td class="text-left"><?php echo $column_model; ?></td>
        <td class="text-left"></td>
        <td class="text-right"><?php echo $column_quantity; ?></td>
        <td class="text-right"><?php echo $column_price; ?></td>
        <td class="text-right"><?php echo $column_total; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $product) { ?>
      <tr>
        <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
        </td>
        <td class="text-left"><?php echo $product['model']; ?></td>
        <td class="text-left">                  <?php foreach ($product['option'] as $option) { ?>
                          <?php if ($option['type'] != 'file') { ?>
                          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                          <?php } else { ?>
                          &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
                          <?php } ?>
                          <?php } ?></td>
        <td class="text-right"><?php echo $product['quantity']; ?></td>
        <td class="text-right"><?php echo $product['price']; ?></td>
        <td class="text-right"><?php echo $product['total']; ?></td>
      </tr>
      <?php } ?>
      <?php if ($gift_programs) { ?>
        <?php foreach ($gift_programs as $gift_program) { ?>
          <?php foreach ($gift_program['gifts'] as $gift) { ?>
            <tr>
              <td class="text-left"><a href="<?php echo $gift['href']; ?>"><?php echo $gift['name']; ?></a>
              </td>
              <td class="text-left"><?php echo $gift['model']; ?></td>
              <td class="text-left"><?php echo $text_gift; ?></td>
              <td class="text-right"><?php echo $gift['quantity']; ?></td>
              <td class="text-right"></td>
              <td class="text-right"></td>
            </tr>
          <?php } ?>
        <?php } ?>
      <?php } ?>
      <?php foreach ($vouchers as $voucher) { ?>
      <tr>
        <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
        <td class="text-left"></td>
        <td class="text-right">1</td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
      </tr>
      <?php } ?>
      <tr>
        <td colspan="4" class="text-right">Купон</td>
        <td class="text-right">
          <input type="text" name="coupon" value="123456" id="input-coupon" class="pull-left" style="width:80%">
          <button id="button-coupon" data-loading-text="Загрузка..." data-toggle="tooltip" title="" class="btn btn-success btn-xs pull-right" data-original-title="Сохранить купон"><i class="fa fa-plus-circle"></i></button>
        </td>
      </tr>
      <?php foreach ($totals as $total) { ?>
      <tr>
        <td colspan="4" class="text-right"><strong><?php echo $total['title']; ?></strong></td>
        <td class="text-right"><?php echo $total['text']; ?></td>
      </tr>
      <?php } ?>
      <tr>
        <td colspan="4" class="text-right"><span data-toggle="tooltip" title="<?php echo $help_order_discount; ?>"><?php echo $entry_order_discount; ?></span></td>
        <td class="text-right">
          <input type="text" name="discount" value="<?php echo $totals['total']['text']; ?>" id="input-discount" class="pull-left" style="width:80%">
          <button id="button-discount" data-loading-text="Загрузка..." title="" class="btn btn-primary btn-xs pull-right" data-original-title="Сохранить купон"><i class="fa fa-save"></i></button>
        </td>
      </tr>
    </tbody>
  </table>
  <table class="table" style="border:none">
    <thead>
      <tr>
        <td>Для вставки в таблицу</td>
      </tr>
    </thead>
    <tr>
      <td>
        <pre>
<?php if (isset($totals['addition'])) { echo $totals['addition']['title']; } ?><?php foreach ($products as $product) { ?>
<?php $option_string = ''; ?>
<?php foreach ($product['option'] as $option) {
$option_string .= '[' . $option['name'] . '-' . trim($option['value']) . ']';
} ?>

<?php echo str_pad('[' . $product['model'] . ']', 20, ' ', STR_PAD_RIGHT); ?><?php echo str_pad($product['name'], 40, ' ', STR_PAD_RIGHT); ?><?php echo str_pad($option_string, 20, ' ', STR_PAD_LEFT); ?><?php echo str_pad('(' . $product['quantity'] . ')', 7, ' ', STR_PAD_BOTH); ?><?php echo str_pad($product['total'], 7, ' ', STR_PAD_LEFT); ?>
    <?php } ?>
    <?php if ($gift_programs) { ?>
      <?php foreach ($gift_programs as $gift_program) { ?>
        <?php foreach ($gift_program['gifts'] as $gift) { ?>

<?php echo str_pad('[' . $gift['model'] . ']', 20, ' ', STR_PAD_RIGHT); ?><?php echo str_pad($gift['name'], 40, ' ', STR_PAD_RIGHT); ?><?php echo str_pad('(' . $gift['quantity'] . ')', 7, ' ', STR_PAD_BOTH); ?><?php echo str_pad($text_gift, 7, ' ', STR_PAD_LEFT); ?>
          </tr>
        <?php } ?>
      <?php } ?>
    <?php } ?>
    </pre>
    </td>
    </tr>
  </table>

</div>
