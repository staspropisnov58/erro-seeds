<?php if (isset($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>

<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td class ="text-left"></td>
        <td class="text-left"><?php echo $text_id_orders; ?></td>
        <td class="text-left"><?php echo $text_status_name; ?></td>
        <td class="text-left"><?php echo $text_total_order; ?></td>
        <td class="text-left"><?php echo $text_bonuse_affiliate; ?></td>
        <td class="text-right"><?php echo $text_date_added; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php  if ($orders) { ?>
      <?php foreach ($orders as $order) { ?>

      <tr>
        <td class="checkbox_td"> <input type="checkbox" name="order" value="<?php echo $order['order_id']; ?>"></td>
        <td class="text-left"><?php echo $order['order_id']; ?></td>
        <td style="color: <?php echo $order['color'];?>; <?php if($order['color'] !== '#666666'){ ?> font-weight:bold; <?php } ?>" class="text-left"><?php echo $order['name']; ?></td>
        <td class="text-left"><?php echo $order['total']; ?></td>
        <td class="text-left"><?php echo $order['commission']; ?></td>
        <td class="text-right"><?php echo $order['date_added']; ?></td>

      </tr>
    <?php } ?>
      </tbody>
      <tfoot>
      <tr>
        <td>&nbsp;</td>
        <td class="text-right"><b><?php echo $text_total_payment_amount; ?></b></td>
        <td class="text-right" id="payouts-total"><?php echo $total_amount_orders; ?></td>
        <td class="text-right"><b><?php echo $text_total_commission; ?></b></td>
        <td class="text-right" id="commission-total"><?php echo $total_commission; ?></td>


      </tr>
      <tfoot>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
      </tr>
      </tbody>
      <?php } ?>

  </table>

  <div class="row">
    <div data-paginate="pending-affiliate-orders" class="col-sm-6 text-left pending-affiliate-orders"><?php if (isset($pagination)){ ?><?php echo $pagination; ?> <?php } ?><a href="<?php echo $url; ?>" class="show-all"><?php if(!isset($show_all)){ ?> <?php echo $text_show_all ?> <?php } ?></a></div>
    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
  </div>

  <?php if ($affiliate_bonus != 1){ ?>
  <div class="text-center clearfix form-group">
    <label class="col-sm-2 control-label" for="notify"> <?php echo $text_notify_email_buyer; ?></label>
    <div class="col-sm-10 text-left" style="margin-top: 9px">  <input type="checkbox" id="notify" name="notify"> </div>
  </div>
<?php } ?>
<?php if ($sms_affiliate_bonus != 1){ ?>
  <div class="text-center clearfix form-group">
    <label class="col-sm-2 control-label" for="notify_sms"> <?php echo $text_notify_sms_buyer; ?></label>
    <div class="col-sm-10 text-left" style="margin-top: 9px">  <input type="checkbox" id="notify_sms" name="notify_sms"  value="1"> </div>
  </div>
</div>
<?php } ?>
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-comment"><?php echo $text_entry_comment; ?></label>
    <div class="col-sm-10 text-left">
      <textarea name="comment" rows="8" placeholder="<?php echo $text_entry_comment; ?>" class="form-control"></textarea>
    </div>
  </div>

  <div class="text-right">
    <button  type="button" id="button-complete-orders" class="btn btn-info"><i class="fa fa-times-circle-o"></i> <?php echo $button_close_order; ?></button>
  </div>
</div>
<script type="text/javascript"><!--
$('#button-complete-orders').on('click', function() {
var orders = [];
$('input[name="order"]:checked').each(function () {
    $(this).parents().each(function () {
    if ($(this).is('tr')) {
      $(this).remove();
      return;
    }
  });
  orders.push($(this).val());
});
if (orders.length) {
  $.ajax({
    url: 'index.php?route=sale/customer/completeOrders&token=<?php echo $token;?>&customer_id=<?php echo $customer_id;?>',
    type: 'post',
    dataType: 'html',
    data: 'orders=' + JSON.stringify(orders) + '&notify=' + ($('#pending-affiliate-orders input[name=\'notify\']').prop('checked') ? 1 : 0) + '&notify_sms=' + ($('#pending-affiliate-orders input[name=\'notify_sms\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('#pending-affiliate-orders textarea[name=\'comment\']').val()),
    beforeSend: function() {
      $('#button-complete-orders').button('loading');
    },
    complete: function() {
      $('#button-complete-orders').button('reset');
    },
    success: function(html) {
      $('.alert').remove();
      $.each(JSON.parse(html), function(tab, value) {
        $('#' + tab).html(value);
      });
      }
  });
}
});
//--></script>
