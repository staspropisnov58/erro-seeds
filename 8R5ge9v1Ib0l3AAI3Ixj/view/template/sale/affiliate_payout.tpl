<?php if (isset($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>

<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td class ="text-left"></td>
        <td class="text-left"><?php echo $column_date_added; ?></td>
        <td class="text-left"><?php echo $column_description; ?></td>
        <td class="text-right"><?php echo $column_amount; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php  if ($payouts) { ?>
      <?php foreach ($payouts as $payout) { ?>

      <tr>
        <td class="checkbox_td"> <input type="checkbox" name="payout" value="<?php echo $payout['date_payout']; ?>"></td>
        <td class="text-left"><?php echo $payout['show_date_payout']; ?></td>
        <td class="text-left"><?php echo $payout['description']; ?></td>
        <td class="text-right"><?php echo $payout['amount']; ?></td>
      </tr>
    <?php } ?>
      </tbody>
      <tfoot>
      <tr>
        <td>&nbsp;</td>
        <td class="text-right"><b><?php echo $text_balance_paid; ?></b></td>
        <td class="text-right" id="payouts-total"><?php echo $payout_total; ?></td>
      </tr>
      <tfoot>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
      </tr>
      </tbody>
      <?php } ?>

  </table>

  <div class="row">
    <div data-paginate="payout" class="col-sm-6 text-left payout"><?php if (isset($pagination)){ ?><?php echo $pagination; ?> <?php } ?><a href="<?php echo $url; ?>" class="show-all"><?php if(!isset($show_all)){ ?> <?php echo $text_show_all ?> <?php } ?></a></div>
    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
  </div>


  <div class="text-center clearfix form-group">
    <label class="col-sm-2 control-label" for="notify"> <?php echo $text_notify_buyer; ?></label>
    <div class="col-sm-10 text-left" style="margin-top: 9px">  <input type="checkbox" name="notify"></div>

  </div>

  <div class="text-center clearfix form-group">
    <label class="col-sm-2 control-label" for="notify_sms"> <?php echo $text_notify_email_buyer; ?></label>
    <div class="col-sm-10 text-left" style="margin-top: 9px">  <input type="checkbox" name="notify_sms"> </div>

  </div>

<div class="form-group">
  <label class="col-sm-2 control-label" for="input-comment"><?php echo $text_notify_sms_buyer; ?></label>
  <div class="col-sm-10">
    <textarea name="comment" rows="8" placeholder="<?php echo $text_entry_comment; ?>" class="form-control"></textarea>
  </div>
</div>
<div class="text-right">
  <button  type="button" id="button-delete-payouts" class="btn btn-danger"><i class="fa fa-times-circle-o"></i> <?php echo $button_transaction_delete; ?></button>
</div>

</div>
<script type="text/javascript"><!--
$('#button-delete-payouts').on('click', function() {
var payouts = [];
$('input[name="payout"]:checked').each(function () {
  $(this).parents().each(function () {
    if ($(this).is('tr')) {
      $(this).remove();
      return;
    }
  });
  payouts.push($(this).val());
});
if (payouts.length) {
$.ajax({
  url: 'index.php?route=sale/customer/deletePayout&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
  type: 'post',
  dataType: 'html',
  data: 'payouts=' + JSON.stringify(payouts) + '&notify=' + ($('#payout input[name=\'notify\']').prop('checked') ? 1 : 0) + '&notify_sms=' + ($('#payout input[name=\'notify_sms\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('#payout textarea[name=\'comment\']').val()),
  beforeSend: function() {
    $('#button-delete-payouts').button('loading');
  },
  complete: function() {
    $('#button-delete-payouts').button('reset');
  },
  success: function(html) {
    $('.alert').remove();

  //  console.log(html);

    $.each(JSON.parse(html), function(tab, value) {
      $('#' + tab).html(value);
    });
  }
});
}
});
//--></script>
