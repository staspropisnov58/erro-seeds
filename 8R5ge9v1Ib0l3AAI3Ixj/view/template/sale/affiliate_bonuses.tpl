<?php if (isset($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<div class="form-group">
  <label class="col-sm-2 control-label" for="input-description"><?php echo $text_code; ?></label>
  <div class="col-sm-10">
    <input type="text" name="code" value="<?php echo $code; ?>" placeholder="" id="input-description" class="form-control" />
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label" for="input-description"><?php echo $text_commission; ?></label>
  <div class="col-sm-10">
    <input type="text" name="commission" value="<?php echo $commission; ?>" placeholder="" id="input-description" class="form-control" />
  </div>
</div>
<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td></td>
        <td class="text-left"><?php echo $column_date_added; ?></td>
        <td class="text-left"><?php echo $column_description; ?></td>
        <td class="text-right"><?php echo $column_amount; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php if ($bonuses) { ?>
      <?php foreach ($bonuses as $bonuse) { ?>
      <tr>
        <td class="checkbox_td"> <input type="checkbox" name="bonus" value="<?php echo $bonuse['affiliate_transaction_id']; ?>"> </td>
        <td class="text-left"><?php echo $bonuse['date_added']; ?></td>
        <td class="text-left"><?php echo $bonuse['description']; ?></td>
        <td class="text-right"><?php echo $bonuse['amount']; ?></td>
      </tr>
      <?php } ?>
      </tbody>
      <tfoot>
      <tr>
        <td>&nbsp;</td>
        <td class="text-right"><b><?php echo $text_balance; ?></b></td>
        <td class="text-right" id="bonuses-total"><?php echo $balance; ?></td>
      </tr>
      <tfoot>
      <?php } else { ?>
      <tr>
        <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
      </tr>
      </tbody>
      <?php } ?>
  </table>
  <div class="row">
    <div class="col-sm-6 text-left affiliate-bonuses" data-paginate="affiliate-bonuses"><?php if (isset($pagination)){ ?><?php echo $pagination; ?> <?php } ?><a href="<?php echo $url; ?>" class="show-all"><?php if(!isset($show_all)){ ?> <?php echo $text_show_all ?> <?php } ?></a></div>
    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
  </div>
</div>
<?php if ($affiliate_bonus !=1){ ?>
  <div class="text-center clearfix form-group">
    <label class="col-sm-2 control-label" for="notify"> <?php echo $text_notify_email_buyer; ?> </label>
    <div class="col-sm-10 text-left" style="margin-top: 9px">  <input type="checkbox" name="notify"> </div>
  </div>
<?php } ?>
<?php if ($affiliate_sms_bonus !=1){ ?>
  <div class="text-center clearfix form-group">
    <label class="col-sm-2 control-label" for="notify_sms"> <?php echo $text_notify_sms_buyer; ?> </label>
    <div class="col-sm-10 text-left" style="margin-top: 9px">  <input type="checkbox" name="notify_sms"> </div>
  </div>
<?php } ?>
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
    <div class="col-sm-10">
      <textarea name="comment" rows="8" placeholder="<?php echo $entry_comment; ?>" class="form-control"></textarea>
    </div>
  </div>
  <div class="text-right">
    <button type="button" id="button-bonus" class="btn btn-primary"><i class="fa fa-money"></i> <?php echo $text_pay_bonuse; ?></button>
  </div>




<script type="text/javascript"><!--
$('#button-bonus').on('click', function() {
var bonuses = [];

$('input[name="bonus"]:checked').each(function () {
  $(this).parents().each(function () {
    if ($(this).is('tr')) {
      $(this).remove();
      return;
    }
  });
  bonuses.push($(this).val());
});
if (bonuses.length) {
$.ajax({
  url: 'index.php?route=sale/customer/payoutBonuses&token=<?php echo $token;?>&customer_id=<?php echo $customer_id;?>',
  type: 'post',
  dataType: 'html',
  data: 'bonuses=' + JSON.stringify(bonuses) + '&notify=' + ($('#affiliate-bonuses input[name=\'notify\']').prop('checked') ? 1 : 0) + '&notify_sms=' + ($('#affiliate-bonuses input[name=\'notify_sms\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('#affiliate-bonuses textarea[name=\'comment\']').val()),
  beforeSend: function() {
    $('#button-bonus').button('loading');
  },
  complete: function() {
    $('#button-bonus').button('reset');
  },
  success: function(html) {
    $('.alert').remove();

    $.each(JSON.parse(html), function(tab, value) {
      $('#' + tab).html(value);
    });
  }
});
}
});
//--></script>
