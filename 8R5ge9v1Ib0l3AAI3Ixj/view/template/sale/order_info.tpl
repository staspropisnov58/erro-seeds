<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $invoice; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info">
          <i class="fa fa-print"></i>
        </a>
        <a href="<?php echo $shipping; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info">
          <i class="fa fa-truck"></i>
        </a>
        <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
          <i class="fa fa-pencil"></i>
        </a>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default">
          <i class="fa fa-reply"></i>
        </a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="col-md-6">
      <div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo (isset($text_order_detail)?$text_order_detail:''); ?> Заказ</h3>
            <input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
            <button type="button" class="btn btn-primary pull-right btn-sm" data-toggle="modal" data-target="#change_order">
              <i class="fa fa-pencil"></i>
            </button>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo (isset($text_store)?$text_store:''); ?>" class="btn btn-info btn-xs"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
                <td><a href="<?php echo $store_url; ?>" target="_blank"><?php echo $store_name; ?></a></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_date_added; ?>" class="btn btn-info btn-xs"><i class="fa fa-calendar fa-fw"></i></button></td>
                <td><?php echo $date_added; ?></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_payment_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-credit-card fa-fw"></i></button></td>
                <td><?php echo $payment_method; ?></td>
              </tr>
              <?php if ($shipping_method) { ?>
              <tr>
                <td><button data-toggle="tooltip" title="<?php echo $text_shipping_method; ?>" class="btn btn-info btn-xs"><i class="fa fa-truck fa-fw"></i></button></td>
                <td><?php echo $shipping_method; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div>
        <div class="panel panel-default">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-comment-o"></i> <?php echo $text_history; ?></h3>
            </div>
            <div class="panel-body">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-history" data-toggle="tab"><?php echo $tab_history; ?></a></li>
                <li><a href="#tab-additional" data-toggle="tab"><?php echo (isset($tab_additional)?$tab_additional:''); ?>Дополнительно</a></li>
                <?php if(isset($tabs)) { ?>
                <?php foreach ($tabs as $tab) { ?>
                <li><a href="#tab-<?php echo $tab['code']; ?>" data-toggle="tab"><?php echo $tab['title']; ?></a></li>
                <?php } } ?>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab-history">
                  <div id="history"></div>
                  <br />
                  <fieldset>
                    <legend><?php echo (isset($text_history_add)?$text_history_add:''); ?></legend>
                    <form class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                        <div class="col-sm-10">
                          <select name="order_status_id" id="input-order-status" class="form-control">
                            <?php foreach ($order_statuses as $order_statuses) { ?>
                            <?php if ($order_statuses['order_status_id'] == $order_status_id) { ?>
                            <option style="color: <?php echo $order_statuses['color']; ?>; <?php if($order_statuses['color'] !== '#666666'){ ?> font-weight:bold; <?php } ?>" value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?></option>
                            <?php } else { ?>
                            <option style="color: <?php echo $order_statuses['color']; ?>; <?php if($order_statuses['color'] !== '#666666'){ ?> font-weight:bold; <?php } ?>" value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-message-template"><?php echo $entry_message_template; ?></label>
                        <div class="col-sm-10">
                          <select name="message_template" id="input-message-template" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach($temp as $template_message){ ?>
                              <option value="<?php echo $template_message['message']; ?>"><?php echo $template_message['name']; ?></option>
                            <?php } ?>

                          </select>
                        </div>
                        </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-override">
                          <span data-toggle="tooltip"
                                       title="<?php echo (isset($help_override)?$help_override:''); ?>"><?php echo (isset($entry_override)?$entry_override:''); ?> Переопределить</span></label>
                        <div class="col-sm-10">
                          <input type="checkbox" name="override" value="1" id="input-override" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-notify"><?php echo $entry_notify; ?></label>
                        <div class="col-sm-10">
                          <input type="checkbox" name="notify" value="1" id="input-notify" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
                        <div class="col-sm-10">
                          <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                        </div>
                      </div>
                    </form>
                  </fieldset>
                  <div class="text-right">
                    <button id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?></button>
                  </div>
                </div>
                <div class="tab-pane" id="tab-additional">
                  <?php if ($account_custom_fields) { ?>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <td colspan="2"><?php echo $text_account_custom_field; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($account_custom_fields as $custom_field) { ?>
                        <tr>
                          <td><?php echo $custom_field['name']; ?></td>
                          <td><?php echo $custom_field['value']; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } ?>
                  <?php if ($payment_custom_fields) { ?>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <td colspan="2"><?php echo $text_payment_custom_field; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($payment_custom_fields as $custom_field) { ?>
                        <tr>
                          <td><?php echo $custom_field['name']; ?></td>
                          <td><?php echo $custom_field['value']; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } ?>
                  <?php if ($shipping_method && $shipping_custom_fields) { ?>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <td colspan="2"><?php echo $text_shipping_custom_field; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($shipping_custom_fields as $custom_field) { ?>
                        <tr>
                          <td><?php echo $custom_field['name']; ?></td>
                          <td><?php echo $custom_field['value']; ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <?php } ?>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <td colspan="2"><?php echo (isset($text_browser)?$text_browser:''); ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><?php echo $text_ip; ?></td>
                          <td><?php echo $ip; ?></td>
                        </tr>
                        <?php if ($forwarded_ip) { ?>
                        <tr>
                          <td><?php echo $text_forwarded_ip; ?></td>
                          <td><?php echo $forwarded_ip; ?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                          <td><?php echo $text_user_agent; ?></td>
                          <td><?php echo $user_agent; ?></td>
                        </tr>
                        <tr>
                          <td><?php echo $text_accept_language; ?></td>
                          <td><?php echo $accept_language; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <?php if(isset($tabs)) { ?>
                <?php foreach ($tabs as $tab) { ?>
                <div class="tab-pane" id="tab-<?php echo $tab['code']; ?>"><?php echo $tab['content']; ?></div>
                <?php } } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-info-circle"></i> <?php echo $text_order; ?> #<?php echo $order_id; ?></h3>
          <button type="button" class="btn btn-primary pull-right btn-sm" data-toggle="modal" data-target="#change_shipping_address">
            <i class="fa fa-pencil"></i>
          </button>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <?php if ($shipping_method) { ?>
                <td style="width: 50%;" class="text-left"><?php echo $text_shipping_address; ?></td>
                <?php } ?>
              </tr>
            </thead>
            <tbody>
              <tr>
                <?php if ($shipping_method) { ?>
                <td class="text-left"><?php echo (isset($shipping_address)?$shipping_address:''); ?>
                  <?php echo $shipping_firstname; ?> <?php echo $shipping_lastname; ?><br>
                  <?php echo $shipping_address_1; ?> <?php echo $shipping_postcode; ?><br>
                  <?php echo $shipping_city; ?><br>
                  <?php echo $shipping_zone; ?><br>
                  <?php echo $shipping_country; ?>
                </td>
                <?php } ?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Корзина</h3>
          <button type="button" class="btn btn-primary pull-right btn-sm" data-toggle="modal" data-target="#change_cart">
            <i class="fa fa-pencil"></i>
          </button>
        </div>
        <div class="panel-body" id="cart-table">
          <div class="alert alert-danger" style="display: none;"></div>
          <div class="alert alert-warning" style="display: none;"></div>
          <div class="alert alert-success" style="display: none;"></div>
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left"><?php echo $column_product; ?></td>
                <td class="text-left"><?php echo $column_model; ?></td>
                <td class="text-left"></td>
                <td class="text-right"><?php echo $column_quantity; ?></td>
                <td class="text-right"><?php echo $column_price; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                </td>
                <td class="text-left"><?php echo $product['model']; ?></td>
                <td class="text-left">                  <?php foreach ($product['option'] as $option) { ?>
                                  <?php if ($option['type'] != 'file') { ?>
                                  &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                  <?php } else { ?>
                                  &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
                                  <?php } ?>
                                  <?php } ?></td>
                <td class="text-right"><?php echo $product['quantity']; ?></td>
                <td class="text-right"><?php echo $product['price']; ?></td>
                <td class="text-right"><?php echo $product['total']; ?></td>
              </tr>
              <?php } ?>
              <?php if ($gift_programs) { ?>
                <?php foreach ($gift_programs as $gift_program) { ?>
                  <?php foreach ($gift_program['gifts'] as $gift) { ?>
                    <tr>
                      <td class="text-left"><a href="<?php echo $gift['href']; ?>"><?php echo $gift['name']; ?></a>
                      </td>
                      <td class="text-left"><?php echo $gift['model']; ?></td>
                      <td class="text-left"><?php echo $text_gift; ?></td>
                      <td class="text-right"><?php echo $gift['quantity']; ?></td>
                      <td class="text-right"></td>
                      <td class="text-right"></td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <tr>
                <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
                <td class="text-left"></td>
                <td class="text-right">1</td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="4" class="text-right">Купон</td>
                <td class="text-right">
                  <input type="text" name="coupon" value="123456" id="input-coupon" class="pull-left" style="width:80%">
                  <button id="button-coupon" data-loading-text="Загрузка..." data-toggle="tooltip" title="" class="btn btn-success btn-xs pull-right" data-original-title="Сохранить купон"><i class="fa fa-plus-circle"></i></button>
                </td>
              </tr>
              <?php foreach ($totals as $total) { ?>
              <tr>
                <td colspan="4" class="text-right"><strong><?php echo $total['title']; ?></strong></td>
                <td class="text-right"><?php echo $total['text']; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="4" class="text-right"><span data-toggle="tooltip" title="<?php echo $help_order_discount; ?>"><?php echo $entry_order_discount; ?></span></td>
                <td class="text-right">
                  <input type="text" name="discount" value="<?php echo $totals['total']['text']; ?>" id="input-discount" class="pull-left" style="width:80%">
                  <button id="button-discount" data-loading-text="Загрузка..." title="" class="btn btn-primary btn-xs pull-right" data-original-title="Сохранить купон"><i class="fa fa-save"></i></button>
                </td>
              </tr>
            </tbody>
          </table>
          <table class="table" style="border:none">
            <thead>
              <tr>
                <td>Для вставки в таблицу</td>
              </tr>
            </thead>
            <tr>
              <td>
                <pre>
<?php if (isset($totals['addition'])) { echo $totals['addition']['title']; } ?><?php foreach ($products as $product) { ?>
<?php $option_string = ''; ?>
<?php foreach ($product['option'] as $option) {
$option_string .= '[' . $option['name'] . '-' . trim($option['value']) . ']';
} ?>

<?php echo str_pad('[' . $product['model'] . ']', 20, ' ', STR_PAD_RIGHT); ?><?php echo str_pad($product['name'], 40, ' ', STR_PAD_RIGHT); ?><?php echo str_pad($option_string, 20, ' ', STR_PAD_LEFT); ?><?php echo str_pad('(' . $product['quantity'] . ')', 7, ' ', STR_PAD_BOTH); ?><?php echo str_pad($product['total'], 7, ' ', STR_PAD_LEFT); ?>
            <?php } ?>
            <?php if ($gift_programs) { ?>
              <?php foreach ($gift_programs as $gift_program) { ?>
                <?php foreach ($gift_program['gifts'] as $gift) { ?>

<?php echo str_pad('[' . $gift['model'] . ']', 20, ' ', STR_PAD_RIGHT); ?><?php echo str_pad($gift['name'], 40, ' ', STR_PAD_RIGHT); ?><?php echo str_pad('(' . $gift['quantity'] . ')', 7, ' ', STR_PAD_BOTH); ?><?php echo str_pad($text_gift, 7, ' ', STR_PAD_LEFT); ?>
                  </tr>
                <?php } ?>
              <?php } ?>
            <?php } ?>
            </pre>
            </td>
            </tr>
          </table>

        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-user"></i> <?php echo $text_customer_detail; ?></h3>
          <button type="button" class="btn btn-primary pull-right btn-sm" data-toggle="modal" data-target="#change_customer">
            <i class="fa fa-pencil"></i>
          </button>
        </div>
        <table class="table">
          <tr>
            <td style="width: 1%;"><button data-toggle="tooltip" title="<?php echo $text_customer; ?>" class="btn btn-info btn-xs"><i class="fa fa-user fa-fw"></i></button></td>
            <td><?php if ($customer) { ?>
              <a href="<?php echo $customer; ?>" target="_blank"><?php echo $firstname; ?> <?php echo $lastname; ?></a>
              <?php } else { ?>
              Гость
              <?php } ?></td>
          </tr>
          <tr>
            <td><button data-toggle="tooltip" title="<?php echo $text_customer_group; ?>" class="btn btn-info btn-xs"><i class="fa fa-group fa-fw"></i></button></td>
            <td><?php echo $customer_group; ?></td>
          </tr>
          <tr>
            <td><button data-toggle="tooltip" title="<?php echo $text_email; ?>" class="btn btn-info btn-xs"><i class="fa fa-envelope-o fa-fw"></i></button></td>
            <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
          </tr>
          <tr>
            <td><button data-toggle="tooltip" title="<?php echo $text_telephone; ?>" class="btn btn-info btn-xs"><i class="fa fa-phone fa-fw"></i></button></td>
            <td><?php if(isset($tel_code)){echo $tel_code;}?><?php echo $telephone; ?>
              <?php if ($no_callback) { ?>
                <span class="label label-danger"><?php echo $text_no_callback; ?></span>
              <?php } ?>
            </td>
          </tr>
        </table>
        <?php if ($comment) { ?>
        <table class="table table-bordered">
          <thead>
            <tr class="info">
              <td><i class="fa fa-comment"></i> <?php echo $text_comment; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="info">
              <td><?php echo $comment; ?></td>
            </tr>
          </tbody>
        </table>
        <?php } ?>
      </div>
      <div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cog"></i> <?php echo (isset($text_option)?$text_option:''); ?>Опции</h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td><?php echo (isset($text_invoice)?$text_invoice:''); ?> Счет</td>
                <td id="invoice" class="text-right"><?php echo $invoice_no; ?></td>
                <td style="width: 1%;" class="text-center"><?php if (!$invoice_no) { ?>
                  <button id="button-invoice" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_generate; ?>" class="btn btn-success btn-xs"><i class="fa fa-cog"></i></button>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-refresh"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_reward; ?></td>
                <td class="text-right"><?php echo $reward; ?></td>
                <td class="text-center"><?php if ($customer && $reward) { ?>
                  <?php if (!$reward_total) { ?>
                  <button id="button-reward-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-reward-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_reward_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_affiliate; ?>
                  <?php if ($affiliate) { ?>
                  (<a href="<?php echo $affiliate; ?>"><?php echo $affiliate_firstname; ?> <?php echo $affiliate_lastname; ?></a>)
                  <?php } ?></td>
                <td class="text-right"><?php echo $commission; ?></td>
                <td class="text-center"><?php if ($affiliate) { ?>
                  <?php if (!$commission_total) { ?>
                  <button id="button-commission-add" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_add; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } else { ?>
                  <button id="button-commission-remove" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_commission_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></button>
                  <?php } ?>
                  <?php } else { ?>
                  <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  <?php } ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">

    </div>
  </div>



  <!-- Модалка "заказ" -->
  <div class="modal fade" id="change_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Заказ</h4>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <label class="col-sm-3 control-label" for="input-shipping-method">Способ доставки</label>
            <div class="col-sm-9">
              <div class="input-group col-sm-12">
                <select name="shipping_method" id="input-shipping-method" class="form-control">
                  <option value="">---Выберите---</option>
                  <optgroup label="Dostavista">
                    <option value="dostavista.dostavista" selected="selected">Dostavista (По Санкт-Петербургу. Доставка в тот же день.) - 400 ₽</option>
                  </optgroup>
                  <optgroup label="Почта России"><option value="rupost.rupost">Почта России (по России) - 250 ₽</option></optgroup>
                  <optgroup label="Почта России"><option value="rupostint.rupostint">Почта России (Международная) - 400 ₽</option></optgroup>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 control-label" for="input-payment-method">Способ оплаты</label>
            <div class="col-sm-9">
              <div class="input-group col-sm-12">
                <select name="payment_method" id="input-payment-method" class="form-control">
                  <option value="">---Выберите---</option>
                  <option value="qiwi" selected="selected">Qiwi</option>
                  <option value="sberacc">Счет в Сбербанке</option>
                  <option value="sbercard">Карта Сбербанка</option>
                  <option value="cod">Наложенный платежТолько по территории РФ.</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Сохранить</button>
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <!-- Модалка "клиент" -->
  <div class="modal fade" id="change_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Клиент</h4>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <label class="col-sm-3 control-label" for="input-firstname">Имя, Фамилия</label>
            <div class="col-sm-9">
              <input type="text" name="firstname" value="test" id="input-firstname" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 control-label" for="input-email">E-Mail</label>
            <div class="col-sm-9">
              <input type="text" name="email" value="seoboostseo1@gmail.com" id="input-email" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 control-label" for="input-telephone">Телефон</label>
            <div class="col-sm-9">
              <input type="text" name="telephone" value="+380667925700" id="input-telephone" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 control-label" for="input-customer-group">Группа покупателей</label>
            <div class="col-sm-9">
              <select name="customer_group_id" id="input-customer-group" class="form-control">
                <option value="1" selected="selected">Default</option>
                <option value="3">test</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Сохранить</button>
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <!-- Модалка "детали заказа" -->
  <div class="modal fade" id="change_shipping_address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Информация о доставке</h4>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <label class="col-sm-3 control-label" for="input-payment-address">Выбрать адрес</label>
            <div class="col-sm-9">
              <select name="payment_address" id="input-payment-address" class="form-control">
                <option value="0" selected="selected"> --- Не выбрано --- </option>
              </select>
            </div>
          </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-firstname">Имя, Отчество</label>
                <div class="col-sm-9">
                  <input type="text" name="firstname" value="test test test" id="input-payment-firstname" class="form-control">
                </div>
              </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-lastname">Фамилия</label>
                <div class="col-sm-9">
                  <input type="text" name="lastname" value="test" id="input-payment-lastname" class="form-control">
                </div>
              </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-company">Компания</label>
                <div class="col-sm-9">
                  <input type="text" name="company" value="" id="input-payment-company" class="form-control">
                </div>
              </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-address-1">Адрес 1</label>
                <div class="col-sm-9">
                  <input type="text" name="address_1" value="test test" id="input-payment-address-1" class="form-control">
                </div>
              </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-city">Город</label>
                <div class="col-sm-9">
                  <input type="text" name="city" value="test test test" id="input-payment-city" class="form-control">
                </div>
              </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-postcode">Индекс:</label>
                <div class="col-sm-9">
                  <input type="text" name="postcode" value="1267627" id="input-payment-postcode" class="form-control">
                </div>
              </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-country">Страна</label>
                <div class="col-sm-9">
                  <select name="country_id" id="input-payment-country" class="form-control">
                    <option value="">---Выберите---</option>
                    <option value="244">Aaland Islands</option>
                    <option value="1">Afghanistan</option>
                    <option value="2">Albania</option>
                    <option value="3">Algeria</option>
                    <option value="4">American Samoa</option>
                    <option value="5">Andorra</option>
                    <option value="175" selected="selected">Romania</option>
                    <option value="176">Российская Федерация</option>
                  </select>
                </div>
              </div>
          <div class="form-group row">
                <label class="col-sm-3 control-label" for="input-payment-zone">Область</label>
                <div class="col-sm-9">
                  <select name="zone_id" id="input-payment-zone" class="form-control">
                    <option value="">---Выберите---</option>
                    <option value="2679">Alba</option>
                    <option value="2680">Arad</option>
                    <option value="2681">Arges</option>
                    <option value="2682">Bacau</option>
                    <option value="2683">Bihor</option>
                    <option value="2684">Bistrita-Nasaud</option>
                  </select>
                </div>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Сохранить</button>
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <!-- Модалка "корзина" -->
  <div class="modal fade" id="change_cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Корзина</h4>
        </div>
        <div class="modal-body">
          <div style="max-height:500px;overflow-y: auto;margin-bottom:20px">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left">Товар</td>
                        <td class="text-left">Модель</td>
                        <td class="text-right">Количество</td>
                        <td class="text-right">Цена за единицу</td>
                        <td class="text-right">Итого</td>
                        <td>Действие</td>
                      </tr>
              </thead>
              <tbody id="cart">
                <tr>
                  <td class="text-left">Amnesia Feminised <br>
                    <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                    <input type="hidden" name="product[0][option][303]" value="273"></td>
                    <td class="text-left">Errors Seeds Gold</td>
                    <td class="text-right">
                      <div class="input-group btn-block" style="max-width: 200px;">
                        <input type="text" name="product[0][quantity]" value="1" class="form-control">
                        <span class="input-group-btn">
                          <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                            <i class="fa fa-refresh"></i>
                          </button>
                        </span>
                      </div>
                    </td>
                    <td class="text-right">1161 ₽</td>
                    <td class="text-right">1161 ₽</td>
                    <td class="text-center" style="width: 3px;">
                      <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                        <i class="fa fa-minus-circle"></i>
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td class="text-left">Amnesia Feminised <br>
                      <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                      <input type="hidden" name="product[0][option][303]" value="273"></td>
                      <td class="text-left">Errors Seeds Gold</td>
                      <td class="text-right">
                        <div class="input-group btn-block" style="max-width: 200px;">
                          <input type="text" name="product[0][quantity]" value="1" class="form-control">
                          <span class="input-group-btn">
                            <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                              <i class="fa fa-refresh"></i>
                            </button>
                          </span>
                        </div>
                      </td>
                      <td class="text-right">1161 ₽</td>
                      <td class="text-right">1161 ₽</td>
                      <td class="text-center" style="width: 3px;">
                        <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                          <i class="fa fa-minus-circle"></i>
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td class="text-left">Amnesia Feminised <br>
                        <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                        <input type="hidden" name="product[0][option][303]" value="273"></td>
                        <td class="text-left">Errors Seeds Gold</td>
                        <td class="text-right">
                          <div class="input-group btn-block" style="max-width: 200px;">
                            <input type="text" name="product[0][quantity]" value="1" class="form-control">
                            <span class="input-group-btn">
                              <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                                <i class="fa fa-refresh"></i>
                              </button>
                            </span>
                          </div>
                        </td>
                        <td class="text-right">1161 ₽</td>
                        <td class="text-right">1161 ₽</td>
                        <td class="text-center" style="width: 3px;">
                          <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                            <i class="fa fa-minus-circle"></i>
                          </button>
                        </td>
                      </tr>
                      <tr>
                        <td class="text-left">Amnesia Feminised <br>
                          <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                          <input type="hidden" name="product[0][option][303]" value="273"></td>
                          <td class="text-left">Errors Seeds Gold</td>
                          <td class="text-right">
                            <div class="input-group btn-block" style="max-width: 200px;">
                              <input type="text" name="product[0][quantity]" value="1" class="form-control">
                              <span class="input-group-btn">
                                <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                                  <i class="fa fa-refresh"></i>
                                </button>
                              </span>
                            </div>
                          </td>
                          <td class="text-right">1161 ₽</td>
                          <td class="text-right">1161 ₽</td>
                          <td class="text-center" style="width: 3px;">
                            <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                              <i class="fa fa-minus-circle"></i>
                            </button>
                          </td>
                        </tr>
                        <tr>
                          <td class="text-left">Amnesia Feminised <br>
                            <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                            <input type="hidden" name="product[0][option][303]" value="273"></td>
                            <td class="text-left">Errors Seeds Gold</td>
                            <td class="text-right">
                              <div class="input-group btn-block" style="max-width: 200px;">
                                <input type="text" name="product[0][quantity]" value="1" class="form-control">
                                <span class="input-group-btn">
                                  <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                                    <i class="fa fa-refresh"></i>
                                  </button>
                                </span>
                              </div>
                            </td>
                            <td class="text-right">1161 ₽</td>
                            <td class="text-right">1161 ₽</td>
                            <td class="text-center" style="width: 3px;">
                              <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                                <i class="fa fa-minus-circle"></i>
                              </button>
                            </td>
                          </tr>
                          <tr>
                            <td class="text-left">Amnesia Feminised <br>
                              <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                              <input type="hidden" name="product[0][option][303]" value="273"></td>
                              <td class="text-left">Errors Seeds Gold</td>
                              <td class="text-right">
                                <div class="input-group btn-block" style="max-width: 200px;">
                                  <input type="text" name="product[0][quantity]" value="1" class="form-control">
                                  <span class="input-group-btn">
                                    <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                                      <i class="fa fa-refresh"></i>
                                    </button>
                                  </span>
                                </div>
                              </td>
                              <td class="text-right">1161 ₽</td>
                              <td class="text-right">1161 ₽</td>
                              <td class="text-center" style="width: 3px;">
                                <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                                  <i class="fa fa-minus-circle"></i>
                                </button>
                              </td>
                            </tr>
                            <tr>
                              <td class="text-left">Amnesia Feminised <br>
                                <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                                <input type="hidden" name="product[0][option][303]" value="273"></td>
                                <td class="text-left">Errors Seeds Gold</td>
                                <td class="text-right">
                                  <div class="input-group btn-block" style="max-width: 200px;">
                                    <input type="text" name="product[0][quantity]" value="1" class="form-control">
                                    <span class="input-group-btn">
                                      <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                                        <i class="fa fa-refresh"></i>
                                      </button>
                                    </span>
                                  </div>
                                </td>
                                <td class="text-right">1161 ₽</td>
                                <td class="text-right">1161 ₽</td>
                                <td class="text-center" style="width: 3px;">
                                  <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                                    <i class="fa fa-minus-circle"></i>
                                  </button>
                                </td>
                              </tr>
                              <tr>
                                <td class="text-left">Amnesia Feminised <br>
                                  <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                                  <input type="hidden" name="product[0][option][303]" value="273"></td>
                                  <td class="text-left">Errors Seeds Gold</td>
                                  <td class="text-right">
                                    <div class="input-group btn-block" style="max-width: 200px;">
                                      <input type="text" name="product[0][quantity]" value="1" class="form-control">
                                      <span class="input-group-btn">
                                        <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                                          <i class="fa fa-refresh"></i>
                                        </button>
                                      </span>
                                    </div>
                                  </td>
                                  <td class="text-right">1161 ₽</td>
                                  <td class="text-right">1161 ₽</td>
                                  <td class="text-center" style="width: 3px;">
                                    <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                                      <i class="fa fa-minus-circle"></i>
                                    </button>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="text-left">Amnesia Feminised <br>
                                    <input type="hidden" name="product[0][product_id]" value="144">  - <small>Фасовка: 5-в упаковке</small><br>
                                    <input type="hidden" name="product[0][option][303]" value="273"></td>
                                    <td class="text-left">Errors Seeds Gold</td>
                                    <td class="text-right">
                                      <div class="input-group btn-block" style="max-width: 200px;">
                                        <input type="text" name="product[0][quantity]" value="1" class="form-control">
                                        <span class="input-group-btn">
                                          <button type="button" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-primary" data-original-title="Обновить">
                                            <i class="fa fa-refresh"></i>
                                          </button>
                                        </span>
                                      </div>
                                    </td>
                                    <td class="text-right">1161 ₽</td>
                                    <td class="text-right">1161 ₽</td>
                                    <td class="text-center" style="width: 3px;">
                                      <button type="button" value="4998" data-toggle="tooltip" title="" data-loading-text="Загрузка..." class="btn btn-danger" data-original-title="Удалить">
                                        <i class="fa fa-minus-circle"></i>
                                      </button>
                                    </td>
                                  </tr>

                </tbody>
              </table>
            </div>
            <div class="tab-pane active" id="tab-product">
              <fieldset>
                <legend>Добавить товар(ы)</legend>
                <div class="form-group row">
                      <label class="col-sm-3 control-label" for="input-product">Выберите товар</label>
                      <div class="col-sm-9">
                        <input type="text" name="product" value="" id="input-product" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
                        <input type="hidden" name="product_id" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 control-label" for="input-quantity">Количество</label>
                      <div class="col-sm-9">
                        <input type="text" name="quantity" value="1" id="input-quantity" class="form-control">
                      </div>
                    </div>
                    <div id="option"></div>
                  </fieldset>
                  <div class="text-right">
                    <button type="button" id="button-product-add" data-loading-text="Загрузка..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Добавить</button>
                  </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Сохранить</button>
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <script type="text/javascript"><!--
$(document).delegate('#input-message-template', 'change', function() {
  $('#input-comment').val($(this).val());
});
$(document).delegate('#button-invoice', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-invoice').button('loading');
		},
		complete: function() {
			$('#button-invoice').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#tab-order').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['invoice_no']) {
				$('#button-invoice').replaceWith(json['invoice_no']);
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addreward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-add').button('loading');
		},
		complete: function() {
			$('#button-reward-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-add').replaceWith('<button id="button-reward-remove" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_reward_remove; ?></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-reward-remove').replaceWith('<button id="button-reward-add" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> <?php echo $button_reward_add; ?></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-add').replaceWith('<button id="button-commission-remove" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_remove; ?></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');

		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('#button-commission-remove').replaceWith('<button id="button-commission-add" class="btn btn-success btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_add; ?></button>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#history').load(this.href);
});

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
  if(typeof verifyStatusChange == 'function'){
    if(verifyStatusChange() == false){
      return false;
    }else{
      addOrderInfo();
    }
  }else{
    addOrderInfo();
  }

	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/order/history&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('#button-history').button('loading');
		},
		complete: function() {
			$('#button-history').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('textarea[name=\'comment\']').val('');

				$('#order-status').html($('select[name=\'order_status_id\'] option:selected').text());
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-discount', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/update_order/addDiscount&token=<?php echo $token; ?>',
		type: 'post',
		dataType: 'json',
		data: 'discount=' + $('input[name=\'discount\']').val() + '&order_id=' + $('input[name=\'order_id\']').val(),
		beforeSend: function() {
      $('#cart-table .alert').map((index, element) => {
        $(element).html('');
        $(element).hide();
      });
			$('#button-discount').button('loading');
		},
		complete: function() {
			$('#button-discount').button('reset');
		},
		success: function(json) {
      console.log(json);

			if (json['error']) {
        $('#cart-table .alert-danger').html(json['error']);
        $('#cart-table .alert-danger').show();
			}

      if (json['warning']) {
        $('#cart-table .alert-warning').html(json['warning']);
        $('#cart-table .alert-warning').show();
			}

			if (json['success']) {
        $('#cart-table .alert-success').text(json['success']);
        $('#cart-table .alert-success').show();

        $.ajax({
          url: 'index.php?route=sale/update_order/updateView&token=<?php echo $token; ?>',
          type: 'post',
      		dataType: 'json',
      		data: 'table=cart&order_id=' + $('input[name=\'order_id\']').val(),
          success: function (json) {
            if (json['error']) {
              $('#cart-table .alert-danger').html(json['error']);
              $('#cart-table .alert-danger').show();
            }

            if (json['success']) {
              $('#cart-table').replaceWith(json['html']);
              $('#cart-table .alert-success').text(json['success']);
              $('#cart-table .alert-success').show();
            }
          }
        });
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function changeStatus(){
  var status_id = $('select[name="order_status_id"]').val();

  $('#openbay-info').remove();

  $.ajax({
    url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id='+status_id,
    dataType: 'html',
    success: function(html) {
      $('#history').after(html);
    }
  });
}

function addOrderInfo(){
  var status_id = $('select[name="order_status_id"]').val();

  $.ajax({
    url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id='+status_id,
    type: 'post',
    dataType: 'html',
    data: $(".openbay-data").serialize()
  });
}

$(document).ready(function() {
  changeStatus();
});

$('select[name="order_status_id"]').change(function(){ changeStatus(); });
//--></script></div>
<?php echo $footer; ?>
