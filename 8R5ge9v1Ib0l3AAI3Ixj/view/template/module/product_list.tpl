<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-featured" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul cl  ass="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <div id="answer"></div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" name="form_send" method="post" enctype="multipart/form-data" id="form-featured" class="form-vertical">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-category"><?php echo $entry_category; ?></label>
            <div class="col-sm-10">
              <input type="text" name="category" value="<?php echo $category_name; ?>" placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control" />
              <input type="hidden" name="category_id" value="<?php echo $category_id; ?>">
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
            <div class="col-sm-10">
              <?php foreach ($languages as $language) { ?>
              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                <input type="text" name="title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($title[$language['language_id']]) ? $title[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
              </div>
              <?php } ?>
            </div>
          </div>
          <div class='form-group'>
            <label class="col-sm-2 control-label" for="input-status"><?php echo $text_appearance; ?></label>
            <div class="col-sm-10">
              <select name="appearance" id="input-appearance" class="form-control">
                <?php if($appearance == 'table') { ?>
                <option value="table" selected="selected"><?php echo $text_table; ?></option>

              <?php }else{ ?>
                <option selected="selected"><?php echo $text_make_choise ?> </option>
                <option value="table"><?php echo $text_table; ?></option>
              <?php } ?>
              </select>
          </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-product"><?php echo $entry_product; ?></label>
            <div class="col-sm-10">
              <input type="text" name="product" value="" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
              <div id="featured-product" class="well well-sm" style="height: 150px; overflow: auto;">
                <?php foreach ($products as $product) { ?>
                <div id="featured-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                  <input type="hidden" name="product[]" value="<?php echo $product['product_id']; ?>" />
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-limit"><?php echo $entry_limit; ?></label>
            <div class="col-sm-10">
              <input type="text" name="limit" value="<?php echo $limit; ?>" placeholder="<?php echo $entry_limit; ?>" id="input-limit" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-width"><?php echo $entry_width; ?></label>
            <div class="col-sm-10">
              <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
              <?php if ($error_width) { ?>
              <div class="text-danger"><?php echo $error_width; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-height"><?php echo $entry_height; ?></label>
            <div class="col-sm-10">
              <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
              <?php if ($error_height) { ?>
              <div class="text-danger"><?php echo $error_height; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><span data-toggle="tooltip" data-container="#tab-general" title="<?php echo $help_location; ?>"><?php echo $entry_attributes; ?></span></label>
            <div class="col-sm-10">
              <div class="checkbox">
                <label>
                  <?php if ($attributes) { ?>
                  <input type="checkbox" name="attributes" value="1" checked="checked" />
                  <?php } else { ?>
                    <input type="checkbox" name="attributes" value="1" />
                  <?php } ?>
                </label>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#input-main-sticker').on('change', function() {
  var filter_main_sticker = $('#input-main-sticker').val() ? '&filter_main_sticker=' +  encodeURIComponent($('#input-main-sticker').val()) : '';
  if (filter_main_sticker) {
    $.ajax({
      url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&limit=all&filter_status=1' + filter_main_sticker + '&filter_name=',
      dataType: 'json',
      success: function(json) {
        var with_sticker = [];
        $.map(json, function(item) {
          with_sticker.push(item['product_id']);
        });
        $('input[name=\'product\[\]\'').map(function() {
          if (with_sticker.includes($(this).val())) {
          } else {
            $(this).parent().remove();
          }
        });
      }
    });
  }
});

$('input[name=\'category\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        json.unshift({
          category_id: 0,
          name: '<?php echo $text_none; ?>'
        });

        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));

      }
    });
  },
  'select': function(item) {
    $('input[name=\'category\']').val(item['label']);
    $('input[name=\'category_id\']').val(item['value']);
    removeProductsOutOfCategory();
  }
});

function removeProductsOutOfCategory(){
  const category_id = $('input[name=\'category_id\']').val() ? '&category_id=' +  encodeURIComponent($('input[name=\'category_id\']').val()) : '';

  if(category_id){
      $.ajax({
        url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&limit=all&filter_status=1' + category_id + '&filter_name=',
        dataType: 'json',
        success: function(json) {
          let in_category = [];
          $.map(json, function(item) {
            in_category.push(item['product_id']);
          });
          $('input[name=\'product\[\]\'').map(function() {
            if (in_category.includes($(this).val())) {
            } else {
              $(this).parent().remove();
            }
          });
        }
      });

  }

  // console.log($('input[name=\'category_id\']').val());


}

$('input[name=\'product\']').autocomplete({

	source: function(request, response) {
    var filter_main_sticker = $('#input-main-sticker').val() ? '&filter_main_sticker=' +  encodeURIComponent($('#input-main-sticker').val()) : '';

    if (document.getElementById("input-category").value != null && document.getElementById("input-category").value != "") {
      var category_id = $('input[name=\'category_id\']').val();
    }else{
      var category_id = 0;
    }

		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_status=1&category_id=' + category_id + '&filter_name=' +  encodeURIComponent(request) + filter_main_sticker,
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'] + ' ' + item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
		$('input[name=\'product\']').val('');

		$('#featured-product' + item['value']).remove();

		$('#featured-product').append('<div id="featured-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product[]" value="' + item['value'] + '" /></div>');
	}
});

$('#featured-product').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script></div>
<?php echo $footer; ?>
