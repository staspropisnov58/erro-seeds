<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-affiliate" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-status" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="sorting_status" id="input-status" class="form-control">
                <?php if ($sorting_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <?php if(isset($admin_message)){ ?>
          <div class="alert alert-info">
            <i class="fa fa-info-circle" aria-hidden="true">&nbsp;</i> <?php echo $admin_message; ?>
          </div>
          <?php }else{ ?>
          <?php foreach ($layouts as $layout){ ?>
          <table class="table table-bordered" style="width:100%" border="1">

            <thead>
              <tr>
                <th colspan="3" class="text-center"> <?php echo $layout["name"]; ?> </th>
              </tr>
              <?php if ($error[$layout['code']]) { ?>
              <tr>
                <th colspan="3" class="text-center">
                  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error[$layout['code']]; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                  </div>
                </th>
              </tr>
              <?php } ?>
              <tr>
                <th><?php echo $sorting_with1; ?></th>
                <th><?php echo $sorting_with2; ?></th>
                <th><?php echo $sorting_type; ?></th>
              </tr>
            </thead>

            <tbody>

              <?php foreach($data_sort as $sort_code => $sort_name){ ?>

              <tr>
                <td>
                  <div class="radio">
                    <label><input type="radio" name="sorting_<?php echo $layout['code']; ?>[default][first]" value="<?php echo $sort_code; ?>" <?php if (isset($sorting_settings[$layout['code']]['default']['first']) && $sorting_settings[$layout['code']]['default']['first'] === $sort_code) {
                    echo 'checked';
                  } ?>></label>
                  </div>
                </td>
                <td>
                  <div class="radio">
                    <label><input type="radio" name="sorting_<?php echo $layout['code']; ?>[default][second]" value="<?php echo $sort_code; ?>" <?php if (isset($sorting_settings[$layout['code']]['default']['second']) && $sorting_settings[$layout['code']]['default']['second'] === $sort_code) {
                    echo 'checked';
                  } ?>></label>
                  </div>
                </td>
                <td>
                  <div class="checkbox">
                    <label><input type="checkbox" name="sorting_<?php echo $layout['code']; ?>[enabled][]" value="<?php echo $sort_code; ?>" <?php if (isset($sorting_settings[$layout['code']]['enabled']) && array_search($sort_code, $sorting_settings[$layout['code']]['enabled']) !== false) {
                      echo 'checked';
                    } ?>><?php echo $sort_name; ?></label>
                  </div>
                </td>
              </tr>
              <?php } ?>
            </tbody>
            <br>
            <?php } ?>
          </table>
          <?php } ?>
        </form>

      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
