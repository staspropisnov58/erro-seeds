<?php if (isset($addition_id)) { ?>
  <p><?=$addition_name?></p>
  <input type="hidden" value="<?=$addition_id?>" name="addition_product[addition_id]">
  <?php if (isset($addition_product_id)) {?>
    <input type="hidden" value="<?=$addition_product_id?>" name="addition_product[addition_product_id]">
  <?php } ?>
  <a href="<?=$delete?>" id="delete-addition" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="<?=$button_delete?>"><i class="fa fa-trash-o"></i></a>
  <?php foreach ($dependencies as $dependency) { ?>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-<?=$dependency['code']?>"><span data-toggle="tooltip" title="" data-original-title="(Автозаполнение)"><?=$dependency['text']?></span></label>
      <div class="col-sm-10">
        <input type="text" name="dependency-<?=$dependency['code']?>" placeholder="" id="input-<?=$dependency['code']?>" class="form-control" autocomplete="off">
        <ul class="dropdown-menu"></ul>
        <div id="addition-<?=$dependency['code']?>" class="well well-sm" style="height: 150px; overflow: auto;">
          <?php if (isset($$dependency['code'])) {
            foreach ($$dependency['code'] as $dependency_value) { ?>
              <div id="addition-<?=$dependency['code']?>">
                <i class="fa fa-minus-circle"></i><?=$dependency_value['name']?><input type="hidden" name="addition_product[dependencies][<?=$dependency['code']?>][]" value="<?=$dependency_value['id']?>" />
              </div>
            <?php }
          } ?>
        </div>
      </div>
    </div>
  <? } ?>
<? } else {?>
  <div class="col-sm-2">
    <?php foreach ($additions as $addition) {?>
      <label class="radio">
        <input type="radio" name="addition" value="<?=$addition['module_id']?>">
      <?=$addition['name']?></label>
    <?php } ?>
  </div>
<?php } ?>
<script type="text/javascript"><!--//
  $('input[name=addition]').click(function() {
    $('#addition').load('index.php?route=module/addition/getAdditionProduct&token=<?=$token?>&addition_id=' + $(this).val());
  });

  $('input[name=\'dependency-shipping\']').autocomplete({
  	'source': function(request, response) {
  		$.ajax({
  			url: 'index.php?route=extension/shipping/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
  			dataType: 'json',
  			success: function(json) {
  				response($.map(json, function(item) {
  					return {
  						label: item['name'],
  						value: item['code']
  					}
  				}));
  			}
  		});
  	},
  	'select': function(item) {
  		$('input[name=\'dependency-shipping\']').val('');

  		$('#addition-shipping' + item['value']).remove();
  		$('#addition-shipping').append('<div id="addition-shipping' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="addition_product[dependencies][shipping][]" value="' + item['value'] + '" /></div>');
  	}
  });

  $('#addition-shipping').delegate('.fa-minus-circle', 'click', function() {
  	$(this).parent().remove();
  });

  $('input[name=\'dependency-category_strict\']').autocomplete({
  	'source': function(request, response) {
  		$.ajax({
  			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
  			dataType: 'json',
  			success: function(json) {
  				response($.map(json, function(item) {
  					return {
  						label: item['name'],
  						value: item['category_id']
  					}
  				}));
  			}
  		});
  	},
  	'select': function(item) {
  		$('input[name=\'dependency-category_strict\']').val('');

  		$('#addition-category_strict' + item['value']).remove();

  		$('#addition-category_strict').append('<div id="additiont-category_strict' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="addition_product[dependencies][category_strict][]" value="' + item['value'] + '" /></div>');
  	}
  });

  $('#addition-category_strict').delegate('.fa-minus-circle', 'click', function() {
  	$(this).parent().remove();
  });

  $('#delete-addition').click(function(e) {
    e.preventDefault();
    var additionProductId = $('input[name="addition_product[addition_product_id]"]').val();
    if (additionProductId !== undefined) {
      var href = $(this).attr('href');
      $.ajax({
        url: href + '&addition_product_id=' +  additionProductId,
        dataType: 'json',
        success: function(json) {
          $('#addition').load('index.php?route=module/addition/getAdditionProduct&token=<?=$token?>');
        }
      });
    }
    $('#addition').load('index.php?route=module/addition/getAdditionProduct&token=<?=$token?>');
  });
-->
</script>
