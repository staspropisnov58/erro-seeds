<?php
/**
* @version     3.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2017 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/
?>

<?php
$style = '';

$header = str_replace('</head>',$style.'</head>', $header);
?>

<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" onclick="$('#form-ajaxzoom').submit();" form="form-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<!-- AZ 5 -->
				<a href="javascript: void(0)" onclick="$('#form-ajaxzoom-reset').submit();" data-toggle="tooltip" title="Reset" class="btn btn-warning"><i class="fa fa-refresh"></i></a>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>

		<div>
			<a href="<?php echo $url_products; ?>" class="btn btn-primary"><?php echo $text_manage_360_views ?></a>
		</div>
		<br><br>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $text_title_ax_settings ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-ajaxzoom-reset" style="display: none;">
					<input type="hidden" name="reset" value="1">
				</form>
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-ajaxzoom" class="form-horizontal">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php foreach($form_data as $key => $data): ?>
							<div class="panel_ _panel-default">
								<div class="panel-heading" role="tab" id="<?php echo $key ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>" aria-expanded="<?php echo ($data['expanded'] == 1 ? 'true' : 'false') ?>" aria-controls="collapse<?php echo $key ?>">
											<h3><?php echo $data['title'] ?></h3>
										</a>
									</h4>
								</div>
								<div id="collapse<?php echo $key ?>" class="panel-collapse collapse <?php echo ($data['expanded'] == 1 ? 'in' : '') ?>" role="tabpanel" aria-labelledby="<?php echo $key ?>">
									<div class="panel-body">
										<?php if($key == 'license'): ?>

											<table class="table">
												<thead>
													<tr>
														<th style="width: 24%"><?php echo $text_ax_table_domain ?></th>
														<th style="width: 10%"><?php echo $text_ax_table_type ?></th>
														<th style="width: 34%"><?php echo $text_ax_table_key ?></th>
														<th style="width: 14%"><?php echo $text_ax_table_error200 ?></th>
														<th style="width: 14%"><?php echo $text_ax_table_error300 ?></th>
														<th style="width: 5%">&nbsp;</th>
													</tr>
												</thead>
												<tbody id="rowsLicense">
												</tbody>
											</table>

											<table id="templateLicense" style="display:none;">
												<tr>
													<td><input name="licenses[domain][]" value="domain_placeholder" class="lic-domain" style="width: 100%"></td>
													<td>
														<select name="licenses[type][]" class="lic-type" style="min-width:100px;">
															<option value="evaluation">evaluation</option>
															<option value="developer">developer</option>
															<option value="basic">basic</option>
															<option value="standard">standard</option>
															<option value="business">business</option>
															<option value="corporate">corporate</option>
															<option value="enterprise">enterprise</option>
															<option value="unlimited">unlimited</option>
														</select>
													</td>
													<td><input name="licenses[key][]" value="key_placeholder" class="lic-key" style="width: 100%"></td>
													<td><input name="licenses[error200][]" value="error200_placeholder" class="lic-error200" style="width: 100%"></td>
													<td><input name="licenses[error300][]" value="error300_placeholder" class="lic-error300" style="width: 100%"></td>
													<td>
														<a class="btn btn-default btn-sm bt-icon link_remove_license" href="#">
																<i class="fa fa-trash"></i>
														</a>
													</td>
												</tr>
											</table>

												<a class="btn btn-default btn-sm bt-icon link_add_license button" href="#" >
													<i class="fa fa-plus"></i> <?php echo $text_ax_btn_add_license ?>
												</a>

											<div class="">
												<div class="help-block">
													<br>
													<a href="http://www.ajax-zoom.com/index.php?cid=contact" target="_blank"><?php echo $text_ax_ask_supprt ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
													<a href="http://www.ajax-zoom.com/index.php?cid=download#heading_3" target="_blank"><?php echo $text_ax_buy_license ?></a>
												</div>
											</div>

										<?php else: ?>
											<?php echo $data['fields'] ?>
										<?php endif ?>

									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function () {
	// AZ 5
	<?php if (substr(trim($licenses), 0, 1) == '['): ?>
		try {
		   var licenses = $.parseJSON('<?php echo $ajaxzoom_LICENSE ?>');
		}
		catch (e) {
			var licenses = [];
		}
	<?php else: ?>
	var licenses = [];
	<?php endif ?>

	function licenseLine(data)
	{
		var line = $("#templateLicense").html();
		line = line.replace(/domain_placeholder/g, data.domain);
		var reg = new RegExp('value="' + data.type + '"',"g");
		line = line.replace(reg, 'value="' + data.type + '" selected');
		line = line.replace(/key_placeholder/g, data.key);
		line = line.replace(/error200_placeholder/g, data.error200);
		line = line.replace(/error300_placeholder/g, data.error300);
		line = line.replace(/<tbody>/gi, "");
		line = line.replace(/<\/tbody>/gi, "");
		$("#rowsLicense").append(line);
	}

	$('.link_add_license').click(function (e) {
		e.preventDefault();
		var data = {domain: '', type: '', key: '', error200: '', error300: ''};
		licenseLine(data);
	});

	$('body').on('click', '.link_remove_license', function(e) {
		e.preventDefault();
		$(this).parent().parent().remove();
	});

	for(var k in licenses) {
		licenseLine(licenses[k]);
	}
});
</script>

<?php echo $footer; ?>
