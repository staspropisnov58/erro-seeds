<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-pesky-window" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-pesky-window" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="pesky_window_status" id="input-status" class="form-control">
                <?php if ($pesky_window_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-product"><?php echo $entry_product; ?></label>
            <div class="col-sm-10">
              <input type="text" name="product" value="" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
              <div id="pesky-window-product" class="well well-sm" style="height: 500px; overflow: auto;">
                <?php foreach ($products as $product) { ?>
                <div id="pesky-window-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                  <input type="hidden" name="pesky_window_products[]" value="<?php echo $product['product_id']; ?>" />
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-first-timeout"><?php echo $entry_first_timeout; ?></label>
            <div class="col-sm-10">
              <input type="text" name="pesky_window_first_timeout" value="<?php echo $pesky_window_first_timeout; ?>" placeholder="<?php echo $entry_first_timeout; ?>" id="input-first-timeout" class="form-control" />
              <?php if ($error_first_timeout) { ?>
              <div class="text-danger"><?php echo $error_first_timeout; ?></div>
              <?php } ?>
            </div>
          </div>
          <fieldset>
            <legend><?php echo $entry_timeout; ?></legend>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-min-timeout"><?php echo $entry_min_timeout; ?></label>
              <div class="col-sm-10">
                <input type="text" name="pesky_window_min_timeout" value="<?php echo $pesky_window_min_timeout; ?>" placeholder="<?php echo $entry_min_timeout; ?>" id="input-min-timeout" class="form-control" />
                <?php if ($error_min_timeout) { ?>
                <div class="text-danger"><?php echo $error_min_timeout; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-max-timeout"><?php echo $entry_max_timeout; ?></label>
              <div class="col-sm-10">
                <input type="text" name="pesky_window_max_timeout" value="<?php echo $pesky_window_max_timeout; ?>" placeholder="<?php echo $entry_max_timeout; ?>" id="input-max-timeout" class="form-control" />
                <?php if ($error_max_timeout) { ?>
                <div class="text-danger"><?php echo $error_max_timeout; ?></div>
                <?php } ?>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'] + ' | ' + item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
		$('input[name=\'product\']').val('');

		$('#pesky-window-product' + item['value']).remove();

		$('#pesky-window-product').append('<div id="pesky-window-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="pesky_window_products[]" value="' + item['value'] + '" /></div>');
	}
});

$('#pesky-window-product').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script></div>
<?php echo $footer; ?>
