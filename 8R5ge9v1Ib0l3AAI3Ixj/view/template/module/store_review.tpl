<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-store-review" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1>
        <?php echo $heading_title; ?>
      </h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>">
            <?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
      <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>
          <?php echo $text_edit; ?>
        </h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-store-review" class="form-horizontal">
          <?php if ($review_to_transfer) { ?>
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-transfer" data-toggle="tab">
                <?php echo $tab_transfer; ?></a></li>
            <li><a href="#tab-settings" data-toggle="tab">
                <?php echo $tab_settings; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-transfer">
              <p><?php echo $text_transfer; ?></p>
              <div class="alert alert-danger">
                <p><?php echo $text_warning; ?></p>
              </div>
              <div class="row">
                <div class="col-sm-10">
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="<?php echo $total_review_store; ?>" id="progress">
                      <span class="sr-only">40% Complete (success)</span>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <button type="button" id="button-start" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-play"></i> <?php echo $button_transfer; ?></button>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-settings">
              <?php } ?>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status">
                  <?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="store_review_status" id="input-status" class="form-control">
                    <?php if ($store_review_status) { ?>
                    <option value="1" selected="selected">
                      <?php echo $text_enabled; ?>
                    </option>
                    <option value="0">
                      <?php echo $text_disabled; ?>
                    </option>
                    <?php } else { ?>
                    <option value="1">
                      <?php echo $text_enabled; ?>
                    </option>
                    <option value="0" selected="selected">
                      <?php echo $text_disabled; ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <?php foreach ($configuration as $config => $value) { ?>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-<?php echo $config; ?>"><span data-toggle="tooltip" title="<?php echo $help[$config]; ?>">
                    <?php echo $entry[$config]; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="store_review_<?php echo $config; ?>" value="<?php echo $value; ?>" placeholder="<?php echo $entry[$config]; ?>" id="input-<?php echo $config; ?>" class="form-control" />
                  <?php if ($error[$config]) { ?>
                  <div class="text-danger">
                    <?php echo $error[$config]; ?>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <?php } ?>
              <div class="tab-pane">
                <ul class="nav nav-tabs" id="language">
                  <?php foreach ($languages as $language) { ?>
                  <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" />
                      <?php echo $language['name']; ?></a></li>
                  <?php } ?>
                </ul>
                <div class="tab-content">
                  <?php foreach ($languages as $language) { ?>
                  <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                    <?php foreach ($descriptions[$language['language_id']] as $page => $description) { ?>
                    <fieldset>
                      <legend>
                        <?php echo $text_description[$page]; ?>
                      </legend>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-title<?php echo $page; ?><?php echo $language['language_id']; ?>">
                          <?php echo $entry_title; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="store_review_description_<?php echo $page; ?>[<?php echo $language['language_id']; ?>][title]" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $page; ?><?php echo $language['language_id']; ?>"
                            value="<?php echo isset($description['title']) ? $description['title'] : ''; ?>" class="form-control" />
                          <?php if (isset($error_description[$page][$language['language_id']]['title'])) { ?>
                          <div class="text-danger">
                            <?php echo $error_description[$page][$language['language_id']]['title']; ?>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-description<?php echo $page; ?><?php echo $language['language_id']; ?>">
                          <?php echo $entry_description; ?></label>
                        <div class="col-sm-10">
                          <textarea name="store_review_description_<?php echo $page; ?>[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $page; ?><?php echo $language['language_id']; ?>"
                            class="form-control"><?php echo isset($description['title']) ? $description['description'] : ''; ?></textarea>
                          <?php if (isset($error_description[$page][$language['language_id']]['description'])) { ?>
                          <div class="text-danger">
                            <?php echo $error_description[$page][$language['language_id']]['description']; ?>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-meta-title<?php echo $page; ?><?php echo $language['language_id']; ?>">
                          <?php echo $entry_meta_title; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="store_review_description_<?php echo $page; ?>[<?php echo $language['language_id']; ?>][meta_title]" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $page; ?><?php echo $language['language_id']; ?>"
                            value="<?php echo isset($description['meta_title']) ? $description['meta_title'] : ''; ?>" class="form-control" />
                          <?php if (isset($error_description[$page][$language['language_id']]['meta_title'])) { ?>
                          <div class="text-danger">
                            <?php echo $error_description[$page][$language['language_id']]['meta_title']; ?>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-meta-description<?php echo $page; ?><?php echo $language['language_id']; ?>">
                          <?php echo $entry_meta_description; ?></label>
                        <div class="col-sm-10">
                          <textarea name="store_review_description_<?php echo $page; ?>[<?php echo $language['language_id']; ?>][meta_description]" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $page; ?><?php echo $language['language_id']; ?>"
                            class="form-control"><?php echo isset($description['meta_description']) ? $description['meta_description'] : ''; ?></textarea>
                          <?php if (isset($error_description[$page][$language['language_id']]['meta_description'])) { ?>
                          <div class="text-danger">
                            <?php echo $error_description[$page][$language['language_id']]['meta_description']; ?>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </fieldset>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <?php if ($review_to_transfer) { ?>
            </div>
          </div>
          <?php } ?>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    <!--
    $('#language a:first').tab('show');
    //-->
  </script>
<?php if ($review_to_transfer) { ?>
<script>
$('#button-start').on('click', function() {
  $('#button-start').button('loading');
  $('#progress').addClass('active');

  var request = {};
  request.item = 'store';
  request.start = 0;
  transferReviews(request);
});

function transferReviews(request) {
  return $.ajax({
    url: 'index.php?route=module/store_review/transferReviews&token=<?php echo $token;?>&item=' + request.item + '&start=' + request.start,
    type: 'get',
    dataType: 'json',
    beforeSend: function() {

    },
    complete: function() {
    }
  })
  .done(function(nextRequest) {
    console.log(nextRequest);
    if (request.item !== nextRequest.item) {
      $('#progress').css('width', '0');
      $('#progress').attr('aria-valuemax', nextRequest.end);
      $('.progress').after('<p>' + nextRequest.success + '</p>');
    }
    if (nextRequest.start < $('#progress').attr('aria-valuemax')) {
      $('#progress').attr('aria-valuenow', nextRequest.start);
      $('#progress').css('width', (nextRequest.start * 100 / $('#progress').attr('aria-valuemax')) + '%');
      transferReviews(nextRequest);
    }
  })
  .fail(function(xhr, ajaxOptions, thrownError) {
    console.log(thrownError);
  });
}
</script>
<?php } ?>
</div>
<?php echo $footer; ?>
