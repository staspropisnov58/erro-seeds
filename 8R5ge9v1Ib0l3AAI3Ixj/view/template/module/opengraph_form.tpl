<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-account" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
        <?php if(!$page_id){ ?>
      <h1><?php echo $new_page; ?></h1>
    <?php }else{ ?>
     <h1>  <?php echo $page_description['name']; ?></h1>
    <?php } ?>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <?php if(!$page_id){ ?>
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_add_opengraph_page; ?></h3>
      <?php }else{ ?>
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit_page . ' ' . $page_description['name']; ?></h3>
      <?php } ?>

      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-pengraph-form" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-page-name"><?php echo $entry_name; ?> </label>
            <div class="col-sm-10">
              <input type="text" name="page_description[name]" value="<?php echo $page_description['name']; ?>" placeholder="<?php echo $entry_name; ?> " id="input-page-name" class="form-control">
              <?php if (isset($error_page_description['name'])) { ?>
              <div class="text-danger"><?php echo $error_page_description['name']; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-page-route"><?php echo $entry_route; ?> </label>
            <div class="col-sm-10">
              <input type="text" name="page_description[route]" value="<?php echo $page_description['route']; ?>" placeholder="<?php echo $entry_route; ?> " id="input-page-route" class="form-control">
              <?php if (isset($error_page_description['route'])) { ?>
              <div class="text-danger"><?php echo $error_page_description['route']; ?></div>
              <?php } ?>
            </div>
          </div>

          <ul class="nav nav-tabs" id="language">
            <?php foreach ($languages as $language) {
                ?>
            <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
            <?php } ?>
          </ul>

          <div class="tab-content">
            <?php foreach ($languages as $language) { ?>
            <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-page-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?> </label>
                <div class="col-sm-10">
                  <input type="text" name="page_description[page_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo $page_description['page_description'][$language['language_id']]['title']; ?>" placeholder="<?php echo $entry_title; ?> " id="input-page-title<?php echo $language['language_id']; ?>" class="form-control">
                  <?php if (isset($error_page_description[$language['language_id']]['title'])) { ?>
                  <div class="text-danger"><?php echo $error_page_description[$language['language_id']]['title']; ?></div>
                  <?php } ?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-page-description<?php echo $language['language_id'];?>"><?php echo $entry_description; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="page_description[page_description][<?php echo $language['language_id']; ?>][description]" value="<?php echo $page_description['page_description'][$language['language_id']]['description']; ?>" placeholder="<?php echo $entry_description; ?> " id="input-page-description<?php echo $language['language_id']; ?>" class="form-control">
                  <?php if (isset($error_page_description[$language['language_id']]['description'])) { ?>
                  <div class="text-danger"><?php echo $error_page_description[$language['language_id']]['description']; ?></div>
                  <?php } ?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-page-image<?php echo $language['language_id']; ?>"><?php echo $entry_image; ?></label>
                <div class="col-sm-10">
                  <a href="" id="thumb-image<?php echo $language['language_id']?>" data-toggle="image" class="img-thumbnail">
                  <img src="<?php echo $page_description['page_description'][$language['language_id']]['thumb'] ;?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /> </a>
                  <input type="hidden" name="page_description[page_description][<?php echo $language['language_id']; ?>][image]" value="<?php echo $page_description['page_description'][$language['language_id']]['image'];?>" id="input-page-image<?php echo $language['language_id']; ?>" />
                  <?php if (isset($error_page_description[$language['language_id']]['image'])) { ?>
                  <div class="text-danger"><?php echo $error_page_description[$language['language_id']]['image']; ?></div>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php } ?>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>

<script type="text/javascript"><!--
  $('#language a:first').tab('show');

  <?php foreach ($languages as $language) { ?>

  <?php } ?>
</script>
