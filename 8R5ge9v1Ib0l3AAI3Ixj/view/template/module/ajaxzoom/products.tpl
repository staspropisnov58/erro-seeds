<?php
/**
* @version     1.2
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

	$styles = '<style type="text/css"></style>';
	$styles .= '<link type="text/css" media="all" rel="stylesheet" href="' . $base_url . 'resources/css/jquery.tablesorter.pager.css" />';
	$styles .= '<link type="text/css" media="all" rel="stylesheet" href="' . $base_url . 'resources/css/theme.bootstrap.css" />';

	$scripts = '';
	$scripts .= '<script src="' . $base_url . 'resources/js/jquery.tablesorter.js"></script>';
	$scripts .= '<script src="' . $base_url . 'resources/js/jquery.tablesorter.pager.js"></script>';
	$scripts .= '<script src="' . $base_url . 'resources/js/jquery.tablesorter.widgets.js"></script>';

	$header = str_replace('</head>',$styles . $scripts .'</head>', $header);
?>
<?php echo $header; ?>

<?php echo $column_left; ?>

<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $url_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="table-responsive">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th class="text-center" data-sorter="false" data-filter="false"><?php echo $text_ax_table_image  ?></th>
						<th class="text-left"><?php echo $text_ax_table_name ?></th>
						<th class="text-left"><?php echo $text_ax_table_model ?></th>
						<th class="text-center" data-filter="false"><?php echo $text_ax_table_360views ?></th>
						<th class="text-right" data-sorter="false" data-filter="false"><?php echo $text_ax_table_action ?></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="text-center"><?php echo $text_ax_table_image  ?></th>
						<th class="text-left"><?php echo $text_ax_table_name ?></th>
						<th class="text-left"><?php echo $text_ax_table_model ?></th>
						<th class="text-center"><?php echo $text_ax_table_360views ?></th>
						<th class="text-right" data-sorter="false"><?php echo $text_ax_table_action ?></th>
					</tr>
					<tr>
						<th colspan="5" class="ts-pager form-horizontal">
							<button type="button" class="btn first"><i class="icon-step-backward fa fa-step-backward"></i></button>
							<button type="button" class="btn prev"><i class="icon-arrow-left fa fa-backward"></i></button>
							<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
							<button type="button" class="btn next"><i class="icon-arrow-right fa fa-forward"></i></button>
							<button type="button" class="btn last"><i class="icon-step-forward fa fa-step-forward"></i></button>
							<select class="pagesize input-mini" title="Select page size">
								<option selected="selected" value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
								<option value="50">50</option>
								<option value="100">100</option>
								<option value="200">200</option>
								<option value="500">500</option>
								<option value="99999999"><?php echo $text_ax_all; ?></option>
							</select>
							<select class="pagenum input-mini" title="Select page number"></select>
						</th>
					</tr>
				</tfoot>
				<tbody>
					<?php if ($products) { ?>
						<?php foreach ($products as $product) { ?>
							<tr>
								<td class="text-center">
									<?php if ($product['image']) { ?>
										<img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" />
									<?php } else { ?>
										<span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
									<?php } ?>
								</td>
								<td class="text-left"><?php echo $product['name']; ?></td>
								<td class="text-left"><?php echo $product['model']; ?></td>
								<td class="text-center" style="font-size: 0"><?php echo $product['num360views'] >= 1 ? $product['num360views'] : 0; ?><span class="label label-success" style="font-size: 10px"><?php echo $product['num360views']; ?></span></td>
								<td class="text-right"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $text_ax_btn_help_manage360 ?>" class="btn btn-primary"><i class="fa fa-photo" style="margin-right: 5px;"></i>Edit</a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
							<tr>
								<td class="text-center" colspan="8"><?php echo $text_ax_no_products ?></td>
							</tr>
					<?php } ?>
				</tbody>
	  		</table>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(function($) {
	// NOTE: $.tablesorter.theme.bootstrap is ALREADY INCLUDED in the jquery.tablesorter.widgets.js
	// file; it is included here to show how you can modify the default classes
	$.tablesorter.themes.bootstrap = {
		// these classes are added to the table. To see other table classes available,
		// look here: http://getbootstrap.com/css/#tables
		table        : 'table table-bordered table-striped',
		caption      : 'caption',
		// header class names
		header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
		sortNone     : '',
		sortAsc      : '',
		sortDesc     : '',
		active       : '', // applied when column is sorted
		hover        : '', // custom css required - a defined bootstrap style may not override other classes
		// icon class names
		icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
		iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
		iconSortAsc  : 'fa fa-chevron-up', // class name added to icon when column has ascending sort
		iconSortDesc : 'fa fa-chevron-down', // class name added to icon when column has descending sort
		filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
		footerRow    : '',
		footerCells  : '',
		even         : '', // even row zebra striping
		odd          : ''  // odd row zebra striping
	};

	// call the tablesorter plugin and apply the uitheme widget
	$("table").tablesorter({
		// this will apply the bootstrap theme if "uitheme" widget is included
		// the widgetOptions.uitheme is no longer required to be set
		theme : "bootstrap",
		widthFixed: true,
		headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

		// widget code contained in the jquery.tablesorter.widgets.js file
		// use the zebra stripe widget if you plan on hiding any rows (filter widget)
		widgets : [ "uitheme", "filter", "zebra", "saveSort"],

		widgetOptions : {
			// using the default zebra striping class name, so it actually isn't included in the theme variable above
			// this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
			zebra : ["even", "odd"],

			// reset filters button
			filter_reset : ".reset",

			// extra css class name (string or array) added to the filter element (input or select)
			filter_cssFilter: "form-control",

			// Delay in milliseconds before the filter widget starts searching; This option prevents searching for
			// every character while typing and should make searching large tables faster.
			filter_searchDelay : 300,

			// Set this option to true to use the filter to find text from the start of the column
			// So typing in "a" will find "albert" but not "frank", both have a's; default is false
			filter_startsWith  : false,

			// if false, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
			// below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
			filter_hideFilters : false
			// set the uitheme widget to use the bootstrap theme class names
			// this is no longer required, if theme is set
			// ,uitheme : "bootstrap"

		}
	})
	.tablesorterPager({
		// target the pager markup - see the HTML block below
		container: $(".ts-pager"),

		// target the pager page select dropdown - choose a page
		cssGoto  : ".pagenum",

		// remove rows from the table to speed up the sort of large tables.
		// setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
		removeRows: false,

		// output string - default is '{page}/{totalPages}';
		// possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
		output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
	});

});
</script>

<?php echo $footer; ?>