<?php
/**
* @version     1.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/
?>

<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $text_ax_header_settings ?></h3>
	</div>
	<div class="panel-body">

		<div class="">
			
			<div class="form-group">
				<label class="control-label col-lg-3">
					<?php echo $text_ax_enable_axzoom ?>
				</label>
				<div class="bootstrap col-lg-9 ">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="az_active" id="az_active_on" value="1"<?php if ($active == 1) echo ' checked="checked"' ?>/>
						<label class="t" for="az_active_on"><?php echo $text_ax_enable ?></label>
						<input type="radio" name="az_active" id="az_active_off" value="0"<?php if ($active == 0) echo ' checked="checked"' ?>/>
						<label class="t" for="az_active_off"><?php echo $text_ax_disable ?></label>
						<a class="slide-button btn"></a>
					</span>
					<p class="help-block"></p>
				</div>
			</div>
		</div>
		
		<div id="product-images360sets">

			<div class="form-group">
				<label class="control-label col-lg-3">
					<?php echo $text_ax_settings_existing ?>
				</label>
				<div class="bootstrap col-lg-9 ">
					<select id="id_360" name="id_360" style="min-width: 100px">
						<option value=""><?php echo $text_ax_select_3d_view ?></option>
						<?php foreach($sets_groups as $group): ?>
						<option value="<?php echo $group['id_360'] ?>" data-settings="<?php echo urlencode($group['settings']) ?>" data-combinations="[<?php echo urlencode($group['combinations']) ?>]"><?php echo $group['name'] ?></option>
						<?php endforeach ?>
					</select>
					<p class="help-block"></p>
				</div>
			</div>

			<div class="form-group" id="pairs" style="display:none;">
				<label class="control-label col-md-3">
					<?php echo $text_ax_title_settings ?>
				</label>
				<div class="bootstrap col-md-4 ">
					<table>
						<thead>
							<tr>
								<th><?php echo $text_ax_table_name ?></th>
								<th></th>
								<th style="width: 220px"><?php echo $text_ax_table_value ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody id="pairRows">
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									<div class="row_">
										<a class="btn btn-default btn-sm bt-icon link_add_option button" href="#">
											<i class="icon-plus"></i> <?php echo $text_ax_add_option ?>
										</a>
									</div>
								</td>
							</tr>
						</tfoot>	
					</table>
					<table id="pairTemplate" style="display: none">
						<tr>
							<td><input type="text" name="name[]" value="name_placeholder" class="pair-names"></td>
							<td> : </td>
							<td><input type="text" name="value[]" value="value_placeholder" class="pair-values"></td>
							<td>
								<a class="btn btn-default btn-xs bt-icon link_textarea_option" href="#" title="<?php echo $text_ax_edit_textarea ?>">
									<span class="fa fa-pencil" aria-hidden="true"></span>
								</a>
							</td>
							<td>	
								<a class="btn btn-default btn-xs bt-icon link_remove_option" href="#">
									 <span class="fa fa-trash" aria-hidden="true"></span>
								</a>
							</td>
						</tr>
					</table>
				</div>
				<div class="bootstrap col-md-5 ">
					<div class="alert alert-info" style="display: block">
						<?php echo $text_ax_options_info ?>
						<a href="http://www.ajax-zoom.com/index.php?cid=docs#VR_Object" target="_blank"><?php echo $text_ax_doc ?> <i class="icon-external-link-sign"></i></a> 
					</div>
				</div>
			</div>

			<?php if (!empty($variants) && count($variants) > 1): ?>
			<div class="form-group" id="comb" style="display:none; clear:both;">
				<label class="control-label col-md-3">
					<?php echo $text_ax_variants ?>
				</label>
				<div class="bootstrap col-md-4 ">
					<a class="comb-check-all" style="margin-bottom: 10px;" href="#"  /><?php echo $text_ax_check_all ?></a><br>

					<?php foreach($variants as $data): ?>
					<input type="checkbox" name="combinations[]" value="<?php echo $data['id'] ?>" class="settings-combinations"><?php echo $data['name'] ?><br>
					<?php endforeach ?>
				</div>
				<div class="bootstrap col-md-5 ">
					<div class="alert alert-info" style="display: block; margin-top: 10px;">
						<?php echo $text_ax_variants_info ?>
					</div>
				</div>
			</div>
			<?php endif ?>
		</div>
	</div>


	<div class="panel-footer">
		<div class="row">
			<div class="col-md-3">

			</div>
			<div class="col-md-9">
				<button type="submit" name="submitSettings" id="submitSettings" class="btn btn-primary btn-sm button" ><i class="process-icon-save"></i> <?php echo $text_ax_btn_save ?></button>
			</div>
		</div>
		
	</div>

</div>