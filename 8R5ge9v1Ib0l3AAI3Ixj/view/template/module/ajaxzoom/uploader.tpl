<?php
/**
* @version     1.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/
?>

<div class="file-upload-wrapper">
	<div id="file-uploader-360">
		<noscript>
			<p>Please enable JavaScript to use file uploader:</p>
		</noscript>
	</div>
	<div id="progressBarImage" class="progressBarImage"></div>
	<div id="showCounter" style="display:none;"><span id="imageUpload">0</span><span id="imageTotal">0</span></div>
		<p class="preference_description" style="clear: both;">
			<?php echo $text_ax_filesize1 ?> <?php echo sprintf("%.2f", $max_image_size) ?> <?php echo $text_ax_filesize2 ?>
		</p>
</div>