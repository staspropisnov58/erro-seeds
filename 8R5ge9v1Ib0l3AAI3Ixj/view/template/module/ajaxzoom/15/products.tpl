<?php
/**
* @version     1.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

	$styles = '';
	$styles .= '<link type="text/css" media="all" rel="stylesheet" href="' . $base_url . 'resources/css/jquery.tablesorter.pager.css" />';
	$styles .= '<link type="text/css" media="all" rel="stylesheet" href="' . $base_url . 'resources/css/theme.bootstrap.css" />';
	
	if(!$is2) {
		$styles .= '<style type="text/css">
			.fa-forward:before {
			    content: ">";
			}
			.fa-step-forward:before {
			    content: ">|";
			}
			.fa-backward:before {
			    content: "<";
			}
			.fa-step-backward:before {
			    content: "|<";
			}
			.fa {
			    display: inline-block;
			    font: normal normal normal 14px/1 FontAwesome;
			    font-size: inherit;
			    text-rendering: auto;
			    -webkit-font-smoothing: antialiased;
			    -moz-osx-font-smoothing: grayscale;
			}
		</style>';
	}

	$scripts = '';
	$scripts .= '<script src="' . $base_url . 'resources/js/jquery.tablesorter.js"></script>';
	$scripts .= '<script src="' . $base_url . 'resources/js/jquery.tablesorter.pager.js"></script>';
	$scripts .= '<script src="' . $base_url . 'resources/js/jquery.tablesorter.widgets.js"></script>';

	$header = str_replace('</head>',$styles . $scripts .'</head>', $header);
?>
<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">

            </div>
        </div>
        <div class="content">

			<table class="table table-bordered table-hover list">
	  			<thead>
					<tr>
						<td class="text-center" data-sorter="false" data-filter="false"><?php echo $text_ax_table_image  ?></td>
						<td class="text-left"><?php echo $text_ax_table_name ?></td>
						<td class="text-left"><?php echo $text_ax_table_model ?></td>
						<td class="text-center"><?php echo $text_ax_table_360views ?></td>
						<td class="text-right" data-sorter="false" data-filter="false"><?php echo $text_ax_table_action ?></td>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="center"><?php echo $text_ax_table_image  ?></th>
						<th class="left"><?php echo $text_ax_table_name ?></th>
						<th class="left"><?php echo $text_ax_table_model ?></th>
						<th class="center"><?php echo $text_ax_table_360views ?></th>
						<th class="right" data-sorter="false"><?php echo $text_ax_table_action ?></th>
					</tr>
					<tr>
						<th colspan="5" class="ts-pager form-horizontal">
							<button type="button" class="btn first"><i class="icon-step-backward fa fa-step-backward"></i></button>
							<button type="button" class="btn prev"><i class="icon-arrow-left fa fa-backward"></i></button>
							<span class="pagedisplay"></span> <!-- this can be any element, including an input -->
							<button type="button" class="btn next"><i class="icon-arrow-right fa fa-forward"></i></button>
							<button type="button" class="btn last"><i class="icon-step-forward fa fa-step-forward"></i></button>
							<select class="pagesize input-mini" title="Select page size">
								<option selected="selected" value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
							</select>
							<select class="pagenum input-mini" title="Select page number"></select>
						</th>
					</tr>
				</tfoot>	  
				<tbody>
					<?php if ($products) { ?>
						<?php foreach ($products as $product) { ?>
							<tr>
								<td class="center">
									<?php if ($product['image']) { ?>
										<img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" />
									<?php } else { ?>
										<span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
									<?php } ?>
								</td>
								<td class="left"><?php echo $product['name']; ?></td>
								<td class="left"><?php echo $product['model']; ?></td>
								<td class="center"><?php echo $product['num360views']; ?></td>
								<td class="right"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $text_ax_btn_help_manage360 ?>" class="btn btn-primary"><?php echo $text_360views ?></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
							<tr>
								<td class="text-center" colspan="8"><?php echo $text_ax_no_products ?></td>
							</tr>
					<?php } ?>
				</tbody>
	  		</table>
		</div>
	</div>
</div>

<script>
$(function() {

	// NOTE: $.tablesorter.theme.bootstrap is ALREADY INCLUDED in the jquery.tablesorter.widgets.js
	// file; it is included here to show how you can modify the default classes
	$.tablesorter.themes.bootstrap = {
		// these classes are added to the table. To see other table classes available,
		// look here: http://getbootstrap.com/css/#tables
		table        : 'table table-bordered table-striped',
		caption      : 'caption',
		// header class names
		header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
		sortNone     : '',
		sortAsc      : '',
		sortDesc     : '',
		active       : '', // applied when column is sorted
		hover        : '', // custom css required - a defined bootstrap style may not override other classes
		// icon class names
		icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
		iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
		iconSortAsc  : 'fa fa-chevron-up', // class name added to icon when column has ascending sort
		iconSortDesc : 'fa fa-chevron-down', // class name added to icon when column has descending sort
		filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
		footerRow    : '',
		footerCells  : '',
		even         : '', // even row zebra striping
		odd          : ''  // odd row zebra striping
	};

  // call the tablesorter plugin and apply the uitheme widget
  $("table").tablesorter({
    // this will apply the bootstrap theme if "uitheme" widget is included
    // the widgetOptions.uitheme is no longer required to be set
    theme : "bootstrap",

    widthFixed: true,

    headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

    // widget code contained in the jquery.tablesorter.widgets.js file
    // use the zebra stripe widget if you plan on hiding any rows (filter widget)
    widgets : [ "uitheme", "filter", "zebra" ],

    widgetOptions : {
      // using the default zebra striping class name, so it actually isn't included in the theme variable above
      // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
      zebra : ["even", "odd"],

      // reset filters button
      filter_reset : ".reset",

      // extra css class name (string or array) added to the filter element (input or select)
      filter_cssFilter: "form-control"

      // set the uitheme widget to use the bootstrap theme class names
      // this is no longer required, if theme is set
      // ,uitheme : "bootstrap"

    }
  })
  .tablesorterPager({

    // target the pager markup - see the HTML block below
    container: $(".ts-pager"),

    // target the pager page select dropdown - choose a page
    cssGoto  : ".pagenum",

    // remove rows from the table to speed up the sort of large tables.
    // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
    removeRows: false,

    // output string - default is '{page}/{totalPages}';
    // possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
    output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

  });

});
</script>

<?php echo $footer; ?>