<?php
/**
* @version     1.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/
?>
<?php if (isset($id_product) && isset($product)): ?>
<input type="hidden" name="id_360set" id="id_360set" value="" />
<div id="product-images360" class="panel product-tab" style="display:none">
	<input type="hidden" name="submitted_tabs[]" value="Images360" />
	<h4>
	<div class="panel-heading tab" >
		<?php echo $text_ax_title_images ?>
	</div>
	</h4>
	<div class="row">
		<div class="form-group">
			<label class="control-label col-lg-3 file_upload_label">
				<span class="label-tooltip" data-toggle="tooltip" title="<?php echo $text_ax_filesize1 ?> <?php echo sprintf("%.2f", $max_image_size) ?> <?php echo $text_ax_filesize2 ?>">
					<?php if (isset($id_image)): ?>
						<?php echo $text_ax_title_edit_image ?>
					<?php else: ?>
						<?php echo $text_ax_title_add_image ?>
					<?php endif ?>
				</span>
			</label>
			<div class="col-lg-9">
				<?php include 'uploader.tpl' ?>
			</div>
		</div>
	</div>
	<table class="table tableDnD list" id="imageTable360">
		<thead>
			<tr class="nodrag nodrop">
				<td class="fixed-width-lg"><span class="title_box"><?php echo $text_ax_table_image ?></span></td>
				<td></td> <!-- action -->
			</tr>
		</thead>
		<tbody id="imageList360">
		</tbody>
	</table>
	<table id="lineType360" style="display:none;">
		<tr id="image_id">
			<td>
				<img src="<?php echo $base_url ?>resources/img/image_path.gif" alt="legend" title="legend" class="img-thumbnail" />
			</td>
			<td>
				<a href="#" class="delete_product_image360 pull-right btn btn-xs btn-default button" >
					<span class="fa fa-trash"></span> <?php echo $text_ax_btn_delete_image ?>
				</a>
			</td>
		</tr>
	</table>
	<div class="panel-footer">
		<a href="" class="btn btn-default btn-sm button btn-cancel"><i class="process-icon-cancel"></i> <?php echo $text_ax_btn_cancel ?></a>
	</div>

</div>
<?php endif ?>