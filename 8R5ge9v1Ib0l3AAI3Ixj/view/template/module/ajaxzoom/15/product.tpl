<?php
/**
* @version     2.1
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

	$styles = '';
	$styles .= '<link type="text/css" media="all" rel="stylesheet" href="' . $base_url . 'axZm/plugins/demo/jquery.fancybox/jquery.fancybox-1.3.4.css" />';
	$styles .= '<link type="text/css" media="all" rel="stylesheet" href="' . $base_url . 'resources/css/main.css" />';
	if(!$is2) {
		$styles .= '<link type="text/css" media="all" rel="stylesheet" href="' . $base_url . 'resources/css/main15.css" />';
	}

	$scripts = '';
	$scripts .= '<script src="' . $base_url . 'axZm/plugins/demo/jquery.fancybox/jquery.fancybox-1.3.4.js"></script>';
	$scripts .= '<script src="' . $base_url . 'axZm/extensions/jquery.axZm.openAjaxZoomInFancyBox.js"></script>';
	$scripts .= '<script src="' . $base_url . 'resources/js/fileuploader.js"></script>';

	$header = str_replace('</head>',$styles . $scripts .'</head>', $header);
?>

<?php echo $header; ?>


<div id="content" class="ajaxzoom-content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
            	<a href="<?php echo $url_back; ?>" class="button btn btn-default"><?php echo $text_button_cancel; ?></a>
            </div>
        </div>
        <div class="content">
			<div style="position:relative;">
				<div id="message" class="alert alert-success" role="alert"></div>
			</div>
			<h3><?php echo $product['name'] ?></h3>

			<?php include "settings.tpl"; ?>

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $text_ax_title_3d_views ?></h3>
				</div>
				<div class="panel-body">
					<?php include "sets.tpl"; ?>
					<?php include "images.tpl"; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
if ($.isFunction($.fn.tooltip)){
    $('.label-tooltip', $('#newForm')).tooltip();
}

var base_url = '<?php echo $base_url ?>';
var base_uri = '<?php echo $base_uri ?>';
var id_product = '<?php echo $id_product ?>';
var iframe = '<?php echo $iframe ?>';


function showSuccessMessage(message) {
	if(typeof(message) == 'object') {
		message = message.join('<br>');	
	}
	
	$('#message').html(message).show();
	
	setTimeout(function() {
		$('#message').fadeOut(3000, function() {
			$(this).hide();
		});
	}, 3000);

	setFrameHeight();
}

function imageLine360(id, path, position, cover, shops, legend)
{
	line = $("#lineType360").html();
	line = line.replace(/image_id/g, id);
	line = line.replace('<?php echo $base_url ?>resources/img/image_path.gif', path);
	line = line.replace(/icon-check-empty/g, cover);
	line = line.replace(/<tbody>/gi, "");
	line = line.replace(/<\/tbody>/gi, "");

	$("#imageList360").append(line);
}

function setFrameHeight() {
	var iFrameID = parent.document.getElementById('iframe_ajaxzoom');

	if(iFrameID) {
		iFrameID.height = "";
		iFrameID.height = $('.ajaxzoom-content').height() + "px";
	}
}

jQuery(function ($) {
	
	if(iframe == '1') {

		$('#header').remove();
		$('#footer').remove();

		$('.breadcrumb').remove();
		$('.heading').remove();

		setTimeout(setFrameHeight, 500);


	}

    function setLine(id, path, position, legend, status, group_id)
    {
        line = $("#lineSet").html();
        line = line.replace(/set_id/g, id);
        line = line.replace(/group_id/g, group_id);
        line = line.replace(/legend/g, legend);
        line = line.replace(/status_field/g, 'status_' + id);
        line = line.replace('<?php echo $base_url ?>resources/img/image_path.gif', path);
        line = line.replace(/<tbody>/gi, "");
        line = line.replace(/<\/tbody>/gi, "");

        if(status == '1') {
            line = line.replace(/checked_on/g, 'checked');
            line = line.replace(/checked_off/g, '');
        } else {
            line = line.replace(/checked_on/g, '');
            line = line.replace(/checked_off/g, 'checked');
        }

        if($('tr[data-group=' + group_id + ']').length) {
            line = line.replace(/hide_class/g, 'hide');
        }

        $("#imageTableSetsRows").append(line);
    }

	function afterDeleteProductImage360(data)
	{
		data = $.parseJSON(data);
		if (data)
		{
			id = data.content.id;
			if (data.status == 'ok')
			{
				$("#" + id).remove();
			}

			$("#countImage360").html(parseInt($("#countImage360").html()) - 1);
			showSuccessMessage(data.confirmations);
		}
	}

    function afterUpdateStatus(data) { 
        data = $.parseJSON(data);
        showSuccessMessage(data.confirmations);
    }

    function afterDeleteSet(data) { 
        var data = $.parseJSON(data);
        $('tr#' + data.id_360set).remove();
        showSuccessMessage(data.confirmations);

        // remove set option from the dropdowns
        if(data.removed == '1') {
            $("select#id_360 option[value='" + data.id_360 + "']").remove();
            $("select#existing option[value='" + data.id_360 + "']").remove();
        }
    }

    function afterAddSet(data) { 
        var data = $.parseJSON(data);

        if(data.sets.length > 0) {
            for (var i = 0; i < data.sets.length; i++) {
                var set = data.sets[i];
                setLine(set.id_360set, set.path, "", set.name, set.status, set.id_360);
            };
        } else {
            setLine(data.id_360set, data.path, "", data.name, data.status, data.id_360);
        }

        $('.link_add').find('i').removeClass('icon-minus').addClass('icon-plus');
        $('#newForm').hide();
        $('#name').val('');
        $('#existing').val('');

        if(data.new_id != '') {
            $('select#id_360')
                .append($("<option></option>")
                .attr("value", data.new_id)
                .attr('data-settings', data.new_settings)
                .attr('data-combinations', '[]')
                .text(data.new_name)); 
            $('select#existing').append($("<option></option>").attr("value", data.new_id).text(data.new_name)); 
        }
        showSuccessMessage(data.confirmations);
    }

    function afterGetImages(data) { 
        var data = $.parseJSON(data);
        
        for (var i = 0; i < data.images.length; i++) {
            imageLine360(data.images[i]['id'], data.images[i]['thumb'], '', "", "", "");
        };
    }

	function doAdminAjax360(data, success_func, error_func) { 
		$.ajax( { 
			url : '<?php echo $url_ajax ?>',
			data : data,
			type : 'GET',
			success : function(data){
				if (success_func)
					return success_func(data);

				data = $.parseJSON(data);
				if (data.confirmations.length != 0)
					showSuccessMessage(data.confirmations);
				else
					showErrorMessage(data.error);
			},
			error : function(data){
				if (error_func)
					return error_func(data);

				alert("[TECHNICAL ERROR]");
			}
		});
	}

	function pairLine(name, value) {  
		var line = $("#pairTemplate").html();
		line = line.replace(/name_placeholder/g, name);
		line = line.replace(/value_placeholder/g, value);
		line = line.replace(/<tbody>/gi, "");
		line = line.replace(/<\/tbody>/gi, "");
		$("#pairRows").append(line);
	}	

	function afterSaveSettings(data) { 
		var data = $.parseJSON(data);

		$('#id_360').replaceWith(data.select);
		$('#pairs').hide();
		$('#comb').hide();
		$('select#id_360').val('');
		showSuccessMessage(data.confirmations);
		$('#submitSettings').removeClass('save-require');
	} 	

    $('body').on('click', '.delete_set', function(e) {
        e.preventDefault();
        
        $('#product-images360').hide();
        $('#imageList360').html('');

        var id = $(this).parent().parent().attr('id');
        if (confirm("Are you sure?"))
        doAdminAjax360( { 
                "action": "deleteSet",
                "id_360set": id,
                "id_product": id_product,
                }, afterDeleteSet
        );
    });

    $('#zip').change(function () {
        if($(this).is(':checked')) {
            $('.field-arcfile').show();
        } else {
            $('.field-arcfile').hide();
        }
    });

    $('.link_add').click(function (e) {
        e.preventDefault();

        var icon = $(this).find('i');

        if(icon.hasClass('icon-plus')) {
            icon.removeClass('icon-plus').addClass('icon-minus');
            $('#newForm').show();
        } else {
            icon.removeClass('icon-minus').addClass('icon-plus');
            $('#newForm').hide();
        }
    });

    $('body').on('change', '.switch-status input', function(e) { 
        e.preventDefault();
        var status = $(this).val();
        var group_id = $(this).parent().parent().parent().data('group');

        doAdminAjax360({
                "action":"set360Status",
                "id_product" : id_product,
                "id_360" : group_id,
                "status" : status,
                }, afterUpdateStatus
        );
    });

    $('body').on('click', '.preview_set', function(e) {
        e.preventDefault();
        var id360 = $(this).parent().parent().data('group');
        var id360set = $(this).parent().parent().attr('id');
		var qty = $('tr[data-group=' + id360 + ']').length;
        var _3dDir = base_uri + 'pic/360/' + id_product + '/' + id360;
		if (qty < 2){
        	_3dDir += '/' + id360set;
		}
        $.openAjaxZoomInFancyBox({href: base_url + 'preview/preview.php?3dDir=' + _3dDir+'&group='+id360+'&id='+id360set, iframe: true});
    });

    $('body').on('click', '.crop_set', function(e) {
        e.preventDefault();
        var id360 = $(this).parent().parent().data('group');
        var id360set = $(this).parent().parent().attr('id');
		var qty = $('tr[data-group=' + id360 + ']').length;
        var href_crop = base_url + 'preview/cropeditor.php?';
        href_crop += '3dDir=' + base_uri + 'pic/360/' + id_product + '/' + id360;
		if (qty < 2){
        	href_crop += '/' + id360set;
		}
        href_crop += '&id_360='+id360+'&id_360set='+id360set+'&id_product='+id_product;
        href_crop += '&token=<?php echo $token ?>';
		href_crop += '&url_ajax=<?php echo $url_ajax_c ?>';
    	$.openAjaxZoomInFancyBox({href: href_crop, iframe: true, scrolling: true});
    });

    $('body').on('click', '.hotspot_set', function(e) {
        e.preventDefault();
        var id360 = $(this).parent().parent().data('group');
        var id360set = $(this).parent().parent().attr('id');
		var qty = $('tr[data-group=' + id360 + ']').length;
        var href_crop = base_url + 'preview/hotspoteditor.php?';
        href_crop += '3dDir=' + base_uri + 'pic/360/' + id_product + '/' + id360;
		if (qty < 2){
        	href_crop += '/' + id360set;
		}
        href_crop += '&id_360='+id360+'&id_360set='+id360set+'&id_product='+id_product;
        href_crop += '&token=<?php echo $token ?>';
		href_crop += '&url_ajax=<?php echo $url_ajax_c ?>';
    	$.openAjaxZoomInFancyBox({href: href_crop, iframe: true, scrolling: true});
    });

    $('body').on('click', '.images_set', function(e) { 
        e.preventDefault();
        
        $('#imageTableSetsRows').find('tr').removeClass('active');
        $(this).parent().parent().addClass('active');
        $('#imageList360').html('');
        $('#file360-success').parent().hide();

        var id = $(this).parent().parent().attr('id');

        doAdminAjax360({
                "action":"getImages",
                "id_product" : id_product,
                "id_360set" : id,
                }, afterGetImages
        );
        
        $('#id_360set').val(id);
        $('#product-images360').show();
    });


    $('.save_set').click(function (e) { 
        e.preventDefault();    
        
        doAdminAjax360({
                "action":"addSet",
                "name":$('#name').val(),
                "existing":$('#existing').val(),
                "zip":$('#zip').is(':checked'),
                "delete":$('#delete').is(':checked'),
                "arcfile":$('#arcfile').val(),
                "id_product" : id_product,
                }, afterAddSet
        );
    });


	$('body').on('click', '.delete_product_image360', function(e) { 
		e.preventDefault();
		id = $(this).parent().parent().attr('id');
		var id_360set = $('#id_360set').val();
		var ext = $(this).parent().parent().find('img').attr('src').split('.').pop();
		if (confirm("<?php echo $text_ax_you_sure ?>"))
		doAdminAjax360({
				"action":"deleteProductImage360",
				"id_image": id,
				'id_360set': id_360set,
				"ext": ext,
				"id_product" : id_product
				}, afterDeleteProductImage360
		);
	});
    
    <?php if(!empty($sets)) foreach($sets as $set): ?>
        setLine("<?php echo $set['id_360set'] ?>", "<?php echo $set['path'] ?>", "", "<?php echo $set['name'] ?>", "<?php echo $set['status'] ?>", "<?php echo $set['id_360'] ?>");
    <?php endforeach ?>

	$('body').on('change', '.pair-names, .pair-values', function(e) {
		$('#submitSettings').addClass('save-require');
	} );

	$('#submitSettings').click(function (e) { 
		e.preventDefault();
		
		var active = $('input[name=az_active]:checked').val();

		var inputs = document.getElementsByClassName( 'pair-names' ), 
			names  = [].map.call(inputs, function( input ) { 
        		return input.value;
    		}).join( '|' );

		var inputs = document.getElementsByClassName( 'pair-values' ), 
			values  = [].map.call(inputs, function( input ) {
        		return input.value;
    		} ).join( '|' );

		var tmp = [];
		$('.settings-combinations').each(function() {
		    if ($(this).is(':checked')) {
		        tmp.push($(this).val());
		    }
		} );
		
		var combinations = tmp.join( '|' );

		var id_360 = $('select#id_360').val();

		doAdminAjax360( { 
				"action":"saveSettings",
				"id_product" : id_product,
				"id_360" : id_360,
				"names" : names,
				"combinations" : combinations,
				"values" : values,
				"active" : active
			}, afterSaveSettings
		);
	});

	$('.link_add_option').click(function (e) {
		e.preventDefault();
		pairLine('', '');
	} );

	$('body').on('click', '.link_remove_option', function(e) {
		e.preventDefault();
		$(this).parent().parent().remove();
		$('#submitSettings').addClass('save-require');
	} );
	
	$('body').on('click', '.link_textarea_option', function(e) {
		e.preventDefault();
		var td = $(this).parent().prev();
		if ($('input', td).length == 1) { 
			var Val = $('input', td).val();
			$('input', td).replaceWith('<textarea class="pair-values" type="text" name="value[]">'+Val+'</textarea>');
		} else if ($('textarea', td).length == 1) { 
			var Val = $('textarea', td).val();
			$('textarea', td).replaceWith('<input class="pair-values" type="text" value="'+Val+'" name="value[]">');
		}
	} );
	
	$('body').on('click', '.btn-cancel', function (e) {
		e.preventDefault();
		$('#product-images360').hide();
	});

	$('body').on('change', 'select#id_360', function(e) {
		$('#pairRows').html('');

		if($(this).val() != '') { 

			// set pairs name:value
			var settings = $.parseJSON(unescape($('option:selected', $(this)).attr('data-settings')));
			for(var k in settings) { 
				pairLine(k, settings[k])
			} 

			// set combinations checkboxes
			var combinations = $.parseJSON(unescape($('option:selected', $(this)).attr('data-combinations')));
			
			$('input.settings-combinations').prop('checked', false);
			for(var k in combinations) { 
				$('input.settings-combinations[value=' + combinations[k] + ']').prop('checked', true);
			} 

			$('#pairs').show();
			$('#comb').show();
		} else {
			$('#pairs').hide();
			$('#comb').hide();
		}
	});

	
    $('a.comb-check-all').click(function(e) { 

    	e.preventDefault();

    	if($(this).html() == 'check all') {
        	$('input.settings-combinations').prop('checked', true);
        	$(this).html('uncheck all');
    	} else {
        	$('input.settings-combinations').prop('checked', false);
        	$(this).html('check all');        
        }
    });

    $('.fancybox').fancybox();
});

</script>

<script type="text/javascript">
	var filecheck = 1;
	var upbutton = '<?php echo $text_ax_btn_upload_image ?>';
	var come_from = '';
	var success_add =  '<?php echo $text_ax_image_added ?>';
	var id_tmp = 0;
	
	var uploader = new qq.FileUploader(
	{
		element: document.getElementById("file-uploader-360"),
		action: '<?php echo $url_ajax ?>',
		debug: false,
		params: {
			id_product : id_product,
			id_360set : $('#id_360set').val(),
			action : 'addProductImage360',
		},
		onComplete: function(id, fileName, responseJSON)
		{
			// set image
			$tr = $('tr[data-group=' + responseJSON.id_360 + ']');
			if($tr.find('img[src*=no_image-100x100]')) {
				$tr.find('img[src*=no_image-100x100]').attr('src', responseJSON.path);
			}

			if (responseJSON.status == 'ok')
			{
				cover = "forbbiden";
				if (responseJSON.cover == "1")
					cover = "enabled";
				imageLine360(responseJSON.id, responseJSON.path, responseJSON.position, cover, responseJSON.shops, '')
				$("#imageTable tr:last").after(responseJSON.html);
				$("#countImage").html(parseInt($("#countImage").html()) + 1);
				$("#img" + id).remove();
				
				showSuccessMessage(responseJSON.name + " " + success_add);
			}
			else
				showErrorMessage(responseJSON.error);
			filecheck++;
		},
		onSubmit: function(id, filename)
		{
			$(this)[0].params.id_360set = $('#id_360set').val();

			$("#imageTable").show();
			$("#listImage").append("<li id='img"+id+"'><div class=\"float\" >" + filename + "</div></div><a style=\"margin-left:10px\"href=\"javascript:delQueue(" + id +");\"><img src=\"../img/admin/disabled.gif\" alt=\"\" border=\"0\"></a><p class=\"errorImg\"></p></li>");
		}
	});
</script>

<?php echo $footer; ?>