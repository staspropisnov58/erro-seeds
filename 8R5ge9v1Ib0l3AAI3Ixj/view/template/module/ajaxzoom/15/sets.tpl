<?php
/**
* @version     1.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/
?>

<div id="product-images360sets">
    <div class="row paddingLeft260">
        <a class="btn btn-default btn-sm bt-icon link_add button" href="#">
            <i class="icon-plus"></i> <?php echo $text_ax_title_add_360 ?>
        </a>
    </div>

    <div class="row" id="newForm" style="display:none; max-width:500px;">
        <div class="form-group">
            <label for="name">
                <span class="label-tooltip" data-toggle="tooltip" data-original-title="<?php echo $text_ax_tip_create_new ?>">
                    <?php echo $text_ax_title_create_new ?>
                </span>
            </label>
            <input type="text" id="name" name="name" value="" class="form-control" /> 
        </div>
        
        <?php if(isset($sets_groups)): ?>
            <div class="txt-or"><?php echo $text_ax_or ?></div>

            <div class="form-group">
                <label for="existing">
                    <span class="label-tooltip" data-toggle="tooltip" data-original-title="<?php echo $text_ax_tip_add_existing ?>">
                        <?php echo $text_ax_title_add_existing ?>
                    </span>
                </label>
                <select name="existing" id="existing" class="form-control">
                    <option value="" style="min-width: 100px"><?php echo $text_ax_select ?></option>
                    <?php foreach($sets_groups as $group): ?>
                    <option value="<?php echo $group['id_360'] ?>"><?php echo $group['name'] ?></option>
                    <?php endforeach ?>
                </select>
                <p class="help-block tooltipReplacement"><?php echo $text_ax_replacement ?></p>
            </div>
        <?php endif ?>

        <div class="checkbox" style="margin-top:0px;">
            <label for="zip">
                &nbsp; <input type="checkbox" id="zip" name="zip" value="1" <?php if(!isset($files)): ?>disabled readonly<?php endif ?> />
                <span class="label-tooltip" data-toggle="tooltip" data-original-title="<?php echo sprintf($text_ax_tip_add_zip, $base_url) ?>">
                    <?php echo $text_ax_title_add_zip ?>
                </span>
            </label>
        </div>

        <div class="form-group field-arcfile" style="display:none;">
            <label for="arcfile">
                <span class="label-tooltip">
                    <?php echo $text_ax_select_tip ?>
                </span>
            </label>

            <select name="arcfile" id="arcfile" class="form-control">
                <option value=""><?php echo $text_ax_select ?></option>
                <?php if(isset($files)) foreach($files as $file): ?>
                <option value="<?php echo $file ?>"><?php echo $file ?></option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="checkbox field-arcfile" style="display:none;">
            <label for="delete">
                &nbsp; <input type="checkbox" id="delete" name="delete" value="1" />
                <span class="label-tooltip" data-toggle="tooltip" data-original-title="<?php echo $text_ax_tip_delete_zip ?>">
                    <?php echo $text_ax_title_delete_zip ?>
                </span>
            </label>
        </div>
        <input type="hidden" id="num" name="num" value="" />
        <a href="#" class="save_set btn btn-primary button" >
            <i class="icon-save"></i> <?php echo $text_ax_add_set ?>
        </a>
    </div>
        
    <br><br>

    <div class="row">
        <table class="table tableDnD list" id="imageTableSets" >
            <thead>
                <tr class="nodrag nodrop">
                    <td class="fixed-width-lg"><span class="title_box"><?php echo $text_ax_table_cover ?></span></td>
                    <td><span class="title_box"><?php echo $text_ax_table_name ?></span></td>
                    <td><span class="title_box"><?php echo $text_ax_table_active ?></span></td>
                    <td></td> <!-- action -->
                </tr>
            </thead>
            <tbody id="imageTableSetsRows">
            </tbody>
        </table>

        <table id="lineSet" style="display:none;">
            <tr id="set_id" data-group="group_id">
                <td><img src="<?php echo $base_url ?>resources/img/image_path.gif" alt="legend" title="legend" class="img-thumbnail" /></td>
                <td>legend</td>
                <td>
                    <span class="switch fixed-width-lg hide_class switch-status">
                        <input type="radio" name="status_field" id="status_field_on" value="1" checked_on />
                        <label class="t" for="status_field_on"><?php echo $text_ax_yes ?></label>
                        <input type="radio" name="status_field" id="status_field_off" value="0" checked_off />
                        <label class="t" for="status_field_off"><?php echo $text_ax_no ?></label>
                        <a class="slide-button btn"></a>
                    </span>
                </td>
                <td>
                    <a href="#" class="delete_set pull-right btn btn-default btn-xs button btn btn-danger" style="margin-left: 3px">
                        <span class="fa fa-trash"></span> <?php echo $text_ax_btn_delete ?>
                    </a>
                    <a href="#" class="crop_set pull-right btn btn-default btn-xs hide_class button" style="margin-left: 3px">
                        <span class="fa fa-film"></span> <?php echo $text_ax_btn_product_tour?>
                    </a>
                    <a href="#" class="hotspot_set pull-right btn btn-default btn-xs hide_class button" style="margin-left: 3px">
                        <span class="fa fa-bullseye"></span> <?php echo $text_ax_btn_hotspots?>
                    </a>
                    <a href="#" class="images_set pull-right btn btn-default btn-xs button" style="margin-left: 3px">
                        <span class="fa fa-photo"></span> <?php echo $text_ax_btn_images ?>
                    </a>
                    <a href="#" class="preview_set pull-right btn btn-default btn-xs hide_class button" style="margin-left: 3px">
                        <span class="fa fa-eye"></span> <?php echo $text_ax_btn_preview ?>
                    </a>
                </td>
            </tr>
        </table>
    </div>
</div>