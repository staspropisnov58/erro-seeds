<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-product_stickers" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product_stickers" class="form-horizontal">
          <fieldset>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
              <div class="col-sm-10">
                <select name="product_stickers_status" id="input-status" class="form-control">
                  <?php if ($product_stickers_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">

              <label class="col-sm-2 control-label" for="product_stickers_new_date"><?php echo $entry_product_stickers_new_date; ?></label>
                <div class="col-sm-8">
                  <div class="input-group date">
                  <input type="text" name="product_stickers_new_date" value="<?php echo $product_stickers_new_date; ?>" placeholder="<?php echo $entry_select_date; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>
              <div class="col-sm-1"></div>


              <div class="col-sm-1">
                <button type="button" id="input-product_stickers_hit_limit" name="update_new" class="btn btn-primary pull-right update-stickers"><i class="fa fa-refresh"></i> <?php echo $button_update_new_items; ?></button>
             </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-product_stickers_hit_limit"><?php echo $entry_stickers_hit_limit; ?></label>
              <div class="col-sm-10">
                <input type="text" name="product_stickers_hit_limit" value="<?php echo $product_stickers_hit_limit; ?>" placeholder="<?php echo $entry_stickers_hit_limit; ?>" id="input-product_stickers_hit_limit" class="form-control" />
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-hit_period"><?php echo $entry_hit_sales_period; ?></label>
              <div class="col-sm-8">
                <select name="product_stickers_hit_period" id="input-hit_period" class="form-control">
                  <?php if($product_stickers_hit_period == 'all_time'){ ?>
                    <option selected value = "all_time"> <?php echo $text_all_time; ?></option>
                  <?php }else{ ?>
                    <option value = "all_time"> <?php echo $text_all_time; ?></option>
                  <?php } ?>
                  <?php if($product_stickers_hit_period == 'last_mounth'){ ?>
                    <option selected value = "last_mounth"> <?php echo $text_last_mounth; ?></option>
                  <?php }else{ ?>
                    <option value = "last_mounth"> <?php echo $text_last_mounth; ?></option>
                  <?php } ?>
                  <?php if($product_stickers_hit_period == 'last_year'){ ?>
                    <option selected value = "last_year"> <?php echo $text_last_year; ?></option>
                  <?php }else{ ?>
                    <option value = "last_year"> <?php echo $text_last_year; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-sm-1"></div>
              <div class="col-sm-1">
                <button type="button" id="input-product_stickers_hit_period" name="update_hit" class="btn btn-primary pull-right update-stickers"><i class="fa fa-refresh"></i> <?php echo $button_update_hit_period; ?></button>
             </div>
            </div>
          </fieldset>
          <fieldset>
            <legend><?php echo $text_categories; ?></legend>
            <div class="alert alert-warning">
              <?php echo $warning_category; ?>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-hit-category"><?php echo $entry_hit_category; ?></label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" name="hit_category" value="<?php echo $product_stickers_hit_category['name']; ?>"
                  placeholder="<?php echo $entry_hit_category; ?>" id="input-category-hit" class="form-control autocomplete-category" />
                  <input type="hidden" name="product_stickers_hit_category" value="<?php echo $product_stickers_hit_category['category_id']; ?>" />
                  <span class="input-group-btn">
                  <button type="button" id="button-update-hit" data-toggle="tooltip" title="<?php echo $button_refresh; ?>"
                    data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-update-category">
                    <i class="fa fa-refresh"></i>
                  </button>
                  </span></div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-new-category"><?php echo $entry_new_category; ?></label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" name="new_category" value="<?php echo $product_stickers_new_category['name']; ?>"
                  placeholder="<?php echo $entry_new_category; ?>" id="input-category-new" class="form-control autocomplete-category" />
                  <input type="hidden" name="product_stickers_new_category" value="<?php echo $product_stickers_new_category['category_id']; ?>" />
                  <span class="input-group-btn">
                  <button type="button" id="button-update-new" data-toggle="tooltip" title="<?php echo $button_refresh; ?>"
                    data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-update-category">
                    <i class="fa fa-refresh"></i>
                  </button>
                  </span></div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-recommended-category"><?php echo $entry_recommended_category; ?></label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" name="recommended_category" value="<?php echo $product_stickers_recommended_category['name']; ?>"
                  placeholder="<?php echo $entry_recommended_category; ?>" id="input-category-recommended" class="form-control autocomplete-category" />
                  <input type="hidden" name="product_stickers_recommended_category" value="<?php echo $product_stickers_recommended_category['category_id']; ?>" />
                  <span class="input-group-btn">
                  <button type="button" id="button-update-recommended" data-toggle="tooltip" title="<?php echo $button_refresh; ?>"
                    data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-update-category">
                    <i class="fa fa-refresh"></i>
                  </button>
                  </span></div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$('.date').datetimepicker({
  pickTime: false
});

$('.update-stickers').on('click', function() {
  var form = this.form;
  var data = $(form).serializeArray();
  data.push({'name' : 'button', 'value' : this.name });
  $.ajax({
		url: 'index.php?route=module/product_stickers/updateStickersAjax&token=<?php echo $token; ?>',
    type: 'post',
		dataType: 'json',
    data: data,
		success: function(json) {
			// Check for errors
			if (json['error']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      } else if (json['success']) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
  });
});

$('.autocomplete-category').autocomplete({
  'source': function(request, response) {
    var sticker = this.id.split('-')[2];
    $('#button-update-' + sticker).removeClass('btn-success').addClass('btn-primary');
    $('#button-update-' + sticker).html('<i class="fa fa-refresh"></i>');
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				json.unshift({
					category_id: 0,
					name: '<?php echo $text_none; ?>'
				});

				response($.map(json, function(item) {
					return {
						label: item['name'] + ' | ' + item['status'],
						value: item['category_id'],
            sticker: sticker
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'' + item['sticker'] + '_category\']').val(item['label']);
		$('input[name=\'product_stickers_' + item['sticker'] + '_category\']').val(item['value']);
	}
});

$('.btn-update-category').click(function() {
  var button = this;
  var data = {};
  data.sticker = this.id.split('-')[2];
  data.category_id = $('input[name=\'product_stickers_' + data.sticker + '_category\']').val();

  $.ajax({
    url: 'index.php?route=module/product_stickers/setStickersCategory&token=<?php echo $token; ?>',
    type: 'post',
    data: data,
    dataType: 'json',
    success: function(json) {
      $(button).removeClass('btn-primary').addClass('btn-success');
      $(button).html('<i class="fa fa-check"></i>');
    }
  });
});
</script>
<?php echo $footer; ?>
