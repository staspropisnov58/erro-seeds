<?php
/**
* @version     1.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/
?>

<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons">
                <a href="<?php echo $url_products; ?>" class="button"><?php echo $text_manage_360_views ?></a>
                <a onclick="$('#form-ajaxzoom').submit();" class="button"><?php echo $text_button_save; ?></a>
                <a href="<?php echo $cancel; ?>" class="button"><?php echo $text_button_cancel; ?></a>
            </div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-ajaxzoom" class="form-horizontal">
                <?php foreach($form_data as $key => $data): ?>
                <h3><?php echo $data['title'] ?></h3>

                <?php if($key == 'license'): ?>

                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 24%"><?php echo $text_ax_table_domain ?></th>
                                <th style="width: 10%"><?php echo $text_ax_table_type ?></th>
                                <th style="width: 34%"><?php echo $text_ax_table_key ?></th>
                                <th style="width: 14%"><?php echo $text_ax_table_error200 ?></th>
                                <th style="width: 14%"><?php echo $text_ax_table_error300 ?></th>
                                <th style="width: 5%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="rowsLicense">
                        </tbody>
                    </table>

                    <table id="templateLicense" style="display:none;">
                        <tr>
                            <td><input name="licenses[domain][]" value="domain_placeholder" class="lic-domain" style="width: 100%"></td>
                            <td>
                                <select name="licenses[type][]" class="lic-type" style="min-width:100px;">
                                    <option value="evaluation">evaluation</option>
                                    <option value="developer">developer</option>
                                    <option value="basic">basic</option>
                                    <option value="standard">standard</option>
                                    <option value="business">business</option>
                                    <option value="corporate">corporate</option>
                                    <option value="enterprise">enterprise</option>
                                    <option value="unlimited">unlimited</option>
                                </select>
                            </td>
                            <td><input name="licenses[key][]" value="key_placeholder" class="lic-key" style="width: 100%"></td>
                            <td><input name="licenses[error200][]" value="error200_placeholder" class="lic-error200" style="width: 100%"></td>
                            <td><input name="licenses[error300][]" value="error300_placeholder" class="lic-error300" style="width: 100%"></td>
                            <td>
                                <a class="btn btn-default btn-sm bt-icon link_remove_license" href="#">
                                        <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    </table>

                        <a class="btn btn-default btn-sm bt-icon link_add_license button" href="#" >
                            <i class="fa fa-plus"></i> <?php echo $text_ax_btn_add_license ?>
                        </a>

                    <div class="">
                        <div class="help-block">
                            <br>
                            <a href="http://www.ajax-zoom.com/index.php?cid=contact" target="_blank"><?php echo $text_ax_ask_supprt ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="http://www.ajax-zoom.com/index.php?cid=download#heading_3" target="_blank"><?php echo $text_ax_buy_license ?></a>
                        </div>
                    </div>

                <?php else: ?>

                    <table class="form">
                        <?php echo $data['fields'] ?>
                    </table>
                <?php endif ?>
                <?php endforeach ?>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function () {

    <?php if (substr(trim($licenses), 0, 1) == '['): ?>
    var licenses = $.parseJSON('<?php echo $ajaxzoom_LICENSE ?>');
    <?php else: ?>
    var licenses = [];
    <?php endif ?>

    function licenseLine(data)
    {
        var line = $("#templateLicense").html();
        line = line.replace(/domain_placeholder/g, data.domain);
        var reg = new RegExp('value="' + data.type + '"',"g");
        line = line.replace(reg, 'value="' + data.type + '" selected');
        line = line.replace(/key_placeholder/g, data.key);
        line = line.replace(/error200_placeholder/g, data.error200);
        line = line.replace(/error300_placeholder/g, data.error300);
        line = line.replace(/<tbody>/gi, "");
        line = line.replace(/<\/tbody>/gi, "");
        $("#rowsLicense").append(line);
    }

    $('.link_add_license').click(function (e) {
        e.preventDefault();
        var data = {domain: '', type: '', key: '', error200: '', error300: ''};
        licenseLine(data);
    });


    $('body').on('click', '.link_remove_license', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    });

    for(var k in licenses) {
        licenseLine(licenses[k]);
    }
});
</script>

<?php echo $footer; ?>
