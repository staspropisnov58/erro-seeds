<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-opengraph" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-opengraph" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-basic-settings" data-toggle="tab"><?php echo $tab_basic_settings; ?></a></li>
            <li><a href="#tab-home-page" data-toggle="tab"><?php echo $tab_home_page; ?></a></li>
            <li><a href="#tab-list" data-toggle="tab"><?php echo $tab_opengraph_list; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-basic-settings">
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
              <div class="col-sm-10">
                <select name="opengraph_status" id="input-status" class="form-control">
                  <?php if ($opengraph_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><?php echo $text_opengraph_use_meta_when_no_og; ?></label>
              <div class="col-sm-10">
                <label class="radio-inline">
                  <?php if($opengraph_use_meta_when_no_og == 1){ ?>
                    <input type="radio" name="opengraph_use_meta_when_no_og" value="1" checked />
                  <?php }else{ ?>
                    <input type="radio" name="opengraph_use_meta_when_no_og" value="1" />
                  <?php } ?>
                  <?php echo $text_yes; ?>
                </label>
                <label class="radio-inline">
                  <?php if($opengraph_use_meta_when_no_og == 0){ ?>
                    <input type="radio" name="opengraph_use_meta_when_no_og" value="0" checked />
                  <?php }else{ ?>
                    <input type="radio" name="opengraph_use_meta_when_no_og" value="0" />
                  <?php } ?>
                  <?php echo $text_no; ?>
                </label>
              </div>
            </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-opengraph-image-height"><?php echo $text_height; ?> </label>
                <div class="col-sm-10">
                  <input type="text" name="opengraph_image_height" value="<?php echo $opengraph_image_height; ?>" placeholder="<?php echo $text_height; ?> " id="input-opengraph-image-height" class="form-control">
                  <?php if ($error_opengraph_image_height) { ?>
                  <div class="text-danger"><?php echo $error_opengraph_image_height; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-opengraph-image-width"><?php echo $text_width; ?> </label>
                <div class="col-sm-10">
                  <input type="text" name="opengraph_image_width" value="<?php echo $opengraph_image_width; ?>" placeholder="<?php echo $text_width; ?> " id="input-opengraph-image-width" class="form-control">
                  <?php if ($error_opengraph_image_width) { ?>
                  <div class="text-danger"><?php echo $error_opengraph_image_width; ?></div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-home-page">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-og_homepage-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="og_homepage[page_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($og_homepage['page_description'][$language['language_id']]) ? $og_homepage['page_description'][$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-og_homepage-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_og_homepage[$language['language_id']]['title'])) { ?>
                      <div class="text-danger"><?php echo $error_og_homepage[$language['language_id']]['title']; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-og_homepage-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="og_homepage[page_description][<?php echo $language['language_id']; ?>][description]" value="<?php echo isset($og_homepage['page_description'][$language['language_id']]) ? $og_homepage['page_description'][$language['language_id']]['description'] : ''; ?>" placeholder="<?php echo $entry_description; ?>" id="input-og_homepage-description<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_og_homepage[$language['language_id']]['description'])) { ?>
                      <div class="text-danger"><?php echo $error_og_homepage[$language['language_id']]['description']; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-og_homepage-image<?php echo $language['language_id']; ?>"><?php echo $entry_image; ?></label>
                    <div class="col-sm-10">
                      <a href="" id="thumb-image<?php echo $language['language_id']?>" data-toggle="image" class="img-thumbnail">
                      <img src="<?php echo $og_homepage['page_description'][$language['language_id']]['thumb'] ;?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /> </a>
                      <input type="hidden" name="og_homepage[page_description][<?php echo $language['language_id']; ?>][image]" value="<?php echo $og_homepage['page_description'][$language['language_id']]['image'];?>" id="input-og_homepage-image<?php echo $language['language_id']; ?>" />
                      <?php if (isset($error_og_homepage[$language['language_id']]['image'])) { ?>
                      <div class="text-danger"><?php echo $error_og_homepage[$language['language_id']]['image']; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              <?php } ?>
              </div>
            </div>
          </form>
            <div class="tab-pane" id="tab-list">
              <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>

                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-opengraph-list').submit() : false;"><i class="fa fa-trash-o"></i></button>
              </div>
              <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-opengraph-list">

              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                    <td class="text-left"><?php echo $column_name; ?></td>
                    <td class="text-right"><?php echo $column_action; ?></td>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($pages) { ?>
                  <?php foreach ($pages as $page) { ?>
                  <tr>
                    <td class="text-center"><?php if (in_array($page['page_id'], $selected)) { ?>
                      <input type="checkbox" name="selected[]" value="<?php echo $page['page_id']; ?>" checked="checked" />
                      <?php } else { ?>
                      <input type="checkbox" name="selected[]" value="<?php echo $page['page_id']; ?>" />
                      <?php } ?></td>
                    <td class="text-left"><?php echo $page['name']; ?></td>
                    <td class="text-right"><a href="<?php echo $page['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                  </tr>
                  <?php } ?>
                  <?php } else { ?>
                  <tr>
                    <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </form>
            </div>
          </div>
      </div>
      </div>
    </div>
  </div>





<?php echo $footer; ?>

<script type="text/javascript"><!--
  $('#language a:first').tab('show');

  <?php foreach ($languages as $language) { ?>
  
  <?php } ?>

</script>
