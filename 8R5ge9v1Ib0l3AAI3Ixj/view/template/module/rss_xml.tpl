<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          </ul>
          <div class="tab-pane" id="input-xml-limit">
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-xml-limit"><?php echo $entry_xml_limit; ?></label>
              <div class="col-sm-10">
                <input type="text" name="xml_limit" value="<?php echo $xml_limit; ?>" placeholder="<?php echo $entry_xml_limit; ?>" id="input-xml-limit" class="form-control" />
                <?php if (isset($error_xml_limit) && $error_xml_limit !='') { ?>
                <div class="text-danger"><?php echo $error_xml_limit; ?></div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="tab-content">
            <div class="tab-pane active in" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_xml_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="xml[<?php echo $language['language_id']; ?>]" value="<?php echo isset($xml[$language['language_id']]) ? $xml[$language['language_id']] : ''; ?>" placeholder="<?php echo $entry_xml_name; ?>" id="input-xml-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_xml_name) && $error_xml_name !='') { ?>
                      <div class="text-danger"><?php echo $error_xml_name; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
      </div>
        </form>
      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>

<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#input-description<?php echo $language['language_id']; ?>').summernote({
height: 300
});
<?php } ?>
</script>

<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#input-description_2<?php echo $language['language_id']; ?>').summernote({
height: 300
});
<?php } ?>
</script>

<script type="text/javascript"><!--
$('#language a:first').tab('show');
</script>


//--></script></div>
