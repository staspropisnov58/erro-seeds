<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-smsc" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-smsc" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="sms-login"><?php echo $entry_sms_login; ?></label>
            <div class="col-sm-10">
              <input type="text" name="smsc[sms_login]" value="<?php echo $smsc['sms_login'];?>" placeholder="<?php echo $entry_sms_login; ?>" id="sms-login" class="form-control" />
            </div>
          </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="sms-password"><?php echo $entry_sms_password; ?></label>
              <div class="col-sm-10">
                <input type="text" name="smsc[sms_password]" value="<?php echo $smsc['sms_password'];?>" placeholder="<?php echo $entry_sms_password; ?>" id="sms-password" class="form-control" />
              </div>
            </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="sms-from"><?php echo $entry_sms_from; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="smsc[sms_from]" value="<?php echo $smsc['sms_from'];?>" placeholder="<?php echo $entry_sms_from; ?>" id="sms-login" class="form-control" />
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="sms-request"><?php echo $entry_sms_request; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="smsc[sms_request]" value="<?php echo $smsc['sms_request'];?>" placeholder="<?php echo $entry_sms_request; ?>" id="sms-request" class="form-control" />
                </div>
              </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
