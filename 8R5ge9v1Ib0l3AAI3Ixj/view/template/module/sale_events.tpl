<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-sale_events" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sale_events" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="sale_events_status" id="input-status" class="form-control">
                <?php if ($sale_events_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <table id="sale-events" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left"><?php echo $entry_name; ?></td>
                <td class="text-left"><?php echo $entry_type; ?></td>

                <td></td>
              </tr>
            </thead>
            <tbody>


              <?php $row = 0; ?>

                <?php foreach($sale_events as $sale_event){ ?>

                  <tr id="row<?php echo $row; ?>">

                <td class="text-left" style="width: 30%;">
                  <div>
                    <input type="text" name="sale_events[<?php echo $row; ?>][name]" placeholder="" value="<?php  echo $sale_event['name']; ?>" id="name[<?php echo $row; ?>]" class="form-control" autocomplete="off">
                    <?php if (isset($error_name[$row])) { ?>
                      <div class="text-danger"><?php echo $error_name[$row]; ?></div>
                    <?php } ?>
                  </div>
                </td>
                <td class="text-left" style="width: 30%;">
                  <div>
                    <input type="text" name="sale_events[<?php echo $row; ?>][type]" placeholder="" value="<?php  echo $sale_event['type']; ?>" id="type[<?php echo $row; ?>]" class="form-control" autocomplete="off">
                    <?php if (isset($error_type[$row])) { ?>
                      <div class="text-danger"><?php echo $error_type[$row]; ?></div>
                    <?php } ?>
                  </div>
                </td>
                <input type="hidden" name="sale_events[<?php echo $row; ?>][sale_event_type_id]" value="<?php  echo $sale_event['sale_event_type_id']; ?>">
              <td class="text-left"><button type="button" onclick="confirmDelete(<?php echo $row; ?>);" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
            </tr>
            <?php $row++; ?>
              <?php } ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="text-left"><button type="button" onclick="addRow();" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </table>

        </form>
      </div>
    </div>
  </div>
</div>
<script>
var row = <?php echo $row; ?>;
function addRow() {
	html  = '<tr id="row' + row + '">';
    html += '  <td class="text-left">';

	html += '    <div class="input-group">';
	html += '      <input type="text" name="sale_events[' + row + '][name]" value="" placeholder="" class="form-control" />';
    html += '    </div>';
	html += '  </td>';
  html += '  <td class="text-left">';

html += '    <div class="input-group">';
html += '      <input type="text" name="sale_events[' + row + '][type]" value="" placeholder="" class="form-control" />';
  html += '    </div>';
html += '  </td>';
html += '<td class="text-left"><button type="button" onclick="$(\'#row' + row + ', .tooltip\').remove();" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>'

	html += '</tr>';

	$('#sale-events tbody').append(html);
	row++;
}

function confirmDelete(row) {
    if (confirm("Одумайтесь!!! Вы действительно хотите удалить строку?")) {
        $('#row' + row +', .tooltip').remove();

    } else {
        return false;
    }
}
</script>


<?php echo $footer; ?>
