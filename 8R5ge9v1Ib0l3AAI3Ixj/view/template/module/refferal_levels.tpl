<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="referal_levels" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="referal_levels" class="form-horizontal">
          <table id="refferal-levels" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left"><?php echo $entry_count_order; ?></td>
                <td class="text-left"><?php echo $entry_commission; ?></td>

                <td></td>
              </tr>
            </thead>
            <tbody>


              <?php $row = 0; ?>

                <?php foreach($refferal_levels as $refferal_level){ ?>

                  <tr id="row<?php echo $row; ?>">

                <td class="text-left" style="width: 30%;">
                  <div>
                    <input type="text" name="refferal_levels[<?php echo $row; ?>][quantity]" placeholder="" value="<?php  echo $refferal_level['quantity']; ?>" id="quantity[<?php echo $row; ?>]" class="form-control" autocomplete="off">
                    <?php if (isset($error_quantity[$row])) { ?>
                      <div class="text-danger"><?php echo $error_quantity[$row]; ?></div>
                    <?php } ?>
                  </div>
                </td>
                <td class="text-left" style="width: 30%;">
                  <div>
                    <input type="text" name="refferal_levels[<?php echo $row; ?>][commission]" placeholder="" value="<?php  echo $refferal_level['commission']; ?>" id="commission[<?php echo $row; ?>]" class="form-control" autocomplete="off">
                    <?php if (isset($error_commission[$row])) { ?>
                      <div class="text-danger"><?php echo $error_commission[$row]; ?></div>
                    <?php } ?>
                  </div>
                </td>
              <td class="text-left"><button type="button" onclick="$('#row<?php echo $row; ?>, .tooltip').remove();" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
            </tr>
            <?php $row++; ?>


              <?php } ?>




            </tbody>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="text-left"><button type="button" onclick="addRow();" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </table>

        </form>
      </div>
    </div>
    <div>

    </div>
  </div>
</div>

<script>
var row = <?php echo $row; ?>;
function addRow() {
	html  = '<tr id="row' + row + '">';
    html += '  <td class="text-left">';

	html += '    <div class="input-group">';
	html += '      <input type="text" name="refferal_levels[' + row + '][quantity]" value="" placeholder="" class="form-control" />';
    html += '    </div>';
	html += '  </td>';
  html += '  <td class="text-left">';

html += '    <div class="input-group">';
html += '      <input type="text" name="refferal_levels[' + row + '][commission]" value="" placeholder="" class="form-control" />';
  html += '    </div>';
html += '  </td>';
html += '<td class="text-left"><button type="button" onclick="$(\'#row' + row + ', .tooltip\').remove();" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>'

	html += '</tr>';

	$('#refferal-levels tbody').append(html);
	row++;
}
</script>
<?php echo $footer; ?>
