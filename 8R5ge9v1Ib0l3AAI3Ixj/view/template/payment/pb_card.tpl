<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-pb-card" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-pb-card" class="form-horizontal">
          <?php foreach ($languages as $language) { ?>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-terms<?php echo $language['language_id']; ?>"><?php echo $entry_terms; ?></label>
            <div class="col-sm-10">
              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                <textarea name="pb_card_terms<?php echo $language['language_id']; ?>" cols="80" rows="10" placeholder="<?php echo $entry_terms; ?>" id="input-terms<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset(${'pb_card_terms' . $language['language_id']}) ? ${'pb_card_terms' . $language['language_id']} : ''; ?></textarea>
              </div>
              <?php if (${'error_terms' . $language['language_id']}) { ?>
              <div class="text-danger"><?php echo ${'error_terms' . $language['language_id']}; ?></div>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-payment-details"><?php echo $entry_payment_details; ?></label>
            <div class="col-sm-10">
              <input type="text" name="pb_card_payment_details" value="<?php echo $pb_card_payment_details; ?>" placeholder="<?php echo $entry_payment_details; ?>" id="input-payment-details" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-type"><span data-toggle="tooltip" title="<?php echo $help_type; ?>"><?php echo $entry_type; ?></span></label>
            <div class="col-sm-10">
              <select name="pb_card_type" id="input-type" class="form-control">
                <?php if ($type == 'P') { ?>
                <option value="P" selected="selected"><?php echo $text_percent; ?></option>
                <?php } else { ?>
                <option value="P"><?php echo $text_percent; ?></option>
                <?php } ?>
                <?php if ($type == 'F') { ?>
                <option value="F" selected="selected"><?php echo $text_amount; ?></option>
                <?php } else { ?>
                <option value="F"><?php echo $text_amount; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-payment-details"><span data-toggle="tooltip" title="<?php echo $help_payment_commission; ?>"><?php echo $entry_payment_commission; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="pb_card_payment_commission" value="<?php echo $pb_card_payment_commission; ?>" placeholder="<?php echo $entry_payment_commission; ?>" id="input-payment-details" class="form-control" />
              <?php if ($error_payment_commission) { ?>
                <div class="text-danger"><?php echo $error_payment_commission; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
            <div class="col-sm-10">
              <select name="pb_card_status_id" id="input-order-status" class="form-control">
                <?php foreach ($pb_card_statuses as $pb_card_status) { ?>
                  <?php if ($pb_card_status['order_status_id'] == $pb_card_status_id) { ?>
                    <option value="<?php echo $pb_card_status['order_status_id']; ?>" selected="selected"><?php echo $pb_card_status['name']; ?></option>
                  <?php } else { ?>
                    <option value="<?php echo $pb_card_status['order_status_id']; ?>"><?php echo $pb_card_status['name']; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="pb_card_status" id="input-status" class="form-control">
                <?php if ($pb_card_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $text_image; ?></label>
            <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" /></a>
              <input type="hidden" name="pb_card_image" value="<?php echo $image; ?>" id="input-image" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image-height"><?php echo $entry_image_height; ?></label>
            <div class="col-sm-10">
              <input type="text" name="pb_card_image_height" value="<?php echo $pb_card_image_height; ?>" placeholder="<?php echo $entry_image_height; ?>" id="input-image-height" class="form-control" />
              <?php if ($error_image_height) { ?>
                <div class="text-danger"><?php echo $error_image_height; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image-width"><?php echo $entry_image_width; ?></label>
            <div class="col-sm-10">
              <input type="text" name="pb_card_image_width" value="<?php echo $pb_card_image_width; ?>" placeholder="<?php echo $entry_image_width; ?>" id="input-image-width" class="form-control" />
              <?php if ($error_image_width) { ?>
                <div class="text-danger"><?php echo $error_image_width; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="pb_card_sort_order" value="<?php echo $pb_card_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
              <?php if ($error_sort_order) { ?>
                <div class="text-danger"><?php echo $error_sort_order; ?></div>
              <?php } ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
