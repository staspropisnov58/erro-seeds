<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <table id="images" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left"><?php echo $entry_title; ?></td>
                <td class="text-left"><?php echo $entry_link; ?></td>
                <td class="text-left"><?php echo $entry_image; ?></td>
                <td class="text-right"><?php echo $entry_sort_order; ?></td>
                <?php if($sale_events_status){ ?>
                  <td class="text-right"><?php echo $entry_sale_event; ?></td>
                  <td class="text-right"><?php echo $entry_status; ?></td>
                <?php } ?>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php $image_row = 0; ?>
              <?php foreach ($banner_images as $banner_image) { ?>
                <?php // var_dump($banner_image);die; ?>


              <tr id="image-row<?php echo $image_row; ?>">
                <td class="text-left"><?php foreach ($languages as $language) { ?>
                  <div class="input-group pull-left"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> </span>
                    <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />
                  </div>
                  <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>
                  <div class="text-danger"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?></div>
                  <?php } ?>
                  <?php } ?>
                </td>

                <td class="text-left" style="width: 30%;">
                  <?php foreach ($languages as $language) { ?>
                  <div>
                    <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][link]" value="<?php echo $banner_image['banner_image_description'][$language['language_id']]['link']; ?>" placeholder="<?php echo $entry_link; ?>" class="form-control" />
                  </div>
                  <?php } ?>
                </td>


                <td class="text-left">
                  <?php  foreach ($languages as $language) { ?>
                <div>
                  <a href="" id="thumb-image<?php echo $language['language_id'] . $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $banner_image['banner_image_description'][$language['language_id']]['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][image]" value="<?php echo $banner_image['banner_image_description'][$language['language_id']]['image']; ?>" id="input-image<?php echo $language['language_id'] . $image_row; ?>" />
                </div>
                  <?php  } ?>
                </td>
                <td class="text-right"><input type="text" name="banner_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $banner_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                <?php if($sale_events_status){ ?>
                    <td class="text-right">
                      <input type="text" class="banner-sale-event" name="banner_image[<?php echo $image_row; ?>][sale_event]" data-row = "<?php echo $image_row; ?>"  value="<?php echo $banner_image['sale_event_name']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" />
                      <input type="hidden" name="banner_image[<?php echo $image_row; ?>][sale_event_id]" value="<?php echo $banner_image['sale_event_id']; ?>" />
                    </td>
                    <td class="text-right">
                      <select name="banner_image[<?php echo $image_row; ?>][status]" id="input-status" class="form-control">
                        <?php // var_dump($banner_image); ?>
                        <?php if ($banner_image['status']) { ?>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </td>
                  <?php } ?>
                  <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>, .tooltip').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
              </tr>
              <?php $image_row++; ?>
              <?php } ?>


            </tbody>
            <tfoot>
              <tr>
                <td colspan="4"></td>
                <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_banner_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </table>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
var next_image_row = <?php echo $image_row; ?>;

$('.banner-sale-event').autocomplete({

	'source': function(request, response) {

    var image_row = $(this).data("row");

		$.ajax({
			url: 'index.php?route=marketing/sale_events/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				json.unshift({
					sale_event_id: 0,
					name: '<?php echo $text_none; ?>'
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['sale_event_id'],
            datetime_start: item['datetime_start'],
            datetime_end: item['datetime_end'],
            image_row: image_row,
					}
				}));
			}
		});
	},

	'select': function(item) {
		$('input[name=\'banner_image[' + item['image_row'] +  '][sale_event]\']').val(item['label']);
    $('input[name=\'banner_image[' + item['image_row'] +  '][sale_event_id]\']').val(item['value']);
	}
});

function addImage() {
	html  = '<tr id="image-row' + next_image_row + '">';
    html += '  <td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '    <div class="input-group">';
	html += '      <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span><input type="text" name="banner_image[' + next_image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="" placeholder="<?php echo $entry_title; ?>" class="form-control" />';
    html += '    </div>';
	<?php } ?>
	html += '  </td>';
  html += '<td class="text-left">'
  <?php foreach($languages as $language){ ?>
  html += '    <div>';
	html += '  <input type="text" name="banner_image[' + next_image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][link]" value="" placeholder="<?php echo $entry_link; ?>" class="form-control" />';
  html += '    </div>';
  <?php } ?>
  html += '</td>'
  html += '  <td class="text-left">';
  <?php foreach ($languages as $language) { ?>
  html += '<div>';
  html += '  <a href="" id="thumb-image<?php echo $language['language_id']; ?>' + next_image_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="banner_image[' + next_image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][image]" value="" id="input-image<?php echo $language['language_id']; ?>' + next_image_row + '" />';
  html += '</div>';
  <?php } ?>
  html += '  </td>';
	html += '  <td class="text-right"><input type="text" name="banner_image[' + next_image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
  <?php if($sale_events_status){ ?>
    html += '<td class="text-right">';
      html += '<input type="text" class="banner-sale-event" name="banner_image[' + next_image_row + '][sale_event]" data-row = "' + next_image_row + '"  value="" placeholder="<?php echo $entry_sale_event; ?>" class="form-control" />';
      html += '<input type="hidden" name="banner_image[' + next_image_row + '][sale_event_id]" value="" />';
    html += '</td>';

    html += '<td class="text-right">';
      html += '<select name="banner_image[' + next_image_row + '][status]" id="input-status" class="form-control">';
        html += '<option value="0" selected="selected"><?php echo $text_disabled; ?></option>';
        html += '<option value="1"><?php echo $text_enabled; ?></option>';
      html += '</select>';
    html += '</td>';

  <?php } ?>
  html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + next_image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#images tbody').append(html);

	next_image_row++;
}
//--></script></div>
<?php echo $footer; ?>
