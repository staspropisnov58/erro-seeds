<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="http://test.errors-seeds.com.ua/8R5ge9v1Ib0l3AAI3Ixj/index.php?route=design/banner/add&amp;token=96b3f69da76131b512fdff2871cddc96" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Добавить"><i class="fa fa-plus"></i></a>
            </div>
            <h1>Меню</h1>
            <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home fa-lg"></i></a></li>
                <li><a href="#">Меню</a></li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> Меню</h3>
            </div>
            <div class="panel-body">
                <form action="http://test.errors-seeds.com.ua/8R5ge9v1Ib0l3AAI3Ixj/index.php?route=design/banner/delete&amp;token=96b3f69da76131b512fdff2871cddc96" method="post" enctype="multipart/form-data" id="form-banner">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
                                    <td class="text-left"> <a href="http://test.errors-seeds.com.ua/8R5ge9v1Ib0l3AAI3Ixj/index.php?route=design/banner&amp;token=96b3f69da76131b512fdff2871cddc96&amp;sort=name&amp;order=DESC" class="asc">Название меню</a>
                                    </td>
                                    <td class="text-right">Действие</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"> <input type="checkbox" name="selected[]" value="7">
                                    </td>
                                    <td class="text-left">Simple menu</td>
                                    <td class="text-right">
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="Редактировать"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a>
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Удалить"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> <input type="checkbox" name="selected[]" value="6">
                                    </td>
                                    <td class="text-left">Dropdown menu</td>
                                    <td class="text-right">
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="Редактировать"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a>
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Удалить"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"> <input type="checkbox" name="selected[]" value="8">
                                    </td>
                                    <td class="text-left">Table menu</td>
                                    <td class="text-right">
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="Редактировать"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                      <a href="#" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a>
                                      <a href="#" data-toggle="tooltip" title="" class="btn  btn-danger" data-original-title="Удалить"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
