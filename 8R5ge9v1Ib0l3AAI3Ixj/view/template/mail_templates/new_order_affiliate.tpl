<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-eye"></i> <?php echo $text_test; ?></h3>
      </div>
      <?php if(isset($errors)){ ?>
        <?php foreach($errors as $error){ ?>
          <div class="alert alert-danger" role="alert"><?php echo $error; ?> </div>
        <?php }?>
      <?php } ?>

      <?php if(isset($success)){ ?>
        <div class="alert alert-success" role="alert"><?php echo $success; ?> </div>
      <?php } ?>
      <div class="panel-body">
        <form action="<?php echo $action;?>" method="post" enctype="multipart/form-data" class="form-horizontal">

          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_language; ?></label>
            <div class="col-sm-10">
              <label class="radio-inline">
                <?php foreach ($languages as $language){ ?>
                  <?php if ($config_admin_language === $language['code']){ ?>
                    <label class="radio-inline">
                      <input type="radio" name="language" value="<?php echo $language['directory'] ?>" checked="checked" /><img src="view/image/flags/<?php echo $language['image']; ?>" /> <?php echo $language['name']; ?>
                    </label>
                <?php }else{ ?>
                  <label class="radio-inline">
                    <input type="radio" name="language"value="<?php echo $language['directory'] ?>" /><img src="view/image/flags/<?php echo $language['image']; ?>" /> <?php echo $language['name']; ?>
                  </label>
                <?php } ?>
                <?php } ?>
              </label>
            </div>
            </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-order-id"><?php echo $text_search; ?></label>
            <div class="col-sm-10">
              <input type="text"  oninput="checkParams()" name="order_id" value="<?php if(isset($order_id)){echo $order_id;}?>" placeholder="<?php echo $text_search; ?>" id="input-order-id" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_mail; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" oninput="checkParams()" value="<?php if(isset($email)){echo $email;}?>" placeholder="<?php echo $entry_mail; ?>" id="input-email" class="form-control" />
            </div>
          </div>

          <div class = "text-right">
            <button type="button" data-fields='order_id' id="button-view-notification" onclick="Open()" class="btn btn-primary js-button"><i class="fa fa-eye"></i> <?php echo $text_wiev_latter; ?> </button>
            <button type="button" data-fields='order_id, email' id="button-make-notification" onclick="SendMail()" class="btn btn-primary js-button"><i class="fa fa-envelope"></i> <?php echo $text_send_test_latter; ?> </button>
        </div>
        </form>
      </div>
    </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1><?php echo $heading_title; ?></h1>
      </div>
      <div class="modal-body">
        <iframe id="myframe" name="preview" width="570px" height="700px"></iframe>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">

document.getElementById('button-view-notification').disabled = true;
document.getElementById('button-make-notification').disabled = true;

function Open() {

  	$.ajax({
  		url: "index.php?route=mail_templates/new_order_affiliate/preview&token=<?php echo $token; ?>&order_id=" + $('#input-order-id').val() + "&template_name=new_order_affiliate&language="+ $('input[name="language"]:checked').val(),
  		type: 'get',
  		dataType: 'json',
  		success: function(json) {

  			if (json.errors) {
          for (const error in json.errors) {
            $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.errors[error] + '</div>');

          }
  			}

  			if (json.html) {
          var myFrame = $("#myframe").contents().find('body');
          myFrame.html(json.html);
          $('#modal-preview').modal()
  			}
  		}
  	});
}

function SendMail() {
  $.ajax({
    url:"index.php?route=mail_templates/new_order_affiliate/sendMail&token=<?php echo $token; ?>&order_id=" + $('#input-order-id').val() + "&email=" + $('#input-email').val() + "&template_name=new_order_affiliate&language="+ $('input[name="language"]:checked').val(),
    type: 'get',
    dataType: 'json',
    success: function(json) {

      if (json.errors) {
        for (const error in json.errors) {
          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.errors[error] + '</div>');

        }
      }

      if (json.success) {
        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json.success + '</div>');
      }
    }
  });
}

$('input[name=\'order_id\']').autocomplete({
'source': function(request, response) {
  $.ajax({
    url: 'index.php?route=sale/order/autocompleteById&token=<?php echo $token; ?>&affiliate=true&order_id=' +  encodeURIComponent(request),
    dataType: 'json',
    success: function(json) {
      json.unshift({
        // order_id: 0,
        order_id: '<?php echo $text_none; ?>'
      });

      response($.map(json, function(item) {
        return {
          label: item['order_id'],
          value: item['order_id']
        }
      }));
    }
  });
},
'select': function(item) {
  $('input[name=\'order_id\']').val(item['label']);
  $('input[name=\'order_id\']').val(item['value']);
  document.getElementById('button-view-notification').disabled = false;
}
});
</script>
