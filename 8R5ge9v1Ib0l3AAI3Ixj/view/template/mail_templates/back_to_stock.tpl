<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h3 class="panel-title"><i class="fa fa-eye"></i> <?php echo $text_test; ?></h3>
      </div>

      <?php if(isset($errors)){ ?>
        <?php foreach($errors as $error){ ?>
          <div class="alert alert-danger" role="alert"><?php echo $error; ?> </div>
        <?php }?>
      <?php } ?>

      <?php if(isset($success)){ ?>
        <div class="alert alert-success" role="alert"><?php echo $success; ?> </div>
      <?php } ?>

      <div class="panel-body">
        <form action="<?php echo $action;?>" method="post" enctype="multipart/form-data" class="form-horizontal">

          <div class="form-group" <?php if(!$field){?> style = "display:none" <?php }?> >
            <label class="col-sm-2 control-label" for="input-shop"><?php echo $entry_shop; ?></label>
            <div class="col-sm-10">
              <select name="shop_id" id="input-shop" class="form-control">
                <?php foreach ($stores as $key => $store) { ?>
                  <?php if ($key === 0){ ?>
                  <option value="<?php echo $store; ?> " selected="selected"><?php echo $store; ?></option>
                <?php }else{ ?>
                  <option value="<?php echo $store; ?> "><?php echo $store; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_language; ?></label>
            <div class="col-sm-10">
              <label class="radio-inline">
                <?php foreach ($languages as $language){ ?>
                  <?php if ($config_admin_language === $language['code']){ ?>
                    <label class="radio-inline">
                      <input type="radio" name="language" value="<?php echo $language['directory'] ?>" checked="checked" /><img src="view/image/flags/<?php echo $language['image']; ?>" /> <?php echo $language['name']; ?>
                    </label>
                <?php }else{ ?>
                  <label class="radio-inline">
                    <input type="radio" name="language"value="<?php echo $language['directory'] ?>" /><img src="view/image/flags/<?php echo $language['image']; ?>" /> <?php echo $language['name']; ?>
                  </label>
                <?php } ?>
                <?php } ?>
              </label>
            </div>
            </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="products"><?php echo $text_search; ?></label>
            <div class="col-sm-10">
              <input type="text" name="products" value="" oninput="checkParams()" placeholder="<?php echo $text_search; ?>" id="products" class="form-control"/>
              <div id="product" class="well well-sm" style="height: 150px; overflow: auto;">
                <?php foreach ($products as $product) { ?>
                <div id="product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['product_name']; ?>
                  <input type="hidden" name="product[]" value="<?php echo $product['product_id']; ?>" />
                </div>
                <?php } ?>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_mail; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" oninput="checkParams()" value="<?php if(isset($email)){echo $email;}?>" placeholder="<?php echo $entry_mail; ?>" id="input-email" class="form-control" />
            </div>
          </div>

          <div class = "text-right">
            <button type="button" data-fields='products[]' id="button-view-notification" onclick="Open()" class="btn btn-primary js-button"><i class="fa fa-eye"></i> <?php echo $text_wiev_latter; ?> </button>
            <button type="button" data-fields='products[], email' id="button-make-notification" onclick="SendMail()" class="btn btn-primary js-button"><i class="fa fa-envelope"></i> <?php echo $text_send_test_latter; ?> </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1><?php echo $heading_title; ?></h1>
      </div>
      <div class="modal-body">
        <iframe id="myframe" name="preview" width="570px" height="700px"></iframe>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">

document.getElementById('button-view-notification').disabled = true;
document.getElementById('button-make-notification').disabled = true;

function Open() {
  var products = [];

  $('input[name="products[]"]').each(function () {

    products.push($(this).val());
  });

    // window.open("index.php?route=mail_templates/back_to_stock/requestTemplate&token=<?php echo $token; ?>&products=" + $('input[name=\'products[]\']') + "&template_name=back_to_stock", "_blank");

      $.ajax({
          type: "POST",
          url: "index.php?route=mail_templates/back_to_stock/requestTemplate&token=<?php echo $token; ?>&template_name=back_to_stock&language="+ $('input[name="language"]:checked').val(),
          data: "products=" + products,
          dataType: "json",
        success: function(json){
            if (json.errors) {
              for (const error in json.errors) {
                $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.errors[error] + '</div>');

              }
            }

            if (json.html) {
              var myFrame = $("#myframe").contents().find('body');
              myFrame.html(json.html);
              $('#modal-preview').modal()
            }
        },

        error: function($data){
          window.location.replace("index.php?route=mail_templates/back_to_stock&token=<?php echo $token; ?>");
        }
    })
}

function SendMail() {

  var products = [];

  $('input[name="products[]"]').each(function () {

    products.push($(this).val());
  });

    // window.open("index.php?route=mail_templates/back_to_stock/requestTemplate&token=<?php echo $token; ?>&products=" + $('input[name=\'products[]\']') + "&template_name=back_to_stock", "_blank");

      $.ajax({
          type: "POST",
          url: "index.php?route=mail_templates/back_to_stock/sendMail&token=<?php echo $token; ?>&template_name=back_to_stock&email=" + $('#input-email').val()+ "&language="+ $('input[name="language"]:checked').val(),
          data: "products=" + products,
          dataType: "json",
          success: function(json) {

            if (json.errors) {
              for (const error in json.errors) {
                $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json.errors[error] + '</div>');

              }
            }

            if (json.success) {
              $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json.success + '</div>');
            }
          }
    })

}




$('input[name=\'products\']').autocomplete({
	'source': function(request, response) {
    var exclude = [];
		$.ajax({
      url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&store_id=' + $('select[name="shop_id"]').val() + '&status=1&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
        json.unshift({
          product_id: 0,
          name: '<?php echo $text_none; ?>'
        });
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'products\']').val('');

		$('#product' + item['value']).remove();

		$('#product').append('<div id="product-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="products[]" value="' + item['value'] + '" /></div>');

    document.getElementById('button-view-notification').disabled = false;

	}

});



$('#product').delegate('.fa-minus-circle', 'click', function() {
  document.getElementById('button-view-notification').disabled = true;
	$(this).parent().remove();
  checkParams();
});



</script>
