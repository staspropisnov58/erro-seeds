<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-calendar" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-calendar" class="form-horizontal">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>

					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab-general">

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-image"><?php echo $entry_opengraph_image; ?></label>
								<div class="col-sm-10">
									<a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
									<input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
								</div>
								<?php if (isset($error_image)) { ?>
								<div class="text-danger"><?php echo $error_image; ?></div>
								<?php } ?>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-date-available"><?php echo $entry_date_available; ?></label>
								<div class="col-sm-3">
									<div class="input-group date">
										<input type="text"
											   name="date_available"
											   value="<?php echo $date_available; ?>"
											   placeholder="<?php echo $entry_date_available; ?>"
											   data-date-format="YYYY-MM-DD"
											   id="input-date-available"
											   class="form-control" />

										<span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
								</div>
								<?php if (isset($error_date_available)) { ?>
								<div class="text-danger"><?php echo $error_date_available; ?></div>
								<?php } ?>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-date_available_end"><?php echo $entry_date_available; ?></label>
								<div class="col-sm-3">
									<div class="input-group date">
										<input type="text"
											   name="date_available_end"
											   value="<?php echo $date_available_end; ?>"
											   placeholder="<?php echo $entry_date_available; ?>"
											   data-date-format="YYYY-MM-DD"
											   id="input-date_available_end"
											   class="form-control" />

										<span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
								</div>
								<?php if (isset($error_date_available_end)) { ?>
								<div class="text-danger"><?php echo $error_date_available_end; ?></div>
								<?php } ?>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
								<div class="col-sm-10">
									<select name="status" id="input-status" class="form-control">
										<?php if ($status) { ?>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
										<option value="0"><?php echo $text_disabled; ?></option>
										<?php } else { ?>
										<option value="1"><?php echo $text_enabled; ?></option>
										<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
								<div class="col-sm-10">
									<input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
								</div>
							</div>
							<ul class="nav nav-tabs" id="language">
								<?php foreach ($languages as $language) { ?>
								<li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content">
								<?php foreach ($languages as $language) { ?>
								<div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
									<div class="form-group required">
										<label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
										<div class="col-sm-10">
											<input type="text" name="calendar_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($calendar_description[$language['language_id']]) ? $calendar_description[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
											<?php if (isset($error_title[$language['language_id']])) { ?>
											<div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
											<?php } ?>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-sm-2 control-label" for="input-url<?php echo $language['language_id']; ?>"><?php echo $entry_url; ?></label>
										<div class="col-sm-10">
											<input type="text" name="calendar_description[<?php echo $language['language_id']; ?>][url]" value="<?php echo isset($calendar_description[$language['language_id']]) ? $calendar_description[$language['language_id']]['url'] : ''; ?>" placeholder="<?php echo $entry_url; ?>" id="input-url<?php echo $language['language_id']; ?>" class="form-control" />

										</div>
									</div>
									<div class="form-group ">
										<label class="col-sm-2 control-label" for="input-img_lan<?php echo $language['language_id']; ?>">Изображение</label>
										<div class="col-sm-10">

											<a href="" id="thumb-img_lan<?php echo $language['language_id']; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_img_lan[$language['language_id']]; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
											<input type="hidden" name="calendar_description[<?php echo $language['language_id']; ?>][img_lan]" value="<?php echo isset($calendar_description[$language['language_id']]) ? $calendar_description[$language['language_id']]['img_lan'] : ''; ?>" id="input-img_lan<?php echo $language['language_id']; ?>" />
										</div>
									</div>


									<div class="form-group ">
										<label class="col-sm-2 control-label" for="input-description"><?php echo $entry_description; ?></label>
										<div class="col-sm-10">
											<textarea name="calendar_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($calendar_description[$language['language_id']]) ? $calendar_description[$language['language_id']]['description'] : ''; ?></textarea>
											<?php if (isset($error_description[$language['language_id']])) { ?>
											<div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
											<?php } ?>
										</div>
									</div>



								</div>
								<?php } ?>
							</div>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function (){
            $('.date').datetimepicker({
                pickDate: true,
                pickTime: false
            });

		})
	</script>
	<script type="text/javascript"><!--
        <?php foreach ($languages as $language) { ?>
            $('#input-description<?php echo $language['language_id']; ?>').summernote({
                height: 300
            });

            <?php } ?>
        //--></script>
	<script type="text/javascript"><!--
        $('#language a:first').tab('show');



        //--></script></div>
<?php echo $footer; ?>
