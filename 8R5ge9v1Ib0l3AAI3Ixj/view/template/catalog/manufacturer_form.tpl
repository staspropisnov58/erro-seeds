<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
            <li><a href="#tab-category_to_manufacturer" data-toggle="tab"><?php echo $category_to_manufacturer_text; ?></a></li>
            <?php if(isset($opengraph_use_meta_when_no_og)){ ?>
              <?php if($opengraph_status){ ?>
              <li><a href="#tab-opengraph" data-toggle="tab"><?php echo $tab_open_graph; ?></a></li>
              <?php } ?>
            <?php } ?>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active in" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
          <!--                --><?php //echo "<pre>"; exit();?>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="manufacturer_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description_2<?php echo $language['language_id']; ?>"><?php echo $entry_description_2; ?></label>
                    <div class="col-sm-10">
                      <textarea name="manufacturer_description[<?php echo $language['language_id']; ?>][description_2]" placeholder="<?php echo $entry_description_2; ?>" id="input-description_2<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['description_2'] : ''; ?></textarea>
                    </div>
                  </div>

                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="manufacturer_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="manufacturer_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="manufacturer_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($manufacturer_description[$language['language_id']]) ? $manufacturer_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                </div>
                <?php } ?>
            </div>
          </div>
          <div class="tab-pane fade" id="tab-data">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
            <div class="col-sm-10">
              <div class="well well-sm" style="height: 150px; overflow: auto;">
                <div class="checkbox">
                  <label>
                    <?php if (in_array(0, $manufacturer_store)) { ?>
                    <input type="checkbox" name="manufacturer_store[]" value="0" checked="checked" />
                    <?php echo $text_default; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="manufacturer_store[]" value="0" />
                    <?php echo $text_default; ?>
                    <?php } ?>
                  </label>
                </div>
                <?php foreach ($stores as $store) { ?>
                <div class="checkbox">
                  <label>
                    <?php if (in_array($store['store_id'], $manufacturer_store)) { ?>
                    <input type="checkbox" name="manufacturer_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                    <?php echo $store['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="manufacturer_store[]" value="<?php echo $store['store_id']; ?>" />
                    <?php echo $store['name']; ?>
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
              <?php if ($error_keyword) { ?>
              <div class="text-danger"><?php echo $error_keyword; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-meta-robots"><span data-toggle="tooltip" title="<?php echo $help_meta_robots; ?>"><?php echo $entry_meta_robots; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="meta_robots" value="<?php echo $meta_robots; ?>" placeholder="<?php echo $entry_meta_robots; ?>" id="input-meta-robots" class="form-control" />
              <?php if ($error_meta_robots) { ?>
              <div class="text-danger"><?php echo $error_meta_robots; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
            <div class="col-sm-10"> <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
              <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="tab-category_to_manufacturer">
          <div class="table-responsive">
            <table id="category_to_manufacturer" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left"><?php echo $text_category; ?></td>
                  <td class="text-left"><?php echo $text_copy_from; ?></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?php $category_to_manufacturer_row = 0; ?>
                <?php foreach ($category_to_manufacturer as $category_manufacturer) { ?>
                <tr id="category_to_manufacturer-row<?php echo $category_to_manufacturer_row; ?>">
                  <td class="text-left"><input type="text" onfocus="complete('category_to_manufacturer[<?php echo $category_to_manufacturer_row; ?>][name');" name="category_to_manufacturer[<?php echo $category_to_manufacturer_row; ?>][name]; ?>" value="<?php echo $category_manufacturer['name']; ?>" placeholder="<?php echo $entry_category; ?>" class="form-control" />
                    <input type="hidden" name="category_to_manufacturer[<?php echo $category_to_manufacturer_row; ?>][name_id]" value="<?php echo $category_manufacturer['category_id']; ?>" />
                  </td>

                  <!-- <td class="text-right"><input type="text" onfocus="complete('category_to_manufacturer[<? // php echo $category_to_manufacturer_row; ?>][copy_from');" name="category_to_manufacturer[<?php // echo $category_to_manufacturer_row; ?>][copy_from]" value="<? // php echo $copy_from; ?>" placeholder="<?php // echo $entry_copy_from; ?>" class="form-control" />
                    <input type="hidden" name="category_to_manufacturer[<?php // echo $category_to_manufacturer_row; ?>][copy_from_id]" value="<?php // echo $copy_from_id; ?>" />

                  </td> -->
                  <td></td>
                  <td class="text-left"><button type="button" onclick="$('#category_to_manufacturer-row<?php echo $category_to_manufacturer_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                </tr>
                <?php $category_to_manufacturer_row++; ?>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"></td>
                  <td class="text-left"><button type="button" onclick="addRow();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <?php if(isset($opengraph_use_meta_when_no_og)){ ?>
          <?php if($opengraph_status){ ?>
          <div class="tab-pane active" id="tab-opengraph">
            <ul class="nav nav-tabs" id="language-opengraph">
              <?php foreach ($languages as $language) { ?>
              <li><a href="#language<?php echo $language['language_id']; ?>-opengraph" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($languages as $language) { ?>
              <div class="tab-pane" id="language<?php echo $language['language_id']; ?>-opengraph">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-opengraph-title<?php echo $language['language_id']; ?>"><?php echo $entry_opengraph_title; ?> </label>
                  <div class="col-sm-10">
                    <input type="text" name="opengraph[<?php echo $language['language_id']; ?>][title]" value="<?php echo $opengraph[$language['language_id']]['title']; ?>" placeholder="<?php echo $entry_opengraph_title; ?> " id="input-opengraph-title<?php echo $language['language_id']; ?>" class="form-control">
                    <?php if (isset($error_opengraph[$language['language_id']]['title'])) { ?>
                    <div class="text-danger"><?php echo $error_opengraph[$language['language_id']]['title']; ?></div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-opengraph-description<?php echo $language['language_id']; ?>"><?php echo $entry_opengraph_description; ?></label>
                  <div class="col-sm-10">
                    <input type="text" name="opengraph[<?php echo $language['language_id']; ?>][description]" value="<?php echo $opengraph[$language['language_id']]['description']; ?>" placeholder="<?php echo $entry_opengraph_description; ?> " id="input-opengraph-description<?php echo $language['language_id']; ?>" class="form-control">
                    <?php if (isset($error_opengraph[$language['language_id']]['description'])) { ?>
                    <div class="text-danger"><?php echo $error_opengraph[$language['language_id']]['description']; ?></div>
                    <?php } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-opengraph-image<?php echo $language['language_id']; ?>"><?php echo $entry_image; ?></label>
                  <div class="col-sm-10">
                    <a href="" id="thumb-opengraph-image<?php echo $language['language_id']?>" data-toggle="image" class="img-thumbnail">
                    <img src="<?php echo $opengraph[$language['language_id']]['thumb'] ;?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /> </a>
                    <input type="hidden" name="opengraph[<?php echo $language['language_id']; ?>][image]" value="<?php echo $opengraph[$language['language_id']]['image'];?>" id="input-opengraph-image<?php echo $language['language_id']; ?>" />
                    <?php if (isset($error_opengraph[$language['language_id']]['image'])) { ?>
                    <div class="text-danger"><?php echo $error_opengraph[$language['language_id']]['image']; ?></div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            <?php } ?>
            </div>
          </div>
        <?php } ?>
      <?php } ?>
      </div>


        </form>
      </div>
    </div>
  </div>

</div>
<?php echo $footer; ?>

<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#input-description<?php echo $language['language_id']; ?>').summernote({
height: 300
});
<?php } ?>
</script>

<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#input-description_2<?php echo $language['language_id']; ?>').summernote({
height: 300
});
<?php } ?>
</script>

<script type="text/javascript"><!--
$('#language a:first').tab('show');
$('#language-opengraph a:first').tab('show');

</script>
<script type="text/javascript"><!--
var category_to_manufacturer_row = <?php echo $category_to_manufacturer_row; ?>;

function addRow() {
html  = '<tr id="category_to_manufacturer-row' + category_to_manufacturer_row + '">';
html += '  <td class="text-left"><input type="text" onfocus="complete(\'category_to_manufacturer[' + category_to_manufacturer_row + '][name\');" name="category_to_manufacturer[' + category_to_manufacturer_row + '][name]" value="" placeholder="<?php echo $entry_category; ?>" class="form-control" /> <input type="hidden" name="category_to_manufacturer[' + category_to_manufacturer_row + '][name_id]" value="" /></td>';
html += '  <td class="text-right"><input type="text" onfocus="complete(\'category_to_manufacturer[' + category_to_manufacturer_row + '][copy_from\');" name="category_to_manufacturer[' + category_to_manufacturer_row + '][copy_from]" value="" placeholder="<?php echo $entry_copy_from; ?>" class="form-control" /> <input type="hidden" name="category_to_manufacturer['+ category_to_manufacturer_row + '][copy_from_id]" value="" /></td>';
html += '  <td class="text-left"><button type="button" onclick="$(\'#category_to_manufacturer-row' + category_to_manufacturer_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
html += '</tr>';

$('#category_to_manufacturer tbody').append(html);

category_to_manufacturer_row++;
}
</script>
<script type="text/javascript"><!--
function complete(name){
  $('input[name="' + name + ']"]').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        json.unshift({
          category_id: 0,
          name: '<?php echo $text_none; ?>'
        });

        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['category_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    $('input[name="' + name + ']"]').val(item['label']);
    $('input[name="' + name + '_id]"]').val(item['value']);
  }
  });
}
</script>
//--></script></div>
