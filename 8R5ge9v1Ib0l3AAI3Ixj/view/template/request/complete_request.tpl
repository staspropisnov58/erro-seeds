<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-customer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">

      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-product_name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-product_name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-complete"><?php echo $entry_date_complete; ?></label>
                  <div class="input-group date">
                    <input type="text" name="filter_date_complete" value="<?php echo $filter_date_complete; ?>" placeholder="<?php echo $entry_date_complete; ?>" data-date-format="YYYY-MM-DD" id="input-date-complete" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>
              </div>
            </div>
            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
          </div>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <div class="tab-content">
              <div class="table-responsive">

                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php if ($sort == 'nrc.customer_id') { ?>
                        <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'nrc.email') { ?>
                        <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'nrc.date_complete') { ?>
                        <a href="<?php echo $sort_date_complete; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_complete; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_date_complete; ?>"><?php echo $column_date_complete; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'product_name') { ?>
                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_name; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_name; ?>"><?php echo $column_product_name; ?></a>
                        <?php } ?>
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($requests_data as $request_data){ ?>
                    <tr>
                      <td class="text-left">
                        <?php if($request_data['customer_data']){ ?>
                          <a href="<?php echo $request_data['customer_data']['href'];?>"> <?php echo $request_data['customer_data']['customer_name'];?> </a>
                        <?php }else{ ?>
                          <?php echo $text_guest; ?>
                        <? } ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['email']; ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['date_complete']; ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['product_name']; ?>
                      </td>
                    </tr>
                    <? } ?>
                  </tbody>
                </table>
              <div class="row">
                <div data-paginate="active" class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>

  $('#button-filter').on('click', function() {
  	var url = 'index.php?route=request/complete_request&token=<?php echo $token; ?>';

    var filter_name = $(' input[name=\'filter_name\']').val();

    if (filter_name) {
      url += '&filter_name=' + encodeURIComponent(filter_name);
    }

    var filter_email = $(' input[name=\'filter_email\']').val();

    if (filter_email) {
      url += '&filter_email=' + encodeURIComponent(filter_email);
    }

    var filter_date_complete = $(' input[name=\'filter_date_complete\']').val();

    if (filter_date_complete) {
      url += '&filter_date_complete=' + encodeURIComponent(filter_date_complete);
    }

    location = url;
  });

  $('.date').datetimepicker({
  	pickTime: false
  });

  $('input[name=\'filter_name\']').autocomplete({
  	'source': function(request, response) {
  		$.ajax({
  			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
  			dataType: 'json',
  			success: function(json) {
  				response($.map(json, function(item) {
  					return {
  						label: item['name'],
  						value: item['product_id']
  					}
  				}));
  			}
  		});
  	},
  	'select': function(item) {
  		$('input[name=\'filter_name\']').val(item['label']);
  	}
  });

  </script>
