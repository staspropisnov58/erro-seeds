<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-product_name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-product_name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
                  <div class="input-group date">
                    <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-deleted"><?php echo $entry_date_deleted; ?></label>
                  <div class="input-group date">
                    <input type="text" name="filter_date_deleted" value="<?php echo $filter_date_deleted; ?>" placeholder="<?php echo $entry_date_deleted; ?>" data-date-format="YYYY-MM-DD" id="input-date-deleted" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-expired"><?php echo $entry_date_expired; ?></label>
                  <div class="input-group date">
                    <input type="text" name="filter_date_expired" value="<?php echo $filter_date_expired; ?>" placeholder="<?php echo $entry_date_expired; ?>" data-date-format="YYYY-MM-DD" id="input-date-expired" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
              </div>
            </div>



            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
          </div>
        </div>
          <div class="tab-content">
            <form action method="post" enctype="multipart/form-data" id="form-product">

              <div class="table-responsive">

                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php if ($sort == 'nrd.customer_id') { ?>
                        <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'nrd.email') { ?>
                        <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'nrd.date_deleted') { ?>
                        <a href="<?php echo $sort_date_deleted; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_deleted; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_date_deleted; ?>"><?php echo $column_date_deleted; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'nrd.date_added') { ?>
                        <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'nrd.date_expired') { ?>
                        <a href="<?php echo $sort_date_expired; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_expired; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_date_expired; ?>"><?php echo $column_date_expired; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'product_name') { ?>
                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_name; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_name; ?>"><?php echo $column_product_name; ?></a>
                        <?php } ?>
                      </td>
                      <td class="text-left"><?php if ($sort == 'nrd.quantity') { ?>
                        <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                        <?php } ?>
                      </td>
                      <td> <?php echo $column_reason; ?> </td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($requests_data as $request_data){ ?>
                    <tr>
                      <td class="text-left">
                        <?php if($request_data['customer_data']){ ?>
                          <a href="<?php echo $request_data['customer_data']['href'];?>"> <?php echo $request_data['customer_data']['customer_name'];?> </a>
                        <?php }else{ ?>
                          <?php echo $text_guest; ?>
                        <? } ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['email']; ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['date_deleted']; ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['date_added']; ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['date_expired']; ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['product_name']; ?>
                      </td>
                      <td class="text-left">
                        <?php echo $request_data['notification_quantity']; ?>
                      </td>

                      <td class = "text-left">
                        <?php if($request_data['reason']){ ?>
                          <?php  echo $request_data['reason']; ?>
                        <?php } ?>
                      </td>

                    </tr>
                    <? } ?>
                  </tbody>
                </table>

              <div class="row">
                <div data-paginate="active" class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
              </div>
              </div>
            </form>
          </div>
      </div>

    </div>
  </div>

  <script>

  $('#button-filter').on('click', function() {
  	var url = 'index.php?route=request/deleted_request&token=<?php echo $token; ?>';

    var filter_quantity = $(' input[name=\'filter_quantity\']').val();

    if (filter_quantity) {
      url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
    }

    var filter_name = $(' input[name=\'filter_name\']').val();

    if (filter_name) {
      url += '&filter_name=' + encodeURIComponent(filter_name);
    }

    var filter_email = $(' input[name=\'filter_email\']').val();

    if (filter_email) {
      url += '&filter_email=' + encodeURIComponent(filter_email);
    }

    var filter_date_added = $(' input[name=\'filter_date_added\']').val();

    if (filter_date_added) {
      url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
    }

    var filter_date_expired = $(' input[name=\'filter_date_expired\']').val();

    if (filter_date_expired) {
      url += '&filter_date_expired=' + encodeURIComponent(filter_date_expired);
    }

    var filter_date_deleted = $(' input[name=\'filter_date_deleted\']').val();

    if (filter_date_deleted) {
      url += '&filter_date_deleted=' + encodeURIComponent(filter_date_deleted);
    }

    location = url;
  });

  $('.date').datetimepicker({
  	pickTime: false
  });

  $('input[name=\'filter_name\']').autocomplete({
  	'source': function(request, response) {
  		$.ajax({
  			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
  			dataType: 'json',
  			success: function(json) {
  				response($.map(json, function(item) {
  					return {
  						label: item['name'],
  						value: item['product_id']
  					}
  				}));
  			}
  		});
  	},
  	'select': function(item) {
  		$('input[name=\'filter_name\']').val(item['label']);
  	}
  });

  </script>
