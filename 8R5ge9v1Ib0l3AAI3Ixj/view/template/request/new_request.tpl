<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-active" data-toggle="tab"><?php echo $tab_active; ?></a></li>
            <li><a href="#tab-newr" data-toggle="tab"><?php echo $tab_newr; ?></a></li>
            <li><a href="#tab-pre_order" data-toggle="tab">Предзаказ</a></li>
            <li><a href="#tab-overdue" data-toggle="tab"><?php echo $tab_overdue; ?></a></li>
            <li><a href="#tab-notifications" data-toggle="tab"><?php echo $tab_notifications; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-active">
              <div id="active"><?php echo $active; ?></div>
            </div>

            <div class="tab-pane" id="tab-newr">
              <div id="newr"><?php echo $newr; ?></div>
            </div>

              <div class="tab-pane" id="tab-pre_order">
              <div id="newr"><?php echo $pre_order; ?></div>
            </div>

            <div class="tab-pane" id = "tab-overdue">
              <div id="overdue"><?php echo $overdue; ?></div>
            </div>

            <div class="tab-pane" id = "tab-notifications">
              <div id="notifications"><?php echo $notifications; ?></div>
            </div>

          </div>
      </div>

    </div>
  </div>
<script>
  document.querySelectorAll('input[name=select_all]').forEach((selectAll) => {
    selectAll.addEventListener('change', () => {
      document.querySelectorAll('input[name=' + selectAll.value + ']').forEach((checkbox) => {
        checkbox.checked = selectAll.checked;
      });
    });
  });

  function deleteRequest(tab) {
    var deleteIt = [];

    if ($('#reason-' + tab.id).val()) {
      $('#reason-' + tab.id).parent().find('.text-danger').remove();


      $('input[name="' + tab.id + '[]"]:checked').each(function () {
        $(this).parents().each(function () {
          if ($(this).is('tr')) {
            $(this).remove();
            return;
          }
        });
        deleteIt.push($(this).val());
      });

      if (tab.id === 'notifications') {
        url = 'index.php?route=request/new_request/deleteRelatedRequests&token=<?php echo $token;?>&tab=' + tab;
      } else {
        url = 'index.php?route=request/new_request/deleteRequests&token=<?php echo $token;?>&tab=' + tab;
      }

      if (deleteIt) {
        var data = {};
        data.deleteIt = deleteIt;
        data.reason = $('#reason-' + tab.id).val();
      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data,
        beforeSend: function() {
        },
        complete: function() {
        },
        success: function(json) {
          $.each(json, function(tab, value) {
            $('#' + tab).html(value);
          });
        }
      });
      }

    } else {
      $('#reason-' + tab).after('<div class="text-danger">Выберите причину удаления заявки</div>');
    }
  }
</script>
