<div class="table-responsive">

  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <td><input type="checkbox" name="select_all" value="notifications\[\]"></td>
        <td class="text-left" style="width: 250px;">
          <?php echo $column_result; ?>
        </td>
        <td class="text-left">
          <?php echo $column_email; ?>
        </td>
        <td class="text-left">
          <?php echo $column_product_name; ?>
        </td>
      </tr>
    </thead>
    <tbody>
      <?php foreach($notifications_data as $notification_data){ ?>
      <?php foreach ($notification_data as $email => $values){ ?>
      <tr>
        <td class="checkbox_td"> <input type="checkbox" name="notifications[]" value="<?php echo $email; ?>"> </td>
        <td class="text-left result">
        </td>
        <td class="text-left">
          <?php echo $email; ?>
        </td>
        <td class="text-left">
          <?php foreach ($values as $key => $products){ ?>
            <?php foreach ($products as $product){?>
        <p> <?php echo $product['name']; ?> <span  style= "float:right"><?php echo $product['model']; ?> <?php echo $product['sku']; ?></span></p>
          <?php } ?>
        <?php } ?>
        </td>
      </tr>
    <?php } ?>
  <?php } ?>
    </tbody>
  </table>
  <div class="row">
    <div class="col-sm-7 col-md-offset-7 text-left">
      <div class="form-inline">
        <div class="form-group">
          <label class="control-label" for="reason-notifications"><?php echo $entry_reason; ?></label>
            <select name="reason" id="reason-notifications" class="form-control">
            <option value=""></option>
            <option value="invalid_email" selected><?php echo $text_invalid_email; ?></option>
            <option value="product_out_of_sale"><?php echo $text_product_out_of_sale; ?></option>
            <option value="outdated_request"><?php echo $text_outdated_request; ?></option>
        </select>
        </div>
        <button type="button" class="btn btn-danger button-delete" onclick="deleteRequest(notifications);"><i class="fa fa-trash"></i> <?php echo $button_delete; ?></button>
        <button type="button" id="button-send-notification" class="btn btn-primary"><i class="fa fa-envelope"></i> <?php echo $button_make_notification; ?></button>
      </div>
    </div>
  </div>
  <br>

<div class="row">
  <div data-paginate="active" class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
</div>
<style>
.loader,
.loader:before,
.loader:after {
  border-radius: 50%;
  width: 2.5em;
  height: 2.5em;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
  -webkit-animation: load7 1.8s infinite ease-in-out;
  animation: load7 1.8s infinite ease-in-out;
}
.loader {
  color: #333333;
  font-size: 3px;
  position: relative;
  text-indent: -9999em;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation-delay: -0.16s;
  animation-delay: -0.16s;
}
.loader:before,
.loader:after {
  content: '';
  position: absolute;
  top: 0;
}
.loader:before {
  -webkit-animation-delay: -0.32s;
  animation-delay: -0.32s;
}
.loader:after {
  left: 3.5em;
}
@-webkit-keyframes load7 {
  0%,
  80%,
  100% {
    box-shadow: 0 2.5em 0 -1.3em;
  }
  40% {
    box-shadow: 0 2.5em 0 0;
  }
}
@keyframes load7 {
  0%,
  80%,
  100% {
    box-shadow: 0 2.5em 0 -1.3em;
  }
  40% {
    box-shadow: 0 2.5em 0 0;
  }
}

</style>
<script type="text/javascript">
$('#notifications').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#notifications').load(this.href);
});

$('#button-send-notification').on('click', function() {
  var toBeSent = $('input[name="notifications[]"]:checked').length;
  var notifications = [];

  $('input[name="notifications[]"]:checked').each(function () {
    var notification = {};
    notification.email = $(this).val();
    notification.tr = $(this).parent().parent();
    notification.input = this;
    notification.result = $(notification.tr).find('.result');

    notifications.push(notification);
  });

  $('#button-send-notification').button('loading');
  var alert = `<div class="alert alert-info" id="notification-status">Началась отправка уведомлений. Это может занять несколько минут. Пожалуйста, не закрывайте страницу.</div>`;

  $('#notifications').prepend(alert);
  sendNotifications(notifications, toBeSent);
});

function sendNotifications(notifications, toBeSent) {
  sendNotification(notifications[0]).done(function(data){
    var nextNotifications = notifications.slice(1, notifications.length);
    if (nextNotifications.length) {
      sendNotifications(nextNotifications, toBeSent);
    } else {
      $('#button-send-notification').button('reset');

      var notSended = $('input[name="notifications[]"]:checked').length;

      var textReady = `Отправлено ${toBeSent - notSended} из ${toBeSent} уведомлений.`;

      $('#notification-status').html(textReady);
    }
  });
}

function sendNotification(notification) {
  return $.ajax({
    url: 'index.php?route=request/new_request/api&token=<?php echo $token;?>&email=' + notification.email,
    type: 'get',
    dataType: 'json',
    beforeSend: function() {
      $(notification.input).hide();
      $(notification.input).after('<div class="loader"></div>');
    },
    complete: function() {
    }
  })
  .done(function(json) {
    $('div.loader').remove();
    $(notification.input).show();
    if (json.success) {
      $(notification.input).replaceWith('<i class="fa fa-check"></i>');
      $(notification.tr).addClass('success');
      $(notification.result).html('<label class="label label-success">Отправлено</label>');
    } else {
      $(notification.tr).addClass('danger');
      $(notification.result).html('<label class="label label-danger">Ошибка: Такого email не существует</label>');
    }
  })
  .fail(function(xhr, ajaxOptions, thrownError) {
    $('div.loader').remove();
    $(notification.input).show();
    $(notification.tr).addClass('danger');
    $(notification.result).html('<label class="label label-danger">Ошибка сервера. Обратитесь к администратору</label>');
  });
}
</script>
