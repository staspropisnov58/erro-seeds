
<div class="well">
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label" for="input-product_name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-product_name" class="form-control" />
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="filter_email" value="<?php echo $filter_email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
                <div class="input-group date">
                    <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                    <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label" for="input-date-expired"><?php echo $entry_date_expired; ?></label>
                <div class="input-group date">
                    <input type="text" name="filter_date_expired" value="<?php echo $filter_date_expired; ?>" placeholder="<?php echo $entry_date_expired; ?>" data-date-format="YYYY-MM-DD" id="input-date-expired" class="form-control" />
                    <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label class="control-label" for="input-reason"><?php echo $entry_reason; ?></label>
                <select name = "filter_reason" id = "input-reason" class="form-control">
                    <option value=""></option>
                    <?php if($filter_reason == 'p.quantity <= 0'){ ?>
                    <option selected value = "p.quantity <= 0"><?php echo $text_out_of_stock; ?></option>
                    <?php }else{ ?>
                    <option value = "p.quantity <= 0"><?php echo $text_out_of_stock; ?></option>
                    <?php } ?>

                    <?php if($filter_reason == 'p.quantity < nr.quantity'){ ?>
                    <option selected value = "p.quantity < nr.quantity"><?php echo $text_insufficient_amount; ?></option>
                    <?php }else{ ?>
                    <option value = "p.quantity < nr.quantity"><?php echo $text_insufficient_amount; ?></option>
                    <?php } ?>

                    <?php if($filter_reason == 'p.quantity >= nr.quantity'){ ?>
                    <option selected value = "p.quantity >= nr.quantity"><?php echo $text_product_disabled; ?></option>
                    <?php }else{ ?>
                    <option value = "p.quantity >= nr.quantity"><?php echo $text_product_disabled; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>


        <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>

    </div>
</div>

<div class="table-responsive">

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <td><input type="checkbox" name="select_all" value="newr\[\]"></td>
            <td class="text-left"><?php if ($sort == 'nr.customer_id') { ?>
                <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                <?php } ?>
            </td>
            <td class="text-left"><?php if ($sort == 'nr.email') { ?>
                <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                <?php } ?>
            </td>
        <!--    <td class="text-left"><?php if ($sort == 'nr.telephone') { ?>
                <a href="<?php echo $sort_telephone; ?>" class="<?php echo strtolower($order); ?>">Телефон</a>
                <?php } else { ?>
                <a href="<?php echo $sort_telephone; ?>">Телефон</a>
                <?php } ?>
            </td>   -->
            <td class="text-left">
                <p >Телефон</p>

            </td>
            <td class="text-left"><?php if ($sort == 'nr.date_added') { ?>
                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                <?php } ?>
            </td>
            <td class="text-left"><?php if ($sort == 'nr.date_expired') { ?>
                <a href="<?php echo $sort_date_expired; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_expired; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_expired; ?>"><?php echo $column_date_expired; ?></a>
                <?php } ?>
            </td>
            <td class="text-left"><?php if ($sort == 'product_name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_product_name; ?></a>
                <?php } ?>
            </td>
            <td class="text-left"><?php if ($sort == 'nr.quantity') { ?>
                <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                <?php } ?>
            </td>
            <td class="text-left">
                <?php echo $column_reason; ?>
            </td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($requests_data as $request_data){ ?>
        <tr>
            <td class="checkbox_td"> <input type="checkbox" name="newr[]" value="<?php echo $request_data['request_id']; ?>"> </td>
            <td class="text-left">
                <?php if($request_data['customer_data']){ ?>
                <a href="<?php echo $request_data['customer_data']['href'];?>"> <?php echo $request_data['customer_data']['customer_name'];?> </a>
                <?php }else{ ?>
                <?php echo $text_guest; ?>
                <? } ?>
            </td>
            <td class="text-left">
                <?php echo $request_data['email']; ?>
            </td>
            <td class="text-left">
                <?php echo $request_data['telephone']; ?>
            </td>
            <td class="text-left">
                <?php echo $request_data['date_added']; ?>
            </td>
            <td class="text-left">
                <?php echo $request_data['date_expired']; ?>
            </td>
            <td class="text-left">
                <?php echo $request_data['product_name']; ?>
            </td>
            <td class="text-left">
                <?php echo $request_data['notification_quantity']; ?>
            </td>
            <td class="text-left">
                <?php echo $request_data['reason']; ?>
            </td>
        </tr>
        <? } ?>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-7 col-md-offset-7 text-left">
            <div class="form-inline">
                <div class="form-group">
                    <label class="control-label" for="reason-new"><?php echo $entry_reason; ?></label>
                    <select name="reason" id="reason-newr" class="form-control">
                        <option value=""></option>
                        <option value="invalid_email"><?php echo $text_invalid_email; ?></option>
                        <option value="product_out_of_sale" selected><?php echo $text_product_out_of_sale; ?></option>
                        <option value="outdated_request"><?php echo $text_outdated_request; ?></option>
                    </select>
                </div>
                <button type="button" class="btn btn-danger button-delete" onclick="deleteRequest(newr);"><i class="fa fa-trash"></i> <?php echo $button_delete; ?></button>
            </div>
        </div>
    </div>
    <div class="row">
        <div data-paginate="newr" class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
    </div>
</div>

<script type="text/javascript"><!--

        $('#newr input[name=\'filter_name\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                $('#newr input[name=\'filter_name\']').val(item['label']);
            }
        });

    $('#newr').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        $('#newr').load(this.href);
    });

    $('#newr').delegate('thead a', 'click', function(e) {
        e.preventDefault();

        $('#newr').load(this.href);
    });

    $('#newr #button-filter').on('click', function() {
        var url = 'index.php?route=request/new_request/paginate&tab=newr&token=<?php echo $token; ?>';

        var filter_quantity = $('#newr input[name=\'filter_quantity\']').val();

        if (filter_quantity) {
            url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
        }

        var filter_name = $('#newr input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        var filter_email = $('#newr input[name=\'filter_email\']').val();

        if (filter_email) {
            url += '&filter_email=' + encodeURIComponent(filter_email);
        }

        var filter_date_added = $('#newr input[name=\'filter_date_added\']').val();

        if (filter_date_added) {
            url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
        }

        var filter_date_expired = $('#newr input[name=\'filter_date_expired\']').val();

        if (filter_date_expired) {
            url += '&filter_date_expired=' + encodeURIComponent(filter_date_expired);
        }

        var filter_reason = $('#newr select[name=\'filter_reason\']').val();

        if (filter_reason ) {
            url += '&filter_reason=' + encodeURIComponent(filter_reason);
        }

        $('#newr').load(url);
    });

    $('.date').datetimepicker({
        pickTime: false
    });

</script>
