<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-setting-register" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-shipping" data-toggle="tab"><?php echo $tab_shipping; ?></a></li>
            <li><a href="#tab-payment" data-toggle="tab"><?php echo $tab_payment; ?></a></li>
          </ul>
          <div class="tab-content">

            <div class="tab-pane active in" id="tab-general">

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $text_checkout_fast_order_status; ?></label>
                <div class="col-sm-10">
                  <select name="checkout_fast_order_status" id="input-status" class="form-control">
                    <?php if ($checkout_fast_order_status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><span data-toggle="tooltip" title="<?php echo $help_no_callback; ?>"><?php echo $entry_no_callback; ?></span></label>
                <div class="col-sm-10">
                  <select name="checkout_no_callback" id="input-status" class="form-control">
                    <?php if ($checkout_no_callback) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_min_order; ?></label>
                <div class="col-sm-10">
                    <div class="input-group">
                      <input type="text" name="checkout_min_order" value="<?php echo $checkout_min_order; ?>" placeholder="" class="form-control" />
                      <?php if (isset($error_min_order)) { ?>
                      <div class="text-danger"><?php echo $error_min_order; ?></div>
                      <?php } ?>
                    </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-process-status"><span data-toggle="tooltip" title="<?php echo $help_fields; ?>"><?php echo $entry_checkout_fields; ?></span></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($checkout_fields as $field) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($field['name'], $checkout_guest_order_fields)) { ?>
                        <input type="checkbox" name="checkout_guest_order_fields[]" value="<?php echo $field['name']; ?>" checked="checked" />
                        <?php echo $field['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="checkout_guest_order_fields[]" value="<?php echo $field['name']; ?>" />
                        <?php echo $field['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>

            </div>
        <div class="tab-pane fade" id="tab-shipping">
          <?php if ($extensions) { ?>
          <?php foreach ($extensions as $extension) { ?>
            <fieldset>

              <legend><?php echo $extension['name']; ?></legend>

              <div class="tab-content">
                <div class="tab-pane active in" id="tab-shipping">
                  <ul class="nav nav-tabs" id="<?php echo $extension['code']; ?>-field">
                    <?php foreach ($fields as $field) { ?>
                      <?php if (isset($error_shipping['checkout_'.$extension['code'].'_fields'][$field['Field']])){ ?>
                        <li><a href="#<?php echo $extension['code']; ?>-field<?php echo $field['Field']; ?>" data-toggle="tab"><p class="text-danger"><?php echo $field['Field']; ?></p></a></li>
                      <?php }else{ ?>
                      <li><a href="#<?php echo $extension['code']; ?>-field<?php echo $field['Field']; ?>" data-toggle="tab"><?php echo $field['Field']; ?></a></li>
                    <?php } ?>
                    <?php } ?>
                  </ul>
                  <div class="tab-content">
                    <?php foreach ($fields as $field) { ?>
                      <div class="tab-pane" id="<?php echo $extension['code']; ?>-field<?php echo $field['Field']; ?>">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_label; ?></label>
                    <div class="col-sm-10">
                      <?php foreach($languages as $language){ ?>

                        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][label][<?php echo $language['language_id']; ?>]" value="<?php if(isset($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['label'][$language['language_id']])){ echo $shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['label'][$language['language_id']];} ?>" placeholder="" class="form-control" />
                          <?php if (isset($error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['label'][$language['language_id']])) { ?>
                          <div class="text-danger"><?php echo $error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['label'][$language['language_id']]; ?></div>
                          <?php } ?>
                        </div>
                      <?php } ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"> <?php echo $entry_placeholder; ?></label>
                    <div class="col-sm-10">
                      <?php foreach($languages as $language){ ?>
                        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][placeholder][<?php echo $language['language_id']; ?>]" value="<?php if(isset($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['placeholder'][$language['language_id']])){ echo $shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['placeholder'][$language['language_id']];} ?>" placeholder="" class="form-control" />
                          <?php if (isset($error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['placeholder'][$language['language_id']])) { ?>
                          <div class="text-danger"><?php echo $error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['placeholder'][$language['language_id']]; ?></div>
                          <?php } ?>
                        </div>
                      <?php } ?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"> <?php echo $entry_helper; ?></label>
                    <div class="col-sm-10">
                      <?php foreach($languages as $language){ ?>
                        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][helper][<?php echo $language['language_id']; ?>]" value="<?php if(isset($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['helper'][$language['language_id']])){ echo $shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['helper'][$language['language_id']];} ?>" placeholder="" class="form-control" />
                          <?php if (isset($error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['helper'][$language['language_id']])) { ?>
                          <div class="text-danger"><?php echo $error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['helper'][$language['language_id']]; ?></div>
                          <?php } ?>
                        </div>
                      <?php } ?>
                    </div>
                  </div>


                  <?php if($field['Field'] == 'country_id'){ ?>
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="shippings[checkout_<?php echo $extension['code'];?>_fields][<?php echo $field['Field']; ?>][value]"><?php echo $entry_country; ?></label>
                      <div class="col-sm-10">
                        <input type="text" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value]" onfocus="completeCountry('checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value','<?php echo $extension['code'];?>')" value="" placeholder="" id="input-country" class="form-control" />
                        <div id="country-<?php echo $extension['code'];?>" class="well well-sm" style="height: 150px; overflow: auto;">
                           <?php if($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'] != ''){ ?>
                            <?php  foreach ($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'] as $key => $product_category) { ?>
                              <div id="country-<?php echo $extension['code']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $key; ?>
                                <input type="hidden" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value][]" value="<?php echo $product_category; ?>" />
                              </div>
                            <?php  } ?>
                        <?php } ?>
                        </div>
                      </div>
                    </div>
                  <?php }elseif($field['Field'] == 'zone_id'){ ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value]"><?php echo $entry_zone; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value]" onfocus="completeZone('checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value','<?php echo $extension['code'];?>')" value="" placeholder="" id="input-zone" class="form-control" />
                          <div id="zone-<?php echo $extension['code'];?>" class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php if($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'] !== ''){ ?>
                              <?php  foreach ($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'] as $key => $product_category) { ?>
                                <div id="zone-<?php echo $extension['code']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $key; ?>
                                  <input type="hidden" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value][]" value="<?php echo $product_category; ?>" />
                                </div>
                              <?php } ?>
                          <?php } ?>
                          </div>
                        </div>
                      </div>
                    <?php }else{ ?>
                    <div class="form-group">
                    <label class="col-sm-2 control-label"> <?php echo $entry_value; ?></label>
                    <div class="col-sm-10">
                      <?php foreach($languages as $language){ ?>
                        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][value][<?php echo $language['language_id']; ?>]" value="<?php if(isset($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'][$language['language_id']])){ echo $shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'][$language['language_id']];} ?>" placeholder="" class="form-control" />
                          <?php if (isset($error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'][$language['language_id']])) { ?>
                          <div class="text-danger"><?php echo $error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['value'][$language['language_id']]; ?></div>
                          <?php } ?>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                <?php } ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"> <?php echo $entry_sorting; ?></label>
                    <div class="col-sm-10">
                        <div class="input-group">
                          <input type="text" name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][sort_order]" value="<?php if(isset($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['sort_order'])){ echo $shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['sort_order'];} ?>" placeholder="" class="form-control" />
                          <?php if (isset($error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['sort_order'])) { ?>
                          <div class="text-danger"><?php echo $error_shipping['checkout_' . $extension['code'] . '_fields'][$field['Field']]['sort_order']; ?></div>
                          <?php } ?>
                        </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-type"><?php echo $entry_type; ?></label>
                    <div class="col-sm-10">
                      <select name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][type]" id="input-type" class="form-control">
                        <?php foreach ($field_types as $type_group) { ?>
                          <optgroup label="<?php echo $type_group['text']; ?>">
                            <?php foreach ($type_group['types'] as $type => $label) { ?>
                              <?php if ($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['type'] === $type) { ?>
                              <option value="<?php echo $type; ?>" selected="selected"><?php echo $label; ?></option>
                              <?php } else { ?>
                              <option value="<?php echo $type; ?>"><?php echo $label; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </optgroup>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"> <?php echo $entry_status; ?></label>
                    <div class="col-sm-10">
                      <select name="checkout_<?php echo $extension['code'];?>_fields[<?php echo $field['Field']; ?>][status]" id="input-status" class="form-control">
                        <?php if ($shippings['checkout_' . $extension['code'] . '_fields'][$field['Field']]['status']) { ?>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                  <?php } ?>
                </div>
              </div>
              </div>
              <script type="text/javascript"><!--
                $('#<?php echo $extension['code']; ?>-field a:first').tab('show');
              </script>
            </fieldset>
          <?php } ?>
        <?php } ?>




      </div>

        <div class="tab-pane fade" id="tab-payment">
        <?php if($payments){ ?>
          <?php  foreach($payments as $payment){ ?>
              <fieldset>
                <legend><?php echo $payment['name']; ?></legend>
                <div class="tab-content">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"> <?php echo $entry_min_order; ?></label>
                    <div class="col-sm-10">
                        <div class="input-group">
                          <input type="text" name="checkout_<?php echo $payment['code'];?>_min_order" value="<?php if(isset($payments_data['checkout_' . $payment['code'] . '_min_order'])){ echo $payments_data['checkout_' . $payment['code'] . '_min_order'];} ?>" placeholder="" class="form-control" />
                          <?php if (isset($error_payment['checkout_' . $payment['code'] . '_min_order'])) { ?>
                          <div class="text-danger"><?php echo $error_payment['checkout_' . $payment['code'] . '_min_order']; ?></div>
                          <?php } ?>
                        </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-shipping"><span data-toggle="tooltip" title="<?php echo $help_shipping; ?>"><?php echo $entry_shipping; ?></span></label>
                    <div class="col-sm-10">
                      <input type="text" name="checkout_<?php echo $payment['code'];?>_shippings" onfocus="completeShippings('checkout_<?php echo $payment['code'];?>_shippings', '<?php echo $payment['code']; ?>')" value="" placeholder="" id="<?php echo $payment['code'];?>-shippings" class="form-control" />
                      <div id="shipping-<?php echo $payment['code']; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                        <?php  foreach ($payments_data['checkout_' . $payment['code'].'_shippings'] as $key => $payment_data) { ?>
                        <div id="shipping-<?php echo $payment['code']; ?>"><i class="fa fa-minus-circle"></i>
                          <?php echo $key; ?>
                          <input type="hidden" name="checkout_<?php echo $payment['code'];?>_shippings[]" value="<?php echo $payment_data; ?>" />
                        </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>

              </fieldset>
          <?php } ?>
        <?php } ?>

        </div>


      </div>


        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--//
function completeShippings(name , payment_code){
  $('input[name="' + name + '"]').autocomplete({
  	'source': function(request, response) {
  		$.ajax({
  			url: 'index.php?route=extension/shipping/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
  			dataType: 'json',
  			success: function(json) {
  				response($.map(json, function(item) {
  					return {
  						label: item['name'],
  						value: item['code']
  					}
  				}));
  			}
  		});
  	},
  	'select': function(item) {
      $('input[name="' + name + '"]').val('');

  		$('#shipping-' + payment_code + item['value']).remove();
  		$('#shipping-' + payment_code).append('<div id="shipping-'+ payment_code + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="' + name + '[]" value="' + item['value'] + '" /></div>');
  	}
  });

  $('#shipping-'+ payment_code).delegate('.fa-minus-circle', 'click', function() {
  	$(this).parent().remove();
  });
}

$('#content').delegate('.fa-minus-circle', 'click', function() {
  $(this).parent().remove();
});
-->
</script>

<script type="text/javascript"><!--
function completeCountry(name, extension_code){
  $('input[name="' + name + ']"]').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=localisation/country/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        json.unshift({
          category_id: 0,
          name: '<?php echo $text_none; ?>'
        });

        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['country_id']
          }
        }));
      }
    });
  },
	'select': function(item) {
		$('input[name="' + name + ']"]').val('');

		$('#country-'+ extension_code + item['value']).remove();

		$('#country-'+ extension_code).append('<div id="country-' + extension_code + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="' + name + '][]" value="' + item['value'] +'"/></div>');
	}
});

$('#country-'+ extension_code).delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
}
</script>

<script type="text/javascript"><!--
function completeZone(name, extension_code){
  var countries = '';
  $('#country-' + extension_code + ' input[type=hidden]').map(function(){
    countries += '&country_id[]=' + $(this).val()
  });

  $('input[name="' + name + ']"]').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=localisation/zone/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request) + countries,
      dataType: 'json',
      success: function(json) {
        json.unshift({
          category_id: 0,
          name: '<?php echo $text_none; ?>'
        });

        response($.map(json, function(item) {
          return {
            label: item['name'],
            value: item['zone_id']
          }
        }));
      }
    });
  },
	'select': function(item) {
		$('input[name="' + name + ']"]').val('');

		$('#zone-'+ extension_code + item['value']).remove();

		$('#zone-'+ extension_code).append('<div id="zone-' + extension_code + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name=' + name + '][]" value="' + item['value'] +'"/></div>');
	}
});

$('#zone-'+ extension_code).delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
}
</script>
<?php echo $footer; ?>
