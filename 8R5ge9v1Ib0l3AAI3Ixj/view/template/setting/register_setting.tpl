<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-setting-register" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-attribute" class="form-horizontal">
  <?php foreach($register_settings as $key => $register_setting){ ?>
  <fieldset>
    <legend><?php echo $key; ?></legend>
    <div class="form-group required">
      <label class="col-sm-2 control-label"><?php echo $entry_label; ?></label>
      <div class="col-sm-10">
        <?php foreach($languages as $language){ ?>
          <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
            <input type="text" name="<?php echo $key; ?>[label][<?php echo $language['language_id']; ?>]" value="<?php if(isset($register_settings[$key]['label'][$language['language_id']])){ echo $register_settings[$key]['label'][$language['language_id']];} ?>" placeholder="" class="form-control" />
            <?php if (isset($error_label[$key]['label'][$language['language_id']])) { ?>
            <div class="text-danger"><?php echo $error_label[$key]['label'][$language['language_id']]; ?></div>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    </div>

    <div class="form-group required">
      <label class="col-sm-2 control-label"> <?php echo $entry_placeholder; ?></label>
      <div class="col-sm-10">
        <?php foreach($languages as $language){ ?>
          <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
            <input type="text" name="<?php echo $key; ?>[placeholder][<?php echo $language['language_id']; ?>]" value="<?php if(isset($register_settings[$key]['placeholder'][$language['language_id']])){ echo $register_settings[$key]['placeholder'][$language['language_id']];} ?>" placeholder="" class="form-control" />
            <?php if (isset($error_label[$key]['placeholder'][$language['language_id']])) { ?>
            <div class="text-danger"><?php echo $error_label[$key]['placeholder'][$language['language_id']]; ?></div>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label"> <?php echo $entry_sorting; ?></label>
      <div class="col-sm-10">
          <div class="input-group">
            <input type="text" name="<?php echo $key; ?>[sort_order]" value="<?php if(isset($register_settings[$key]['sort_order'])){ echo $register_settings[$key]['sort_order'];} ?>" placeholder="" class="form-control" />
            <?php if (isset($error_sort_order[$key]['sort_order'])) { ?>
            <div class="text-danger"><?php echo $error_sort_order[$key]['sort_order']; ?></div>
            <?php } ?>
          </div>
      </div>
    </div>
    <?php if($key == 'email' || $key == 'password'){ ?>
    <? }else{ ?>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-status"> <?php echo $entry_status; ?></label>
      <div class="col-sm-10">
        <select name="<?php echo $key; ?>[status]" id="input-status" class="form-control">
          <?php if ($register_setting['status']) { ?>
          <option value="1" selected="selected"> <?php echo $text_enabled; ?></option>
          <option value="0"><?php echo $text_disabled; ?></option>
          <?php } else { ?>
          <option value="1"><?php echo $text_enabled; ?></option>
          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
<?php } ?>

<?php if($key != 'password'){ ?>
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-type"><?php echo $entry_type; ?></label>
    <div class="col-sm-10">
      <select name="type" id="input-type" class="form-control">
        <optgroup label="<?php echo $text_choose; ?>">
        <?php if ($register_setting['type'] == 'select') { ?>
        <option value="select" selected="selected"><?php echo $text_select; ?></option>
        <?php } else { ?>
        <option value="select"><?php echo $text_select; ?></option>
        <?php } ?>
        <?php if ($register_setting['type'] == 'radio') { ?>
        <option value="radio" selected="selected"><?php echo $text_radio; ?></option>
        <?php } else { ?>
        <option value="radio"><?php echo $text_radio; ?></option>
        <?php } ?>
        <?php if ($register_setting['type'] == 'checkbox') { ?>
        <option value="checkbox" selected="selected"><?php echo $text_checkbox; ?></option>
        <?php } else { ?>
        <option value="checkbox"><?php echo $text_checkbox; ?></option>
        <?php } ?>
        </optgroup>
        <optgroup label="<?php echo $text_input; ?>">
        <?php if ($register_setting['type'] == 'text') { ?>
        <option value="text" selected="selected"><?php echo $text_text; ?></option>
        <?php } else { ?>
        <option value="text"><?php echo $text_text; ?></option>
        <?php } ?>
        <?php if ($register_setting['type'] == 'tel') { ?>
        <option value="tel" selected="selected"><?php echo $text_tel; ?></option>
        <?php } else { ?>
        <option value="tel"><?php echo $text_tel; ?></option>
        <?php } ?>
        <?php if ($register_setting['type'] == 'textarea') { ?>
        <option value="textarea" selected="selected"><?php echo $text_textarea; ?></option>
        <?php } else { ?>
        <option value="textarea"><?php echo $text_textarea; ?></option>
        <?php } ?>
        </optgroup>
        <optgroup label="<?php echo $text_file; ?>">
        <?php if ($register_setting['type'] == 'file') { ?>
        <option value="file" selected="selected"><?php echo $text_file; ?></option>
        <?php } else { ?>
        <option value="file"><?php echo $text_file; ?></option>
        <?php } ?>
        </optgroup>
        <optgroup label="<?php echo $text_date; ?>">
        <?php if ($register_setting['type'] == 'date') { ?>
        <option value="date" selected="selected"><?php echo $text_date; ?></option>
        <?php } else { ?>
        <option value="date"><?php echo $text_date; ?></option>
        <?php } ?>
        <?php if ($register_setting['type'] == 'time') { ?>
        <option value="time" selected="selected"><?php echo $text_time; ?></option>
        <?php } else { ?>
        <option value="time"><?php echo $text_time; ?></option>
        <?php } ?>

        <?php if ($register_setting['type'] == 'datetime') { ?>
        <option value="datetime" selected="selected"><?php echo $text_datetime; ?></option>
        <?php } else { ?>
        <option value="datetime"><?php echo $text_datetime; ?></option>
        <?php } ?>
        </optgroup>
      </select>
    </div>
  </div>
<?php } ?>

    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-editable"> <?php echo $entry_editable; ?></label>
      <div class="col-sm-10">
        <select name="<?php echo $key; ?>[editable]" id="input-editable" class="form-control">
          <?php if ($register_setting['editable']) { ?>
          <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
          <option value="0"><?php echo $text_disabled; ?></option>
          <?php } else { ?>
          <option value="1"><?php echo $text_enabled; ?></option>
          <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <?php if($key == 'telephone'){ ?>
      <?php if($tel_code == 1){}else{ ?>

      <div class="form-group">
        <label class="col-sm-2 control-label" for="input-tel-code"> <?php echo $entry_tel_code; ?></label>
        <div class="col-sm-10">
          <?php if($tel_code == 1){ ?>
            <input type="checkbox" checked name = 'enebled_tel_code' value="1" class="form-check-input" id="input-tel-code">
          <?php }else{ ?>
            <input type="checkbox" name = 'enebled_tel_code' value="1" class="form-check-input" id="input-tel-code">
          <?php } ?>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
  </fieldset>
<?php } ?>
</form>
</div>
</div>
</div>
</div>









<?php echo $footer; ?>
