<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-redirects" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="success">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-redirects" class="form-horizontal">
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <td class="text-left"><?php echo $entry_from; ?></td>
                  <td class="text-left"><?php echo $entry_to; ?></td>
                  <td class="text-right"><?php echo $entry_status_code; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($redirects) { ?>
                <?php foreach ($redirects as $redirect) { ?>
                  <?php if (isset($errors[$redirect['redirect_id']])) { ?>
                    <tr id="error-<?php echo $redirect['redirect_id']; ?>">
                      <td colspan="3"><div class="text-danger"><?php echo $errors[$redirect['redirect_id']]; ?></div></td>
                    </tr>
                  <?php } ?>
                <tr id="redirect-<?php echo $redirect['redirect_id']; ?>">
                  <td class="text-left">
                    <input type="text" name="redirects[<?php echo $redirect['redirect_id']; ?>][redirect_from]" value="<?php echo $redirect['redirect_from']; ?>" placeholder="<?php echo $entry_from; ?>" class="form-control" />
                  </td>
                  <td class="text-left">
                    <input type="text" name="redirects[<?php echo $redirect['redirect_id']; ?>][redirect_to]" value="<?php echo $redirect['redirect_to']; ?>" placeholder="<?php echo $entry_to; ?>" class="form-control" />
                  </td>
                  <td class="text-right">
                    <select name="redirects[<?php echo $redirect['redirect_id']; ?>][status_code]" class="form-control">
                      <option value="301" <?php if ($redirect['status_code'] === '301') { ?> selected="selected" <?php } ?>>301 permanent</option>
                      <option value="302" <?php if ($redirect['status_code'] === '302') { ?> selected="selected" <?php } ?>>302 temporary</option>
                    </select>
                  </td>
                  <td class="text-left">
                    <button type="button" onclick="deleteRedirect(<?php echo $redirect['redirect_id']; ?>)" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr id="redirect-1">
                  <td class="text-left">
                    <input type="text" name="redirects[1][redirect_from]" value="" placeholder="<?php echo $entry_from; ?>" class="form-control" />
                  </td>
                  <td class="text-left">
                    <input type="text" name="redirects[1][redirect_to]" value="" placeholder="<?php echo $entry_to; ?>" class="form-control" />
                  </td>
                  <td class="text-right">
                    <select name="redirects[1][status_code]" class="form-control">
                      <option value="301" selected="selected">301 permanent</option>
                      <option value="302">302 temporary</option>
                    </select>
                  </td>
                  <td class="text-left">
                    <button type="button" onclick="$('#redirect-1').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="text-left"><button type="button" onclick="addRedirect();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
              </tr>
            </tfoot>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
function addRedirect() {
  var row = $('tbody tr').last().attr('id').split('-');
  html  = '<tr id="redirect-' + row[1] + '">';
  html += '<td class="text-left"><input type="text" name="redirects[' + row[1] + '][redirect_from]" value="" placeholder="<?php echo $entry_from; ?>" class="form-control" /></td>';
  html += '<td class="text-left"><input type="text" name="redirects[' + row[1] + '][redirect_to]" value="" placeholder="<?php echo $entry_to; ?>" class="form-control" /></td>';
  html += '<td class="text-right"><select name="redirects[' + row[1] + '][status_code]" class="form-control"><option value="301" selected="selected">301 permanent</option><option value="302">302 temporary</option></select></td>';
  html += '<td class="text-left"><button type="button" onclick="$(\'#redirect-' + row[1] + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
  html += '</tr>';

  $('tbody').append(html);
}
</script>
<?php echo $footer; ?>
