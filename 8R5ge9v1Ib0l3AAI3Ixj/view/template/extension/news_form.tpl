<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-news" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-news" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <?php if(isset($opengraph_use_meta_when_no_og)){ ?>
              <?php if($opengraph_status){ ?>
              <li><a href="#tab-opengraph" data-toggle="tab"><?php echo $tab_open_graph; ?></a></li>
              <?php } ?>
            <?php } ?>
          </ul>
          <div class="tab-content">

            <div class="tab-pane active in" id="tab-general">

          <ul class="nav nav-tabs" id="language">
			<?php foreach ($languages as $language) { ?>
			<li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
			<?php } ?>
		  </ul>
      <div class="tab-content">

			<?php foreach ($languages as $language) { ?>
			<div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
			  <div class="form-group required">
				<label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $text_title; ?></label>
				<div class="col-sm-10">
				  <input type="text" name="news[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($news[$language['language_id']]) ? $news[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $text_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
				</div>
			  </div>
			  <div class="form-group required">
				<label class="col-sm-2 control-label" for="input-short<?php echo $language['language_id']; ?>"><?php echo $text_short_description; ?></label>
				<div class="col-sm-10">
				  <textarea name="news[<?php echo $language['language_id']; ?>][short_description]" rows="5" placeholder="<?php echo $text_short_description; ?>" id="input-short<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($news[$language['language_id']]) ? $news[$language['language_id']]['short_description'] : ''; ?></textarea>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $text_description; ?></label>
				<div class="col-sm-10">
				  <textarea name="news[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $text_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($news[$language['language_id']]) ? $news[$language['language_id']]['description'] : ''; ?></textarea>
				</div>
			  </div>


        <div class="form-group">
        <label class="col-sm-2 control-label" for="input-image<?php echo $language['language_id']; ?>"><?php echo $text_image; ?></label>
        <div class="col-sm-10">
          <a href="" id="thumb-image<?php echo $language['language_id']; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $news[$language['language_id']]['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $news[$language['language_id']]['thumb']; ?>" /></a>
          <input type="hidden" name="news[<?php echo $language['language_id']; ?>][image]" value="<?php echo $news[$language['language_id']]['image']; ?>" id="input-image<?php echo $language['language_id']; ?>" />
        </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image<?php echo $language['language_id']; ?>"><?php echo $text_image_2; ?></label>
            <div class="col-sm-10">
                <a href="" id="thumb-image_2<?php echo $language['language_id']; ?>" data-toggle="image_2" class="img-thumbnail"><img src="<?php echo $news[$language['language_id']]['thumb_2']; ?>" alt="" title="" data-placeholder="<?php echo $news[$language['language_id']]['thumb_2']; ?>" /></a>
                <input type="hidden" name="news[<?php echo $language['language_id']; ?>][image_2]" value="<?php echo $news[$language['language_id']]['image_2']; ?>" id="input-image_2<?php echo $language['language_id']; ?>" />
            </div>
        </div>
        <div class="form-group">
             <label class="col-sm-2 control-label" for="input-meta_title"><?php echo $text_meta_title; ?></label>
                <div class="col-sm-10">
                     <input type="text" name="news[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo $news[$language['language_id']]['meta_title']; ?>" placeholder="<?php echo $news[$language['language_id']]['meta_title']; ?>" id="input-meta_title" class="form-control" />
                </div>
        </div>
        <div class="form-group">
             <label class="col-sm-2 control-label" for="input-meta-description"><?php echo $text_meta_description; ?></label>
                <div class="col-sm-10">
                     <input type="text" name="news[<?php echo $language['language_id']; ?>][meta_description]" value="<?php echo $news[$language['language_id']]['meta_description']; ?>" placeholder="<?php echo $news[$language['language_id']]['meta_description']; ?>" id="input-meta_description" class="form-control" />
                </div>
        </div>
        <div class="form-group">
             <label class="col-sm-2 control-label" for="input-meta_keyword"><?php echo $text_meta_keyword; ?></label>
                <div class="col-sm-10">
                     <input type="text" name="news[<?php echo $language['language_id']; ?>][meta_keyword]" value="<?php echo $news[$language['language_id']]['meta_keyword']; ?>" placeholder="<?php echo $news[$language['language_id']]['meta_keyword']; ?>" id="input-meta_keyword" class="form-control" />
                </div>
        </div>
      </div>



			<?php } ?>
		  </div>

      <div class="form-group required">
			     <label class="col-sm-2 control-label" for="input-keyword"><?php echo $text_keyword; ?></label>
			     <div class="col-sm-10">
			          <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-keyword" class="form-control" />
                <?php if ($error_meta_robots) { ?>
                <div class="text-danger"><?php echo $error_keyword;?></div>
                <?php } ?>
			     </div>
		  </div>
      <div class="form-group">
        <label class="col-sm-2 control-label" for="input-meta-robots"><span data-toggle="tooltip" title="<?php echo $help_meta_robots; ?>"><?php echo $text_meta_robots; ?></span></label>
        <div class="col-sm-10">
          <input type="text" name="meta_robots" value="<?php echo $meta_robots; ?>" placeholder="<?php echo $text_meta_robots; ?>" id="input-meta-robots" class="form-control" />
          <?php if ($error_meta_robots) { ?>
          <div class="text-danger"><?php echo $error_meta_robots; ?></div>
          <?php } ?>
        </div>
      </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-status"><?php echo $text_status; ?></label>
			<div class="col-sm-10">
			  <select name="status" id="input-status" class="form-control">
				<?php if ($status) { ?>
				<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
				<option value="1"><?php echo $text_enabled; ?></option>
				<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
			  </select>
			</div>
		  </div>
      <?php if($sale_events_status){ ?>
      <div class="form-group">
        <label class="col-sm-2 control-label" for="input-sale-event"><span data-toggle="tooltip" title="<?php echo $help_sale_event; ?>"><?php echo $entry_sale_event; ?></span></label>
        <div class="col-sm-10">
          <input type="text" name="sale_event" value="<?php echo $sale_event['name'] ?>" placeholder="<?php echo $entry_sale_event; ?>" id="input-sale-events" class="form-control" />
          <input type="hidden" name="sale_event_id" value="<?php echo $sale_event['sale_event_id']; ?>" />
          <input type="hidden" name="sale_event_datetime_start" value="<?php echo $sale_event['datetime_start']; ?>" />
          <input type="hidden" name="sale_event_datetime_end" value="<?php echo $sale_event['datetime_end']; ?>" />
        </div>
      </div>
    <?php } ?>
    </div>
    <?php if(isset($opengraph_use_meta_when_no_og)){ ?>
      <?php if($opengraph_status){ ?>
      <div class="tab-pane" id="tab-opengraph">
        <ul class="nav nav-tabs" id="language-opengraph">
          <?php foreach ($languages as $language) { ?>
          <li><a href="#language<?php echo $language['language_id']; ?>-opengraph" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
          <?php } ?>
        </ul>
        <div class="tab-content">
          <?php foreach ($languages as $language) { ?>
          <div class="tab-pane" id="language<?php echo $language['language_id']; ?>-opengraph">
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-opengraph-title<?php echo $language['language_id']; ?>"><?php echo $entry_opengraph_title; ?> </label>
              <div class="col-sm-10">
                <input type="text" name="opengraph[<?php echo $language['language_id']; ?>][title]" value="<?php echo $opengraph[$language['language_id']]['title']; ?>" placeholder="<?php echo $entry_opengraph_title; ?> " id="input-opengraph-title<?php echo $language['language_id']; ?>" class="form-control">
                <?php if (isset($error_opengraph[$language['language_id']]['title'])) { ?>
                <div class="text-danger"><?php echo $error_opengraph[$language['language_id']]['title']; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-opengraph-description<?php echo $language['language_id']; ?>"><?php echo $entry_opengraph_description; ?></label>
              <div class="col-sm-10">
                <input type="text" name="opengraph[<?php echo $language['language_id']; ?>][description]" value="<?php echo $opengraph[$language['language_id']]['description']; ?>" placeholder="<?php echo $entry_opengraph_description; ?> " id="input-opengraph-description<?php echo $language['language_id']; ?>" class="form-control">
                <?php if (isset($error_opengraph[$language['language_id']]['description'])) { ?>
                <div class="text-danger"><?php echo $error_opengraph[$language['language_id']]['description']; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-opengraph-image<?php echo $language['language_id']; ?>"><?php echo $entry_opengraph_image; ?></label>
              <div class="col-sm-10">
                <a href="" id="thumb-opengraph-image<?php echo $language['language_id']?>" data-toggle="image" class="img-thumbnail">
                <img src="<?php echo $opengraph[$language['language_id']]['thumb'] ;?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /> </a>
                <input type="hidden" name="opengraph[<?php echo $language['language_id']; ?>][image]" value="<?php echo $opengraph[$language['language_id']]['image'];?>" id="input-opengraph-image<?php echo $language['language_id']; ?>" />
                <?php if (isset($error_opengraph[$language['language_id']]['image'])) { ?>
                <div class="text-danger"><?php echo $error_opengraph[$language['language_id']]['image']; ?></div>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
  </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$('input[name=\'sale_event\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=marketing/sale_events/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				json.unshift({
					sale_event_id: 0,
					name: '<?php echo $text_none; ?>'
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['sale_event_id'],
            datetime_start: item['datetime_start'],
            datetime_end: item['datetime_end']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'sale_event\']').val(item['label']);
		$('input[name=\'sale_event_id\']').val(item['value']);
    $('input[name=\'sale_event_datetime_start\']').val(item['datetime_start']);
    $('input[name=\'sale_event_datetime_end\']').val(item['datetime_end']);
	}
});

$('#language a:first').tab('show');
$('#language-opengraph a:first').tab('show');


<?php foreach ($languages as $language) { ?>
$('#input-description<?php echo $language['language_id']; ?>').summernote({height: 300});

<?php } ?>
</script>
<?php echo $footer; ?>
