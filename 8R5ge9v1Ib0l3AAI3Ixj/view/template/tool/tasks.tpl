<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-tasks').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">

    <div class="text" id ="notice"></div>

    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-tasks">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>

                  <td class="text-left"><?php if ($sort == 'datetime_run') { ?>
                    <a href="<?php echo $sort_datetime_run; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_datetime_run; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_datetime_run; ?>"><?php echo $entry_datetime_run; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php if ($sort == 'action') { ?>
                    <a href="<?php echo $sort_action; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_action; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_action; ?>"><?php echo $entry_action; ?></a>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $entry_args; ?></td>
                  <td class="text-left"><?php if ($sort == 'frequency') { ?>
                    <a href="<?php echo $sort_frequency; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_periodicity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_frequency; ?>"><?php echo $column_periodicity; ?></a>
                    <?php } ?>
                  </td>
                </tr>
              </thead>
              <tbody>
                <?php if ($tasks) { ?>
                <?php foreach ($tasks as $task) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($task['task_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $task['task_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $task['task_id']; ?>" />
                    <?php } ?></td>

                  <td class="text-right">
                    <?php echo $task['datetime_run']; ?>
                  </td>
                  <td class="text-right">
                    <?php echo $task['action']; ?>
                  </td>
                  <td class="text-right">
                    <?php echo $task['args']; ?>
                  </td>
                  <td><?php echo $task['periodicity']; ?></td>
                  <td class="text-right">
                    <a href="<?php echo $task['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <?php if($show_button_run){ ?>

                      <a id="<?php echo $task['task_id']; ?>" data-toggle="tooltip" data-run="" title="<?php echo $button_run; ?>" class="btn btn-primary js-run"><i class="fa fa-play"></i></a>
                    <?php } ?>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

<?php echo $footer; ?>

<script>

let run = document.querySelectorAll('.js-run');

document.querySelectorAll('.js-run').forEach((btn, i) => {
      btn.addEventListener('click', function(e) {
          run.forEach(element => element.style = "pointer-events: none");

          $.ajax({
            url: 'index.php?route=tool/task_runner/run&token=<?php echo $token; ?>&task_id=' + this.id,
            type: 'get',
            dataType: "json",

            success: function(json){
              if(json == '200'){
                html = '<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $text_success_notice; ?> <button type="button" class="close" data-dismiss="alert">&times;</button> </div>';
              }else if(json == '500'){
                html = '<div class="alert alert-error"><i class="fa fa-exclamation-circle"></i> <?php echo $text_error_notice; ?> <button type="button" class="close" data-dismiss="alert">&times;</button> </div>';
              }
              document.querySelector('#notice').innerHTML = html;

            },
          });
          run.forEach(element => element.style = "pointer-events: auto");

      });
  });
</script>
