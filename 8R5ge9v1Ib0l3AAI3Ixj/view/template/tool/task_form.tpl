<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-attribute" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-attribute" class="form-horizontal">
          <div class="form-group ">
            <label class="col-sm-2 control-label" for="input-datetime"><?php echo $entry_action; ?></label>
            <div class="col-sm-10">
              <input type="text" name="controller_action" value="<?php echo $controller_action; ?>" placeholder="<?php echo $entry_action; ?>" id="input-action" class="form-control" />
              <?php if ($error_action) { ?>
              <div class="text-danger"><?php echo $error_action; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-datetime-run"><?php echo $entry_datetime_run; ?></label>
            <div class="col-sm-3">
              <div class="input-group date">
                <input type="text" readonly name="datetime_run" value="<?php echo $datetime_run; ?>" placeholder="<?php echo $datetime_run; ?>" data-date-format="YYYY-MM-DD HH:mm"   class="form-control" />
                <?php if ($error_datetime_run) { ?>
                  <div class="text-danger text-center"><?php echo $error_datetime_run; ?></div>
                <?php } ?>
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group ">
            <label class="col-sm-2 control-label" for="input-datetime"><?php echo $entry_args; ?></label>
            <div class="col-sm-10">
              <input type="text" readonly name="args" value="<?php echo $args; ?>" placeholder="<?php echo $entry_args; ?>" id="input-args" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-frequency"><?php echo $entry_frequency; ?></label>
            <div class="col-sm-10">
              <select name="frequency" id="input-frequency" class="form-control">
                <option value="" selected="selected" disabled="disabled"><?php echo $text_select_option; ?></option>
                <?php foreach ($periodicity as $key => $value){ ?>
                  <?php if($key == $frequency){ ?>
                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                  <?php }else{ ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                  <?php } ?>
                <?php } ?>
                <?php if (isset($error_frequency)) { ?>
                <div class="text-danger"><?php echo $error_frequency; ?></div>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>

<script>
$('.date').datetimepicker({
  locale: 'ru',
  pick12HourFormat: false,
});
</script>
