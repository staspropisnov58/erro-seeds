<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-new-post" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-new-post" class="form-horizontal">
          <?php foreach ($languages as $language) { ?>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-note<?php echo $language['language_id']; ?>"><?php echo $entry_note; ?></label>
            <div class="col-sm-10">
              <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                <textarea name="cod_note<?php echo $language['language_id']; ?>" cols="80" rows="10" placeholder="<?php echo $entry_note; ?>" id="input-note<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset(${'cod_note' . $language['language_id']}) ? ${'cod_note' . $language['language_id']} : ''; ?></textarea>
              </div>
              <?php if (${'error_note' . $language['language_id']}) { ?>
              <div class="text-danger"><?php echo ${'error_note' . $language['language_id']}; ?></div>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order">Стоимость доставки</label>
            <div class="col-sm-10">
              <input type="text" name="cod_cost" value="<?php echo $cod_cost; ?>" placeholder="Стоимость доставки" id="input-sort-order" class="form-control" />
              <?php if ($error_cost) { ?>
                <div class="text-danger"><?php echo $error_cost; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="cod_status" id="input-status" class="form-control">
                <?php if ($cod_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $text_image; ?></label>
            <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" /></a>
              <input type="hidden" name="cod_image" value="<?php echo $image; ?>" id="input-image" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image-height"><?php echo $entry_image_height; ?></label>
            <div class="col-sm-10">
              <input type="text" name="cod_image_height" value="<?php echo $cod_image_height; ?>" placeholder="<?php echo $entry_image_height; ?>" id="input-image-height" class="form-control" />
              <?php if ($error_image_height) { ?>
                <div class="text-danger"><?php echo $error_image_height; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image-width"><?php echo $entry_image_width; ?></label>
            <div class="col-sm-10">
              <input type="text" name="cod_image_width" value="<?php echo $cod_image_width; ?>" placeholder="<?php echo $entry_image_width; ?>" id="input-image-width" class="form-control" />
              <?php if ($error_image_width) { ?>
                <div class="text-danger"><?php echo $error_image_width; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="cod_sort_order" value="<?php echo $cod_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
              <?php if ($error_sort_order) { ?>
                <div class="text-danger"><?php echo $error_sort_order; ?></div>
              <?php } ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
