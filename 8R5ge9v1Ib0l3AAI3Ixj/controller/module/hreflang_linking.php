<?php
class ControllerModuleHreflangLinking extends Controller {

  private $error = array();

  public function index(){

    $this->load->language('module/hreflang_linking');
    $this->document->setTitle($this->language->get('heading_title'));
    $this->load->model('setting/setting');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      $this->model_setting_setting->editSetting('show_hreflang_linking', ['show_hreflang_linking' => $this->request->post['hreflang_linking']]);

      $this->session->data['success'] = $this->language->get('text_success');

		  $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
    }

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_edit']     = $this->language->get('text_edit');
    $data['text_enabled']  = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/register_setting', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/register_setting', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    if (isset($this->request->post['hreflang_linking'])) {
			$data['hreflang_linking'] = $this->request->post['hreflang_linking'];
		} elseif($this->config->get('show_hreflang_linking') != null) {
			$data['hreflang_linking'] = $this->config->get('show_hreflang_linking');
		}else{
      $data['hreflang_linking'] = '';
    }

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$this->response->setOutput($this->load->view('module/hreflang_linking.tpl', $data));

  }

  public function install() {
    $this->load->model('localisation/language');

    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "own_hreflang_link (
      link_id int(11) NOT NULL,
      hreflang varchar(10) NOT NULL,
      href varchar(255) NOT NULL,
      page varchar(255) NOT NULL
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

    $this->db->query("ALTER TABLE " . DB_PREFIX . "own_hreflang_link ADD PRIMARY KEY (`link_id`)");

    $this->db->query("ALTER TABLE " . DB_PREFIX . "own_hreflang_link MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT;");

  }


  public function uninstall() {
    $this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "own_hreflang_link");
  }

  public function validate(){
    if (!$this->user->hasPermission('modify', 'module/hreflang_linking')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
    return !$this->error;
  }

}
