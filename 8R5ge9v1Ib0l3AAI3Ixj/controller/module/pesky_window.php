<?php
class ControllerModulePeskyWindow extends Controller
{
  private $error;

  public function index()
  {
    $this->load->language('module/pesky_window');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/product');
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('pesky_window', $this->request->post);
      $this->cache->delete('product.pesky_window');

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

    $data['entry_product'] = $this->language->get('entry_product');
    $data['entry_first_timeout'] = $this->language->get('entry_first_timeout');
    $data['entry_timeout'] = $this->language->get('entry_timeout');
    $data['entry_min_timeout'] = $this->language->get('entry_min_timeout');
    $data['entry_max_timeout'] = $this->language->get('entry_max_timeout');
    $data['entry_status'] = $this->language->get('entry_status');

		$data['help_product'] = $this->language->get('help_product');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    if (isset($this->error['first_timeout'])) {
			$data['error_first_timeout'] = $this->error['first_timeout'];
		} else {
			$data['error_first_timeout'] = '';
		}

    if (isset($this->error['min_timeout'])) {
			$data['error_min_timeout'] = $this->error['min_timeout'];
		} else {
			$data['error_min_timeout'] = '';
		}

    if (isset($this->error['max_timeout'])) {
			$data['error_max_timeout'] = $this->error['max_timeout'];
		} else {
			$data['error_max_timeout'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/pesky_window', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/pesky_window', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['pesky_window_status'])) {
			$data['pesky_window_status'] = $this->request->post['pesky_window_status'];
		} else {
			$data['pesky_window_status'] = $this->config->get('pesky_window_status');
		}

    if (isset($this->request->post['pesky_window_first_timeout'])) {
			$data['pesky_window_first_timeout'] = $this->request->post['pesky_window_first_timeout'];
		} else {
			$data['pesky_window_first_timeout'] = $this->config->get('pesky_window_first_timeout');
		}

    if (isset($this->request->post['pesky_window_min_timeout'])) {
			$data['pesky_window_min_timeout'] = $this->request->post['pesky_window_min_timeout'];
		} else {
			$data['pesky_window_min_timeout'] = $this->config->get('pesky_window_min_timeout');
		}

    if (isset($this->request->post['pesky_window_max_timeout'])) {
			$data['pesky_window_max_timeout'] = $this->request->post['pesky_window_max_timeout'];
		} else {
			$data['pesky_window_max_timeout'] = $this->config->get('pesky_window_max_timeout');
		}

    $data['products'] = [];

		if($this->config->get('pesky_window_products') !== NULL){
			foreach($this->config->get('pesky_window_products') AS $product_id){
				$product = $this->model_catalog_product->getProduct($product_id);
        if ($product['quantity'] > 0) {
          $data['products'][] = ['product_id' => $product_id,
  					                   'name'				=> $product['name'],
                              ];
        }
			}
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/pesky_window.tpl', $data));
  }

  private function validate()
  {
    if (!$this->user->hasPermission('modify', 'module/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

    if ((int)$this->request->post['pesky_window_first_timeout'] <= 0 ||
    (string)(int)$this->request->post['pesky_window_first_timeout'] != (string)$this->request->post['pesky_window_first_timeout']) {
      $this->error['first_timeout'] = $this->language->get('error_first_timeout');
    }

    if ((int)$this->request->post['pesky_window_min_timeout'] <= 0 ||
    (string)(int)$this->request->post['pesky_window_min_timeout'] != (string)$this->request->post['pesky_window_min_timeout']) {
      $this->error['min_timeout'] = $this->language->get('error_min_timeout');
    }

    if ((int)$this->request->post['pesky_window_max_timeout'] <= 0 ||
    (string)(int)$this->request->post['pesky_window_max_timeout'] != (string)$this->request->post['pesky_window_max_timeout']) {
      $this->error['max_timeout'] = $this->language->get('error_max_timeout');
    }

		return !$this->error;
  }

  public function install()
  {
    $this->load->model('setting/setting');

    $product_query = $this->db->query("SELECT DISTINCT op.product_id
                                       FROM " . DB_PREFIX . "order_product op
                                       INNER JOIN " . DB_PREFIX . "product p ON op.product_id = p.product_id
                                       WHERE p.quantity > 0
                                       ORDER BY op.order_id DESC LIMIT 200;");

    $defautl_data['pesky_window_products'] = array_column($product_query->rows, 'product_id');
    $defautl_data['pesky_window_min_timeout'] = 1;
    $defautl_data['pesky_window_max_timeout'] = 30;
    $defautl_data['pesky_window_first_timeout'] = 27;

    $this->model_setting_setting->editSetting('pesky_window', $defautl_data);
  }
}
