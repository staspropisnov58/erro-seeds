<?php
class ControllerModuleNotificationRequest extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/notification_request');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('notification_request', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/notification_request', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/notification_request', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['notification_request_status'])) {
			$data['notification_request_status'] = $this->request->post['notification_request_status'];
		} else {
			$data['notification_request_status'] = $this->config->get('notification_request_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/notification_request.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/notification_request')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "notification_request (
											request_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  										product_id int(11) NOT NULL,
											quantity int(11) NOT NULL DEFAULT '1',
  										customer_id int(11) NOT NULL DEFAULT '0',
  										email varchar(255) NOT NULL,
  										date_added datetime NOT NULL,
  										date_expired datetime NOT NULL,
											language_id tinyint(3) NOT NULL
											) ENGINE=InnoDB DEFAULT CHARSET=utf8");

		$this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "notification_request_deleted (
			 								request_id INT(11) NOT NULL PRIMARY KEY,
											product_id INT(11) NOT NULL,
											quantity int(11) NOT NULL DEFAULT '1',
											customer_id int(11) NOT NULL DEFAULT '0',
											email VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
											date_added DATETIME NOT NULL,
											date_expired DATETIME NOT NULL,
											date_deleted DATETIME NOT NULL ,
											language_id TINYINT(3) NOT NULL
											) ENGINE=InnoDB DEFAULT CHARSET=utf8");

	  $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "notification_request_complete (
			 								request_id INT(11) NOT NULL PRIMARY KEY,
											product_id INT(11) NOT NULL,
											quantity INT(11) NOT NULL DEFAULT '1',
											customer_id int(11) NOT NULL DEFAULT '0',
											email VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
											date_added DATETIME NOT NULL,
											date_expired DATETIME NOT NULL,
											date_complete DATETIME NOT NULL ,
											language_id TINYINT(3) NOT NULL
											) ENGINE=InnoDB DEFAULT CHARSET=utf8");


	}

	public function uninstall() {
		$this->db->query("DROP table " . DB_PREFIX . "notification_request");
		$this->db->query("DROP table " . DB_PREFIX . "notification_request_deleted");
		$this->db->query("DROP table " . DB_PREFIX . "notification_request_complete");


	}
}
