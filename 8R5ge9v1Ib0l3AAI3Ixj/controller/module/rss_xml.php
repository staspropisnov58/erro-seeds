<?php
class ControllerModuleRssXml extends Controller {
  private $error = array();
  public function index(){

    $this->load->language('module/rss_xml');

    $this->load->model('localisation/language');

    $this->load->model('setting/setting');

    $this->document->setTitle($this->language->get('heading_title'));

    $data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
    $data['text_form'] = $this->language->get('text_form');
    $data['tab_general'] = $this->language->get('tab_general');
    $data['entry_xml_name'] = $this->language->get('entry_xml_name');
    $data['entry_xml_limit'] = $this->language->get('entry_xml_limit');
    $data['text_none'] = $this->language->get('text_none');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/rss_xml', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/rss_xml', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      $this->model_setting_setting->editSetting('xml_limit', ['xml_limit' => $this->request->post['xml_limit']]);
			$this->model_setting_setting->editSetting('xml', ['xml' => $this->request->post['xml']]);

      $this->generateXml($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

    $data['languages'] = $this->model_localisation_language->getLanguages();

    if($this->config->get('xml_limit') != null){
      $data['xml_limit'] = $this->config->get('xml_limit');
    }elseif(isset($this->request->post['xml_limit'])){
      $data['xml_limit'] = $this->request->post['xml_limit'];
    }else{
      $data['xml_limit'] = '';
    }

    if($this->config->get('xml') != null){
      $data['xml'] = $this->config->get('xml');
    }elseif(isset($this->request->post['xml'])){
      $data['xml'] = $this->request->post['xml'];
    }else{
      $data['xml'] = '';
    }

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['xml_name'])) {
      $data['error_xml_name'] = $this->error['xml_name'];
    } else {
      $data['error_xml_name'] = '';
    }

    if (isset($this->error['xml_limit'])) {
      $data['error_xml_limit'] = $this->error['xml_limit'];
    } else {
      $data['error_xml_limit'] = '';
    }



    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/rss_xml.tpl', $data));

  }

  protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/affiliate')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
    if (!is_numeric($this->request->post['xml_limit']) || $this->request->post['xml_limit'] == ''){
      $this->error['xml_limit'] = $this->language->get('error_xml_limit');
    }

    if(isset($this->request->post['xml'])){

      if(count($this->request->post['xml']) !== count(array_unique($this->request->post['xml']))){
        $this->error['xml_name'] = $this->language->get('error_xml_name');
      }
    }
		return !$this->error;
	}

  public function generateXml($data = null){
    $this->load->model('module/rss_xml');
    $this->load->model('extension/news');
    if($data!= null){
      $arr['limit'] = $data['xml_limit'];
    }else{
      $arr['limit'] = $this->config->get('xml_limit');
    }
    file_put_contents('limit.txt', $arr['limit']);
    $arr['start'] = 0;
    $all_news = $this->model_extension_news->getAllNews($arr);
    $path = realpath('../rss/');

    if($data==null){
      if($this->config->get('xml')!== NULL){
        $data['xml'] = $this->config->get('xml');
      }
    }
    foreach ($data['xml'] as $key=> $xml){
      $xmlstr = '<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0"></rss>';
      $xmlf = new SimpleXMLElement($xmlstr, 0, false, 'http://purl.org/dc/elements/1.1/');
      $track = $xmlf->addChild('channel');
      $track->addChild('title',  iconv("UTF-8", "UTF-8*//TRANSLIT", $this->config->get('blog_names')[$key]) .' - ' .  iconv("UTF-8", "UTF-8*//TRANSLIT", $this->config->get('config_meta_title'.  $key)));
      $track->addChild('link', HTTP_CATALOG.'blog');
      $track->addChild('description', '<![CDATA[' .  iconv("UTF-8", "UTF-8*//TRANSLIT", $this->config->get('blog_names')[$key]).']]>');
      $track->addChild('lastBuildDate',date('r'));
      foreach ($all_news as $news) {
        $d = new DateTime($news['date_added']);
        $a = $track->addChild('item');
        $a->addChild('title', mb_convert_encoding($news['title'], "UTF-8"));
        $a->addChild('description','<![CDATA[' .  iconv("UTF-8", "UTF-8*//TRANSLIT", $news['description']) . ']]>');
        $a->addChild('content:encoded','<![CDATA[' .  iconv("UTF-8", "UTF-8*//TRANSLIT", $news['description']) . ']]>');
        $a->addChild('pubDate', $d->format('r'));
        $a->addChild('link', HTTP_CATALOG.'news/'.$this->model_module_rss_xml->getSeoUrl('news_id='.$news['news_id']));
        $a->addChild('guid', HTTP_CATALOG.'news/'.$news['news_id']);
      }
      Header('Content-type: text/xml');

      $xmlf->asXML("../rss/$xml.xml");
      $data = file_get_contents("../rss/" . $xml . '.xml');
      $data = html_entity_decode($data, ENT_NOQUOTES, 'UTF-8');
      file_put_contents("../rss/$xml.xml", $data);
    }
  }

  // public function generateXml($data = null){
  //   $this->load->model('module/rss_xml');
  //   $this->load->model('extension/news');
  //   $arr['limit'] = $this->config->get('xml_limit');
  //   $arr['start'] = 0;
  //   $all_news = $this->model_extension_news->getAllNews($arr);
  //   $path = realpath('../rss/');
  //
  //   if($data==null){
  //     if($this->config->get('xml')!== NULL){
  //       $data['xml'] = $this->config->get('xml');
  //     }
  //   }
  //   foreach ($data['xml'] as $key=> $xml){
  //     $xmlstr = '<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0"></rss>';
  //     $xmlf = new SimpleXMLElement($xmlstr, 0, false, 'http://purl.org/dc/elements/1.1/');
  //     $track = $xmlf->addChild('channel');
  //     $track->addChild('title');
  //     $track->addChild('link');
  //     $track->addChild('description');
  //     $track->addChild('lastBuildDate');
  //     foreach ($all_news as $news) {
  //       $d = new DateTime($news['date_added']);
  //       $a = $track->addChild('item');
  //       $a->addChild('title');
  //       $a->addChild('description','<![CDATA[' .  iconv("UTF-8", "UTF-8*//TRANSLIT", $news['description']) . ']]');
  //       $a->addChild('content:encoded');
  //       $a->addChild('pubDate');
  //       $a->addChild('link');
  //       $a->addChild('guid');
  //     }
  //     Header('Content-type: text/xml');
  //     $xmlf->asXML("../rss/$xml.xml");
  //     $data = file_get_contents("../rss/" . $xml . '.xml');
  //     $data = html_entity_decode($data, ENT_NOQUOTES, 'UTF-8');
  //     file_put_contents("../rss/$xml.xml", $data);
  //   }
  // }

}
