<?php
class ControllerModuleOpengraph extends Controller {
  private $error = array();


  public function index() {

    $this->load->language('module/opengraph');

    $this->load->model('module/opengraph');
    $this->load->model('localisation/language');
    $this->load->model('tool/image');

		$this->document->setTitle($this->language->get('heading_title'));

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/opengraph', 'token=' . $this->session->data['token'], 'SSL')
		);

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      $this->load->model('setting/setting');

      $this->model_setting_setting->editSetting('opengraph', $this->request->post);

      $this->model_setting_setting->editSetting('og_homepage', $this->request->post);


      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
    }

    if (isset($this->error['opengraph_image_height'])) {
      $data['error_opengraph_image_height'] = $this->error['opengraph_image_height'];
    } else {
      $data['error_opengraph_image_height'] = '';
    }

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    if (isset($this->error['opengraph_image_width'])) {
      $data['error_opengraph_image_width'] = $this->error['opengraph_image_width'];
    } else {
      $data['error_opengraph_image_width'] = '';
    }

    if (isset($this->error['og_homepage'])) {
			$data['error_og_homepage'] = $this->error['og_homepage'];
		} else {
			$data['error_og_homepage'] = array();
		}

    $this->load->model('setting/setting');

    $data['token'] = $this->session->data['token'];

    if (isset($this->request->post['opengraph_status'])) {
			$data['opengraph_status'] = $this->request->post['opengraph_status'];
		} elseif($this->config->get('affiliate_status') !== NULL) {
			$data['opengraph_status'] = $this->config->get('opengraph_status');
		}else{
      $data['opengraph_status'] = 0;
    }

    if(isset($this->request->post['opengraph_use_meta_when_no_og'])){
      $data['opengraph_use_meta_when_no_og'] = $this->request->post['opengraph_use_meta_when_no_og'];
    }elseif($this->config->get('opengraph_use_meta_when_no_og') !== NULL){
      $data['opengraph_use_meta_when_no_og'] = $this->config->get('opengraph_use_meta_when_no_og');
    }else{
      $data['opengraph_use_meta_when_no_og'] = 0;
    }

    if(isset($this->request->post['opengraph_image_width'])){
      $data['opengraph_image_width'] = $this->request->post['opengraph_image_width'];
    }elseif($this->config->get('opengraph_image_width') !== NULL){
      $data['opengraph_image_width'] = $this->config->get('opengraph_image_width');
    }else{
      $data['opengraph_image_width'] = '';
    }

    if(isset($this->request->post['opengraph_image_height'])){
      $data['opengraph_image_height'] = $this->request->post['opengraph_image_height'];
    }elseif($this->config->get('opengraph_image_height') !== NULL){
      $data['opengraph_image_height'] = $this->config->get('opengraph_image_height');
    }else{
      $data['opengraph_image_height'] = '';
    }

    $data['languages'] = $this->model_localisation_language->getLanguages();

    $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

    if(isset($this->request->post['og_homepage'])){
      foreach($this->request->post['og_homepage']['page_description'] as $key => $value){
        $this->request->post['og_homepage']['page_description'][$key]['thumb'] =  $this->model_tool_image->resize($value['image'], 100, 100);
      }
      $data['og_homepage'] = $this->request->post['og_homepage'];
    }elseif($this->config->get('og_homepage')!== NULL){
      $og_homepage = $this->config->get('og_homepage');
      foreach($og_homepage['page_description'] as $key => $value){
        $og_homepage['page_description'][$key]['thumb'] =  $this->model_tool_image->resize($value['image'], 100, 100);
      }
      $data['og_homepage'] = $og_homepage;
    }else{
      foreach($data['languages'] as $laguage){
        $data['og_homepage']['page_description'][$laguage['language_id']]['thumb'] =  $this->model_tool_image->resize($value['image'], 100, 100);
      }
    }

    if (isset($this->request->post['selected'])) {
			$data['selected'] = $this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

    $data['heading_title'] = $this->language->get('heading_title');
    $data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
    $data['text_edit'] = $this->language->get('text_edit');
    $data['text_enabled'] = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');
    $data['text_opengraph_use_meta_when_no_og'] = $this->language->get('text_opengraph_use_meta_when_no_og');
    $data['text_yes'] = $this->language->get('text_yes');
    $data['text_no'] = $this->language->get('text_no');
    $data['text_height'] = $this->language->get('text_height');
    $data['text_width'] = $this->language->get('text_width');
    $data['text_no_results'] = $this->language->get('text_no_results');

    $data['entry_status'] = $this->language->get('entry_status');
    $data['entry_title'] = $this->language->get('entry_title');
    $data['entry_description'] = $this->language->get('entry_description');
    $data['entry_image'] = $this->language->get('entry_image');

    $data['tab_basic_settings'] = $this->language->get('tab_basic_settings');
    $data['tab_home_page'] = $this->language->get('tab_home_page');
    $data['tab_opengraph_list'] = $this->language->get('tab_opengraph_list');

    $data['column_name'] = $this->language->get('column_name');
    $data['column_action'] = $this->language->get('column_action');
    $data['button_edit'] = $this->language->get('button_edit');
    $data['text_confirm'] = $this->language->get('text_confirm');
    $data['button_add'] = $this->language->get('button_add');
    $data['button_delete'] = $this->language->get('button_delete');

    $data['pages'] = array();

    $results = $this->model_module_opengraph->getAllPages();

    foreach ($results as $result) {
			$data['pages'][] = array(
				'page_id' => $result['page_id'],
				'name'      => $result['name'],
				'edit'      => $this->url->link('module/opengraph/edit', 'token=' . $this->session->data['token'] . '&page_id=' . $result['page_id'] , 'SSL')
			);
		}

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');
    $data['add'] = $this->url->link('module/opengraph/add', 'token=' . $this->session->data['token'] , 'SSL');
    $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
    $data['action'] = $this->url->link('module/opengraph', 'token=' . $this->session->data['token'], 'SSL');
    $data['delete'] = $this->url->link('module/opengraph/delete', 'token=' . $this->session->data['token'], 'SSL');


    $this->response->setOutput($this->load->view('module/opengraph.tpl', $data));
  }

  public function edit(){
    $this->load->model('module/opengraph');
    $this->load->language('module/opengraph');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

      $this->model_module_opengraph->editPage($this->request->post['page_description'], $this->request->get['page_id']);

      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('module/opengraph', 'token=' . $this->session->data['token'] , 'SSL'));
    }


    $this->getForm();

  }

  public function add(){
    $this->load->model('module/opengraph');
    $this->load->language('module/opengraph');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {


      $this->model_module_opengraph->addPage($this->request->post['page_description']);
      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('module/opengraph', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }

    $this->getForm();

  }

  public function getForm(){
    $this->load->model('module/opengraph');
    $this->load->language('module/opengraph');
    $this->load->model('localisation/language');
    $this->load->model('tool/image');

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/opengraph', 'token=' . $this->session->data['token'], 'SSL')
		);

    if (isset($this->error['page_description'])) {
			$data['error_page_description'] = $this->error['page_description'];
		} else {
			$data['error_page_description'] = array();
		}

    $data['text_add_opengraph_page'] = $this->language->get('text_add_opengraph_page');
    $data['entry_name']  = $this->language->get('entry_name');
    $data['entry_route']  = $this->language->get('entry_route');
    $data['entry_title']  = $this->language->get('entry_title');
    $data['entry_description']  = $this->language->get('entry_description');
    $data['entry_image']  = $this->language->get('entry_image');
    $data['button_save']  = $this->language->get('button_save');
    $data['button_cancel']  = $this->language->get('button_cancel');
    $data['new_page'] = $this->language->get('new_page');
    $data['text_edit_page'] = $this->language->get('text_edit_page');

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    $data['languages'] = $this->model_localisation_language->getLanguages();

    if(isset($this->request->get['page_id'])){

      $data['page_id'] = $this->request->get['page_id'];

      $page_information = $this->model_module_opengraph->getPage($this->request->get['page_id']);

      foreach($data['languages'] as $language){
        if($page_information['page_description'][$language['language_id']]['image'] === ''){
          $page_information['page_description'][$language['language_id']]['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }else{
          $page_information['page_description'][$language['language_id']]['thumb'] = $this->model_tool_image->resize($page_information['page_description'][$language['language_id']]['image'], 100, 100);
        }
      }

      $data['breadcrumbs'][] = array(
  			'text' => $page_information['name'],
  			'href' => $this->url->link('module/opengraph/edit', 'token=' . $this->session->data['token'] . '&page_id=' . $this->request->get['page_id'] , 'SSL')
  		);
    }else{
      $data['page_id'] = '';


      $data['breadcrumbs'][] = array(
  			'text' => $this->language->get('new_page'),
  			'href' => $this->url->link('module/opengraph/add', 'token=' . $this->session->data['token'] , 'SSL')
  		);
    }

    $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

    if(isset($this->request->post['page_description'])){
      foreach($data['languages'] as $language){

        if($this->request->post['page_description']['page_description'][$language['language_id']]['image'] === ''){
          $this->request->post['page_description']['page_description'][$language['language_id']]['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }else{
          $this->request->post['page_description']['page_description'][$language['language_id']]['thumb'] = $this->model_tool_image->resize($this->request->post['page_description']['page_description'][$language['language_id']]['image'], 100, 100);
        }
      }
      $data['page_description'] = $this->request->post['page_description'];
    }elseif(isset($page_information)){
      $data['page_description'] = $page_information;
    }else{

      // $data['page_description'] = array(
      //   'name' => '',
      //   'route' => '',
      // );

      foreach ($data['languages'] as $language){
        $data['page_description']['page_description'][$language['language_id']] = array(
          'description' => '',
          'title' => '',
          'image' => '',
          'thumb' => $this->model_tool_image->resize('no_image.png', 100, 100),
        );
      }
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');
    $data['cancel'] = $this->url->link('module/opengraph', 'token=' . $this->session->data['token'] , 'SSL');

    if (!isset($this->request->get['page_id'])) {
			$data['action'] = $this->url->link('module/opengraph/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
      $data['action'] = $this->url->link('module/opengraph/edit', 'token=' . $this->session->data['token']. '&page_id=' . $this->request->get['page_id'], 'SSL');
		}

    $this->response->setOutput($this->load->view('module/opengraph_form.tpl', $data));

  }



  protected function validateForm() {

    $opengraph_use_meta_when_no_og = $this->config->get('opengraph_use_meta_when_no_og');

    foreach($this->request->post['page_description']['page_description'] as $key => $value){
      if(is_numeric($key)){
        if((int)$opengraph_use_meta_when_no_og !== 1 && utf8_strlen(trim($value['title'])) < 1 ||(int)$opengraph_use_meta_when_no_og !== 1 && utf8_strlen(trim($value['title'])) > 100){
          $this->error['page_description'][$key]['title'] = $this->language->get('error_lenght_title');
        }elseif((int)$opengraph_use_meta_when_no_og === 1 && utf8_strlen(trim($value['title'])) > 100){
          $this->error['page_description'][$key]['title'] = $this->language->get('error_empty_title_2');
        }

        if((int)$opengraph_use_meta_when_no_og !== 1 && utf8_strlen(trim($value['description'])) < 1 ||(int)$opengraph_use_meta_when_no_og !== 1 && utf8_strlen(trim($value['description'])) > 300){
          $this->error['page_description'][$key]['description'] = $this->language->get('error_empty_description');
        }elseif((int)$opengraph_use_meta_when_no_og === 1 && utf8_strlen(trim($value['description'])) > 300){
          $this->error['page_description'][$key]['description'] = $this->language->get('error_empty_description_2');
        }

        if((int)$opengraph_use_meta_when_no_og !== 1 && utf8_strlen(trim($value['image'])) < 1){
          $this->error['page_description'][$key]['image'] = $this->language->get('error_empty_image');
        }
      }
    }


    if(utf8_strlen($this->request->post['page_description']['name']) > 255 || utf8_strlen($this->request->post['page_description']['name']) < 3){
      $this->error['page_description']['name'] = $this->language->get('error_name');
    }

    if(utf8_strlen($this->request->post['page_description']['route']) > 255 || utf8_strlen($this->request->post['page_description']['route']) < 3){
      $this->error['page_description']['route'] = $this->language->get('error_name');
    }
		return !$this->error;
	}

  protected function validate() {

    if (!is_numeric($this->request->post['opengraph_image_height']) && $this->request->post['opengraph_image_height'] < 0 || utf8_strlen(trim($this->request->post['opengraph_image_height'])) < 1){
      $this->error['opengraph_image_height'] = $this->language->get('error_image');
    }
    if (!is_numeric($this->request->post['opengraph_image_width']) && $this->request->post['opengraph_image_width'] < 0 || utf8_strlen(trim($this->request->post['opengraph_image_width'])) < 1){
      $this->error['opengraph_image_width'] = $this->language->get('error_image');
    }

    foreach($this->request->post['og_homepage']['page_description'] as $key => $value){
      if((int)$this->request->post['opengraph_use_meta_when_no_og'] !== 1 && utf8_strlen(trim($value['title'])) < 1 || (int)$this->request->post['opengraph_use_meta_when_no_og'] !== 1 && utf8_strlen(trim($value['title'])) > 100){
        $this->error['og_homepage'][$key]['title'] = $this->language->get('error_lenght_title');
      }elseif((int)$this->request->post['opengraph_use_meta_when_no_og'] === 1 && utf8_strlen(trim($value['title'])) > 100){
        $this->error['og_homepage'][$key]['title'] = $this->language->get('error_empty_title_2');
      }

      if((int)$this->request->post['opengraph_use_meta_when_no_og'] !== 1 && utf8_strlen(trim($value['description'])) < 1 || (int)$this->request->post['opengraph_use_meta_when_no_og'] !== 1 && utf8_strlen(trim($value['description'])) > 300){
        $this->error['og_homepage'][$key]['description'] = $this->language->get('error_empty_description');
      }elseif((int)$this->request->post['opengraph_use_meta_when_no_og'] === 1 && utf8_strlen(trim($value['description'])) > 300){
        $this->error['og_homepage'][$key]['description'] = $this->language->get('error_empty_description_2');
      }
      if((int)$this->request->post['opengraph_use_meta_when_no_og'] !== 1 && utf8_strlen(trim($value['image'])) < 1){
        $this->error['og_homepage'][$key]['image'] = $this->language->get('error_empty_image');
      }
    }

		return !$this->error;
	}

  public function install(){

    $this->db->query("ALTER TABLE " . DB_PREFIX . "category_description ADD `og_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, ADD `og_description` VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `og_title`, ADD `og_image` VARCHAR(255) NOT NULL AFTER `og_description`");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "manufacturer_description ADD `og_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, ADD `og_description` VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `og_title`, ADD `og_image` VARCHAR(255) NOT NULL AFTER `og_description`");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "information_description ADD `og_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, ADD `og_description` VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `og_title`, ADD `og_image` VARCHAR(255) NOT NULL AFTER `og_description`");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "news_description ADD `og_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, ADD `og_description` VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `og_title`, ADD `og_image` VARCHAR(255) NOT NULL AFTER `og_description`");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "product_description ADD `og_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, ADD `og_description` VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `og_title`, ADD `og_image` VARCHAR(255) NOT NULL AFTER `og_description`");


    $this->db->query("CREATE TABLE " . DB_PREFIX . "page ( `page_id` INT(11) NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `route` VARCHAR(255) NOT NULL , PRIMARY KEY (`page_id`)) ENGINE = MyISAM");
    $this->db->query("CREATE TABLE " . DB_PREFIX . "page_description ( `page_id` INT(11) NOT NULL , `og_title` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , `og_description` VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL , `og_image` VARCHAR(255) NOT NULL , `language_id` INT(11) NOT NULL ) ENGINE = MyISAM");

    $this->load->model('setting/setting');

    $opengraph['opengraph_image_width'] = 1200;
    $opengraph['opengraph_image_height'] =  630;
    $opengraph['opengraph_use_meta_when_no_og'] = 1;

    $this->load->model('localisation/language');

    $languages = $this->model_localisation_language->getLanguages();

    foreach($languages as $language){
      $og_homepage['og_homepage']['page_description'][$language['language_id']] = array(
        'title' => '',
        'description' => '',
        'image' => 'no_image.png'
      );
    }

    $this->model_setting_setting->editSetting('opengraph', $opengraph);
  }

  public function delete() {
    $this->load->language('model/opengraph');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('module/opengraph');

    if (isset($this->request->post['selected']) && $this->validateDelete()) {
      foreach ($this->request->post['selected'] as $opengraph_id) {
        $this->model_module_opengraph->deletePage($opengraph_id);
      }

      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('module/opengraph', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }

    $this->response->redirect($this->url->link('module/opengraph', 'token=' . $this->session->data['token'] . $url, 'SSL'));
  }

  protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

  public function uninstall(){
    $this->db->query("DROP TABLE " . DB_PREFIX .  "page");
    $this->db->query("DROP TABLE " . DB_PREFIX .  "page_description");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "category_description DROP `og_title`, DROP `og_description`, DROP `og_image`");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "manufacturer_description DROP `og_title`, DROP `og_description`, DROP `og_image`;");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "information_description DROP `og_title`, DROP `og_description`, DROP `og_image`;");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "news_description DROP `og_title`, DROP `og_description`, DROP `og_image`;");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "product_description DROP `og_title`, DROP `og_description`, DROP `og_image`;");

    $this->load->model('setting/setting');

    $this->model_setting_setting->deleteSetting('opengraph');

    $this->model_setting_setting->deleteSetting('og_homepage');
  }

}
