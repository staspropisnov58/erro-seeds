<?php
class ControllerModuleRegisterSetting extends Controller {

  private $error = array();

  public function index(){

    $this->load->language('module/register_setting');
    $this->document->setTitle($this->language->get('heading_title'));
    $this->load->model('setting/setting');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      $this->model_setting_setting->editSetting('show_register_setting', ['show_register_setting' => $this->request->post['register_setting']]);

      $this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));

    }

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_edit']     = $this->language->get('text_edit');
    $data['text_enabled']  = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/register_setting', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/register_setting', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    if (isset($this->request->post['register_setting'])) {
			$data['register_setting'] = $this->request->post['register_setting'];
		} elseif($this->config->get('show_register_setting') != null) {
			$data['register_setting'] = $this->config->get('show_register_setting');
		}else{
      $data['register_setting'] = '';
    }

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$this->response->setOutput($this->load->view('module/register_setting.tpl', $data));

  }

  public function install() {
    $this->load->model('localisation/language');

    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "register_setting (
        `field_id` int(11) NOT NULL,
        `field_name` varchar(255) NOT NULL,
        `field_label` text NOT NULL,
        `field_type` varchar(20) NOT NULL,
        `field_placeholder` text NOT NULL,
        `field_sort_order` tinyint(4) NOT NULL,
        `field_status` tinyint(1) NOT NULL,
        `field_editable` tinyint(1) NOT NULL
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

    $this->db->query("ALTER TABLE " . DB_PREFIX . "register_setting ADD PRIMARY KEY (`field_id`), ADD UNIQUE KEY `field_name` (`field_name`)");

    $this->db->query("ALTER TABLE " . DB_PREFIX . "register_setting MODIFY `field_id` int(11) NOT NULL AUTO_INCREMENT;");


    $languages = $this->model_localisation_language->getLanguages();

    $names= array('login','referred_by','firstname','lastname','patronymic','email','telephone','fax','password');


    foreach ($names as $name){
      foreach ($languages as $language ) {
        $label[$name][$language['language_id']] = ucfirst($name);
      }
    }

    $this->db->query("INSERT INTO " . DB_PREFIX . "register_setting (field_name, field_label, field_placeholder, field_sort_order, field_status, field_editable, field_type) VALUES('login', '"  . $this->db->escape(serialize($label['login'])) . "', '', '1', '0', '1', 'text'), ('referred_by','"  . $this->db->escape(serialize($label['referred_by'])) . "', '', '7', '0', '0', 'text'), ('firstname', '"  . $this->db->escape(serialize($label['firstname'])) . "', '', '2', '0', '1', 'text'), ('lastname', '"  . $this->db->escape(serialize($label['lastname'])) . "', '', '3', '0', '1', 'text'), ('patronymic', '"  . $this->db->escape(serialize($label['patronymic'])) . "', '', '4', '0', '1', 'text'), ('email', '"  . $this->db->escape(serialize($label['email'])) . "', '', '0', '1', '1', 'text'), ('telephone', '"  . $this->db->escape(serialize($label['telephone'])) . "', '', '5', '0', '1', 'tel'), ('fax', '"  . $this->db->escape(serialize($label['fax'])) . "', '', '6', '0', '1', 'text'), ('password', '"  . $this->db->escape(serialize($label['password'])) . "', '', '8', '1', '0', 'password')");



    $customer_columns = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "customer");

    $customer_columns = $customer_columns->rows;

    foreach ($customer_columns as $cusromer_column) {
      $columns[] = $cusromer_column['Field'];
    }

    $instaling_columns = array('login','referred_by','patronymic');

    $instaling_columns = array_diff($instaling_columns,$columns);

    if($instaling_columns){

      foreach ($instaling_columns as $instaling_column) {
        if($customer_columns == 'login'){
          $this->db->query("ALTER IGNORE TABLE " . DB_PREFIX . "customer ADD COLUMN IF NOT EXISTS login varchar(255) UNIQUE");
        }else{
          $this->db->query("ALTER TABLE " . DB_PREFIX . "customer ADD COLUMN IF NOT EXISTS "  . $instaling_column . " varchar(255)");
        }
      }
    }
  }


  public function uninstall() {
    $this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "register_setting");
  }

  public function validate(){
    if (!$this->user->hasPermission('modify', 'module/register_setting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
    return !$this->error;
  }

}
