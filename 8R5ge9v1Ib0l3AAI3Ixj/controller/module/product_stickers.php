<?php
class ControllerModuleProductStickers extends Controller {
	private $error = array();
	private $stickers = ['hit', 'new', 'recommended'];

	public function index() {
		$this->load->language('module/product_stickers');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('product_stickers_status', ['product_stickers_status' => $this->request->post['product_stickers_status']]);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['token'] = $this->session->data['token'];

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_time'] = $this->language->get('text_all_time');
		$data['text_last_year'] = $this->language->get('text_last_year');
		$data['text_last_mounth'] = $this->language->get('text_last_mounth');
		$data['text_categories'] = $this->language->get('text_categories');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_none'] = $this->language->get('text_none');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_product_stickers_new_date'] = $this->language->get('entry_product_stickers_new_date');
		$data['entry_select_date'] = $this->language->get('entry_select_date');
		$data['entry_stickers_hit_limit'] = $this->language->get('entry_stickers_hit_limit');
		$data['entry_hit_sales_period'] = $this->language->get('entry_hit_sales_period');
		$data['entry_hit_category'] = $this->language->get('entry_hit_category');
		$data['entry_new_category'] = $this->language->get('entry_new_category');
		$data['entry_recommended_category'] = $this->language->get('entry_recommended_category');

		$data['button_update_new_items'] = $this->language->get('button_update_new_items');
		$data['button_update_hit_period'] = $this->language->get('button_update_hit_period');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_refresh'] = $this->language->get('button_refresh');

		$data['warning_category'] = $this->language->get('warning_category');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/product_stickers', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/product_stickers', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['product_stickers_status'])) {
			$data['product_stickers_status'] = $this->request->post['product_stickers_status'];
		} else {
			$data['product_stickers_status'] = $this->config->get('product_stickers_status');
		}

		if ($this->config->get('product_stickers_new_date') !== NULL) {
			$data['product_stickers_new_date'] = $this->config->get('product_stickers_new_date');
		}else{
			$data['product_stickers_new_date'] = '';
		}

		if ($this->config->get('product_stickers_hit_limit') !== NULL) {
			$data['product_stickers_hit_limit'] = $this->config->get('product_stickers_hit_limit');
		}else{
			$data['product_stickers_hit_limit'] = '';
		}

		if ($this->config->get('product_stickers_hit_period') !== NULL) {
			$data['product_stickers_hit_period'] = $this->config->get('product_stickers_hit_period');
		}else{
			$data['product_stickers_hit_period'] = 'all_time';
		}

		$this->load->model('catalog/category');

		foreach ($this->stickers as $sticker) {
			if (isset($this->request->post['product_stickers_' . $sticker . '_category'])) {
				$category_id = $this->request->post['product_stickers_' . $sticker . '_category'];
			} elseif ($this->config->get('product_stickers_' . $sticker . '_category') != NULL) {
				$category_id = $this->config->get('product_stickers_' . $sticker . '_category');
			} else {
				$category_id = 0;
			}

			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$name = ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name'];
				$status = $category_info['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled');
				$data['product_stickers_' . $sticker . '_category'] = [
					'category_id' => $category_info['category_id'],
					'name' => $name . ' | ' . $status,
				];
			} else {
				$data['product_stickers_' . $sticker . '_category'] = [
					'category_id' => 0,
					'name' => '',
				];
			}
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/product_stickers.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/product_stickers')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}

	public function install() {
    $this->db->query("ALTER TABLE " . DB_PREFIX ."product ADD `main_stickers` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");
		$this->db->query("ALTER TABLE " . DB_PREFIX ."product ADD `deprecated_stickers` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");

      $main_stickers = array();
      $deprecated_stickers = array();

    $this->db->query("UPDATE " . DB_PREFIX . "product SET main_stickers = '" . $this->db->escape(serialize($main_stickers)) . "'");
		$this->db->query("UPDATE " . DB_PREFIX . "product SET deprecated_stickers = '" . $this->db->escape(serialize($deprecated_stickers)) . "'");

		$this->setHitStickers();
    $this->setNewStickers();

	}

	public function setStickersCategory()
	{
		$json = [];

		if (isset($this->request->post['category_id']) && isset($this->request->post['sticker'])) {
			$this->load->model('module/product_stickers');

			$this->model_module_product_stickers->setStickersCategory(
				['category_id' => $this->request->post['category_id'],
				 'sticker' => $this->request->post['sticker']
			 	]
			);

			$json['success'] = true;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function updateStickersAjax()
	{
		$this->load->language('module/product_stickers');

		$json = array();

		if ($this->validate()) {
			if (isset($this->request->post['button'])) {
				$url = '&filter_main_sticker=';
				if ($this->request->post['button'] === 'update_new') {
					$this->setNewStickers($this->request->post['product_stickers_new_date']);
					$url .= 'new';
				} else if ($this->request->post['button'] === 'update_hit') {
					$this->setHitStickers((int)$this->request->post['product_stickers_hit_limit'], $this->request->post['product_stickers_hit_period']);
					$url .= 'hit';
				}

				$json['success'] = sprintf($this->language->get('sticker_update_success'), $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} else {
				$json['error'] = 'No button';
			}
		} else {
			$json['error'] = $this->language->get('error_permission');

		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

  public function setNewStickers($last_date = ''){

    $date = new DateTime();
    $now_date = $date->format('Y-m-d H:i:s');
		$date->modify('-2 month');

		if(!$last_date){
    	$last_date = $date->format('Y-m-d H:i:s');
		}else{
			$date = new DateTime($last_date);
			$last_date = $date->format('Y-m-d H:i:s');
		}

		$old_products = $this->db->query("SELECT product_id, main_stickers FROM " . DB_PREFIX . "product WHERE main_stickers LIKE '%new%'");
		foreach($old_products->rows as $old_product){
			$old_product['main_stickers'] = unserialize($old_product['main_stickers']);
			unset($old_product['main_stickers'][array_search('new', $old_product['main_stickers'])]);
			$this->db->query("UPDATE " . DB_PREFIX . "product SET main_stickers = '" . $this->db->escape(serialize($old_product['main_stickers'])) . "' WHERE product_id = '" . $this->db->escape((int)$old_product['product_id']) . "'");
		}

    $new_products = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE date_added BETWEEN '" . $last_date . "' AND '" . $now_date . "'");

    if($new_products->num_rows){
      foreach ($new_products->rows as $new_product) {
        $query = $this->db->query("SELECT main_stickers, deprecated_stickers  FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$new_product['product_id'] . "'");

        $main_stickers = unserialize($query->row['main_stickers']);
				$deprecated_stickers = unserialize($query->row['deprecated_stickers']);
        array_push($main_stickers, "new");
        $main_stickers = array_unique($main_stickers);
				if(!in_array('new', $deprecated_stickers)){
        	$this->db->query("UPDATE " . DB_PREFIX . "product SET main_stickers = '" . $this->db->escape(serialize($main_stickers)) . "' WHERE product_id = '" . (int)$new_product['product_id'] . "'");
				}
			}
    }
		$date = new DateTime($last_date);
		$last_date = $date->format('Y-m-d');

		$this->load->model('setting/setting');

		$this->model_setting_setting->editSetting('product_stickers_new', ['product_stickers_new_date'=>$last_date]);

  }

  public function setHitStickers($limit = 20, $period = 'all_time'){

		$filter_hit = array();
		$filter_hit['start'] = 0;
		if($limit){
			$filter_hit['limit'] = $limit;
		}else{
			$limit = 20;
		}


		if($period !== 'all_time'){
			$date = new DateTime();
			$filter_hit['filter_date_end'] = $date->format('Y-m-d H:i:s');
		}

		if($period === 'last_mounth'){
			$filter_hit['filter_date_start'] = $date->modify('-1 month')->format('Y-m-d H:i:s');
		}

		if($period === 'last_year'){
			$filter_hit['filter_date_start'] = $date->modify('-1 year')->format('Y-m-d H:i:s');
		}

		$old_products = $this->db->query("SELECT product_id, main_stickers FROM " . DB_PREFIX . "product WHERE main_stickers LIKE '%hit%'");
		$old_products = $old_products->rows;

		foreach($old_products as $old_product){
			$old_product['main_stickers'] = unserialize($old_product['main_stickers']);
			$old_product['deprecated_stickers'] = unserialize($old_product['deprecated_stickers']);
			if (in_array('hit', $old_product['main_stickers'])) {
				unset($old_product['main_stickers'][array_search('hit', $old_product['main_stickers'])]);
			}

				$this->db->query("UPDATE " . DB_PREFIX . "product SET main_stickers = '" . $this->db->escape(serialize($old_product['main_stickers'])) . "' WHERE product_id = '" . $this->db->escape((int)$old_product['product_id']) . "'");

		}

    $this->load->model('report/product');
    $hit_products = $this->model_report_product->getPurchased($filter_hit);

    foreach ($hit_products as $hit_product) {
      $query = $this->db->query("SELECT main_stickers, deprecated_stickers FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$hit_product['product_id'] . "'");

        $main_stickers = unserialize($query->row['main_stickers']);
				$deprecated_stickers = unserialize($query->row['deprecated_stickers']);
        array_push($main_stickers, "hit");
        $main_stickers = array_unique($main_stickers);

			if(!in_array('hit', $deprecated_stickers)){
      	$this->db->query("UPDATE " . DB_PREFIX . "product SET main_stickers = '" . $this->db->escape(serialize($main_stickers)) . "' WHERE product_id = '" . (int)$hit_product['product_id'] . "'");
			}
    }

		$this->load->model('setting/setting');

		$product_stickers_hit = array(
			'product_stickers_hit_limit' => $limit,
			'product_stickers_hit_period' => $period
		);

		$this->model_setting_setting->editSetting('product_stickers_hit', $product_stickers_hit);

  }

	public function uninstall() {
		$this->db->query("ALTER TABLE " . DB_PREFIX . "product DROP `main_stickers`;");
		$this->db->query("ALTER TABLE " . DB_PREFIX . "product DROP `deprecated_stickers`;");

		$this->load->model('setting/setting');

		$this->model_setting_setting->deleteSetting('product_stickers_new');
		$this->model_setting_setting->deleteSetting('product_stickers_hit');
		$this->model_setting_setting->deleteSetting('product_stickers_status');

	}
}
