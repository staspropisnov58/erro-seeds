<?php
class ControllerModuleGift extends Controller {
	private $error = array();

  public function index() {
    $this->load->language('module/gift');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('setting/setting');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      $this->session->data['success'] = $this->language->get('text_success');

			$this->model_setting_setting->editSetting('gift', $this->request->post);

      $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
    }

    $data['heading_title'] = $this->language->get('heading_title');

    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_edit'] = $this->language->get('text_edit');

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_width'] 					= $this->language->get('entry_width');
		$data['entry_height'] 				= $this->language->get('entry_height');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

		if (isset($this->error['width'])) {
			$data['error_width'] = $this->error['width'];
		} else {
			$data['error_width'] = '';
		}

		if (isset($this->error['height'])) {
			$data['error_height'] = $this->error['height'];
		} else {
			$data['error_height'] = '';
		}

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_module'),
      'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('module/gift', 'token=' . $this->session->data['token'], 'SSL')
    );

		if (isset($this->request->post['gift_status'])) {
			$data['gift_status'] = $this->request->post['gift_status'];
		} elseif($this->config->get('gift_status') !== NULL) {
			$data['gift_status'] = $this->config->get('gift_status');
		}else{
			$data['gift_status'] = false;
		}

		if (isset($this->request->post['gift_image_width'])) {
			$data['gift_image_width'] = $this->request->post['gift_image_width'];
		} elseif ($this->config->get('gift_image_width') !== NULL) {
			$data['gift_image_width'] = $this->config->get('gift_image_width');
		} else {
			$data['gift_image_width'] = 200;
		}

		if (isset($this->request->post['gift_image_height'])) {
			$data['gift_image_height'] = $this->request->post['gift_image_height'];
		} elseif ($this->config->get('gift_image_height') !== NULL) {
			$data['gift_image_height'] = $this->config->get('gift_image_height');
		} else {
			$data['gift_image_height'] = 200;
		}

    $data['action'] = $this->url->link('module/gift', 'token=' . $this->session->data['token'], 'SSL');

    $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('module/gift.tpl', $data));
  }

  protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/gift')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['gift_image_width']) {
			$this->error['width'] = $this->language->get('error_width');
		}

		if (!$this->request->post['gift_image_height']) {
			$this->error['height'] = $this->language->get('error_height');
		}

		return !$this->error;
	}

  public function install(){
		$this->db->query("CREATE TABLE " . DB_PREFIX . "gift_program (
			 								gift_program_id INT(11) NOT NULL AUTO_INCREMENT,
											datetime_start DATETIME NOT NULL,
											datetime_end DATETIME NOT NULL,
											stackable tinyint(1) NOT NULL DEFAULT '0',
											status tinyint(1) NOT NULL DEFAULT '0',
											PRIMARY KEY (gift_program_id))
											ENGINE = MyISAM");

		$this->db->query("CREATE TABLE " . DB_PREFIX . "gift_program_description (
											gift_program_id INT(11) NOT NULL,
											language_id INT(11) NOT NULL,
											name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
											description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
											PRIMARY KEY (gift_program_id, language_id))
											ENGINE = MyISAM");

		$this->db->query("CREATE TABLE " . DB_PREFIX . "gift_tier (
											gift_tier_id INT(11) NOT NULL AUTO_INCREMENT,
			                gift_program_id INT(11) NOT NULL,
											price_start DECIMAL(15,4) NOT NULL,
											price_end DECIMAL(15,4) NULL,
											PRIMARY KEY (gift_tier_id))
											ENGINE = MyISAM");
		$this->db->query("CREATE TABLE " . DB_PREFIX . "gift_product (
											gift_tier_id INT(11) NOT NULL,
											product_id INT(11) NOT NULL,
											quantity INT(11) NOT NULL,
										  PRIMARY KEY (gift_tier_id, product_id))
											ENGINE = MyISAM");

		$this->db->query("CREATE TABLE " . DB_PREFIX . "order_gift_product (
			                order_id INT(11) NOT NULL,
											product_id INT(11) NOT NULL,
											gift_program_id INT(11) NOT NULL,
											quantity INT(11) NOT NULL,
											PRIMARY KEY (order_id, product_id))
											ENGINE = MyISAM");

		if ($this->config->get('sale_events_status')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "gift_program ADD `sale_event_id` INT(11) NOT NULL");
		}
	}

	public function uninstall() {
		$this->db->query("DROP TABLE " . DB_PREFIX . "gift_program");
		$this->db->query("DROP TABLE " . DB_PREFIX . "gift_program_description");
		$this->db->query("DROP TABLE " . DB_PREFIX . "gift_tier");
		$this->db->query("DROP TABLE " . DB_PREFIX . "gift_product");
		$this->db->query("DROP TABLE " . DB_PREFIX . "order_gift_product");

		$this->load->model('setting/setting');

		$this->model_setting_setting->deleteSetting('gift_status');
	}
}
