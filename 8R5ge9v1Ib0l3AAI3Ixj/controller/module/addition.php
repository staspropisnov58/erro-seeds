<?php
class ControllerModuleAddition extends Controller
{
  public function index() {
    $this->load->language('module/addition');

    $this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('addition', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_edit'] = $this->language->get('text_edit');
    $data['text_enabled'] = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');
    $data['text_dependencies'] = $this->language->get('text_dependencies');
    $data['text_blocks'] = $this->language->get('text_blocks');

    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_title'] = $this->language->get('entry_title');
    $data['entry_description'] = $this->language->get('entry_description');
    $data['entry_status'] = $this->language->get('entry_status');
    $data['entry_dependencies'] = $this->language->get('entry_dependencies');
    $data['entry_block'] = $this->language->get('entry_block');
    $data['entry_product_description'] = $this->language->get('entry_product_description');

    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['name'])) {
      $data['error_name'] = $this->error['name'];
    } else {
      $data['error_name'] = '';
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_module'),
      'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
    );

    if (!isset($this->request->get['module_id'])) {
      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('module/addition', 'token=' . $this->session->data['token'], 'SSL')
      );
    } else {
      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('module/addition', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
      );
    }

    if (!isset($this->request->get['module_id'])) {
      $data['action'] = $this->url->link('module/addition', 'token=' . $this->session->data['token'], 'SSL');
    } else {
      $data['action'] = $this->url->link('module/addition', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
    }

    $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
    }

    if (isset($this->request->post['name'])) {
      $data['name'] = $this->request->post['name'];
    } elseif (!empty($module_info)) {
      $data['name'] = $module_info['name'];
    } else {
      $data['name'] = '';
    }

    if (isset($this->request->post['module_description'])) {
      $data['module_description'] = $this->request->post['module_description'];
    } elseif (!empty($module_info)) {
      $data['module_description'] = $module_info['module_description'];
    } else {
      $data['module_description'] = '';
    }

    $this->load->model('localisation/language');

    $data['languages'] = $this->model_localisation_language->getLanguages();

    if (isset($this->request->post['dependencies'])) {
      $data['dependencies'] = $this->request->post['dependencies'];
    } elseif (!empty($module_info)) {
      $data['dependencies'] = $module_info['dependencies'];
    } else {
      $data['dependencies'] = '';
    }

    if (isset($this->request->post['block'])) {
      $data['block'] = $this->request->post['block'];
    } elseif (!empty($module_info)) {
      $data['block'] = $module_info['block'];
    } else {
      $data['block'] = '';
    }

    if (isset($this->request->post['product_description'])) {
      $data['product_description'] = $this->request->post['product_description'];
    } elseif (!empty($module_info)) {
      $data['product_description'] = $module_info['product_description'];
    } else {
      $data['product_description'] = '';
    }

    if (isset($this->request->post['status'])) {
      $data['status'] = $this->request->post['status'];
    } elseif (!empty($module_info)) {
      $data['status'] = $module_info['status'];
    } else {
      $data['status'] = '';
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('module/addition.tpl', $data));
  }

  protected function validate() {
    if (!$this->user->hasPermission('modify', 'module/addition')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
      $this->error['name'] = $this->language->get('error_name');
    }

    return !$this->error;
  }

  public function load() {
    if (isset($this->request->get['addition_id'])) {
      $this->load->model('extension/module');

      $addition_info = $this->model_extension_module->getModule((int)$this->request->get['addition_id']);

      if ($addition_info) {
        $data = $this->extractAdditionData($addition_info);
        $data['addition_id'] = (int)$this->request->get['addition_id'];
      }
    }

    $this->response->setOutput($this->load->view('module/addition_product.tpl', $data));
  }

  public function getAdditionProduct() {
    $this->load->model('extension/module');
    $this->load->model('module/addition');
    $this->load->model('catalog/category');
    $data['token'] = $this->request->get['token'];
    if (isset($this->request->get['product_id'])) {
      $addition_product_info = $this->model_module_addition->getAdditionProduct($this->request->get['product_id']);

      if ($addition_product_info) {
        $data['addition_product_id'] = $addition_product_info['addition_product_id'];
        foreach ($addition_product_info['dependencies_value'] as $type => $value) {
          if ($type === 'shipping') {
            $shippings = $this->load->controller('extension/shipping/getShippings', $value);
            foreach ($shippings as $shipping) {
              $data[$type][] = array(
                'name' => $shipping['name'],
                'id' =>  $shipping['code']
              );
            }
          } elseif (strpos($type, 'category') !== false) {
            foreach ($value as $category_id) {
              $category_info = $this->model_catalog_category->getCategory($category_id);
              $data[$type][] = array(
                'name' => ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name'],
                'id' => $category_id
              );
            }
          }
        }

        $addition_info = $this->model_extension_module->getModule($addition_product_info['addition_id']);

        if ($addition_info) {
          $data = array_merge($data, $this->extractAdditionData($addition_info));
          $data['addition_id'] = $addition_product_info['addition_id'];
        }
      } else {
        $data['additions'] = $this->model_extension_module->getModulesByCode('addition');
      }
    } elseif (isset($this->request->get['addition_id'])) {
      $addition_info = $this->model_extension_module->getModule((int)$this->request->get['addition_id']);

      if ($addition_info) {
        $data = array_merge($data, $this->extractAdditionData($addition_info));
        $data['addition_id'] = (int)$this->request->get['addition_id'];
      }
    } else {
      $data['additions'] = $this->model_extension_module->getModulesByCode('addition');
    }

    $this->response->setOutput($this->load->view('module/addition_product.tpl', $data));
  }
  /**
   * is called when product is added or edited
   */
  public function addAdditionProduct($product_id) {
    if (isset($this->request->post['addition_product'])) {
      $this->load->model('module/addition');
      if (isset($this->request->post['addition_product']['addition_product_id'])) {
        $this->model_module_addition->editAdditionProduct($this->request->post['addition_product']);
      } else {
        $this->model_module_addition->addAdditionProduct($this->request->post['addition_product'], $product_id);
      }
    }
  }
  /**
   * is called when manager deletes adition manually from product settings
   */
  public function deleteAdditionProduct() {
    if (isset($this->request->get['addition_product_id'])) {
      $this->load->model('module/addition');
      $this->model_module_addition->deleteAdditionProduct($this->request->get['addition_product_id']);
      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode('deleted'));
    }
  }

  public function deleteWithProduct($product_id) {
    $this->db->query("DELETE FROM " . DB_PREFIX . "addition_product WHERE product_id = " . (int)$product_id);
  }

  private function extractAdditionData($addition_info) {
    $this->load->language('module/addition');
    $data = array();
    $data['addition_name'] = $addition_info['name'];
    foreach ($addition_info['dependencies'] as $dependency) {
      $data['dependencies'][] = array(
        'code' => $dependency,
        'text' => $this->language->get('text_dependency_' . $dependency)
      );
    }
    $data['button_delete'] = $this->language->get('button_delete');
    $data['delete'] = $this->url->link('module/addition/deleteAdditionProduct', 'token=' . $this->request->get['token'], 'SSL');
    return $data;
  }

  public function install() {
    $this->load->model('extension/event');
    $this->model_extension_event->addEvent('addition', 'post.admin.product.add', 'module/addition/addAdditionProduct');
    $this->model_extension_event->addEvent('addition', 'post.admin.product.edit', 'module/addition/addAdditionProduct');
    $this->model_extension_event->addEvent('addition', 'post.admin.product.delete', 'module/addition/deleteWithProduct');
    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "addition_product (
                      addition_product_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                      module_id int(11) NOT NULL,
                      product_id int(11) NOT NULL,
                      dependencies_value varchar(1000) NOT NULL
                      ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
  }

  public function uninstall() {
    $this->load->model('extension/event');
    $this->model_extension_event->deleteEvent('addition');
    $this->db->query("DROP table " . DB_PREFIX . "addition_product");
  }
}
