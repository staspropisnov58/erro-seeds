<?php
class ControllerModuleSaleEvents extends Controller {
	private $error = array();

  public function index() {
    $this->load->language('module/sale_events');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('setting/setting');

		$this->load->model('module/sale_events');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      $this->session->data['success'] = $this->language->get('text_success');

			if(isset($this->request->post['sale_events'])){
				$this->model_module_sale_events->editSaleEventType($this->request->post['sale_events']);
			}

			$this->model_setting_setting->editSetting('sale_events_status', ['sale_events_status' => $this->request->post['sale_events_status']]);

      $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
    }

    $data['heading_title'] = $this->language->get('heading_title');

    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_edit'] = $this->language->get('text_edit');

		$data['entry_type'] = $this->language->get('entry_type');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_status'] = $this->language->get('entry_status');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = [];
		}

		if (isset($this->error['type'])) {
			$data['error_type'] = $this->error['type'];
		} else {
			$data['error_type'] = [];
		}

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_module'),
      'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('module/sale_events', 'token=' . $this->session->data['token'], 'SSL')
    );

		if (isset($this->request->post['sale_events_status'])) {
			$data['sale_events_status'] = $this->request->post['sale_events_status'];
		} elseif($this->config->get('sale_events_status') !== NULL) {
			$data['sale_events_status'] = $this->config->get('sale_events_status');
		}else{
			$data['sale_events_status'] = false;
		}

    $data['action'] = $this->url->link('module/sale_events', 'token=' . $this->session->data['token'], 'SSL');

    $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    if (isset($this->request->post['sale_events_status'])) {
      $data['sale_events_status'] = $this->request->post['sale_events_status'];
    } else {
      $data['sale_events_status'] = $this->config->get('sale_events_status');
    }

		if(isset($this->request->post['sale_events'])){
			$data['sale_events'] = $this->request->post['sale_events'];
		}else{
			$data['sale_events'] = $this->model_module_sale_events->getSaleEventType();
		}

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('module/sale_events.tpl', $data));
  }

  protected function validate() {

		foreach ($this->request->post['sale_events'] as $row => $sale_events){
			if(utf8_strlen(trim($sale_events['name'])) < 1 || utf8_strlen(trim($sale_events['name'])) > 101){
				 $this->error['name'][$row] = $this->language->get('error_length_name');
			}
			if(preg_match('/^[a-z_]*$/', $sale_events['type']) !== 1){
				$this->error['type'][$row] = $this->language->get('error_syntax_type');
			}

			if(utf8_strlen(trim($sale_events['type'])) < 2 || utf8_strlen(trim($sale_events['type'])) > 101){
				 $this->error['type'][$row] = $this->language->get('error_length_type');
			}
		}

		$types = array_column($this->request->post['sale_events'], 'type');

		if(count($types) !== count(array_unique($types))){
			$array = array_count_values($types);
			$arr1 = array();
			foreach($array as $key => $value){
				if((int)$value > 1){
					$arr1[] = $key;
				}
			}
			foreach($arr1 as $arr){
				$values = array_keys($types, $arr);
				foreach ($values as $value){
					$this->error['type'][$value] = $this->language->get('error_not_unique_type');
				}
			}
		}



		if (!$this->user->hasPermission('modify', 'module/sale_events')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

  public function install(){

    $this->db->query("CREATE TABLE " . DB_PREFIX . "sale_event ( `sale_event_id` INT(11) NOT NULL AUTO_INCREMENT , `datetime_start` DATETIME NOT NULL ,  `sale_event_setting` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, `disable_coupon` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, `datetime_end` DATETIME NOT NULL , PRIMARY KEY (`sale_event_id`)) ENGINE = MyISAM");

    $this->db->query("CREATE TABLE " . DB_PREFIX . "sale_event_description ( `sale_event_id` INT(11) NOT NULL , `language_id` INT(11) NOT NULL , `name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ) ENGINE = MyISAM");

		$this->db->query("CREATE TABLE " . DB_PREFIX . "sale_event_type ( `sale_event_type_id` INT(11) NOT NULL, `type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , `name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ) ENGINE = MyISAM");

		// ALTER TABLE `oc_sale_event` ADD AFTER `disable_coupon`;

		$this->db->query("ALTER TABLE  " . DB_PREFIX . "sale_event_type CHANGE  `sale_event_type_id`  `sale_event_type_id` INT( 11 ) primary key AUTO_INCREMENT");

		$this->db->query("ALTER TABLE " . DB_PREFIX . "sale_event ADD sale_event_type_id INT(11) NOT NULL DEFAULT '0'");

    $this->db->query("ALTER TABLE " . DB_PREFIX . "product_special ADD `sale_event_id` INT(11) NOT NULL");

    $this->db->query("ALTER TABLE " . DB_PREFIX . "news ADD `sale_event_id` INT(11) NOT NULL");

		$this->db->query("ALTER TABLE " . DB_PREFIX . "banner_image ADD `sale_event_id` INT(11) NOT NULL");

		$this->db->query("INSERT INTO " . DB_PREFIX . "sale_event_type SET name = 'Чёрная пятница', type = 'bf'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "sale_event_type SET name = 'День Рождения ES', type = 'birthday'");




		$array = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "product_special");

		foreach($array->rows as $field){
			$fields[] = $field['Field'];
		}

		if(in_array("status", $fields) === false){
			$this->db->query("ALTER TABLE ". DB_PREFIX . "product_special ADD `status` TINYINT(1) NOT NULL");
		}

		$array = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "banner_image");

		foreach($array->rows as $field){
			$fields[] = $field['Field'];
		}

		if(in_array("status", $fields) === false){
			$this->db->query("ALTER TABLE " . DB_PREFIX . "banner_image ADD `status` TINYINT(1) NOT NULL");
		}

    $array = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "news");

		foreach($array->rows as $field){
			$fields[] = $field['Field'];
		}

		if(in_array("status", $fields) === false){
			$this->db->query("ALTER TABLE " . DB_PREFIX . "news ADD `status` TINYINT(1) NOT NULL");
		}

		$products_special = $this->db->query("SELECT DISTINCT ps.product_special_id FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) RIGHT JOIN oc_product_special psdef ON ps.product_id = psdef.product_id WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) AND psdef.customer_group_id = '1' AND ps.date_start = psdef.date_start ");


		foreach($products_special->rows as $product_special_id){
			$this->db->query("UPDATE " . DB_PREFIX . "product_special SET status=1 WHERE product_special_id = '" . (int)$product_special_id['product_special_id'] . "'");
		}

		$this->db->query("UPDATE " . DB_PREFIX . "banner_image SET status = 1");

	}

	public function uninstall() {

		$this->db->query("DROP TABLE " . DB_PREFIX .  "sale_event");
    $this->db->query("DROP TABLE " . DB_PREFIX .  "sale_event_description ");
		$this->db->query("DROP TABLE " . DB_PREFIX .  "sale_event_type ");
		$this->db->query("ALTER TABLE " . DB_PREFIX . "product_special DROP `sale_event_id`;");
		$this->db->query("ALTER TABLE " . DB_PREFIX . "banner_image DROP `sale_event_id`;");
    $this->db->query("ALTER TABLE " . DB_PREFIX . "news DROP `sale_event_id`;");

		$this->db->query("ALTER TABLE " . DB_PREFIX . "banner_image DROP `sale_event_id`;");

		$this->load->model('setting/setting');

		$this->model_setting_setting->deleteSetting('sale_events_status');


	}

}
