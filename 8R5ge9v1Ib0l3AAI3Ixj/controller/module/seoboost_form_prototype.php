<?php
class ControllerModuleSeoboostFormPrototype extends Controller
{
  public function index()
  {
    $this->load->language('module/seoboost_form_prototype');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('seoboost_form_prototype', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');
    $data['entry_email_to'] = $this->language->get('entry_email_to');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/seoboost_form_prototype', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/seoboost_form_prototype', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['seoboost_form_prototype_status'])) {
			$data['seoboost_form_prototype_status'] = $this->request->post['seoboost_form_prototype_status'];
		} else {
			$data['seoboost_form_prototype_status'] = $this->config->get('seoboost_form_prototype_status');
		}

    if (isset($this->request->post['seoboost_form_prototype_email_to'])) {
			$data['seoboost_form_prototype_email_to'] = $this->request->post['seoboost_form_prototype_email_to'];
		} else {
			$data['seoboost_form_prototype_email_to'] = $this->config->get('seoboost_form_prototype_email_to');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/seoboost_form_prototype.tpl', $data));
  }

  protected function validate()
  {
		if (!$this->user->hasPermission('modify', 'module/seoboost_form_prototype')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
