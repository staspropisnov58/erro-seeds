<?php

class ControllerModuleCheckModified extends Controller
{

  public function index() {
    $this->load->language('module/check_modified');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('setting/setting');

  if ($this->request->server['REQUEST_METHOD'] == 'POST') {

      $this->model_setting_setting->editSetting('check_modified', ['check_modified' => $this->request->post['check_modified']]);

      unset($this->request->post['check_modified']);

      if(empty($this->request->post['check_modified_setting'])){
        $this->request->post['check_modified_setting'] = array();
      }

      $difference = array_diff($this->config->get('check_modified_setting'), $this->request->post['check_modified_setting']);

      if(!empty($difference)){
        $this->model_module_check_modified->deleteCheckModified($difference);
      };


      if(isset($this->request->post['check_modified_setting'])){
        $this->model_setting_setting->editSetting('check_modified_setting', ['check_modified_setting' => $this->request->post['check_modified_setting']]);
      }

      $this->load->model('module/check_modified');

      if (isset($this->request->post['check_modified_setting'])){
        $this->model_module_check_modified->updateCheckModified($this->request->post['check_modified_setting']);
      }
      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
    }

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_edit'] = $this->language->get('text_edit');
    $data['text_enabled'] = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');

    $data['entry_count_order'] = $this->language->get('entry_count_order');
    $data['entry_commission'] = $this->language->get('entry_commission');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_module'),
      'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('module/check_modified', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['action'] = $this->url->link('module/check_modified', 'token=' . $this->session->data['token'], 'SSL');

    $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    if (isset($this->request->post['check_modified'])) {
      $data['check_modified'] = $this->request->post['check_modified'];
    } else {
      $data['check_modified'] = $this->config->get('check_modified');
    }

    if (isset($this->request->post['check_modified_setting'])) {
      $data['check_modified_setting'] = $this->request->post['check_modified_setting'];
    } else {
      $data['check_modified_setting'] = $this->config->get('check_modified_setting');
    }


    $this->load->model('module/check_modified');

    $data['layouts'] = $this->model_module_check_modified->getLayoutList();


    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');


    $this->response->setOutput($this->load->view('module/check_modified.tpl', $data));
  }

  public function install(){
    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "date_modified (
                      date_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                      layout_id int(11) NOT NULL,
                      date_modified datetime NOT NULL,
                      page varchar(255) NOT NULL UNIQUE,
                      KEY (page)
                      ) ENGINE=MyISAM DEFAULT CHARSET=utf8");

    // $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) SELECT CONCAT('product_id=', product_id) AS page, NOW() FROM " . DB_PREFIX . "product" );
    // $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) SELECT CONCAT('category_id=', category_id) AS page, NOW() FROM " . DB_PREFIX . "category" );
    // $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) SELECT CONCAT('news_id=', news_id) AS page, NOW() FROM " . DB_PREFIX . "news" );
    // $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) SELECT CONCAT('information_id=', information_id) AS page, NOW() FROM " . DB_PREFIX . "information" );
    //
    //
    // $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) VALUES ('homepage', NOW()), ('contact', NOW()), ('sitemap', NOW()), ('sale', NOW()), ('testmonial', NOW()), ('news', NOW())");
  }
  public function unistall(){
    $this->db->query("DROP TABLE " . DB_PREFIX . "date_modified");
  }

}
