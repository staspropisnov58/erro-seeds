<?php
class ControllerModuleSmsc extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/smsc');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('smsc', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');

    $data['entry_sms_login'] = $this->language->get('entry_sms_login');
    $data['entry_sms_password'] = $this->language->get('entry_sms_password');
    $data['entry_sms_from'] = $this->language->get('entry_sms_from');
    $data['entry_sms_request'] = $this->language->get('entry_sms_request');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/smsc', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/smsc', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['smsc_status'])) {
			$data['smsc'] = $this->request->post['smsc'];
		} elseif($this->config->get('smsc')) {
			$data['smsc'] = $this->config->get('smsc');
		}else{
      $data['smsc'] = array();
    }

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/smsc.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/smsc')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
