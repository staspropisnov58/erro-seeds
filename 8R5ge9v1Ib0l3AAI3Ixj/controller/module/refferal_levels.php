<?php
class ControllerModuleRefferalLevels extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/refferal_levels');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$this->load->model('module/refferal_levels');

	if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			usort($this->request->post['refferal_levels'], function ($a, $b) {
    		return $a['quantity'] <=> $b['quantity'];
			});

		 if ($this->validate($this->request->post['refferal_levels'])) {
			 $this->model_setting_setting->editSetting('refferal_levels', $this->request->post);

			 $this->model_module_refferal_levels->editCommissionByRefferal($this->request->post['refferal_levels']);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		 }
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_count_order'] = $this->language->get('entry_count_order');
    $data['entry_commission'] = $this->language->get('entry_commission');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['quantity'])) {
			$data['error_quantity'] = $this->error['quantity'];
		} else {
			$data['error_quantity'] = [];
		}

		if (isset($this->error['commission'])) {
			$data['error_commission'] = $this->error['commission'];
		} else {
			$data['error_commission'] = [];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/refferal_levels', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/refferal_levels', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['refferal_levels'])) {
			$data['refferal_levels'] = $this->request->post['refferal_levels'];
		} else {
			$data['refferal_levels'] = $this->config->get('refferal_levels');
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$this->response->setOutput($this->load->view('module/refferal_levels.tpl', $data));
	}

	protected function validate($refferal_levels) {
		foreach ($refferal_levels as $row => $level) {
			if ((int)$level['quantity'] <= 0 ||
	    (string)(int)$level['quantity'] != (string)$level['quantity']) {
	      $this->error['quantity'][$row] = $this->language->get('error_quantity_not_int');
	    }

			if ((int)$level['commission'] <= 0 ||
	    (string)(int)$level['commission'] != (string)$level['commission']) {
	      $this->error['commission'][$row] = $this->language->get('error_commission_not_int');
	    }
		}

		if (!$this->error) {
			foreach ($refferal_levels as $row => $level) {
				if ($row === 0) {
					continue;
				} else {
					if ((int)$level['quantity'] === (int)$refferal_levels[$row - 1]['quantity']) {
						$this->error['quantity'][$row] = $this->language->get('error_quantity_dublicate');
					}

					if ((int)$level['commission'] <= (int)$refferal_levels[$row - 1]['commission']) {
						$this->error['commission'][$row] = $this->language->get('error_commission_less');

						while($row < count($refferal_levels)) {
							$this->error['commission'][++$row] = $this->language->get('error_commission_less');
						}
					}
				}
			}
		}

		return !$this->error;
	}

  public function install() {
    $this->load->model('extension/event');
    $this->model_extension_event->addEvent('refferal_levels', 'pre.affiliate.add.transaction', 'module/refferal_levels/updateCommission');

    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "affiliate (
                      commission int(11) NOT NULL,
                      ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
  }

  public function uninstall() {
		$this->load->model('setting/setting');
		$this->load->model('module/refferal_levels');
    $this->load->model('extension/event');
		$this->model_setting_setting->deleteSetting('refferal_levels');
    $this->model_extension_event->deleteEvent('refferal_levels');
    $this->db->query("UPDATE" . DB_PREFIX . " affiliate SET commission = 10 WHERE affiliate_id > 0");
  }
}
