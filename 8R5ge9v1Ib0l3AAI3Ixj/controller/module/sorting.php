<?php
class ControllerModuleSorting extends Controller {
  private $error = array();
  private $layouts = [];

  public function __construct($registry)
  {
      parent::__construct($registry);

      $this->load->model('module/sorting');

      $this->layouts = $this->model_module_sorting->getLayoutsWithModule('sorting');
  }

  public function index()
  {
      $this->load->language('module/sorting');

      $this->document->setTitle($this->language->get('heading_title'));

      $this->load->model('setting/setting');

      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
          $this->model_setting_setting->editSetting('sorting', $this->request->post);

          $this->session->data['success'] = $this->language->get('text_success');

          $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'] . '&type=module', true));
      }
      $data['heading_title'] = $this->language->get('heading_title');

      $data['text_edit'] = $this->language->get('text_edit');
      $data['text_enabled'] = $this->language->get('text_enabled');
      $data['text_disabled'] = $this->language->get('text_disabled');
      $data['sorting_with1'] = $this->language->get('sorting_with1');
      $data['sorting_type'] = $this->language->get('sorting_type');
      $data['data_sort'] = $this->language->get('data_sort');
      $data['sorting_with2'] = $this->language->get('sorting_with2');
      $data['entry_status'] = $this->language->get('entry_status');

      $data['button_save'] = $this->language->get('button_save');
      $data['button_cancel'] = $this->language->get('button_cancel');

      if (isset($this->error['warning'])) {
          $data['error_warning'] = $this->error['warning'];
      } else {
          $data['error_warning'] = '';
      }

      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_module'),
          'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('heading_title'),
          'href' => $this->url->link('module/sorting', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['action'] = $this->url->link('module/sorting', 'token=' . $this->session->data['token'] . '&type=module', true);

      $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

      if (isset($this->request->post['sorting_status'])) {
          $data['sorting_status'] = $this->request->post['sorting_status'];
      } else {
          $data['sorting_status'] = $this->config->get('sorting_status');
      }

      if ($this->layouts) {
          foreach ($this->layouts as $layout) {
              if (isset($this->request->post['sorting_' . $layout['code']])) {
                  $data['sorting_settings'][$layout['code']] = $this->request->post['sorting_' . $layout['code']];
              } elseif ($this->config->get('sorting_' . $layout['code'])) {
                  $data['sorting_settings'][$layout['code']] = $this->config->get('sorting_' . $layout['code']);
              } else {
                  $data['sorting_settings'][$layout['code']] = [];
              }

              if (isset($this->error[$layout['code']])) {
                  $data['error'][$layout['code']] = $this->error[$layout['code']];
              } else {
                  $data['error'][$layout['code']] = '';
              }
          }

          $data['layouts'] = $this->layouts;
      } else {
          $data['admin_message'] = $this->language->get('admin_message');
      }

      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      $this->response->setOutput($this->load->view('module/sorting.tpl', $data));
  }

  protected function validate()
  {
      foreach ($this->layouts as $layout) {
          if (isset($this->request->post['sorting_' . $layout['code']]['default']['first']) && isset($this->request->post['sorting_' . $layout['code']]['default']['second'])) {
              $first_sort = substr($this->request->post['sorting_' . $layout['code']]['default']['first'], 0, strrpos($this->request->post['sorting_' . $layout['code']]['default']['first'], '_', -0));
              $second_sort = substr($this->request->post['sorting_' . $layout['code']]['default']['second'], 0, strrpos($this->request->post['sorting_' . $layout['code']]['default']['second'], '_', -0));

              if ($first_sort === $second_sort) {
                  $this->error[$layout['code']] = $this->language->get('error_selection');
              }
          }
      }

      if (!$this->user->hasPermission('modify', 'module/sorting')) {
          $this->error['warning'] = $this->language->get('error_permission');
      }

      return !$this->error;
  }
}
