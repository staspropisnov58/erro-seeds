<?php
class ControllerModuleProductList extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/product_list');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$module_id = $this->model_extension_module->addModule('product_list', $this->request->post);
				$this->updateCache($module_id);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
				$this->updateCache($this->request->get['module_id']);
			}

			$this->session->data['success'] = $this->session->data['success'] . ' ' . $this->language->get('text_success_cache');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title']			  = $this->language->get('heading_title');

		$data['text_edit'] 						= $this->language->get('text_edit');
		$data['text_enabled'] 				= $this->language->get('text_enabled');
		$data['text_disabled'] 				= $this->language->get('text_disabled');
    $data['text_appearance'] 			= $this->language->get('text_appearance');
    $data['text_make_choise'] 		= $this->language->get('text_make_choise');
    $data['text_vertical_slider'] = $this->language->get('text_vertical_slider');
    $data['text_table'] 					= $this->language->get('text_table');
		$data['text_none']						= $this->language->get('text_none');

		$data['entry_name'] 					= $this->language->get('entry_name');
		$data['entry_title'] 					= $this->language->get('entry_title');
		$data['entry_product'] 				= $this->language->get('entry_product');
		$data['entry_limit'] 					= $this->language->get('entry_limit');
		$data['entry_width'] 					= $this->language->get('entry_width');
		$data['entry_height'] 				= $this->language->get('entry_height');
		$data['entry_status'] 				= $this->language->get('entry_status');
		$data['entry_category']				= $this->language->get('entry_category');
		$data['entry_attributes']			= $this->language->get('entry_attributes');

		$data['help_location']				= $this->language->get('help_location');
		$data['help_product'] 				= $this->language->get('help_product');

		$data['button_save'] 					= $this->language->get('button_save');
		$data['button_cancel'] 				= $this->language->get('button_cancel');
		$data['button_update_cache'] 	= $this->language->get('button_update_cache');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['width'])) {
			$data['error_width'] = $this->error['width'];
		} else {
			$data['error_width'] = '';
		}

		if (isset($this->error['height'])) {
			$data['error_height'] = $this->error['height'];
		} else {
			$data['error_height'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/product_list', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/product_list', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}

		if(isset($this->request->get['module_id'])){
			$data['module_id'] = $this->request->get['module_id'];
		}else{
			$data['module_id'] = '';
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('module/product_list', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('module/product_list', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['attributes'])) {
			$data['attributes'] = $this->request->post['attributes'];
		} elseif (!empty($module_info)) {
			if(isset($module_info['attributes'])){
				$data['attributes'] = $module_info['attributes'];
			}else{
				$data['attributes'] = 0;
			}
		} else {
			$data['attributes'] = 0;
		}

		if(!empty($module_info)){
			if($module_info['category_id']){
				$this->load->model('catalog/category');
				$category = $this->model_catalog_category->getCategory($module_info['category_id']);
			}else{
				$category = array();
			}
		}

		if (isset($this->request->post['category'])) {
			$data['category_id'] = $this->request->post['category_id'];
		} elseif (!empty($category)) {
			$data['category_id'] = $module_info['category_id'];
		} else {
			$data['category_id'] = '';
		}

		if (isset($this->request->post['category'])) {
			$data['category_name'] = $this->request->post['category'];
		} elseif (!empty($category)) {
			$data['category_name'] = $category['name'];
		} else {
			$data['category_name'] = '';
		}

		if (isset($this->request->post['title'])) {
			$data['title'] = $this->request->post['title'];
		} elseif (!empty($module_info)) {
			$data['title'] = $module_info['title'];
		} else {
			$data['title'] = [];
		}

		if(isset($this->request->post['appearance'])){
			$data['appearance'] = $this->request->post['appearance'];
		} elseif(!empty($module_info)){
			$data['appearance'] = $module_info['appearance'];
		}else{
			$data['appearance'] = '';
		}

		$this->load->model('catalog/product');

		$data['products'] = array();

		if (isset($this->request->post['product'])) {
			$products = $this->request->post['product'];
		} elseif (!empty($module_info)) {
			$products = $module_info['product'];
		} else {
			$products = array();
		}

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				$data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'name'       => $product_info['name']
				);
			}
		}

		if (isset($this->request->post['limit'])) {
			$data['limit'] = $this->request->post['limit'];
		} elseif (!empty($module_info)) {
			$data['limit'] = $module_info['limit'];
		} else {
			$data['limit'] = 5;
		}

		if (isset($this->request->post['width'])) {
			$data['width'] = $this->request->post['width'];
		} elseif (!empty($module_info)) {
			$data['width'] = $module_info['width'];
		} else {
			$data['width'] = 200;
		}

		if (isset($this->request->post['height'])) {
			$data['height'] = $this->request->post['height'];
		} elseif (!empty($module_info)) {
			$data['height'] = $module_info['height'];
		} else {
			$data['height'] = 200;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/product_list.tpl', $data));
	}

	public function updateCache($module_id=''){

		$this->load->language('module/product_list');

		$json = false;

		if($module_id) {

			$this->load->model('extension/module');

			$module_info = $this->model_extension_module->getModule($module_id);

			$this->load->model('tool/api');

			$this->model_tool_api->apiLogin();

			if (isset($this->session->data['cookie'])) {

				$this->load->model('setting/store');

				if (isset($this->request->get['store_id'])) {
						$store_id = $this->request->get['store_id'];
				} else {
						$store_id = 0;
				}

	      $store_info = $this->model_setting_store->getStore($store_id);

	      if ($store_info) {
	          $url = $store_info['ssl'];
	      } else {
	          $url = HTTPS_CATALOG;
	      }

				$url_data = array();

				foreach ($this->request->get as $key => $value) {
					if ($key != 'route' && $key != 'token' && $key != 'store_id') {
							$url_data[$key] = $value;
					}
				}

				$this->load->model('tool/curl');

				$this->model_tool_curl->getCurl($module_info, $url_data, $url, 'module/product_list/updateCache');

			}

		}
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/product_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (!$this->request->post['width']) {
			$this->error['width'] = $this->language->get('error_width');
		}

		if (!$this->request->post['height']) {
			$this->error['height'] = $this->language->get('error_height');
		}

		return !$this->error;
	}
}
