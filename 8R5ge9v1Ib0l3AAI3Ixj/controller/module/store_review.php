<?php
/**
* namespace Admin\Controller\Module
*/
class ControllerModuleStoreReview extends Controller
{
	/**
  * use Admin\Model\Feedback\Review;
  * use Admin\Model\Module\StoreReview;
  */
	private $configuration = [
		'limit_page' => 20,
		'limit_module' => 5,
		'depth' => 5,
		'keyword_one' => 'report',
		'keyword_many' => 'reports',
	];
	private $pages = [
		'store_one',
		'store_many',
		'product_one',
		'product_many',
		'news_one',
		'news_many',
	];
	private $error = [];

  public function index()
  {
		$this->load->language('module/store_review');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      //seo_url should be written in oc_url_alias, not in oc_setting
      $this->load->model('catalog/url_alias');

			$this->model_setting_setting->editSetting('store_review', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit']     = $this->language->get('text_edit');
		$data['text_enabled']  = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes']      = $this->language->get('text_yes');
		$data['text_no']       = $this->language->get('text_no');

		$data['entry_status']           = $this->language->get('entry_status');
		$data['entry_title']            = $this->language->get('entry_title');
		$data['entry_description']      = $this->language->get('entry_description');
		$data['entry_meta_title']       = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');

		$data['button_save']   = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$this->load->model('module/store_review');

		$this->model_module_store_review->setDBStructure();

		$review_to_transfer = $this->model_module_store_review->getTotalReviewsToTransfer();

		if ($review_to_transfer['review_to_transfer']) {
			$data['tab_settings'] = $this->language->get('tab_settings');
			$data['tab_transfer'] = $this->language->get('tab_transfer');

			$data['text_transfer'] = sprintf($this->language->get('text_transfer'), $review_to_transfer['review_to_transfer']);
			$data['text_warning'] = $this->language->get('text_warning');
			$data['text_loading'] = $this->language->get('text_loading');

			$data['button_transfer'] = $this->language->get('button_transfer');

			$data['review_to_transfer'] = $review_to_transfer['review_to_transfer'];
			$data['total_review_store'] = $review_to_transfer['total_review_store'];

			$data['token'] = $this->session->data['token'];

		}

		foreach (array_keys($this->configuration) as $config) {
			$data['entry'][$config] = $this->language->get('entry_' . $config);
			$data['help'][$config] = $this->language->get('help_' . $config);

			if (isset($this->error[$config])) {
				$data['error'][$config] = $this->error[$config];
			} else {
				$data['error'][$config] = '';
			}

			if (isset($this->request->post['store_review_' . $config])) {
				$data['configuration'][$config] = $this->request->post['store_review_' . $config];
			} else {
				$data['configuration'][$config] = $this->config->get('store_review_' . $config);
			}
		}

		$this->load->model('localisation/language');

    $data['languages'] = $this->model_localisation_language->getLanguages();

		foreach ($this->pages as $page) {
			//fieldsets
			$data['text_description'][$page] = $this->language->get('text_description_' . $page);

			if (isset($this->error['description_' . $page])) {
				$data['error_description'][$page] = $this->error['description_' . $page];
			} else {
				$data['error_description'][$page] = array();
			}

			foreach ($data['languages'] as $language) {
				if (isset($this->request->post['store_review_description_' . $page])) {
						$data['descriptions'][$language['language_id']][$page] = $this->request->post['store_review_description_' . $page][$language['language_id']];
				} elseif ($this->config->get('store_review_description_' . $page)) {
						$data['descriptions'][$language['language_id']][$page] = $this->config->get('store_review_description_' . $page)[$language['language_id']];
				} else {
						$data['descriptions'][$language['language_id']][$page] = [];
				}
			}

		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/store_review', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/store_review', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/store_review', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['store_review_status'])) {
			$data['store_review_status'] = $this->request->post['store_review_status'];
		} else {
			$data['store_review_status'] = $this->config->get('store_review_status');
		}

		if (isset($this->request->post['store_review_limit_module'])) {
			$data['store_review_limit_module'] = $this->request->post['store_review_limit_module'];
		} else {
			$data['store_review_limit_module'] = $this->config->get('store_review_limit_module');
		}

		if (isset($this->request->post['store_review_depth'])) {
			$data['store_review_depth'] = $this->request->post['store_review_depth'];
		} else {
			$data['store_review_depth'] = $this->config->get('store_review_depth');
		}

    if (isset($this->request->post['store_review_keyword_one'])) {
			$data['store_review_keyword_one'] = $this->request->post['store_review_keyword_one'];
		} else {
			$data['store_review_keyword_one'] = $this->config->get('store_review_keyword_one');
		}

		if (isset($this->request->post['store_review_keyword_many'])) {
			$data['store_review_keyword_many'] = $this->request->post['store_review_keyword_many'];
		} else {
			$data['store_review_keyword_many'] = $this->config->get('store_review_keyword_many');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/store_review.tpl', $data));
  }

  protected function validate()
  {
      if (!$this->user->hasPermission('modify', 'module/store_review')) {
          $this->error['warning'] = $this->language->get('error_permission');
      }

      if (((int)$this->request->post['store_review_limit'] <= 0) || ((int)$this->request->post['store_review_limit'] > 20)) {
          $this->error['limit'] = $this->language->get('error_limit');
      }

			if (((int)$this->request->post['store_review_depth'] <= 0) || ((int)$this->request->post['store_review_depth'] > 6)) {
          $this->error['depth'] = $this->language->get('error_depth');
      }

			$this->load->model('catalog/url_alias');
      if (preg_match('/^[a-zA-Z0-9-_]{3,20}+$/', $this->request->post['store_review_keyword_one'])) {
  			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['store_review_keyword_one']);

  			if ($url_alias_info && $url_alias_info['query'] != 'store_review/store_review') {
  				$this->error['keyword_one'] = $this->language->get('error_keyword_unique');
  			}
  		} else {
        $this->error['keyword_one'] = $this->language->get('error_keyword_valid');
      }

			if (preg_match('/^[a-zA-Z0-9-_]{3,20}+$/', $this->request->post['store_review_keyword_many'])) {
  			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['store_review_keyword_many']);

  			if ($url_alias_info && $url_alias_info['query'] != 'store_review/store_review') {
  				$this->error['keyword_many'] = $this->language->get('error_keyword_unique');
  			}
  		} else {
        $this->error['keyword_many'] = $this->language->get('error_keyword_valid');
      }

			foreach ($this->pages as $page) {
				foreach ($this->request->post['store_review_description_' . $page] as $lang => $description) {
	        if ((utf8_strlen($description['title']) < 3) || (utf8_strlen($description['title']) > 255)) {
	            $this->error['description'][$page][$lang]['title'] = $this->language->get('error_title');
	        }

	        if ((utf8_strlen($description['description']) > 1000)) {
	            $this->error['description'][$page][$lang]['description'] = $this->language->get('error_description');
	        }

	        if ((utf8_strlen($description['meta_title']) < 3) || (utf8_strlen($description['meta_title']) > 255)) {
	            $this->error['description'][$page][$lang]['meta_title'] = $this->language->get('error_meta_title');
	        }

	        if ((utf8_strlen($description['meta_description']) < 3) || (utf8_strlen($description['meta_description']) > 300)) {
	            $this->error['description'][$page][$lang]['meta_description'] = $this->language->get('error_meta_description');
	        }
	      }
			}

      return !$this->error;
  }

	public function transferReviews()
	{
		$limit = 500;
		$items = ['store', 'product', 'article'];

		if (isset($this->request->get['item']) && isset($this->request->get['start'])) {
			$this->load->model('module/store_review');

			$this->model_module_store_review->setDBStructure();

			$reviews = [];
			$reviews = $this->model_module_store_review->getReviews($this->request->get['item'], $this->request->get['start'], $limit);
			$this->log->write('count ' . count($reviews) . ' start ' . $this->request->get['start']);
			$this->model_module_store_review->saveReviews($reviews);

			$total_reviews = $this->model_module_store_review->getTotalReviews($this->request->get['item']);

			$next_start = (int)$this->request->get['start'] + $limit;

			if ($next_start < $total_reviews) {
				$json['start'] = $next_start;
				$json['item'] = $this->request->get['item'];
			} else {
				$json['success'] = $this->language->get('success_' . $this->request->get['item']);
				$json['start'] = 0;
				$item_key = array_search($this->request->get['item'], $items) + 1;
				if (isset($items[$item_key])) {
					$json['item'] = $items[$item_key];
					$json['end'] = $this->model_module_store_review->getTotalReviews($items[$item_key]);
				} else {
					$json['item'] = '';
					$json['end'] = 0;
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

  public function install()
  {
		$this->load->model('setting/setting');
		$defautl_data = [];

		foreach ($this->configuration as $key => $value) {
			$defautl_data['store_review_' . $key] = $value;
		}

		$this->model_setting_setting->editSetting('store_review', $defautl_data);

    $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "store_review` (
												`review_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
												`title` varchar(255) NOT NULL,
												`item` varchar(64) NOT NULL,
												`parent_id` int(11) NOT NULL,
												`customer_id` int(11) NOT NULL,
			 									`user_id` int(11) NOT NULL DEFAULT '0',
												`author` varchar(64) NOT NULL,
												`email` varchar(96) NOT NULL,
												`text` text NOT NULL,
												`rating` int(1) NOT NULL,
												`review_like` int(11) NOT NULL DEFAULT '0',
												`review_dislike` int(11) NOT NULL DEFAULT '0',
												`status` tinyint(1) NOT NULL DEFAULT '0',
												`on_main` tinyint(1) NOT NULL DEFAULT '0',
												`date_added` datetime NOT NULL,
												`date_modified` datetime NOT NULL,
			 									`old_id` int(11) NOT NULL
                      ) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");

		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "store_review_hierarchy` (
												`parent_id` int(11) NOT NULL,
												`child_id` int(11) NOT NULL,
												 `depth` int(11) NOT NULL DEFAULT '0',
												  PRIMARY KEY(`parent_id`, `child_id`)
									     ) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");

  }

  public function uninstall()
  {
    $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `code` = 'store_review'");
    $this->db->query("DELETE FROM `" . DB_PREFIX . "url_alias` WHERE `query` = 'store_review/store_review'");
    $this->db->query("DROP TABLE `" . DB_PREFIX . "store_review`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "store_review_hierarchy`");
  }
}
