<?php
/**
* @version     3.1
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2018 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

class ControllerModuleAjaxZoom extends Controller {

	private $az_pic_dirs = array('cache', 'zoomgallery', 'zoommap', 'zoomthumb', 'zoomtiles_80', 'tmp', '360', 'json');

	// AZ 5
	private $_categories = array();
	private $_lang = array();

	// AZ 5
	private $_fields_list = array();
	private $mouseover_settings = array();

	public function __construct($registry) {

		parent::__construct($registry);

		$this->load->model('catalog/ajaxzoom');
		$this->model = $this->model_catalog_ajaxzoom;

		$this->language->load('module/ajaxzoom');

		if(method_exists($this->language, 'all')) {
			$this->_lang = $this->language->all();
		} else {
			$this->_lang = $this->languageAll(); // for 1.5
		}

	}

	public function languageAll() {
		$_ = array();
		$lang = $this->getLanguage($this->config->get('config_admin_language'));
		$file = DIR_LANGUAGE . $lang['directory'] . '/module/ajaxzoom.php';
		if (file_exists($file)) {
			require($file);
		}
		return $_;
	}

	public function getLanguage($code) {
		$tmp = $this->db->query("SELECT * FROM `".$this->model->tablePrefix()."language` WHERE code = '" . $code . "'")->rows;
		return $tmp[0];
	}

	public function install() {
		$base_path = $this->model->getDir();
		$base_path_replace = preg_replace('%/$%', '', $base_path);
		@chmod($base_path_replace, 0775);
		@chmod($base_path . 'pic', 0775);

		//$this->checkIoncube();
		$this->createDir();

		// register events
		if ($this->is2()) {
			$this->load->model('extension/event');
			$this->model_extension_event->addEvent('ajaxzoom',  'pre.admin.product.edit', 'module/ajaxzoom/editProductPre');
			$this->model_extension_event->addEvent('ajaxzoom', 'post.admin.product.edit', 'module/ajaxzoom/editProductPost');
			$this->model_extension_event->addEvent('ajaxzoom',  'pre.admin.product.delete', 'module/ajaxzoom/deleteProductPre');
		}


		$this->createConfig();
		$this->createDatabaseTable();
		$this->uploadAxZm();
	}

	public function is2() {
		if(version_compare ( VERSION , '2.0' ) >= 0) {
			return true;
		}
	}

	public function uninstall() {

		if (version_compare ( VERSION , '2.0' ) >= 0) {
			$this->load->model('extension/event');
			$this->model_extension_event->deleteEvent('ajaxzoom');
		}

		$this->removeDir();
		$this->removeDatabaseTable();
	}

	public function checkIoncube() {
		$extensions = get_loaded_extensions();
		$ioncube = false;
		$sourceguardian = false;

		foreach ($extensions as $v) {
			if (stristr($v, 'ioncube')) {
				$ioncube = true;
			}

			if (stristr($v, 'sourceguardian')) {
				$sourceguardian = true;
			}
		}

		if ($ioncube || $sourceguardian) {
			return true;
		}

		return false;
	}

	public function createConfig() {
		$this->load->model('setting/setting');
		$settingsToSave = array();

		$fileds = $this->getFieldList();

		foreach($fileds as $key => $data) {
			$settingsToSave[$key] = $data['default'];
		}

		$this->model_setting_setting->editSetting('ajaxzoom', $settingsToSave);
	}

	public function updateCropField(){
		$crop_field_there = false;
		$hotspot_field_there = false;
		$check_crop_field = $this->db->query('SHOW FIELDS FROM `ajaxzoom360`')->rows;
		foreach($check_crop_field as $k=>$v){
			if ($v['Field'] == 'crop'){
				$crop_field_there = true;
			}
			if ($v['Field'] == 'hotspots'){
				$hotspot_field_there = true;
			}
		}
		if (!$crop_field_there){
			$this->db->query('ALTER TABLE `ajaxzoom360` ADD `crop` TEXT NOT NULL');
		}
		if (!$hotspot_field_there){
			$this->db->query('ALTER TABLE `ajaxzoom360` ADD `hotspots` TEXT NOT NULL');
		}
	}

	public function createDatabaseTable() {
		/* $this->db->query('DROP TABLE IF EXISTS `ajaxzoom360`'); */
		$this->db->query('CREATE TABLE IF NOT EXISTS `ajaxzoom360` (`id_360` int(11) NOT NULL AUTO_INCREMENT,
				`id_product` int(11) NOT NULL, `name` varchar(255) NOT NULL, `num` int(11) NOT NULL DEFAULT \'1\',
				`settings` text NOT NULL, `status` tinyint(1) NOT NULL DEFAULT \'0\',
				`combinations` text NOT NULL, `crop` TEXT NOT NULL, `hotspots` TEXT NOT NULL,
				PRIMARY KEY (`id_360`))
				ENGINE=InnoDB
				DEFAULT CHARSET=utf8;');

		$this->updateCropField();

		$this->db->query('CREATE TABLE IF NOT EXISTS `ajaxzoom360set` (`id_360set` int(11) NOT NULL AUTO_INCREMENT, `id_360` int(11) NOT NULL,
				`sort_order` int(11) NOT NULL, PRIMARY KEY (`id_360set`))
				ENGINE=InnoDB
				DEFAULT CHARSET=utf8
				AUTO_INCREMENT=1;');

		$this->db->query('CREATE TABLE IF NOT EXISTS `ajaxzoomproducts` (`id_product` int(11) NOT NULL,
				PRIMARY KEY (`id_product`))
				ENGINE=InnoDB
				DEFAULT CHARSET=utf8;');

		$this->db->query('CREATE TABLE IF NOT EXISTS `ajaxzoomproductsimages` ( `id_product` int(11) NOT NULL,
				`images` text NOT NULL,
				PRIMARY KEY (`id_product`))
				ENGINE=InnoDB
				DEFAULT CHARSET=utf8;');

		/*
		$this->db->query('CREATE TABLE IF NOT EXISTS `ajaxzoomlicenses` (`id_license` int(11) NOT NULL AUTO_INCREMENT,
				`domain` varchar(255) NOT NULL,
				`key` text NOT NULL,
				`type` varchar(255) NOT NULL,
				`error200` text NOT NULL,
				`error300` text NOT NULL,
				PRIMARY KEY (`id_license`))
				ENGINE=InnoDB
				DEFAULT CHARSET=utf8;');
		*/

	}

	private function removeDatabaseTable() {
		$this->db->query('DROP TABLE IF EXISTS `ajaxzoom360`');
		$this->db->query('DROP TABLE IF EXISTS `ajaxzoom360set`');
		$this->db->query('DROP TABLE IF EXISTS `ajaxzoomproducts`');
		$this->db->query('DROP TABLE IF EXISTS `ajaxzoomproductsimages`');
		//$this->db->query('DROP TABLE IF EXISTS `ajaxzoomlicenses`');
	}

	public function createDir() {
		$res = true;
		$base_path = $this->model->getDir();

		foreach ($this->az_pic_dirs as $dir) {
			$path = $base_path.'pic/'.$dir;

			if (!file_exists($path)) {
				$res = mkdir($path, 0775);
				@chmod($path, 0775);
			} else {
				if (!is_writable($path)) {
					$res = @chmod($path, 0775);
					if (!$res) {
						break;
					}
				}
			}
		}

		if (!file_exists($base_path.'pic/360/.htaccess')) {
			file_put_contents($base_path.'pic/360/.htaccess', 'deny from all');
		}

		@chmod($base_path.'zip', 0775);

		return $res;
	}

	public function uploadAxZm() {

		set_time_limit(600);

		$dir = $this->model->getDir();

		if (!file_exists($dir.'axZm') && ini_get('allow_url_fopen')) {

			$remoteFileContents = file_get_contents('http://www.ajax-zoom.com/download.php?ver=latest&ref=opencart');
			$localFilePath = $dir.'pic/tmp/jquery.ajaxZoom_ver_latest.zip';

			if ($remoteFileContents !== false) {
				file_put_contents($localFilePath, $remoteFileContents);

				$path = $dir.'pic/tmp/';

				$zip = new ZipArchive;
				$res = $zip->open($localFilePath);
				if ($res === TRUE) {
					$zip->extractTo("$path/");
					$zip->close();
				}

				rename($dir.'pic/tmp/axZm', $dir.'axZm');

				$this->model->deleteDirectory($dir.'pic/tmp');

				mkdir($dir.'pic/tmp', 0775);
				@chmod($dir.'pic/tmp', 0775);
			}
		}
	}

	public function removeDir() {
		$base_path = $this->model->getDir();

		foreach ($this->az_pic_dirs as $dir) {
			$path = $base_path.'pic/'.$dir;
			$this->model->deleteDirectory($path);
		}

		return true;
	}

	public function deleteProductPre($id_product) {
		$images = $this->model->getProductImages($id_product);
		foreach($images as $image) {
			$this->model->deleteImageAZcache($image);
		}

		$sets = $this->model->getSets($id_product);

		foreach ($sets as $set) {
			$this->model->deleteSet($set['id_360set']);
		}
	}

	public function editProductPre($data) {
		$query = $this->db->query('REPLACE INTO `ajaxzoomproductsimages` (id_product, images)
			VALUES(\''. (int)$this->request->get['product_id'] .'\', \'' . implode(',', $this->model->getProductImages($this->request->get['product_id'])) . '\')');
	}

	public function editProductPost($data) {
		$tmp = $this->db->query('SELECT * FROM `ajaxzoomproductsimages` WHERE id_product = ' . (int)$this->request->get['product_id'])->rows;
		$array2 = $this->model->getProductImages($this->request->get['product_id']);
		if($tmp[0]['images'] != implode(',', $array2)) {
			$array1 = explode(',', $tmp[0]['images']);
			$result = array_diff($array1, $array2);
			if(!empty($result)) {
				foreach($result as $image) {
					$this->model->deleteImageAZcache($image);
				}
			}
		}
	}

	public function ajax() {
		$method = preg_replace('/[^a-z0-9_]/i', '', $this->request->get['action']) . 'Action';
		$this->$method();
		exit;
	}

	public function addSetAction() {
		$id_product = (int)$this->request->get['id_product'];
		$name = $this->request->get['name'];
		if(empty($name)) {
			$name = 'Unnamed_'.uniqid(getmypid());
		}
		$existing = isset($this->request->get['existing']) ? intval($this->request->get['existing']) : false;
		$zip = $this->request->get['zip'];
		$delete = $this->request->get['delete'];
		$arcfile = $this->request->get['arcfile'];
		$new_id = '';
		$new_name = '';
		$new_settings = '';

		if (!empty($existing)) {
			$id_360 = $existing;
			$query = $this->db->query('SELECT * FROM `ajaxzoom360`
				WHERE id_360 = '.(int)$id_360);
			foreach ($query->rows as $result) {
				$tmp[] = $result;
			}

			$name = $tmp[0]['name'];
		} else {
			$new_settings = $settings = '{"position":"first","spinReverse":"true","spinBounce":"false","spinDemoRounds":"3","spinDemoTime":"4500"}';

			$query = $this->db->query('INSERT INTO `ajaxzoom360` (id_product, name, settings, status)
				VALUES(\''.$id_product.'\', \''.$this->db->escape($name).'\', \''.$settings.'\', \''.($zip == 'true' ? 1 : 0).'\')');

			$id_360 = $this->db->getLastId();
			$new_id = $id_360;
			$new_name = $name;
		}

		$query = $this->db->query('INSERT INTO `ajaxzoom360set` (id_360, sort_order)
			VALUES(\''.$id_360.'\', 0)');

		$id_360set = $this->db->getLastId();

		$sets = array();

		if ($zip == 'true') {
			$sets = $this->model->addImagesArc($arcfile, $id_product, $id_360, $id_360set, $delete);
		}

		die($this->model->jsonEncode(array(
			'status' => '0',
			'name' => $name,
			'path' => $this->model->getUrl().'resources/img/no_image-100x100.jpg',
			'sets' => $sets,
			'id_360' => $id_360,
			'id_product' => $id_product,
			'id_360set' => $id_360set,
			'confirmations' => array($this->l('The image set was successfully added.')),
			'new_id' => $new_id,
			'new_name' => $new_name,
			'new_settings' => urlencode($new_settings)
		)));
	}

	public function getImagesAction() {
		$id_product = (int)$this->request->get['id_product'];
		$id_360set = (int)$this->request->get['id_360set'];
		$images = $this->model->get360Images($id_product, $id_360set);

		die($this->model->jsonEncode(array(
			'status' => 'ok',
			'id_product' => $id_product,
			'id_360set' => $id_360set,
			'images' => $images
		)));
	}

	public function addProductImage360Action() {
		$id_product = (int)$this->request->get['id_product'];
		$id_360set = (int)$this->request->get['id_360set'];

		$id_360 = $this->model->getSetParent($id_360set);
		$folder = $this->model->createProduct360Folder($id_product, $id_360set);

		$path = $this->model->getDir().'pic/tmp/'.$this->request->get['qqfile'];

		$this->model->upload($path);

		$files = array(array(
			'name' => $this->request->get['qqfile'],
			'save_path' => $path,
			'id_product' => $id_product,
			'id_360' => $id_360,
			'id_360set' => $id_360set,
		));

		$fname = 'file360';

		foreach ($files as &$file) {
			$file['name'] = $id_product.'_'.$id_360set.'_'.$file['name'];
			$file['name'] = $this->model->imgNameFilter($file['name']);

			$tmp = explode('.', $file['name']);
			$ext = end($tmp);
			$name = preg_replace('|\.'.$ext.'$|', '', $file['name']);
			$dst = $folder.'/'.$file['name'];
			rename($file['save_path'], $dst);

			$thumb = $this->model->getUrl().'axZm/zoomLoad.php';
			$thumb .= '?azImg='.$this->model->getUri().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set.'/'.$file['name'].'&width=100&height=100&qual=90';

			$file['status'] = 'ok';
			$file['id'] = $name;
			$file['id_product'] = $id_product;
			$file['id_360'] = $id_360;
			$file['id_360set'] = $id_360set;
			$file['path'] = $thumb;
		}

		die($this->model->jsonEncode($files[0]));
	}

	public function deleteProductImage360Action() {
		$id_image = $this->request->get['id_image'];
		$id_product = (int)$this->request->get['id_product'];
		$id_360set = (int)$this->request->get['id_360set'];
		$id_360 = $this->model->getSetParent($id_360set);
		$tmp = explode('&amp;', $this->request->get['ext']);
		$ext = $tmp[0];
		$filename = $id_image.'.'.$ext;

		$dst = $this->model->getDir().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set.'/'.$filename;
		unlink($dst);

		$this->model->deleteImageAZcache($filename);

		die($this->model->jsonEncode(array(
			'status' => 'ok',
			'content' => (object)array('id' => $id_image),
			'confirmations' => array($this->l('The image was successfully deleted.'))
		)));
	}

	public function deleteSetAction() {
		$id_360set = (int)$this->request->get['id_360set'];
		$id_360 = $this->model->getSetParent($id_360set);
		$id_product = $this->model->getSetProduct($id_360);

		$this->model->deleteSet($id_360set);

		die($this->model->jsonEncode(array(
			'id_360set' => $id_360set,
			'id_360' => $id_360,
			'path' => $this->model->getDir().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set,
			'removed' => ($this->db->query('SELECT * FROM `ajaxzoom360`
				WHERE id_360 = '.$id_360)->num_rows ? 0 : 1),
			'confirmations' => array($this->l('The 360 image set was successfully removed.'))
		)));
	}

	public function set360StatusAction() {
		$status = (int)$this->request->get['status'];
		$id_360 = (int)$this->request->get['id_360'];
		$this->model->set360status($id_360, $status);

		die($this->model->jsonEncode(array(
			'status' => 'ok',
			'confirmations' => array($this->l('The status has been updated.'))
		)));
	}

	public function getCropJsonAction()
	{
		$id_360 = (int)$this->request->get['id_360'];

		$this->updateCropField();

		$row = $this->db->query('SELECT * FROM `ajaxzoom360` WHERE id_360 = '.$id_360.' LIMIT 1')->rows;

		if ($row[0]['crop']){
			die(stripslashes($row[0]['crop']));
		}else{
			die('[]');
		}
	}

	public function setCropJsonAction()
	{
		$id_360 = (int)$this->request->get['id_360'];
		//$json = $this->request->post['json'];

		$json = $_POST['json'];

		if (!$this->is2()){
			$json = html_entity_decode($json);
		}

		$json = preg_replace('/\s+/S', " ", $json);

		if (!get_magic_quotes_gpc()){
			$json = addslashes($json);
		}

		$this->db->query('UPDATE `ajaxzoom360` SET crop = \''.($json).'\' WHERE id_360 = '.(int)$id_360);

		die($this->model->jsonEncode(array(
			'status' => $this->db->countAffected()
		)));
	}

	public function getHotspotJsonAction()
	{
		$id_360 = (int)$this->request->get['id_360'];

		$this->updateCropField();

		$row = $this->db->query('SELECT * FROM `ajaxzoom360` WHERE id_360 = '.$id_360.' LIMIT 1')->rows;

		if ($row[0]['hotspots']){
			die(stripslashes($row[0]['hotspots']));
		}else{
			die('{}');
		}
	}

	public function setHotspotJsonAction()
	{
		$id_360 = (int)$this->request->get['id_360'];
		//$json = $this->request->post['json'];

		$json = $_POST['json'];

		if (!$this->is2()){
			$json = html_entity_decode($json);
		}

		$json = preg_replace('/\s+/S', " ", $json);

		if (!get_magic_quotes_gpc()){
			$json = addslashes($json);
		}

		$this->db->query('UPDATE `ajaxzoom360` SET hotspots = \''.($json).'\' WHERE id_360 = '.(int)$id_360);

		die($this->model->jsonEncode(array(
			'status' => $this->db->countAffected()
		)));
	}

	public function saveSettingsAction() {
		$id_product = (int)$this->request->get['id_product'];
		$id_360 = (int)$this->request->get['id_360'];
		$active = (int)$this->request->get['active'];
		$names = explode('|', $this->request->get['names']);
		$values = explode('|', $this->request->get['values']);
		$combinations = explode('|', $this->request->get['combinations']);
		$count_names = count($names);
		$settings = array();

		for ($i = 0; $i < $count_names; $i++) {
			$key = $names[$i];
			$value = $values[$i];
			if ($key != 'name_placeholder' && !empty($key)) {
				$settings[$key] = $value;
			}
		}

		$this->db->query('UPDATE `ajaxzoom360`
			SET settings = \''.$this->model->jsonEncode($settings).'\', combinations = \''.implode(',', $combinations).'\'
			WHERE id_360 = '.(int)$id_360);

		// update dropdown
		$sets_groups = $this->model->getSetsGroups($id_product);
		$select = '<select id="id_360" name="id_360"><option value="">Select 360/3D View</option>';
		if(!empty($sets_groups)) {
			foreach ($sets_groups as $group) {
				$select .= '<option value="'.$group['id_360'].'" ';
				$select .= 'data-settings="'.urlencode($group['settings']).'" ';
				$select .= 'data-combinations="['.urlencode($group['combinations']).']">'.$group['name'].'</option>';
			}
		}
		$select .= '</select>';

		// active/not active
		$this->db->query('DELETE FROM `ajaxzoomproducts`
			WHERE id_product = '.(int)$id_product);

		if ($active == 0) {
			$this->db->query('INSERT INTO `ajaxzoomproducts` (id_product) VALUES('.(int)$id_product.')');
		}

		die($this->model->jsonEncode(array(
			'status' => 'ok',
			'select' => $select,
			'id_product' => $id_product,
			'id_360' => $id_360,
			'confirmations' => array($this->l('The settings has been updated.'))
		)));
	}

	public function product() {
		$id_product = intval($this->request->get['product_id']);

		$this->load->model('catalog/product');

		$sets_groups = $this->model->getSetsGroups($id_product);

		if (!is_array($sets_groups)) {
			$sets_groups = array();
		}

		$data = array(
			'heading_title' => $this->_lang['heading_title'],
			//'header' => $this->load->controller('common/header'),
			//'footer' => $this->load->controller('common/footer'),
			//'column_left' => $this->load->controller('common/column_left'),
			'breadcrumbs' => $this->_breagcrumbs(),
			'url_back' => $this->url->link('module/ajaxzoom/products', 'token=' . $this->session->data['token'], 'SSL'),
			'url_ajax' => str_replace('&amp;', '&', $this->url->link('module/ajaxzoom/ajax', 'token=' . $this->session->data['token'], 'SSL')),
			'url_ajax_c' => $this->url->link('module/ajaxzoom/ajax', '', 'SSL'),
			'product' => $this->model_catalog_product->getProduct($id_product),
			'active' => $this->model->isProductActive($id_product),
			'token' => $this->session->data['token'],
			'base_url' => $this->model->getUrl(),
			'base_uri' => $this->model->getUri(),
			'id_product' => $id_product,
			'sets_groups' => $sets_groups,
			'sets' => $this->model->getSets($id_product),
			'files' => $this->model->getArcList(),
			'max_image_size' => 99999999 / 1024 / 1024,
			'table' => '',
			'images' => array(),
			'countImages' => 0,
			'is2' => $this->is2(),
			'iframe' => isset($this->request->get['iframe']) ? intval($this->request->get['iframe']) : ''
			);

		$data = array_merge($data, $this->_lang);

		if($this->is2()) {
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			$this->response->setOutput($this->load->view('module/ajaxzoom/product.tpl', $data));
		} else {
			$this->template = 'module/ajaxzoom/15/product.tpl';
			$this->data = array_merge($data, $this->data);
			$this->load->model('design/layout');
			$this->data['layouts'] = $this->model_design_layout->getLayouts();
			$this->children = array(
				'common/header',
				'common/footer'
			);

			$this->response->setOutput($this->render());
		}
	}

	public function products() {

		$data = array(
			'heading_title' => $this->_lang['heading_title'],
			//'header' => $this->load->controller('common/header'),
			//'footer' => $this->load->controller('common/footer'),
			//'column_left' => $this->load->controller('common/column_left'),
			'breadcrumbs' => $this->_breagcrumbs(),
			'products' => $this->model->getProducts(),
			'url_back' => $this->url->link('module/ajaxzoom', 'token=' . $this->session->data['token'], 'SSL'),
			'base_url' => $this->model->getUrl(),
			'is2' => $this->is2()
			);

		$data = array_merge($data, $this->_lang);

		if($this->is2()) {
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			$this->response->setOutput($this->load->view('module/ajaxzoom/products.tpl', $data));
		} else {
			$this->template = 'module/ajaxzoom/15/products.tpl';
			$this->data = array_merge($data, $this->data);
			$this->load->model('design/layout');
			$this->data['layouts'] = $this->model_design_layout->getLayouts();
			$this->children = array(
				'common/header',
				'common/footer'
			);

			$this->response->setOutput($this->render());
		}
	}

	// AZ 5
  	public function index()
  	{

		$this->load->model('setting/setting');

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate() && !isset($_POST['reset'])) {
			$settingsToSave = array();

			foreach ($_POST as $key => $value) {
				if ($key == 'licenses') {
					$settingsToSave['ajaxzoom_LICENSE'] = $this->saveLicensesValue();
				} else {
					$value = htmlspecialchars_decode($value);
					$settingsToSave[$key] = $value;
				}
			}

			$this->model_setting_setting->editSetting('ajaxzoom', $settingsToSave);

			$this->session->data['success'] = $this->_lang['text_success'];

			if ($this->is2()) {
				$this->response->redirect($this->url->link('module/ajaxzoom', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('module/ajaxzoom', 'token=' . $this->session->data['token'], 'SSL'));
			}
		} elseif ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate() && isset($_POST['reset'])) {
			$this->createConfig();
			if ($this->is2()) {
				$this->response->redirect($this->url->link('module/ajaxzoom', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('module/ajaxzoom', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}

		$data = array(
			'licenses' => '[]',
			'breadcrumbs' => $this->_breagcrumbs(),
			'action' => $this->url->link('module/ajaxzoom', 'token=' . $this->session->data['token'], 'SSL'),
			'url_products' => $this->url->link('module/ajaxzoom/products', 'token=' . $this->session->data['token'], 'SSL'),
			'cancel' => $this->url->link('module', 'token=' . $this->session->data['token'], 'SSL'),
			'token' => $this->session->data['token'],
			'error_warning' => isset($this->error['warning']) ? $this->error['warning'] : '',
			//'header' => $this->load->controller('common/header'),
			//'column_left' => $this->load->controller('common/column_left'),
			//'footer' => $this->load->controller('common/footer'),
			'form_data' => $this->renderForm(),
			'is2' => $this->is2()
		);

		$this->loadSetting($data, 'ajaxzoom_status');
		$this->loadSetting($data, 'ajaxzoom_LICENSE');

		$this->load->model('catalog/ajaxzoom');

		$data = array_merge($data, $this->_lang);

		if($this->is2()) {
			$this->template = 'module/ajaxzoom.tpl';
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			$this->response->setOutput($this->load->view('module/ajaxzoom.tpl', $data));
		} else {
			$this->template = 'module/ajaxzoom15.tpl';
			$this->data = array_merge($data, $this->data);
			$this->load->model('design/layout');
			$this->data['layouts'] = $this->model_design_layout->getLayouts();
			$this->children = array(
				'common/header',
				'common/footer'
			);

			$this->response->setOutput($this->render());
		}
	}

	private function _breagcrumbs() {
		$breadcrumbs = array();

		$breadcrumbs[] = array(
			'text'      => $this->_lang['text_home'],
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$breadcrumbs[] = array(
			'text'      => $this->_lang['text_module'],
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$breadcrumbs[] = array(
			'text'      => $this->_lang['heading_title'],
			'href'      => $this->url->link('module/ajaxzoom', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		if(in_array($this->request->get['route'], array('module/ajaxzoom/products', 'module/ajaxzoom/product'))) {
			$breadcrumbs[] = array(
				'text'      => $this->_lang['text_products'],
				'href'      => $this->url->link('module/ajaxzoom/products', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			);
		}

		if($this->request->get['route'] == 'module/ajaxzoom/product') {
			$breadcrumbs[] = array(
				'text'      => $this->_lang['text_360views'],
				'href'      => $this->url->link('module/ajaxzoom/product', 'product_id=' . $this->request->get['product_id'] . '&token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			);
		}

		return $breadcrumbs;
	}

	public function saveLicensesValue() {
		$licenses = array();
		$tmp = $_POST['licenses'];
		$count_domain = count($tmp['domain']);

		for ($i = 0; $i < $count_domain; $i++) {
			if ($tmp['domain'][$i] == 'domain_placeholder') continue;

			$licenses[] = array(
				'domain' => $tmp['domain'][$i],
				'type' => $tmp['type'][$i],
				'key' => $tmp['key'][$i],
				'error200' => $tmp['error200'][$i],
				'error300' => $tmp['error300'][$i]
			);
		}

		return json_encode($licenses);
	}

	public function loadSetting(&$data, $name, $defaultIfConfigEmpty = null) {
		if (isset($this->request->post[$name])) {
			$data[$name] = $this->request->post[$name];
		} else {
			$data[$name] = $this->config->get($name);

			if (!isset($data[$name]) && $defaultIfConfigEmpty) {
				$data[$name] = $defaultIfConfigEmpty;
			}
		}
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/ajaxzoom')) {
			$this->error['warning'] = $this->_lang['error_permission'];
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	// AZ 5
	public function renderForm() {
		$html = '';
		$cats = array();
		$field_list = $this->getFieldList();
		foreach($field_list as $key => $data) {
			$category = empty($data['category']) ? 'general' : $data['category'];

			$tmp = array();
			$this->loadSetting($tmp, $key, $data['default']);

			if(!isset($cats[$category])) $cats[$category] = '';
			$cats[$category] .= $this->formField($key, $tmp[$key], $data);
		}

		$data = array();
		foreach($this->_categories as $key => $category) {
			$data[$key] = array(
				'title' => $category['title'],
				'expanded' => $category['expanded'],
				'fields' => isset($cats[$key]) ? $cats[$key] : ''
			);
		}

		return $data;
	}

	public function formField($key, $value, $data) {
		if(empty($value)) {
			$value = $data['default'];
		}
		if(empty($data['type'])) {
			$data['type'] = 'text';
		}
		switch ($data['type']) {
			case 'switch':
				$field = '<select name="' . $key . '" id="' . $key . '" class="form-control">';
				foreach (array('true' => 'true', 'false' => 'false') as $option_title => $option_value) {
					$field .= '<option value="' . $option_value . '" ' . ($option_value == $value ? 'selected' : '') . '>' . $option_title . '</option>';
				}
				$field .= '</select>';
				break;

			case 'select':
				$field = '<select name="' . $key . '" id="' . $key . '" class="form-control">';
				foreach ($data['values'] as $option_value => $option_title) {
					$field .= '<option value="' . $option_value . '" ' . ($option_value == $value ? 'selected' : '') . '>' . $option_title . '</option>';
				}
				$field .= '</select>';
				break;

			case 'textarea':
				$style = '';
				if(!empty($data['textareaHeight'])) {
					$style .= 'height:' . $data['textareaHeight'] . ';';
				}
				$field = '<textarea name="' . $key . '" id="' . $key . '" class="form-control" style="width:100%;min-height:100px;" style="' . $style . '">' . $value . '</textarea>';
				break;

			default:
				$field = '<input type="text" name="' . $key . '" id="' . $key . '" value="' . $value . '" class="form-control">';
				break;
		}
		if($this->is2()) {
			return '
				<div class="form-group">
					<label class="col-sm-2 control-label" for="' . $key . '">' . (isset($this->_lang["text_title_$key"]) ? $this->_lang["text_title_$key"] : $data['title']) . '</label>
					<div class="col-sm-10">
						' . $field . '
						<p class="help-block">' . (isset($this->_lang["text_comment_$key"]) ? $this->_lang["text_comment_$key"] : $data['comment']) . '</p>
					</div>
				</div>
			';
		} else {
			return '
                    <tr>
                        <td valign="top">' . (isset($this->_lang["text_title_$key"]) ? $this->_lang["text_title_$key"] : $data['title']) . '</td>
                        <td>' . $field . '
                        <p class="help-block">' . (isset($this->_lang["text_comment_$key"]) ? $this->_lang["text_comment_$key"] : $data['comment']) . '</p>
                        </td>
                    </tr>
			';
		}
	}

	public function l($string) {
		return $string;
	}

	// AZ 5
	public function configVendor()
	{
		return array(
			'oneSrcImg' => true,
			'heightRatioOneImg' => 1.0,
			'width' => 1200,
			'height' => 1200,
			'thumbSliderPosition' => 'bottom',
			'galleryAxZmThumbSliderParamHorz' => '{
	"thumbLiStyle": {
		"borderRadius": 3
	},
	"btn": true,
	"btnClass": "axZmThumbSlider_button_new",
	"btnHidden": true,
	"btnOver": false,
	"scrollBy": 1,
	"centerNoScroll": true
}',
			'galleryAxZmThumbSliderParamVert' => '{
	"thumbLiStyle": {
		"borderRadius": 3
	},
	"btn": true,
	"btnClass": "axZmThumbSlider_button_new",
	"btnHidden": true,
	"btnOver": false,
	"scrollBy": 1,
	"centerNoScroll": true
}',
			'axZmCallBacks' => '{
onFullScreenReady: function() {
	// Here you can place you custom code
}
}',
			'cropAxZmThumbSliderParam' => '{
	"btn": true,
	"btnClass": "axZmThumbSlider_button_new",
	"btnHidden": true,
	"centerNoScroll": true,
	"thumbImgWrap": "azThumbImgWrapRound",
	"scrollBy": 1
}',
			'zoomWidth' => 'h1|+15',
			'zoomHeight' => '#axZm_mouseOverWithGalleryContainer',
			'showTitle' => true,
			'autoFlip' => 120,
			'fullScreenApi' => true,
			'prevNextArrows' => true,
			'cropSliderThumbAutoMargin' => 7,
			'fsAxZmThumbSliderParam' => '{
	"scrollBy": 1,
	"btn": true,
	"btnClass": "axZmThumbSlider_button_new",
	"btnLeftText": null,
	"btnRightText": null,
	"btnHidden": true,
	"pressScrollSnap": true,
	"centerNoScroll": true,
	"wrapStyle": {
		"borderWidth": 0
	}
}'
		);
	}

	// AZ 5
	public function initAzMouseoverSettings()
	{
		if (!$this->mouseover_settings || empty($this->mouseover_settings)) {
			include dirname(DIR_SYSTEM).'/ajaxzoom/AzMouseoverSettings.php';
			$this->mouseover_settings = new AzMouseoverSettings(array(
				'config_vendor' => $this->configVendor(),
				'vendor' => 'Opencart',
				'exclude_opt_vendor' => array(
					'axZmPath',
					'lang',
					'images',
					'images360',
					'videos',
					'enableNativeSlider',
					'enableCssInOtherPages',
					'default360settingsEmbed',
					'defaultVideoYoutubeSettings',
					'defaultVideoVimeoSettings',
					'defaultVideoDailymotionSettings',
					'defaultVideoVideojsSettings',
					'defaultVideoVideojsJS'
				),
				'exclude_cat_vendor' => array('video_settings'),
				'config_extend' => array(
					'appendToContainer' => array(
						'prefix' => 'ajaxzoom',
						'important' => true,
						'type' => 'string',
						'isJsObject' => false,
						'isJsArray' => false,
						'display' => 'text',
						'height' => null,
						'default' => '',
						'options' => null,
						'comment' => array(
							'EN' => '
								Selector to append AJAX-ZOOM to a specific container on product detail page.
								So in case there is AJAX-ZOOM output in frontend but AJAX-ZOOM is not seen,
								this option might help. Open dev tools (press e.g. F12 in Chrome browser and select "Elemets" tab)
								inpect the DOM elements and choose a container where you would like AJAX-ZOOM
								to be appended.
							',
							'DE' => '
								Selector to append AJAX-ZOOM to a specific container on product detail page.
								So in case there is AJAX-ZOOM output in frontend but AJAX-ZOOM is not seen,
								this option might help. Open dev tools (press e.g. F12 in Chrome browser and select "Elemets" tab)
								inpect the DOM elements and choose a container where you would like AJAX-ZOOM
								to be appended.
							'
						)
					),
					'appendToContCss' => array(
						'prefix' => 'ajaxzoom',
						'important' => true,
						'type' => 'string',
						'isJsObject' => false,
						'isJsArray' => false,
						'display' => 'text',
						'height' => null,
						'default' => '',
						'options' => null,
						'comment' => array(
							'EN' => '
								Additional CSS to the container where AJAX-ZOOM will be appended to.
								Must be valid JSON string, e.g. {"height": "auto", "width": "100%"}
							',
							'DE' => '
								Additional CSS to the container where AJAX-ZOOM will be appended to.
								Must be valid JSON string, e.g. {"height": "auto", "width": "100%"}
							'
						)
					),
					'appendToOnDocReady' => array(
						'prefix' => 'ajaxzoom',
						'important' => true,
						'type' => 'bool',
						'isJsObject' => false,
						'isJsArray' => false,
						'display' => 'switch',
						'height' => null,
						'default' => false,
						'options' => null,
						'comment' => array(
							'EN' => '
								Use jQuery document ready function to init AJAX-ZOOM.
							',
							'DE' => '
								Use jQuery document ready function to init AJAX-ZOOM.
							'
						)
					)
				)
			));
		}
	}

	// AZ 5
	public function convertSelectField($f)
	{
		$arr = array();
		foreach ($f as $k => $v) {
			$arr[$v[0]] = strtolower(var_export($v[1], true));
		}
		return $arr;
	}

	// AZ 5
	public function getFieldList()
	{
		if (!empty($this->_fields_list)) {
			return $this->_fields_list;
		}

		$this->initAzMouseoverSettings();
		$cfg = $this->mouseover_settings->getConfig();
		$cat = $this->mouseover_settings->getCategories();

		$current_cat = '';
		$field_map = array(
			'textarea' => 'textarea',
			'text' => 'text',
			'switch' => 'switch',
			'select' => 'select'
		);

		$this->az_opt_prefix = 'ajaxzoom';
		$this->_categories = array(
			'license' => array(
				'expanded' => 0,
				'title' => 'Licenses'
			)
		);

		foreach ($cfg as $k => $v) {
			$varname = $this->az_opt_prefix.'_'.strtoupper($k);
			$category = $v['category'];

			if ($category != $current_cat) {
				$current_cat = $category;
				if (!array_key_exists($current_cat, $this->_categories)) {
					$this->_categories[$current_cat] = array(
						'title' => $this->mouseover_settings->cleanComment($cat[$category]['title']['EN']),
						'expanded' => 0
					);
				}
			}

			$arr_option = array();
			$arr_option['title'] = $k;
			$arr_option['category'] = $category;
			$arr_option['type'] = $field_map[$v['display']];
			$arr_option['comment'] = $this->mouseover_settings->cleanComment($v['comment']['EN']);

			if (isset($v['default'])) {
				if ($v['default'] === true || $v['default'] === false || $v['default'] === null) {
					$arr_option['default'] = strtolower(var_export($v['default'], true));
				} else {
					$arr_option['default'] = $v['default'];
				}
			} else {
				$arr_option['default'] = '';
			}

			if ($v['display'] == 'select' && is_array($v['options']) && !empty($v['options'])) {
				$arr_option['values'] = $this->convertSelectField($v['options']);
			}

			if (isset($v['isJsObject']) && $v['isJsObject'] == true) {
				$arr_option['isJsObject'] = true;
			}

			if (isset($v['isJsArray']) && $v['isJsArray'] == true) {
				$arr_option['isJsArray'] = true;
			}

			if (isset($v['important']) && $v['important'] == true) {
				$arr_option['important'] = true;
			}

			if (isset($v['useful']) && $v['useful'] == true) {
				$arr_option['useful'] = true;
			}

			$this->_fields_list[$varname] = $arr_option;
		}

		return $this->_fields_list;
	}
}
