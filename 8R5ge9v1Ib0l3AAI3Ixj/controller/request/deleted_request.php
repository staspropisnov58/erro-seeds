<?php
class ControllerRequestDeletedRequest extends Controller {

  public function index() {
    $this->load->language('request/deleted_request');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->getForm();
  }

  public function getForm(){

    $this->load->model('request/request');

    $url = '';

    $data['heading_title'] = $this->language->get('heading_title');

    if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

    if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}

    if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

    if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

    if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

    if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

    if (isset($this->request->get['filter_date_expired'])) {
			$filter_date_expired = $this->request->get['filter_date_expired'];
		} else {
			$filter_date_expired = null;
		}

    if (isset($this->request->get['filter_date_deleted'])) {
			$filter_date_deleted = $this->request->get['filter_date_deleted'];
		} else {
			$filter_date_deleted = null;
		}

    if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . urlencode(html_entity_decode($this->request->get['filter_quantity'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . urlencode(html_entity_decode($this->request->get['filter_date_added'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_date_expired'])) {
			$url .= '&filter_date_expired=' . urlencode(html_entity_decode($this->request->get['filter_date_expired'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_date_deleted'])) {
			$url .= '&filter_date_deleted=' . urlencode(html_entity_decode($this->request->get['filter_date_deleted'], ENT_QUOTES, 'UTF-8'));
		}

    $filter_data = array(
      'order' => $order,
      'sort'  => $sort,
      'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
      'filter_name' => $filter_name,
      'filter_email' => $filter_email,
      'filter_quantity' => $filter_quantity,
      'filter_date_added' => $filter_date_added,
      'filter_date_expired' => $filter_date_expired,
      'filter_date_deleted' => $filter_date_deleted
    );

    $this->load->model('sale/customer');
    $this->load->model('catalog/product');

    $results = $this->model_request_request->getDeletedRequest($filter_data);

    foreach ($results as &$result){

      if($result['reason']){
        $result['reason'] = $this->language->get('text_' . $result['reason']);
      }
      if($result['customer_id'] != 0){
        $customer = $this->model_sale_customer->getCustomer($result['customer_id']);
        if(empty($customer)){
          $customer['firstname'] = '';
          $customer['lastname'] = '';
        }
        if($customer['firstname'] === '' && $customer['lastname'] === ''){
          $customer['firstname'] = $this->language->get('text_no_name');
        }
        $customer_data = array(
          'customer_name' => $customer['firstname']. ' ' . $customer['lastname'],
          'href'        => $this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')
         );
      }else{
        $customer_data = array();
      }
      $result['customer_data'] = $customer_data;
    }

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_form'] = $this->language->get('text_form');
    $data['text_guest'] = $this->language->get('text_guest');
    $data['text_invalid_email'] = $this->language->get('text_invalid_email');
    $data['text_product_out_of_sale'] = $this->language->get('text_product_out_of_sale');
    $data['text_outdated_request'] = $this->language->get('text_outdated_request');

    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_date_added'] = $this->language->get('entry_date_added');
    $data['entry_date_deleted'] = $this->language->get('entry_date_deleted');
    $data['entry_date_expired'] = $this->language->get('enrty_date_expired');
    $data['entry_quantity'] = $this->language->get('entry_quantity');

    $data['column_email'] = $this->language->get('column_email');
    $data['column_date_complete'] = $this->language->get('column_date_complete');
    $data['column_product_name'] = $this->language->get('column_product_name');
    $data['column_customer'] = $this->language->get('column_customer');
    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_date_expired'] = $this->language->get('column_date_expired');
    $data['column_date_deleted'] = $this->language->get('column_date_deleted');
    $data['column_quantity'] = $this->language->get('column_quantity');
    $data['column_reason'] = $this->language->get('column_reason');

    $data['button_filter'] = $this->language->get('button_filter');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');

    $data['token'] = $this->request->get['token'];

    $data['tab_deleted'] = $this->language->get('tab_deleted');

    $data['requests_data'] = $results;
    $deleted_total = $this->model_request_request->getTotalDeletedRequest($filter_data);

    $data['filter_name'] = $filter_name;
    $data['filter_email'] = $filter_email;
    $data['filter_quantity'] = $filter_quantity;
    $data['filter_date_added'] = $filter_date_added;
    $data['filter_date_expired'] = $filter_date_expired;
    $data['filter_date_deleted'] = $filter_date_deleted;

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    if ($order == 'DESC') {
      $url .= '&order=ASC';
    } else {
      $url .= '&order=DESC';
    }

    $data['sort_email'] = $this->url->link('request/deleted_request', 'token=' . $this->session->data['token'] . '&sort=nrd.email' . $url, 'SSL');
    $data['sort_customer'] = $this->url->link('request/deleted_request', 'token=' . $this->session->data['token'] . '&sort=nrd.customer_id' . $url, 'SSL');
    $data['sort_date_added'] = $this->url->link('request/deleted_request', 'token=' . $this->session->data['token'] . '&sort=nrd.date_added' . $url, 'SSL');
    $data['sort_date_expired'] = $this->url->link('request/deleted_request', 'token=' . $this->session->data['token'] . '&sort=nrd.date_expired' . $url, 'SSL');
    $data['sort_date_deleted'] = $this->url->link('request/deleted_request', 'token=' . $this->session->data['token'] . '&sort=nrd.date_deleted' . $url, 'SSL');
    $data['sort_name'] = $this->url->link('request/deleted_request', 'token=' . $this->session->data['token'] . '&sort=product_name' . $url, 'SSL');
    $data['sort_quantity'] = $this->url->link('request/deleted_request', 'token=' . $this->session->data['token'] . '&sort=nrd.quantity' . $url, 'SSL');

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['cancel'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['action'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . $url, 'SSL');

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

    $pagination = new Pagination();
		$pagination->total = $deleted_total;
		$pagination->page = $page;
		$pagination->limit = 20;
		$pagination->url = $this->url->link('request/deleted_request/', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($deleted_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($deleted_total - $this->config->get('config_limit_admin'))) ? $deleted_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $deleted_total, ceil($deleted_total / $this->config->get('config_limit_admin')));

    $this->response->setOutput($this->load->view('request/deleted_request.tpl', $data));

  }

}
