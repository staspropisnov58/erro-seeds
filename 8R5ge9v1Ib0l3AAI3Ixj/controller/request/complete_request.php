<?php
class ControllerRequestCompleteRequest extends Controller {

  public function index() {
    $this->load->language('request/complete_request');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->getForm();
  }

  public function getForm(){

    $this->load->model('request/request');

    $url = '';

    $data['heading_title'] = $this->language->get('heading_title');

    $data['token'] = $this->request->get['token'];


    if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

    if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}

    if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

    if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

    if (isset($this->request->get['filter_date_complete'])) {
			$filter_date_complete = $this->request->get['filter_date_complete'];
		} else {
			$filter_date_complete = null;
		}

    if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_date_complete'])) {
			$url .= '&filter_date_complete=' . urlencode(html_entity_decode($this->request->get['filter_date_complete'], ENT_QUOTES, 'UTF-8'));
		}

    $filter_data = array(
      'order' => $order,
      'sort'  => $sort,
      'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
      'filter_name' => $filter_name,
      'filter_email' => $filter_email,
      'filter_date_complete' => $filter_date_complete
    );


    $this->load->model('sale/customer');
    $this->load->model('catalog/product');

    $results =  $this->model_request_request->getCompleteRequest($filter_data);

    foreach ($results as &$result){
      if($result['customer_id'] != 0){

        $customer = $this->model_sale_customer->getCustomer($result['customer_id']);

          if(empty($customer)){
            $customer['firstname'] = '';
            $customer['lastname'] = '';
          }
          if($customer['firstname'] === '' && $customer['lastname'] === ''){
            $customer['firstname'] = $this->language->get('text_no_name');
          }
          $customer_data = array(
            'customer_name' => $customer['firstname']. ' ' . $customer['lastname'],
            'href'        => $this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')
           );
      }else{
        $customer_data = array();
      }
      $result['customer_data'] = $customer_data;
    }


    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_form'] = $this->language->get('text_form');
    $data['text_guest'] = $this->language->get('text_guest');

    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_date_complete'] = $this->language->get('entry_date_complete');

    $data['column_email'] = $this->language->get('column_email');
    $data['column_date_complete'] = $this->language->get('column_date_complete');
    $data['column_product_name'] = $this->language->get('column_product_name');
    $data['column_customer'] = $this->language->get('column_customer');

    $data['button_save'] = $this->language->get('button_save');
    $data['button_filter'] = $this->language->get('button_filter');
    $data['button_cancel'] = $this->language->get('button_cancel');

    $data['tab_complete'] = $this->language->get('tab_complete');

    $data['requests_data'] = $results;

    $complete_total = $this->model_request_request->getTotalCompleteRequest($filter_data);

    $data['filter_name'] = $filter_name;
    $data['filter_email'] = $filter_email;
    $data['filter_date_complete'] = $filter_date_complete;

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    if ($order == 'DESC') {
      $url .= '&order=ASC';
    } else {
      $url .= '&order=DESC';
    }

    $data['sort_email'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . '&sort=nrc.email' . $url, 'SSL');
    $data['sort_date_complete'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . '&sort=nrc.date_complete' . $url, 'SSL');
    $data['sort_name'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . '&sort=product_name' . $url, 'SSL');
    $data['sort_customer'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . '&sort=nrc.customer_id' . $url, 'SSL');

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['cancel'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['action'] = $this->url->link('request/complete_request', 'token=' . $this->session->data['token'] . $url, 'SSL');

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

    $pagination = new Pagination();
		$pagination->total = $complete_total;
		$pagination->page = $page;
		$pagination->limit = 20;
		$pagination->url = $this->url->link('request/complete_request/', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($complete_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($complete_total - $this->config->get('config_limit_admin'))) ? $complete_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $complete_total, ceil($complete_total / $this->config->get('config_limit_admin')));

    $this->response->setOutput($this->load->view('request/complete_request.tpl', $data));

  }

}
