<?php
class ControllerRequestNewRequest extends Controller {


  public function index() {
    $this->load->language('request/new_request');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->controller('sale/order/apiLogin');

    $this->getForm();
  }

  public function api() {

      $json = array();

      // Store
      if (isset($this->request->get['store_id'])) {
          $store_id = $this->request->get['store_id'];
      } else {
          $store_id = 0;
      }

      $this->load->model('setting/store');

      $store_info = $this->model_setting_store->getStore($store_id);


      if ($store_info) {
          $url = $store_info['ssl'];
      } else {
          $url = HTTPS_CATALOG;
      }



      if (isset($this->session->data['cookie'])) {
          // Include any URL perameters
          $url_data = array();

          foreach ($this->request->get as $key => $value) {
              if ($key != 'route' && $key != 'token' && $key != 'store_id') {
                  $url_data[$key] = $value;
              }
          }

          if($this->session->data['requests_id'] != NULL && isset($this->request->get['email'])){

            $this->request->post['requests_id'] = $this->session->data['requests_id'];
            $this->request->post['email'] = $this->request->get['email'];

          }

          $curl = curl_init();

          // Set SSL if required
          if (substr($url, 0, 5) == 'https') {
              curl_setopt($curl, CURLOPT_PORT, 443);
          }

          curl_setopt($curl, CURLOPT_HEADER, false);
          curl_setopt($curl, CURLINFO_HEADER_OUT, true);
          curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
          curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_URL, $url . 'index.php?route=' . 'api/mail/backToStock' . ($url_data ? '&' . http_build_query($url_data) : ''));

          if ($this->request->post) {

              curl_setopt($curl, CURLOPT_POST, true);
              curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->request->post));

          }

          curl_setopt($curl, CURLOPT_COOKIE, session_name() . '=' . $this->session->data['cookie'] . ';');

          $json = curl_exec($curl);

          if(array_key_exists('success', json_decode($json))){

            $this->load->model('request/request');

            $this->model_request_request->editCompmleteRequest($this->request->post['requests_id'][$this->request->post['email']]);

            unset($this->session->data['requests_id'][$this->request->post['email']]);
          }

          curl_close($curl);
      }

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput($json);
  }

  public function getForm(){

    // $this->session->data['requests_id'] = null;

    $this->load->model('request/request');

    $url = '';

    $data['heading_title'] = $this->language->get('heading_title');

    if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

    if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}

    if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    $filter_data = array(
      'order' => $order,
      'sort'  => $sort,
      'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
    );


    $this->load->model('sale/customer');
    $this->load->model('catalog/product');

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_form'] = $this->language->get('text_form');
    $data['text_guest'] = $this->language->get('text_guest');

    $data['column_email'] = $this->language->get('column_email');
    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_date_expired'] = $this->language->get('column_date_expired');
    $data['column_product_name'] = $this->language->get('column_product_name');
    $data['column_quantity'] = $this->language->get('column_quantity');
    $data['column_customer'] = $this->language->get('column_customer');
    $data['column_reason'] = $this->language->get('column_reason');

    $data['tab_active'] = $this->language->get('tab_active');
    $data['tab_newr']  = $this->language->get('tab_newr');
    $data['tab_overdue'] = $this->language->get('tab_overdue');
    $data['tab_notifications'] = $this->language->get('tab_notifications');

    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
    $data['button_make_notification'] = $this->language->get('button_make_notification');

    $data['token'] = $this->session->data['token'];

    $data['active'] = $this->active();
    $data['newr'] = $this->newr();
    $data['pre_order'] = $this->pre_order();
    $data['overdue'] = $this->overdue();
    $data['notifications'] = $this->notifications();

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $data['cancel'] = $this->url->link('request/new_request', 'token=' . $this->session->data['token'] . $url, 'SSL');
    $data['action'] = $this->url->link('request/new_request', 'token=' . $this->session->data['token'] . $url, 'SSL');

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('request/new_request.tpl', $data));

  }

  public function notifications(){

    $this->load->language('request/new_request');
    $this->load->model('request/request');

    if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    $data['token'] = $this->session->data['token'];
    $data['entry_reason'] = $this->language->get('entry_reason');
    $data['text_invalid_email'] = $this->language->get('text_invalid_email');
    $data['text_product_out_of_sale'] = $this->language->get('text_product_out_of_sale');
    $data['text_outdated_request'] = $this->language->get('text_outdated_request');
    $data['column_email'] = $this->language->get('column_email');
    $data['column_result'] = $this->language->get('column_result');
    $data['column_product_name'] = $this->language->get('column_product_name');
    $data['button_make_notification'] = $this->language->get('button_make_notification');
    $data['button_delete'] = $this->language->get('button_delete');

    if(isset($this->session->data['requests_id']) && $this->session->data['requests_id'] !== null){
      foreach ($this->session->data['requests_id'] as $key1 => $value1) {
        foreach($value1 as $key => $value){
          $notifications_data[$key1][$key] = $this->model_request_request->getProductsByRequestId($value);
        }
      }


    $notifications_data = array_chunk($notifications_data, $this->config->get('config_limit_admin'), TRUE);
    $notifications_data = array_slice($notifications_data, $page - 1, 1);
    $data['notifications_data'] = $notifications_data;
    $notifications_total =  count($this->session->data['requests_id']);
  }else{
    $notifications_total = 0;
    $data['notifications_data'] = array();
  }

    $pagination = new Pagination();
		$pagination->total = $notifications_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&page={page}' . '&tab=notifications'  , 'SSL');

		$data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($notifications_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($notifications_total - $this->config->get('config_limit_admin'))) ? $notifications_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $notifications_total, ceil($notifications_total / $this->config->get('config_limit_admin')));

    return $this->load->view('request/new/notifications.tpl', $data);

  }

  public function active(){

    $this->load->language('request/new_request');

    $this->load->model('request/request');
    $this->load->model('sale/customer');

    $url = '';

    $data['text_invalid_email'] = $this->language->get('text_invalid_email');
    $data['text_product_out_of_sale'] = $this->language->get('text_product_out_of_sale');
    $data['text_outdated_request'] = $this->language->get('text_outdated_request');
    $data['text_guest'] = $this->language->get('text_guest');
    $data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
    $data['text_insufficient_amount'] = $this->language->get('text_insufficient_amount');
    $data['text_product_disabled'] = $this->language->get('text_product_disabled');

    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_date_added'] = $this->language->get('entry_date_added');
    $data['entry_date_expired'] = $this->language->get('enrty_date_expired');
    $data['entry_quantity'] = $this->language->get('entry_quantity');
    $data['entry_reason'] = $this->language->get('entry_reason');

    $data['column_email'] = $this->language->get('column_email');
    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_date_expired'] = $this->language->get('column_date_expired');
    $data['column_product_name'] = $this->language->get('column_product_name');
    $data['column_quantity'] = $this->language->get('column_quantity');
    $data['column_customer'] = $this->language->get('column_customer');
    $data['column_reason'] = $this->language->get('column_reason');

    $data['button_filter'] = $this->language->get('button_filter');
    $data['button_make_notification'] = $this->language->get('button_make_notification');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
    $data['button_delete'] = $this->language->get('button_delete');

    if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

    if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}

    if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    $filters = $this->Filters();

    $filter_data = array(
      'order' => $order,
      'sort'  => $sort,
      'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
    );

    $filter_data = $result = array_merge ($filters['filter_data'], $filter_data);

    $data['filter_name'] = $filters['filter_data']['filter_name'];
    $data['filter_email'] = $filters['filter_data']['filter_email'];
    $data['filter_quantity'] = $filters['filter_data']['filter_quantity'];
    $data['filter_date_added'] = $filters['filter_data']['filter_date_added'];
    $data['filter_date_expired'] = $filters['filter_data']['filter_date_expired'];



    $active_total = $this->model_request_request->getTotalActiveRequest($filter_data);

    $results = $this->model_request_request->getActiveRequest($filter_data);

    foreach ($results as $key => &$result){

      if($result['customer_id'] !== 0){
        $customer = $this->model_sale_customer->getCustomer($result['customer_id']);
        if(empty($customer)){
          $customer['firstname'] = '';
          $customer['lastname'] = '';
        }

          if(isset($customer['firstname']) && $customer['firstname'] === '' || isset($customer['lastname']) && $customer['lastname'] === ''){
            $customer['firstname'] = $this->language->get('text_no_name');
          }
          $customer_data = array(
            'customer_name' => $customer['firstname']. ' ' . $customer['lastname'],
            'href'        => $this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')
           );
      }else{
        $customer_data = array();
      }
      $result['customer_data'] = $customer_data;


    }

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if(isset($filters['url']) && !empty($filters['url'])){
      $url .= $filters['url'];
    }

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['token'] = $this->request->get['token'];

    $data['sort_email'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.email' . '&tab=active' . $url, 'SSL');
    $data['sort_customer'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.customer_id' . '&tab=active' . $url, 'SSL');
    $data['sort_date_added'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_added' . '&tab=active' . $url, 'SSL');
    $data['sort_date_expired'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_expired' . '&tab=active' . $url, 'SSL');
    $data['sort_name'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=product_name' . '&tab=active' . $url, 'SSL');
    $data['sort_quantity'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.quantity' . '&tab=active' . $url, 'SSL');

    $data['requests_data'] =  $results;

    $pagination = new Pagination();
		$pagination->total = $active_total;
		$pagination->page = $page;
		$pagination->limit = 20;
		$pagination->url = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&page={page}' . '&tab=active' . $url, 'SSL');

		$data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($active_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($active_total - $this->config->get('config_limit_admin'))) ? $active_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $active_total, ceil($active_total / $this->config->get('config_limit_admin')));

    return $this->load->view('request/new/active.tpl', $data);


  }

  public function newr(){

    $this->load->language('request/new_request');

    $this->load->model('request/request');
    $this->load->model('sale/customer');

    $url = '';


    $data['text_guest'] = $this->language->get('text_guest');

    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_date_added'] = $this->language->get('entry_date_added');
    $data['entry_date_expired'] = $this->language->get('enrty_date_expired');
    $data['entry_quantity'] = $this->language->get('entry_quantity');
    $data['entry_reason'] = $this->language->get('entry_reason');

    $data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
    $data['text_insufficient_amount'] = $this->language->get('text_insufficient_amount');
    $data['text_product_disabled'] = $this->language->get('text_product_disabled');
    $data['text_invalid_email'] = $this->language->get('text_invalid_email');
    $data['text_product_out_of_sale'] = $this->language->get('text_product_out_of_sale');
    $data['text_outdated_request'] = $this->language->get('text_outdated_request');

    $data['column_email'] = $this->language->get('column_email');
    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_date_expired'] = $this->language->get('column_date_expired');
    $data['column_product_name'] = $this->language->get('column_product_name');
    $data['column_quantity'] = $this->language->get('column_quantity');
    $data['column_customer'] = $this->language->get('column_customer');
    $data['column_reason'] = $this->language->get('column_reason');

    $data['button_filter'] = $this->language->get('button_filter');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
    $data['button_delete'] = $this->language->get('button_delete');

    if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

    if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}



    if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    $filters = $this->Filters();

    $filter_data = array(
      'order' => $order,
      'sort'  => $sort,
      'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
    );

    $filter_data = $result = array_merge($filters['filter_data'], $filter_data);

    $data['filter_name'] = $filters['filter_data']['filter_name'];
    $data['filter_email'] = $filters['filter_data']['filter_email'];
    $data['filter_quantity'] = $filters['filter_data']['filter_quantity'];
    $data['filter_date_added'] = $filters['filter_data']['filter_date_added'];
    $data['filter_date_expired'] = $filters['filter_data']['filter_date_expired'];
    $data['filter_reason'] = $filters['filter_data']['filter_reason'];

    $active_total = $this->model_request_request->getTotalNewRequest($filter_data);

    $results = $this->model_request_request->getNewRequest($filter_data);

    foreach ($results as &$result){
      if($result['customer_id'] != 0){
        $customer = $this->model_sale_customer->getCustomer($result['customer_id']);
        if(empty($customer)){
          $customer['firstname'] = '';
          $customer['lastname'] = '';
        }
          if($customer['firstname'] === '' && $customer['lastname'] === ''){
            $customer['firstname'] = $this->language->get('text_no_name');
          }
          $customer_data = array(
            'customer_name' => $customer['firstname']. ' ' . $customer['lastname'],
            'href'        => $this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')
           );
      }else{
        $customer_data = array();
      }
      $result['customer_data'] = $customer_data;

      if ($result['product_quantity'] < $result['notification_quantity']) {
        $result['reason'] = $this->language->get('text_insufficient_amount');
      } else {
        $result['reason'] = $this->language->get('text_product_disabled');
      }
    }

    $data['token'] = $this->request->get['token'];

    $data['requests_data'] =  $results;

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }
    if(isset($filters['url']) && !empty($filters['url'])){
      $url .= $filters['url'];
    }

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['sort_email'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.email' . '&tab=newr' . $url, 'SSL');
    $data['sort_customer'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.customer_id' . '&tab=newr' . $url, 'SSL');
    $data['sort_date_added'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_added' . '&tab=newr' . $url, 'SSL');
    $data['sort_date_expired'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_expired' . '&tab=newr' . $url, 'SSL');
    $data['sort_name'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=product_name' . '&tab=newr' . $url, 'SSL');
    $data['sort_quantity'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.quantity' . '&tab=newr' . $url, 'SSL');


    $pagination = new Pagination();
		$pagination->total = $active_total;
		$pagination->page = $page;
		$pagination->limit = 20;
		$pagination->url = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&page={page}' . '&tab=newr', 'SSL');

		$data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($active_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($active_total - $this->config->get('config_limit_admin'))) ? $active_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $active_total, ceil($active_total / $this->config->get('config_limit_admin')));

    return $this->load->view('request/new/new.tpl', $data);
  }
    public function pre_order(){

        $this->load->language('request/new_request');

        $this->load->model('request/request');
        $this->load->model('sale/customer');

        $url = '';


        $data['text_guest'] = $this->language->get('text_guest');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_date_added'] = $this->language->get('entry_date_added');
        $data['entry_date_expired'] = $this->language->get('enrty_date_expired');
        $data['entry_quantity'] = $this->language->get('entry_quantity');
        $data['entry_reason'] = $this->language->get('entry_reason');

        $data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
        $data['text_insufficient_amount'] = $this->language->get('text_insufficient_amount');
        $data['text_product_disabled'] = $this->language->get('text_product_disabled');
        $data['text_invalid_email'] = $this->language->get('text_invalid_email');
        $data['text_product_out_of_sale'] = $this->language->get('text_product_out_of_sale');
        $data['text_outdated_request'] = $this->language->get('text_outdated_request');

        $data['column_email'] = $this->language->get('column_email');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_expired'] = $this->language->get('column_date_expired');
        $data['column_product_name'] = $this->language->get('column_product_name');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_reason'] = $this->language->get('column_reason');

        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'date_added';
        }



        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $filters = $this->Filters();

        $filter_data = array(
            'order' => $order,
            'sort'  => $sort,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin'),
        );

        $filter_data = $result = array_merge($filters['filter_data'], $filter_data);

        $data['filter_name'] = $filters['filter_data']['filter_name'];
        $data['filter_email'] = $filters['filter_data']['filter_email'];
        $data['filter_telephone'] = $filters['filter_data']['filter_telephone'];
        $data['filter_quantity'] = $filters['filter_data']['filter_quantity'];
        $data['filter_date_added'] = $filters['filter_data']['filter_date_added'];
        $data['filter_date_expired'] = $filters['filter_data']['filter_date_expired'];
        $data['filter_reason'] = $filters['filter_data']['filter_reason'];

        $active_total = $this->model_request_request->getTotalOrderRequest($filter_data);

        $results = $this->model_request_request->getOrderRequest($filter_data);

        foreach ($results as &$result){
            if($result['customer_id'] != 0){
                $customer = $this->model_sale_customer->getCustomer($result['customer_id']);
                if(empty($customer)){
                    $customer['firstname'] = '';
                    $customer['lastname'] = '';
                }
                if($customer['firstname'] === '' && $customer['lastname'] === ''){
                    $customer['firstname'] = $this->language->get('text_no_name');
                }
                $customer_data = array(
                    'customer_name' => $customer['firstname']. ' ' . $customer['lastname'],
                    'href'        => $this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')
                );
            }else{
                $customer_data = array();
            }
            $result['customer_data'] = $customer_data;

            if ($result['product_quantity'] < $result['notification_quantity']) {
                $result['reason'] = $this->language->get('text_insufficient_amount');
            } else {
                $result['reason'] = $this->language->get('text_product_disabled');
            }
        }

        $data['token'] = $this->request->get['token'];

        $data['requests_data'] =  $results;

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        if(isset($filters['url']) && !empty($filters['url'])){
            $url .= $filters['url'];
        }

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['sort_email'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.email' . '&tab=pre_order' . $url, 'SSL');
        $data['sort_telephone'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.telephone' . '&tab=pre_order' . $url, 'SSL');
        $data['sort_customer'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.customer_id' . '&tab=pre_order' . $url, 'SSL');
        $data['sort_date_added'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_added' . '&tab=pre_order' . $url, 'SSL');
        $data['sort_date_expired'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_expired' . '&tab=pre_order' . $url, 'SSL');
        $data['sort_name'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=product_name' . '&tab=pre_order' . $url, 'SSL');
        $data['sort_quantity'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.quantity' . '&tab=pre_order' . $url, 'SSL');


        $pagination = new Pagination();
        $pagination->total = $active_total;
        $pagination->page = $page;
        $pagination->limit = 20;
        $pagination->url = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&page={page}' . '&tab=pre_order', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($active_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($active_total - $this->config->get('config_limit_admin'))) ? $active_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $active_total, ceil($active_total / $this->config->get('config_limit_admin')));

        return $this->load->view('request/new/order.tpl', $data);
    }
  public function overdue(){

    $this->load->language('request/new_request');

    $this->load->model('request/request');
    $this->load->model('sale/customer');

    $url = '';

    $data['text_guest'] = $this->language->get('text_guest');

    $data['text_invalid_email'] = $this->language->get('text_invalid_email');
    $data['text_product_out_of_sale'] = $this->language->get('text_product_out_of_sale');
    $data['text_outdated_request'] = $this->language->get('text_outdated_request');

    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_date_added'] = $this->language->get('entry_date_added');
    $data['entry_date_expired'] = $this->language->get('enrty_date_expired');
    $data['entry_quantity'] = $this->language->get('entry_quantity');
    $data['entry_reason'] = $this->language->get('entry_reason');

    $data['column_email'] = $this->language->get('column_email');
    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_date_expired'] = $this->language->get('column_date_expired');
    $data['column_product_name'] = $this->language->get('column_product_name');
    $data['column_quantity'] = $this->language->get('column_quantity');
    $data['column_customer'] = $this->language->get('column_customer');
    $data['column_reason'] = $this->language->get('column_reason');

    $data['button_filter'] = $this->language->get('button_filter');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
    $data['button_delete'] = $this->language->get('button_delete');

    if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

    if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}

    if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

    $data['token'] = $this->request->get['token'];

    $filters = $this->Filters();

    $filter_data = array(
      'order' => $order,
      'sort'  => $sort,
      'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin'),
    );

    $filter_data = $result = array_merge($filters['filter_data'], $filter_data);

    $data['filter_name'] = $filters['filter_data']['filter_name'];
    $data['filter_email'] = $filters['filter_data']['filter_email'];
    $data['filter_quantity'] = $filters['filter_data']['filter_quantity'];
    $data['filter_date_added'] = $filters['filter_data']['filter_date_added'];
    $data['filter_date_expired'] = $filters['filter_data']['filter_date_expired'];

    $active_total = $this->model_request_request->getTotalOverdueRequest($filter_data);

    $results = $this->model_request_request->getOverdueRequest($filter_data);

    foreach ($results as &$result){
      if($result['customer_id'] != 0){
        $customer = $this->model_sale_customer->getCustomer($result['customer_id']);
        if(empty($customer)){
          $customer['firstname'] = '';
          $customer['lastname'] = '';
        }
          if($customer['firstname'] === '' && $customer['lastname'] === ''){
            $customer['firstname'] = $this->language->get('text_no_name');
          }
          $customer_data = array(
            'customer_name' => $customer['firstname']. ' ' . $customer['lastname'],
            'href'        => $this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'], 'SSL')
           );
      }else{
        $customer_data = array();
      }
      $result['customer_data'] = $customer_data;
    }

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if(isset($filters['url']) && !empty($filters['url'])){
      $url .= $filters['url'];
    }

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['sort_email'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.email' . '&tab=overdue' . $url, 'SSL');
    $data['sort_customer'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.customer_id' . '&tab=overdue' . $url, 'SSL');
    $data['sort_date_added'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_added' . '&tab=overdue' . $url, 'SSL');
    $data['sort_date_expired'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.date_expired' . '&tab=overdue' . $url, 'SSL');
    $data['sort_name'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=product_name' . '&tab=overdue' . $url, 'SSL');
    $data['sort_quantity'] = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&sort=nr.quantity' . '&tab=overdue' . $url, 'SSL');

    $data['requests_data'] =  $results;

    $pagination = new Pagination();
		$pagination->total = $active_total;
		$pagination->page = $page;
		$pagination->limit = 20;
		$pagination->url = $this->url->link('request/new_request/paginate', 'token=' . $this->session->data['token'] . '&page={page}' . '&tab=overdue', 'SSL');

		$data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($active_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($active_total - $this->config->get('config_limit_admin'))) ? $active_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $active_total, ceil($active_total / $this->config->get('config_limit_admin')));

    return $this->load->view('request/new/overdue.tpl', $data);
  }

  public function Filters(){

    $url = '';

    if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

    if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

    if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

    if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

    if (isset($this->request->get['filter_date_expired'])) {
			$filter_date_expired = $this->request->get['filter_date_expired'];
		} else {
			$filter_date_expired = null;
		}

    if (isset($this->request->get['filter_reason'])) {
			$filter_reason = $this->request->get['filter_reason'];
		} else {
			$filter_reason = null;
		}

    if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_quantity'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . urlencode(html_entity_decode($this->request->get['filter_quantity'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . urlencode(html_entity_decode($this->request->get['filter_date_added'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_date_expired'])) {
			$url .= '&filter_date_expired=' . urlencode(html_entity_decode($this->request->get['filter_date_expired'], ENT_QUOTES, 'UTF-8'));
		}

    if (isset($this->request->get['filter_reason'])) {
			$url .= '&filter_reason=' . urlencode(html_entity_decode($this->request->get['filter_reason'], ENT_QUOTES, 'UTF-8'));
		}

    $array['filter_data'] = array(
      'filter_name'	  => $filter_name,
      'filter_email'	        => $filter_email,
      'filter_quantity'       => $filter_quantity,
      'filter_date_added'     => $filter_date_added,
      'filter_date_expired'   => $filter_date_expired,
      'filter_reason'         => $filter_reason
    );

    $array['url'] = $url;

    return $array;

  }

  public function makeNotification(){
    $tabs = array();

    if(isset($this->request->post['requests_id'])){
      $tabs[] = 'active';
      $tabs[] = 'notifications';
    }

    $this->load->model('request/request');

    $requests_id= array();

    $array = explode(",", $this->request->post['requests_id']);

    foreach($array AS $request_id){
      $requests_id[$this->model_request_request->getNewRequestEmail($request_id)][] = $request_id;
    }

    if(isset($this->session->data['requests_id']) && $this->session->data['requests_id']!== null){
      $this->session->data['requests_id'] = array_merge($this->session->data['requests_id'], $requests_id);
    }else{
      $this->session->data['requests_id'] = $requests_id;
    }

    $json = $this->fillTabs($tabs);

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));  }

  public function fillTabs($tabs = false){

      foreach($tabs as $tab) {
        $tabs_key = $tab;
        $tab = explode("-",$tab);
        foreach($tab as $key => $value){
          if($key===0){
              unset($functions);
            $functions[]=$value;
          }else {
            $functions[]=ucfirst($value);
          }
        }
        $functions = implode($functions);
        $data[$tabs_key] = $this->$functions();
      }

      return $data;
    }

    public function deleteRelatedRequests(){


      if(isset($this->request->post['deleteIt']) && isset($this->request->post['reason'])){
        $tabs[] = 'notifications';
      }
      $this->load->model('request/request');

      if($this->request->post){
        foreach($this->request->post['deleteIt'] as $email){
          $this->model_request_request->deleteRelatedRequests($this->session->data['requests_id'][$email], $this->request->post['reason']);
          unset($this->session->data['requests_id'][$email]);
        }
      }

      $json = $this->fillTabs($tabs);

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));

    }

    public function deleteRequests(){


      if(isset($this->request->post['deleteIt']) && isset($this->request->post['reason'])){
        $tabs[] = $this->request->get['tab'];
      }

      $this->load->model('request/request');

      if(isset($this->request->post['deleteIt']) && isset($this->request->post['reason'])){

        $this->model_request_request->deleteRequests($this->request->post);
      }

      $json = $this->fillTabs($tabs);

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json, JSON_UNESCAPED_UNICODE));

    }



  public function paginate(){
		if(isset($this->request->get['tab'])){
  		$link = $this->request->get['tab'];
  		$link = explode("-",$link);
  		foreach($link as $key => $value){
  			if($key===0){
  					unset($functions);
  				$functions[]=$value;
  			}else {
  				$functions[]=ucfirst($value);
  			}
  		}
  		$functions = implode($functions);

  	  $json = $this->$functions();

  		$this->response->setOutput($json, JSON_UNESCAPED_UNICODE);
    }
	}
}
