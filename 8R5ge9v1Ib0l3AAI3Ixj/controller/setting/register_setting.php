<?php
class ControllerSettingRegisterSetting extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('setting/register_setting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/register_setting');
    $this->load->model('localisation/language');

	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		if(isset($this->request->post['enebled_tel_code'])){
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('enebled_tel_code', ['enebled_tel_code' => $this->request->post['enebled_tel_code']]);
		}

			$this->model_setting_register_setting->editRegisterSettings($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL'));
		}

    $data['languages'] = $this->model_localisation_language->getLanguages();
    $results = $this->model_setting_register_setting->getRegisterSettings();

		if(!empty($this->request->post)){
			$data['register_settings'] = $this->request->post;
			unset($data['register_settings']['type']);

		}elseif(!empty($results)){
			$data['register_settings'] = $results;
		}

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['entry_type'] = $this->language->get('entry_type');
    $data['text_disabled'] = $this->language->get('text_disabled');
    $data['entry_label'] = $this->language->get('entry_label');
    $data['entry_placeholder'] = $this->language->get('entry_placeholder');
    $data['entry_sorting'] = $this->language->get('entry_sorting');
    $data['entry_status'] = $this->language->get('entry_status');
    $data['entry_editable'] = $this->language->get('entry_editable');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
		$data['entry_tel_code'] = $this->language->get('entry_tel_code');
		$data['text_tel']	= $this->language->get('text_tel');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_radio'] = $this->language->get('text_radio');
		$data['text_checkbox'] = $this->language->get('text_checkbox');
		$data['text_input'] = $this->language->get('text_input');
		$data['text_text'] = $this->language->get('text_text');
		$data['text_textarea'] = $this->language->get('text_textarea');
		$data['text_file'] = $this->language->get('text_file');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_datetime'] = $this->language->get('text_datetime');
		$data['text_time'] = $this->language->get('text_time');
		$data['text_choose'] = $this->language->get('text_choose');




    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if(isset($this->error['error_label'])){
			$data['error_label'] = $this->error['error_label'];
		}else{
			$data['error_label'] = '';
		}

		if(isset($this->error['error_placeholder'])){
			$data['error_placeholder'] = $this->error['error_placeholder'];
		}else{
			$data['error_placeholder'] = '';
		}

		if(isset($this->error['error_sort_order'])){
			$data['error_sort_order'] = $this->error['error_sort_order'];
		}else{
			$data['error_sort_order'] = '';
		}


		if(isset($this->request->post['enebled_tel_code'])){
			$data['tel_code'] = $this->request->post['enebled_tel_code'];
		}elseif($this->config->get('enebled_tel_code') !== null){
			$data['tel_code'] = $this->config->get('enebled_tel_code');
		}else {
			$data['tel_code'] = '';
		}

		$data['tel_code'] = $this->config->get('enebled_tel_code');

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_stores'),
			'href' => $this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('setting/register_setting', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action'] = $this->url->link('setting/register_setting', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL');

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('setting/register_setting.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'setting/register_setting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post as $key => $posts){

			if($key == 'type'){
				next();
			}else{

			if($key == 'enebled_tel_code'){}else{
			foreach($posts['label'] as $key1 => $post){

				if(utf8_strlen($post) < 1 || utf8_strlen($post) > 100){
					$this->error['error_label'][$key]['label'][$key1] = $this->language->get('error_label');
				}
			}


				foreach($posts['placeholder'] as $key1 => $post){

					if(utf8_strlen($post) > 100){
						$this->error['error_placeholder'][$key]['placeholder'][$key1] = $this->language->get('error_placeholder');
					}
				}
				if(!is_numeric($posts['sort_order'])){
					$this->error['error_sort_order'][$key]['sort_order'] = $this->language->get('error_sort_order');
				}
			}
		}
		}

		return !$this->error;
	}
}
