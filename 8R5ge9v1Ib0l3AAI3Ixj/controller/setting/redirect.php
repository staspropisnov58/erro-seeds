<?php
class ControllerSettingRedirect extends Controller
{
  public function index()
  {
    $this->load->language('setting/redirect');
    $this->load->model('setting/redirect');
    $this->document->setTitle($this->language->get('heading_title'));
    $errors = array();

    if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
      $errors = $this->validate($this->request->post['redirects']);
      if (empty($errors)) {
        $this->model_setting_redirect->saveRedirects($this->request->post['redirects']);
        unset($this->request->post['redirects']);
        $data['success'] = $this->language->get('success');
      }
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('setting/redirect', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['heading_title'] = $this->language->get('heading_title');

    $data['entry_from'] = $this->language->get('entry_from');
    $data['entry_to'] = $this->language->get('entry_to');
    $data['entry_status_code'] = $this->language->get('entry_status_code');

    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
    $data['button_remove'] = $this->language->get('button_remove');
    $data['button_add'] = $this->language->get('button_add');

    $data['errors'] = $errors;

    $data['action'] = $this->url->link('setting/redirect', 'token=' . $this->session->data['token'], 'SSL');
    $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL');

    if (isset($this->request->post['redirects'])) {
      $data['redirects'] = $this->request->post['redirects'];
    } else {
      $data['redirects'] = $this->model_setting_redirect->getRedirects();
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('setting/redirect.tpl', $data));
  }

  public function validate($data)
  {
    $errors = array();
    $this->load->language('setting/redirect');
    $this->load->model('setting/redirect');
    foreach ($data as $key => $redirect) {
      if ($this->model_setting_redirect->checkRedirectExists(trim($redirect['redirect_from'], '/'))) {
        $errors[$key] = $this->language->get('error_redirect_exists');
        continue;
      }
      if (!$this->model_setting_redirect->checkUrlExists(trim($redirect['redirect_to'], '/'))) {
        $errors[$key] = $this->language->get('error_url_not exists');
        continue;
      }
    }

    return $errors;
  }
}
