<?php
class ControllerSettingCheckoutSetting extends Controller {
	private $error = array();

	public function index() {

    $this->load->model('setting/checkout_setting');
    $this->load->model('localisation/language');
		$this->load->model('setting/setting');
		$this->load->model('localisation/country');
		$this->load->model('localisation/zone');
		$this->load->model('extension/extension');


		$extensions = $this->model_extension_extension->getInstalled('shipping');

		foreach ($extensions as $key => $value) {
			if (!file_exists(DIR_APPLICATION . 'controller/shipping/' . $value . '.php')) {
				$this->model_extension_extension->uninstall('shipping', $value);

				unset($extensions[$key]);
			}
		}

		$data['extensions'] = array();

		$files = glob(DIR_APPLICATION . 'controller/shipping/*.php');


		if ($files) {
			foreach ($files as $file) {
				$extension = basename($file, '.php');

				$this->load->language('shipping/' . $extension);

					if(in_array($extension, $extensions)){
					$data['extensions'][] = array(
						'name'       => $this->language->get('heading_title'),
						'code' => $extension
					);
				}
			}
		}

		$payments = $this->model_extension_extension->getInstalled('payment');

		foreach ($payments as $key => $value) {
			if (!file_exists(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
				$this->model_extension_extension->uninstall('payment', $value);

				unset($payments[$key]);
			}
		}

		$data['payments'] = array();

		$files = glob(DIR_APPLICATION . 'controller/payment/*.php');

		if ($files) {
			foreach ($files as $file) {
				$payment = basename($file, '.php');

				$this->load->language('payment/' . $payment);

					if(in_array($payment, $payments)){
					$data['payments'][] = array(
						'name'       => $this->language->get('heading_title'),
						'code' => $payment
					);
				}
			}
		}

		$this->load->language('setting/checkout_setting');

	  if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate($data['extensions'], $data['payments'])) {

			$data['fields'] = $this->model_setting_checkout_setting->getAddressFields();

			foreach ($data['fields'] as $key => $field) {
				if (in_array($field['Field'], ['address_id', 'customer_id', 'custom_field', 'country', 'zone'])) {
					unset($data['fields'][$key]);
				}
			}

			$this->model_setting_setting->editSetting('checkout', $this->request->post);

			if (isset($this->request->post['checkout_no_callback'])) {
				$this->model_setting_checkout_setting->enableNoCallback($this->request->post['checkout_no_callback']);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL'));

		}

			$this->document->setTitle($this->language->get('heading_title'));

      $data['heading_title'] = $this->language->get('heading_title');


      $data['text_stores'] = $this->language->get('text_stores');
      $data['text_form']  = $this->language->get('text_form');
      $data['text_enabled'] = $this->language->get('text_enabled');
      $data['text_disabled'] = $this->language->get('text_disabled');
      $data['text_checkout_fast_order_status'] = $this->language->get('text_checkout_fast_order_status');
			$data['firstname'] = $this->language->get('firstname');
			$data['lastname'] = $this->language->get('lastname');
			$data['company'] = $this->language->get('company');
			$data['patronymic'] = $this->language->get('patronymic');
			$data['address_1'] = $this->language->get('address_1');
			$data['address_2'] = $this->language->get('address_2');
			$data['city'] = $this->language->get('city');
			$data['postcode'] = $this->language->get('postcode');
			$data['country_id'] = $this->language->get('country_id');
			$data['zone_id'] = $this->language->get('zone_id');
			$data['custom_field'] = $this->language->get('custom_field');
			$data['text_none'] = $this->language->get('text_none');
			$data['entry_min_order'] = $this->language->get('entry_min_order');
			$data['entry_checkout_fields'] = $this->language->get('entry_checkout_fields');

			$data['entry_label'] = $this->language->get('entry_label');
	    $data['entry_placeholder'] = $this->language->get('entry_placeholder');
	    $data['entry_sorting'] = $this->language->get('entry_sorting');
	    $data['entry_status'] = $this->language->get('entry_status');
	    $data['entry_editable'] = $this->language->get('entry_editable');
			$data['entry_helper'] = $this->language->get('entry_helper');
			$data['entry_value'] = $this->language->get('entry_value');
			$data['entry_country'] = $this->language->get('entry_country');
			$data['entry_zone'] = $this->language->get('entry_zone');
			$data['help_shipping'] = $this->language->get('help_shipping');
			$data['entry_shipping'] = $this->language->get('entry_shipping');
			$data['entry_type'] = $this->language->get('entry_type');
			$data['entry_no_callback'] = $this->language->get('entry_no_callback');

			$data['help_fields'] = $this->language->get('help_fields');
			$data['help_no_callback'] = $this->language->get('help_no_callback');

      $data['tab_general'] = $this->language->get('tab_general');
      $data['tab_shipping']  = $this->language->get('tab_shipping');
      $data['tab_payment'] = $this->language->get('tab_payment');

      $data['button_save'] = $this->language->get('button_save');
      $data['button_cancel'] = $this->language->get('button_cancel');


      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_stores'),
        'href' => $this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('setting/checkout_setting', 'token=' . $this->session->data['token'], 'SSL')
      );

			if(isset($this->error['error_shipping'])){
				$data['error_shipping'] = $this->error['error_shipping'];
			}else{
				$data['error_shipping'] = array();
			}

			if(isset($this->error['error_payment'])){
				$data['error_payment'] = $this->error['error_payment'];
			}else{
				$data['error_payment'] = array();
			}

			if(isset($this->error['warning'])){
				$data['error_warning'] = $this->error['warning'];
			}else{
				$data['error_warning'] = '';
			}

			if(isset($this->error['error_min_order'])){
				$data['error_min_order'] = $this->error['error_min_order'];
			}else{
				$data['error_min_order']='';
			}

      if (isset($this->session->data['success'])) {
        $data['success'] = $this->session->data['success'];
        unset($this->session->data['success']);
      } else {
        $data['success'] = '';
      }

			if(isset($this->request->post['checkout_min_order'])){
				$data['checkout_min_order'] = $this->request->post['checkout_min_order'];
			}elseif(($this->config->get('checkout_min_order')!== null)){
				$data['checkout_min_order'] = $this->config->get('checkout_min_order');
			}else{
				$data['checkout_min_order'] = '';
			}

			$this->load->model('module/register_setting');

      $fields = $this->model_module_register_setting->getCustomerRegisterFields();


			foreach ($fields as $field){
				$data['checkout_fields'][] = array(
					 'name' => $field['name']
				 );
			}
			$data['fields'] = $this->model_setting_checkout_setting->getAddressFields();

			foreach ($data['fields'] as $key => $field) {
				if (in_array($field['Field'], ['address_id', 'customer_id', 'custom_field', 'country', 'zone'])) {
					unset($data['fields'][$key]);
				}
			}

      $data['languages'] = $this->model_localisation_language->getLanguages();

			$data['checkout_fast_order_status'] = $this->config->get('checkout_fast_order_status');
			$data['checkout_no_callback'] = isset($this->request->post['checkout_no_callback']) ? $this->request->post['checkout_no_callback'] : $this->config->get('checkout_no_callback');

			foreach($data['extensions'] as $shipping){
				$sort_order = 0;
				foreach ($data['fields'] as $key => $filed) {
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'][$filed['Field']]['label'] = [];
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'][$filed['Field']]['placeholder'] = [];
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'][$filed['Field']]['helper'] = [];
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'][$filed['Field']]['value'] = [];
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'][$filed['Field']]['sort_order'] = $sort_order++;
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'][$filed['Field']]['type'] = '';
				}
				if(isset($this->request->post['checkout_' . $shipping['code'] . '_fields'])){
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'] = $this->request->post['checkout_' . $shipping['code'] . '_fields'];
				} elseif ($this->config->get('checkout_' . $shipping['code'] . '_fields')!==null){
					$data['shippings']['checkout_' . $shipping['code'] . '_fields'] = $this->config->get('checkout_' . $shipping['code'] . '_fields');
				} else {

				}
			}

			if(isset($this->request->post['checkout_guest_order_fields'])){
				$data['checkout_guest_order_fields'] = $this->request->post['checkout_guest_order_fields'];
			}elseif($this->config->get('checkout_guest_order_fields')){
				$data['checkout_guest_order_fields'] = $this->config->get('checkout_guest_order_fields');
			}else{
				$data['checkout_guest_order_fields'] = array();
			}


			foreach($data['shippings']  as $checkout_shippings_fields => $shippings){
				foreach ($shippings as $key1 => $value) {
					if($shippings['country_id']['value'] != ''){
						foreach($shippings['country_id']['value'] as $key => $shipping){
							$array_country[$this->model_localisation_country->getCountryName($shipping)] = $shipping;
							$data['shippings'][$checkout_shippings_fields]['country_id']['value'] = $array_country;
						}
						unset($array_country);
					}
				if($shippings['zone_id']['value']!= ''){
					foreach($shippings['zone_id']['value'] as $key => $shipping){
						$array_zone[$this->model_localisation_zone->getZoneName($shipping)] = $shipping;
						$data['shippings'][$checkout_shippings_fields]['zone_id']['value'] = $array_zone;
					}
					unset($array_zone);
				}
			}
		}

		$data['field_types']['choose']['text'] = $this->language->get('text_choose');
		$data['field_types']['input']['text'] = $this->language->get('text_input');
		$data['field_types']['file']['text'] = $this->language->get('text_file');
		$data['field_types']['date']['text'] = $this->language->get('text_date');

		$data['field_types']['choose']['types']['select'] = $this->language->get('text_select');
		$data['field_types']['choose']['types']['radio'] = $this->language->get('text_radio');
		$data['field_types']['choose']['types']['checkbox'] = $this->language->get('text_checkbox');
		$data['field_types']['choose']['types']['datalist'] = $this->language->get('text_datalist');
		$data['field_types']['input']['types']['text'] = $this->language->get('text_text');
		$data['field_types']['input']['types']['tel'] = $this->language->get('text_tel');
		$data['field_types']['input']['types']['textarea'] = $this->language->get('text_textarea');
		$data['field_types']['file']['types']['file'] = $this->language->get('text_file');
		$data['field_types']['date']['types']['date'] = $this->language->get('text_date');
		$data['field_types']['date']['types']['time'] = $this->language->get('text_time');
		$data['field_types']['date']['types']['datetime'] = $this->language->get('text_datetime');


		foreach($data['payments'] as $payment){
			if(isset($this->request->post['checkout_' . $payment['code'] . '_min_order'])){
				$data['payments_data']['checkout_' . $payment['code'] . '_min_order'] = $this->request->post['checkout_' . $payment['code'] . '_min_order'];
			}elseif($this->config->get('checkout_' . $payment['code'] . '_min_order') !== null){
				$data['payments_data']['checkout_' . $payment['code'] . '_min_order'] = $this->config->get('checkout_' . $payment['code'] . '_min_order');
			}else{
				$data['payments_data']['checkout_' . $payment['code'] . '_min_order'] = array();
			}

			if(isset($this->request->post['checkout_' . $payment['code'] . '_shippings'])){
				$data['payments_data']['checkout_' . $payment['code'] . '_shippings'] = $this->request->post['checkout_' . $payment['code'] . '_shippings'];
			}elseif($this->config->get('checkout_' . $payment['code'] . '_shippings')!==null){
				$data['payments_data']['checkout_' . $payment['code'] . '_shippings'] = $this->config->get('checkout_' . $payment['code'] . '_shippings');
			}else{
				$data['payments_data']['checkout_' . $payment['code'] . '_shippings'] = array();
			}
		}


		foreach ($data['payments_data'] as $key => $payments_data) {
			if(stripos($key,'shippings')){
				foreach($payments_data as $key1 => $payment_data){
					$extension = basename($payment_data, '.php');
					$this->load->language('shipping/' . $extension);
					$array[$this->language->get('heading_title')] = $payments_data[$key1];
					$data['payments_data'][$key] = $array;
				}
				unset($array);
			}
		}

      $data['action'] = $this->url->link('setting/checkout_setting', 'token=' . $this->session->data['token'], 'SSL');

      $data['cancel'] = $this->url->link('setting/store', 'token=' . $this->session->data['token'], 'SSL');

      $data['token'] = $this->session->data['token'];

      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      $this->response->setOutput($this->load->view('setting/checkout_setting.tpl', $data));
  }

	protected function validate($extensions, $payments) {

		foreach($payments as $payment){

			if(isset($this->request->post['checkout_' . $payment['code'] . '_min_order'])){
				if(trim($this->request->post['checkout_' . $payment['code'] . '_min_order']) == ''){
					$this->request->post['checkout_' . $payment['code'] . '_min_order'] = 0;
				}
				if(!is_numeric($this->request->post['checkout_' . $payment['code'] . '_min_order'])){
					$this->error['error_payment']['checkout_' . $payment['code'] . '_min_order'] = $this->language->get('error_numeric');
				}
			}
		}

		if(utf8_strlen($this->request->post['checkout_min_order'])<1){
			$this->error['error_min_order'] = $this->language->get('error_min_order');
		}

		if(!is_numeric($this->request->post['checkout_min_order'])){
			$this->error['error_min_order'] = $this->language->get('error_numeric');
		}

		foreach($extensions as $shipping){
			if(isset($this->request->post['checkout_' . $shipping['code'] . '_fields'])){
				$this->request->post['shippings']['checkout_' . $shipping['code'] . '_fields'] = $this->request->post['checkout_' . $shipping['code'] . '_fields'];
			}
		}

		if (!$this->user->hasPermission('modify', 'setting/checkout_setting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['shippings'] as $key1 => $shipping_fields) {
			foreach ($shipping_fields as $name => $setting) {
				foreach ($setting['placeholder'] as $key => $value) {
					if(utf8_strlen($value) < 0 || utf8_strlen($value) > 100){
						$this->error['error_shipping'][$key1][$name]['placeholder'][$key] = $this->language->get('error_lenght');
					}
				}
				foreach ($setting['label'] as $key => $value) {

					if(utf8_strlen($value) < 0 || utf8_strlen($value) > 100){
						$this->error['error_shipping'][$key1][$name]['label'][$key] = $this->language->get('error_lenght');
					}
				}

				if(trim($setting['sort_order']) == ''){
					$setting['sort_order'] = 0;
				}

				if(!is_numeric($setting['sort_order'])){
					$this->error['error_shipping'][$key1][$name]['sort_order'] = $this->language->get('error_numeric');
				}
			}
		}


		unset($this->request->post['shippings']);


		return !$this->error;

	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
			$this->load->model('localisation/country');
			$this->load->model('catalog/option');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'filter_model' => $filter_model,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_localisation_country->getCountries($filter_data);

			$json = $results;



			foreach ($results as $result) {

				$json[] = array(
					'country_id' => $result['country_id'],
					'name'       => $result['name']
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}
