<?php
class ControllerMarketingSaleEvents extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('marketing/sale_events');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/sale_events');

		$this->getList();
	}

	public function add() {

		$this->load->language('marketing/sale_events');
		//
		$this->document->setTitle($this->language->get('heading_title'));
		//
		$this->load->model('marketing/sale_events');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			if(!isset($this->request->post['disable_coupon'])){
				$this->request->post['disable_coupon'] = array();
			}

			$this->model_marketing_sale_events->addSaleEvents($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_type_name'])) {
				$url .= '&filter_type_name=' . $this->request->get['filter_type_name'];
			}

			if (isset($this->request->get['filter_datetime_start'])) {
				$url .= '&filter_datetime_start=' . $this->request->get['filter_datetime_start'];
			}

			if (isset($this->request->get['filter_datetime_end'])) {
				$url .= '&filter_datetime_end=' . $this->request->get['filter_datetime_end'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function edit() {

		$this->load->language('marketing/sale_events');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/sale_events');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {


			if(!isset($this->request->post['disable_coupon'])){
				$this->request->post['disable_coupon'] = array();
			}

			$this->model_marketing_sale_events->editSaleEvents($this->request->get['sale_event_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_type_name'])) {
				$url .= '&filter_type_name=' . $this->request->get['filter_type_name'];
			}

			if (isset($this->request->get['filter_datetime_start'])) {
				$url .= '&filter_datetime_start=' . $this->request->get['filter_datetime_start'];
			}

			if (isset($this->request->get['filter_datetime_end'])) {
				$url .= '&filter_datetime_end=' . $this->request->get['filter_datetime_end'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {

		$this->load->language('marketing/sale_events');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('marketing/sale_events');

		if (isset($this->request->get['sale_event_id']) && $this->validateDelete()) {

		 		$this->model_marketing_sale_events->deleteSaleEvent($this->request->get['sale_event_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_type_name'])) {
			$url .= '&filter_type_name=' . $this->request->get['filter_type_name'];
		}

		if (isset($this->request->get['filter_datetime_start'])) {
			$url .= '&filter_datetime_start=' . $this->request->get['filter_datetime_start'];
		}

		if (isset($this->request->get['filter_datetime_end'])) {
			$url .= '&filter_datetime_end=' . $this->request->get['filter_datetime_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			$this->response->redirect($this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_type_name'])) {
			$filter_type_name = $this->request->get['filter_type_name'];
		} else {
			$filter_type_name = null;
		}

		if (isset($this->request->get['filter_datetime_start'])) {
			$filter_datetime_start = $this->request->get['filter_datetime_start'];
		} else {
			$filter_datetime_start = null;
		}

		if (isset($this->request->get['filter_datetime_end'])) {
			$filter_datetime_end = $this->request->get['filter_datetime_end'];
		} else {
			$filter_datetime_end = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'se.datetime_start';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_type_name'])) {
			$url .= '&filter_type_name=' . $this->request->get['filter_type_name'];
		}

		if (isset($this->request->get['filter_datetime_start'])) {
			$url .= '&filter_datetime_start=' . $this->request->get['filter_datetime_start'];
		}

		if (isset($this->request->get['filter_datetime_end'])) {
			$url .= '&filter_datetime_end=' . $this->request->get['filter_datetime_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('marketing/sale_events/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('marketing/sale_events/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['marketings'] = array();

		$filter_data = array(
			'filter_name'       		=> $filter_name,
			'filter_type_name'      => $filter_type_name,
			'filter_datetime_start' => $filter_datetime_start,
			'filter_datetime_end' 	=> $filter_datetime_end,
			'sort'              		=> $sort,
			'order'             		=> $order,
			'start'             		=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             		=> $this->config->get('config_limit_admin')
		);

		$sale_events_total = $this->model_marketing_sale_events->getTotalSaleEvents($filter_data);

		$results = $this->model_marketing_sale_events->getSaleEvents($filter_data);

		$data['sale_events'] = array();

		foreach ($results as $result) {

			if(isset($result['class'])){
				$class = $result['class'];
			}else{
				$class = '';
			}

			$data['sale_events'][] = array(
				'sale_event_id'      => $result['sale_event_id'],
        'type_name'          => $result['type_name'],
				'name'               => $result['name'],
				'class'							 => $class,
				'datetime_start'     => date($this->language->get('date_format_short'), strtotime($result['datetime_start'])),
				'datetime_end'       => date($this->language->get('date_format_short'), strtotime($result['datetime_end'])),
				'edit'               => $this->url->link('marketing/sale_events/edit', 'token=' . $this->session->data['token'] . '&sale_event_id=' . $result['sale_event_id'] . $url, 'SSL'),
				'info'							 => $this->url->link('marketing/sale_events/info', 'token=' . $this->session->data['token'] . '&sale_event_id=' . $result['sale_event_id'] . $url, 'SSL'),
				'delete'						 => $this->url->link('marketing/sale_events/delete', 'token=' . $this->session->data['token'] . '&sale_event_id=' . $result['sale_event_id'] . $url, 'SSL')
			);
		}

		$data['heading_title'] 					= $this->language->get('heading_title');

		$data['text_list'] 							= $this->language->get('text_list');
		$data['text_no_results'] 				= $this->language->get('text_no_results');
		$data['text_confirm'] 					= $this->language->get('text_confirm');

		$data['column_name']		 				= $this->language->get('column_name');
		$data['column_type_name'] 			= $this->language->get('column_type_name');
		$data['column_datetime_start'] 	= $this->language->get('column_datetime_start');
		$data['column_datetime_end'] 		= $this->language->get('column_datetime_end');
		$data['column_action'] 					= $this->language->get('column_action');

		$data['entry_name'] 						= $this->language->get('entry_name');
		$data['entry_type_name'] 				= $this->language->get('entry_type_name');
		$data['entry_datetime_start'] 	= $this->language->get('entry_datetime_start');
		$data['entry_datetime_end']			= $this->language->get('entry_datetime_end');


		$data['button_add'] 						= $this->language->get('button_add');
		$data['button_view'] 						= $this->language->get('button_view');
		$data['button_edit'] 						= $this->language->get('button_edit');
		$data['button_delete'] 					= $this->language->get('button_delete');
		$data['button_filter'] 					= $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_type_name'])) {
			$url .= '&filter_type_name=' . $this->request->get['filter_type_name'];
		}

		if (isset($this->request->get['filter_datetime_start'])) {
			$url .= '&filter_datetime_start=' . $this->request->get['filter_datetime_start'];
		}

		if (isset($this->request->get['filter_datetime_end'])) {
			$url .= '&filter_datetime_end=' . $this->request->get['filter_datetime_end'];
		}

		$data['sale_event_types'] = $this->model_marketing_sale_events->getSaleEventTypes();

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . '&sort=sed.name' . $url, 'SSL');
		$data['sort_type_name'] = $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . '&sort=st.name' . $url, 'SSL');
		$data['sort_datetime_start'] = $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . '&sort=se.datetime_start' . $url, 'SSL');
    $data['sort_datetime_end'] = $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . '&sort=se.datetime_end' . $url, 'SSL');


		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_type_name'])) {
			$url .= '&filter_type_name=' . $this->request->get['filter_type_name'];
		}

		if (isset($this->request->get['filter_datetime_start'])) {
			$url .= '&filter_datetime_start=' . $this->request->get['filter_datetime_start'];
		}

		if (isset($this->request->get['filter_datetime_end'])) {
			$url .= '&filter_datetime_end=' . $this->request->get['filter_datetime_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $sale_events_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($sale_events_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sale_events_total - $this->config->get('config_limit_admin'))) ? $sale_events_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sale_events_total, ceil($sale_events_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_type_name'] = $filter_type_name;
		$data['filter_datetime_start'] = $filter_datetime_start;
		$data['filter_datetime_end'] = $filter_datetime_end;


		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/sale_events_list.tpl', $data));
	}

	public function getBannerImages(){
		if(isset($this->request->get['banner_id'])){
			$this->load->model('design/banner');
			$this->load->model('tool/image');

			$banner_id = $this->request->get['banner_id'];

			if(isset($this->request->get['sale_event_id']) && (int)$this->request->get['sale_event_id'] !== 0){
				$data['banner_image_radio'] = $this->model_design_banner->getBannerImageIdForSaleEvents($this->request->get['sale_event_id']);
			}else{
				$data['banner_image_radio'] = 0;
			}

			$data['config_language_id'] = $this->config->get('config_language_id');


			$results = $this->model_design_banner->getBannerImagesForSaleEvent($banner_id, $this->request->get['sale_event_id']);

			foreach ($results as $key => &$result){
				foreach($result['banner_image_description'] as $language_id => &$array_result){
					if (is_file(DIR_IMAGE . $array_result['image'])) {
						$results[$key]['banner_image_description'][$language_id]['image'] = $this->model_tool_image->resize($array_result['image'], 242, 200);
					}else{
						$results[$key]['banner_image_description'][$language_id]['image'] = $this->model_tool_image->resize('no_image.png', 242, 200);

					}
				}
			}
			$data['banner_images'] = $results;

			$this->response->setOutput($this->load->view('marketing/banner_image.tpl', $data));
		}else{
			false;
		}
	}

	public function getSettings(){
		if (isset($this->request->get['setting_row'])){

			$this->load->language('marketing/sale_events');
			$this->load->model('sale/customer_group');
			$this->load->model('catalog/option');


			$data['setting_row'] 						= $this->request->get['setting_row'];

			$data['token'] = $this->session->data['token'];

			$data['text_new_heading_title'] = $this->language->get('text_new_heading_title');

			$data['entry_persent'] 					= $this->language->get('entry_persent');
			$data['entry_priority']				= $this->language->get('entry_priority');
			$data['entry_customer_group']		= $this->language->get('entry_customer_group');
			$data['entry_option']						= $this->language->get('entry_option');
			$data['entry_product'] 					= $this->language->get('entry_product');
			$data['entry_category']					= $this->language->get('entry_category');
			$data['entry_manufacturer'] 		= $this->language->get('entry_manufacturer');

			$data['help_customer_group']		= $this->language->get('help_customer_group');
			$data['help_option']						= $this->language->get('help_option');
			$data['help_product']						= $this->language->get('help_product');
			$data['help_category'] 					= $this->language->get('help_category');
			$data['help_manufacturer'] 			= $this->language->get('help_manufacturer');

			$data['button_remove'] 					= $this->language->get('button_remove');

			$data['customer_groups']				= $this->model_sale_customer_group->getCustomerGroups();

			$options = $this->model_catalog_option->getOptions();

			foreach ($options as $option){
				$values = $this->model_catalog_option->getOptionValues($option['option_id']);
				foreach ($values as $value){
					$data['options'][$option['option_id']][] = array(
						$value['option_value_id'] => $option['name'].' : '. $value['name'],
					);
				}
			}

			$this->response->setOutput($this->load->view('marketing/sale_events_settings.tpl', $data));

		}else{
			false;
		}
	}

	public function deleteSale(){
		if(isset($this->request->get['sale_event_id'])){
			$this->load->model('marketing/sale_events');
			$this->model_marketing_sale_events->deleteSaleEventsSetting($this->request->get['sale_event_id']);
		}else{
			false;
		}
	}

	public function info(){


		$this->load->language('marketing/sale_events');

		$data['heading_title'] 										= $this->language->get('heading_title');
		$data['text_sale_event_type']							= $this->language->get('text_sale_event_type');
		$data['text_sale_events_time'] 						= $this->language->get('text_sale_events_time');
		$data['text_sale_events']									= $this->language->get('text_sale_events');
		$data['text_enebled_coupons'] 						= $this->language->get('text_enebled_coupons');
		$data['text_disabled_coupons']						= $this->language->get('text_disabled_coupons');
		$data['text_sale_event_name']							= $this->language->get('text_sale_event_name');
		$data['text_sale_event_description'] 			= $this->language->get('text_sale_event_description');
		$data['text_sale_event_banner_and_news']  = $this->language->get('text_sale_event_banner_and_news');
		$data['text_banner_status']								= $this->language->get('text_banner_status');
		$data['text_banner_image'] 								= $this->language->get('text_banner_image');
		$data['text_enebled']											= $this->language->get('text_enebled');
		$data['text_disabled'] 										= $this->language->get('text_disabled');
		$data['text_news_name']										= $this->language->get('text_news_name');
		$data['text_news_short_description'] 			= $this->language->get('text_news_short_description');
		$data['text_news_status']									= $this->language->get('text_news_status');
		$data['text_categories'] 									= $this->language->get('text_categories');
		$data['text_manufacturers'] 							= $this->language->get('text_manufacturers');
		$data['text_products'] 										= $this->language->get('text_products');
		$data['text_customer_groups']						 	= $this->language->get('text_customer_groups');
		$data['text_product_name']								= $this->language->get('text_product_name');
		$data['text_product_model']								= $this->language->get('text_product_model');
		$data['text_product_price']								= $this->language->get('text_product_price');
		$data['text_options_name']								= $this->language->get('text_options_name');
		$data['text_options_price']								= $this->language->get('text_options_price');

		$data['button_edit'] 											= $this->language->get('button_edit');
		$data['button_cancel']										= $this->language->get('button_cancel');
		$data['button_delete'] 										= $this->language->get('button_delete');

		$this->load->model('localisation/language');
		$this->load->model('marketing/sale_events');

		$data['token']														= $this->session->data['token'];


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['sale_event_id'])) {
			$sale_event_info = $this->model_marketing_sale_events->getSaleEvent($this->request->get['sale_event_id']);
			$this->document->setTitle($this->language->get('heading_title_info') . ' ' . $sale_event_info['name']);
			$data['heading_title_info']								= $this->language->get('heading_title_info') . ' ' . $sale_event_info['name'];
			$data['text_sale_event_setting'] 					= $this->language->get('text_sale_event_setting') . ' ' . $sale_event_info['name'];

			if($sale_event_info['datetime_start']){
				$sale_event_info['datetime_start'] = date($this->language->get('date_long_format'), strtotime($sale_event_info['datetime_start']));
			}

			if($sale_event_info['datetime_end']){
				$sale_event_info['datetime_end'] = date($this->language->get('date_long_format'), strtotime($sale_event_info['datetime_end']));
			}

			$this->load->model('catalog/product');
			$this->load->model('catalog/category');
			$this->load->model('catalog/manufacturer');
			$this->load->model('design/banner');
			$this->load->model('marketing/coupon');

			$data['coupons'] = $this->model_marketing_coupon->getEnebledCoupons();

			$results = array();

			$results = $sale_event_info['sale_event_setting'];

			if($results){
				foreach($results as $key => $result){
					foreach ($result['product'] as $key1 => $product_id){
						$results[$key]['product'][$key1] = array(
							'product_id' => $product_id,
							'name' => $this->model_catalog_product->getProductName($product_id),
						);
					}
					if(isset($result['category']) && $result['category'] ){
						foreach ($result['category'] as $key1 => $category_id){
							$category = $this->model_catalog_category->getCategory($category_id);
							if($category['parent_id']!== 0){
								$name = $category['path'] . ' > ' . $category['name'];
							}else{
								$name = $category['name'];
							}
							$results[$key]['category'][$key1] = array(
								'category_id' => $category_id,
								'name' => $name,
							);
						}
					}

					if(isset($result['manufacturer']) && $result['manufacturer']){
						foreach ($result['manufacturer'] as $key1 => $manufacturer_id){
							$manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

							$results[$key]['manufacturer'][$key1] = array(
								'manufacturer_id' => $manufacturer_id,
								'name' => $manufacturer['name'],
							);
						}
					}
				}
			}
		}

		$sale_event_info['sale_event_setting'] = $results;

		$sale_event_type = $this->model_marketing_sale_events->getSaleEventType($sale_event_info['sale_event_type_id']);

		if($sale_event_type){
			$sale_event_info['sale_event_type_name'] = $sale_event_type[0]['name'];
		}

		$data['sale_event'] = $sale_event_info;



		//Banner

		$banner_id = $this->model_design_banner->getSaleEventBannerId($this->request->get['sale_event_id']);


		if($banner_id){
			$banner_image_id = $this->model_design_banner->getBannerImageIdForSaleEvents($this->request->get['sale_event_id']);
			$banners = $this->model_design_banner->getBannerImages($banner_id);

			if($banner_image_id && $banners){
				foreach ($banners as $banner){
					if($banner_image_id && (int)$banner['banner_image_id'] === (int)$banner_image_id){
						$banner_image = $banner['banner_image_description'][$this->config->get('config_language_id')]['image'];
					}
				}

				$this->load->model('tool/image');

				$banner_image = $this->model_tool_image->resize($banner_image, 200,70);

			}else{
				$banner_image = '';
			}
			$banner = $this->model_design_banner->getBanner($banner_id);


			$data['banner'] = array(
				'banner_image' => $banner_image,
				'status' => $banner['status'],
			);
		}else{
			$data['banner'] = array(
				'banner_image' => '',
				'status' => 0,
			);
		}

		//NEWS

		$this->load->model('extension/news');

		$news_id = $this->model_marketing_sale_events->getSaleEventNewsId($this->request->get['sale_event_id']);

		if($news_id){

			$news = $this->model_extension_news->getNews($news_id);

			$data['news_status'] = $news['status'];

			$news_descriptions = $this->model_extension_news->getNewsDescription($news_id);
			foreach($news_descriptions as $key => $news_description){
				if($key === (int)$this->config->get('config_language_id')){
					$data['news_description'] = array(
						'title' => $news_description['title'],
						'short_description'	=> $news_description['short_description'],
						'href' => $this->url->link('extension/news/edit', 'news_id=' . $news_id . '&token=' . $this->session->data['token'], 'SSL')
					);
				}
			}



		}else{
			$data['news_status'] = array();
			$data['news_description'] = array();
		}
		$products =	$this->model_marketing_sale_events->getProductsBySaleEventId($this->request->get['sale_event_id']);

		$this->load->model('sale/customer_group');

		foreach ($products as $key => $product){

			foreach($sale_event_info['sale_event_setting'][$key]['customer_group'] as $customer_group){
				$customer_group_data = $this->model_sale_customer_group->getCustomerGroup($customer_group);
				$products[$key]['customers_groups'][] = $customer_group_data['name'];
			}

			$products[$key]['tab_name'] = sprintf($this->language->get('text_percent_name'), $key) . '%';
		}

		if($sale_event_info['sale_event_setting']){
			foreach($sale_event_info['sale_event_setting'] as $value){
				if(isset($value['category']) && $value['category']){
					foreach($value['category'] as $category){
						$products[$value['persent']]['category_name'][$category['category_id']] = $category['name'];
					}
				}
				if(isset($value['manufacturer']) && $value['manufacturer']){

					foreach($value['manufacturer'] as $manufacturer){
						$products[$value['persent']]['manufacturer_name'][$manufacturer['manufacturer_id']] = $manufacturer['name'];
					}
				}
			}
		}

		$data['products'] = $products;

		$sale_event_description = $this->model_marketing_sale_events->getSaleEventDescription($this->request->get['sale_event_id']);

		foreach ($sale_event_description as $key => $description){
			$sale_event_description[$key]['description'] = html_entity_decode($description['description'], ENT_QUOTES, 'UTF-8');
		}


		$data['sale_event_description'] = $sale_event_description;

		$data['edit'] = $this->url->link('marketing/sale_events/edit', 'token=' . $this->session->data['token'] . '&sale_event_id=' . $this->request->get['sale_event_id'], 'SSL');
		$data['delete'] = $this->url->link('marketing/sale_events/delete', 'token=' . $this->session->data['token'] . '&sale_event_id=' .  $this->request->get['sale_event_id'], 'SSL');
		$data['cancel'] = $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'], 'SSL');

		$data['header']	= $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/sale_events_info.tpl', $data));

	}

	protected function getForm() {

		$this->load->model('catalog/option');
		$this->load->model('sale/customer_group');


		$options = $this->model_catalog_option->getOptions();

		$this->load->language('marketing/sale_events');

		$this->load->model('marketing/sale_events');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['marketing_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if(isset($this->request->get['sale_event_id'])){
			$data['sale_event_id'] = $this->request->get['sale_event_id'];
		}else{
			$data['sale_event_id'] = 0;
		}

		$data['text_new_heading_title'] = $this->language->get('text_new_heading_title');
		$data['text_none']							= $this->language->get('text_none');
		$data['text_heading_title']			= $this->language->get('text_heading_title');
		$data['text_enebled']						= $this->language->get('text_enebled');
		$data['text_disabled']					= $this->language->get('text_disabled');

		$data['entry_name'] 						= $this->language->get('entry_name');
		$data['entry_description'] 			= $this->language->get('entry_description');
		$data['entry_sale_event_type'] 	= $this->language->get('entry_sale_event_type');
		$data['entry_datetime_start'] 	= $this->language->get('entry_datetime_start');
		$data['entry_datetime_end'] 		= $this->language->get('entry_datetime_end');
		$data['entry_turn_coupons']			= $this->language->get('entry_turn_coupons');
		$data['entry_banner_group'] 		= $this->language->get('entry_banner_group');
		$data['entry_persent'] 					= $this->language->get('entry_persent');
		$data['entry_priority']					= $this->language->get('entry_priority');
		$data['entry_customer_group']		= $this->language->get('entry_customer_group');
		$data['entry_option']						= $this->language->get('entry_option');
		$data['entry_product'] 					= $this->language->get('entry_product');
		$data['entry_category']					= $this->language->get('entry_category');
		$data['entry_manufacturer'] 		= $this->language->get('entry_manufacturer');

		$data['column_name_news'] 			= $this->language->get('column_name_news');
		$data['column_on_off'] 					= $this->language->get('column_on_off');

		$data['help_turn_coupons'] 			= $this->language->get('help_turn_coupons');
		$data['help_customer_group']		= $this->language->get('help_customer_group');
		$data['help_option']						= $this->language->get('help_option');
		$data['help_product']						= $this->language->get('help_product');
		$data['help_category'] 					= $this->language->get('help_category');
		$data['help_manufacturer'] 			= $this->language->get('help_manufacturer');

		$data['button_save'] 						= $this->language->get('button_save');
		$data['button_cancel'] 					= $this->language->get('button_cancel');
		$data['button_settings_add']		= $this->language->get('button_settings_add');
		$data['button_remove']					= $this->language->get('button_remove');

		$data['tab_general'] 						= $this->language->get('tab_general');
		$data['tab_banners'] 						= $this->language->get('tab_banners');
		$data['tab_news'] 							= $this->language->get('tab_news');
		$data['tab_special'] 						= $this->language->get('tab_special');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['datetime_end'])) {
			$data['error_datetime_end'] = $this->error['datetime_end'];
		} else {
			$data['error_datetime_end'] = '';
		}

		if (isset($this->error['datetime_start'])) {
			$data['error_datetime_start'] = $this->error['datetime_start'];
		} else {
			$data['error_datetime_start'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if(isset($this->error['error_special'])){
			$data['error_special'] = $this->error['error_special'];
		}else{
			$data['error_special'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_type_name'])) {
			$url .= '&filter_type_name=' . $this->request->get['filter_type_name'];
		}

		if (isset($this->request->get['filter_datetime_start'])) {
			$url .= '&filter_datetime_start=' . $this->request->get['filter_datetime_start'];
		}

		if (isset($this->request->get['filter_datetime_end'])) {
			$url .= '&filter_datetime_end=' . $this->request->get['filter_datetime_end'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (isset($this->request->post['sale_event_description'])) {
				$data['sale_event_description'] = $this->request->post['sale_event_description'];
		} elseif (isset($this->request->get['sale_event_id'])) {
				$data['sale_event_description'] = $this->model_marketing_sale_events->getSaleEventDescription($this->request->get['sale_event_id']);
		} else {
				$data['sale_event_description'] = array();
		}

		$this->load->model('localisation/language');
		$this->load->model('extension/news');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->get['sale_event_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$sale_event_info = $this->model_marketing_sale_events->getSaleEvent($this->request->get['sale_event_id']);
		}

		if(isset($this->request->get['sale_event_id']) && !empty($sale_event_info['sale_event_setting']) || isset($this->request->post['sale_event_settings'])){

			$data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();

			$options = $this->model_catalog_option->getOptions();

			foreach ($options as $option){
				$values = $this->model_catalog_option->getOptionValues($option['option_id']);
				foreach ($values as $value){
					$data['options'][$option['option_id']][] = array(
						$value['option_value_id'] => $option['name'].' : '. $value['name'],
					);
				}
			}
		}

		if (!isset($this->request->get['sale_event_id'])) {
			$data['action'] = $this->url->link('marketing/sale_events/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('marketing/sale_events/edit', 'token=' . $this->session->data['token'] . '&sale_event_id=' . $this->request->get['sale_event_id'] . $url, 'SSL');
		}

		if(isset($this->request->post['sale_event_settings'])){
			$this->load->model('catalog/product');
			$this->load->model('catalog/category');
			$this->load->model('catalog/manufacturer');

			$results = $this->request->post['sale_event_settings'];

			foreach($results as $key => $result){
				if(isset($result['product']) && !empty($result['product'])){
					foreach ($result['product'] as $key1 => $product_id){
						$results[$key]['product'][$key1] = array(
							'product_id' => $product_id,
							'name' => $this->model_catalog_product->getProductName($product_id),
						);
					}
				}
				if(isset($result['category']) && !empty($result['category'])){
					foreach ($result['category'] as $key1 => $category_id){
						$category = $this->model_catalog_category->getCategory($category_id);
						if($category['parent_id']!== 0){
							$name = $category['path'] . ' > ' . $category['name'];
						}else{
							$name = $category['name'];
						}
						$results[$key]['category'][$key1] = array(
							'category_id' => $category_id,
							'name' => $name,
						);
					}
				}

				if(isset($result['manufacturer']) && !empty($result['manufacturer'])){
					foreach ($result['manufacturer'] as $key1 => $manufacturer_id){
							$manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
							$results[$key]['manufacturer'][$key1] = array(
								'manufacturer_id' => $manufacturer_id,
								'name' => $manufacturer['name'],
							);
						}
					}
				}

			$data['sale_event_settings'] = $results;
		}elseif(isset($this->request->get['sale_event_id']) && !empty($sale_event_info['sale_event_setting'])){

			$this->load->model('catalog/product');
			$this->load->model('catalog/category');
			$this->load->model('catalog/manufacturer');


			$results = $sale_event_info['sale_event_setting'];
			if($results){
				foreach($results as $key => $result){
					foreach ($result['product'] as $key1 => $product_id){
						$results[$key]['product'][$key1] = array(
							'product_id' => $product_id,
							'name' => $this->model_catalog_product->getProductName($product_id),
						);
					}
					if(isset($result['category']) && $result['category']){
						foreach ($result['category'] as $key1 => $category_id){
							$category = $this->model_catalog_category->getCategory($category_id);
							if($category['parent_id']!== 0){
								$name = $category['path'] . ' > ' . $category['name'];
							}else{
								$name = $category['name'];
							}
							$results[$key]['category'][$key1] = array(
								'category_id' => $category_id,
								'name' => $name,
							);
						}
					}
					if(isset($result['manufacturer']) && $result['manufacturer']){
						foreach ($result['manufacturer'] as $key1 => $manufacturer_id){
								$manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
								$results[$key]['manufacturer'][$key1] = array(
									'manufacturer_id' => $manufacturer_id,
									'name' => $manufacturer['name'],
								);
							}
						}
					}
				}

			$data['sale_event_settings'] = $results;


		}else{
			$data['sale_event_settings'] = array();
		}

		$data['cancel'] = $this->url->link('marketing/sale_events', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['datetime_start'])) {
			$data['datetime_start'] = $this->request->post['datetime_start'];
		} elseif (!empty($sale_event_info)) {
			$data['datetime_start'] = date($this->language->get('datetime_format_long'), strtotime($sale_event_info['datetime_start']));
		} else {
			$data['datetime_start'] = '';
		}

		if(isset($this->request->get['sale_event_id']) && !empty($sale_event_info)){
			$sale_event_id = $this->request->get['sale_event_id'];
			$date_added = $sale_event_info['datetime_start'];
			$date_added_year_ago = date("Y-m-d H:i:s", strtotime('- 1 year', strtotime($date_added)));
		}else{
			$sale_event_id = NULL;
			$date_added = date("Y-m-d H:i:s");
			$date_added_year_ago = date("Y-m-d H:i:s", strtotime('- 1 year'));
		}

		$results = $this->model_extension_news->getNewsSaleEvents($sale_event_id, $date_added, $date_added_year_ago);

		foreach($results as $result){
			$data['sale_events_news'][] = array(
																						'news_id' => $result['news_id'],
																						'name' => $result['title'],
																						'href' => $this->url->link('extension/news/edit', 'news_id=' . $result['news_id'] . '&token=' . $this->session->data['token'], 'SSL')
																					);
		}
		if(isset($this->request->post['sale_event_news'])){
			$data['sale_event_news_id'] = $this->request->post['sale_event_news'];
		}elseif(isset($this->request->get['sale_event_id'])){
			$data['sale_event_news_id'] = $this->model_marketing_sale_events->getSaleEventNewsId($this->request->get['sale_event_id']);
		}else{
			$data['sale_event_news_id'] = 0;
		}

		if(isset($this->request->post['sale_event_news'])){
			$data['sale_event_news'] = $this->request->post['sale_event_news'];
		}elseif(!empty($sale_event_news)){
			$data['sale_event_news'] = array();
		}else{
			$data['sale_event_news'] = array();
		}

		if(isset($this->request->post['disable_coupon'])){
			$data['disable_coupon'] = $this->request->post['disable_coupon'];
		}elseif(!empty($sale_event_info)){
			$data['disable_coupon'] = unserialize($sale_event_info['disable_coupon']);
		}else{
			$data['disable_coupon'] = array();
		}

		if (isset($this->request->post['datetime_end'])) {
			$data['datetime_end'] = $this->request->post['datetime_end'];
		} elseif (!empty($sale_event_info)) {
			$data['datetime_end'] = date($this->language->get('datetime_format_long'), strtotime($sale_event_info['datetime_end']));
		} else {
			$data['datetime_end'] = '';
		}

		if (isset($this->request->post['type'])) {
			$data['sale_event_type_id'] = $this->request->post['type'];
		} elseif (!empty($sale_event_info)) {
			$data['sale_event_type_id'] = $sale_event_info['sale_event_type_id'];
		} else {
			$data['sale_event_type_id'] = '';
		}

		$this->load->model('design/banner');

		$data['banners'] = $this->model_design_banner->getBanners();


		if(isset($this->request->get['sale_event_id'])){
			$data['banner_image_radio'] = $this->model_design_banner->getBannerImageIdForSaleEvents($this->request->get['sale_event_id']);
			$data['banner_group_id'] = $this->model_design_banner->getSaleEventBannerId($this->request->get['sale_event_id']);
		}else{
			$data['banner_image_radio'] = 0;
			$data['banner_group_id'] = 0;
		}

		$this->load->model('marketing/coupon');
		if(isset($this->request->get['sale_event_id'])){
			$sale_event_id = $this->request->get['sale_event_id'];
		}else{
			$sale_event_id = 0;

		}
			$data['coupons'] = $this->model_marketing_coupon->getSaleEventsCoupons($sale_event_id);

		$data['types'] = $this->model_marketing_sale_events->getSaleEventTypes();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/sale_events_form.tpl', $data));
	}

	protected function validateForm() {

		$this->load->language('marketing/sale_events');

		if(isset($this->request->post['sale_event_settings'])){
			foreach($this->request->post['sale_event_settings'] as $key => $sale_event_settings){

				$persent_array[$key] = $sale_event_settings['persent'];
				$custom_group_arr[$key] = implode(",",$sale_event_settings['customer_group']);

				if(!preg_match('/^[0-9]*$/', $sale_event_settings['persent']) || $sale_event_settings['persent'] > 100 || $sale_event_settings['persent'] < 0){
					$this->error['error_special'][$key]['persent'] = $this->language->get('error_persent');
				}

				if(!preg_match('/^[0-9]*$/', $sale_event_settings['priority'])){
					$this->error['error_special'][$key]['priority'] = $this->language->get('error_priority');
				}
			}


			if(count($persent_array) !== count(array_unique($persent_array))){


				$array = array_count_values($persent_array);
				$arr1 = array();
				foreach($array as $key => $value){
					if((int)$value > 1){
						$arr1[] = $key;
					}
				}

				foreach($arr1 as $arr){
					$values = array_keys($persent_array, $arr);
					foreach ($values as $value){

						foreach($custom_group_arr as $cust_key => $custom_group){
							if($cust_key === $value){}else{
								if(strripos($custom_group, $custom_group_arr[$value])!== false || strripos($custom_group_arr[$value], $custom_group)!== false){
									$this->error['error_special'][$value]['persent'] = $this->language->get('error_not_unique_persent');
								}
							}
						}

					}
				}
			}

		}

		foreach($this->request->post['sale_event_description'] as $language_id => $value){
			if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if(strtotime($this->request->post['datetime_start']) >= strtotime($this->request->post['datetime_end'])){
			$this->error['datetime_end'] = $this->language->get('error_datetime');
		}

		if (!$this->user->hasPermission('modify', 'marketing/sale_events')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function validateDelete() {
		if (!$this->user->hasPermission('modify', 'marketing/sale_events')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('marketing/sale_events');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_marketing_sale_events->getSaleEvents($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'sale_event_id'	 => $result['sale_event_id'],
					'name'         	 => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'datetime_start' => $result['datetime_start'],
					'datetime_end' 	 => $result['datetime_end'],
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function autocompleteCategory(){
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/category');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'sort'        => 'name',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_category->getCategories($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'],
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}
