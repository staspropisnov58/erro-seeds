<?php
class ControllerMailTemplatesBackToStock extends Controller {
	private $error = array();
	private $success = false;

	public function index() {
		$this->load->language('mail_templates/back_to_stock');

		if(isset($this->session->data['products'])){
			$products = explode(',',$this->session->data['products']);

			$this->load->model('catalog/product');

			foreach($products as $product_id){
				$prod = $this->model_catalog_product->getProduct($product_id);

				$products_array[] = array(
					'product_name' => $prod['name'],
					'product_id' => $prod['product_id']
				);
			}
			unset($this->session->data['products']);
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$data['config_admin_language'] = $this->config->get('config_admin_language');

		if(isset($products_array)){
			$data['products'] = $products_array;
		}else{
			$data['products'] = array();
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_test'] = $this->language->get('text_test');
		$data['text_wiev_latter'] = $this->language->get('text_wiev_latter');
		$data['text_send_test_latter'] = $this->language->get('text_send_test_latter');
		$data['text_search'] = $this->language->get('text_search');
		$data['text_none'] = $this->language->get('text_none');
		$data['entry_mail'] = $this->language->get('entry_mail');
		$data['entry_language'] = $this->language->get('entry_language');
		$data['entry_shop'] = $this->language->get('entry_shop');

		$data['token'] = $this->session->data['token'];
		$this->load->controller('sale/order/apiLogin');

		if(isset($this->session->data['error']) && $this->session->data['error'] !== NULL){
			$data['errors'] = $this->session->data['error'];
		}

		unset($this->session->data['error']);

		if($this->success){
			$data['success'] = $this->success;
		}

		unset($this->session->data['success']);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_mail_notify'),
			'href' => $this->url->link('design/mail_notify', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('mail_templates/back_to_stock', 'token=' . $this->session->data['token'], 'SSL')
		);

    $data['action'] = '';

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');


    $this->response->setOutput($this->load->view('mail_templates/back_to_stock.tpl', $data));

	}

	public function requestTemplate(){

		$this->load->language('mail_templates/back_to_stock');

		$json = array();

		// Store
		if (isset($this->request->get['store_id'])) {
				$store_id = $this->request->get['store_id'];
		} else {
				$store_id = 0;
		}

		$this->load->model('setting/store');

		$store_info = $this->model_setting_store->getStore($store_id);

		if ($store_info) {
				$url = $store_info['ssl'];
		} else {
				$url = HTTPS_CATALOG;
		}

		if (isset($this->session->data['cookie'])) {
			// Include any URL perameters
			$url_data = array();

			foreach ($this->request->get as $key => $value) {
				if ($key != 'route' && $key != 'token' && $key != 'store_id') {
						$url_data[$key] = $value;
				}
			}

			if(isset($this->request->post['products']) && isset($this->request->get['template_name'])){
				$this->session->data['products'] = $this->request->post['products'];
				$this->session->data['template_name'] = $this->request->get['template_name'];
				$this->session->data['title'] = $this->language->get('title_for_preview');
				$this->request->post['template_name'] = $this->request->get['template_name'];
				$this->request->post['title'] = $this->language->get('title_for_preview');
				$this->request->post['language_directory'] = $this->request->get['language'];

			}

			// if($this->request->get['products'] != NULL && $this->request->get['template_name'] !== NULL){
			// 	$this->request->post['products'] = $this->request->get['products'];
			// }

			$this->load->model('tool/curl');
			$json = $this->model_tool_curl->getCurl($this->request->post, $url_data, $url, 'api/mail/getContent');

			$this->response->setOutput($json);
		}

	}

	public function sendMail(){

		$this->load->language('mail_templates/back_to_stock');

		if (isset($this->request->get['store_id'])) {
				$store_id = $this->request->get['store_id'];
		} else {
				$store_id = 0;
		}

		$this->load->model('setting/store');

		$store_info = $this->model_setting_store->getStore($store_id);

		if ($store_info) {
				$url = $store_info['ssl'];
		} else {
				$url = HTTPS_CATALOG;
		}

		if (isset($this->session->data['cookie'])) {
			// Include any URL perameters
			$url_data = array();

			foreach ($this->request->get as $key => $value) {
				if ($key != 'route' && $key != 'token' && $key != 'store_id') {
						$url_data[$key] = $value;
				}
			}
		}

		if($this->request->post['products'] != NULL && $this->request->get['template_name'] !== NULL){
			$this->session->data['products'] = $this->request->post['products'];
			$this->request->post['template_name'] = $this->request->get['template_name'];
			$this->request->post['title'] = $this->language->get('title_for_preview');
			$this->request->post['email'] = $this->request->get['email'];
			$this->request->post['language_directory'] = $this->request->get['language'];

		}

		$this->load->model('tool/curl');
		$json = $this->model_tool_curl->getCurl($this->request->post, $url_data, $url, 'api/mail/SendMail');

		$json = json_decode($json, true);

		if($json['success']){
			$json['success'] = sprintf($this->language->get('text_mail_success'), $this->request->get['email']);
		}

		$json = json_encode($json);

		$this->response->setOutput($json);
	}

	public function preview($html){
		$this->load->language('mail_templates/back_to_stock');

		$json['success'] = $html;
		$json['title'] = $this->language->get('title_for_preview');
		$this->response->setOutput(json_encode($json));
	}

}
