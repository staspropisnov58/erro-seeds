<?php
class ControllerPaymentPaypal extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/paypal');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
    $this->load->model('localisation/order_status');
		$this->load->model('tool/image');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('paypal', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));

		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
    $data['text_percent'] = $this->language->get('text_percent');
    $data['text_amount'] = $this->language->get('text_amount');
		$data['text_image'] = $this->language->get('text_image');


		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_payment_details'] = $this->language->get('entry_payment_details');
    $data['entry_payment_commission'] = $this->language->get('entry_payment_commission');
		$data['entry_type'] = $this->language->get('entry_type');
		$data['entry_image_height'] = $this->language->get('entry_image_height');
		$data['entry_image_width'] = $this->language->get('entry_image_width');
		$data['entry_terms'] = $this->language->get('entry_terms');

		$data['help_type']  = $this->language->get('help_type');
    $data['help_payment_commission'] = $this->language->get('help_payment_commission');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (isset($this->error['terms' . $language['language_id']])) {
				$data['error_terms' . $language['language_id']] = $this->error['terms' . $language['language_id']];
			} else {
				$data['error_terms' . $language['language_id']] = '';
			}
		}

		if (isset($this->error['payment_commission'])) {
			$data['error_payment_commission'] = $this->error['payment_commission'];
		} else {
			$data['error_payment_commission'] = '';
		}

		if (isset($this->error['sort_order'])) {
			$data['error_sort_order'] = $this->error['sort_order'];
		} else {
			$data['error_sort_order'] = '';
		}

		if (isset($this->error['image_height'])) {
			$data['error_image_height'] = $this->error['image_height'];
		} else {
			$data['error_image_height'] = '';
		}

		if (isset($this->error['image_width'])) {
			$data['error_image_width'] = $this->error['image_width'];
		} else {
			$data['error_image_width'] = '';
		}


		$data['paypal_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/ecample', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('payment/paypal', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		foreach ($languages as $language) {
			if (isset($this->request->post['paypal_terms' . $language['language_id']])) {
				$data['paypal_terms' . $language['language_id']] = $this->request->post['paypal_terms' . $language['language_id']];
			} else {
				$data['paypal_terms' . $language['language_id']] = $this->config->get('paypal_terms' . $language['language_id']);
			}
		}

		if (isset($this->request->post['paypal_total'])) {
			$data['paypal_total'] = $this->request->post['paypal_total'];
		} else {
			$data['paypal_total'] = $this->config->get('paypal_total');
		}

    if (isset($this->request->post['type'])) {
			$data['type'] = $this->request->post['paypal_type'];
		} elseif ($this->config->get('paypal_type')!== null) {
			$data['type'] = $this->config->get('paypal_type');
		} else {
			$data['type'] = '';
		}

		if (isset($this->request->post['paypal_image'])) {
			$data['image'] = $this->request->post['paypal_image'];
		} elseif (!empty($this->config->get('paypal_image'))) {
			$data['image'] = $this->config->get('paypal_image');
		} else {
			$data['image'] = '';
		}

		if (isset($this->request->post['paypal_image']) && is_file(DIR_IMAGE . $this->request->post['paypal_image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['paypal_image'], 100, 100);
		} elseif (!empty($this->config->get('paypal_image')) && is_file(DIR_IMAGE . $this->config->get('paypal_image'))) {
			$data['thumb'] = $this->model_tool_image->resize($this->config->get('paypal_image'), 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		if (isset($this->request->post['paypal_order_status_id'])) {
			$data['paypal_order_status_id'] = $this->request->post['paypal_order_status_id'];
		} else {
			$data['paypal_order_status_id'] = $this->config->get('paypal_order_status_id');
		}

		if (isset($this->request->post['paypal_image_height'])) {
			$data['paypal_image_height'] = $this->request->post['paypal_image_height'];
		} else {
			$data['paypal_image_height'] = $this->config->get('paypal_image_height');
		}

		if (isset($this->request->post['paypal_image_width'])) {
			$data['paypal_image_width'] = $this->request->post['paypal_image_width'];
		} else {
			$data['paypal_image_width'] = $this->config->get('paypal_image_width');
		}

		$this->load->model('localisation/order_status');

		$data['paypal_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['paypal_geo_zone_id'])) {
			$data['paypal_geo_zone_id'] = $this->request->post['paypal_geo_zone_id'];
		} else {
			$data['paypal_geo_zone_id'] = $this->config->get('paypal_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['paypal_status'])) {
			$data['paypal_status_id'] = $this->request->post['paypal_status'];
		} else {
			$data['paypal_status_id'] = $this->config->get('paypal_status_id');
		}

		if (isset($this->request->post['paypal_sort_order'])) {
			$data['examle_sort_order'] = $this->request->post['paypal_sort_order'];
		} else {
			$data['paypal_sort_order'] = $this->config->get('paypal_sort_order');
		}

		if (isset($this->request->post['paypal_payment_details'])) {
			$data['paypal_payment_details'] = $this->request->post['paypal_payment_details'];
		} else {
			$data['paypal_payment_details'] = $this->config->get('paypal_payment_details');
		}

    if (isset($this->request->post['paypal_payment_commission'])) {
			$data['paypal_payment_commission'] = $this->request->post['paypal_payment_commission'];
		} else {
			$data['paypal_payment_commission'] = $this->config->get('paypal_payment_commission');
		}

		$data['languages'] = $languages;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('payment/paypal.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/paypal')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (empty($this->request->post['paypal_terms' . $language['language_id']])) {
				$this->error['terms' .  $language['language_id']] = $this->language->get('error_terms');
			}
		}

		if( isset($this->request->post['paypal_payment_commission']) && trim($this->request->post['paypal_payment_commission']) == ''){
			$this->request->post['paypal_payment_commission'] = 0;
		}

		if(!is_numeric($this->request->post['paypal_payment_commission'])){
			$this->error['payment_commission'] = $this->language->get('error_payment_commission');
		}

		if( isset($this->request->post['paypal_sort_order']) && trim($this->request->post['paypal_sort_order']) == ''){
			$this->request->post['paypal_sort_order'] = 0;
		}

		if(!is_numeric($this->request->post['paypal_sort_order'])){
			$this->error['sort_order'] = $this->language->get('error_sort_order');
		}

		if(!is_numeric($this->request->post['paypal_image_height']) || strpos($this->request->post['paypal_image_height'], '.') !== false || strpos($this->request->post['paypal_image_height'], ',') !== false){
			$this->error['image_height'] = $this->language->get('error_image_height');
		}

		if(!is_numeric($this->request->post['paypal_image_width']) || strpos($this->request->post['paypal_image_width'], '.') !== false || strpos($this->request->post['paypal_image_width'], ',') !== false){
			$this->error['image_width'] = $this->language->get('error_image_width');
		}


		return !$this->error;
	}
}
