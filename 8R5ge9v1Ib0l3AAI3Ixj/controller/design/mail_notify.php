<?php
class ControllerDesignMailNotify extends Controller {
  private $error = array();
  public function index() {
    $this->load->language('design/mail_notify');


		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('design/banner');

		$this->getList();
	}

  protected function getList() {
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('design/mail_notify', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_layout'] = sprintf($this->language->get('text_layout'), $this->url->link('design/layout', 'token=' . $this->session->data['token'], 'SSL'));
    $data['text_list'] = $this->language->get('text_list');
    $data['text_no_results'] = $this->language->get('text_no_results');
    $data['text_confirm'] = $this->language->get('text_confirm');

    $data['column_name'] = $this->language->get('column_name');
    $data['column_action'] = $this->language->get('column_action');

    $data['button_view'] = $this->language->get('button_view');
    $data['button_delete'] = $this->language->get('button_delete');
    $data['button_install'] = $this->language->get('button_install');
    $data['button_uninstall'] = $this->language->get('button_uninstall');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }




    	$files = glob(DIR_APPLICATION . 'controller/mail_templates/*.php');

      if ($files) {
  			foreach ($files as $file) {
  				$extension = basename($file, '.php');

          $this->load->language('mail_templates/' . $extension);

          $data['mail_templates'][] = array(
  					'name'      => $this->language->get('heading_title'),
  					'info'      => $this->url->link('mail_templates/' . $extension, 'token=' . $this->session->data['token'], 'SSL')
  				);
  			}
  		}

      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      $this->response->setOutput($this->load->view('design/mail_notify.tpl', $data));

  }

}
