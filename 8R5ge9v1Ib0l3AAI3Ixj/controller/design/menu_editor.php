<?php
class ControllerDesignMenuEditor extends Controller
{
  public function index()
  {
    $this->load->language('design/menu_editor');

    $this->document->setTitle($this->language->get('heading_title'));

    // $this->load->model('design/menu_editor');

    $this->getList();
  }

  protected function getList()
  {

    $data['add_menu'] = $this->url->link('design/menu_editor/addMenu', 'token=' . $this->session->data['token'] . $url, 'SSL');

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('design/menu_editor/menu_list.tpl', $data));
  }

  public function addMenu()
  {
    
  }
}
