<?php
class ControllerShippingIntime extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('shipping/intime');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('tool/image');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('intime', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');

		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_image_height'] = $this->language->get('entry_image_height');
		$data['entry_image_width'] = $this->language->get('entry_image_width');
		$data['entry_note'] = $this->language->get('entry_note');
		$data['text_image'] = $this->language->get('text_image');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['cost'])) {
			$data['error_cost'] = $this->error['cost'];
		} else {
			$data['error_cost'] = '';
		}

		if (isset($this->error['sort_order'])) {
			$data['error_sort_order'] = $this->error['sort_order'];
		} else {
			$data['error_sort_order'] = '';
		}

		if (isset($this->error['image_height'])) {
			$data['error_image_height'] = $this->error['image_height'];
		} else {
			$data['error_image_height'] = '';
		}

		if (isset($this->error['image_width'])) {
			$data['error_image_width'] = $this->error['image_width'];
		} else {
			$data['error_image_width'] = '';
		}

		foreach ($languages as $language) {
			if (isset($this->error['note'.$language['language_id']])) {
				$data['error_note'.$language['language_id']] = $this->error['note'.$language['language_id']];
			} else {
				$data['error_note'.$language['language_id']] = '';
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('shipping/intime', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('shipping/intime', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

		foreach ($languages as $language) {
			if (isset($this->request->post['intime_note' . $language['language_id']])) {
				$data['intime_note' . $language['language_id']] = $this->request->post['intime_note' . $language['language_id']];
			} else {
				$data['intime_note' . $language['language_id']] = $this->config->get('intime_note' . $language['language_id']);
			}
		}

		$data['languages'] = $languages;

		if (isset($this->request->post['intime_cost'])) {
			$data['intime_cost'] = $this->request->post['intime_cost'];
		} else {
			$data['intime_cost'] = $this->config->get('intime_cost');
		}

		if (isset($this->request->post['intime_image'])) {
			$data['image'] = $this->request->post['intime_image'];
		} elseif (!empty($this->config->get('intime_image'))) {
			$data['image'] = $this->config->get('intime_image');
		} else {
			$data['image'] = '';
		}

		if (isset($this->request->post['intime_image']) && is_file(DIR_IMAGE . $this->request->post['intime_image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['intime_image'], 100, 100);
		} elseif (!empty($this->config->get('intime_image')) && is_file(DIR_IMAGE . $this->config->get('intime_image'))) {
			$data['thumb'] = $this->model_tool_image->resize($this->config->get('intime_image'), 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		if (isset($this->request->post['intime_image_height'])) {
			$data['intime_image_height'] = $this->request->post['intime_image_height'];
		} else {
			$data['intime_image_height'] = $this->config->get('intime_image_height');
		}

		if (isset($this->request->post['intime_image_width'])) {
			$data['intime_image_width'] = $this->request->post['intime_image_width'];
		} else {
			$data['intime_image_width'] = $this->config->get('intime_image_width');
		}

		if (isset($this->request->post['intime_total'])) {
			$data['intime_total'] = $this->request->post['intime_total'];
		} else {
			$data['intime_total'] = $this->config->get('intime_total');
		}

		if (isset($this->request->post['intime_geo_zone_id'])) {
			$data['intime_geo_zone_id'] = $this->request->post['intime_geo_zone_id'];
		} else {
			$data['intime_geo_zone_id'] = $this->config->get('intime_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['intime_status'])) {
			$data['intime_status'] = $this->request->post['intime_status'];
		} else {
			$data['intime_status'] = $this->config->get('intime_status');
		}

		if (isset($this->request->post['intime_sort_order'])) {
			$data['intime_sort_order'] = $this->request->post['intime_sort_order'];
		} else {
			$data['intime_sort_order'] = $this->config->get('intime_sort_order');
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/intime.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/intime')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (utf8_strlen($this->request->post['intime_note' . $language['language_id']]) > 1000) {
				$this->error['note' .  $language['language_id']] = $this->language->get('error_note');
			}
		}

		if( isset($this->request->post['intime_cost']) && trim($this->request->post['intime_cost']) == ''){
			$this->request->post['intime_cost'] = 0;
		}

		if(!is_numeric($this->request->post['intime_cost'])){
			$this->error['cost'] = $this->language->get('error_cost');
		}

		if( isset($this->request->post['intime_sort_order']) && trim($this->request->post['intime_sort_order']) == ''){
			$this->request->post['intime_sort_order'] = 0;
		}

		if(!is_numeric($this->request->post['intime_sort_order'])){
			$this->error['sort_order'] = $this->language->get('error_sort_order');
		}

		if(!is_numeric($this->request->post['intime_image_height']) || strpos($this->request->post['intime_image_height'], '.') !== false || strpos($this->request->post['intime_image_height'], ',') !== false){
			$this->error['image_height'] = $this->language->get('error_image_height');
		}

		if(!is_numeric($this->request->post['intime_image_width']) || strpos($this->request->post['intime_image_width'], '.') !== false || strpos($this->request->post['intime_image_width'], ',') !== false){
			$this->error['image_width'] = $this->language->get('error_image_width');
		}

		return !$this->error;
	}
}
