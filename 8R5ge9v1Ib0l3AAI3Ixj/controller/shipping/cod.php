<?php
class ControllerShippingCod extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('shipping/cod');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('tool/image');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('cod', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_none'] = $this->language->get('text_none');

		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_image_height'] = $this->language->get('entry_image_height');
		$data['entry_image_width'] = $this->language->get('entry_image_width');
		$data['entry_note'] = $this->language->get('entry_note');
		$data['text_image'] = $this->language->get('text_image');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['cost'])) {
			$data['error_cost'] = $this->error['cost'];
		} else {
			$data['error_cost'] = '';
		}

		if (isset($this->error['sort_order'])) {
			$data['error_sort_order'] = $this->error['sort_order'];
		} else {
			$data['error_sort_order'] = '';
		}

		if (isset($this->error['image_height'])) {
			$data['error_image_height'] = $this->error['image_height'];
		} else {
			$data['error_image_height'] = '';
		}

		if (isset($this->error['image_width'])) {
			$data['error_image_width'] = $this->error['image_width'];
		} else {
			$data['error_image_width'] = '';
		}

		foreach ($languages as $language) {
			if (isset($this->error['note'.$language['language_id']])) {
				$data['error_note'.$language['language_id']] = $this->error['note'.$language['language_id']];
			} else {
				$data['error_note'.$language['language_id']] = '';
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('shipping/cod', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('shipping/cod', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

		foreach ($languages as $language) {
			if (isset($this->request->post['cod_note' . $language['language_id']])) {
				$data['cod_note' . $language['language_id']] = $this->request->post['cod_note' . $language['language_id']];
			} else {
				$data['cod_note' . $language['language_id']] = $this->config->get('cod_note' . $language['language_id']);
			}
		}

		$data['languages'] = $languages;

		if (isset($this->request->post['cod_cost'])) {
			$data['cod_cost'] = $this->request->post['cod_cost'];
		} else {
			$data['cod_cost'] = $this->config->get('cod_cost');
		}

		if (isset($this->request->post['cod_image'])) {
			$data['image'] = $this->request->post['cod_image'];
		} elseif (!empty($this->config->get('cod_image'))) {
			$data['image'] = $this->config->get('cod_image');
		} else {
			$data['image'] = '';
		}

		if (isset($this->request->post['cod_image']) && is_file(DIR_IMAGE . $this->request->post['cod_image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['cod_image'], 100, 100);
		} elseif (!empty($this->config->get('cod_image')) && is_file(DIR_IMAGE . $this->config->get('cod_image'))) {
			$data['thumb'] = $this->model_tool_image->resize($this->config->get('cod_image'), 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		if (isset($this->request->post['cod_image_height'])) {
			$data['cod_image_height'] = $this->request->post['cod_image_height'];
		} else {
			$data['cod_image_height'] = $this->config->get('cod_image_height');
		}

		if (isset($this->request->post['cod_image_width'])) {
			$data['cod_image_width'] = $this->request->post['cod_image_width'];
		} else {
			$data['cod_image_width'] = $this->config->get('cod_image_width');
		}

		if (isset($this->request->post['cod_total'])) {
			$data['cod_total'] = $this->request->post['cod_total'];
		} else {
			$data['cod_total'] = $this->config->get('cod_total');
		}

		if (isset($this->request->post['cod_geo_zone_id'])) {
			$data['cod_geo_zone_id'] = $this->request->post['cod_geo_zone_id'];
		} else {
			$data['cod_geo_zone_id'] = $this->config->get('cod_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['cod_status'])) {
			$data['cod_status'] = $this->request->post['cod_status'];
		} else {
			$data['cod_status'] = $this->config->get('cod_status');
		}

		if (isset($this->request->post['cod_sort_order'])) {
			$data['cod_sort_order'] = $this->request->post['cod_sort_order'];
		} else {
			$data['cod_sort_order'] = $this->config->get('cod_sort_order');
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/cod.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/cod')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (utf8_strlen($this->request->post['cod_note' . $language['language_id']]) > 1000) {
				$this->error['note' .  $language['language_id']] = $this->language->get('error_note');
			}
		}

		if( isset($this->request->post['cod_cost']) && trim($this->request->post['cod_cost']) == ''){
			$this->request->post['cod_cost'] = 0;
		}

		if(!is_numeric($this->request->post['cod_cost'])){
			$this->error['cost'] = $this->language->get('error_cost');
		}

		if( isset($this->request->post['cod_sort_order']) && trim($this->request->post['cod_sort_order']) == ''){
			$this->request->post['cod_sort_order'] = 0;
		}

		if(!is_numeric($this->request->post['cod_sort_order'])){
			$this->error['sort_order'] = $this->language->get('error_sort_order');
		}

		if(!is_numeric($this->request->post['cod_image_height']) || strpos($this->request->post['cod_image_height'], '.') !== false || strpos($this->request->post['cod_image_height'], ',') !== false){
			$this->error['image_height'] = $this->language->get('error_image_height');
		}

		if(!is_numeric($this->request->post['cod_image_width']) || strpos($this->request->post['cod_image_width'], '.') !== false || strpos($this->request->post['cod_image_width'], ',') !== false){
			$this->error['image_width'] = $this->language->get('error_image_width');
		}

		return !$this->error;
	}
}
