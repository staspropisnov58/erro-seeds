<?php
class ControllerSaleUpdateOrder extends Controller
{
    public function updateView()
    {
        $this->load->language('sale/update_order');
        $json = $this->validate();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && !$json) {
            if (isset($this->request->post['table']) && $this->request->post['table']) {
                $get_table = 'get' . ucfirst($this->request->post['table']);
                $json['html'] = $this->{$get_table}($this->request->post['order_id']);

                $json['success'] = $this->language->get('text_success_order');
            } else {
                $json['error'] = $this->language->get('error_table');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addDiscount()
    {
        $this->load->language('sale/update_order');
        $json = $this->validate();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && !$json) {
            if (isset($this->request->post['discount'])) {
                $discount = (float) str_replace(',', '.', $this->request->post['discount']);
            } else {
                $discount = 0;
            }


            if ($discount) {
                $this->load->model('sale/update_order');
                $this->model_sale_update_order->addDiscount($this->request->post['order_id'], $discount);

                $json['success'] = $this->language->get('text_success_discount');
            } else {
                $json['error'] = $this->language->get('error_discount');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'sale/update_order')) {
            $error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['order_id'])) {
            $error['error'] = $this->language->get('error_order_id');
        }

        return $error;
    }

    private function getCart($order_id)
    {
        $this->load->model('sale/order');
        $this->load->language('sale/order');

        $order_info = $this->model_sale_order->getOrder($order_id);

        if ($order_info) {
            $data['column_product'] = $this->language->get('column_product');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');

            $data['entry_order_discount'] = $this->language->get('entry_order_discount');

            $data['products'] = array();

            $products = $this->model_sale_order->getOrderProducts($order_id);

            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                        'name'  => $option['name'],
                        'value' => $option['value'],
                        'type'  => $option['type']
                    );
                    } else {
                      $this->load->model('model/tool/upload');
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $option_data[] = array(
                            'name'  => $option['name'],
                            'value' => $upload_info['name'],
                            'type'  => $option['type'],
                            'href'  => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], 'SSL')
                        );
                        }
                    }
                }

                $data['products'][] = array(
                'order_product_id' => $product['order_product_id'],
                'product_id'       => $product['product_id'],
                'name'    	 	   => $product['name'],
                'model'    		   => $product['model'],
                'option'   		   => $option_data,
                'quantity'		   => $product['quantity'],
                'price'    		   => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'total'    		   => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                'href'     		   => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], 'SSL')
            );
            }

            $data['gift_programs'] = [];

            if ($this->config->get('gift_status')) {
                $data['text_gift'] = $this->language->get('text_gift');
                $data['gift_programs'] = $this->model_sale_order->getOrderGifts($order_id);
            }


            $data['vouchers'] = array();

            $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                'description' => $voucher['description'],
                'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
                'href'        => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], 'SSL')
            );
            }

            $totals = $this->model_sale_order->getOrderTotals($order_id);

            foreach ($totals as $total) {
                $data['totals'][$total['code']] = array(
                'title' => $total['title'],
                'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
            );
            }

            return $this->load->view('sale/order_table/cart.tpl', $data);
        }

    }
}
