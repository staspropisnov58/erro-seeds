<?php
class ControllerExtensionNews extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/news', 'token=' . $this->session->data['token'] . $url, 'SSL')
   		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];

			unset($this->error['warning']);
		} else {
			$data['error'] = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		$filter_data = array(
			'page' => $page,
			'limit' => $this->config->get('config_limit_admin'),
			'start' => $this->config->get('config_limit_admin') * ($page - 1),
		);

		$total = $this->model_extension_news->getTotalNews();

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/news', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_title'] = $this->language->get('text_title');
		$data['text_short_description'] = $this->language->get('text_short_description');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_action'] = $this->language->get('text_action');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_delete'] = $this->language->get('button_delete');

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['add'] = $this->url->link('extension/news/insert', '&token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('extension/news/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['all_news'] = array();

		$all_news = $this->model_extension_news->getAllNews($filter_data);

		foreach ($all_news as $news) {
			$data['all_news'][] = array (
				'news_id' 			=> $news['news_id'],
				'title' 			=> $news['title'],
				'short_description'	=> $news['short_description'],

				'module_status' => $news['module']==0 ? 'Нет' : 'Да',
				'module' => $news['module'],
				'date_added' 		=> date($this->language->get('date_format_short'), strtotime($news['date_added'])),
				'edit' 				=> $this->url->link('extension/news/edit', 'news_id=' . $news['news_id'] . '&token=' . $this->session->data['token'] . $url, 'SSL')
			);
		}
		$data['token']=$this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/news_list.tpl', $data));
	}

	public function edit() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$news = $this->model_extension_news->getNews($this->request->get['news_id']);
			if($this->request->post['status'] != $news['status']){
				$prev_news = $this->model_extension_news->prevNews($this->request->get['news_id']);
				$this->load->model('module/check_modified');
				if($prev_news){
					$this->model_module_check_modified->editDateModifiedEditNews($prev_news);
				}
				$next_news = $this->model_extension_news->nextNews($this->request->get['news_id']);
				if($next_news){
					$this->model_module_check_modified->editDateModifiedEditNews($next_news);
				}
			}

			$this->model_extension_news->editNews($this->request->get['news_id'], $this->request->post);

			if($this->config->get('xml')!== null){
				$this->load->controller('module/rss_xml/generateXml');
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->form();
	}

	public function insert() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_extension_news->addNews($this->request->post);

			if($this->config->get('xml')!== null){
				$this->load->controller('module/rss_xml/generateXml');
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));

		}

		$this->form();

	}

	protected function form() {

		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->load->model('tool/image');

		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		if (isset($this->request->get['news_id'])) {
			$data['action'] = $this->url->link('extension/news/edit', '&news_id=' . $this->request->get['news_id'] . '&token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('extension/news/insert', '&token=' . $this->session->data['token'], 'SSL');
		}

		$data['cancel'] = $this->url->link('extension/news', '&token=' . $this->session->data['token'], 'SSL');

		$data['heading_title'] 									= $this->language->get('heading_title');

		$data['entry_sale_event']								= $this->language->get('entry_sale_event');
		$data['entry_opengraph_image'] 					= $this->language->get('entry_opengraph_image');
		$data['entry_opengraph_title'] 					= $this->language->get('entry_opengraph_title');
		$data['entry_opengraph_description'] 		= $this->language->get('entry_opengraph_description');

		$data['text_image'] 										= $this->language->get('text_image');
		$data['text_image_2'] 									= $this->language->get('text_image_2');
		$data['text_title'] 										= $this->language->get('text_title');
		$data['text_description'] 							= $this->language->get('text_description');
		$data['text_short_description'] 				= $this->language->get('text_short_description');
		$data['text_status'] 										= $this->language->get('text_status');
		$data['text_keyword'] 									= $this->language->get('text_keyword');
		$data['text_meta_robots'] 							= $this->language->get('text_meta_robots');
		$data['text_meta_description'] 					= $this->language->get('text_meta_description');
		$data['text_meta_title'] 								= $this->language->get('text_meta_title');
		$data['text_meta_keyword'] 							= $this->language->get('text_meta_keyword');
		$data['text_enabled'] 									= $this->language->get('text_enabled');
		$data['text_disabled'] 									= $this->language->get('text_disabled');
		$data['text_browse'] 										= $this->language->get('text_browse');
		$data['text_clear'] 										= $this->language->get('text_clear');
		$data['text_image_manager'] 						= $this->language->get('text_image_manager');

		$data['help_meta_robots'] 							= $this->language->get('help_meta_robots');
		$data['help_sale_event'] 								= $this->language->get('help_sale_event');

		$data['button_save'] 										= $this->language->get('button_save');
		$data['button_cancel'] 									= $this->language->get('button_cancel');

		$data['tab_general']										= $this->language->get('tab_general');
		$data['tab_open_graph']										= $this->language->get('tab_open_graph');


		$data['token'] 													= $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		} else {
			$data['error'] = '';
		}

		if(isset($this->error['opengraph'])){
			$data['error_opengraph'] = $this->error['opengraph'];
		}else{
			$data['error_opengraph'] = array();
		}

		if (isset($this->error['meta_robots'])) {
			$data['error_meta_robots'] = $this->error['meta_robots'];
		} else {
			$data['error_meta_robots'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}


		if (isset($this->request->get['news_id'])) {
			$news = $this->model_extension_news->getNews($this->request->get['news_id']);
		} else {
			$news = array();
		}

		if (isset($this->request->post['news'])) {
			$data['news'] = $this->request->post['news'];
		} elseif (!empty($news)) {
			$data['news'] = $this->model_extension_news->getNewsDescription($this->request->get['news_id']);
		} else {
			$data['news'] = '';
		}

		$data['sale_event'] = array();

		if($this->config->get('sale_events_status')!== NULL && (int)$this->config->get('sale_events_status') !== 0){

			$this->load->model('marketing/sale_events');
			$data['sale_events_status'] = $this->config->get('sale_events_status');
			if(isset($news['sale_event_id']) && (int)$news['sale_event_id'] !== 0){
				$data['sale_event'] = $this->model_marketing_sale_events->getSaleEventForAdminPanel($news['sale_event_id']);
			}
		}else{
			$data['sale_events_status'] = 0;
		}

		foreach ($data['news'] as &$news_descript) {
			foreach ($news_descript as $key => &$nd){
				if(stristr($key, 'image')){
					if(is_file(DIR_IMAGE . $nd)){
						if((strlen($key)) > 5){
							$keys = 5 - strlen($key);
							$keys = substr($key, $keys);
							$thumb_2_key = 'thumb' . $keys;
							$news_descript[$thumb_2_key] = $this->model_tool_image->resize($nd,100,100);


						}else{
							$thumb_key = 'thumb';
							$news_descript[$thumb_key] = $this->model_tool_image->resize($nd,100,100);
						}
					}else{
						if((strlen($key)) > 5){
							$keys = 5 - strlen($key);
							$keys = substr($key, $keys);
							$thumb_2_key = 'thumb' . $keys;
							$news_descript[$thumb_2_key] = $this->model_tool_image->resize('no_image.png',100,100);


						}else{
							$thumb_key = 'thumb';
							$news_descript[$thumb_key] = $this->model_tool_image->resize('no_image.png',100,100);
						}
					}
				}
			}

		}



		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($news)) {
			$data['keyword'] = $news['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['meta_robots'])) {
			$data['meta_robots'] = $this->request->post['meta_robots'];
		} elseif (!empty($news)) {
			$data['meta_robots'] = $news['meta_robots'];
		} else {
			$data['meta_robots'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($news)) {
			$data['status'] = $news['status'];
		} else {
			$data['status'] = '';
		}

		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL && $this->config->get('opengraph_status') !== NULL && (int)$this->config->get('opengraph_status') === 1){

			$data['opengraph_use_meta_when_no_og'] = $this->config->get('opengraph_use_meta_when_no_og');
			$data['opengraph_status'] = $this->config->get('opengraph_status');

			$data['opengraph'] = array();

			if(isset($this->request->post['opengraph'])){
				$opengraph = $this->request->post['opengraph'];
			}elseif(isset($this->request->get['news_id'])){
				$opengraph = $this->model_extension_news->getOpengraph($this->request->get['news_id']);
			}else{
				$opengraph = array();
			}

			foreach($opengraph as $key => $value){
				if($value['image']!== ''){
					$opengraph[$key]['thumb'] = $this->model_tool_image->resize($value['image'],100,100);
				}else{
					$opengraph[$key]['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
				}
			}
			$data['opengraph'] = $opengraph;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/news_form.tpl', $data));
	}

	public function delete() {
		$this->language->load('extension/news');

		$this->load->model('extension/news');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $news_id) {
				$this->model_extension_news->deleteNews($news_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->response->redirect($this->url->link('extension/news', 'token=' . $this->session->data['token'], 'SSL'));
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'extension/news')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/news')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->config->get('opengraph_use_meta_when_no_og')!== NULL  && $this->config->get('opengraph_status')!== NULL && (int)$this->config->get('opengraph_status') === 1){
	    foreach($this->request->post['opengraph'] as $language_id => $value){
        if((int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['title'])) < 1 ||(int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['title'])) > 100){
          $this->error['opengraph'][$language_id]['title'] = $this->language->get('error_lenght_title');
        }elseif((int)$this->config->get('opengraph_use_meta_when_no_og') === 1 && utf8_strlen(trim($value['title'])) > 100){
          $this->error['opengraph'][$language_id]['title'] = $this->language->get('error_empty_title_2');
        }

        if((int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['description'])) < 1 ||(int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['description'])) > 300){
          $this->error['opengraph'][$language_id]['description'] = $this->language->get('error_empty_description');
        }elseif((int)$this->config->get('opengraph_use_meta_when_no_og') === 1 && utf8_strlen(trim($value['description'])) > 300){
          $this->error['opengraph'][$language_id]['description'] = $this->language->get('error_empty_description_2');
        }

        if((int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['image'])) < 1){
          $this->error['opengraph'][$language_id]['image'] = $this->language->get('error_empty_image');
        }
	    }
		}


		if (utf8_strlen($this->request->post['keyword']) > 0) {
			$this->load->model('catalog/url_alias');

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

			if ($url_alias_info && isset($this->request->get['news_id']) && $url_alias_info['query'] != 'news_id=' . $this->request->get['news_id']) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}

			if ($url_alias_info && !isset($this->request->get['news_id'])) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}
		}

		if(utf8_strlen($this->request->post['meta_robots']) > 150){
			$this->error['meta_robots'] = sprintf($this->language->get('error_meta_robots'));
		}

		if(utf8_strlen($this->request->post['keyword']) == ''){
			$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}

	}

	public function setModule() {
		$this->load->model('extension/news');
		$id = $this->request->get['news_id'];
		$value = $this->request->get['value'];
		if($value=='1'){
			$this->model_extension_news->setModule($id,0);
			echo 'Нет';
		} else {
			$this->model_extension_news->setModule($id,1);
			echo 'Да';
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_title'])) {
			$this->load->model('extension/news');

			$filter_data = array(
				'filter_title' => $this->request->get['filter_title'],
				'sort'        => 'title',
				'order'       => 'ASC',
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_extension_news->getAllNews($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'article_id' => $result['news_id'],
					'title'        => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['title'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
