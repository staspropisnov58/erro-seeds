<?php
class ControllerToolTasks extends Controller {

    private $error = array();

    public function index()
    {
        $this->load->language('tool/tasks');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('tool/tasks');

        $this->getList();

    }

    public function getList()
    {

        $this->load->model('tool/tasks');
        $this->load->language('tool/tasks');

        if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
          $href = 'Once';
        }else{
          $href = 'Regular';
        }

        $data['show_button_run'] = false;

        if ($this->user->hasPermission('modify', 'tool/task_runner')) {
          $data['show_button_run'] = true;
        }
        $this->document->setTitle($this->language->get('heading_title_' . mb_strtolower($href)));

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'datetime_run';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
          $page = $this->request->get['page'];
        } else {
          $page = 1;
        }

        $url = '';

        if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
          $url .= '&frequency=once';
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_' . mb_strtolower($href)),
            'href' => $this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('tool/tasks/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('tool/tasks/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['tasks'] = array();

        $filter_data = array(
            'sort'            => $sort,
            'order'           => $order,
            'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit'           => $this->config->get('config_limit_admin'),
            'frequency'       => mb_strtolower($href),
        );

        $data['entry_datetime_run'] = $this->language->get('entry_datetime_run');
        $data['entry_action'] = $this->language->get('entry_action');
        $data['entry_args'] = $this->language->get('entry_args');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_periodicity'] = $this->language->get('column_periodicity');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_list'] = $this->language->get('text_list');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_run'] = $this->language->get('button_run');

        $event_log_url = $this->url->link('tool/cli_event_log', 'token=' . $this->session->data['token'], 'SSL');

        $data['text_success_notice'] = sprintf($this->language->get('text_success_notice'), $event_log_url);


        $data['text_error_notice'] = $this->language->get('text_error_notice');

        $tasks_total = $this->model_tool_tasks->getTotalTasks($filter_data);
        $results = $this->model_tool_tasks->getTasks($filter_data);


        foreach ($results as $result) {
          $data['tasks'][] = array(
            'task_id' 		    => $result['task_id'],
            'datetime_run'    => date($this->language->get('date_format_short'), strtotime($result['datetime_run'])),
            'action'       		=> $result['action'],
            'args'      		  => implode(",", unserialize($result['args'])),
            'periodicity'     => $this->language->get('text_' . $result['frequency']),
            'edit'       		  => $this->url->link('tool/tasks/edit', 'token=' . $this->session->data['token'] . '&task_id=' . $result['task_id'] . $url, 'SSL'),
            'run'             => $this->url->link('tool/task_runner/run', 'token=' . $this->session->data['token'] . '&task_id=' . $result['task_id'] . $url, 'SSL'),
          );
        }

        $data['heading_title'] = $this->language->get('heading_title' . mb_strtolower('_'.$href));

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
          $url .= '&frequency=once';
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_action'] = $this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . '&sort=action' . $url, 'SSL');
        $data['sort_datetime_run'] = $this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . '&sort=datetime_run' . $url, 'SSL');
        $data['sort_frequency'] = $this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . '&sort=frequency' . $url, 'SSL');

        $url = '';

        if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
          $url .= '&frequency=once';
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $tasks_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($tasks_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($tasks_total - $this->config->get('config_limit_admin'))) ? $tasks_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $tasks_total, ceil($tasks_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('tool/tasks.tpl', $data));
    }

    public function add()
    {
        $this->load->language('tool/tasks');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('tool/tasks');


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_tool_tasks->addTask($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
              $url .= '&frequency=once';
            }

            if (isset($this->request->get['sort'])) {
              $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
              $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
              $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . $url, 'SSL'));

        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('tool/tasks');

        $this->load->model('tool/tasks');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_tool_tasks->editTask($this->request->post, $this->request->get['task_id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
              $url .= '&frequency=once';
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . $url, 'SSL'));

        }

        $this->getForm();
    }

    protected function getForm()
    {

      $this->load->language('tool/tasks');

      if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
        $heading_title = $this->language->get('heading_title_once');
      }else{
        $heading_title = $this->language->get('heading_title_regular');
      }

        $data['heading_title'] = $heading_title;

        $this->document->setTitle($heading_title);


        $data['entry_action'] = $this->language->get('entry_action');
        $data['entry_datetime_run'] = $this->language->get('entry_datetime_run');
        $data['entry_args'] = $this->language->get('entry_args');
        $data['entry_frequency'] = $this->language->get('entry_frequency');
        $data['text_select_option'] = $this->language->get('text_select_option');
        $data['text_form'] = $this->language->get('text_form');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');


        if (isset($this->error['warning'])) {
          $data['error_warning'] = $this->error['warning'];
        } else {
          $data['error_warning'] = '';
        }

        $url = '';

        if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
          $url .= '&frequency=once';
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $heading_title,
            'href' => $this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (isset($this->error['action'])) {
            $data['error_action'] = $this->error['action'];
        } else {
            $data['error_action'] = '';
        }

        if (isset($this->error['datetime_run'])) {
            $data['error_datetime_run'] = $this->error['datetime_run'];
        } else {
            $data['error_datetime_run'] = '';
        }

        if (isset($this->error['frequency'])) {
            $data['error_frequency'] = $this->error['frequency'];
        } else {
            $data['error_frequency'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (!isset($this->request->get['task_id'])) {
            $data['action'] = $this->url->link('tool/tasks/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('tool/tasks/edit', 'token=' . $this->session->data['token'] . '&task_id=' . $this->request->get['task_id'] . $url, 'SSL');
            $data['task_id'] = $this->request->get['task_id'];
        }

        $data['cancel'] = $this->url->link('catalog/tasks', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $task_info = [];

        if (isset($this->request->get['task_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $task_info = $this->model_tool_tasks->getTask($this->request->get['task_id']);
        }

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['datetime_run'])) {
            $data['datetime_run'] = $this->request->post['datetime_run'];
        } elseif (!empty($task_info)) {
            $data['datetime_run'] = $task_info['datetime_run'];
        } else {
            $data['datetime_run'] = '';
        }

        if(isset($this->request->post['args'])){
          $data['args'] = $this->request->post['args'];
        }elseif(!empty($task_info)){
          $data['args'] = implode(",", unserialize($task_info['args']));
        }else{
          $data['args'] = '';
        }

        if(isset($this->request->post['controller_action'])){
          $data['controller_action'] = $this->request->post['controller_action'];
        }elseif(!empty($task_info)){
          $data['controller_action'] = $task_info['action'];
        }else{
          $data['controller_action'] = '';
        }

        if (isset($this->request->post['frequency'])) {
            $data['frequency'] = $this->request->post['frequency'];
        } elseif (!empty($task_info)) {
            $data['frequency'] = $task_info['frequency'];
        } else {
            $data['frequency'] = '';
        }

        $data['periodicity'] = array(
          'once'    =>  $this->language->get('text_once'),
          'year'    =>  $this->language->get('text_year'),
          'month'   =>  $this->language->get('text_month'),
          'week'    =>  $this->language->get('text_week'),
          'day'     =>  $this->language->get('text_day'),
          'hour'    =>  $this->language->get('text_hour'),
          'minute'  =>  $this->language->get('text_minute'),
        );


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('tool/task_form.tpl', $data));
    }

    public function delete(){
      $this->load->language('tool/tasks');

      $this->document->setTitle($this->language->get('heading_title'));

      $this->load->model('tool/tasks');
      if (isset($this->request->post['selected']) && $this->validateDelete()) {
          foreach ($this->request->post['selected'] as $task_id) {
              $this->model_tool_tasks->deleteTask($task_id);
          }

          $this->session->data['success'] = $this->language->get('text_success');

          $url = '';

          if(isset($this->request->get['frequency']) && $this->request->get['frequency'] === 'once'){
            $url .= '&frequency=once';
          }

          if (isset($this->request->get['sort'])) {
              $url .= '&sort=' . $this->request->get['sort'];
          }

          if (isset($this->request->get['order'])) {
              $url .= '&order=' . $this->request->get['order'];
          }

          if (isset($this->request->get['page'])) {
              $url .= '&page=' . $this->request->get['page'];
          }

          $this->response->redirect($this->url->link('tool/tasks', 'token=' . $this->session->data['token'] . $url, 'SSL'));
      }

      $this->getList();
    }

    public function validateDelete(){
      return !$this->error;
    }

    public function validateForm(){
      if(trim($this->request->post['controller_action']) === ''){
        $this->error['action'] = $this->language->get('error_empty_action');
      }else{

        $file_array = explode('/', $this->request->post['controller_action']);

        if(count($file_array) > 3 && count($file_array) <= 0){
          $this->error['action'] = $this->language->get('error_format_action');
        }else{

          if(!is_file(DIR_CLI_APPLICATION . 'controller/' . implode('/', array_slice($file_array, 0,2)). '.php')){
            $this->error['action'] = $this->language->get('error_not_file_action');
          }else{


            include_once(DIR_CLI_APPLICATION . 'controller/' . implode('/', array_slice($file_array, 0,2)). '.php');

            $class_name_string = 'Controller';

            foreach (array_slice($file_array, 0,2) as $key => $value) {
              if(strripos($value, '_')){
                $values = explode("_", $value);
                $value = '';
                foreach ($values as $value1) {
                  $value .= ucfirst($value1);
                }
              }

              $class_name_string .= ucfirst($value);
            }

            $controller = new $class_name_string($this->registry);


    				if (is_callable(array($controller, array_slice($file_array, 2)[0]))) {
      			} else {
              $this->error['action'] = sprintf($this->language->get('error_method_not_found'), array_slice($file_array, 2)[0], 'controller/' . implode('/', array_slice($file_array, 0,2)). '.php');
      			}
          }
        }
      }

      if(trim($this->request->post['datetime_run']) === ''){
        $this->error['datetime_run'] = $this->language->get('error_datetime_run');
      }

      if(!isset($this->request->post['frequency']) || trim($this->request->post['frequency']) === ''){
        $this->error['warning'] = $this->language->get('error_frequency');
      }

      return !$this->error;
    }
}
