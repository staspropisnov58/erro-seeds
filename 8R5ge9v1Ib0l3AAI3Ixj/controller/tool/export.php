<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ControllerToolExport extends Controller
{

  public function index(){
    $this->load->language('tool/export');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('tool/export');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->user->hasPermission('modify', 'tool/export')) {

				$this->session->data['success'] = $this->language->get('text_success');
				$this->response->redirect($this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$data['entry_restore'] = $this->language->get('entry_restore');
		$data['entry_export'] = $this->language->get('entry_export');

		$data['button_export'] = $this->language->get('button_export');
		$data['button_restore'] = $this->language->get('button_restore');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL')
		);

		// $data['restore'] = $this->url->link('tool/export', 'token=' . $this->session->data['token'], 'SSL');


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('tool/export.tpl', $data));
  }

  public function downloadSample()
  {
    $spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'It works!');
		$sheet->setCellValue('A2', 'It works!');
		$sheet->setCellValue('B2', 'Blah blah!');

		$writer = new Xlsx($spreadsheet);
		$writer->save('test.xlsx');

		$file = 'test.xlsx';

    if (file_exists($file)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="'.basename($file).'"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file));
      readfile($file);
      exit;
    }
  }
}
