<?php
class ControllerToolTaskRunner extends Controller {

  public function run(){

    if(isset($this->request->get['task_id']) && $this->request->get['task_id'] != '' && $this->user->hasPermission('modify', 'tool/task_runner')){


      $this->load->model('tool/tasks');

      $task = $this->model_tool_tasks->getTask($this->request->get['task_id']);

      if (isset($this->request->get['store_id'])) {
          $store_id = $this->request->get['store_id'];
      } else {
          $store_id = 0;
      }

      $this->load->model('setting/store');

      $store_info = $this->model_setting_store->getStore($store_id);

      if ($store_info) {
          $url = $store_info['ssl'];
      } else {
          $url = HTTPS_CLI;
      }

      // Include any URL perameters
      $url_data = array();

      foreach ($this->request->get as $key => $value) {
        if ($key != 'route' && $key != 'token' && $key != 'store_id') {
            $url_data[$key] = $value;
        }
      }

      if($this->request->get['task_id'] != NULL ){
        $this->request->post['task_id'] = $this->request->get['task_id'];
      }

      $this->load->model('tool/curl');

      $json = $this->model_tool_curl->getCurlGetCli($this->request->post, $url_data, $url, $task['action']);

      $this->response->setOutput(json_encode($json));



  }

}
}
