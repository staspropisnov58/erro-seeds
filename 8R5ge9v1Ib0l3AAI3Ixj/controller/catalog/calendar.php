<?php
class ControllerCatalogCalendar extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('catalog/calendar');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/calendar');

        $this->getList();
    }

    public function add() {
        $this->load->language('catalog/calendar');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/calendar');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {



            $this->model_catalog_calendar->addcalendar($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('catalog/calendar');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/calendar');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_calendar->editcalendar($this->request->get['calendar_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/calendar');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/calendar');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $calendar_id) {
                $this->model_catalog_calendar->deletecalendar($calendar_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'id.title';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('catalog/calendar/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('catalog/calendar/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['calendars'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $calendar_total = $this->model_catalog_calendar->getTotalcalendars();

        $results = $this->model_catalog_calendar->getcalendars($filter_data);
        $this->load->model('tool/image');

        foreach ($results as $result) {

            if(!empty($result['image']) && is_file(DIR_IMAGE . $result['image'])){
               $image = $this->model_tool_image->resize($result['image'], 100, 100);
                }else{
                $image=$this->model_tool_image->resize('no_image.png', 100, 100);
            }

            $data['calendars'][] = array(
                'calendar_id' => $result['calendar_id'],
                'title'          => $result['title'],
                'date_available'          => $result['date_available'],
                'date_available_end'          => $result['date_available_end'],
                'url'          => $result['url'],
                'image'          => $image,
                'sort_order'     => $result['sort_order'],
                'edit'           => $this->url->link('catalog/calendar/edit', 'token=' . $this->session->data['token'] . '&calendar_id=' . $result['calendar_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_title'] = $this->language->get('column_title');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_title'] = $this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . '&sort=id.title' . $url, 'SSL');
        $data['sort_sort_order'] = $this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $calendar_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($calendar_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($calendar_total - $this->config->get('config_limit_admin'))) ? $calendar_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $calendar_total, ceil($calendar_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/calendar_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] 									= $this->language->get('heading_title');

        $data['text_form'] 											= !isset($this->request->get['calendar_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_default'] 									= $this->language->get('text_default');
        $data['text_enabled'] 									= $this->language->get('text_enabled');
        $data['text_disabled'] 									= $this->language->get('text_disabled');

        $data['entry_title'] 										= $this->language->get('entry_title');
        $data['entry_description'] 							= $this->language->get('entry_description');
        $data['entry_microdata'] 								= $this->language->get('entry_microdata');
        $data['entry_meta_title'] 							= $this->language->get('entry_meta_title');
        $data['entry_meta_description'] 				= $this->language->get('entry_meta_description');
        $data['entry_meta_keyword'] 						= $this->language->get('entry_meta_keyword');
        $data['entry_keyword'] 									= $this->language->get('entry_keyword');
        $data['entry_meta_robots'] 							= $this->language->get('entry_meta_robots');
        $data['entry_store'] 										= $this->language->get('entry_store');
        $data['entry_bottom'] 									= $this->language->get('entry_bottom');
        $data['entry_sort_order'] 							= $this->language->get('entry_sort_order');
        $data['entry_status'] 									= $this->language->get('entry_status');
        $data['entry_layout'] 									= $this->language->get('entry_layout');
        $data['entry_opengraph_title'] 					= $this->language->get('entry_opengraph_title');
        $data['entry_opengraph_description'] 		= $this->language->get('entry_opengraph_description');
        $data['entry_opengraph_image'] 					= $this->language->get('entry_opengraph_image');

        $data['help_keyword'] 									= $this->language->get('help_keyword');
        $data['help_bottom'] 										= $this->language->get('help_bottom');
        $data['help_meta_robots'] 							= $this->language->get('help_meta_robots');

        $data['button_save'] 										= $this->language->get('button_save');
        $data['button_cancel'] 									= $this->language->get('button_cancel');

        $data['tab_general'] 										= $this->language->get('tab_general');
        $data['tab_data'] 											= $this->language->get('tab_data');
        $data['tab_design'] 										= $this->language->get('tab_design');
        $data['tab_open_graph'] 								= $this->language->get('tab_open_graph');
        $data['entry_date_available'] 								= $this->language->get('entry_date_available');
        $data['entry_url'] 								= $this->language->get('entry_url');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['title'])) {
            $data['error_title'] = $this->error['title'];
        } else {
            $data['error_title'] = array();
        }

        if (isset($this->error['description'])) {
            $data['error_description'] = $this->error['description'];
        } else {
            $data['error_description'] = array();
        }

        if (isset($this->error['image'])) {
            $data['error_image'] = $this->error['image'];
        } else {
            $data['error_image'] = '';
        }

        if(isset($this->error['date_available'])) {
            $data['error_date_available'] = $this->error['date_available'];
        } else {
            $data['error_date_available'] = '';
        }


        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['calendar_id'])) {
            $data['action'] = $this->url->link('catalog/calendar/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('catalog/calendar/edit', 'token=' . $this->session->data['token'] . '&calendar_id=' . $this->request->get['calendar_id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('catalog/calendar', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['calendar_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $calendar_info = $this->model_catalog_calendar->getcalendar($this->request->get['calendar_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['calendar_description'])) {
            $data['calendar_description'] = $this->request->post['calendar_description'];
        } elseif (isset($this->request->get['calendar_id'])) {
            $data['calendar_description'] = $this->model_catalog_calendar->getcalendarDescriptions($this->request->get['calendar_id']);
        } else {
            $data['calendar_description'] = array();
        }


        foreach ($data['calendar_description'] as $key => $thumb_img_lan){
            if($thumb_img_lan && is_file(DIR_IMAGE . $thumb_img_lan['img_lan'])){
                $data['thumb_img_lan'][$key] = $this->model_tool_image->resize( $thumb_img_lan['img_lan'], 100, 100);
            }else{
                $data['thumb_img_lan'][$key] = $this->model_tool_image->resize('no_image.png', 100, 100);
            }
        }


        if (isset($this->request->post['date_available'])) {
            $data['date_available'] = $this->request->post['date_available'];
        } elseif (!empty($calendar_info)) {
            $data['date_available'] = $calendar_info['date_available'];
        } else {
            $data['date_available'] = '';
        }
        if (isset($this->request->post['date_available_end'])) {
            $data['date_available_end'] = $this->request->post['date_available_end'];
        } elseif (!empty($calendar_info)) {
            $data['date_available_end'] = $calendar_info['date_available_end'];
        } else {
            $data['date_available_end'] = '';
        }


        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($calendar_info)) {
            $data['status'] = $calendar_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($calendar_info)) {
            $data['sort_order'] = $calendar_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }



        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($calendar_info) && is_file(DIR_IMAGE . $calendar_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($calendar_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($calendar_info)) {
            $data['image'] = $calendar_info['image'];
        } else {
            $data['image'] = '';
        }
        if (isset($this->request->post['url'])) {
            $data['url'] = $this->request->post['url'];
        } elseif (!empty($calendar_info)) {
            $data['url'] = $calendar_info['url'];
        } else {
            $data['url'] = '';
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);




        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/calendar_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/calendar')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['date_available']) < 3) || (utf8_strlen($this->request->post['date_available']) > 64)) {
            $this->error['date_available'] = $this->language->get('error_date_available');
        }
//        if ((utf8_strlen($this->request->post['image']) < 3) || (utf8_strlen($this->request->post['image']) > 64)) {
//            $this->error['image'] = $this->language->get('error_empty_image');
//        }
        foreach ($this->request->post['calendar_description'] as $language_id => $value) {

            if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
                $this->error['title'][$language_id] = $this->language->get('error_title');
            }




//            if($value['microdata']!='' && is_null(json_decode(html_entity_decode($value['microdata'])))){
//                $this->error['microdata'][$language_id] = $this->language->get('error_microdata');
//            }
//            if (utf8_strlen($value['description']) < 3) {
//                $this->error['description'][$language_id] = $this->language->get('error_description');
//            }

//            if ((utf8_strlen($value['meta_title']) < 3) || (utf8_strlen($value['meta_title']) > 255)) {
//                $this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
//            }
        }
//        if (utf8_strlen($this->request->post['keyword']) > 0) {
//            $this->load->model('catalog/url_alias');
//
//            $url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);
//
//            if ($url_alias_info && isset($this->request->get['calendar_id']) && $url_alias_info['query'] != 'calendar_id=' . $this->request->get['calendar_id']) {
//                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
//            }
//
//            if ($url_alias_info && !isset($this->request->get['calendar_id'])) {
//                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
//            }
//        }

//        if(utf8_strlen($this->request->post['meta_robots']) > 150){
//            $this->error['meta_robots'] = sprintf($this->language->get('error_meta_robots'));
//        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/calendar')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('setting/store');

        foreach ($this->request->post['selected'] as $calendar_id) {
//            if ($this->config->get('config_account_id') == $calendar_id) {
//                $this->error['warning'] = $this->language->get('error_account');
//            }

            if ($this->config->get('config_checkout_id') == $calendar_id) {
                $this->error['warning'] = $this->language->get('error_checkout');
            }

            if ($this->config->get('config_affiliate_id') == $calendar_id) {
                $this->error['warning'] = $this->language->get('error_affiliate');
            }

            if ($this->config->get('config_return_id') == $calendar_id) {
                $this->error['warning'] = $this->language->get('error_return');
            }

//            $store_total = $this->model_setting_store->getTotalStoresBycalendarId($calendar_id);

//            if ($store_total) {
//                $this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
//            }
        }

        return !$this->error;
    }
}
