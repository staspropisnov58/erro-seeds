<?php
class ControllerCatalogReview extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/review');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/review');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/review');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/review');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_review->addReview($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_author'])) {
				$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/review', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function edit() {


		$this->load->language('catalog/review');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/review');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$data_review = $this->model_catalog_review->getReview($this->request->get['review_id']);


			$this->model_catalog_review->editReview($this->request->get['review_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_author'])) {
				$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->load->model('sale/customer');

			if ($data_review['customer_id'] !=0){
				$sms_notifications = $this->model_sale_customer->getSmsNotifications($data_review['customer_id']);
				$email_notifications = $this->model_sale_customer->getEmailNotifications($data_review['customer_id']);
				$customer = $this->model_sale_customer->getCustomer($data_review['customer_id']);
			}else{
				$sms_notifications = 0;
				$email_notifications = 0;
			}

			if (($this->request->post['status'] != $data_review['status']) && $this->request->post['status'] == 1 && $data_review['customer_id'] == 0 || ($this->request->post['status'] != $data_review['status']) && $this->request->post['status'] == 1 && $email_notifications['review_moderation'] == 1){
				$language_directory = $this->model_sale_customer->getCustomerDirectory($data_review['customer_id']);
				$language = new Language($language_directory);
				$this->load->language('mail/customer');

				$href = explode("/", $this->url->link('product/product', '&product_id=' . $data_review['product_id']));
				unset($href[3]);
				$href = implode("/", $href);

				$subject = $this->language->get('theme_review_moderated');
				$data['store_url'] = $this->config->get('config_url');
				$data['text_greeting'] = sprintf($this->language->get('text_hello'), $data_review['author']) . "\n\n";
				$data['text_main'][] = sprintf($this->language->get('text_review_moderated'), $data_review['product']) . "\n\n";
				$data['text_main'][] = sprintf($this->language->get('text_review_moderated_x1'), $href) . "\n\n";
				$data['text_note'] = sprintf($this->language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

				$html = $this->load->view('mail/message.tpl', $data);

				if ($data_review['email'] !== ''){
					$mail_config = $this->config->get('config_mail');
					$mail = new Mail($mail_config);
					$mail->setTo($data_review['email']);
					if ($mail_config['smtp_username']) {
						$mail->setFrom($mail_config['smtp_username']);
						$mail->setReplyTo($this->config->get('config_email'));
					} else {
						$mail->setFrom($this->config->get('config_email'));
					}
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($subject);
					$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
					$mail->send();
				}
				unset($data['text_main']);
			}

			if(((int)$this->request->post['status'] !== (int)$data_review['status']) && (int)$this->request->post['status'] === 1 && (int)$sms_notifications['review_moderation'] === 1){

			if (!empty($customer['telephone'])){
				$language_directory = $this->model_sale_customer->getCustomerDirectory($data_review['customer_id']);
				$language = new Language($language_directory);
				$this->load->language('sms/affiliate');
				if(!empty($customer['telephone_country_id'])){
					$this->load->model('localisation/country');
					$tel_code = $this->model_localisation_country->getCountryTelephoneCode($customer['telephone_country_id']);
					$telephone = $tel_code . $customer['telephone'];
				}else{
					$telephone = $customer['telephone'];
				}
				$config_sms = $this->config->get('config_sms');
				$sms_message = sprintf($this->language->get('text_reviews'), $data_review['product']);

				if($this->config->get('config_sms')){
					$sms_configurations = $this->config->get($config_sms);

					if($sms_configurations['sms_login']){
						$sms_login = $sms_configurations['sms_login'];
					}else{
						$sms_login = '';
					}

					if($sms_configurations['sms_password']){
						$sms_password = $sms_configurations['sms_password'];
					}else{
						$sms_password = '';
					}

					if($sms_configurations['sms_from']){
						$sms_from = $sms_configurations['sms_from'];
					}else{
						$sms_from = '';
					}

					if($sms_configurations['sms_request']){
						$sms_request = $sms_configurations['sms_request'];
					}else{
						$sms_request = '';
					}

					$sms_array = array(
						'to' 				=> $telephone,
						'message' 	=> html_entity_decode($sms_message, ENT_QUOTES, 'UTF-8'),
						'login'			=> $sms_login,
						'from'			=> $sms_from,
						'password'  => $sms_password,
						'request'		=> $sms_request,
					);

					$sms = new SMS($this->config->get('config_sms'), $sms_array);
					$sms->send();
				}
			}
		}

				if(!empty($data_review['rew_id'])){
					$data_rew = $this->model_catalog_review->getReview($data_review['rew_id']);
					if ($data_rew['customer_id'] !=0){
						$rew_sms_notifications = $this->model_sale_customer->getSmsNotifications($data_rew['customer_id']);
						$rew_email_notifications = $this->model_sale_customer->getEmailNotifications($data_rew['customer_id']);
						$rew_customer = $this->model_sale_customer->getCustomer($data_rew['customer_id']);
					}else{
						$rew_sms_nitifications = 0;
						$rew_email_notifications = 0;
					}

					if (($this->request->post['status'] != $data_review['status']) && $this->request->post['status'] == 1 && $data_rew['customer_id'] == 0 || ($this->request->post['status'] != $data_review['status']) && $this->request->post['status'] == 1 && $rew_email_notifications['new_review_reply'] == 1){

						$subject = $this->language->get('theme_answer_comment');
						$data['store_url'] = $this->config->get('config_url');
						$data['text_greeting'] = sprintf($this->language->get('text_hello'), $data_rew['author']) . "\n\n";
						$data['text_main'][] = sprintf($this->language->get('text_answear_comment'), $data_review['author']) . "\n\n";
						$data['text_main'][] = sprintf($this->language->get('text_review_moderated_x1'), $href) . "\n\n";
						$data['text_note'] = sprintf($this->language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

						$html = $this->load->view('mail/message.tpl', $data);

						if ($data_review['email'] !== ''){
							$mail_config = $this->config->get('config_mail');
							$mail = new Mail($mail_config);
							$mail->setTo($data_rew['email']);
							if ($mail_config['smtp_username']) {
								$mail->setFrom($mail_config['smtp_username']);
								$mail->setReplyTo($this->config->get('config_email'));
							} else {
								$mail->setFrom($this->config->get('config_email'));
							}
							$mail->setSender($this->config->get('config_name'));
							$mail->setSubject($subject);
							$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
							$mail->send();
						}
					}


					$this->load->language('sms/affiliate');

					if(((int)$this->request->post['status'] !== (int)$data_review['status']) && (int)$this->request->post['status'] === 1 && (int)$rew_sms_notifications['new_review_reply'] === 1){
					if (!empty($rew_customer['telephone'])){
						$this->load->language('sms/affiliate');
						if(!empty($customer['telephone_country_id'])){
							$this->load->model('localisation/country');
							$tel_code = $this->model_localisation_country->getCountryTelephoneCode($customer['telephone_country_id']);
							$telephone = $tel_code . $customer['telephone'];
						}else{
							$telephone = $customer['telephone'];
						}
						$config_sms = $this->config->get('config_sms');
						$sms_message = sprintf($this->language->get('text_answer_reviews'), $data_review['product']);

						if($this->config->get('config_sms')){
							$sms_configurations = $this->config->get($config_sms);

							if($sms_configurations['sms_login']){
								$sms_login = $sms_configurations['sms_login'];
							}else{
								$sms_login = '';
							}

							if($sms_configurations['sms_password']){
								$sms_password = $sms_configurations['sms_password'];
							}else{
								$sms_password = '';
							}

							if($sms_configurations['sms_from']){
								$sms_from = $sms_configurations['sms_from'];
							}else{
								$sms_from = '';
							}

							if($sms_configurations['sms_request']){
								$sms_request = $sms_configurations['sms_request'];
							}else{
								$sms_request = '';
							}

							$sms_array = array(
								'to' 				=> $telephone,
								'message' 	=> html_entity_decode($sms_message, ENT_QUOTES, 'UTF-8'),
								'login'			=> $sms_login,
								'from'			=> $sms_from,
								'password'  => $sms_password,
								'request'		=> $sms_request,
							);

							$sms = new SMS($this->config->get('config_sms'), $sms_array);
							$sms->send();
						}
					}
				}
			}

			if($this->config->get('check_modified')){
				if($this->request->post['status'] == 1){
					$this->load->model('model/check_modified');
					$this->model_check_modified->UpdateDateMofifiedProductComment($this->request->get['review_id']);
				}
			}
			$this->response->redirect($this->url->link('catalog/review', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/review');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/review');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $review_id) {
				$this->model_catalog_review->deleteReview($review_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_author'])) {
				$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/review', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_product'])) {
			$filter_product = $this->request->get['filter_product'];
		} else {
			$filter_product = null;
		}

		if (isset($this->request->get['filter_author'])) {
			$filter_author = $this->request->get['filter_author'];
		} else {
			$filter_author = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'r.date_added';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('catalog/review/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('catalog/review/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['reviews'] = array();

		$filter_data = array(
			'filter_product'    => $filter_product,
			'filter_author'     => $filter_author,
			'filter_status'     => $filter_status,
			'filter_date_added' => $filter_date_added,
			'sort'              => $sort,
			'order'             => $order,
			'start'             => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'             => $this->config->get('config_limit_admin')
		);

		$review_total = $this->model_catalog_review->getTotalReviews($filter_data);

		$results = $this->model_catalog_review->getReviews($filter_data);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'review_id'  => $result['review_id'],
				'name'       => $result['name'],
				'author'     => $result['author'],
				'rating'     => $result['rating'],
				'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'edit'       => $this->url->link('catalog/review/edit', 'token=' . $this->session->data['token'] . '&review_id=' . $result['review_id'] . $url, 'SSL')
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_author'] = $this->language->get('column_author');
		$data['column_rating'] = $this->language->get('column_rating');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_author'] = $this->language->get('entry_author');
		$data['entry_rating'] = $this->language->get('entry_rating');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_date_added'] = $this->language->get('entry_date_added');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_product'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$data['sort_author'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&sort=r.author' . $url, 'SSL');
		$data['sort_rating'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&sort=r.rating' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&sort=r.status' . $url, 'SSL');
		$data['sort_date_added'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&sort=r.date_added' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($review_total - $this->config->get('config_limit_admin'))) ? $review_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $review_total, ceil($review_total / $this->config->get('config_limit_admin')));

		$data['filter_product'] = $filter_product;
		$data['filter_author'] = $filter_author;
		$data['filter_status'] = $filter_status;
		$data['filter_date_added'] = $filter_date_added;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/review_list.tpl', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['review_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_author'] = $this->language->get('entry_author');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_rating'] = $this->language->get('entry_rating');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_text'] = $this->language->get('entry_text');

		$data['help_product'] = $this->language->get('help_product');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['product'])) {
			$data['error_product'] = $this->error['product'];
		} else {
			$data['error_product'] = '';
		}

		if (isset($this->error['author'])) {
			$data['error_author'] = $this->error['author'];
		} else {
			$data['error_author'] = '';
		}

		if (isset($this->error['text'])) {
			$data['error_text'] = $this->error['text'];
		} else {
			$data['error_text'] = '';
		}

		if (isset($this->error['rating'])) {
			$data['error_rating'] = $this->error['rating'];
		} else {
			$data['error_rating'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (!isset($this->request->get['review_id'])) {
			$data['action'] = $this->url->link('catalog/review/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('catalog/review/edit', 'token=' . $this->session->data['token'] . '&review_id=' . $this->request->get['review_id'] . $url, 'SSL');
		}



		$data['cancel'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['review_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$review_info = $this->model_catalog_review->getReview($this->request->get['review_id']);
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$data['product_id'] = $this->request->post['product_id'];
		} elseif (!empty($review_info)) {
			$data['product_id'] = $review_info['product_id'];
		} else {
			$data['product_id'] = '';
		}

		if (isset($this->request->post['product'])) {
			$data['product'] = $this->request->post['product'];
		} elseif (!empty($review_info)) {
			$data['product'] = $review_info['product'];
		} else {
			$data['product'] = '';
		}

		if (isset($this->request->post['author'])) {
			$data['author'] = $this->request->post['author'];
		} elseif (!empty($review_info)) {
			$data['author'] = $review_info['author'];
		} else {
			$data['author'] = '';
		}

		if (isset($this->request->post['text'])) {
			$data['text'] = $this->request->post['text'];
		} elseif (!empty($review_info)) {
			$data['text'] = $review_info['text'];
		} else {
			$data['text'] = '';
		}

		if (isset($this->request->post['rating'])) {
			$data['rating'] = $this->request->post['rating'];
		} elseif (!empty($review_info)) {
			$data['rating'] = $review_info['rating'];
		} else {
			$data['rating'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($review_info)) {
			$data['status'] = $review_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['email'] = $review_info['email'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/review_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/review')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['product_id']) {
			$this->error['product'] = $this->language->get('error_product');
		}

		if ((utf8_strlen($this->request->post['author']) < 3) || (utf8_strlen($this->request->post['author']) > 64)) {
			$this->error['author'] = $this->language->get('error_author');
		}

		if (utf8_strlen($this->request->post['text']) < 1) {
			$this->error['text'] = $this->language->get('error_text');
		}

		if (!isset($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
			$this->error['rating'] = $this->language->get('error_rating');
		}

		return !$this->error;
	}



	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/review')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete(){
		$json = array();

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->get['review_id']) && $this->request->get['review_id'] !== 0 ){

			if (isset($this->request->get['review_id'])) {
				$review_id = $this->request->get['review_id'];
			} else {
				$review_id = '';
			}

			if (isset($this->request->get['answer'])) {
				$answer = $this->request->get['answer'];
			} else {
				$answer = '';
			}

			if (isset($this->request->get['answer_answer'])) {
				$answer_answer = $this->request->get['answer_answer'];
			} else {
				$answer_answer = '';
			}

			if (isset($this->request->get['answer_id'])) {
				$answer_id = $this->request->get['answer_id'];
			} else {
				$answer_id = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'review_id' => $review_id,
				'limit' => $limit,
				'answer_id' => $answer_id,
				'answer' => $answer,
				'answer_answer' => $answer_answer

			);

			$this->load->model('catalog/review');

			$results = $this->model_catalog_review->getReviewForAutocomplete($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'review_id' => $result['review_id'],
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}
