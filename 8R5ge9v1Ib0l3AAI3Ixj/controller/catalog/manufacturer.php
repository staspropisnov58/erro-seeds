<?php
class ControllerCatalogManufacturer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/manufacturer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/manufacturer');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/manufacturer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/manufacturer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_manufacturer->addManufacturer($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/manufacturer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/manufacturer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			if(isset($this->request->post['category_to_manufacturer'])){

			$arr = $this->request->post['category_to_manufacturer'];

			foreach ($arr as $key => $arr1) {
				if(isset($arr1['copy_from'])){
					$with_copy_from[$arr1['name_id']] = array(
						'category' => $arr1['name'],
						'category_id' => $arr1['name_id'],
						'copy_from' => $arr1['copy_from'],
						'copy_from_id' => $arr1['copy_from_id']
					);
				}else{
					$without_copy_from[$arr1['name_id']] = array(
						'category' => $arr1['name'],
						'category_id' => $arr1['name_id']
					);
				}
			}

			if(!empty($with_copy_from)){
				$this->request->post['with_copy_from'] = $with_copy_from;
			}
		}

			$this->model_catalog_manufacturer->editManufacturer($this->request->get['manufacturer_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/manufacturer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $manufacturer_id) {
				$this->model_catalog_manufacturer->deleteManufacturer($manufacturer_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('catalog/manufacturer/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('catalog/manufacturer/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['manufacturers'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$manufacturer_total = $this->model_catalog_manufacturer->getTotalManufacturers();

		$results = $this->model_catalog_manufacturer->getManufacturers($filter_data);

		foreach ($results as $result) {
			$data['manufacturers'][] = array(
				'manufacturer_id' => $result['manufacturer_id'],
				'name'            => $result['name'],
				'sort_order'      => $result['sort_order'],
				'edit'            => $this->url->link('catalog/manufacturer/edit', 'token=' . $this->session->data['token'] . '&manufacturer_id=' . $result['manufacturer_id'] . $url, 'SSL')
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$data['sort_sort_order'] = $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $manufacturer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($manufacturer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($manufacturer_total - $this->config->get('config_limit_admin'))) ? $manufacturer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $manufacturer_total, ceil($manufacturer_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/manufacturer_list.tpl', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] 											= !isset($this->request->get['manufacturer_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] 									= $this->language->get('text_enabled');
		$data['text_disabled'] 									= $this->language->get('text_disabled');
		$data['text_default'] 									= $this->language->get('text_default');
		$data['text_percent'] 									= $this->language->get('text_percent');
		$data['text_amount'] 										= $this->language->get('text_amount');
		$data['text_none'] 											= $this->language->get('$text_none');

		$data['entry_description'] 							= $this->language->get('entry_description');
		$data['entry_description_2'] 						= $this->language->get('entry_description_2');
		$data['entry_meta_title']								= $this->language->get('entry_meta_title');
		$data['entry_meta_description'] 				= $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] 						= $this->language->get('entry_meta_keyword');

		$data['category_to_manufacturer_text'] 	= $this->language->get('category_to_manufacturer_text');
		$data['entry_category'] 								= $this->language->get('entry_category');
		$data['text_category'] 									= $this->language->get('text_category');
		$data['entry_copy_from'] 								= $this->language->get('entry_copy_from');
		$data['text_copy_from'] 								= $this->language->get('text_copy_from');

		$data['entry_name'] 										= $this->language->get('entry_name');
		$data['entry_store'] 										= $this->language->get('entry_store');
		$data['entry_keyword'] 									= $this->language->get('entry_keyword');
		$data['entry_meta_robots'] 							= $this->language->get('entry_meta_robots');
		$data['entry_image'] 										= $this->language->get('entry_image');
		$data['entry_sort_order'] 							= $this->language->get('entry_sort_order');
		$data['entry_customer_group'] 					= $this->language->get('entry_customer_group');
		$data['entry_opengraph_title'] 					= $this->language->get('entry_opengraph_title');
		$data['entry_opengraph_description'] 		= $this->language->get('entry_opengraph_description');

		$data['help_keyword'] 									= $this->language->get('help_keyword');
		$data['help_meta_robots'] 							= $this->language->get('help_meta_robots');

		$data['button_save'] 										= $this->language->get('button_save');
		$data['button_add'] 										= $this->language->get('button_add');
		$data['button_remove'] 									= $this->language->get('button_remove');
		$data['button_cancel'] 									= $this->language->get('button_cancel');

		$data['tab_general']										= $this->language->get('tab_general');
		$data['tab_data']												= $this->language->get('tab_data');
		$data['tab_open_graph'] 								= $this->language->get('tab_open_graph');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		if (isset($this->error['meta_robots'])) {
			$data['error_meta_robots'] = $this->error['meta_robots'];
		} else {
			$data['error_meta_robots'] = '';
		}

		if(isset($this->error['opengraph'])){
			$data['error_opengraph'] = $this->error['opengraph'];
		}else{
			$data['error_opengraph'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (!isset($this->request->get['manufacturer_id'])) {
			$data['action'] = $this->url->link('catalog/manufacturer/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('catalog/manufacturer/edit', 'token=' . $this->session->data['token'] . '&manufacturer_id=' . $this->request->get['manufacturer_id'] . $url, 'SSL');
		}

		$data['cancel'] = $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->post['manufacturer_description'])) {
				$data['manufacturer_description'] = $this->request->post['manufacturer_description'];
		} elseif (isset($this->request->get['manufacturer_id'])) {
				$data['manufacturer_description'] = $this->model_catalog_manufacturer->getManufacturerDescriptions($this->request->get['manufacturer_id']);
		} else {
				$data['manufacturer_description'] = array();
		}

		if (isset($this->request->get['manufacturer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($manufacturer_info)) {
			$data['name'] = $manufacturer_info['name'];
		} else {
			$data['name'] = '';
		}

		if(isset($this->request->post['category_to_manufacturer'])){
			$data['category_to_manufacturer'] = $this->request->post['category_to_manufacturer'];
		}elseif(isset($this->request->get['manufacturer_id'])){
			$data['category_to_manufacturer'] = $this->model_catalog_manufacturer->getCategoriesInManufacturer($this->request->get['manufacturer_id']);
		}else{
			$data['category_to_manufacturer']='';
		}

		$this->load->model('setting/store');

		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['manufacturer_store'])) {
			$data['manufacturer_store'] = $this->request->post['manufacturer_store'];
		} elseif (isset($this->request->get['manufacturer_id'])) {
			$data['manufacturer_store'] = $this->model_catalog_manufacturer->getManufacturerStores($this->request->get['manufacturer_id']);
		} else {
			$data['manufacturer_store'] = array(0);
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($manufacturer_info)) {
			$data['keyword'] = $manufacturer_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['meta_robots'])) {
			$data['meta_robots'] = $this->request->post['meta_robots'];
		} elseif (!empty($manufacturer_info)) {
			$data['meta_robots'] = $manufacturer_info['meta_robots'];
		} else {
			$data['meta_robots'] = '';
		}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($manufacturer_info)) {
			$data['image'] = $manufacturer_info['image'];
		} else {
			$data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($manufacturer_info) && is_file(DIR_IMAGE . $manufacturer_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($manufacturer_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($manufacturer_info)) {
			$data['sort_order'] = $manufacturer_info['sort_order'];
		} else {
			$data['sort_order'] = '';
		}

		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL && $this->config->get('opengraph_status') !== NULL && (int)$this->config->get('opengraph_status') === 1){

			$data['opengraph_use_meta_when_no_og'] = $this->config->get('opengraph_use_meta_when_no_og');
			$data['opengraph_status'] = $this->config->get('opengraph_status');

			$data['opengraph'] = array();

			if(isset($this->request->post['opengraph'])){
				$opengraph = $this->request->post['opengraph'];
			}elseif(isset($this->request->get['manufacturer_id'])){
				$opengraph = $this->model_catalog_manufacturer->getOpengraph($this->request->get['manufacturer_id']);
			}else{
				$opengraph = array();
			}

			foreach($opengraph as $key => $value){
				if($value['image']!==''){
					$opengraph[$key]['thumb'] = $this->model_tool_image->resize($value['image'],100,100);
				}else{
					$opengraph[$key]['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
				}
			}
			$data['opengraph'] = $opengraph;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/manufacturer_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/manufacturer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if($this->config->get('opengraph_use_meta_when_no_og')!== NULL  && $this->config->get('opengraph_status')!== NULL && (int)$this->config->get('opengraph_status') === 1){
	    foreach($this->request->post['opengraph'] as $language_id => $value){
        if((int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['title'])) < 1 ||(int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['title'])) > 100){
          $this->error['opengraph'][$language_id]['title'] = $this->language->get('error_lenght_title');
        }elseif((int)$this->config->get('opengraph_use_meta_when_no_og') === 1 && utf8_strlen(trim($value['title'])) > 100){
          $this->error['opengraph'][$language_id]['title'] = $this->language->get('error_empty_title_2');
        }

        if((int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['description'])) < 1 ||(int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['description'])) > 300){
          $this->error['opengraph'][$language_id]['description'] = $this->language->get('error_empty_description');
        }elseif((int)$this->config->get('opengraph_use_meta_when_no_og') === 1 && utf8_strlen(trim($value['description'])) > 300){
          $this->error['opengraph'][$language_id]['description'] = $this->language->get('error_empty_description_2');
        }

        if((int)$this->config->get('opengraph_use_meta_when_no_og') !== 1 && utf8_strlen(trim($value['image'])) < 1){
          $this->error['opengraph'][$language_id]['image'] = $this->language->get('error_empty_image');
        }
	    }
		}

		if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if ((utf8_strlen($this->request->post['meta_title']) < 2) || (utf8_strlen($this->request->post['meta_title']) > 64)) {
			// $this->error['meta_title'] = $this->language->get('error_meta_title');
		}

		if (utf8_strlen($this->request->post['keyword']) > 0) {
			$this->load->model('catalog/url_alias');

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

			if ($url_alias_info && isset($this->request->get['manufacturer_id']) && $url_alias_info['query'] != 'manufacturer_id=' . $this->request->get['manufacturer_id']) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}

			if ($url_alias_info && !isset($this->request->get['manufacturer_id'])) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}
		}

		if(utf8_strlen($this->request->post['meta_robots']) > 150) {
			$this->error['meta_robots'] = sprintf($this->language->get('error_meta_robots'));
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/manufacturer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/product');

		foreach ($this->request->post['selected'] as $manufacturer_id) {
			$product_total = $this->model_catalog_product->getTotalProductsByManufacturerId($manufacturer_id);

			if ($product_total) {
				$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
			}
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/manufacturer');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_catalog_manufacturer->getManufacturers($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'manufacturer_id' => $result['manufacturer_id'],
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
