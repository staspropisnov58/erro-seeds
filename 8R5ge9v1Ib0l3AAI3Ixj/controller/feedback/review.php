<?php
/**
* namespace Admin\Controller\Feedback
*/
class ControllerFeedbackReview extends Controller
{
  /**
  * use Admin\Model\Feedback\Review;
  */
  private $error = array();
  private $item = '';

  public function index()
  {
    $this->load->language('feedback/review');

		$this->load->model('feedback/review');

    $this->item = $this->request->get['item'];

    $this->document->setTitle(sprintf($this->language->get('heading_title'), $this->language->get('text_' . $this->item)));

		$this->getList();
  }

  public function add()
  {
    $this->load->language('feedback/review');

		$this->document->setTitle($this->language->get('heading_title_add'));

		$this->load->model('feedback/review');

    $this->item = $this->request->get['item'];

    $this->getForm();
  }

  public function edit()
  {
    $this->load->language('feedback/review');

		$this->document->setTitle($this->language->get('heading_title_edit'));

		$this->load->model('feedback/review');

    $this->item = $this->request->get['item'];

    $this->getForm();
  }

  public function delete()
  {

  }

  protected function getList()
  {
    if (isset($this->request->get['filter_rating'])) {
			$filter_rating = $this->request->get['filter_rating'];
		} else {
			$filter_rating = null;
		}

		if (isset($this->request->get['filter_author'])) {
			$filter_author = $this->request->get['filter_author'];
		} else {
			$filter_author = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

    if (isset($this->request->get['filter_item'])) {
			$filter_item = $this->request->get['filter_item'];
		} else {
			$filter_item = '';
		}

    if (isset($this->request->get['filter_text'])) {
			$filter_text = $this->request->get['filter_text'];
		} else {
			$filter_text = '';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = $this->buildUrl(true);

    $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => sprintf($this->language->get('heading_title'), $this->language->get('text_' . $this->item)),
			'href' => $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('feedback/review/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('feedback/review/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['reviews'] = array();

    $filter_data = array(
			'filter_rating'       => $filter_rating,
			'filter_author'       => $filter_author,
			'filter_status'       => $filter_status,
			'filter_date_added'   => $filter_date_added,
      'filter_item'         => $filter_item,
      'filter_text'         => $filter_text,
      'item'                => $this->item
		);

    $review_total = $this->model_feedback_review->getTotalReviews($filter_data);

    $filter_data['sort'] = $sort;
    $filter_data['order'] = $order;
    $filter_data['start'] = ($page - 1) * $this->config->get('config_limit_admin');
    $filter_data['limit'] = $this->config->get('config_limit_admin');

    $results = $this->model_feedback_review->getReviews($filter_data);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'review_id'         => $result['review_id'],
				'author'            => $result['author'],
        'customer'           => $result['customer_id'] ? $this->url->link('sale/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL') : '',
        'email'             => $result['email'],
				'rating'            => $result['rating'],
        'answers'          => $result['answers'],
        'disabled_answers' => $result['disabled_answers'],
				'status'            => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'date_added'        => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'info'              => $this->url->link('feedback/review/info', 'token=' . $this->session->data['token'] . '&review_id=' . $result['review_id'] . $url, 'SSL')
			);
		}

    $data['heading_title'] = sprintf($this->language->get('heading_title'), $this->language->get('text_' . $this->item));

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['column_author']            = $this->language->get('column_author');
		$data['column_rating']            = $this->language->get('column_rating');
		$data['column_status']            = $this->language->get('column_status');
		$data['column_date_added']        = $this->language->get('column_date_added');
		$data['column_action']            = $this->language->get('column_action');
    $data['column_answers']          = $this->language->get('column_answers');
    $data['column_disabled_answers'] = $this->language->get('column_disabled_answers');

		$data['entry_product']      = $this->language->get('entry_product');
		$data['entry_author']       = $this->language->get('entry_author');
		$data['entry_rating']       = $this->language->get('entry_rating');
		$data['entry_status']       = $this->language->get('entry_status');
		$data['entry_date_added']   = $this->language->get('entry_date_added');
    $data['entry_text']         = $this->language->get('entry_text');

    if ($this->item !== 'store') {
      $data['entry_item'] = $this->language->get('entry_' . $this->item);
    }

		$data['button_add']    = $this->language->get('button_add');
		$data['button_view']   = $this->language->get('button_view');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

    if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

    $url = '';

    $url .= '&item=' . $this->item;

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_author'] = $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . '&sort=r.author' . $url, 'SSL');
		$data['sort_rating'] = $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . '&sort=r.rating' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . '&sort=r.status' . $url, 'SSL');
    $data['sort_answers'] = $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . '&sort=r.answers' . $url, 'SSL');
    $data['sort_disabled_answers'] = $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . '&sort=r.disabled_answers' . $url, 'SSL');
		$data['sort_date_added'] = $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . '&sort=r.date_added' . $url, 'SSL');

		$url = $this->buildUrl();

    $pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($review_total - $this->config->get('config_limit_admin'))) ? $review_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $review_total, ceil($review_total / $this->config->get('config_limit_admin')));

		$data['filter_rating'] = $filter_rating;
		$data['filter_author'] = $filter_author;
		$data['filter_status'] = $filter_status;
		$data['filter_date_added'] = $filter_date_added;
    $data['filter_text'] = $filter_text;

    if ($this->item !== 'store') {
      $data['filter_item'] = $filter_item;
    }

		$data['sort'] = $sort;
		$data['order'] = $order;

    $data['item'] = $this->item;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('feedback/review_list.tpl', $data));
  }

  public function info()
  {
    $review_id = isset($this->request->get['review_id']) ? $this->request->get['review_id'] : 0;

    $this->load->language('feedback/review');

    $this->load->model('feedback/review');

    $this->item = $this->request->get['item'];

    $discussion = $this->model_feedback_review->getDiscussion($review_id);

    if ($discussion) {
      if (isset($this->request->get['filter_rating'])) {
        $filter_rating = $this->request->get['filter_rating'];
      } else {
        $filter_rating = null;
      }

      if (isset($this->request->get['filter_author'])) {
        $filter_author = $this->request->get['filter_author'];
      } else {
        $filter_author = null;
      }

      if (isset($this->request->get['filter_status'])) {
        $filter_status = $this->request->get['filter_status'];
      } else {
        $filter_status = null;
      }

      if (isset($this->request->get['filter_date_added'])) {
        $filter_date_added = $this->request->get['filter_date_added'];
      } else {
        $filter_date_added = null;
      }

      if (isset($this->request->get['filter_item'])) {
        $filter_item = $this->request->get['filter_item'];
      } else {
        $filter_item = '';
      }

      if (isset($this->request->get['filter_text'])) {
        $filter_text = $this->request->get['filter_text'];
      } else {
        $filter_text = '';
      }

      if (isset($this->request->get['order'])) {
        $order = $this->request->get['order'];
      } else {
        $order = 'ASC';
      }

      if (isset($this->request->get['sort'])) {
        $sort = $this->request->get['sort'];
      } else {
        $sort = 'date_added';
        $order = 'DESC';
      }

      if (isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
      } else {
        $page = 1;
      }

      $url = $this->buildUrl(true);

      $this->document->setTitle(sprintf($this->language->get('heading_title_info'), $this->language->get('text_' . $this->item)));

      $data['heading_title_info'] = sprintf($this->language->get('heading_title_info'), $this->language->get('text_' . $this->item));

      $data['text_discussion'] = $this->language->get('text_discussion');

      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
        'text' => sprintf($this->language->get('heading_title'), $this->language->get('text_' . $this->item)),
        'href' => $this->url->link('feedback/review', 'token=' . $this->session->data['token'] . $url, 'SSL')
      );

      $data['breadcrumbs'][] = array(
        'text' => sprintf($this->language->get('heading_title_info'), $this->language->get('text_' . $this->item)),
        'href' => $this->url->link('feedback/review/info', 'token=' . $this->session->data['token'] . '&review_id=' . $review_id . $url, 'SSL')
      );

    } else {
      $this->load->language('error/not_found');

      $this->document->setTitle($this->language->get('heading_title'));

      $data['heading_title'] = $this->language->get('heading_title');

      $data['text_not_found'] = $this->language->get('text_not_found');

      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
      );

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('heading_title'),
          'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
      );
    }

    $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('feedback/review_info.tpl', $data));

  }

  protected function getForm()
  {
    $data['text_form'] = !isset($this->request->get['review_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_author'] = $this->language->get('entry_author');
		$data['entry_date_added'] = $this->language->get('entry_date_added');
		$data['entry_rating'] = $this->language->get('entry_rating');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_text'] = $this->language->get('entry_text');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

    $this->load->model('user/user');

    $user_info = $this->model_user_user->getUser($this->user->getId());

        if ($user_info) {
            $data['user_group'] = $user_info['user_group'] ;
        } else {
            $data['user_group'] = '';
        }

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['author'])) {
			$data['error_author'] = $this->error['author'];
		} else {
			$data['error_author'] = '';
		}

		if (isset($this->error['text'])) {
			$data['error_text'] = $this->error['text'];
		} else {
			$data['error_text'] = '';
		}

		if (isset($this->error['rating'])) {
			$data['error_rating'] = $this->error['rating'];
		} else {
			$data['error_rating'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_rating'])) {
			$url .= '&filter_rating=' . $this->request->get['filter_rating'];
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('review/store_review', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (!isset($this->request->get['review_id'])) {
			$data['action'] = $this->url->link('review/store_review/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('review/store_review/edit', 'token=' . $this->session->data['token'] . '&review_id=' . $this->request->get['review_id'] . $url, 'SSL');
		}

		$data['cancel'] = $this->url->link('review/store_review', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['review_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$review_info = $this->model_review_store_review->getReview($this->request->get['review_id']);
		}

		$data['token'] = $this->session->data['token'];


		if (isset($this->request->post['author'])) {
			$data['author'] = $this->request->post['author'];
		} elseif (!empty($review_info)) {
			$data['author'] = $review_info['author'];
		} else {
			$data['author'] = '';
		}

		if (isset($this->request->post['date_added'])) {
			$data['date_added'] = $this->request->post['date_added'];
		} elseif (!empty($review_info)) {
			$data['date_added'] = ($review_info['date_added'] != '0000-00-00') ? $review_info['date_added'] : '';
		} else {
			$data['date_added'] = date('Y-m-d');
		}

		if (isset($this->request->post['text'])) {
			$data['text'] = $this->request->post['text'];
		} elseif (!empty($review_info)) {
			$data['text'] = $review_info['text'];
		} else {
			$data['text'] = '';
		}

        if (isset($this->request->post['admin_author'])) {
            $data['admin_author'] = $this->request->post['admin_author'];
        } elseif (!empty($review_info)) {
            $data['admin_author'] = $review_info['admin_author'] ? $review_info['admin_author'] : $data['user_group'];
        } else {
            $data['admin_author'] = $data['user_group'];
        }

        if (isset($this->request->post['answer'])) {
            $data['answer'] = $this->request->post['answer'];
        } elseif (!empty($review_info)) {
            $data['answer'] = $review_info['answer'];
        } else {
            $data['answer'] = '';
        }

		if (isset($this->request->post['rating'])) {
			$data['rating'] = $this->request->post['rating'];
		} elseif (!empty($review_info)) {
			$data['rating'] = $review_info['rating'];
		} else {
			$data['rating'] = '';
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($review_info)) {
			$data['status'] = $review_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('review/store_review_form.tpl', $data));
  }

  protected function validateForm()
  {

  }

  protected function validateDelete()
  {

  }

  protected function buildUrl($with_page = false)
  {
    $url = '';

    $url .= '&item=' . $this->item;

    if (isset($this->request->get['filter_rating'])) {
			$url .= '&filter_rating=' . $this->request->get['filter_rating'];
		}

		if (isset($this->request->get['filter_author'])) {
			$url .= '&filter_author=' . urlencode(html_entity_decode($this->request->get['filter_author'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

    if (isset($this->request->get['filter_item'])) {
			$url .= '&filter_item=' . $this->request->get['filter_item'];
		}

    if (isset($this->request->get['filter_text'])) {
			$url .= '&filter_text=' . $this->request->get['filter_text'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if ($with_page && isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

    return $url;
  }
}
