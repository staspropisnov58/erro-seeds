<?php
// Heading
$_['heading_title']     		            = 'Новости';

// Text
$_['text_list']					                = 'Список новостей';
$_['text_image']				                = 'Картинка для превью новостей';
$_['text_image_2']				              = 'Картинка для одной новости 1110 x 390';
$_['text_title']				                = 'Название';
$_['text_description']			            = 'Описание';
$_['text_short_description']	          = 'Краткое описание';
$_['text_date']					                = 'Дата публикации';
$_['text_action']				                = 'Действия';
$_['text_edit']					                = 'Редактировать';
$_['text_meta_description']             = 'Meta description';
$_['text_meta_title']                   = 'Meta title';
$_['text_meta_keyword']                 = 'Meta keyword';
$_['text_meta_robots']                  = 'Meta Robots';
$_['text_status']				                = 'Статус';
$_['text_keyword']				              = 'SEO алиас';
$_['text_no_results']			              = 'Пустой список';
$_['text_browse']				                = 'Выбрать';
$_['text_clear']				                = 'Очитстить';
$_['text_image_manager']	              = 'Менеджер изображений';
$_['text_success']				              = 'Успешно!';


//Entry
$_['entry_sale_event']                  = 'Связанная распродажа';
$_['entry_opengraph_image']             = 'Выберите изображение';
$_['entry_opengraph_title']             = 'Введите заголовок';
$_['entry_opengraph_description']       = 'Введите описание';

//help
$_['help_meta_robots']                  = 'Вводить только реальные значения content для robots, через запятую';
$_['help_sale_event']                   = 'Выберите распродажу, с котой будет связана эта новость';

// Error
$_['error_permission'] 			            = 'У вас нет прав!';
$_['error_meta_robots']                 = 'Количество символов не должно превышать 150';
$_['error_keyword']                     = 'Введите Seo Url';
$_['error_lenght_title']                = 'Поле не может быть пустым и не должно превышать 100 символов';
$_['error_empty_title_2']               = 'Поле не должно превышать 100 символов';
$_['error_empty_description']           = 'Поле не может быть пустым и не должно превышать 300 символов';
$_['error_empty_description_2']         = 'Поле не должно превышать 300 символов';
$_['error_empty_image']                 = 'Выберите изображение';
