<?php
// Heading
$_['heading_title']                         = 'Распродажи';
$_['heading_title_info']                    = 'Распродажа';

// Text
$_['text_success']                          = 'Настройки успешно изменены!';
$_['text_list']                             = 'Распродажи';
$_['text_add']                              = 'Добавить';
$_['text_edit']                             = 'Редактирование';
$_['text_new_heading_title']                = 'Новая скидка';
$_['text_heading_title']                    = 'Скидка';
$_['text_sale_event_type']                  = 'Тип распродажи:';
$_['text_sale_events_time']                 = 'Длительность распродажи:';
$_['text_sale_events']                      = 'Скидки';
$_['text_enebled_coupons']                  = 'Действующие купоны в момент распродажи:';
$_['text_sale_event_setting']               = 'Текущие настройки распродажи';
$_['text_disabled_coupons']                 = 'Все купоны отключены';
$_['text_sale_event_name']                  = 'Название распродажи:';
$_['text_sale_event_description']           = 'Описание распродажи: ';
$_['text_sale_event_banner_and_news']       = 'Баннеры и новости';
$_['text_banner_image']                     = 'Изображение баннера';
$_['text_banner_status']                    = 'Статус баннера';
$_['text_enebled']                          = 'Включен';
$_['text_disabled']                         = 'Выключен';
$_['text_news_name']                        = 'Название новости';
$_['text_news_short_description']           = 'Краткое описание новости';
$_['text_news_status']                      = 'Статус новости';
$_['text_percent_name']                     = 'Скидка %s' ;
$_['text_categories']                       = 'Категории';
$_['text_manufacturers']                    = 'Производители';
$_['text_products']                         = 'Товары';
$_['tab_special']                           = 'Скидки';
$_['text_customer_groups']                  = 'Группа покупателей';
$_['text_product_name']                     = 'Название товара';
$_['text_product_model']                    = 'Модель товара';
$_['text_product_price']                    = 'Цена товара';
$_['text_options_name']                     = 'Название опции';
$_['text_options_price']                    = 'Цена опции';


// Column
$_['column_name']                           = 'Название Распродажи';
$_['column_type_name']                      = 'Тип Распродажи';
$_['column_datetime_start']                 = 'Дата начала';
$_['column_datetime_end']                   = 'Дата окончания';
$_['column_name_news']                      = 'Название Новости';
$_['column_on_off']                         = 'Выбрать новость';
$_['column_action']                         = 'Действие';

// Entry
$_['entry_name']                            = 'Название распродажи';
$_['entry_type_name']                       = 'Тип распродажи';
$_['entry_description']                     = 'Описание распродажи';
$_['entry_datetime_start']                  = 'Дата начала распродажи';
$_['entry_datetime_end']                    = 'Дата окончания распродажи';
$_['entry_sale_event_type']                 = 'Выберите тип распродажи';
$_['entry_turn_coupons']                    = 'Отключить купоны';
$_['entry_banner_group']                    = 'Страница баннера';
$_['entry_persent']                         = 'Процент скидки';
$_['entry_priority']                        = 'Приоритет';
$_['entry_customer_group']                  = 'Группа пользователей';
$_['entry_option']                          = 'Исключенные опции товаров';
$_['entry_product']                         = 'Товары';
$_['entry_category']                        = 'Категории';
$_['entry_manufacturer']                    = 'Производители';


// Help
$_['help_code']                             = 'Данный код используется для отслеживания продаж маркетинговой акции.';
$_['help_example']                          = 'Таким образом, Вы можете добавить код отслеживания, в конце URL-адреса ссылающихся на ваш сайт или товар на вашем сайте.';
$_['help_turn_coupons']                     = 'Выберите купоны .которые нужно будет отключить';
$_['help_customer_group']                   = 'Выберите группу пользователей, к которой применяются распродажа';
$_['help_option']                           = 'Выберите опции, в которых распродажа не учитывается';
$_['help_product']                          = 'Выберите товары, к которым нужно применить распродажу';
$_['help_category']                         = 'Выберите категорию, к товарам которой необходимо применить распродажу';
$_['help_manufacturer']                     = 'Выберите производителя, к товарам которого необходимо применить распродажу';


//Button
$_['button_settings_add']                   = 'Добавить скидку';
$_['button_remove']                         = 'Удалить';

// Error
$_['error_permission']                      = 'У Вас нет прав для изменения настроек модуля Маркетинговая акция !';
$_['error_name']                            = 'Название распродажи должно быть от 3 до 32 символов!';
$_['error_code']                            = 'Необходимо ввести Код отслеживания!';
$_['error_datetime']                        = 'дата завершения распродажи. Не может быть меньше даты начала';
$_['error_persent']                         = 'Внимание! Допустимо только любое число больше 0 и меньше 100!';
$_['error_not_unique_persent']              = 'Внимание! Проценты должны быть уникальными!';
$_['error_priority']                      = 'Внимание! Допустимы только числа!';
