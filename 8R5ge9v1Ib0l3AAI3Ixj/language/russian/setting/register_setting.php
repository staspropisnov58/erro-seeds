<?php
// Heading
$_['heading_title']      	             = 'Настройка регистрации';

// Text
$_['text_stores']							         = 'Магазины';
$_['text_success']                     = 'Настройки регистрации успешно изменены!';
$_['text_edit']             	         = 'Редактирование';
$_['text_enabled']                     = 'Включен';
$_['text_disabled']                    = 'Выключен';
$_['text_none']                        = '-- Не выбрано --';
$_['entry_type']                       = 'Тип поля';

//Entry

$_['entry_label']                      = 'Значения для label';
$_['entry_placeholder']                = 'Значение для placeholder';
$_['entry_sorting']                    = 'Порядок Сортировки';
$_['entry_status']                     = 'Выберите статус для поля';
$_['entry_editable']                   = 'Возможность правки пользователю';
$_['text_tel']                         = 'Телефон';
$_['text_select']                      = 'Список';
$_['text_radio']                       = 'Переключатель';
$_['text_checkbox']                    = 'Флажок';
$_['text_input']                       = 'Ввод текста';
$_['text_text']                        = 'Текст';
$_['text_textarea']                    = 'Текстовая область';
$_['text_file']                        = 'Файл';
$_['text_date']                        = 'Дата';
$_['text_datetime']                    = 'Дата &amp; Время';
$_['text_time']                        = 'Время';
$_['text_account']                     = 'Аккаунт';
$_['text_address']                     = 'Адрес';
$_['text_choose']                      = 'Выбор';
$_['entry_tel_code']                   = 'Показывать код телефонов в зависимости от страны';
//Button
$_['button_save']                      = 'Сохранить';
$_['button_cancel']                    = 'Выйти';

//error
$_['error_label']                      = 'Поле label должно быть заполнено и содержать менее 100 символов';
$_['error_placeholder']                = 'Поле placeholder Должно содержать менее 100 символов';
$_['error_sort_order']                 = 'Поле сортировки должно содержать только числа';
$_['error_permission']                 = 'У Вас нет прав редактировать Настройку Регистрации';
