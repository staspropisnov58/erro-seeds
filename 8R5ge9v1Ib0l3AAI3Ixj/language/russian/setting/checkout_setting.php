<?php
// Heading
$_['heading_title']     	                    = 'Настройка оформления заказа';
// Text
$_['text_success']                            = 'Настройки оформления заказа успешно изменены!';
$_['text_stores']							                = 'Магазины';
$_['text_form']                               = 'Редактирование';
$_['text_enabled']                            = 'Enabled';
$_['text_disabled']                           = 'Disabled';
$_['text_checkout_fast_order_status']         = 'Статус оформления быстрого заказа';
$_['text_no_results']                         = 'Нет данных';
$_['text_tel']                         = 'Телефон';
$_['text_select']                      = 'Список';
$_['text_radio']                       = 'Переключатель';
$_['text_checkbox']                    = 'Флажок';
$_['text_input']                       = 'Ввод текста';
$_['text_text']                        = 'Текст';
$_['text_textarea']                    = 'Текстовая область';
$_['text_file']                        = 'Файл';
$_['text_date']                        = 'Дата';
$_['text_datetime']                    = 'Дата &amp; Время';
$_['text_time']                        = 'Время';
$_['text_account']                     = 'Аккаунт';
$_['text_address']                     = 'Адрес';
$_['text_choose']                      = 'Выбор';
$_['text_datalist']                    = 'Поле с подсказками';
$_['firstname']                               = 'Имя';
$_['lastname']                                = 'Фамилия';
$_['company']                                 = 'Компания';
$_['patronymic']                              = 'Отчество';
$_['address_1']                               = 'Основной адрес';
$_['address_2']                               = 'Дополнительный адрес';
$_['city']                                    = 'Город';
$_['postcode']                                = 'Индекс';
$_['country_id']                              = 'Страна';
$_['zone_id']                                 = 'Область';
$_['error_lenght']                            = 'Длина поля должна быть до 100 символов';
$_['error_numeric']                           = 'Поле должно состоять толькло из чисел';
$_['error_min_order']                         = 'Поле не должно быть пустым';
$_['entry_checkout_fields']                   = 'Поле для гостевого заказа';

//help
$_['help_fields']                             = 'Выбранные поля будут обязательны для заполнения гостями и зарегистрированными пользователями';

//tab
$_['tab_general']                             = 'Общие настройки';
$_['tab_shipping']                            = 'Доставка';
$_['tab_payment']                             = 'Оплата';

//Entry
$_['entry_label']                             = 'Значения для label';
$_['entry_placeholder']                       = 'Значение для placeholder';
$_['entry_sorting']                           = 'Порядок Сортировки';
$_['entry_status']                            = 'Выберите статус для поля';
$_['entry_helper']                            = 'Подсказка для поля';
$_['entry_value']                             = 'Значение для поля';
$_['entry_country']                           = 'Выберите страну';
$_['entry_zone']                              = 'Выберите область';
$_['entry_shipping']                          = 'Доставка';
$_['entry_min_order']                         = 'Минимальный заказ';
$_['entry_type']                              = 'Тип поля';
$_['entry_no_callback']                       = 'Чекбокс «Не перезванивайте» в обычном оформлении заказа';

$_['help_shipping']                           = 'Выберите способ доставки';
$_['help_no_callback']                        = 'На последнем шаге будет показан чекбокс «Не перезванивайте». В заказе с «Не перезванивайте» будет пометка рядом с номером покупателя';

//Button
$_['button_save']                             = 'Сохранить';
$_['button_cancel']                           = 'Выйти';

//error
