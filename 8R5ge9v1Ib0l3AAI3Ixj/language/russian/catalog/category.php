<?php
// Heading
$_['heading_title']                     = 'Категории';

// Text
$_['text_success']                      = 'Настройки успешно изменены!';
$_['text_list']                         = 'Категории';
$_['text_add']                          = 'Добавить';
$_['text_edit']                         = 'Редактирование';
$_['text_default']                      = 'Основной магазин';

// Column
$_['column_name']                       = 'Категории';
$_['column_sort_order']                 = 'Порядок сортировки';
$_['column_action']                     = 'Действие';

// Entry
$_['entry_name']                        = 'Категория';
$_['entry_description']                 = 'Описание';
$_['entry_description_2']               = 'Описание для низа страницы';
$_['entry_meta_title'] 	                = 'Мета-тег Title';
$_['entry_meta_keyword'] 	              = 'Мета-тег Keywords';
$_['entry_meta_robots']                 = 'Meta Robots';
$_['entry_meta_description']            = 'Мета-тег Description';
$_['entry_keyword']                     = 'SEO URL';
$_['entry_parent']                      = 'Родительская категория';
$_['entry_filter']                      = 'Фильтры';
$_['entry_store']                       = 'Магазины';
$_['entry_image']                       = 'Изображение категории (иконка)';
$_['entry_opengraph_image']             = 'Выберите изображение';
$_['entry_image_2']                     = 'Изображение категории (внутри категории)';
$_['entry_top']                         = 'Главное меню';
$_['entry_column']                      = 'Столбцы';
$_['entry_sort_order']                  = 'Порядок сортировки';
$_['entry_status']                      = 'Статус';
$_['entry_layout']                      = 'Макет';
$_['entry_opengraph_title']             = 'Введите заголовок';
$_['entry_opengraph_description']       = 'Введите описание';

// Help
$_['help_filter']                       = '(Автозаполнение)';
$_['help_keyword']                      = 'Должно быть уникальным на всю систему.';
$_['help_meta_robots']                  = 'Вводить только реальные значения content для robots, через запятую';
$_['help_top']                          = 'Показывать в главном меню (только для главных родительских категорий).';
$_['help_column']                       = 'Количество столбцов в выпадающем меню категории (только для главных родительских категорий)';

// Error
$_['error_warning']                     = 'Внимательно проверьте форму на ошибки!';
$_['error_permission']                  = 'У Вас нет прав для изменения категорий!';
$_['error_name']                        = 'Название категории должно быть от 2 до 32 символов!';
$_['error_meta_title']                  = 'Ключевое слово должно быть от 3 до 255 символов!';
$_['error_keyword']		     	            = 'SEO URL занят!';
$_['error_meta_robots']                 = 'Количество символов должно быть не меньше 150';
$_['error_lenght_title']                = 'Поле не может быть пустым и не должно превышать 100 символов';
$_['error_empty_title_2']               = 'Поле не должно превышать 100 символов';
$_['error_empty_description']           = 'Поле не может быть пустым и не должно превышать 300 символов';
$_['error_empty_description_2']         = 'Поле не должно превышать 300 символов';
$_['error_empty_image']                 = 'Выберите изображение';
