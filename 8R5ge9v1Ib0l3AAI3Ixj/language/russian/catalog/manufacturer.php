<?php
// Heading
$_['heading_title']                     = 'Производители';

// Text
$_['text_success']                      = 'Настройки успешно изменены!';
$_['text_list']                         = 'Производители';
$_['text_add']                          = 'Добавить';
$_['text_edit']                         = 'Редактирование';
$_['text_default']                      = 'Основной магазин';
$_['text_percent']                      = 'Процент';
$_['text_amount']                       = 'Фиксированная сумма';
$_['text_none']                         = 'Пусто';
// Column
$_['column_name']                       = 'Производитель';
$_['column_sort_order']                 = 'Порядок сортировки';
$_['column_action']                     = 'Действие';

// Entry
$_['entry_description']                 = 'Описание';
$_['entry_description_2']               = 'Краткое описание производителя';
$_['tab_general']                       = 'Основное';
$_['tab_data']                          = 'Данные';
$_['category_to_manufacturer_text']     = 'Категории товаров';
$_['entry_category']                    = 'Введите категорию';
$_['text_category']                     = 'Категория';
$_['entry_copy_from']                   = 'Скопировать товары из';
$_['text_copy_from']                    = 'Скопировать товары';
$_['entry_name']                        = 'Производитель';
$_['entry_meta_title'] 	                = 'Мета-тег Title';
$_['entry_meta_description']            = 'Мета-тег Description';
$_['entry_meta_keyword'] 	              = 'Мета-тег Keywords';
$_['button_remove']                     = 'Удалить';
$_['button_add']                        = 'Добавить';

$_['entry_store']                       = 'Магазины';
$_['entry_keyword']                     = 'SEO URL';
$_['entry_meta_robots']                 = 'Meta Robots';
$_['entry_image']                       = 'Изображение';
$_['entry_sort_order']                  = 'Порядок сортировки';
$_['entry_type']                        = 'Тип';
$_['entry_opengraph_title']             = 'Введите заголовок';
$_['entry_opengraph_description']       = 'Введите описание';

// Help
$_['help_keyword']                      = 'Должно быть уникальным на всю систему и без пробелов';
$_['help_meta_robots']                  = 'Вводить только реальные значения content для robots, через запятую';

// Error
$_['error_permission']                  = 'У Вас нет прав для изменения производителей!';
$_['error_name']                        = 'Название производителя должно быть от 3 до 64 символов!';
$_['error_keyword']			                = 'SEO URL занят!';
$_['error_meta_title']                  = 'Meta Title производителя должно быть от 3 до 64 символов!';
$_['error_meta_robots']                 = 'Количество символов не должно превышать 150';
$_['error_product']                     = 'Внимание: Данного производителя нельзя удалить так как назначен %s товарам!';
$_['error_lenght_title']                = 'Поле не может быть пустым и не должно превышать 100 символов';
$_['error_empty_title_2']               = 'Поле не должно превышать 100 символов';
$_['error_empty_description']           = 'Поле не может быть пустым и не должно превышать 300 символов';
$_['error_empty_description_2']         = 'Поле не должно превышать 300 символов';
$_['error_empty_image']                 = 'Выберите изображение';
