<?php
// Heading
$_['heading_title']     = 'Отзывы новостей';

// Text
$_['text_success']      = 'Настройки успешно изменены!!';
$_['text_list']         = 'Отзывы';
$_['text_add']          = 'Добавить';
$_['text_edit']         = 'Редактирование';

// Column
$_['column_product']    = 'Новость';
$_['column_author']     = 'Автор';
$_['column_rating']     = 'Рейтинг';
$_['column_status']     = 'Статус';
$_['column_date_added'] = 'Дата';
$_['column_action']     = 'Действие';

// Entry
$_['entry_product']     = 'Новость';
$_['entry_author']      = 'Автор';
$_['entry_rating']      = 'Рейтинг';
$_['entry_status']      = 'Статус';
$_['entry_text']        = 'Текст';
$_['entry_date_added']  = 'Дата';
$_['entry_email']       = 'Email';

// Help
$_['help_product']      = '(Автозаполнение)';

// Error
$_['error_permission']  = 'У Вас нет прав для изменения отзывов!';
$_['error_product']     = 'Требуется выбрать Новость!';
$_['error_author']      = 'Имя автора должно содержать от 3 до 64 символов!';
$_['error_text']        = 'Текст отзыва должен содержать хотя бы 1 символ!';
$_['error_rating']      = 'Требуется установить рейтинг!';
