<?php
// Heading
$_['heading_title']	   = 'Журнал событий';

// Text
$_['text_success']	   = 'Журнал событий очищен!';
$_['text_list']        = 'События';

// Error
$_['error_warning']	   = 'Внимание: Ваш  файл событий %s is %s!';
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
