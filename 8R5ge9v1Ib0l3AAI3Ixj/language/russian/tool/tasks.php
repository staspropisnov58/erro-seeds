<?php
  $_['heading_title_once'] = 'Единоразовые задачи';
  $_['heading_title_regular'] = 'Регулярные задачи';

  $_['entry_datetime_run'] = 'Дата и время начала';
  $_['entry_action'] = 'Контроллер';
  $_['entry_args'] = 'Аргументы';
  $_['entry_args']  = 'Введите аргументы через запятую';
  $_['entry_frequency'] = 'Введите периодчность выполнения задачи';

  $_['column_action'] = 'Действие';
  $_['column_periodicity'] = 'Периодчность';

  $_['text_once'] = 'Единоразово';
  $_['text_year'] = 'Каждый год';
  $_['text_month'] = 'Каждый месяц';
  $_['text_week'] = 'Каждую неделю';
  $_['text_day'] = 'Каждый день';
  $_['text_hour'] = 'Каждый час';
  $_['text_minute'] = 'Каждую минуту';
  $_['text_success'] = 'Настройки успешно изменены.';
  $_['text_select_option'] = 'Выберите значение';
  $_['text_list'] = 'Список задач';
  $_['text_form'] = 'Задача';
  $_['text_success_notice'] = 'Тестовый запуск задачи прошёл успешно. Проверьте <a href="%s">event.log</a>';
  $_['text_error_notice'] = 'Тестовый запуск задачи был прерван ошибкой сервера';


  $_['error_permission'] = 'У Вас нет прав на изменения задачи';
  $_['error_empty_action'] = 'Название метода не может быть пустым';
  $_['error_format_action'] = 'Неверный формат метода';
  $_['error_not_file_action'] = 'Такого файла нет';
  $_['error_datetime_run'] = 'Дата не может быть пустаня';
  $_['error_frequency'] = 'Выберите переодичность';
  $_['error_method_not_found'] = 'Метода %s не существует в файле %s';

  $_['button_run'] = 'Выполнить сейчас';
 ?>
