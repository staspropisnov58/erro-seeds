<?php
// Heading
$_['heading_title']    = 'Стикера для товаров';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';
$_['text_all_time']    = 'За все время';
$_['text_last_year']   = 'За последний год';
$_['text_last_mounth'] = 'За последний месяц';
$_['text_categories'] = 'Категории для товаров со стикерами';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_product_stickers_new_date'] = 'Товар считается новинкой с...';
$_['entry_select_date'] = 'Укажите дату';
$_['entry_stickers_hit_limit'] = 'Количество "ХИТ" товаров';
$_['entry_hit_sales_period'] = 'Выбрать самые продаваемые товары';
$_['entry_hit_category'] = 'Добавить все товары со стикером "ХИТ" в категорию';
$_['entry_new_category'] = 'Добавить все новинки в категорию';
$_['entry_recommended_category'] = 'Добавить все рекомендуемые товары в категорию';

//Button

$_['button_update_new_items'] = 'Обновить новинки';
$_['button_update_hit_period'] = 'Обновить период';

// Error
$_['error_permission'] = 'У вас нет прав для управления данным модулем!';

$_['sticker_update_success'] = 'Стикеры обновлены. Посмотреть все товары со стикерами вы можете <a href="%s">здесь</a>';
$_['warning_category'] = 'Все товары без стикеров будут удалеы из категории';
