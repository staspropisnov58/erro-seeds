<?php
// Heading
$_['heading_title']    = 'Категория';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль категории успешно изменен!';
$_['text_edit']        = 'Изменить модуль категории';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_category'] = 'Категории';

//Help
$_['help_category'] = '(Автозаполнение)';

// Error
$_['error_permission'] = 'У вас нет прав для изменения модуля категории!';
