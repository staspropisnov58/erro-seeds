<?php
// Heading
$_['heading_title']    = 'Подключить html-файл как модуль';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_name'] = 'Название модуля';
$_['entry_file_name'] = 'Название файла в папке catalog/view/theme/%s/module/outer_html/';

// Error
$_['error_permission'] = 'У Вас нет прав для управления даннным модулем!';
$_['error_name'] = 'Название модуля должно содержать от 3 до 64 символов!';
$_['error_file_name'] = 'Указанный файл не найден в папке catalog/view/theme/%s/module/outer_html/';
