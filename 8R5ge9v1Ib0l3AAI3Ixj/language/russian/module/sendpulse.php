<?php
// Heading
$_['heading_title']    = 'СМС модуль SendPulse';

$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_sms_api']                = 'Строка запроса';

// Error
$_['error_permission'] = 'У Вас нет прав для изменения модуля SendPulse!';
