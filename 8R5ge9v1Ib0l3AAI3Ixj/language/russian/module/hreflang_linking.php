<?php
$_['heading_title']    = 'Связывание сайтов через hrerflang';

$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';
$_['text_enabled']     = 'Enabled';
$_['text_disabled']    = 'Disabled';


// Entry
$_['entry_commission']     = 'Комиссия';

// Error
$_['error_permission'] = 'У Вас нет прав для изменения модуля Связывание сайтов через hrerflang!';
$_['error_validate'] = 'Допускается вводить только числа!';
