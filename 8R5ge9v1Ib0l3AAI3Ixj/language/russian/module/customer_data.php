<?php
// Heading
$_['heading_title']    = 'Данные покупателей';

$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль данные покупателей был изменен!';
$_['text_edit']        = 'Редактировать модуль данные покупателей';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас нет прав на изменение модуля данные покупателей!';
