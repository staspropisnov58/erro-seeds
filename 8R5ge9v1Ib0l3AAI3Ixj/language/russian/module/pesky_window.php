<?php
// Heading
$_['heading_title']    = 'Всплывающее окно «Только что купили»';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_product']          = 'Товары';
$_['entry_first_timeout']    = 'Время в секундах, через которое окно появляется в первый раз, когда пользователь зашёл на сайт';
$_['entry_timeout']          = 'Время, через которое появляется окно выбирается случайным образом из промежутка, заданного минимальным и максимальным интервалом';
$_['entry_min_timeout']      = 'Минимальный интервал в минутах';
$_['entry_max_timeout']      = 'Максимальный интервал в минутах';
$_['entry_status']           = 'Статус';

// Help
$_['help_product']     = '(Автодополнение)';

// Error
$_['error_permission']    = 'У Вас нет прав для управления даннным модулем!';
$_['error_first_timeout'] = 'Введите целое число для времени первого появления в секундах';
$_['error_min_timeout']   = 'Введите целое число для минимального интервала в минутах';
$_['error_max_timeout']   = 'Введите целое число для максимального интервала в минутах';
