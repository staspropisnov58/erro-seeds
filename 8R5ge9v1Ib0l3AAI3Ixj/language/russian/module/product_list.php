<?php
// Heading
$_['heading_title']    = 'Список товаров';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';
$_['text_appearance']  = 'Внешний вид';
$_['text_main_stickers'] = 'Стикер к товару';
$_['text_make_choise'] = 'Выберите внешний вид';
$_['text_table']       = 'Таблица';
$_['text_vertical_slider'] = 'Вертикальный слайдер';

$_['text_success_cache'] = 'Кэш успешно обновлен.';

// Entry
$_['entry_name'] = 'Название модуля';
$_['entry_title'] = 'Название на витрине';
$_['entry_product']    = 'Товары';
$_['entry_limit']      = 'Лимит';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';
$_['entry_category']   = 'Категория';
$_['entry_attributes']	= 'Выводить атрибуты';

//Button
$_['button_update_cache'] = 'Обновить кэш и сохранить';

// Help
$_['help_product']     = '(Автодополнение)';
$_['help_main_stickers'] = 'Выбор стикера удаляет из списка все товары, у которых нет этого стикера';

// Error
$_['error_permission'] = 'У Вас нет прав для управления даннным модулем!';
$_['error_name'] = 'Название модуля должно содержать от 3 до 64 символов!';
$_['error_width'] = 'Введите ширину изображения!';
$_['error_height'] = 'Введите высоту изображения!';
