<?php
/**
* @version     2.1
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

// Heading
$_['heading_title']                   = 'AJAX-ZOOM';

// Text
$_['text_button_edit']                = 'Редактировать';
$_['text_button_save']                = 'Сохранить';
$_['text_button_cancel']              = 'Отменить';
$_['text_home']                       = 'Домой';
$_['text_module']                     = 'Модули';
$_['text_products']                   = 'ТОвары';
$_['text_360views']                   = '360/3D Показ';
$_['text_success']                    = 'Успех AJAX-ZOOM изменен! ';
$_['entry_status']                    = 'Статус модуля';
$_['button_clear']                    = 'Очистить';

$_['text_manage_360_views']           = 'Управление видами 360 / 3D';
$_['text_title_ax_settings']          = 'AJAX-ZOOM Настройки';
$_['text_ax_table_domain']            = 'Домен';
$_['text_ax_table_type']              = 'Тип лицензии';
$_['text_ax_table_key']               = 'Ключ Лицензии';
$_['text_ax_table_error200']          = 'Ошибка200';
$_['text_ax_table_error300']          = 'Ошибка300';
$_['text_ax_btn_add_license']         = 'Добавить лицензию';
$_['text_ax_ask_supprt']              = 'Спросить у поддержки';
$_['text_ax_buy_license']             = 'Купить лицензию';
$_['text_ax_title_images']            = 'Изображения';
$_['text_ax_filesize1']               = 'Формат JPG, GIF, PNG. Размер файла';
$_['text_ax_filesize2']               = 'MB max.';
$_['text_ax_title_edit_image']        = 'Измеить изображения товаров:';
$_['text_ax_title_add_image']         = 'Добавить новое изображение в этот набор изображений';
$_['text_ax_btn_delete_image']        = 'Удалить это изображение';
$_['text_ax_btn_cancel']              = 'Отмена';
$_['text_ax_title_3d_views']          = '360/3D Показ';
$_['text_ax_you_sure']                = 'Вы уверенны?';
$_['text_ax_btn_upload_image']        = 'Загрузите изображение';
$_['text_ax_image_added']             = 'Изображение было успешно добавлено';
$_['text_ax_table_image']             = 'Изображение';
$_['text_ax_table_name']              = 'Название';
$_['text_ax_table_model']             = 'модель';
$_['text_ax_table_360views']          = '3D/360 Показ';
$_['text_ax_table_action']            = 'Действие';
$_['text_ax_btn_help_manage360']      = 'Управление просмотром 3D / 360 для этого продукта';
$_['text_ax_no_products']             = 'Товаров не найдено';
$_['text_ax_title_add_360']           = 'Добавить новый вид 360 / 3D';
$_['text_ax_title_create_new']        = 'Создать новый';
$_['text_ax_tip_create_new']          = 'Пожалуйста, введите любое имя';
$_['text_ax_or']                      = 'Или';
$_['text_ax_title_add_existing']      = 'Добавить к существующему 3D в качестве следующей строки';
$_['text_ax_tip_add_existing']        = 'Вы не должны ничего выбирать здесь, если вы не хотите создать 3D (не 360), который содержит более одной строки!';
$_['text_ax_select']                  = 'Выбрать';
$_['text_ax_replacement']             = 'Вы не должны ничего выбирать здесь, если вы не хотите создать 3D (не 360), который содержит более одной строки!';
$_['text_ax_title_add_zip']           = 'Добавить изображения из ZIP-архива или папки';
$_['text_ax_tip_add_zip']             = 'Это самый простой и быстрый способ добавить 360 просмотров к вашему продукту! Загрузите по FTP ваши zip-архивы (каждое изображение установлено в один zip-файл) в каталог %s zip. Вы также можете загрузить папку со своими изображениями 360°. После того, как вы это сделаете, имена этих zip-файлов/папок мгновенно появятся в поле выбора ниже. Все, что вам нужно сделать, это выбрать одно из имен zip-файлов/папок и нажать кнопку «Добавить». Изображения из выбранного zip-файла/папки будут немедленно импортированы.';
$_['text_ax_select_tip']              = 'Выберите ZIP-архив или папку';
$_['text_ax_title_delete_zip']        = 'Удалить Zip архив/папку после импорта';
$_['text_ax_tip_delete_zip']          = 'Удалить Zip архив/папку после импорта';
$_['text_ax_num_sets']                 = 'Количество комплектов';
$_['text_ax_add_set']                  = 'Добавить';
$_['text_ax_table_cover']              = 'Изображение на обложке';
$_['text_ax_table_active']             = 'Активный';
$_['text_ax_yes']                      = 'Да';
$_['text_ax_no']                       = 'Нет';
$_['text_ax_btn_delete']               = 'Удалить';
$_['text_ax_btn_images']               = 'Изображения';
$_['text_ax_btn_preview']              = 'Предпросмотр';
$_['text_ax_btn_product_tour']         = '360 "Обзор товара"';
$_['text_ax_btn_hotspots']             = 'Горячие точки"';
$_['text_ax_header_settings']          = 'Настройки';
$_['text_ax_enable_axzoom']            = 'Включить/Выключить AJAX-ZOOM для этого товара';
$_['text_ax_enable']                   = 'Включить';
$_['text_ax_disable']                  = 'Выключить';
$_['text_ax_settings_existing']        = 'Настройки длясуществующих 360/3D';
$_['text_ax_select_3d_view']           = 'Выберите вид 360/3D';
$_['text_ax_title_settings']           = 'Настройки';
$_['text_ax_table_value']              = 'Значение';
$_['text_ax_add_option']               = 'Добавить вариант';
$_['text_ax_edit_textarea']            = 'РЕдактировать в Textarea';
$_['text_ax_options_info']             = 'AJAX-ZOOM есть несколько сотен вариантов. Некоторые из них могут быть установлены динамически через JS, а здесь другие должны быть установлены в файле конфигурации. Наиболее важные из них уже перечислены выше, но при необходимости вы можете добавить больше! Дополнительные параметры, непосредственно связанные с 360, смотри в AJAX-ZOOM.';
$_['text_ax_doc']                      = 'Документация';
$_['text_ax_variants']                 = 'Варианты';
$_['text_ax_check_all']                = 'Проверить все';
$_['text_ax_all']                      = 'Все';
$_['text_ax_variants_info']            = 'Как и в случае с изображениями, вы можете определить, какие 360 градусов должны отображаться вместе с какими комбинациями. Если вы не выберете ни одного, эти 360 будут показаны для всех комбинаций.';
$_['text_ax_btn_save']                 = 'Сохранить';
$_['text_ax_btn_save_settings']        = 'Сохранить настройки';

// Categories
$_['text_category_license']            = 'Лицензия';
$_['text_category_general']            = 'Главные настройки';
$_['text_category_mouseOverZoomParam'] = 'Конкретные параметры увеличения при наведении курсора';
$_['text_category_pinterest']          = 'Настройки для кнопки pinterest';
$_['text_category_displayOnly']        = 'Отображать только в этих товарах';

// Configuration
// english values gets from admin/controller/module/ajaxzoom.php getFieldList() function
// to translate config title/comment for other languages add the array values as shown below
// for title $_['text_title_KEY']
// for comments $_['text_comment_KEY'])
$_['text_title_ajaxzoom_DIVID']        = 'divID';
$_['text_comment_ajaxzoom_DIVID']      = 'DIV (контрейнер) ID для увеличения при наведении курсора :-)';

// Error
$_['error_permission']                 = 'Внимание: У вас нет разрешения на изменение модуля AJAX-ZOOM!';
