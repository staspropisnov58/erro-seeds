<?php
// Heading
$_['heading_title']     = 'Всплывающее окно';

// Text
$_['text_module']       = 'Модули';
$_['text_success']      = 'Модуль «Всплывающее окно» успешно изменен!';
$_['text_edit']         = 'Редактировать модуль «Всплывающее окно»';

// Entry
$_['entry_heading']     = 'Заголовок';
$_['entry_description'] = 'Код';
$_['entry_status']      = 'Статус';
$_['entry_name']        = 'Название модуля';
$_['entry_title']       = 'Заголовок';
$_['entry_button_yes']  = 'Кнопка «Да»';
$_['entry_button_no']   = 'Кнопка «Нет»';
$_['entry_for_all']     = 'Отображать на всех страницах';


// Error
$_['error_permission']  = 'У вас нет прав для изменения модуля «Всплывающее окно»!';
$_['error_module']      = 'Внимание: Требуется указать модуль!';
$_['error_name']        = 'Название модуля должно быть от 3 до 64 символов';
$_['error_button_yes']  = 'Текст для кнопки должен быть от 2 до 80 символов';
$_['error_button_no']   = 'Текст для кнопки должен быть от 2 до 80 символов';
