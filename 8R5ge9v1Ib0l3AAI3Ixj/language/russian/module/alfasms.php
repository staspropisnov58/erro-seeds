<?php
// Heading
$_['heading_title']    = 'СМС модуль AlfaSms';

$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_sms_login']                  = 'Логин';
$_['entry_sms_password']               = 'Пароль';
$_['entry_sms_from']                   = 'Отправитель';
$_['entry_sms_request']                = 'Строка запроса';
// Error
$_['error_permission'] = 'У Вас нет прав для изменения модуля AlfaSms!';
