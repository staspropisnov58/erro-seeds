<?php
// Heading
$_['heading_title']     = 'Дополнения к заказам';

// Text
$_['text_module']       = 'Модули';
$_['text_success']      = 'Настройки успешно изменены!';
$_['text_edit']         = 'Настройки дополнений';
$_['text_dependencies']['shipping']        = 'Только к заказам с определённой доставкой';
$_['text_dependencies']['category_strict'] = 'Только при заказе товаров из определённой категории';
$_['text_blocks']['shipping'] = 'Доставка';
$_['text_blocks']['cart']     = 'Корзина';

$_['text_dependency_shipping'] = 'Способы доставки';
$_['text_dependency_category_strict'] = 'Категории товаров';

// Entry
$_['entry_name'] = 'Название модуля';
$_['entry_title']     = 'Заголовок';
$_['entry_description'] = 'Текст';
$_['entry_status']      = 'Статус';
$_['entry_dependencies'] = 'Зависимости (дополнение будет видимо в корзине)';
$_['entry_block'] = 'Отображать после блока';
$_['entry_product_description'] = 'Показывать описание товара';

// Error
$_['error_permission']  = 'У Вас нет прав для управления данным модулем!';
$_['error_name'] = 'Название дополнения должно содержать от 3 до 64 символов!';
