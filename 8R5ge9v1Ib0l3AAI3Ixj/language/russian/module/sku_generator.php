<?php
// Heading
$_['heading_title']    = 'Настройка и генерация артикулов';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль категории успешно изменен!';
$_['text_edit']        = 'Изменить модуль категории';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_sku_generator'] = 'Категории';

//Help
$_['help_sku_generator'] = '(Автозаполнение)';

// Error
$_['error_permission'] = 'У вас нет прав для настройки и генерации артикулов!';
