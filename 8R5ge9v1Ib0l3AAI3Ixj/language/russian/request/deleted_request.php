<?php
//Title
$_['heading_title'] = 'Удаленные заявки';

//Entry
$_['entry_name'] = 'Название товара';
$_['entry_email'] = 'E-mail';
$_['entry_date_added'] = 'Дата добавления';
$_['enrty_date_expired'] = 'Крайний срок';
$_['entry_quantity'] = 'Количество';
$_['entry_date_deleted'] = 'Дата удаления';

//Text
$_['text_form'] = 'Удаленные заявки';
$_['text_guest'] = 'Гость';
$_['text_no_name'] = 'Имя не указано';
$_['text_out_of_sale'] = 'Товара нет на складе';
$_['text_insufficient_amount'] = 'Не хватает количества';
$_['text_reason_not_clear'] = 'Причина нехватки товара не ясна';
$_['text_product_out_of_sale'] = 'Товар снят с продажи';
$_['text_invalid_email'] = 'Email не существует';
$_['text_outdated_request'] = 'Заявка просрочена';

//Column
$_['column_email'] = 'Email:';
$_['column_date_deleted'] = 'Дата удаления:';
$_['column_date_added'] = 'Дата поступления:';
$_['column_date_expired'] = 'Актуальна до:';
$_['column_product_name'] = 'Товар(название, модель, артикул)';
$_['column_date_deleted'] = 'Дата удваления:';
$_['column_quantity'] = 'Количество';
$_['column_customer'] = 'Клиент';
$_['column_reason'] = 'Причина';

//Button
$_['button_filter'] = 'Фильтр';
