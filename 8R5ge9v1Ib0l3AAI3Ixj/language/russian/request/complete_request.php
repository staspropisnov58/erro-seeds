<?php
//Title
$_['heading_title'] = 'Обработанные заявки';

//Entry

$_['entry_name'] = 'Название товара';
$_['entry_email'] = 'E-mail';
$_['entry_date_complete'] = 'Дата отправки';

//Text
$_['text_form'] = 'Обработанные заявки';
$_['text_guest'] = 'Гость';
$_['text_no_name'] = 'Имя не указано';
$_['text_out_of_stock'] = 'Товара нет на складе';
$_['text_insufficient_amount'] = 'Не хватает количества';
$_['text_reason_not_clear'] = 'Причина нехватки товара не ясна';

//Column
$_['column_email'] = 'Email:';
$_['column_date_complete'] = 'Дата отправки:';
$_['column_date_expired'] = 'Актуальна до:';
$_['column_product_name'] = 'Товар(название, модель, артикул)';
$_['column_quantity'] = 'Количество';
$_['column_customer'] = 'Клиент';
$_['column_reason'] = 'Причина';
