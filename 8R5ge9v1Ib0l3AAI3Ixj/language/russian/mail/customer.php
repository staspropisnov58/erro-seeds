<?php
// Text
$_['text_approve_subject']      = '%s - Ваш аккаунт был активирован!';
$_['text_approve_welcome']      = 'Добро пожаловать и благодарим Вас за регистрацию в %s!';
$_['text_approve_login']        = 'Ваш аккаунт создан и Вы можете войти используя Ваши E-mail и пароль, посетив наш сайт по адресу:';
$_['text_approve_services']     = 'После регистрации на сайте, Вы сможете воспользоваться дополнительными возможностями: просмотр истории заказов, печать счета, изменение информации Вашей учетной записи и др.';
$_['text_approve_thanks']       = 'Спасибо,';
$_['text_transaction_subject']  = '%s - Счет аккаунта';
$_['text_transaction_received'] = 'Вы получили на счет: %s';
$_['text_transaction_total']    = 'Общая сумма на Вашем аккаунте: %s.' . "\n\n" . 'Эта сумма будет автоматически расходоваться при следующих покупках.';
$_['text_reward_subject']       = '%s - Бонусные баллы';
$_['text_reward_received']      = 'Вы получили %s бонусных баллов!';
$_['text_reward_total']         = 'Общее число бонусных баллов %s.';
$_['text_hello']                = 'Здравствуйте, %s!';
$_['theme_review_moderated']    = 'Ваш отзыв прошел модерацию';
$_['text_review_moderated']     = 'Ваш отзыв к товару "%s", прошёл модерацию.';
$_['text_review_moderated_x1']  = 'Посмотреть его вы можете нажав по этой <a href="%s">ссылке</a>';
$_['text_footer']               = 'С уважением, %s';
$_['text_answear_comment']      = '%s написал комментарий к вашему комментарию';
$_['theme_answer_comment']      = 'Новый ответ';
$_['text_news_moderated']       = 'Ваш отзыв к новости: "%s", прошёл модерацию.';
