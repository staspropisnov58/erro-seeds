<?php
// Text
$_['text_approve_subject']      = '%s - Ваш партнерский аккаунт активирован!';
$_['text_approve_welcome']      = 'Добро пожаловать и спасибо за регистрацию на %s!';
$_['text_approve_login']        = 'Ваш аккаунт создан и Вы можете войти используя Ваш E-mail и пароль, посетив наш сайт по адресу:';
$_['text_approve_services']     = 'После входа, Вы сможете получить адрес партнерской ссылки.';
$_['text_approve_thanks']       = 'Спасибо,';
$_['text_transaction_subject']  = '%s - Партнерское вознаграждение.';
$_['text_transaction_received'] = 'Вы получили %s!';
$_['text_transaction_total']    = 'Общая сумма Ваших партнерских вознаграждений %s.';

$_['text_delete_payout'] = 'Были удалены Ваши бонусы в размере: %s';

$_['text_hello']              = 'Добрый день, %s!';
$_['text_add_payout'] = 'Вам были начислены бонусы в размере: %s';
$_['complete_order'] = 'Вам был начислен бонус за заказ по Вашей реферальной ссылке. Проверить сумму бонусов и выплат Вы всегда можете в своём личном кабинете на нашем сайте <a href="%s"> %s </a>.';
$_['complete_order_next_1'] = 'Спасибо за участие в нашей реферальной программе! Оставайтесь с нами!';


$_['theme_complete_order'] = 'Вам начислен бонус за заказ(ы)';

$_['text_footer'] = 'С уважением, %s';
$_['theme_payout'] = 'Произведена выплата';
$_['text_bonuses'] = 'Вам была произведена выплата бонусов за заказы по Вашей реферальной ссылке. Проверить сумму бонусов, а также историю выплат Вы можете в личном кабинете на сайте <a href="%s"> %s </a>.';
$_['text_bonuses_x2'] = 'Поздравляем Ваш процент по заказам по рефферальной программе вырос до %s%';
$_['text_bonuses_x1'] = 'Спасибо за участие в нашей реферальной программе! Оставайтесь с нами!';

$_['text_subject'] = 'Вы успешно зарегестрированы.';
