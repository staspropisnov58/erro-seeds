<?php
$_['heading_title'] = 'Ответ на отзыв прошёл модерацию';
$_['text_mail_notify'] = 'Email уведомления';
$_['text_test'] = 'Тестирование отправки';
$_['text_wiev_latter'] = 'Просмотреть письмо';
$_['text_send_test_latter'] = 'Отправить тестовое письмо';
$_['text_search'] = 'Поиск отзыва по id';
$_['text_answer'] = 'Поиск ответа на отзыв по id';
$_['entry_mail'] = 'Введите Email';
$_['title_for_preview'] = 'Ответ на отзыв с id %s';
$_['text_mail_success'] = 'Сообщение успешно отправленно на email: %s';
$_['entry_language'] = 'Выберите язык';
