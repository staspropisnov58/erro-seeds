<?php
$_['heading_title']      = 'Отзывы | %s';
$_['heading_title_add']  = 'Добавить отзыв';
$_['heading_title_edit'] = 'Редактировать отзыв';
$_['heading_title_info'] = 'Отзыв | %s';

$_['text_list']         = 'Список отзывов';
$_['text_add']          = 'Добавить';
$_['text_edit']         = 'Редактировать';
$_['text_product']      = 'Товары';
$_['text_article']      = 'Новости';
$_['text_store']        = 'Магазин';
$_['text_discussion']   = 'Дискуссия';

$_['column_author']            = 'Автор';
$_['column_rating']            = 'Рейтинг';
$_['column_status']            = 'Статус';
$_['column_answers']          = 'Всего комментариев';
$_['column_disabled_answers'] = 'Выключенные комментарии';
$_['column_date_added']        = 'Дата добавления';
$_['column_action']            = 'Просмотр дискуссии';

$_['entry_author']       = 'Автор';
$_['entry_rating']       = 'Рейтинг';
$_['entry_status']       = 'Статус';
$_['entry_text']         = 'Текст отзыва или комментария';
$_['entry_date_added']   = 'Дата добавления';
$_['entry_product']      = 'Товар';
$_['entry_article']      = 'Новость';
