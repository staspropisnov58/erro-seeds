<?php
class ModelMarketingSaleEvents extends Model {

  public function getSaleEvents($data = array()){

    $sql = "SELECT st.name as type_name, sed.name, se.sale_event_id, se.datetime_start, se.datetime_end, se.sale_event_setting FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON (sed.sale_event_id = se.sale_event_id) LEFT JOIN " . DB_PREFIX . "sale_event_type st ON (st.sale_event_type_id = se.sale_event_type_id) WHERE sed.language_id = '" . (int)$this->config->get('config_language_id') . "' AND NOW() BETWEEN se.datetime_start AND se.datetime_end";

    $implode = array();

    if (!empty($data['filter_name'])) {
      $implode[] = "sed.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
    }

    if (!empty($data['filter_type_name'])) {
      $implode[] = "st.name LIKE '%" . $this->db->escape($data['filter_type_name']) . "%'";
    }

    if (!empty($data['filter_datetime_start'])) {
      $implode[] = "DATE(se.datetime_start) = DATE('" . $this->db->escape($data['filter_datetime_start']) . "')";
    }

    if (!empty($data['filter_datetime_end'])) {
      $implode[] = "DATE(se.datetime_end) = DATE('" . $this->db->escape($data['filter_datetime_end']) . "')";
    }

    if ($implode) {
      $sql .= " AND " .  implode(" AND ", $implode);
    }

    $first = $this->db->query($sql);

    $sql = "SELECT st.name as type_name, sed.name, se.sale_event_id, se.datetime_start, se.datetime_end, se.sale_event_setting FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON (sed.sale_event_id = se.sale_event_id) LEFT JOIN " . DB_PREFIX . "sale_event_type st ON (st.sale_event_type_id = se.sale_event_type_id) WHERE sed.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

    $implode = array();

    if(isset($first) && $first->num_rows !== 0 ){
      $implode[] = "se.sale_event_id NOT IN (" . implode("," , array_column($first->rows, 'sale_event_id')) . ")";
    }

    if (!empty($data['filter_name'])) {
      $implode[] = "sed.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
    }

    if (!empty($data['filter_type_name'])) {
      $implode[] = "st.name LIKE '%" . $this->db->escape($data['filter_type_name']) . "%'";
    }

    if (!empty($data['filter_datetime_start'])) {
      $implode[] = "DATE(se.datetime_start) = DATE('" . $this->db->escape($data['filter_datetime_start']) . "')";
    }

    if (!empty($data['filter_datetime_end'])) {
      $implode[] = "DATE(se.datetime_end) = DATE('" . $this->db->escape($data['filter_datetime_end']) . "')";
    }

    if ($implode) {
      $sql .= " AND " .  implode(" AND ", $implode);
    }

    $sort_data = array(
			'st.name',
			'sed.name',
			'se.datetime_start',
      'se.datetime_end'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY se.datetime_start";
		}

		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1 && isset($first) && $first->num_rows !== 0) {
				$data['limit'] = 20 - count($first->rows);
			}elseif($data['limit'] > 1 && isset($first) && $first->num_rows !== 0){
        $data['limit'] = $data['limit'] - count($first->rows);
      }elseif($data['limit'] < 1 && !isset($first)){
        $data['limit'] = 20;
      }

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

    $query = $this->db->query($sql);

    if(isset($first) && $first->num_rows !== 0){

      foreach($first->rows as &$value){
        $value['class'] = 'success';
      }
      $first = $first->rows;

      foreach($query->rows as $rows){
        $first[] = $rows;
      }

      $query->rows = $first;
    }

    return $query->rows;

  }

  public function getSaleEventForAdminPanel($sale_event_id){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON(se.sale_event_id = sed.sale_event_id) WHERE sed.language_id = '" . (int)$this->config->get("config_language_id") . "' AND se.sale_event_id = '" . (int)$sale_event_id . "'");

    return $query->row;
  }

  public function getSaleEventTypes(){
    $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sale_event_type");

    return $query->rows;
  }


  public function getProductsBySaleEventId($sale_event_id){
    $query = $this->db->query("SELECT pd.name, ps.percent, ps.sale_event_id, pd.product_id, p.price, ((1-ps.percent)* p.price) as special_price, p.model, p.tax_class_id FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product_description pd ON(ps.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pd.product_id) WHERE sale_event_id = '" . (int)$sale_event_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
    $products = array();

    if($query->num_rows){
      foreach ($query->rows as $product){
        $products[($product['percent'] * 100)]['products'][$product['product_id']]['name'] = $product['name'];
        $products[($product['percent'] * 100)]['products'][$product['product_id']]['href'] = $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], 'SSL');
        $products[($product['percent'] * 100)]['products'][$product['product_id']]['model'] = $product['model'];
        $products[($product['percent'] * 100)]['products'][$product['product_id']]['price'] = $this->currency->format($product['price'], $this->config->get('config_currency'));
        $products[($product['percent'] * 100)]['products'][$product['product_id']]['special_price'] = $this->currency->format($product['special_price'], $this->config->get('config_currency'));



        $options_query = $this->db->query("SELECT DISTINCT pov.product_option_value_id, pov.price AS old_price, povd.name, pos.price as new_price FROM `oc_product_option_value` pov LEFT JOIN oc_option_value_description povd ON (pov.option_value_id = povd.option_value_id) LEFT JOIN oc_option_value ov ON (pov.option_value_id = ov.option_value_id) INNER JOIN oc_product_option_special pos ON (pov.product_option_value_id = pos.product_option_value_id ) WHERE pov.product_id = '" . (int)$product['product_id'] . "' AND povd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        foreach($options_query->rows as &$option_query){
          $option_query['old_price'] = $this->currency->format($option_query['old_price'], $this->config->get('config_currency'));
          $option_query['new_price'] = $this->currency->format($option_query['new_price'], $this->config->get('config_currency'));

        }

        $products[($product['percent'] * 100)]['products'][$product['product_id']]['options'] = $options_query->rows;
      }
    }


    return $products;

  }

  public function getSaleEventNewsId($sale_event_id){
    $query = $this->db->query("SELECT DISTINCT news_id FROM " . DB_PREFIX . "news WHERE sale_event_id = '" . (int)$sale_event_id . "'");

    if($query->num_rows){
      return $query->row['news_id'];
    }else{
      return 0;
    }
  }

  public function getTotalSaleEvents($data = array()){

    $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON (sed.sale_event_id = se.sale_event_id) LEFT JOIN " . DB_PREFIX . "sale_event_type st ON (st.sale_event_type_id = se.sale_event_type_id) WHERE sed.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

		$implode = array();

    if (!empty($data['filter_name'])) {
      $implode[] = "sed.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
    }

    if (!empty($data['filter_type_name'])) {
      $implode[] = "st.name LIKE '%" . $this->db->escape($data['filter_type_name']) . "%'";
    }

    if (!empty($data['filter_datetime_start'])) {
      $implode[] = "DATE(se.datetime_start) = DATE('" . $this->db->escape($data['filter_datetime_start']) . "')";
    }

    if (!empty($data['filter_datetime_end'])) {
      $implode[] = "DATE(se.datetime_end) = DATE('" . $this->db->escape($data['filter_datetime_end']) . "')";
    }

    if ($implode) {
      $sql .= " AND " . implode(" AND ", $implode);
    }

		$query = $this->db->query($sql);

		return $query->row['total'];
  }

  public function getSaleEventDescription($sale_event_id){
		$sale_event_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_event_description WHERE sale_event_id = '" . (int)$sale_event_id . "'");

		foreach ($query->rows as $result) {
			$sale_event_description_data[$result['language_id']] = array(
				'name'       => $result['name'],
				'description' => $result['description'],
			);
		}

		return $sale_event_description_data;
	}

  public function getSaleEvent($sale_event_id){

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON (se.sale_event_id = sed.sale_event_id) WHERE se.sale_event_id = '" . (int)$sale_event_id . "' AND language_id = '" . $this->config->get("config_language_id") . "'");
      $sale_event_settings = unserialize($query->row['sale_event_setting']);

      if($sale_event_settings){
        foreach($sale_event_settings as $key => $sale_event_setting){

          $sql = "SELECT DISTINCT product_id, percent FROM " . DB_PREFIX . "product_special ps WHERE ps.sale_event_id = '" . (int)$sale_event_id . "' AND ps.percent = '" . $this->db->escape($sale_event_setting['persent']/100) . "' ";

          if(isset($sale_event_setting['category']) && $sale_event_setting['category']){
            $sql .= " AND product_id NOT IN (SELECT product_id FROM oc_product_to_category WHERE category_id IN (" . $this->db->escape(implode(",", $sale_event_setting['category'])) . ")) ";
          }

          if(isset($sale_event_setting['manufacturer']) && $sale_event_setting['manufacturer']){
            $sql .= " AND product_id NOT IN (SELECT product_id FROM oc_product WHERE manufacturer_id IN(" . $this->db->escape(implode(",", $sale_event_setting['manufacturer'])) . "))";
          }

          $sale_products = $this->db->query($sql);

          if($sale_products->num_rows){
            foreach($sale_products->rows as $sale_product){
              $sale_event_settings[$key]['product'][] = $sale_product['product_id'];
            }
          }else{
            $sale_event_settings[$key]['product']= array();
          }
          $query->row['sale_event_setting'] = $sale_event_settings;
        }
      }

		return $query->row;
  }

  public function editSaleEvents($sale_event_id, $data = array()){

    $this->db->query("DELETE FROM " . DB_PREFIX . "task WHERE args = CONCAT('sale_event_id='," . (int)$sale_event_id . ")");

    $this->load->model('catalog/product');

    $specials_id = $this->db->query("SELECT product_special_id FROM " . DB_PREFIX . "product_special WHERE sale_event_id = '" . (int)$sale_event_id . "'");


    foreach($specials_id->rows as $special_key => $special_id){
      $specials_id_array[] = $special_id['product_special_id'];
    }

    $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_special WHERE special_id IN (" . implode(',',$specials_id_array) . ")");

    $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE sale_event_id = '" . (int)$sale_event_id . "'");

    foreach($data['sale_event_settings'] as $key => $sale_event_settings){

      $data['sale_event_settings'][$sale_event_settings['persent']] = $sale_event_settings;
      unset($data['sale_event_settings'][$key]);
    }

    ksort($data['sale_event_settings']);
    $data['sale_event_settings'] = array_reverse($data['sale_event_settings'],true);

    foreach($data['sale_event_settings'] as $key => $sale_event_settings){
      if(isset($sale_event_settings['product'])){
        foreach($sale_event_settings['product'] as $product_id){
          foreach ($sale_event_settings['customer_group'] as $customer_group_id){

            $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', priority= '" . (int)$sale_event_settings['priority'] . "', price = ((SELECT price FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "') * (1 - (" . (float)$sale_event_settings['persent'] . ")/100)) , date_start = '" . $this->db->escape($data['datetime_start']) . "', date_end = '" . $this->db->escape($data['datetime_end']) . "', percent = '" . (float)($sale_event_settings['persent']/100) . "', sale_event_id = '" . (int)$sale_event_id . "', status = 1");
          }
        }
      }
      unset($data['sale_event_settings'][$key]['product']);
    }

    foreach($data['sale_event_settings'] as $sale_event_settings){
      foreach($sale_event_settings['category'] as $category_id){
        $products_category_id = array_column($this->model_catalog_product->getProductsByCategoryId($category_id), 'product_id');
        foreach($products_category_id as $product_id){
          foreach ($sale_event_settings['customer_group'] as $customer_group_id){

            $this->db->query("INSERT INTO oc_product_special ( product_id, customer_group_id, priority, price, date_start, date_end, percent, sale_event_id, status ) SELECT '" . (int)$product_id . "' as product_id, '" . (int)$customer_group_id . "' as customer_group_id, '" . (int)$sale_event_settings['priority'] . "' as priority, (1 - (" . $sale_event_settings['persent'] . "/100))*price as price, '" . $this->db->escape($data['datetime_start']) . "' as date_start, '" . $this->db->escape($data['datetime_end']) . "' as date_end, '" . (float)($sale_event_settings['persent']/100) . "' as percent, '" . (int)$sale_event_id . "' as sale_event_id, 1 as status FROM oc_product WHERE product_id = '" . (int)$product_id . "' AND NOT EXISTS ( SELECT customer_group_id, sale_event_id, product_id FROM oc_product_special WHERE customer_group_id = '" . (int)$customer_group_id . "' AND sale_event_id = '" . (int)$sale_event_id . "' AND product_id = '" . (int)$product_id . "' )");

          }
        }
      }
    }

    foreach($data['sale_event_settings'] as $sale_event_settings){
      foreach($sale_event_settings['manufacturer'] as $manufacturer_id){
        $products_manufacturer_id = array_column($this->model_catalog_product->getProducts(['filter_manufacturer_id' => $manufacturer_id, 'limit' => 'all']), 'product_id');
        foreach($products_manufacturer_id as $product_id){
          foreach ($sale_event_settings['customer_group'] as $customer_group_id){

            $this->db->query("INSERT INTO oc_product_special ( product_id, customer_group_id, priority, price, date_start, date_end, percent, sale_event_id, status ) SELECT '" . (int)$product_id . "' as product_id, '" . (int)$customer_group_id . "' as customer_group_id, '" . (int)$sale_event_settings['priority'] . "' as priority, (1 - (" . $sale_event_settings['persent'] . "/100))*price as price, '" . $this->db->escape($data['datetime_start']) . "' as date_start, '" . $this->db->escape($data['datetime_end']) . "' as date_end, '" . (float)($sale_event_settings['persent']/100) . "' as percent, '" . (int)$sale_event_id . "' as sale_event_id, 1 as status FROM oc_product WHERE product_id = '" . (int)$product_id . "' AND NOT EXISTS ( SELECT customer_group_id, sale_event_id, product_id FROM oc_product_special WHERE customer_group_id = '" . (int)$customer_group_id . "' AND sale_event_id = '" . (int)$sale_event_id . "' AND product_id = '" . (int)$product_id . "' )");

          }
        }
      }
    }

    foreach($data['sale_event_settings'] as $sale_event_settings){
      $sql = "INSERT INTO " . DB_PREFIX . "product_option_special (product_id, special_id, product_option_value_id, price) SELECT ps.product_id, ps.product_special_id, pov.product_option_value_id, ((1-ps.percent) * pov.price)  FROM " . DB_PREFIX . "product_special ps INNER JOIN oc_product_option_value pov ON (ps.product_id = pov.product_id) WHERE sale_event_id = '" . (int)$sale_event_id . "' ";
      if(isset($sale_event_settings['option'])){

        $sql .= " AND pov.option_value_id NOT IN (" . implode(",",$sale_event_settings['option']) .")";

      }

      $this->db->query($sql);

    }

    //Проверяем Дату и записываем логи если она или совпадает или больше

    $this->db->query("UPDATE " . DB_PREFIX . "sale_event SET datetime_start = '" . $this->db->escape($data['datetime_start']) . "', datetime_end = '" . $this->db->escape($data['datetime_end']) . "', sale_event_type_id = '" . (int)$data['type'] . "', disable_coupon = '" . $this->db->escape(serialize($data['disable_coupon'])) .  "', sale_event_setting = '" . $this->db->escape(serialize($data['sale_event_settings'])) . "' WHERE sale_event_id = '" . (int)$sale_event_id . "'");

    $this->db->query("DELETE FROM " . DB_PREFIX . "sale_event_description WHERE sale_event_id = '" . (int)$sale_event_id . "'");

    foreach ($data['sale_event_description'] as $language_id => $sale_event_description){

      $this->db->query("INSERT INTO " . DB_PREFIX . "sale_event_description SET name = '" . $this->db->escape($sale_event_description['name']) . "', description = '" . $this->db->escape($sale_event_description['description']) . "', language_id = '" . (int)$language_id . "', sale_event_id = '" . (int)$sale_event_id . "'");

    }

    if(isset($data['sale_event_news'])){
      $this->load->model('extension/news');
      $this->model_extension_news->editNewsSaleEventId($sale_event_id, $data['sale_event_news']);
    }

    if(isset($data['banner_image_radio']) && $data['banner_image_radio']!== 0 ){
      $this->load->model('design/banner');
      $this->model_design_banner->editBannerSaleEventId($sale_event_id, $data['banner_image_radio']);
    }

    if($data['disable_coupon']){
      foreach($data['disable_coupon'] as $coupon_id){
        $this->db->query("UPDATE " . DB_PREFIX . "coupon c1 LEFT JOIN " . DB_PREFIX . "coupon c2 ON (c2.coupon_id = c1.coupon_id) SET c1.status = 0 WHERE NOW() >= c2.date_start AND NOW() < c2.date_end AND c2.coupon_id = '" . (int)$coupon_id . "'");
      }
    }


    if($data['datetime_start'] > date("Y-m-d H:i")) {
      $this->db->query("INSERT INTO " . DB_PREFIX . "task SET datetime_run = '" . $data['datetime_start'] . "', action = 'marketing/sale_events/enableSaleEvent', args = CONCAT('sale_event_id=', " . (int)$sale_event_id . "), frequency = 'once'");
      $this->db->query("INSERT INTO " . DB_PREFIX . "task SET datetime_run = '" . $data['datetime_end'] . "', action = 'marketing/sale_events/disableSaleEvent', args = CONCAT('sale_event_id=', " . (int)$sale_event_id . "), frequency = 'once'");
    }
  }

  public function getSaleEventType($sale_event_type_id){

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_event_type WHERE sale_event_type_id = '" . (int)$sale_event_type_id . "'");

    return $query->rows;
  }

  public function addSaleEvents($data = array()){

    $this->db->query("INSERT INTO " . DB_PREFIX . "sale_event SET datetime_start = '" . $this->db->escape($data['datetime_start']) . "', datetime_end = '" . $this->db->escape($data['datetime_end']) . "', sale_event_type_id = '" . (int)$data['type'] . "', disable_coupon = '" . $this->db->escape(serialize($data['disable_coupon'])) .  "', sale_event_setting = '" . $this->db->escape(serialize($data['sale_event_settings'])) . "'");

    $sale_event_id = $this->db->getLastId();

    $this->load->model('catalog/product');


    foreach($data['sale_event_settings'] as $key => $sale_event_settings){
      $data['sale_event_settings'][$sale_event_settings['persent']] = $sale_event_settings;
      unset($data['sale_event_settings'][$key]);
    }

    ksort($data['sale_event_settings']);
    $data['sale_event_settings'] = array_reverse($data['sale_event_settings'],true);

    foreach($data['sale_event_settings'] as $key => $sale_event_settings){
      if(isset($sale_event_settings['product'])){
        foreach($sale_event_settings['product'] as $product_id){
          foreach ($sale_event_settings['customer_group'] as $customer_group_id){

            $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', priority= '" . (int)$sale_event_settings['priority'] . "', price = ((SELECT price FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "') * (1 - (" . (float)$sale_event_settings['persent'] . ")/100)) , date_start = '" . $this->db->escape($data['datetime_start']) . "', date_end = '" . $this->db->escape($data['datetime_end']) . "', percent = '" . (float)($sale_event_settings['persent']/100) . "', sale_event_id = '" . (int)$sale_event_id . "', status = 1");
          }
        }
      }

      unset($data['sale_event_settings'][$key]['product']);
    }

    foreach($data['sale_event_settings'] as $sale_event_settings){
      foreach($sale_event_settings['category'] as $category_id){
        $products_category_id = array_column($this->model_catalog_product->getProductsByCategoryId($category_id), 'product_id');
        foreach($products_category_id as $product_id){
          foreach ($sale_event_settings['customer_group'] as $customer_group_id){

            $this->db->query("INSERT INTO oc_product_special ( product_id, customer_group_id, priority, price, date_start, date_end, percent, sale_event_id, status ) SELECT '" . (int)$product_id . "' as product_id, '" . (int)$customer_group_id . "' as customer_group_id, '" . (int)$sale_event_settings['priority'] . "' as priority, (1 - (" . $sale_event_settings['persent'] . "/100))*price as price, '" . $this->db->escape($data['datetime_start']) . "' as date_start, '" . $this->db->escape($data['datetime_end']) . "' as date_end, '" . (float)($sale_event_settings['persent']/100) . "' as percent, '" . (int)$sale_event_id . "' as sale_event_id, 1 as status FROM oc_product WHERE product_id = '" . (int)$product_id . "' AND NOT EXISTS ( SELECT customer_group_id, sale_event_id, product_id FROM oc_product_special WHERE customer_group_id = '" . (int)$customer_group_id . "' AND sale_event_id = '" . (int)$sale_event_id . "' AND product_id = '" . (int)$product_id . "' )");

          }
        }
      }
    }

    foreach($data['sale_event_settings'] as $sale_event_settings){
      foreach($sale_event_settings['manufacturer'] as $manufacturer_id){
        $products_manufacturer_id = array_column($this->model_catalog_product->getProducts(['filter_manufacturer_id' => $manufacturer_id, 'limit' => 'all']), 'product_id');
        foreach($products_manufacturer_id as $product_id){
          foreach ($sale_event_settings['customer_group'] as $customer_group_id){

            $this->db->query("INSERT INTO oc_product_special ( product_id, customer_group_id, priority, price, date_start, date_end, percent, sale_event_id, status ) SELECT '" . (int)$product_id . "' as product_id, '" . (int)$customer_group_id . "' as customer_group_id, '" . (int)$sale_event_settings['priority'] . "' as priority, (1 - (" . $sale_event_settings['persent'] . "/100))*price as price, '" . $this->db->escape($data['datetime_start']) . "' as date_start, '" . $this->db->escape($data['datetime_end']) . "' as date_end, '" . (float)($sale_event_settings['persent']/100) . "' as percent, '" . (int)$sale_event_id . "' as sale_event_id, 1 as status FROM oc_product WHERE product_id = '" . (int)$product_id . "' AND NOT EXISTS ( SELECT customer_group_id, sale_event_id, product_id FROM oc_product_special WHERE customer_group_id = '" . (int)$customer_group_id . "' AND sale_event_id = '" . (int)$sale_event_id . "' AND product_id = '" . (int)$product_id . "' )");

          }
        }
      }
    }

    foreach($data['sale_event_settings'] as $sale_event_settings){
      $sql = "INSERT INTO " . DB_PREFIX . "product_option_special (product_id, special_id, product_option_value_id, price) SELECT ps.product_id, ps.product_special_id, pov.product_option_value_id, ((1-ps.percent) * pov.price)  FROM " . DB_PREFIX . "product_special ps INNER JOIN oc_product_option_value pov ON (ps.product_id = pov.product_id) WHERE sale_event_id = '" . (int)$sale_event_id . "' ";
      if(isset($sale_event_settings['option'])){

        $sql .= " AND pov.option_value_id NOT IN (" . implode(",",$sale_event_settings['option']) .")";

      }

      $this->db->query($sql);

    }


    if(isset($data['sale_event_news'])){
      $this->load->model('extension/news');
      $this->model_extension_news->editNewsSaleEventId($sale_event_id, $data['sale_event_news']);
    }

    if(isset($data['banner_image_radio']) && $data['banner_image_radio']!== 0 ){
      $this->load->model('design/banner');
      $this->model_design_banner->editBannerSaleEventId($sale_event_id, $data['banner_image_radio']);
    }

    foreach ($data['sale_event_description'] as $language_id => $sale_event_description){

      $this->db->query("INSERT INTO " . DB_PREFIX . "sale_event_description SET name = '" . $this->db->escape($sale_event_description['name']) . "', description = '" . $this->db->escape($sale_event_description['description']) . "', sale_event_id = '" . (int)$sale_event_id . "', language_id = '" . (int)$language_id . "'");

    }

    $this->load->model('marketing/coupon');

    $this->model_marketing_coupon->disabledCoupons($data['disable_coupon']);

    if($data['datetime_start'] > date("Y-m-d H:i")) {
      $this->db->query("INSERT INTO " . DB_PREFIX . "task SET datetime_run = '" . $data['datetime_start'] . "', action = 'marketing/sale_events/enableSaleEvent', args = CONCAT('sale_event_id=', " . (int)$sale_event_id . "), frequency = 'once'");
      $this->db->query("INSERT INTO " . DB_PREFIX . "task SET datetime_run = '" . $data['datetime_end'] . "', action = 'marketing/sale_events/disableSaleEvent', args = CONCAT('sale_event_id=', " . (int)$sale_event_id . "), frequency = 'once'");
    }

  }

  public function deleteSaleEventsSetting($sale_event_id){

    $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_special WHERE special_id IN (SELECT product_special_id FROM oc_product_special ps WHERE ps.sale_event_id = '" . (int)$sale_event_id . "')");

    $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE sale_event_id = '" . (int)$sale_event_id . "'");

    $this->db->query("UPDATE " . DB_PREFIX . "sale_event SET sale_event_setting = '" . serialize(array()) . "' WHERE sale_event_id = '" . (int)$sale_event_id . "'");

  }

  public function DeleteSaleEvent($sale_event_id){
    $this->deleteSaleEventsSetting($sale_event_id);
    $this->db->query("UPDATE " . DB_PREFIX . "news SET sale_event_id = 0 WHERE sale_event_id = '" . (int)$sale_event_id . "'");
    $this->db->query("UPDATE " . DB_PREFIX . "banner_image SET sale_event_id = 0 WHERE sale_event_id = '" . (int)$sale_event_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "sale_event WHERE sale_event_id = '" . (int)$sale_event_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "sale_event_description WHERE sale_event_id = '" . (int)$sale_event_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "task WHERE args = CONCAT('sale_event_id='," . (int)$sale_event_id . ")");
  }

  public function getSaleEventNameForCoupon($coupon_id){

    $query = $this->db->query("SELECT sed.name, se.sale_event_id FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON (se.sale_event_id = sed.sale_event_id) WHERE sed.language_id = '". $this->config->get('config_language_id') ."' AND se.disable_coupon LIKE '%" . '"' .  (int)$coupon_id . '"' . "%' AND se.datetime_end BETWEEN NOW() + INTERVAL -1 YEAR AND NOW() + INTERVAL 1 YEAR");

    return $query->rows;

  }

  public function getCategoriesForSaleEvents($data = array()){
  		$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

  		if (!empty($data['filter_name'])) {
  			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
  		}

  		$sql .= " GROUP BY cp.category_id";

  		$sort_data = array(
  			'name',
  			'sort_order'
  		);

  		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
  			$sql .= " ORDER BY " . $data['sort'];
  		} else {
  			$sql .= " ORDER BY sort_order";
  		}

  		if (isset($data['order']) && ($data['order'] == 'DESC')) {
  			$sql .= " DESC";
  		} else {
  			$sql .= " ASC";
  		}

  		if (isset($data['start']) || isset($data['limit'])) {
  			if ($data['start'] < 0) {
  				$data['start'] = 0;
  			}

  			if ($data['limit'] < 1) {
  				$data['limit'] = 20;
  			}

  			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
  		}

  		$query = $this->db->query($sql);

  		return $query->rows;

  }

}
