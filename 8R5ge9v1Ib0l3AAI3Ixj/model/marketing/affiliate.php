<?php
class ModelMarketingAffiliate extends Model {
	public function addAffiliate($data) {
		$this->event->trigger('pre.admin.affiliate.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . trim($this->db->escape($data['email'])) . "', telephone = '" . $this->db->escape(trim($data['telephone'])) . "', fax = '" . $this->db->escape($data['fax']) . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', company = '" . $this->db->escape($data['company']) . "', website = '" . $this->db->escape($data['website']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', code = '" . $this->db->escape($data['code']) . "', commission = '" . (float)$data['commission'] . "', tax = '" . $this->db->escape($data['tax']) . "', payment = '" . $this->db->escape($data['payment']) . "', cheque = '" . $this->db->escape($data['cheque']) . "', paypal = '" . $this->db->escape($data['paypal']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

		$affiliate_id = $this->db->getLastId();

		$this->event->trigger('post.admin.affiliate.add', $affiliate_id);

		return $affiliate_id;
	}

	public function addPayout($bonuse_id, $customer_id, $notify, $notify_sms = 0){

		$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id ='" . $customer_id . "'");

		$affiliate_id = $customer->row['affiliate_id'];

		$pre_commission = $this->db->query("SELECT commission AS commission FROM " . DB_PREFIX . "affiliate WHERE affiliate_id='" . $affiliate_id ."'");

		$pre_commission = $pre_commission->row['commission'];

		$this->db->query("UPDATE " . DB_PREFIX . "affiliate_transaction SET date_payout = NOW() WHERE affiliate_transaction_id IN (" . $bonuse_id . ")") ;

		$payout = $this->db->query("SELECT affiliate_id, GROUP_CONCAT(description SEPARATOR ',') AS description, SUM(amount) AS amount, date_payout AS date_payout FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_transaction_id IN (" .$bonuse_id. ") GROUP BY date_payout");

		$data['payout'] = array(
			'description' => $payout->row['description'],
			'amount'      => $this->currency->format($payout->row['amount'], $this->config->get('config_currency')),
			'date_payout' => date($this->language->get('date_format_short'), strtotime($payout->row['date_payout']))
		);

		$affiliate_id = $payout->row['affiliate_id'];

		$data['bonuses_total'] = $this->currency->format($this->getTotalBonuses($affiliate_id, $paid_out = true), $this->config->get('config_currency'));

		$data['payouts_total'] = $this->currency->format($this->getTotalBonuses($affiliate_id, $paid_out = false), $this->config->get('config_currency'));

		$count_bonuses = $this->db->query("SELECT SUM(amount) as amount FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_transaction_id IN (" . $bonuse_id . ")" );

	//	$customer_id = $this->db->query("SELECT customer_id FROM " . DB_PREFIX ."customer WHERE affiliate_id ='" . (int)$affiliate_id . "'");

		$this->load->model('sale/customer');



		$result_notifications = $this->model_sale_customer->getEmailNotifications($customer_id);
		$notify_mail = $result_notifications['affiliate_bonus'];
//		$customer = $this->db->query("SELECT * FROM " . DB_PREFIX ."customer WHERE affiliate_id ='" . (int)$affiliate_id . "'");

		$post_commission = $this->db->query("SELECT commission AS commission FROM " . DB_PREFIX . "affiliate WHERE affiliate_id='" . $affiliate_id ."'");

		$post_commission = $post_commission->row['commission'];

		if ($notify == 1 || $notify_mail == 1){

		 $email = $customer->row['email'];

		 $language_directory = $this->model_sale_customer->getCustomerDirectory($customer_id);
		 $language = new Language($language_directory);

		 $language->load('mail/affiliate');

		 $subject = sprintf($language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		 $data['text_greeting']  = sprintf($language->get('text_hello'), $customer->row['firstname']) . "\n\n";

		 $data['text_main'][]= sprintf($language->get('text_bonuses'), $this->config->get('store_url'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		 if($pre_commission !== $post_commission){
			$data['text_main'][]= sprintf($language->get('text_bonuses_x2'), $post_commission . "\n\n");
		}

		 $data['text_main'][]= $language->get('text_bonuses_x1') . "\n\n";

		 if($pre_commission !== $post_commission){
			 $data['text_main'][]= $language->get('text_bonuses_x2') . "\n\n";
		 }

		 $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

		 $html = $this->load->view('mail/message.tpl', $data);

		 $mail_config = $this->config->get('config_mail');
		 $mail = new Mail($mail_config);
		 $mail->setTo($email);
		 if ($mail_config['smtp_username']) {
			 $mail->setFrom($mail_config['smtp_username']);
			 $mail->setReplyTo($this->config->get('config_email'));
		 } else {
			 $mail->setFrom($this->config->get('config_email'));
		 }
		 $mail->setSender($this->config->get('config_name'));
		 $mail->setSubject($subject);
		 $mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));

		 // $mail->send();
	}
	$result_sms_notifications = $this->model_sale_customer->getSmsNotifications($customer_id);
	$notify_sms_customer = $result_sms_notifications['affiliate_payout'];

		if ($notify_sms === true || (int)$notify_sms === 1 || (int)$notify_sms_customer === 1){
			if(!empty($customer->row['telephone_country_id'])){
				$this->load->model('localisation/country');
				$tel_code = $this->model_localisation_country->getCountryTelephoneCode($customer->row['telephone_country_id']);
				$telephone = $tel_code . $customer->row['telephone'];
			}else{
				$telephone = $customer->row['telephone'];
			}
			$config_sms = $this->config->get('config_sms');
			$language_directory = $this->model_sale_customer->getCustomerDirectory($customer_id);
	 		$language = new Language($language_directory);
			$language->load('sms/affiliate');
			$sms_message = $language->get('text_sms_payout');

			if($this->config->get('config_sms')){
				$sms_configurations = $this->config->get($config_sms);

				if($sms_configurations['sms_login']){
					$sms_login = $sms_configurations['sms_login'];
				}else{
					$sms_login = '';
				}

				if($sms_configurations['sms_password']){
					$sms_password = $sms_configurations['sms_password'];
				}else{
					$sms_password = '';
				}

				if($sms_configurations['sms_from']){
					$sms_from = $sms_configurations['sms_from'];
				}else{
					$sms_from = '';
				}

				if($sms_configurations['sms_request']){
					$sms_request = $sms_configurations['sms_request'];
				}else{
					$sms_request = '';
				}

				$sms_array = array(
					'to' 				=> $telephone,
					'message' 	=> html_entity_decode($sms_message, ENT_QUOTES, 'UTF-8'),
					'login'			=> $sms_login,
					'from'			=> $sms_from,
					'password'  => $sms_password,
					'request'		=> $sms_request,
				);

				$sms = new SMS($this->config->get('config_sms'), $sms_array);
				// $sms->send();
			}
		}

		return $data;
	}

	public function editAffiliate($affiliate_id, $data) {
		$this->event->trigger('pre.admin.affiliate.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . trim($this->db->escape($data['email'])) . "', telephone = '" . $this->db->escape(trim($data['telephone'])) . "', fax = '" . $this->db->escape($data['fax']) . "', company = '" . $this->db->escape($data['company']) . "', website = '" . $this->db->escape($data['website']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', code = '" . $this->db->escape($data['code']) . "', commission = '" . (float)$data['commission'] . "', tax = '" . $this->db->escape($data['tax']) . "', payment = '" . $this->db->escape($data['payment']) . "', cheque = '" . $this->db->escape($data['cheque']) . "', paypal = '" . $this->db->escape($data['paypal']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "', status = '" . (int)$data['status'] . "' WHERE affiliate_id = '" . (int)$affiliate_id . "'");

		if ($data['password']) {
			$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE affiliate_id = '" . (int)$affiliate_id . "'");
		}

		$this->event->trigger('post.admin.affiliate.edit', $affiliate_id);
	}

	public function deleteAffiliate($affiliate_id) {
		$this->event->trigger('pre.admin.affiliate.delete', $affiliate_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate WHERE affiliate_id = '" . (int)$affiliate_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_activity WHERE affiliate_id = '" . (int)$affiliate_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int)$affiliate_id . "'");

		$this->event->trigger('post.admin.affiliate.delete', $affiliate_id);
	}

	public function getAffiliate($affiliate_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate WHERE affiliate_id = '" . (int)$affiliate_id . "'");

		return $query->row;
	}

	public function getAffiliateByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "affiliate WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getAffiliates($data = array()) {
		$sql = "SELECT *, CONCAT(a.firstname, ' ', a.lastname) AS name, (SELECT SUM(at.amount) FROM " . DB_PREFIX . "affiliate_transaction at WHERE at.affiliate_id = a.affiliate_id GROUP BY at.affiliate_id) AS balance FROM " . DB_PREFIX . "affiliate a";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(a.firstname, ' ', a.lastname) LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$implode[] = "LCASE(a.email) = '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "'";
		}

		if (!empty($data['filter_code'])) {
			$implode[] = "a.code = '" . $this->db->escape($data['filter_code']) . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "a.status = '" . (int)$data['filter_status'] . "'";
		}

		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "a.approved = '" . (int)$data['filter_approved'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(a.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'name',
			'a.email',
			'a.code',
			'a.status',
			'a.approved',
			'a.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function approve($affiliate_id) {

		$affiliate_info = $this->getAffiliate($affiliate_id);

		if ($affiliate_info) {
			$this->event->trigger('pre.admin.affiliate.approve', $affiliate_id);

			$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET approved = '1' WHERE affiliate_id = '" . (int)$affiliate_id . "'");

			$this->load->language('mail/affiliate');

			$message  = sprintf($this->language->get('text_approve_welcome'), $this->config->get('config_name')) . "\n\n";
			$message .= $this->language->get('text_approve_login') . "\n";
			$message .= HTTP_CATALOG . 'index.php?route=affiliate/login' . "\n\n";
			$message .= $this->language->get('text_approve_services') . "\n\n";
			$message .= $this->language->get('text_approve_thanks') . "\n";
			$message .= $this->config->get('config_name');

			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo($affiliate_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(sprintf($this->language->get('text_approve_subject'), $this->config->get('config_name')));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			$this->event->trigger('post.admin.affiliate.approve', $affiliate_id);
		}
	}

	public function getAffiliatesByNewsletter() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE newsletter = '1' ORDER BY firstname, lastname, email");

		return $query->rows;
	}

	public function getTotalAffiliates($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$implode[] = "LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "status = '" . (int)$data['filter_status'] . "'";
		}

		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "approved = '" . (int)$data['filter_approved'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalAffiliatesAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE status = '0' OR approved = '0'");

		return $query->row['total'];
	}

	public function getTotalAffiliatesByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE country_id = '" . (int)$country_id . "'");

		return $query->row['total'];
	}

	public function getTotalAffiliatesByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate WHERE zone_id = '" . (int)$zone_id . "'");

		return $query->row['total'];
	}

	public function addTransaction($affiliate_id, $description = '', $amount = '', $order_id = 0) {
		$affiliate_info = $this->getAffiliate($affiliate_id);

		if ($affiliate_info) {
			$this->event->trigger('pre.admin.affiliate.transaction.add', $affiliate_id);

			$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate_transaction SET affiliate_id = '" . (int)$affiliate_id . "', order_id = '" . (float)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");

			$affiliate_transaction_id = $this->db->getLastId();

			$this->load->language('mail/affiliate');

			$message  = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($affiliate_id), $this->config->get('config_currency')));

			$mail_config = $this->config->get('config_mail');
			$mail = new Mail($mail_config);
			$mail->setTo($affiliate_info['email']);
			if ($mail_config['smtp_username']) {
				$mail->setFrom($mail_config['smtp_username']);
				$mail->setReplyTo($this->config->get('config_email'));
			} else {
				$mail->setFrom($this->config->get('config_email'));
			}
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(sprintf($this->language->get('text_transaction_subject'), $this->config->get('config_name')));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			// $mail->send();

			$this->event->trigger('post.admin.affiliate.transaction.add', $affiliate_transaction_id);

			return $affiliate_transaction_id;
		}
	}

public function deletePayout($date_payouts, $affiliate_id, $notify, $notify_sms){

	$count_bonuses = $this->db->query("SELECT SUM(amount) AS amount FROM " . DB_PREFIX . "affiliate_transaction WHERE date_payout= '" . $date_payouts . "'");

	$this->db->query("UPDATE " . DB_PREFIX . "affiliate_transaction SET date_payout = '0000-00-00 00:00:00' WHERE affiliate_id='" . $affiliate_id . "' AND date_payout IN ('". $date_payouts ."')");

	$data['payout']	= $this->db->query("SELECT affiliate_id, GROUP_CONCAT(description SEPARATOR ',') AS description, SUM(amount) AS amount, date_payout AS date_payout FROM " . DB_PREFIX . "affiliate_transaction WHERE date_payout IN ('" .$date_payouts. "') GROUP BY date_payout");
/*	$data['payout'] = array(
		'description' => $payout->row['description'],
		'amount'      => $this->currency->format($payout->row['amount'], $this->config->get('config_currency')),
		'date_payout' => date($this->language->get('date_format_short'), strtotime($payout->row['date_payout']))
	);*/

	//$affiliate_id = $payout->row['affiliate_id'];

	$data['bonuses_total'] = $this->currency->format($this->getTotalBonuses($affiliate_id, $paid_out = true), $this->config->get('config_currency'));

	$data['payouts_total'] = $this->currency->format($this->getTotalBonuses($affiliate_id, $paid_out = false), $this->config->get('config_currency'));

	$affiliate_info = $this->getAffiliate($affiliate_id);

	$this->load->model('sale/customer');

	$customer = $this->db->query("SELECT * FROM " . DB_PREFIX ."customer WHERE affiliate_id ='" . (int)$affiliate_id . "'");

	$customer_id = $customer->row['customer_id'];

	if ($notify == true){
	$language_directory = $this->model_sale_customer->getCustomerDirectory($customer_id);
	$language = new Language($language_directory);
//	$this->load->language('mail/customer');


	$language->load('mail/affiliate');

	$subject = sprintf($language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

	 $data['text_greeting']  = sprintf($language->get('text_hello'), $customer->row['firstname']) . "\n\n";
	 $data['text_main'][]= sprintf($language->get('text_delete_payout'), $this->currency->format($count_bonuses->row['amount']), $this->config->get('config_currency'));
	 $data['text_note'] = '';

	 $html = $this->load->view('mail/message.tpl', $data);

	 $mail_config = $this->config->get('config_mail');
	 $mail = new Mail($mail_config);
	 $mail->setTo($email);
	 if ($mail_config['smtp_username']) {
		 $mail->setFrom($mail_config['smtp_username']);
		 $mail->setReplyTo($this->config->get('config_email'));
	 } else {
		 $mail->setFrom($this->config->get('config_email'));
	 }
	 $mail->setSender($this->config->get('config_name'));
	 $mail->setSubject($subject);
	 $mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
	 // $mail->send();
	}

		if ($notify_sms === true || (int)$notify_sms === 1){

			if(!empty($customer->row['telephone_country_id'])){
				$this->load->model('localisation/country');
				$tel_code = $this->model_localisation_country->getCountryTelephoneCode($customer->row['telephone_country_id']);
				$telephone = $tel_code . $customer->row['telephone'];
			}else{
				$telephone = $customer->row['telephone'];
			}

			$config_sms = $this->config->get('config_sms');
			$language_directory = $this->model_sale_customer->getCustomerDirectory($customer_id);
			$language = new Language($language_directory);
			$language->load('sms/affiliate');
			$telephone = $telephone;
			$sms_message = $language->get('text_sms_delete_payout');

			if($this->config->get('config_sms')){
				$sms_configurations = $this->config->get($config_sms);

				if($sms_configurations['sms_login']){
					$sms_login = $sms_configurations['sms_login'];
				}else{
					$sms_login = '';
				}

				if($sms_configurations['sms_password']){
					$sms_password = $sms_configurations['sms_password'];
				}else{
					$sms_password = '';
				}

				if($sms_configurations['sms_from']){
					$sms_from = $sms_configurations['sms_from'];
				}else{
					$sms_from = '';
				}

				if($sms_configurations['sms_request']){
					$sms_request = $sms_configurations['sms_request'];
				}else{
					$sms_request = '';
				}

				$sms_array = array(
					'to' 				=> $telephone,
					'message' 	=> html_entity_decode($sms_message, ENT_QUOTES, 'UTF-8'),
					'login'			=> $sms_login,
					'from'			=> $sms_from,
					'password'  => $sms_password,
					'request'		=> $sms_request,
				);

				$sms = new SMS($this->config->get('config_sms'), $sms_array);
				$sms->send();
			}
		}

	return $data;
}

	public function deleteTransaction($order_id) {
		$this->event->trigger('pre.admin.affiliate.transaction.delete', $order_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int)$order_id . "'");

		$this->event->trigger('post.admin.affiliate.transaction.delete', $order_id);
	}

public function getTransactions($affiliate_id, $start = false, $limit = false, $show_all = false) {

	$sql = "SELECT * FROM " . DB_PREFIX . "affiliate_transaction at
	INNER JOIN " . DB_PREFIX . "order o ON at.order_id = o.order_id
	WHERE at.affiliate_id = '" . (int)$affiliate_id . "' AND at.date_payout = '0000-00-00 00:00:00' AND o.order_status_id IN(" . implode(',', $this->config->get('config_complete_status')) . ") ORDER BY at.date_added DESC";

			if(empty($show_all)){

				if ($start < 0) {
					$start = 0;
				}

				if ($limit < 1) {
					$limit = 10;
				}

					$sql .= " LIMIT " . (int)$start . "," . (int)$limit;
			}

	//		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int)$affiliate_id . "' AND date_payout = '0000-00-00 00:00:00' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCountTransactions($affiliate_id){
		$query =	$this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "affiliate_transaction at
		INNER JOIN " . DB_PREFIX . "order o ON at.order_id = o.order_id
		WHERE at.affiliate_id = '" . (int)$affiliate_id . "' AND at.date_payout = '0000-00-00 00:00:00' AND o.order_status_id IN(" . implode(',', $this->config->get('config_complete_status')) . ")");

		return $query->row;
	}

	public function getPayouts($affiliate_id,$start = 0, $limit = 10, $show_all){

		$sql = "SELECT GROUP_CONCAT(description SEPARATOR ',') AS description, SUM(amount) AS amount, date_payout AS date_payout FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . $affiliate_id ."' AND date_payout !='0000-00-00 00:00:00' GROUP BY date_payout";

		if(empty($show_all)){

		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 10;
		}

		$sql .= " LIMIT " . (int)$start . "," . (int)$limit;
	}

	$query = $this->db->query($sql);

	return $query->rows;

	}

	public function getTotalPayouts($affiliate_id){
		$query = $this->db->query("SELECT COUNT(DISTINCT date_payout)  FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . $affiliate_id ."' AND date_payout !='0000-00-00 00:00:00'");

	return $query->row;

	}

	public function getTotalBonuses($affiliate_id, $paid_out = false) {

		if ($paid_out === true ){
			$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "affiliate_transaction
			WHERE date_payout = '0000-00-00 00:00:00' AND affiliate_id = '" . (int)$affiliate_id ."'");
		}else{
			$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "affiliate_transaction
			WHERE date_payout != '0000-00-00 00:00:00' AND affiliate_id = '" . (int)$affiliate_id . "'");
		}


		return $query->row['total'];
	}

	public function CountTransactions($affiliate_id){
			$query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int)$affiliate_id . "'");

			return $query->row;

	}

	public function getTransactionTotal($affiliate_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int)$affiliate_id . "'");

		return $query->row['total'];
	}

	public function getTotalTransactionsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function getDatePayout($affiliate_id, $order_id){
		$query = $this->db->query("SELECT date_payout FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . (int)$affiliate_id . "'AND order_id= '" . (int)$order_id ."'");
		return $query->row;
	}

	public function getTotalLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "affiliate_login` WHERE `email` = '" . $this->db->escape($email) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_login` WHERE `email` = '" . $this->db->escape($email) . "'");
	}
}
