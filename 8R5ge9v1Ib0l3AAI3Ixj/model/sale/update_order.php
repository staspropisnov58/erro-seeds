<?php
class ModelSaleUpdateOrder extends Model
{
    public function addDiscount($order_id, $discount)
    {
        $this->load->model('sale/order');
        $totals = $this->model_sale_order->getOrderTotals($order_id);

        $total_key = array_search('total', array_column($totals, 'code'));
        $discount_key = array_search('discount', array_column($totals, 'code'));

        $this->db->query("UPDATE " . DB_PREFIX . "order
                      SET total = " . (float) $discount . "
                      WHERE order_id = " . (int) $order_id);
        $this->db->query("UPDATE " . DB_PREFIX . "order_total
                      SET `value` = " . (float) $discount . "
                      WHERE order_total_id = " . (int) $totals[$total_key]['order_total_id']);

        $saved_discount = - ((float) $totals[$total_key]['value'] - (float) $discount);

        if ($saved_discount) {
            if ($discount_key !== false) {
                $this->db->query("UPDATE " . DB_PREFIX . "order_total
                          SET `value` = " . $saved_discount . "
                          WHERE order_total_id = " . (int) $totals[$discount_key]['order_total_id']);
            } else {
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_total
                          SET order_id = " . (int) $order_id . ",
                          `code` = 'discount',
                          title = 'Скидка',
                          sort_order = 8,
                          `value` = " . $saved_discount);
            }
        } else {
            $this->db->query("DELETE FROM " . DB_PREFIX . "order_total
                        WHERE `code` = 'discount' AND order_id = " . (int) $order_id);
        }
    }
}
