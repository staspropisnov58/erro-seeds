<?php
 class ModelSeoboostCatalogUpgradeValidator extends Model
 {
   public function validateQuanity()
   {
     $errors = [];
     $selectable_option_types = ['select', 'checkbox', 'radio', 'image'];

     if (isset($this->request->post['product_option'])) {
       foreach ($this->request->post['product_option'] as $product_option) {
          if (in_array($product_option['type'], $selectable_option_types) && !$product_option['multiply']) {
            $option_values_quantity = array_sum(array_column($product_option['product_option_value'], 'quantity'));

            if ($option_values_quantity !== (int)$this->request->post['quantity']) {
              $errors['quantity_different'] = sprintf($this->language->get('error_quantity_different'), $product_option['name']);
              $errors['options_values_quantity_different'][$product_option['product_option_id']] = sprintf($this->language->get('error_quantity_different'), $product_option['name']);
            }
          }
       }
     }

     return $errors;
   }
 }
