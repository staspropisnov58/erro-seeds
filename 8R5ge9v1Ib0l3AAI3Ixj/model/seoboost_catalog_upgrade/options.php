<?php
class ModelSeoboostCatalogUpgradeOptions extends Model
{
  private $option_templates = ['text' => 'simple',
                               'textarea' => 'simple',
                               'file' => 'simple',
                               'date' => 'simple',
                               'time' => 'simple',
                               'datetime' => 'simple',
                               'select' => 'selectable',
                               'radio' => 'selectable',
                               'checkbox' => 'selectable',
                               'image' => 'selectable'];

  public function getOptions($product_id)
  {
    $this->load->model('catalog/option');
    $this->load->model('seoboost_catalog_upgrade/options');
    $this->load->model('catalog/product');

    $this->load->language('catalog/product');

    $data['text_yes'] 											= $this->language->get('text_yes');
    $data['text_no'] 												= $this->language->get('text_no');
    $data['text_enabled'] 								  = $this->language->get('text_enabled');
    $data['text_disabled'] 								  = $this->language->get('text_disabled');
    $data['text_add_new_option']            = $this->language->get('text_add_new_option');

    $data['entry_option'] 									= $this->language->get('entry_option');
    $data['entry_required'] 								= $this->language->get('entry_required');

    $data['button_option_add'] 							= $this->language->get('button_option_add');
		$data['button_remove'] 									= $this->language->get('button_remove');

    $data['token']                          = $this->session->data['token'];

    $product_options = $this->model_catalog_product->getProductOptions($product_id);

    $data['product_id'] = $product_id;

    $data['product_options'] = array();

    foreach ($product_options as $product_option) {
      $method_name = 'render' . ucfirst($this->option_templates[$product_option['type']]);
      $product_option_value = $this->{$method_name}($product_option);

      $data['product_options'][] = array(
        'product_option_id'         => $product_option['product_option_id'],
        'product_option_value'      => $product_option_value,
        'option_id'                 => $product_option['option_id'],
        'name'                      => $product_option['name'],
        'type'                      => $product_option['type'],
        'required'                  => $product_option['required'],
        'multiply'                  => $product_option['multiply'],
        'text_with_multiplication'  => $product_option['multiply'] ? $this->language->get('text_with_multiplication') : '',
      );
    }

    $query_params = '';
    $query_params = 'token=' . $this->session->data['token'];
    $query_params .= '&product_id=' . $product_id;

    $data['add_option'] = 'index.php?route=seoboost_catalog_upgrade/options/getAddOptionForm&' . $query_params;

    $data['new_product_option_id'] = $this->getNewProductOptionId();
    $data['new_product_option_value_id'] = $this->getNewProductOptionValueId();

    return $this->load->view('seoboost_catalog_upgrade/product_options.tpl', $data);
  }

  public function editOption($data){
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$data['product_option_id'] . "'");

    foreach($data['product_option'] as $product_options){
      foreach ($product_options['product_option_value'] as $product_option) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id= '" . $data['product_option_id'] . "', product_id= '" . $data['product_id'] . "', option_id='" . $data['option_id'] . "', option_value_id= '" . $product_option['option_value_id'] . "', quantity='" . $product_option['quantity'] . "', price_prefix='" . $product_option['price_prefix'] . "', price='" . $product_option['price'] . "', status='" . $product_option['status'] . "', points_prefix='" . $product_option['points_prefix'] . "', points='" . $product_option['points'] . "', subtract='" . $product_option['subtract'] . "', weight_prefix='" . $product_option['weight_prefix'] . "', weight='" . $product_option['weight'] . "'");
      }
    }
  }

  public function addOption($data){

    $product_option_id = false;

    foreach ($data['product_option'] as $product_option) {

      $this->db->query("INSERT INTO " .  DB_PREFIX . "product_option SET product_id = '" . $data['product_id'] . "', option_id= '" . $product_option['option_id'] . "', required= '" . $product_option['required'] . "', quantity_to_subtract_option = 'one_to_many'");

      $product_option_id = $this->db->getLastId();

      foreach ($product_option['product_option_value'] as $product_option_value){
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id='" . (int)$product_option_id . "', product_id='" . $data['product_id'] . "', option_id='" . $product_option['option_id'] . "', option_value_id='" . $product_option_value['option_value_id'] . "', quantity= '" . $product_option_value['quantity'] . "', subtract='" . $product_option_value['subtract'] . "', price = '" . $product_option_value['price'] . "', price_prefix='" . $product_option_value['price_prefix'] . "', points= '" . $product_option_value['points'] . "', points_prefix= '" . $product_option_value['points_prefix'] . "', weight = '" . $product_option_value['weight'] . "', weight_prefix= '" . $product_option_value['weight_prefix'] . "', status= '" . $product_option_value['status'] . "'");

      }

    }

    return $product_option_id;

  }

  public function renderOptions($data)
  {
    $this->load->language('catalog/product');
    $this->load->model('catalog/product');
    $this->load->model('catalog/option');

    $data['entry_select_option']            = $this->language->get('entry_select_option');
    $data['entry_option_value']             = $this->language->get('entry_option_value');
    $data['entry_quantity']                 = $this->language->get('entry_quantity');
    $data['entry_status']                   = $this->language->get('entry_status');
    $data['entry_subtract']                 = $this->language->get('entry_subtract');
    $data['entry_price']                    = $this->language->get('entry_price');
    $data['entry_option_points']            = $this->language->get('entry_option_points');
    $data['entry_weight']                   = $this->language->get('entry_weight');
    $data['text_enabled']                   = $this->language->get('text_enabled');
    $data['text_disabled']                  = $this->language->get('text_disabled');
    $data['text_yes']                       = $this->language->get('text_yes');
    $data['text_no']                        = $this->language->get('text_no');
    $data['entry_points']                   = $this->language->get('entry_points');
    $data['button_remove']                  = $this->language->get('button_remove');
    $data['button_save']                    = $this->language->get('button_save');
    $data['button_option_value_add']        = $this->language->get('button_option_value_add');

    $data['token']                          = $this->session->data['token'];

    $url = '';

    if(isset($data['option_id'])){
      $url .= '&option_id=' . $data['option_id'];
    }

    if(isset($data['product_id'])){
      $url .= '&product_id=' . $data['product_id'];
    }

    if(isset($data['product_option_id'])){
      $url .= '&product_option_id=' . $data['product_option_id'];
    }

    $data['url'] = htmlspecialchars_decode($this->url->link('seoboost_catalog_upgrade/template_parts/getNewProductOptionValue', 'token=' . $this->session->data['token'] . $url, 'SSL'), ENT_NOQUOTES);

    if(isset($data['product_id'])){
      $data['option'] = $this->model_catalog_product->getProductOptions($data['product_id']);
      $data['option_values'] = $this->model_catalog_option->getOptionValues($data['option_id']);
    }

    return $data;
  }

  public function renderNewOption($data){

    $this->load->language('catalog/product');
    $this->load->model('catalog/product');
    $this->load->model('catalog/option');

    $data['button_save'] = $this->language->get('button_save');
    $data['text_select'] = $this->language->get('text_select');
    $data['token'] = $this->session->data['token'];
    $data['entry_option'] = $this->language->get('entry_option');
    $data['options'] = $this->model_catalog_option->getOptions();




    $url = '';

    if(isset($data['product_id'])){
      $url .= '&product_id=' . $data['product_id'];
    }

    $data['url'] = htmlspecialchars_decode($this->url->link('seoboost_catalog_upgrade/template_parts/getNewProductOption', 'token=' . $this->session->data['token'] . $url, 'SSL'), ENT_NOQUOTES);

    return $data;

  }


  public function renderNewOptionForm($option_id, $new_product_option_id)
  {
    $this->load->language('catalog/product');

    $data['text_yes'] 											= $this->language->get('text_yes');
    $data['text_no'] 												= $this->language->get('text_no');
    $data['entry_required'] 								= $this->language->get('entry_required');


    $this->load->model('catalog/option');
    $data['option'] = $this->model_catalog_option->getOption($option_id);

    $data['option']['product_option_id'] = $new_product_option_id;

    $method_name = 'render' . ucfirst($this->option_templates[$data['option']['type']]);

    $data['option']['product_option_value'] = $this->{$method_name}($data['option'], true);

    return $this->load->view('seoboost_catalog_upgrade/new_product_option.tpl', $data);
  }

  public function renderNewOptionValue($option_id, $product_option_id, $new_product_option_value_id)
  {
    $this->load->language('catalog/product');

    $data['text_yes'] 											= $this->language->get('text_yes');
    $data['text_no'] 												= $this->language->get('text_no');
    $data['text_enabled'] 								 = $this->language->get('text_enabled');
    $data['text_disabled'] 								 = $this->language->get('text_disabled');

		$data['button_remove'] 									= $this->language->get('button_remove');

    $data['entry_points']                   = $this->language->get('entry_points');
    $data['entry_price'] 										= $this->language->get('entry_price');
    $data['entry_quantity'] 								= $this->language->get('entry_quantity');
    $data['entry_weight'] 									= $this->language->get('entry_weight');

    $this->load->model('catalog/option');
    $data['option_id'] = $option_id;
    $data['option'] = $this->model_catalog_option->getOption($option_id);
    $data['product_option_id'] = $product_option_id;
    $data['option_values'] = $this->model_catalog_option->getOptionValues($option_id);
    $data['product_option_value_id'] = $new_product_option_value_id;

    return $this->load->view('seoboost_catalog_upgrade/new_product_option_value.tpl', $data);
  }

  private function renderSimple($product_option, $form = false)
  {
    $data['entry_option_value'] = $this->language->get('entry_option_value');

    $data['product_option'] = $product_option;
    if($form){
      return $this->load->view('seoboost_catalog_upgrade/product_option_type_form/' . $product_option['type'] . '.tpl', $data);
    }else{
      return $this->load->view('seoboost_catalog_upgrade/product_option_type/' . $product_option['type'] . '.tpl', $data);
    }
  }

  private function renderSelectable($product_option, $form = false){

    $this->load->language('catalog/product');

    $data['text_yes']  											= $this->language->get('text_yes');
    $data['text_no'] 												= $this->language->get('text_no');
    $data['text_enabled'] 								  = $this->language->get('text_enabled');
    $data['text_disabled'] 								  = $this->language->get('text_disabled');
    $data['text_currency']                  = $this->language->get('text_currency');
    $data['text_edit_option']               = $this->language->get('text_edit_option');
    $data['text_delete_option']             = $this->language->get('text_delete_option');

    $data['entry_option_points'] 						= $this->language->get('entry_option_points');
    $data['entry_option_value'] 						= $this->language->get('entry_option_value');
    $data['entry_points']                   = $this->language->get('entry_points');
    $data['entry_price'] 										= $this->language->get('entry_price');
    $data['entry_quantity'] 								= $this->language->get('entry_quantity');
    $data['entry_status'] 									= $this->language->get('entry_status');
    $data['entry_subtract'] 								= $this->language->get('entry_subtract');
    $data['entry_weight'] 									= $this->language->get('entry_weight');
    $data['entry_points']                   = $this->language->get('entry_points');
    $data['button_remove']                  = $this->language->get('button_remove');
    $data['button_option_value_add']        = $this->language->get('button_option_value_add');
    $data['button_save']                    = $this->language->get('button_save');

    $data['button_remove'] 									= $this->language->get('button_remove');
    $data['button_option_value_add'] 				= $this->language->get('button_option_value_add');

    if(isset($this->session->data['errors'])){
      $data['errors'] = $this->session->data['errors'];
    }
    $data['product_option'] = $product_option;

    $data['token'] = $this->session->data['token'];
    $data['option_values'] = $this->model_catalog_option->getOptionValues($product_option['option_id']);

    $query_params = '';
    $query_params = 'token=' . $this->session->data['token'];
    if(isset($product_option['product_id'])){
      $query_params .= '&product_id=' . $product_option['product_id'];
    }

    if(isset($product_option['option_id'])){
      $query_params .= '&option_id=' . $product_option['option_id'];
    }
    if(isset($product_option['product_option_id'])){
      $query_params .= '&product_option_id=' . $product_option['product_option_id'];
    }
    $data['edit_option'] = 'index.php?route=seoboost_catalog_upgrade/options/getEditOptionForm&' . $query_params;
    $data['delete_option'] = 'index.php?route=seoboost_catalog_upgrade/options/deleteOption&' . $query_params;

    $data['url'] = $this->url->link('seoboost_catalog_upgrade/template_parts/getNewProductOptionValue', $query_params, true);
    if($form){
      return $this->load->view('seoboost_catalog_upgrade/product_option_type_form/selectable.tpl', $data);
    }else{
      return $this->load->view('seoboost_catalog_upgrade/product_option_type/selectable.tpl', $data);

    }
  }

  public function getNewProductOptionId()
  {
    if (isset($this->request->post['product_option'])) {
      $new_product_option_id = max(array_keys($this->request->post['product_option'])) + 1;
    } else {
      $result = $this->db->query('SELECT product_option_id + 1 AS new_product_option_id FROM ' . DB_PREFIX . 'product_option ORDER BY product_option_id DESC LIMIT 1');

      $new_product_option_id = $result->num_rows ? $result->row['new_product_option_id'] : 1;
    }

    return $new_product_option_id;
  }

  public function deleteProductOption($product_option_id){
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . $product_option_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . $product_option_id . "'");
  }

  public function getNewProductOptionValueId()
  {
    if (isset($this->request->post['product_option'])) {
      $new_product_option_value_id = 1;
      array_walk($this->request->post['product_option'], function ($product_option, $i, $new_product_option_value_id) {
        $max_option_value_id = max(array_keys($product_option['product_option_value']));
        $new_product_option_value_id = max($new_product_option_value_id, $max_option_value_id);
      }, $new_product_option_value_id);
    } else {
      $result = $this->db->query('SELECT product_option_value_id + 1 AS new_product_option_value_id FROM ' . DB_PREFIX . 'product_option_value ORDER BY product_option_value_id DESC LIMIT 1');

      $new_product_option_value_id = $result->num_rows ? $result->row['new_product_option_value_id'] : 1;
    }

    return $new_product_option_value_id;
  }
}
