<?php
class ModelStoreReviewStoreReview extends Model
{

  public function getReviews($filter_data = [])
  {
    $sql = "SELECT * FROM " . DB_PREFIX . "store_review WHERE user_id = 0";


		if (isset($filter_data['filter_rating']) && !is_null($filter_data['filter_rating'])) {
			$sql .= " AND rating = '" . (int)$filter_data['filter_rating'] . "'";
		}

		if (!empty($filter_data['filter_text'])) {
			$sql .= " AND text LIKE '" . $this->db->escape($filter_data['filter_text']) . "%'";
		}

		if (!empty($filter_data['filter_author'])) {
			$sql .= " AND author LIKE '" . $this->db->escape($filter_data['filter_author']) . "%'";
		}

		if (isset($filter_data['filter_status']) && !is_null($filter_data['filter_status'])) {
			$sql .= " AND status = '" . (int)$filter_data['filter_status'] . "'";
		}

		if (!empty($filter_data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($filter_data['filter_date_added']) . "')";
		}

    if (!empty($filter_data['filter_admin_answer'])) {
      if ($filter_data['filter_admin_answer'] === 'without') {
        $sql .= " AND review_id NOT IN";
      } elseif ($filter_data['filter_admin_answer'] === 'with') {
        $sql .= " AND review_id IN";
      }
			$sql .= "(SELECT p.review_id FROM " . DB_PREFIX . "store_review p
                INNER JOIN " . DB_PREFIX . "store_review ch ON p.review_id = ch.parent_id WHERE ch.user_id != 0)";
		}

		$sort_data = array(
			'author',
			'rating',
			'status',
			'date_added'
		);

		if (isset($filter_data['sort']) && in_array($filter_data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $filter_data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($filter_data['start']) || isset($filter_data['limit'])) {
			if ($filter_data['start'] < 0) {
				$filter_data['start'] = 0;
			}

			if ($filter_data['limit'] < 1) {
				$filter_data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
		}

		$query = $this->db->query($sql);

    if (isset($filter_data['sort']['admin_answer'])) {
      foreach($query->rows as $key => $review) {
        if ($this->hasAdminAnswer($review['review_id'])) {
          $with[] = $review;
        } else {
          $without[] = $review;
        }
      }

      if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
        $data = array_merge($with, $without);
      } else {
        $data = array_merge($without, $with);
      }
    } else {
      $data = $query->rows;
    }

		return $data;
  }

  public function getTotalReviews($filter_data = [])
  {
    $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "store_review WHERE user_id = 0";

		if (isset($filter_data['filter_rating']) && !is_null($filter_data['filter_rating'])) {
			$sql .= " AND rating = '" . (int)$filter_data['filter_rating'] . "'";
		}

		if (!empty($filter_data['filter_author'])) {
			$sql .= " AND author LIKE '" . $this->db->escape($filter_data['filter_author']) . "%'";
		}

		if (isset($filter_data['filter_status']) && !is_null($filter_data['filter_status'])) {
			$sql .= " AND status = '" . (int)$filter_data['filter_status'] . "'";
		}

		if (!empty($filter_data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($filter_data['filter_date_added']) . "')";
		}

    if (!empty($filter_data['filter_admin_answer'])) {
      if ($filter_data['filter_admin_answer'] === 'without') {
        $sql .= " AND review_id NOT IN";
      } elseif ($filter_data['filter_admin_answer'] === 'with') {
        $sql .= " AND review_id IN";
      }
			$sql .= "(SELECT p.review_id FROM " . DB_PREFIX . "store_review p
                INNER JOIN " . DB_PREFIX . "store_review ch ON p.review_id = ch.parent_id WHERE ch.user_id != 0)";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
  }

  public function hasAdminAnswer($review_id)
  {
    $query = $this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "store_review WHERE parent_id = '" . (int)$review_id . "' AND user_id != 0");

    if ($query->num_rows) {
      return true;
    } else {
      return false;
    }
  }
}
