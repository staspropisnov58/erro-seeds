<?php
/**
* based on MySQL Hierarchical Data Closure Table Pattern
* namespace Admin\Model\Feedback
*/

class ModelFeedbackReviewHierarchy extends Model
{
  public function addNode($node)
  {
    $this->db->query("INSERT INTO " . DB_PREFIX . "store_review_hierarchy
                      SET parent_id = " . (int)$node['review_id'] . ",
                          child_id = " . (int)$node['review_id'] . ",
                          depth = 0");

    if ((int)$node['parent_id']) {
      $this->addNodeAsChild($node);
    }
  }

  private function addNodeAsChild($node) {
    $this->db->query("INSERT INTO " . DB_PREFIX . "store_review_hierarchy
                      (parent_id, child_id, depth)
                      SELECT p.parent_id, " . (int)$node['review_id'] . ", p.depth + 1
                      FROM " . DB_PREFIX . "store_review_hierarchy p
                      WHERE p.child_id = " . (int)$node['parent_id']);
  }
}
