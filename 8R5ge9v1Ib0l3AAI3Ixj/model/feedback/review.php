<?php
/**
* namespace Admin\Model\Feedback
*/

class ModelFeedbackReview extends Model
{
  /**
  * use Admin\Model\Feedback\ReviewHierarchy;
  */
  private $filter_data = ['item' => '',
                          'answers' => false];

  public function addReview($review, $import = false)
  {
    if ($review['parent_id'] && $import) {
      $parent_id_query = $this->db->query("SELECT review_id AS parent_id
                                            FROM " . DB_PREFIX . "store_review
                                            WHERE old_id = " . (int)$review['parent_id'] . "
                                            AND item = '" . $this->db->escape($review['item']) . "'");

      $parent_id = isset($parent_id_query->row['parent_id']) ? $parent_id_query->row['parent_id'] : $review['parent_id'];
    } else {
      $parent_id = $review['parent_id'];
    }

    $sql = "INSERT INTO " . DB_PREFIX . "store_review
                      SET title = '" . $this->db->escape($review['title']) . "',
                          parent_id = " . (int)$parent_id . ",
                          item = '" . $this->db->escape($review['item']) . "',
                          customer_id = " . (int)$review['customer_id'] . ",
                          author = '" . $this->db->escape($review['author']) . "',
                          email = '". $this->db->escape(trim($review['email'])) . "',
                          text = '" . $this->db->escape(trim($review['text'])) . "',
                          rating = " . (int)$review['rating'] . ",
                          status = " . (int)$review['status']  . ",
                          date_modified = NOW()";

    if ($import) {
      $like = isset($review['review_like']) ? (int)$review['review_like'] : 0;
      $dislike = isset($review['review_dislike']) ? (int)$review['review_dislike'] : 0;

      $sql .= ", review_like = " . $like . ",
                 review_dislike = " . $dislike . ",
                 date_added = '" . $this->db->escape($review['date_added']) . "',
                 old_id = " . (int)$review['review_id'];
    } else {
      $sql .= ", date_added = NOW()";
    }

    $this->db->query($sql);
    $review_id = $this->db->getLastId();

    $this->load->model('feedback/review_hierarchy');
    $this->model_feedback_review_hierarchy->addNode(['review_id' => $review_id, 'parent_id' => $parent_id]);
  }

  public function getDiscussion($review_id)
  {
    $discussion = [];

    $discussion_query = $this->db->query("SELECT ");

    return $discussion;
  }

  public function getReview($review_id)
  {
    $review = [];

    $review_query = "SELECT r.review_id, r.parent_id, r.item_id,
                    r.author, r.customer_id, r.user_id, r.email,
                    r.rating, r.text, r.date_added, r.status
                    FROM " . DB_PREFIX . "store_review r
                    WHERE r.review_id = " . (int)$review_id;

    if ($review_query->num_rows) {
      if (!$review_query->row['parent_id']) {
        if (strpos($review_query->row['item'], 'product') === 0) {
          $product_query = $this->db->query("SELECT p.product_id, CONCAT(pd.name, ' | ', p.model) AS name
                                             FROM " . DB_PREFIX . "product p
                                             INNER JOIN " . DB_PREFIX . "product_description pd
                                             ON p.product_id = pd.product_id
                                             WHERE pd.language_id = " . (int)$this->config->get('config_language_id') . "
                                             AND p.product_id = " . (int)$review_query->row['item_id']);

          if ($product_query->num_rows) {

          }
        } elseif (strpos($review_query->row['item'], 'article') === 0) {

        } else {
        }
      }
    }

    return $review;
  }

  public function getReviews($filter_data = [])
  {
    $sql = "SELECT r.review_id, r.author, r.customer_id, r.email, r.rating, r.date_added, r.status,
            (SELECT COUNT(*) FROM " . DB_PREFIX . "store_review_hierarchy rh WHERE rh.parent_id = r.review_id AND rh.depth != 0) AS answers,
            (SELECT COUNT(*) FROM " . DB_PREFIX . "store_review_hierarchy rhd
            INNER JOIN " . DB_PREFIX . "store_review rd ON rhd.child_id = rd.review_id
            WHERE rhd.parent_id = r.review_id AND rhd.depth != 0 AND rd.status = 0) AS disabled_answers
            FROM " . DB_PREFIX . "store_review r";

    if ($filter_data) {
      $sql .= $this->buildQueryWithFilters($filter_data);
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getTotalReviews($filter_data = []) {
    $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "store_review r";

    if ($filter_data) {
      $sql .= $this->buildQueryWithFilters($filter_data);
    }

    $query = $this->db->query($sql);

		return $query->row['total'];
  }

  private function buildQueryWithFilters($filter_data)
  {
    $sql = "";
    $filter_item_string = "";
    $filter_text_string = "";

    if ($filter_data['filter_item']) {
      if (isset($filter_data['item'])) {
        if ($filter_data['item'] === 'product') {
          $sql .= " INNER JOIN " . DB_PREFIX . "product_description pd ON r.item = CONCAT('product=', pd.product_id) ";
          $filter_item_string = " AND pd.name LIKE '%" . $this->db->escape($filter_data['filter_item']) . "%' AND pd.language_id = " . (int)$this->config->get('config_language_id');
        } elseif ($filter_data['item'] === 'article') {
          $sql .= " INNER JOIN " . DB_PREFIX . "news_description nd ON r.item = CONCAT('article=', nd.news_id) ";
          $filter_item_string = " AND nd.title LIKE '%" . $this->db->escape($filter_data['filter_item']) . "%' AND nd.language_id = " . (int)$this->config->get('config_language_id');
        }
      }
    }

    if (!empty($filter_data['filter_text'])) {
      $sql .= " INNER JOIN (SELECT GROUP_CONCAT(DISTINCT ra.text) AS answer_text, rh.parent_id
      FROM " . DB_PREFIX . "store_review_hierarchy rh
      INNER JOIN " . DB_PREFIX . "store_review ra ON rh.child_id = ra.review_id GROUP BY rh.parent_id)
      AS at ON r.review_id = at.parent_id";

      $filter_text_string = " AND (r.text LIKE '%" . $this->db->escape($filter_data['filter_text']) . "%' ";

      $filter_text_string .= " OR at.answer_text LIKE '%" . $this->db->escape($filter_data['filter_text']) . "%') ";
    }

    $sql .= " WHERE ";

    if (isset($filter_data['item'])) {
      $sql .= " r.item LIKE '" . $this->db->escape($filter_data['item']) . "%' ";
    }

    $sql .= $filter_item_string;

    if (isset($filter_data['answers'])) {
    } else {
      $sql .= " AND r.parent_id = 0 ";
    }

    if (isset($filter_data['filter_rating']) && !is_null($filter_data['filter_rating'])) {
      $sql .= " AND r.rating = '" . (int)$filter_data['filter_rating'] . "'";
    }

    if (!empty($filter_data['filter_author'])) {
      $sql .= " AND (r.author LIKE '" . $this->db->escape($filter_data['filter_author']) . "%'";
      $sql .= " OR r.email LIKE '" . $this->db->escape($filter_data['filter_author']) . "%')";
    }

    if (isset($filter_data['filter_status']) && !is_null($filter_data['filter_status'])) {
      $sql .= " AND r.status = '" . (int)$filter_data['filter_status'] . "'";
    }

    if (!empty($filter_data['filter_date_added'])) {
      $sql .= " AND DATE(r.date_added) = DATE('" . $this->db->escape($filter_data['filter_date_added']) . "')";
    }

    $sql .= $filter_text_string;

    $sort_data = array(
      'author',
      'rating',
      'status',
      'answers',
      'disabled_answers',
      'date_added'
    );

    if (isset($filter_data['sort']) && in_array($filter_data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $filter_data['sort'];
    } else {
      $sql .= " ORDER BY r.date_added";
    }

    if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    if (isset($filter_data['start']) || isset($filter_data['limit'])) {
      if ($filter_data['start'] < 0) {
        $filter_data['start'] = 0;
      }

      if ($filter_data['limit'] < 1) {
        $filter_data['limit'] = 20;
      }

      $sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
    }

    return $sql;
  }
}
