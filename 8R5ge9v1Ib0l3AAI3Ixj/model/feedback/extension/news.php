<?php
class ModelExtensionNews extends Model {
	public function addNews($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "news SET date_added = NOW(), status = '" . (int)$data['status'] . "', meta_robots = '" . $data['meta_robots'] . "'");

		$news_id = $this->db->getLastId();

		foreach ($data['news'] as $key => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX ."news_description SET news_id = '" . (int)$news_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "', image = '" . $this->db->escape($value['image']) . "', image_2 = '" . $this->db->escape($value['image_2']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '". $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL){
			if($this->config->get('opengraph_status')){
				foreach ($data['opengraph'] as $language_id => $value){
					$this->db->query("UPDATE " . DB_PREFIX . "news_description SET og_title = '" . $this->db->escape($value['title']) . "', og_description = '" . $this->db->escape($value['description']) . "', og_image = '" . $this->db->escape($value['image']) . "' WHERE language_id = '" . (int)$language_id . "' AND news_id = '" . (int)$news_id . "'");
				}
			}
		}

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_id=" . (int)$news_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}

		if(isset($data['sale_event_id']) && $data['sale_event_id']!== ''){

			$query = $this->db->query("SELECT news_id FROM " . DB_PREFIX . "news WHERE sale_event_id = '" . (int)$data['sale_event_id'] . "'");

			foreach($query->rows as $upd_news){
				$this->db->query("UPDATE " . DB_PREFIX . "news SET sale_event_id = 0 WHERE news_id = '" . (int)$upd_news['news_id'] . "'");
			}

			$this->db->query("UPDATE " . DB_PREFIX . "news SET sale_event_id = '" . (int)$data['sale_event_id'] . "', date_added = '" . $this->db->escape($data['sale_event_datetime_start']) . "' WHERE news_id = '" . (int)$news_id . "'");
		}

		if($this->config->get('check_modified')){
			$this->load->model('module/check_modified');
			$this->model_module_check_modified->eddDateModifiedAddNews($news_id);
		}

	}

	public function editNews($news_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "news SET status = '" . (int)$data['status'] . "', meta_robots = '" . $data['meta_robots'] . "' WHERE news_id = '" . (int)$news_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int)$news_id. "'");

		foreach ($data['news'] as $key => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX ."news_description SET news_id = '" . (int)$news_id . "', language_id = '" . (int)$key . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', short_description = '" . $this->db->escape($value['short_description']) . "', image = '" . $this->db->escape($value['image']) . "', image_2 = '" . $this->db->escape($value['image_2']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '". $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL){
			if($this->config->get('opengraph_status')){
				foreach ($data['opengraph'] as $language_id => $value){
					$this->db->query("UPDATE " . DB_PREFIX . "news_description SET og_title = '" . $this->db->escape($value['title']) . "', og_description = '" . $this->db->escape($value['description']) . "', og_image = '" . $this->db->escape($value['image']) . "' WHERE language_id = '" . (int)$language_id . "' AND news_id = '" . (int)$news_id . "'");
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int)$news_id. "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'news_id=" . (int)$news_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}

		if(isset($data['sale_event_id']) && $data['sale_event_id']!== ''){

			$query = $this->db->query("SELECT news_id FROM " . DB_PREFIX . "news WHERE sale_event_id = '" . (int)$data['sale_event_id'] . "'");

			foreach($query->rows as $upd_news){
				$this->db->query("UPDATE " . DB_PREFIX . "news SET sale_event_id = 0 WHERE news_id = '" . (int)$upd_news['news_id'] . "'");
			}

			$this->db->query("UPDATE " . DB_PREFIX . "news SET sale_event_id = '" . (int)$data['sale_event_id'] . "', date_added = '" . $this->db->escape($data['sale_event_datetime_start']) . "' WHERE news_id = '" . (int)$news_id . "'");
		}

		if($this->config->get('check_modified')){
			$this->load->model('module/check_modified');
			$this->model_module_check_modified->editDateModifiedEditNews($news_id);
		}
	}

	public function getNews($news_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int)$news_id . "') AS keyword FROM " . DB_PREFIX . "news WHERE news_id = '" . (int)$news_id . "'");

		if ($query->num_rows) {
			return $query->row;
		} else {
			return false;
		}
	}

	public function getNewsDescription($news_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int)$news_id . "'");
		foreach ($query->rows as $result) {
			$news_description[$result['language_id']] = array(
				'title'       			=> $result['title'],
				'short_description'		=> $result['short_description'],
				'description' 			=> $result['description'],
				'image' => $result['image'],
				'image_2' => $result['image_2'],
				'meta_title' => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword' => $result['meta_keyword']
			);
		}

		return $news_description;

	}

	public function getOpengraph($news_id){

		$opengraph = array();

		$query = $this->db->query("SELECT og_description, og_title, og_image, language_id FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int)$news_id . "'");

		foreach ($query->rows as $result) {
			$opengraph[$result['language_id']] = array(
				'title' => $result['og_title'],
				'description' => $result['og_description'],
				'image' => $result['og_image'],
			);

		}

		return $opengraph;
	}


	public function getAllNews($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON n.news_id = nd.news_id WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY date_added DESC";

		if (isset($data['filter_title']) && !empty($data['filter_title'])) {
			$sql .= " AND nd.title LIKE '" . $this->db->escape($data['filter_title']) . "%'";
		}

		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getNewsSaleEvents($sale_event_id = 0, $date_added, $date_added_year_ago){
		if($sale_event_id !== 0){
			$first = $this->db->query("SELECT n.news_id, nd.title  FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON(n.news_id = nd.news_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND date_added BETWEEN '" . $this->db->escape($date_added_year_ago) . "' AND '" . $this->db->escape($date_added) . "' AND sale_event_id = '" . (int)$sale_event_id . "'");
		}

		$sql = "SELECT n.news_id, nd.title  FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON(n.news_id = nd.news_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND date_added BETWEEN '" . $this->db->escape($date_added_year_ago) . "' AND '" . $this->db->escape($date_added) . "' AND sale_event_id = 0 ";


		$last = $this->db->query("SELECT n.news_id, nd.title  FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON(n.news_id = nd.news_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND date_added BETWEEN '" . $this->db->escape($date_added_year_ago) . "' AND '" . $this->db->escape($date_added) . "' AND sale_event_id NOT IN (0, '" . $sale_event_id . "') ");

		$query = $this->db->query($sql);

		if($sale_event_id !== 0 && $first->num_rows !== 0){
      $first = $first->rows;

      foreach($query->rows as $rows){
        $first[] = $rows;
      }
			foreach($last->rows as $rows){
        $first[] = $rows;
      }
      $query->rows = $first;
    }
		return($query->rows);
	}

	public function editNewsSaleEventId($sale_event_id, $sale_event_news_id){
		$this->db->query("UPDATE " . DB_PREFIX . "news SET sale_event_id = 0 WHERE sale_event_id = '" . (int)$sale_event_id . "'");
		$this->db->query("UPDATE " . DB_PREFIX . "news SET sale_event_id =  '" . (int)$sale_event_id . "', status = 1 WHERE news_id = '" . (int)$sale_event_news_id . "'");
	}

	public function deleteNews($news_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "news WHERE news_id = '" . (int)$news_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int)$news_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'news_id=" . (int)$news_id. "'");

		if($this->config->get('check_modified')){
			$this->load->model('module/check_modified');
			$this->model_module_check_modified->deleteDateModifiedDeleteNews($news_id);
		}
	}

	public function prevNews($news_id){
		$query = $this->db->query("SELECT n.news_id FROM oc_news n LEFT JOIN oc_news_description nd ON n.news_id = nd.news_id WHERE n.news_id < '" . $news_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = 1 ORDER BY n.news_id DESC LIMIT 1");

		return $query->row['news_id'];
	}

	public function nextNews($news_id){
		$query = $this->db->query("SELECT n.news_id FROM oc_news n LEFT JOIN oc_news_description nd ON n.news_id = nd.news_id WHERE n.news_id > '" . $news_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n.status = 1 ORDER BY n.news_id DESC LIMIT 1");

		return $query->row['news_id'];
	}

	public function getTotalNews() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news");

		return $query->row['total'];
	}
	public function setModule($news_id, $value){
		$this->db->query("UPDATE " . DB_PREFIX . "news SET module = '" . (int)$value . "' WHERE news_id = '" . (int)$news_id . "'");
	}
}
