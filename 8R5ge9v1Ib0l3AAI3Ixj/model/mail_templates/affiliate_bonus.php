<?php
class ModelMailTemplatesAffiliateBonus extends Model {
  public function getAffiliateOrders($data){

    $sql = "SELECT order_id FROM " . DB_PREFIX . "order WHERE affiliate_id != 0";

    if($data['order_id']){
      $sql .= " AND order_id LIKE '%" . (int)$data['order_id'] . "%' ";
    }

    $sql .= " ORDER BY order_id DESC ";


    $sql .= " LIMIT 0," . $data['limit'];

    $query = $this->db->query($sql);

    return $query->rows;

  }
}
