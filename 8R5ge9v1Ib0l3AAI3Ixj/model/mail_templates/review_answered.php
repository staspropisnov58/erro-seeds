<?php
class ModelMailTemplatesReviewAnswered extends Model {
  public function getReviewsId($data){

    $sql = "SELECT review_id FROM " . DB_PREFIX . "review WHERE review_id IN (SELECT DISTINCT rew_id FROM " . DB_PREFIX . "review where rew_id != 0)";

    // $sql = "SELECT review_id FROM " . DB_PREFIX . "review";

    if($data['review_id']){
      $sql .= " AND review_id LIKE '%" . (int)$data['review_id'] . "%' ";
    }

    $sql .= " ORDER BY review_id DESC ";


    $sql .= " LIMIT 0," . $data['limit'];

    $query = $this->db->query($sql);

    return $query->rows;

  }

  public function getAnswersId($data){
    $sql = "SELECT review_id FROM " . DB_PREFIX . "review WHERE rew_id = '" . (int)$data['review_id'] . "'";


    if($data['answer_id']){
      $sql .= " AND rew_id LIKE '%" . (int)$data['answer_id'] . "%' ";
    }

    $sql .= " ORDER BY review_id DESC ";


    $sql .= " LIMIT 0," . $data['limit'];

    $query = $this->db->query($sql);

    return $query->rows;
  }
}
