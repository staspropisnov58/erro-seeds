<?php
class ModelMailTemplatesReviewModerated extends Model {
  public function getReviewsId($data){

    $sql = "SELECT review_id FROM " . DB_PREFIX . "review";

    if($data['review_id']){
      $sql .= " WHERE review_id LIKE '%" . (int)$data['review_id'] . "%' ";
    }

    $sql .= " ORDER BY review_id DESC ";
    

    $sql .= " LIMIT 0," . $data['limit'];

    $query = $this->db->query($sql);

    return $query->rows;

  }
}
