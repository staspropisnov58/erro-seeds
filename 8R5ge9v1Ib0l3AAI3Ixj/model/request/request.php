<?php
class ModelRequestRequest extends Model {
  public function getAllNewRequest($data){
    $sql = "SELECT nr.request_id, (SELECT DISTINCT name FROM " . DB_PREFIX .  "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "') AS product_name, (SELECT sku FROM " . DB_PREFIX . "product p WHERE p.product_id = nr.product_id) AS product_sku, (SELECT model FROM " . DB_PREFIX . "product p WHERE p.product_id = nr.product_id) AS product_model, nr.customer_id, nr.email, nr.date_expired, nr.date_added, nr.quantity, nr.product_id, nr.language_id FROM " . DB_PREFIX . "notification_request nr";

    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getCompleteRequest($data){
    $sql = "SELECT nrc.request_id, CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nrc.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') AS product_name, nrc.customer_id, nrc.email, nrc.date_expired, nrc.date_added, nrc.date_complete, nrc.language_id, nrc.quantity AS notification_quantity, nrc.product_id FROM " . DB_PREFIX . "notification_request_complete nrc LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nrc.product_id) WHERE nrc.request_id > 0";

    $sort_data = array(
			'product_name',
			'nrc.email',
			'nrc.customer_id',
      'nrc.date_complete',
		);

    if (!empty($data['filter_email'])) {
      $sql .= " AND nrc.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nrc.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_complete'])) {
      $sql .= " AND nrc.date_complete LIKE '%" . $data['filter_date_complete'] . "%'";
    }

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY nrc.date_complete";
		}

    if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function getDeletedRequest($data){
    $sql = "SELECT nrd.request_id, p.quantity AS product_quantity,  CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nrd.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') AS product_name, nrd.customer_id, nrd.email, nrd.date_expired, nrd.date_added, nrd.date_deleted, nrd.language_id, nrd.quantity AS notification_quantity, nrd.product_id, nrd.reason FROM " . DB_PREFIX . "notification_request_deleted nrd LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nrd.product_id) WHERE nrd.request_id > 0";

    $sort_data = array(
			'product_name',
			'nrd.email',
			'nrd.customer_id',
			'nrd.date_added',
			'nrd.date_expired',
      'nrd.date_deleted',
			'nrd.quantity'
		);

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nrd.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nrd.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nrd.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nrd.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nrd.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if (!empty($data['filter_date_deleted'])) {
      $sql .= " AND nrd.date_deleted LIKE '%" . $data['filter_date_deleted'] . "%'";
    }

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY nrd.date_deleted";
		}

    if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getActiveRequest($data){

    $sql = "SELECT nr.request_id, p.quantity AS product_quantity,  CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') AS product_name, nr.customer_id, nr.email, nr.date_expired, nr.date_added, nr.language_id, nr.quantity AS notification_quantity, nr.product_id FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE p.quantity >= nr.quantity AND p.status = 1 AND nr.date_expired > NOW()";

    $sort_data = array(
			'product_name',
			'nr.email',
			'nr.customer_id',
			'nr.date_added',
			'nr.date_expired',
      'nr.date_deleted',
			'nr.quantity'
		);

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nr.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nr.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if(!empty($this->session->data['requests_id'])){

    $string = '';

    foreach($this->session->data['requests_id'] as $value){
      $string .= implode(',', $value) . ',';
    }


    if(implode(end($this->session->data['requests_id'])) == ''){
      $length = -2;
    }else{
      $length = -1;
    }

    $string = substr($string,0,$length);

      $sql .= " AND nr.request_id NOT IN ($string)";
    }

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY nr.date_added";
		}

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getNewRequest($data){
    $sql = "SELECT nr.request_id, p.quantity AS product_quantity, CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') AS product_name, nr.customer_id, nr.email, nr.date_expired, nr.date_added, nr.language_id, nr.quantity AS notification_quantity, nr.product_id FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE nr.date_expired > NOW() AND (p.quantity < nr.quantity OR p.status = 0)";

    $sort_data = array(
			'product_name',
			'nr.email',
			'nr.customer_id',
			'nr.date_added',
			'nr.date_expired',
      'nr.date_deleted',
			'nr.quantity'
		);

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nr.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nr.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if(!empty($data['filter_reason'])){
      $sql .= " AND " . html_entity_decode($data['filter_reason']) . "";
    }

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY nr.date_added";
		}

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getOrderRequest($data){
    $sql = "SELECT nr.request_id, p.quantity AS product_quantity, CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') AS product_name, nr.customer_id, nr.email,nr.telephone, nr.date_expired, nr.date_added, nr.language_id, nr.quantity AS notification_quantity, nr.product_id FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE nr.pre_order=1 AND (p.quantity < nr.quantity OR p.status = 0)";

    $sort_data = array(
			'product_name',
			'nr.email',
			'nr.telephone',
			'nr.customer_id',
			'nr.date_added',
			'nr.date_expired',
      'nr.date_deleted',
			'nr.quantity'
		);

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nr.email LIKE '%" . $data['filter_email'] . "%'";
    }
    if (!empty($data['filter_telephone'])) {
      $sql .= " AND nr.telephone LIKE '%" . $data['filter_telephone'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nr.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if(!empty($data['filter_reason'])){
      $sql .= " AND " . html_entity_decode($data['filter_reason']) . "";
    }

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY nr.date_added";
		}

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getOverdueRequest($data){
    $sql = "SELECT nr.request_id, p.quantity AS product_quantity, CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') AS product_name, nr.customer_id, nr.email, nr.date_expired, nr.date_added, nr.language_id, nr.quantity AS notification_quantity, nr.product_id FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE nr.date_expired < NOW()";

    $sort_data = array(
			'product_name',
			'nr.email',
			'nr.customer_id',
			'nr.date_added',
			'nr.date_expired',
      'nr.date_deleted',
			'nr.quantity'
		);

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nr.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nr.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY nr.date_expired";
		}

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

    if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getTotalActiveRequest($data){

    $sql = "SELECT COUNT(nr.request_id) as total FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE p.quantity >= nr.quantity AND p.status = 1 AND nr.date_expired > NOW()";

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nr.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nr.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if(!empty($this->session->data['requests_id'])){

    $string = '';

    foreach($this->session->data['requests_id'] as $value){
      $string .= implode(',', $value) . ',';
    }
    if(implode(end($this->session->data['requests_id'])) == ''){
      $length = -2;
    }else{
      $length = -1;
    }

    $string = substr($string,0,$length);
      $sql .= " AND nr.request_id NOT IN ($string)";
    }

    if (!empty($data['filter_reason'])) {
      // $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    $query = $this->db->query($sql);

    return $query->row['total'];

  }

  public function getTotalCompleteRequest($data){
    $sql = "SELECT COUNT(nrc.request_id) AS total FROM " . DB_PREFIX . "notification_request_complete nrc";

    if (!empty($data['filter_email'])) {
      $sql .= " AND nrd.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nrd.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_complete'])) {
      $sql .= " AND nrc.date_complete LIKE '%" . $data['filter_date_complete'] . "%'";
    }

    $query = $this->db->query($sql);


    return $query->row['total'];
  }

  public function getTotalDeletedRequest($data){
    $sql = "SELECT COUNT(nrd.request_id) AS total FROM " . DB_PREFIX . "notification_request_deleted nrd WHERE nrd.request_id > 0";

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nrd.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nrd.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nrd.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nrd.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nrd.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if (!empty($data['filter_date_deleted'])) {
      $sql .= " AND nrd.date_deleted LIKE '%" . $data['filter_date_deleted'] . "%'";
    }

    $query = $this->db->query($sql);

    return $query->row['total'];
  }
    public function getTotalOrderRequest($data){
        $sql = "SELECT COUNT(nr.request_id) as total FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE nr.pre_order='1' AND (p.quantity < nr.quantity OR p.status = 0)";

        if (!empty($data['filter_quantity'])) {
            $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
        }

        if (!empty($data['filter_email'])) {
            $sql .= " AND nr.email LIKE '%" . $data['filter_email'] . "%'";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND nr.date_added LIKE '%" . $data['filter_date_added'] . "%'";
        }

        if (!empty($data['filter_date_expired'])) {
            $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
        }

        if(!empty($data['filter_reason'])){
            $sql .= " AND " . html_entity_decode($data['filter_reason']) . "";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }
  public function getTotalNewRequest($data){
    $sql = "SELECT COUNT(nr.request_id) as total FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE nr.date_expired > NOW() AND (p.quantity < nr.quantity OR p.status = 0)";

    if (!empty($data['filter_quantity'])) {
      $sql .= " AND nr.quantity = '" . (int)$data['filter_quantity'] . "'";
    }

    if (!empty($data['filter_email'])) {
      $sql .= " AND nr.email LIKE '%" . $data['filter_email'] . "%'";
    }

    if (!empty($data['filter_name'])) {
      $sql .= " AND CONCAT((SELECT DISTINCT name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = nr.product_id AND language_id = '" . (int)$this->config->get('config_language_id') . "'), ' (', p.model, ') ', '(', p.sku, ') ') LIKE '%" . $data['filter_name'] . "%'";
    }

    if (!empty($data['filter_date_added'])) {
      $sql .= " AND nr.date_added LIKE '%" . $data['filter_date_added'] . "%'";
    }

    if (!empty($data['filter_date_expired'])) {
      $sql .= " AND nr.date_expired LIKE '%" . $data['filter_date_expired'] . "%'";
    }

    if(!empty($data['filter_reason'])){
      $sql .= " AND " . html_entity_decode($data['filter_reason']) . "";
    }

    $query = $this->db->query($sql);

    return $query->row['total'];
  }

  public function getTotalOverdueRequest(){
    $query = $this->db->query("SELECT COUNT(nr.request_id) as total FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = nr.product_id) WHERE nr.date_expired < NOW()");

    return $query->row['total'];
  }

  public function getProductsByRequestId($request_id){
    $query = $this->db->query("SELECT pd.name, p.model, p.sku FROM " . DB_PREFIX . "notification_request nr LEFT JOIN " . DB_PREFIX . "product p ON(nr.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = nr.product_id) WHERE pd.language_id = '". (int)$this->config->get('config_language_id') . "' AND request_id = '" . (int)$request_id . "'");

    return $query->rows;
  }

  public function getNewRequestEmail($id){

    $query = $this->db->query("SELECT email FROM " . DB_PREFIX . "notification_request WHERE request_id = '" . (int)$id . "'");

    return $query->row['email'];

  }

  public function editCompmleteRequest($requests_id){

    foreach($requests_id as $request_id){
        $notification_request_data = $this->db->query("SELECT * FROM " . DB_PREFIX . "notification_request WHERE request_id ='" . (int)$request_id . "'");

        foreach($notification_request_data->rows as $notification_request){

        $this->db->query("INSERT INTO " . DB_PREFIX . "notification_request_complete SET request_id= '" . (int)$request_id . "', product_id = '" . (int)$notification_request['product_id'] . "', quantity ='" . (int)$notification_request['quantity'] . "', customer_id = '" . (int)$notification_request['customer_id'] . "', email = '" . $this->db->escape($notification_request['email']) . "', date_added = '" . $this->db->escape($notification_request['date_added']) . "', date_expired = '" . $this->db->escape($notification_request['date_expired']) . "', date_complete = NOW(), language_id = '" . (int)$notification_request['language_id'] . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "notification_request WHERE request_id = '" . (int)$request_id . "'");
      }

    }
  }

  public function deleteRequests($data){
    foreach($data['deleteIt'] as $request_id){
      $requests_data = $this->db->query("SELECT * FROM " . DB_PREFIX . "notification_request WHERE request_id = '" . (int)$request_id . "'");

      foreach($requests_data->rows as $request_data){

        $this->db->query("INSERT INTO " . DB_PREFIX . "notification_request_deleted SET request_id = '" . (int)$request_id . "', product_id = '" . (int)$request_data['product_id'] . "', quantity = '" . (int)$request_data['quantity'] . "', customer_id = '" . (int)$request_data['customer_id'] . "', email = '" . $this->db->escape($request_data['email']) . "', date_added = '" . $this->db->escape($request_data['date_added']) . "', date_expired = '" . $this->db->escape($request_data['date_expired']) . "', date_deleted = NOW(), language_id = '" . (int)$request_data['language_id'] . "', reason = '" . $this->db->escape($data['reason']) . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "notification_request WHERE request_id = '" . (int)$request_id . "'");
      }
    }
  }

  public function deleteRelatedRequests($requests_id, $reason){

    foreach ($requests_id as $request_id){
      $requests_data = $this->db->query("SELECT * FROM " . DB_PREFIX . "notification_request WHERE request_id = '" . (int)$request_id . "'");
      foreach($requests_data->rows as $request_data){
        $this->db->query("INSERT INTO " . DB_PREFIX . "notification_request_deleted SET request_id = '" . (int)$request_id . "', product_id = '" . (int)$request_data['product_id'] . "', quantity = '" . (int)$request_data['quantity'] . "', customer_id = '" . (int)$request_data['customer_id'] . "', email = '" . $this->db->escape($request_data['email']) . "', date_added = '" . $this->db->escape($request_data['date_added']) . "', date_expired = '" . $this->db->escape($request_data['date_expired']) . "', date_deleted = NOW(), language_id = '" . (int)$request_data['language_id'] . "', reason = '" . $this->db->escape($reason) . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "notification_request WHERE request_id = '" . (int)$request_id . "'");
      }
    }
  }

}
