<?php
class ModelModuleRefferalLevels extends Model{
  public function editCommissionByRefferal($refferal_levels){
    $refferal_levels = array_reverse($refferal_levels);

    foreach ($refferal_levels as $key => $level) {
      $affiliates_to_update_sql = "SELECT affiliate_id FROM " . DB_PREFIX . "order
                                      WHERE order_status_id IN(" .implode(',', $this->config->get('config_complete_status')). ")
                                      AND affiliate_id != 0
                                      GROUP BY affiliate_id HAVING COUNT(*)";

      if ($key === 0) {
        $affiliates_to_update_sql .= " >= " . (int)$level['quantity'];
      } else {
        $affiliates_to_update_sql .= " BETWEEN " . (int)$level['quantity'] . " AND " . ((int)$refferal_levels[$key - 1]['quantity'] - 1);
      }

      $affiliates_to_update_query = $this->db->query($affiliates_to_update_sql);

      if ($affiliates_to_update_query->num_rows) {
        $affiliates_to_update = implode(',', array_column($affiliates_to_update_query->rows, 'affiliate_id'));
        $this->db->query("UPDATE " . DB_PREFIX . "affiliate SET commission =" . (int)$level['commission'] . " WHERE affiliate_id IN(" . $affiliates_to_update . ")");
      }

    }
  }
}
