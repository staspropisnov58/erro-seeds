<?php
/**
* namespace Admin\Model\Module;
*/
class ModelModuleStoreReview extends Model
{
  /**
  * use Admin\Model\Feedback\Review;
  * use Admin\Model\Module\Testimonial;
  * use Admin\Model\Review\StoreReview;
  * use Admin\Model\Blog\Comment;
  */
  private $structure = '';

  public function setDBStructure()
  {
    $codes = ['testimonial', 'sitereviews', 'storereview'];

    $module_query = $this->db->query("SELECT `code` FROM " . DB_PREFIX . "module
                                            WHERE `code` IN('" . implode('\', \'', $codes) . "')");

    if ($module_query->num_rows) {
      $module = array_column($module_query->rows, 'code');
      if (count($module) === 1) {
        if ($module[0] === 'testimonial') {
          $this->structure = 'ua';
        } elseif ($module[0] === 'sitereviews') {
          $this->structure = 'ge';
        } elseif ($module[0] === 'storereview') {
          $this->structure = 'spb';
        } else {
          trigger_error('There is no defined methods wich is returning structure for module ' . $module[0]);
          return;
        }
      } else {
        trigger_error('Modules ' . implode(',', $module) . ' are installed in the same time. Please, leave one of them and try again');
        return;
      }

    } else {
      trigger_error('No one from modules ' . implode(',', $codes) . ' is installed');
      return;
    }
  }

  public function getReviews($item, $start = 0, $limit = 500)
  {
    $method = 'get' . ucfirst($this->structure) . ucfirst($item) . 'Reviews';
    if (method_exists($this, $method)) {
      return $this->{$method}($start, $limit);
    } else {
      trigger_error('Cannot find method to get ' . $item  . ' reviews for structure ' . $this->structure);
      return;
    }
  }

  public function getTotalReviews($item)
  {
    $method = 'getTotal' . ucfirst($this->structure) . ucfirst($item) . 'Reviews';
    if (method_exists($this, $method)) {
      return $this->{$method}();
    } else {
      trigger_error('Cannot find method to get ' . $item  . ' total reviews for structure ' . $this->structure);
      return;
    }
  }

  public function getTotalReviewsToTransfer()
  {
    $this->load->model('feedback/review');

    $total_transferred_reviews = $this->model_feedback_review->getTotalReviews();

    $method = 'getTotal' . ucfirst($this->structure) . 'Reviews';
    if (method_exists($this, $method)) {
      $data['review_to_transfer'] = (int)($this->{$method}() - $total_transferred_reviews);
      $data['total_review_store'] = $this->getTotalReviews('store');
      return $data;
    } else {
      trigger_error('Cannot find method to get ' . $item  . ' total reviews for structure ' . $this->structure);
      return;
    }
  }

  public function saveReviews($reviews)
  {
    $review_to_save = [];
    $child_reviews = [];

    foreach ($reviews as $review) {
      $this->load->model('feedback/review');
      $this->model_feedback_review->addReview($review, true);
    }
  }

  private function getUaStoreReviews($start, $limit)
  {
    $reviews = [];

    $sql = "SELECT * FROM " . DB_PREFIX . "review WHERE product_id = 0";
    $sql .= " LIMIT " . (int)$start . "," . (int)$limit;

    $raw_reviews = $this->db->query($sql);

    if ($raw_reviews->num_rows) {
      $reviews = array_map(function ($raw_review) {
        return [
          'review_id' => $raw_review['review_id'],
          'title' => $raw_review['title'] ? $raw_review['title'] : 'review ' . $raw_review['review_id'],
          'item' => 'store',
          'parent_id' => $raw_review['rew_id'],
          'customer_id' => $raw_review['customer_id'],
          'author' => $raw_review['author'],
          'email' => $raw_review['email'],
          'text'  => $raw_review['text'],
          'rating' => $raw_review['rating'],
          'review_like' => $raw_review['clike'],
          'review_dislike' => $raw_review['cdislike'],
          'status' => $raw_review['status'],
          'date_added' => $raw_review['date_added'],
          'old_id' => $raw_review['review_id']
        ];
      }, $raw_reviews->rows);
    }

    return $reviews;
  }

  private function getTotalUaStoreReviews() {
    $this->load->model('module/testimonial');

    return $this->model_module_testimonial->getTotalReviews();
  }

  private function getUaProductReviews($start, $limit)
  {
    $reviews = [];

    $raw_reviews = $this->db->query("SELECT * FROM " . DB_PREFIX . "review
                                    WHERE product_id != 0
                                    LIMIT " . $start . ", " . $limit);

    if ($raw_reviews->num_rows) {
      $reviews = array_map(function ($raw_review) {
        return [
          'review_id' => $raw_review['review_id'],
          'title' => $raw_review['title'] ? $raw_review['title'] : 'review for product ' . $raw_review['review_id'],
          'item' => 'product=' . $raw_review['product_id'],
          'parent_id' => $raw_review['rew_id'],
          'customer_id' => $raw_review['customer_id'],
          'author' => $raw_review['author'],
          'email' => $raw_review['email'],
          'text'  => $raw_review['text'],
          'rating' => $raw_review['rating'],
          'review_like' => $raw_review['clike'],
          'review_dislike' => $raw_review['cdislike'],
          'status' => $raw_review['status'],
          'date_added' => $raw_review['date_added'],
          'old_id' => $raw_review['review_id']
        ];
      }, $raw_reviews->rows);
    }

    return $reviews;
  }

  private function getTotalUaProductReviews() {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review WHERE product_id != 0");

    return $query->row['total'];
  }

  private function getUaArticleReviews($start, $limit)
  {
    $reviews = [];

    $raw_reviews = $this->db->query("SELECT * FROM " . DB_PREFIX . "review_for_news
                                     LIMIT " . $start . ", " . $limit);

    if ($raw_reviews->num_rows) {
      $reviews = array_map(function ($raw_review) {
        return [
          'review_id' => $raw_review['review_id'],
          'title' => 'review for article ' . $raw_review['review_id'],
          'item' => 'article=' . $raw_review['product_id'],
          'parent_id' => $raw_review['rew_id'],
          'customer_id' => $raw_review['customer_id'],
          'author' => $raw_review['author'],
          'email' => $raw_review['email'],
          'text'  => $raw_review['text'],
          'rating' => $raw_review['rating'],
          'review_like' => $raw_review['clike'],
          'review_dislike' => $raw_review['cdislike'],
          'status' => $raw_review['status'],
          'date_added' => $raw_review['date_added'],
          'old_id' => $raw_review['review_id']
        ];
      }, $raw_reviews->rows);
    }

    return $reviews;
  }

  private function getTotalUaArticleReviews()
  {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review_for_news");

    return $query->row['total'];
  }

  private function getTotalUaReviews()
  {
    $store_and_products = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review");
    $news = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review_for_news");

    return (int)$store_and_products->row['total'] + (int)$news->row['total'];
  }

  private function getGeStoreReviews($start, $limit)
  {
    $reviews = [];

    // TODO: уточнить, как лучше поступить с отзывами о сайте: выбрать только на том языке, на котором писался отзыв или добавлять перевод

    return $reviews;
  }

  private function getTotalGeStoreReviews()
  {
  }

  private function getGeProductReviews($start, $limit)
  {
    $reviews = [];

    $this->load->model('catalog/review');

    $raw_reviews = $this->model_catalog_review->getReviews(['start' => $start, 'limit' => $limit]);

    if ($raw_reviews) {
      $reviews = array_map(function ($raw_review) {
        return [
          'review_id' => $raw_review['review_id'],
          'title' => 'review for product ' . $raw_review['review_id'],
          'item' => 'product=' . $raw_review['product_id'],
          'author' => $raw_review['author'],
          'email' => $raw_review['email'],
          'text'  => $raw_review['text'],
          'rating' => $raw_review['rating'],
          'status' => $raw_review['status'],
          'date_added' => $raw_review['date_added'],
          'old_id' => $raw_review['review_id']
        ];
      }, $raw_reviews);
    }

    return $reviews;
  }

  private function getTotalGeProductReviews()
  {
  }

  private function getGeArticleReviews($start, $limit)
  {
    $reviews = [];
    return $reviews;
  }

  private function getTotalGeArticleReviews()
  {
  }

  private function getTotalGeReviews()
  {
  }

  private function getSpbStoreReviews($start, $limit)
  {
    $reviews = [];

    $this->load->model('review/store_review');

    $raw_reviews = $this->model_review_store_review->getReviews(['start' => $start, 'limit' => $limit]);

    if ($raw_reviews) {
      $reviews = array_map(function ($raw_review) {
        return [
          'review_id' => $raw_review['review_id'],
          'title' => 'review ' . $raw_review['review_id'],
          'item' => 'store',
          'parent_id' => $raw_review['parent_id'],
          'customer_id' => $raw_review['customer_id'],
          'author' => $raw_review['author'],
          'email' => $raw_review['email'],
          'text'  => $raw_review['text'],
          'rating' => $raw_review['rating'],
          'review_like' => $raw_review['review_like'],
          'review_dislike' => $raw_review['review_dislike'],
          'status' => $raw_review['status'],
          'date_added' => $raw_review['date_added'],
          'old_id' => $raw_review['review_id']
        ];
      }, $raw_reviews);
    }

    return $reviews;
  }

  private function getTotalSpbStoreReviews()
  {
  }

  private function getSpbProductReviews($start, $limit)
  {
    $reviews = [];

    $this->load->model('catalog/review');

    $raw_reviews = $this->model_catalog_review->getReviews(['start' => $start, 'limit' => $limit]);

    if ($raw_reviews) {
      $reviews = array_map(function ($raw_review) {
        return [
          'review_id' => $raw_review['review_id'],
          'title' => 'review for product ' . $raw_review['review_id'],
          'item' => 'product=' . $raw_review['product_id'],
          'parent_id' => $raw_review['parent_id'],
          'customer_id' => $raw_review['customer_id'],
          'author' => $raw_review['author'],
          'email' => $raw_review['email'],
          'text'  => $raw_review['text'],
          'rating' => $raw_review['rating'],
          'review_like' => $raw_review['review_like'],
          'review_dislike' => $raw_review['review_dislike'],
          'status' => $raw_review['status'],
          'date_added' => $raw_review['date_added'],
          'old_id' => $raw_review['review_id']
        ];
      }, $raw_reviews);
    }

    return $reviews;
  }

  private function getTotalSpbProductReviews()
  {
  }

  private function getSpbArticleReviews($start, $limit)
  {
    $reviews = [];

    $this->load->model('blog/comment');

    $raw_reviews = $this->model_blog_comment->getComments(['start' => $start, 'limit' => $limit]);

    if ($raw_reviews) {
      $reviews = array_map(function ($raw_review) {
        return [
          'review_id' => $raw_review['review_id'],
          'title' => 'review for product ' . $raw_review['review_id'],
          'item' => 'article=' . $raw_review['article_id'],
          'parent_id' => $raw_review['parent_id'],
          'customer_id' => $raw_review['customer_id'],
          'author' => $raw_review['author'],
          'email' => $raw_review['email'],
          'text'  => $raw_review['text'],
          'rating' => $raw_review['rating'],
          'review_like' => $raw_review['review_like'],
          'review_dislike' => $raw_review['review_dislike'],
          'status' => $raw_review['status'],
          'date_added' => $raw_review['date_modified'],
          'old_id' => $raw_review['review_id']
        ];
      }, $raw_reviews);
    }

    return $reviews;
  }

  private function getTotalSpbArticleReviews()
  {
  }

  private function getTotalSpbReviews()
  {
  }
}
