<?php
class ModelModuleProductStickers extends Model
{
  public function setStickersCategory($data)
  {
    $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category
                      WHERE category_id = " . (int)$data['category_id']);

    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category (product_id, category_id)
                      SELECT product_id, " . (int)$data['category_id'] . "
                      FROM " . DB_PREFIX . "product
                      WHERE main_stickers LIKE '%" . $this->db->escape($data['sticker']) . "%'");

	  $this->load->model('setting/setting');
	  $this->model_setting_setting->editSetting('product_stickers_' . $data['sticker'] . '_category',
	  ['product_stickers_' . $data['sticker'] . '_category' => (int)$data['category_id']]);
  }
}
