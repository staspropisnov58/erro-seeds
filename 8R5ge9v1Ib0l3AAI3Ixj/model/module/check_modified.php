<?php
class ModelModuleCheckModified extends Model{

  public function getLayoutList(){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout");
    return $query->rows;
  }

  public function updateCheckModified($data){
    $array = array("product", "category", "news", "information", "manufacturer");
    foreach ($data as $key => $value) {
      $all_data = $this->db->query("SELECT DISTINCT date_id FROM " . DB_PREFIX . "date_modified WHERE layout_id = '" . (int)$value . "'");
      if(!$all_data->num_rows){

        $route = $this->db->query("SELECT route FROM " . DB_PREFIX . "layout_route WHERE layout_id = '" . $value . "'" );

        if (in_array(str_replace("/","",stristr($route->row['route'],'/')), $array)) {
          $variable = str_replace("/","",stristr($route->row['route'],'/'));
          $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified, layout_id) SELECT CONCAT( '" . $variable ."_id=', " . $variable."_id) AS page, NOW(), '". $value ."' FROM " . DB_PREFIX . "$variable " );
          }else{
          $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified SET layout_id = '" . $value . "', page= '" . $route->row['route'] . "', date_modified = NOW() ");
        }
      }
    }
  }

  public function deleteCheckModified($data){
    foreach ($data as $layout_id) {
      $this->db->query("DELETE FROM " . DB_PREFIX . "date_modified WHERE layout_id = '" . (int)$layout_id . "'");
    }
  }

  //FOR PRODUCT

  //for product comments
  public function updateDateMofifiedProductComment($product_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE page = CONCAT('product_id=','" . $product_id . "')");
  }

  //For operations with products

   public function addDateModifiedAddProduct($product_id){
    $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) VALUES(CONCAT('product_id=','" . $product_id . "') AS page, NOW())");
  }
  public function editDateModifiedEditProduct($product_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE page = CONCAT('product_id=','" . $product_id . "')");
  }
  public function deleteDateModifiedDeleteProduct($product_id){
    $this->db->query("DELETE FROM " . DB_PREFIX . "date_modified WHERE page= CONCAT('product_id=','" . $product_id . "')");

    $category_id = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id ='" . (int)$product_id . "'");
    $this->editDateModifiedEditCategory($category_id->row['category_id']);
  }
  //for attribute

  public function editDateModifiedEditAtribute($attribute_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified dm INNER JOIN " . DB_PREFIX . "product_attribute pa ON dm.page = CONCAT('product_id=', pa.product_id) SET dm.date_modified = NOW() WHERE pa.attribute_id = '" . $attribute_id . "'" );
  }

  public function editDateModifiedEditOption($option_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified dm INNER JOIN " . DB_PREFIX . "product_option po ON dm.page = CONCAT('product_id=', po.product_id) SET dm.date_modified = NOW() WHERE po.option_id = '" . $option_id . "'" );
  }

  public function addDateModifiedAddCategory($category_id){
    $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) VALUES(CONCAT('category_id=','" . $category_id . "') AS page, NOW())");
  }

  public function editDateModifiedEditCategory($category_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE page = CONCAT('category_id=','" . $category_id . "')");

    $main_category = $this->db->query("SELECT main_category FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . $category_id . "'");
    if($main_category->row['main_category'] == 1 ){
      $this->db->query("UPDATE " . DB_PREFIX . "date_modified dm INNER JOIN " . DB_PREFIX . "product_to_category pc ON dm.page = CONCAT('product_id=', pc.product_id) SET dm.date_modified = NOW() WHERE pc.category_id = '" . $category_id . "'" );
    }

    $this->db->query("UPDATE " . DB_PREFIX . "date_modified dm INNER JOIN " . DB_PREFIX . "category c ON  dm.page = CONCAT('category_id=', c.category_id) SET dm.date_modified = NOW() WHERE c.parent_id ='" . $category_id . "'" );

    $parent_id = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category WHERE category_id = '" . $category_id . "'" );
    if ($parent_id->row['parent_id']!=0){
      $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW WHERE category_id = '" . $parent_id . "'");
    }
  }

  public function deleteDateModifiedDeleteCategory($category_id){
    $this->db->query("DELETE FROM " . DB_PREFIX . "date_modified WHERE page= CONCAT('category_id=','" . $category_id . "')");

    $parent_id = $this->db->query("SELECT parent_id FROM" . DB_PREFIX . "category WHERE category_id = '" . $category_id . "'");
    if($parent_id->row['parent_id'] != 0){


      $this->db->query("UPDATE " . DB_PREFIX . "date_modified dm INNER JOIN " . DB_PREFIX . "product_to_category pc ON dm.page = CONCAT('product_id=', pc.product_id) SET dm.date_modified = NOW() WHERE pc.category_id = '" . $category_id . "'" );
    }

    $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE page = CONCAT('category_id=','" . $category_id . "')");

    $main_category = $this->db->query("SELECT main_category FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . $category_id . "'");

    if($main_category->row['main_category'] == 1 ){
      $this->db->query("UPDATE " . DB_PREFIX . "date_modified dm INNER JOIN " . DB_PREFIX . "product_to_category pc ON dm.page = CONCAT('product_id=', pa.product_id) SET dm.date_modified = NOW() WHERE pc.category_id = '" . $category_id . "'" );
    }

    $this->db->query("UPDATE " . DB_PREFIX . "date_modified dm INNER JOIN " . DB_PREFIX . "category c ON  dm.page = CONCAT('category_id=', c.category_id) SET dm.date_modified = NOW() WHERE c.parent_id ='" . $category_id . "'" );
    $parent_id = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category WHERE category_id = '" . $category_id . "'" );

    if ($parent_id->row['parent_id']!=0){
      $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW WHERE category_id = '" . $parent_id . "'");
    }

  }

  public function addDateModifiedAddInformation($information_id){
    $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) VALUES(CONCAT('information_id=','" . $information_id . "') AS page, NOW())");
  }

  public function editDateModifiedEditInformation($information_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE page = CONCAT('information_id=','" . $information_id . "')");
  }

  public function deleteDateModifiedDeleteInformation($information_id){
    $this->db->query("DELETE FROM " . DB_PREFIX . "date_modified WHERE page= CONCAT('information_id=','" . $information_id . "')");
  }

  public function addDateModifiedAddNews($news_id){
    $this->db->query("INSERT INTO " . DB_PREFIX . "date_modified (page, date_modified) VALUES(CONCAT('news_id=','" . $news_id . "') AS page, NOW())");

    $this->load->model('extension/news');

    $prev_news_id = $this->model_extension_news->prevNews($news_id);
    $this->editDateModifiedEditNews($prev_news_id);
  }

  public function editDateModifiedEditNews($news_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE page = CONCAT('news_id=','" . $news_id . "')");
  }

  public function deleteDateModifiedDeleteNews($news_id){
    $this->db->query("DELETE FROM " . DB_PREFIX . "date_modified WHERE page= CONCAT('news_id=','" . $news_id . "')");

    $this->load->model('extension/news');

    $prev_news_id = $this->model_extension_news->prevNews($news_id);
    if($prev_news_id){
      $this->editDateModifiedEditNews($prev_news_id);
    }

    $next_news_id = $this->model_extension_news->nextNews($news_id);

    if($next_news_id){
      $this->editDateModifiedEditNews($next_news_id);
    }
  }

  public function UpdateDateMofifiedNewsComment($news_id){
    $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE page = CONCAT('news_id=','" . $news_id . "')");
  }

  public function updateDateModifiedEditLayout($layout_id){
    $layouts_id = $this->db->query("SELECT DISTINCT layout_id FROM " . DB_PREFIX . "date_modified");
    if (in_array($layout_id, $layouts_id->rows)) {
      $this->db->query("UPDATE " . DB_PREFIX . "date_modified SET date_modified = NOW() WHERE layout_id ='" . $layout_id . "'" );
    }
  }

  public function updateDateModifiedModule($module_id){
    // $this->load->model('extension/module');
    //
    // $module = $this->model_extension_module->getModule($module_id);
    //
    // $layout_id = $this->db->query("SELECT layout_id FROM " . DB_PREFIX . "layout_module WHERE code= CONCAT('" . $module['code'] . "', '.', '" . $module_id . "')");
    // if($layout_id->num_rows){
    //   $this-> updateDateModifiedEditLayout($layout_id->row['layout_id']);
    // }
  }

  public function updateDateModifiedSettings($code){
    $layout_id = $this->db->query("SELECT layout_id FROM " . DB_PREFIX . "layout_module WHERE code= '" . $code . "'" );

    if($layout_id->num_rows){
      $this-> updateDateModifiedEditLayout($layout_id->row['layout_id']);
    }
  }

}
