<?php

class ModelModuleSaleEvents extends Model
{

  public function getSaleEventType(){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_event_type");

    return $query->rows;
  }

  public function editSaleEventType($data = array()){


    $query = $this->getSaleEventType();

    $types_id = array_column($query, 'sale_event_type_id');

    $types_post_id = array_column($data, 'sale_event_type_id');

    $deleted_id = implode(",", array_diff($types_id,$types_post_id));

    if($deleted_id !==''){
      $this->db->query("DELETE FROM " . DB_PREFIX . "sale_event_type WHERE sale_event_type_id IN (" . $deleted_id . ")");
    }

    foreach($data as $mydata){
      if(isset($mydata['sale_event_type_id'])){
        $this->db->query("UPDATE " . DB_PREFIX . "sale_event_type SET name = '" . $this->db->escape($mydata['name']) . "', type = '" . $this->db->escape($mydata['type']) . "' WHERE sale_event_type_id = '" . $this->db->escape($mydata['sale_event_type_id']) . "'");
      }else{
        $this->db->query("INSERT INTO " . DB_PREFIX . "sale_event_type (name, type) VALUES ('" . $this->db->escape($mydata['name']) . "', '" . $this->db->escape($mydata['type']) . "')");
      }
    }
  }
}
