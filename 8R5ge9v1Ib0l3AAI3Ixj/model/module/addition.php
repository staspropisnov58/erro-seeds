<?php
class ModelModuleAddition extends Model
{
  public function getAdditionProduct($product_id)
  {
    $query = $this->db->query("SELECT addition_product_id, addition_id, product_id, dependencies_value FROM " . DB_PREFIX . "addition_product WHERE product_id = '" . (int)$product_id . "'");

		if ($query->row) {
      $data = array(
        'addition_product_id' => $query->row['addition_product_id'],
        'addition_id'         => $query->row['addition_id'],
        'product_id'          => $query->row['product_id'],
        'dependencies_value' => unserialize($query->row['dependencies_value'])
      );
			return $data;
		} else {
			return array();
		}
  }

  public function addAdditionProduct($data, $product_id)
  {
    $this->db->query("INSERT INTO " . DB_PREFIX . "addition_product (addition_id, product_id, dependencies_value)
    VALUES (" . (int)$data['addition_id'] . ", " . (int)$product_id . ", '" . $this->db->escape(serialize($data['dependencies'])) . "')");
  }

  public function editAdditionProduct($data)
  {
    $this->db->query("UPDATE " . DB_PREFIX . "addition_product SET dependencies_value = '" . $this->db->escape(serialize($data['dependencies'])) . "' WHERE addition_product_id = " .  (int)$data['addition_product_id']);
  }

  public function deleteAdditionProduct($addition_product_id) {
    $this->db->query("DELETE FROM " . DB_PREFIX . "addition_product WHERE addition_product_id = " .  (int)$addition_product_id);
  }
}
