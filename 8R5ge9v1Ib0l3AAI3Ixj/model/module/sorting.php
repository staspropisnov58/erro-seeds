<?php
class ModelModuleSorting extends Model{
  public function getLayoutsWithModule($code)
  {
      $layouts = [];

      $query = $this->db->query("SELECT lm.layout_id, l.name, lr.route
                            FROM " . DB_PREFIX . "layout_module lm
                            INNER JOIN " . DB_PREFIX . "layout l ON lm.layout_id = l.layout_id
                            INNER JOIN " . DB_PREFIX . "layout_route lr ON l.layout_id = lr.layout_id
                             WHERE lm.code= '" . $code . "'");

      if ($query->num_rows) {
          $layouts = array_map(function ($row) {
              return [
        'layout_id' => $row['layout_id'],
        'name'      => $row['name'],
        'route'     => $row['route'],
        'code'      => str_replace('/', '_', $row['route']),
      ];
          }, $query->rows);
      }

      return $layouts;
  }
}
