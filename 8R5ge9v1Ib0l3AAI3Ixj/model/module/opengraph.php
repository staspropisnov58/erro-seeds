<?php
class ModelModuleOpengraph extends Model
{

  public function getAllPages(){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "page WHERE route != 'common/home'");

    return $query->rows;
  }

  public function getPage($page_id){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "page WHERE page_id = '" . (int)$page_id . "'");

    $page_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "page_description WHERE page_id ='" . (int)$page_id . "'");

    $page_description = array();

    if($page_description_query->num_rows){
      foreach ($page_description_query->rows as $value){
        $page_description[$value['language_id']] = array(
          'title'       => $value['og_title'],
          'description' => $value['og_description'],
          'image'       => $value['og_image'],
        );
      }
    }

    $page_descriptions = array(
      'page_id'          => $query->row['page_id'],
      'name'             => $query->row['name'],
      'route'            => $query->row['route'],
      'page_description' => $page_description
    );

    return $page_descriptions;

  }

  public function addPage($page_data){
    $this->db->query("INSERT INTO " . DB_PREFIX . "page SET name = '" . $this->db->escape($page_data['name']) . "', route = '" . $this->db->escape($page_data['route']) . "'");
    $page_id = $this->db->getLastId();
    foreach ($page_data as $key => $value){
      foreach($value as $key1 => $value1){
        if(is_numeric($key1)){
          $this->db->query("INSERT INTO " . DB_PREFIX . "page_description SET page_id = '" . (int)$page_id . "', og_title = '" . $this->db->escape($value1['title']) . "', og_description = '" . $this->db->escape($value1['description']) . "', og_image = '" . $this->db->escape($value1['image']) . "', language_id = '" . (int)$key1 . "'");
        }
      }
    }
  }

  public function editPage($page_data, $page_id){
    $this->db->query("UPDATE " . DB_PREFIX . "page SET name = '" . $this->db->escape($page_data['name']) . "', route = '" . $this->db->escape($page_data['route']) . "' WHERE page_id = '" . (int)$page_id . "'");
    foreach ($page_data['page_description'] as $key => $value){
      if(is_numeric($key)){
        $this->db->query("UPDATE " . DB_PREFIX . "page_description SET page_id = '" . (int)$page_id . "', og_title = '" . $this->db->escape($value['title']) . "', og_description = '" . $this->db->escape($value['description']) . "', og_image = '" . $this->db->escape($value['image']) . "' WHERE language_id = '" . (int)$key . "' AND page_id = '" . (int)$page_id . "'");
      }
    }
  }

  public function deletePage($page_id){
    $this->db->query("DELETE FROM " . DB_PREFIX . "page WHERE page_id = '" . (int)$page_id . "'");
    $this->db->query("DELETE FROM " . DB_PREFIX . "page_description WHERE page_id = '" . (int)$page_id . "'");
  }

}
