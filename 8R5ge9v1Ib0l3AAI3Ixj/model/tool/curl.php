<?php
class ModelToolCurl extends Model {

  public function getCurl($data, $url_data, $url, $route){


    $curl = curl_init();

    // Set SSL if required
    if (substr($url, 0, 5) == 'https') {
        curl_setopt($curl, CURLOPT_PORT, 443);
    }

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_URL, $url . 'index.php?route=' . $route . ($url_data ? '&' . http_build_query($url_data) : ''));


    if ($data) {
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    }

    curl_setopt($curl, CURLOPT_COOKIE, session_name() . '=' . $this->session->data['cookie'] . ';');


    $json = curl_exec($curl);

    curl_close($curl);



    return $json;

  }

  public function getCurlGet($data, $url_data, $url, $route){

    $curl = curl_init();

    // Set SSL if required
    if (substr($url, 0, 5) == 'https') {
        curl_setopt($curl, CURLOPT_PORT, 443);
    }

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERPWD, 'camille_lang:gUHkHxLCupCeh4bE');
    curl_setopt($curl, CURLOPT_URL, $url . 'index.php?route=' . $route . ($url_data ? '&' . http_build_query($url_data) : ''));

    if ($data) {
      curl_setopt($curl, CURLOPT_HTTPGET, true);
      // curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    }

    curl_setopt($curl, CURLOPT_COOKIE, session_name() . '=' . $this->session->data['cookie'] . ';');


    $json = curl_exec($curl);

    curl_close($curl);

    return $json;
  }

  public function getCurlGetCli($data, $url_data, $url, $route){

    $url_data['cli_token'] = CLI_TOKEN;

    $curl = curl_init();

    // Set SSL if required
    if (substr($url, 0, 5) == 'https') {
        curl_setopt($curl, CURLOPT_PORT, 443);
    }

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERPWD, 'camille_lang:gUHkHxLCupCeh4bE');
    curl_setopt($curl, CURLOPT_URL, trim($url . '?' . ($url_data ? http_build_query($url_data) . '&'  : ''), '&'));


    if ($data) {
      curl_setopt($curl, CURLOPT_HTTPGET, true);
    }



    $json = curl_exec($curl);
    $json = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);

    return $json;
  }

}
