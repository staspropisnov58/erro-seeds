<?php
class ModelToolTasks extends Model {

  public function getTasks($data = array()){
    $sql = "SELECT * FROM " . DB_PREFIX . "task WHERE frequency ";

    if(isset($data['frequency']) && $data['frequency'] === 'once'){
      $sql .= " = 'once'";
    }else{
      $sql .= " != 'once'";
    }

    $sort_data = array(
			'action',
			'datetime_run',
			'frequency',

		);
    if($data['sort'] === 'frequency'){
      $sql .= " ORDER BY FIELD(frequency, 'once','minute','hour','day','week','month','year')";
    }elseif (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else{
      $sql .= " ORDER BY datetime_run";
    }

    if(isset($data['order']) && $data['order'] == 'ASC'){
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }

  public function deleteTask($task_id){
    $this->db->query("DELETE FROM " . DB_PREFIX . "task WHERE task_id = '" . (int)$task_id . "'");
  }

  public function getTotalTasks($data = array()){
    $sql = "SELECT COUNT(task_id) as count FROM " . DB_PREFIX . "task WHERE frequency ";

    if(isset($data['frequency']) && $data['frequency'] === 'once'){
      $sql .= " = 'once'";
    }else{
      $sql .= " != 'once'";
    }

    $sort_data = array(
			'action',
			'datetime_run',
			'frequency',
		);
    if($data['sort'] === 'frequency'){
      $sql .= " ORDER BY FIELD(frequency, 'once','minute','hour','day','week','month','year')";
    }elseif (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
      $sql .= " ORDER BY " . $data['sort'];
    } else{
      $sql .= " ORDER BY datetime_run";
    }

    if(isset($data['sort'])){
      $sql .= " DESC";
    } else {
      $sql .= " ASC";
    }

    $query = $this->db->query($sql);

    return $query->row['count'];
  }

  public function addTask($data){

    $this->db->query("INSERT INTO " . DB_PREFIX . "task SET datetime_run = '" . $data['datetime_run'] . "', action= '" . $data['controller_action'] . "', args = '" . serialize(explode(",", $data['args'])) . "', frequency = '" . $data['frequency'] . "'");

    return $this->db->getLastId();

  }

  public function editTask($data, $task_id){
    $this->db->query("UPDATE " . DB_PREFIX . "task SET datetime_run = '" . $data['datetime_run'] . "', action= '" . $data['controller_action'] . "', args = '" . serialize(explode(",", $data['args'])) . "', frequency = '" . $data['frequency'] . "' WHERE task_id = '" . (int)$task_id . "'");
  }


  public function getTask($task_id){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "task WHERE task_id = '" . $task_id . "'");

    return $query->row;
  }
}
