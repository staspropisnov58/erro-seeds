<?php
class ModelCatalogProduct extends Model {
    public function addProduct($data) {
        $this->event->trigger('pre.admin.product.add', $data);

        $sql = "INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', meta_robots = '" . $this->db->escape($data['meta_robots']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', maximum = '" . (int)$data['maximum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', order_pre = '" . (int)$data['order_pre'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "', sort_order = '" . (int)$data['sort_order'] . "'";

        if($this->config->get('product_stickers_status')){
            $sql .= ", main_stickers = '" . $this->db->escape(serialize($data['main_stickers'])) . "', deprecated_stickers = '" . $this->db->escape(serialize($data['deprecated_stickers'])) . "'";
        }

        $sql .= ", date_added = NOW()";

        $this->db->query($sql);

        $product_id = $this->db->getLastId();

        $this->db->query("INSERT INTO " . DB_PREFIX . "1c_product SET product_id = '" . (int)$product_id . "', 1c_product_id = '" . (int)$data['product_id_1c'] . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', seo_text = '" . $this->db->escape($value['seo_text']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', alt_image = '" . $this->db->escape($value['alt_image']) . "', title_image = '" . $this->db->escape($value['title_image']) . "'");
        }

        if($this->config->get('opengraph_use_meta_when_no_og') !== NULL){
            if($this->config->get('opengraph_status')){
                foreach ($data['opengraph'] as $language_id => $value){
                    $this->db->query("UPDATE " . DB_PREFIX . "product_description SET og_title = '" . $this->db->escape($value['title']) . "', og_description = '" . $this->db->escape($value['description']) . "', og_image = '" . $this->db->escape($value['image']) . "' WHERE language_id = '" . (int)$language_id . "' AND product_id = '" . (int)$product_id . "'");
                }
            }
        }

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "', quantity_to_subtract_option = '" . $product_option['quantity_to_subtract_option'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', status = '" . (int)$product_option_value['status'] . "', price = '" . (float)$product_option_value['price'] . "', price_dev = '" . (float)$product_option_value['price_dev'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "', quantity_to_subtract_option = '" . $product_option['quantity_to_subtract_option'] . "'");
                }
            }
        }

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $special_id = 0;

                $sql = "INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'";

                if($this->config->get('sale_events_status')!== NULL){
                    if((int)$product_special['sale_event_id'] === 0){
                        $product_special['status'] = 0;
                    }
                    $sql .= ", sale_event_id = '" . (int)$product_special['sale_event_id'] . "', status = '" . $this->db->escape($product_special['status']) . "'";
                }

                $this->db->query($sql);
                $special_id = $this->db->getLastId();

                foreach ($data['product_option_special'][$key] as $product_option_value_id => $product_option_value_special) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_special SET price= '" . (int)$product_option_value_special . "', product_id = '" . (int)$product_id . "', special_id = '" . (int)$special_id . "', product_option_value_id= '" . $product_option_value_ids[$product_option_value_id] . "'"  );
                }
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }
        if($this->config->get('recomended_products_status') !== NULL && $this->config->get('recomended_products_status')){
            if (isset($data['product_recomended'])) {
                foreach ($data['product_recomended'] as $recomended_id) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_recomended WHERE product_id = '" . (int)$product_id . "' AND recomended_id = '" . (int)$recomended_id . "'");
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_recomended SET product_id = '" . (int)$product_id . "', recomended_id = '" . (int)$recomended_id . "'");

                }
            }
        }

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
            }
        }

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
        }

        if (isset($data['product_recurrings'])) {
            foreach ($data['product_recurrings'] as $recurring) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
            }
        }

        $this->cache->delete('product');

        //add column in date_modified
        if($this->config->get('check_modified')){
            $this->load->model('module/check_modified');
            $this->model_module_check_modified->addDateModifiedAddProduct($product_id);
        }
        $this->event->trigger('post.admin.product.add', $product_id);

        return $product_id;
    }

    public function editProduct($product_id, $data) {

        $this->event->trigger('pre.admin.product.edit', $data);

        $sql = "UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', meta_robots = '" . $this->db->escape($data['meta_robots']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', maximum = '" . (int)$data['maximum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', order_pre = '" . (int)$data['order_pre'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "', sort_order = '" . (int)$data['sort_order'] . "'";

        if($this->config->get('product_stickers_status')){
            $sql .= ", main_stickers = '" . $this->db->escape(serialize($data['main_stickers'])) . "', deprecated_stickers = '" . $this->db->escape(serialize($data['deprecated_stickers'])) . "'";
        }

        $sql .= ", date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'";

        $this->db->query($sql);

        $this->db->query("UPDATE " . DB_PREFIX . "1c_product SET 1c_product_id = '" . $data['product_id_1c'] . "' WHERE product_id = " . (int)$product_id);

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', seo_text = '" . $this->db->escape($value['seo_text']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', alt_image = '" . $this->db->escape($value['alt_image']) . "', title_image = '" . $this->db->escape($value['title_image']) . "'");
        }

        if($this->config->get('opengraph_use_meta_when_no_og') !== NULL){
            if($this->config->get('opengraph_status')){
                foreach ($data['opengraph'] as $language_id => $value){
                    $this->db->query("UPDATE " . DB_PREFIX . "product_description SET og_title = '" . $this->db->escape($value['title']) . "', og_description = '" . $this->db->escape($value['description']) . "', og_image = '" . $this->db->escape($value['image']) . "' WHERE language_id = '" . (int)$language_id . "' AND product_id = '" . (int)$product_id . "'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

        if (!empty($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {



                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "', quantity_to_subtract_option = '" . $product_option['quantity_to_subtract_option'] . "'");

                        $product_option_id = $this->db->getLastId();



                        foreach ($product_option['product_option_value'] as $key => $product_option_value) {
//                            print_r();
//                            exit();
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', status = '" . (int)$product_option_value['status'] . "', price = '" . (float)$product_option_value['price'] . "', price_dev = '" . (float)$product_option_value['price_dev'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");

                            $product_option_value_ids[$product_option_value['option_value_id']] = $this->db->getLastId();
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "', quantity_to_subtract_option = '" . $product_option['quantity_to_subtract_option'] . "'");
                    $product_option_id = $product_option['product_option_id'];

                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_special WHERE product_id = '" . (int)$product_id . "'" );

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $key => $product_special) {
                $special_id = 0;
                $sql = "INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'";

                if($this->config->get('sale_events_status')!== NULL){
                    $sql .= ", sale_event_id = '" . (int)$product_special['sale_event_id'] . "', status = '" . $this->db->escape($product_special['status']) . "'";
                }

                $this->db->query($sql);

                $special_id = $this->db->getLastId();
                foreach ($data['product_option_special'][$key] as $product_option_value_id => $product_option_value_special) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_special SET price= '" . (float)$product_option_value_special . "', product_id = '" . (int)$product_id . "', special_id = '" . (int)$special_id . "', product_option_value_id= '" . $product_option_value_ids[$product_option_value_id] . "'" );
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_recomended WHERE product_id = '" . (int)$product_id . "'");

        if($this->config->get('recomended_products_status') !== NULL && $this->config->get('recomended_products_status')){
            if (isset($data['product_recomended'])) {
                foreach ($data['product_recomended'] as $recomended_id) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_recomended WHERE product_id = '" . (int)$product_id . "' AND recomended_id = '" . (int)$recomended_id . "'");
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_recomended SET product_id = '" . (int)$product_id . "', recomended_id = '" . (int)$recomended_id . "'");

                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
        }

        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = " . (int)$product_id);

        if (isset($data['product_recurrings'])) {
            foreach ($data['product_recurrings'] as $recurring) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
            }
        }

        $this->cache->delete('product');

        //update column date_modified for product which was changed

        if($this->config->get('check_modified')){
            $this->load->model('module/check_modified');
            $this->model_module_check_modified->editDateModifiedEditProduct($product_id);
        }
        $this->event->trigger('post.admin.product.edit', $product_id);
    }

    public function getSpecialNow($product_id){
        $query = $this->db->query("SELECT product_special_id FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

        if($query->num_rows){
            return $query->row['product_special_id'];
        }else{
            return 0;
        }
    }
    public function upSku($data){
        $this->db->query("UPDATE `" . DB_PREFIX . "product` SET `sku`='".$data['sku']."' WHERE `product_id`=".$data['product_id']."");
    }
    public function upQa($data){
        if((float)$data['quantity']<0||(float)$data['quantity']==0){
            (float)$data['quantity']=0.000;
        }
        $this->db->query("UPDATE `" . DB_PREFIX . "product` SET `quantity`='".(float)$data['quantity']."' WHERE `product_id`=".$data['product_id']."");

    }

    public function getProductOptionSpecial($product_id){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_special WHERE product_id= '" . $product_id . "'");

        return $query->rows;
    }

    public function copyProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
            $data = array();

            $data = $query->row;

            $data['sku'] = '';
            $data['upc'] = '';
            $data['viewed'] = '0';
            $data['keyword'] = '';
            $data['status'] = '0';

            $data = array_merge($data, array('product_attribute' => $this->getProductAttributes($product_id)));
            $data = array_merge($data, array('product_description' => $this->getProductDescriptions($product_id)));
            $data = array_merge($data, array('product_discount' => $this->getProductDiscounts($product_id)));
            $data = array_merge($data, array('product_filter' => $this->getProductFilters($product_id)));
            $data = array_merge($data, array('product_image' => $this->getProductImages($product_id)));
            $data = array_merge($data, array('product_option' => $this->getProductOptions($product_id)));
            $data = array_merge($data, array('product_related' => $this->getProductRelated($product_id)));
            $data = array_merge($data, array('product_reward' => $this->getProductRewards($product_id)));
            $data = array_merge($data, array('product_special' => $this->getProductSpecials($product_id)));
            $data = array_merge($data, array('product_category' => $this->getProductCategories($product_id)));
            $data = array_merge($data, array('product_download' => $this->getProductDownloads($product_id)));
            $data = array_merge($data, array('product_layout' => $this->getProductLayouts($product_id)));
            $data = array_merge($data, array('product_store' => $this->getProductStores($product_id)));
            $data = array_merge($data, array('product_recurrings' => $this->getRecurrings($product_id)));

            $this->addProduct($data);
        }
    }

    public function deleteProduct($product_id) {
        $this->event->trigger('pre.admin.product.delete', $product_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "1c_product WHERE product_id = '" . (int)$product_id . "'");
        //delete note from date_modified for this product
        if($this->config->get('check_modified')){
            $this->load->model('module/check_modified');
            $this->model_module_check_modified->deleteDateModifiedDeleteProduct($product_id);
        }
        $this->cache->delete('product');

        $this->event->trigger('post.admin.product.delete', $product_id);
    }

    public function getProduct($product_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT 1c_product_id FROM " . DB_PREFIX . "1c_product WHERE product_id = '" . (int)$product_id . "') as product_id_1c, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getOptionPrice($product_option_value_id){
        $query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_option_special WHERE product_option_value_id='" . $product_option_value_id . "'");
        if($query->num_rows){
            return $query->row['price'];
        }else{
            return 0;
        }
    }

    public function getSpecial($product_option_value_id){
        $query = $this->db->query("SELECT special_id FROM " . DB_PREFIX . "product_option_special WHERE product_option_value_id='" . $product_option_value_id . "'");
        return $query->row['special_id'];
    }

    public function getProducts($data = array()) {
        $query = $this->db->query("SELECT DISTINCT customer_group_id FROM " . DB_PREFIX . "customer_group ");

        foreach ($query->rows as $key => $customer_group_id){
            if($key == 0){
                $string = "ps.customer_group_id = " . $customer_group_id['customer_group_id'] . "";
            }else{
                $string .= " OR ps.customer_group_id = " . $customer_group_id['customer_group_id'] . "";
            }
        }



        $sql = "SELECT *,( SELECT price FROM " . DB_PREFIX . "product_special ps WHERE  ps.product_id = p.product_id AND (" . $string . ") AND ( ( ps.date_start = '0000-00-00' OR ps.date_start < NOW() ) AND ( ps.date_end = '0000-00-00' OR ps.date_end > NOW() ) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) ";

        if(!empty($data['store_id'])){
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) ";
        }

        if(!empty($data['filter_category_id'])){
            $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) ";
        }

        $sql .=" WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if(!empty($data['filter_category_id'])){
            $sql .= " AND p2c.category_id = '" . $data['filter_category_id'] . "'";
        }

        if(!empty($data['quantity'])){
            $sql .= " AND p.quantity > 0";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (!empty($data['filter_sku'])) {
            $sql .= " AND p.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
        }

        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = " . (int)$data['filter_manufacturer_id'];
        }

        if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
        }

        if($this->config->get('product_stickers_status')){
            if(isset($data['filter_main_sticker']) && !is_null($data['filter_main_sticker'])){
                if($data['filter_main_sticker'] === 'with_stickers'){
                    $sql .= " AND p.main_stickers IS NOT NULL AND p.main_stickers != 'a:0:{}'";
                }elseif($data['filter_main_sticker'] === 'without_stickers'){
                    $sql .= " AND p.main_stickers = 'a:0:{}'";
                }elseif($data['filter_main_sticker'] !== '' && $data['filter_main_sticker'] !== 'undefined'){
                    $sql .= " AND p.main_stickers LIKE '%" . $this->db->escape($data['filter_main_sticker']) . "%'";
                }
            }
        }

        if(!empty($data['status'])){
            $sql .= " AND p.status = '" . (int)$data['status'] . "' ";
        }

        if(!empty($data['store_id'])){
            $sql .= " AND p2s.store_id = '" . (int)$data['store_id'] . "'";
        }

        if (isset($data['filter_special']) && !is_null($data['filter_special'])) {

            if($data['filter_special'] == 'only_specials'){

                $sql .= " AND ( SELECT price FROM " . DB_PREFIX . "product_special ps WHERE  ps.product_id = p.product_id AND (" . $string. ") AND ( ( ps.date_start = '0000-00-00' OR ps.date_start < NOW() ) AND ( ps.date_end = '0000-00-00' OR ps.date_end > NOW() ) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) IS NOT NULL ";

            }elseif($data['filter_special'] == 'not_specials'){

                $sql .= " AND ( SELECT price FROM " . DB_PREFIX . "product_special ps WHERE  ps.product_id = p.product_id AND (" . $string. ") AND ( ( ps.date_start = '0000-00-00' OR ps.date_start < NOW() ) AND ( ps.date_end = '0000-00-00' OR ps.date_end > NOW() ) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) IS NULL ";

            }else{

                $range = explode("-", $data['filter_special']);

                $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "product WHERE ((price -( SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ( ( ps.date_start = '0000-00-00' OR ps.date_start < NOW() ) AND ( ps.date_end = '0000-00-00' OR ps.date_end > NOW() ) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1 ))/(price/100)) BETWEEN '" . $range[0] . "' AND '" . $range[1] . "') ";

            }
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        $sql .= " GROUP BY p.product_id";

        $sort_data = array(
            'pd.name',
            'p.sku',
            'p.model',
            'p.price',
            'p.quantity',
            'p.status',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } elseif(isset($data['sort']) && $data['sort'] == 'special' && $data['order'] == 'ASC') {
            $sql .= " ORDER BY - " . $data['sort'];
        }elseif(isset($data['sort']) && $data['sort'] == 'special' && $data['order'] == 'DESC'){
            $sql .= " ORDER BY " . $data['sort'];
        }else{
            $sql .= " ORDER BY pd.name";
        }

        if(isset($data['sort']) && $data['sort'] == 'special'){
            $sql .= " DESC";
        }elseif (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if($data['limit'] !== 'all'){
            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }
        }


        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductsByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    public function getProductDescriptions($product_id) {
        $product_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name'             => $result['name'],
                'seo_text'				 => $result['seo_text'],
                'description'      => $result['description'],
                'meta_title'       => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword'     => $result['meta_keyword'],
                'alt_image'				 => $result['alt_image'],
                'title_image'			 => $result['title_image'],
                'tag'              => $result['tag']
            );
        }

        return $product_description_data;
    }

    public function getOpengraph($product_id){
        $opengraph = array();
        $query = $this->db->query("SELECT og_title, og_description, og_image, language_id FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $opengraph[$result['language_id']] = array(
                'title'       => $result['og_title'],
                'description'	=> $result['og_description'],
                'image'      	=> $result['og_image'],
            );
        }

        return $opengraph;
    }

    public function getProductCategories($product_id) {
        $product_category_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_category_data[] = $result['category_id'];
        }

        return $product_category_data;
    }

    public function getProductFilters($product_id) {
        $product_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_filter_data[] = $result['filter_id'];
        }

        return $product_filter_data;
    }

    public function getProductAttributes($product_id) {
        $product_attribute_data = array();

        $product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");

        foreach ($product_attribute_query->rows as $product_attribute) {
            $product_attribute_description_data = array();

            $product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

            foreach ($product_attribute_description_query->rows as $product_attribute_description) {
                $product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
            }

            $product_attribute_data[] = array(
                'attribute_id'                  => $product_attribute['attribute_id'],
                'product_attribute_description' => $product_attribute_description_data
            );
        }

        return $product_attribute_data;
    }

    public function getProductOptions($product_id) {
        $product_option_data = array();

        $product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        foreach ($product_option_query->rows as $product_option) {
            $product_option_value_data = array();

            $product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$product_option['product_option_id'] . "'");

            foreach ($product_option_value_query->rows as $product_option_value) {
                $product_option_value_data[] = array(
                    'product_option_value_id' => $product_option_value['product_option_value_id'],
                    'option_value_id'         => $product_option_value['option_value_id'],
                    'quantity'                => $product_option_value['quantity'],
                    'subtract'                => $product_option_value['subtract'],
                    'price'                   => $product_option_value['price'],
                    'price_dev'                   => $product_option_value['price_dev'],
                    'price_prefix'            => $product_option_value['price_prefix'],
                    'points'                  => $product_option_value['points'],
                    'points_prefix'           => $product_option_value['points_prefix'],
                    'weight'                  => $product_option_value['weight'],
                    'weight_prefix'           => $product_option_value['weight_prefix'],
                    'status'									=> $product_option_value['status']
                );
            }

            $product_option_data[] = array(
                'product_option_id'    => $product_option['product_option_id'],
                'product_option_value' => $product_option_value_data,
                'option_id'            => $product_option['option_id'],
                'name'                 => $product_option['name'],
                'type'                 => $product_option['type'],
                'value'                => $product_option['value'],
                'required'             => $product_option['required'],
                'quantity_to_subtract_option' => $product_option['quantity_to_subtract_option']
            );
        }

        return $product_option_data;
    }
    public function getProductRecomended($product_id) {
        $product_recomended_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_recomended WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_recomended_data[] = $result['recomended_id'];
        }

        return $product_recomended_data;
    }
    public function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getProductDiscounts($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");

        return $query->rows;
    }

    public function getProductSpecials($product_id) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (ps.customer_group_id = cgd.customer_group_id) WHERE ps.product_id = '" . (int)$product_id . "' AND cgd.language_id = '" . $this->config->get('config_language_id') . "' ORDER BY priority, price");

        return $query->rows;
    }

    public function getProductRewards($product_id) {
        $product_reward_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
        }

        return $product_reward_data;
    }

    public function getProductDownloads($product_id) {
        $product_download_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_download_data[] = $result['download_id'];
        }

        return $product_download_data;
    }

    public function getProductStores($product_id) {
        $product_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_store_data[] = $result['store_id'];
        }

        return $product_store_data;
    }

    public function getProductLayouts($product_id) {
        $product_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $product_layout_data;
    }

    public function getProductRelated($product_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }

        return $product_related_data;
    }

    public function getRecurrings($product_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = '" . (int)$product_id . "'");

        return $query->rows;
    }

    public function getProductName($product_id){
        $query = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row['name'];
    }

    public function getTotalProducts($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

        $sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if(isset($data['filter_main_sticker']) && !is_null($data['filter_main_sticker'])){
            $sql .= " AND p.main_stickers LIKE '%" . $this->db->escape($data['filter_main_sticker']) . "%'";
        }

        if (!empty($data['filter_model'])) {
            $sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
        }

        if (!empty($data['filter_sku'])) {
            $sql .= " AND p.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
        }

        if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
            $sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
        }

        if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
            $sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['filter_special']) && !is_null($data['filter_special'])) {

            if($data['filter_special'] == 'only_specials'){

                $sql .= " AND ( SELECT price FROM " . DB_PREFIX . "product_special ps WHERE  ps.product_id = p.product_id AND ( ( ps.date_start = '0000-00-00' OR ps.date_start < NOW() ) AND ( ps.date_end = '0000-00-00' OR ps.date_end > NOW() ) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) IS NOT NULL ";

            }elseif($data['filter_special'] == 'not_specials'){

                $sql .= " AND ( SELECT price FROM " . DB_PREFIX . "product_special ps WHERE  ps.product_id = p.product_id  AND ( ( ps.date_start = '0000-00-00' OR ps.date_start < NOW() ) AND ( ps.date_end = '0000-00-00' OR ps.date_end > NOW() ) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) IS NULL ";

            }else{

                $range = explode("-", $data['filter_special']);

                $sql .= " AND p.product_id IN (SELECT product_id FROM " . DB_PREFIX . "product WHERE ((price -( SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ( ( ps.date_start = '0000-00-00' OR ps.date_start < NOW() ) AND ( ps.date_end = '0000-00-00' OR ps.date_end > NOW() ) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1 ))/(price/100)) BETWEEN '" . $range[0] . "' AND '" . $range[1] . "') ";

            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalProductsByTaxClassId($tax_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByStockStatusId($stock_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByWeightClassId($weight_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByLengthClassId($length_class_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByDownloadId($download_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByManufacturerId($manufacturer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByAttributeId($attribute_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByOptionId($option_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByProfileId($recurring_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_recurring WHERE recurring_id = '" . (int)$recurring_id . "'");

        return $query->row['total'];
    }

    public function getTotalProductsByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }
}
