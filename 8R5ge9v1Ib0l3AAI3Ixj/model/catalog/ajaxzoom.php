<?php
/**
* @version     3.1
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2018 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

class ModelCatalogAjaxzoom extends Model {
	static $axzmh;
	static $zoom;

	public function getUrl() {
		return HTTPS_CATALOG . 'ajaxzoom/';
	}

	public function getUri() {
		$p = parse_url(HTTPS_CATALOG);
		return $p['path'] . 'ajaxzoom/';
	}

	public function getDir() {
		// return str_replace('admin' . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'catalog', '', __DIR__) . 'ajaxzoom' . DIRECTORY_SEPARATOR;
		return DIR_ROOT . 'ajaxzoom' . DIRECTORY_SEPARATOR;

	}

	public function getDirImage(){
		return DIR_ROOT . 'ajaxzoom' . DIRECTORY_SEPARATOR;
	}

	public function jsonEncode($data) {
		return json_encode($data);
	}

	public function jsonDecode($data) {
		return json_decode($data);
	}

	public function tablePrefix() {
		return DB_PREFIX;
	}

	public function getProductImages($id_product) {

		$this->load->model('catalog/product');
		$data = $this->model_catalog_product->getProduct($id_product);
		$data['product_image'] = $this->model_catalog_product->getProductImages($id_product);

		$files = array();
		if(!empty($data['image'])) {
			array_push($files, basename($data['image']));
		}
		if(!empty($data['product_image'])) {
			foreach ($data['product_image'] as $image) {
				array_push($files, basename($image['image']));
			}
		}
		return $files;
	}

	public function upload($path) {
		$input = fopen('php://input', 'r');
		$temp = tmpfile();
		$realSize = stream_copy_to_stream($input, $temp);
		fclose($input);
		if ($realSize != $this->getSize()) {
			return false;
		}
		$this->log->write($path);
		$target = fopen($path, 'w');
		fseek($temp, 0, SEEK_SET);
		stream_copy_to_stream($temp, $target);
		fclose($target);

		return true;
	}

	public function getSize() {
		if (isset($_SERVER['CONTENT_LENGTH'])) {
			return (int)$_SERVER['CONTENT_LENGTH'];
		} else {
			throw new Exception('Getting content length is not supported.');
		}
	}

	public function set360status($id_360, $status) {
		$this->db->query('UPDATE `ajaxzoom360`
			SET status = '.(int)$status.'
			WHERE id_360 = '.(int)$id_360);
	}

	public function deleteSet($id_360set) {
		$id_360 = $this->getSetParent($id_360set);

		if(empty($id_360)) {
			return;
		}

		$id_product = $this->getSetProduct($id_360);

		// clear AZ cache
		$images = $this->get360Images($id_product, $id_360set);

		foreach ($images as $image) {
			$this->deleteImageAZcache($image['filename']);
		}

		$this->db->query('DELETE
			FROM `ajaxzoom360set`
			WHERE id_360set = '.$id_360set);

		$query = $this->db->query('SELECT *
			FROM `ajaxzoom360set`
			WHERE id_360 = '.$id_360);

		if ($query->num_rows <= 0) {
			$this->db->query('DELETE
				FROM `ajaxzoom360`
				WHERE id_360 = '.$id_360);
		}

		$path = $this->getDir().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set;

		$this->deleteDirectory($path);
	}

	public function getSetParent($id_360set) {
		if ($tmp = $query = $this->db->query('SELECT * FROM `ajaxzoom360set` WHERE id_360set = '.(int)$id_360set)->rows) {
			return $tmp[0]['id_360'];
		}
	}

	public function getSetProduct($id_360) {
		if ($tmp = $query = $this->db->query('SELECT * FROM `ajaxzoom360` WHERE id_360 = '.(int)$id_360)->rows) {
			return $tmp[0]['id_product'];
		}
	}

	public function deleteImageAZcache($file) {
		$axzmh = '';
		$zoom = array();

		// Include all classes
		include_once ($this->getDir().'axZm/zoomInc.inc.php');

		if (!ModelCatalogAjaxZoom::$axzmh) {
			ModelCatalogAjaxZoom::$axzmh = $axzmh;
			ModelCatalogAjaxZoom::$zoom = $zoom;
		}

		// What to delete
		$arr_del = array('In' => true, 'Th' => true, 'tC' => true, 'mO' => true, 'Ti' => true);

		// Remove all cache
		if (is_object(ModelCatalogAjaxZoom::$axzmh)) {
			ModelCatalogAjaxZoom::$axzmh->removeAxZm(ModelCatalogAjaxZoom::$zoom, $file, $arr_del, false);
		}
	}

	public function get360Images($id_product, $id_360set = '') {
		$files = array();
		$id_360 = $this->getSetParent($id_360set);
		$dir = $this->getDir().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set;

		if (file_exists($dir) && $handle = opendir($dir)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != '.' && $entry != '..' && $entry != '.htaccess') {
					$files[] = $entry;
				}
			}
			closedir($handle);
		}

		sort($files);

		$res = array();

		foreach ($files as $entry) {
			$tmp = explode('.', $entry);
			$ext = end($tmp);
			$name = preg_replace('|\.'.$ext.'$|', '', $entry);
			$thumb = $this->getUrl().'axZm/zoomLoad.php?';
			$thumb .= 'azImg=' . $this->getUri() . 'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set.'/'.$entry.'&width=100&height=100&qual=90';

			$res[] = array(
				'thumb' => $thumb,
				'filename' => $entry,
				'id' => $name,
				'ext' => $ext
			);
		}

		return $res;
	}

	public function addImagesArc($arcfile, $id_product, $id_360, $id_360set, $delete = '') {
		set_time_limit(0);

		$path = $this->getDir() . 'zip/' . $arcfile;
		$dst = is_dir($path) ? $path : $this->extractArc($path);

		if($dst == false) {
			return false;
		}

		@chmod($dst, 0777);
		$data = $this->getFolderData($dst);

		$query = $this->db->query('SELECT * FROM `ajaxzoom360` WHERE id_360 = ' . (int)$id_360);
		foreach ($query->rows as $result) {
			$tmp[] = $result;
		}

		$name = $tmp[0]['name'];

		$thumb = $this->getUrl().'axZm/zoomLoad.php';
		$thumb .= '?qq=1&azImg360='.$this->getUri().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set;
		$thumb .= '&width=100&height=100&thumbMode=contain';

		$sets = array(array(
			'name' => $name,
			'path' => $thumb,
			'id_360set' => $id_360set,
			'id_360' => $id_360,
			'status' => '1'
		));

		$count_data_folders = count($data['folders']);

		$move = is_dir($path) ? false : true;

		if ($count_data_folders == 0) { // files (360)
			$this->copyImages($id_product, $id_360, $id_360set, $dst, $move);
		} elseif ($count_data_folders == 1) { // 1 folder (360)
			$this->copyImages($id_product, $id_360, $id_360set, $dst.'/'.$data['folders'][0], $move);
		} else {
			// 3d
			$this->copyImages($id_product, $id_360, $id_360set, $dst.'/'.$data['folders'][0], $move);

			// checkr - $i <= $count_data_folders
			for ($i = 1; $i < $count_data_folders; $i++) {
				$this->db->query('INSERT INTO `ajaxzoom360set` (id_360, sort_order) VALUES(\''.$id_360.'\', 0)');

				$id_360set = $this->db->getLastId();

				$this->copyImages($id_product, $id_360, $id_360set, $dst.'/'.$data['folders'][$i], $move);

				$thumb = $this->getUrl().'axZm/zoomLoad.php';
				$thumb .= '?qq=1&azImg360='.$this->getUri().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set;
				$thumb .= '&width=100&height=100&thumbMode=contain';

				$sets[] = array(
					'name' => $name,
					'path' => $thumb,
					'id_360set' => $id_360set,
					'id_360' => $id_360
				);
			}
		}

		// delete temp directory which was created when zip extracted
		if(!is_dir($path)) {
			$this->deleteDirectory($dst);
		}

		// delete the sourece file (zip/dir) if checkbox is checked
		if($delete == 'true') {
			if(is_dir($path)) {
				$this->deleteDirectory($dst);
			} else {
				@unlink($path);
			}
		}
		return $sets;
	}

	public function getFolderData($path) {
		$files = array();
		$folders = array();

		if ($handle = opendir($path)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != '.' && $entry != '..' && $entry != '.htaccess' && $entry != '__MACOSX') {
					if (is_dir($path.'/'.$entry)) {
						array_push($folders, $entry);
					} else {
						array_push($files, $entry);
					}
				}
			}

			closedir($handle);
		}

		sort($folders);
		sort($files);

		return array(
			'folders' => $folders,
			'files' => $files
		);
	}

	public function extractArc($file) {
		$folder = uniqid(getmypid());
		$path = $this->getDir().'pic/tmp/'.$folder;
		mkdir($path, 0777);

		$zip = new ZipArchive;
		$res = $zip->open($file);
		if ($res === TRUE) {
			$zip->extractTo("$path/");
			$zip->close();
			return $path;
		} else {
			return false;
		}
	}

	public function copyImages($id_product, $id_360, $id_360set, $path, $move) {
		if (!$id_360 && !$id_360set) { // useless code to validate
			return;
		}

		$files = $this->getFilesFromFolder($path);
		$folder = $this->createProduct360Folder($id_product, $id_360set);

		foreach ($files as $file) {
			$name = $id_product.'_'.$id_360set.'_'.$this->imgNameFilter($file);
			$dst = $folder.'/'.$name;

			if($move) {
				if(@!rename($path.'/'.$file, $dst)) {
					copy($path.'/'.$file, $dst);
				}
			} else {
				copy($path.'/'.$file, $dst);
			}
		}
	}

	public function getFilesFromFolder($path) {
		$files = array();

		if ($handle = opendir($path)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != '.' && $entry != '..' && $entry != '.htaccess' && $entry != '__MACOSX') {
					$files[] = $entry;
				}
			}

			closedir($handle);
		}

		return $files;
	}

	public function createProduct360Folder($id_product, $id_360set) {
		$id_product = (int)$id_product;
		$id_360set = (int)$id_360set;
		$id_360 = $this->getSetParent($id_360set);

		$this->log->write($this->getDirImage().'pic/360/'.$id_product);
		$this->log->write('hui');

		if (!file_exists($this->getDirImage().'pic/360/'.$id_product)) {
			mkdir($this->getDirImage().'pic/360/'.$id_product, 0775);
		}

		if (!file_exists($this->getDirImage().'pic/360/'.$id_product.'/th')) {
			mkdir($this->getDirImage().'pic/360/'.$id_product.'/th', 0775);
		}

		if (!file_exists($this->getDirImage().'pic/360/'.$id_product.'/'.$id_360)) {
			mkdir($this->getDirImage().'pic/360/'.$id_product.'/'.$id_360, 0775);
		}

		$folder = $this->getDirImage().'pic/360/'.$id_product.'/'.$id_360.'/'.$id_360set;

		if (!file_exists($folder)) {
			mkdir($folder, 0775);
		} else {
			chmod($folder, 0775);
		}

		return $folder;
	}


	public function imgNameFilter($filename) {
		$filename = preg_replace('/[^A-Za-z0-9_\.-]/', '-', $filename);
		return $filename;
	}

	public function deleteDirectory($dir) {
		if (!file_exists($dir)) {
			return true;
		}

		if (!is_dir($dir)) {
			return unlink($dir);
		}

		foreach (scandir($dir) as $item) {
			if ($item == '.' || $item == '..') {
				continue;
			}

			if (!$this->deleteDirectory($dir . '/' . $item)) {
				return false;
			}

		}

		return rmdir($dir);
	}

	public function getSetsGroups($id_product, $variant_id='') {
		$filter = '';
		if(!empty($variant_id)) {
			$filter = " AND (combinations = '' OR FIND_IN_SET(" . intval($variant_id) . ", combinations)) ";
		}

		$query = $this->db->query('SELECT g.*, COUNT(g.id_360)
			AS qty, s.id_360set
			FROM `ajaxzoom360` g
			LEFT JOIN `ajaxzoom360set` s ON g.id_360 = s.id_360
			WHERE g.id_product = '.(int)$id_product.'
			' . $filter . '
			GROUP BY g.id_360');
		if($query->num_rows) {
			foreach ($query->rows as $result) {
				$tmp[] = $result;
			}

			return $tmp;
		}
	}

	public function getSets($id_product) {
		$query = $this->db->query('SELECT s.*, g.name, g.id_360, g.status
			FROM `ajaxzoom360set` s, `ajaxzoom360` g
			WHERE g.id_360 = s.id_360 AND g.id_product = '.(int)$id_product.'
		ORDER BY g.name, s.sort_order');

		if(!$query->num_rows) {
			return false;
		}

		foreach ($query->rows as $result) {
			$sets[] = $result;
		}

		foreach ($sets as &$set) {
			$thumb = $this->getUrl() . 'axZm/zoomLoad.php?';
			$thumb .= '?qq=1&azImg360=' . $this->getUri() . 'pic/360/'.$id_product.'/'.$set['id_360'].'/'.$set['id_360set'].'&width=100&height=100&thumbMode=contain';

			if (file_exists($this->getDir().'pic/360/'.$id_product.'/'.$set['id_360'].'/'.$set['id_360set'])) {
				$set['path'] = $thumb;
			} else {
				$set['path'] = $this->getUrl().'resources/img/no_image-100x100.jpg';
			}
		}

		return $sets;
	}

	public function getArcList() {
		$files = array();

		if ($handle = opendir($this->getDir() . 'zip/')) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != '.' && $entry != '..' && (strtolower(substr($entry, -3)) == 'zip' || is_dir($this->getDir() . 'zip/' . $entry))) {
					array_push($files, $entry);
				}
			}
			closedir($handle);
		}

		return $files;
	}

	public function isProductActive($id_product) {
		$query = $this->db->query('SELECT *
			FROM `ajaxzoomproducts`
			WHERE id_product = '.(int)$id_product);
		return !$query->num_rows;
	}

	public function getAllProductsVews() {
		$res = array();
		$rows = $query = $this->db->query('SELECT *, COUNT(id_360) as num360views FROM `ajaxzoom360` GROUP BY id_product')->rows;
		foreach($rows as $row) {
			$res[$row['id_product']] = $row;
		}
		return $res;
	}

	public function is2() {
		if(version_compare ( VERSION , '2.0' ) >= 0) {
			return true;
		}
	}

	public function getProducts() {
		$products = array();
		$views = $this->getAllProductsVews();

		$url = '';

		$filter_data = array(
			'start' => 0,
			'limit' => 1000
			);

		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		//$product_total = $this->model_catalog_product->getTotalProducts($filter_data);
		$results = $this->model_catalog_product->getProducts($filter_data);


		foreach ($results as $result) {
			if($this->is2()) {
				if (is_file(DIR_IMAGE . $result['image'])) {
					$image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
					$image = $this->model_tool_image->resize('no_image.png', 40, 40);
				}
			} else {
				$this->load->model('tool/image');
				if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
					$image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
				}
			}

			$products[] = array(
				'product_id' => $result['product_id'],
				'num360views'=> isset($views[$result['product_id']]) ? $views[$result['product_id']]['num360views'] : '',
				'image'      => $image,
				'name'       => $result['name'],
				'model'      => $result['model'],
				'price'      => $result['price'],
				'quantity'   => $result['quantity'],
				'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'       => $this->url->link('module/ajaxzoom/product', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'], 'SSL')
			);
		}

		return $products;
	}
}


ModelCatalogAjaxZoom::$axzmh;
ModelCatalogAjaxZoom::$zoom;
