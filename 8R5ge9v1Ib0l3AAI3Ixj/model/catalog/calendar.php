<?php
class ModelCatalogCalendar extends Model {
    public function addcalendar($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "calendar SET sort_order = '" . (int)$data['sort_order'] . "',date_available = '" . $this->db->escape($data['date_available']) . "',date_available_end = '" . $this->db->escape($data['date_available_end']) . "',image = '" . $this->db->escape($data['image']) . "', status = '" . (int)$data['status'] . "'");

        $calendar_id = $this->db->getLastId();

        foreach ($data['calendar_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "calendar_description SET calendar_id = '" . (int)$calendar_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', url = '" . $this->db->escape($value['url']) . "', img_lan = '" . $this->db->escape($value['img_lan']) . "'");
        }


        return $calendar_id;
    }

    public function editcalendar($calendar_id, $data) {


        $this->db->query("UPDATE " . DB_PREFIX . "calendar SET sort_order = '" . (int)$data['sort_order'] . "',date_available = '" . $this->db->escape($data['date_available']) . "',date_available_end = '" . $this->db->escape($data['date_available_end']) . "',image = '" . $this->db->escape($data['image']) . "', status = '" . (int)$data['status'] . "' WHERE calendar_id = '" . (int)$calendar_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "calendar_description WHERE calendar_id = '" . (int)$calendar_id . "'");

        foreach ($data['calendar_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "calendar_description SET calendar_id = '" . (int)$calendar_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', url = '" . $this->db->escape($value['url']) . "', img_lan = '" . $this->db->escape($value['img_lan']) . "'");
        }


    }

    public function deletecalendar($calendar_id) {
        $this->event->trigger('pre.admin.calendar.delete', $calendar_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "calendar WHERE calendar_id = '" . (int)$calendar_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "calendar_description WHERE calendar_id = '" . (int)$calendar_id . "'");

    }

    public function getcalendar($calendar_id) {
        $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "calendar WHERE calendar_id = '" . (int)$calendar_id . "'");

        return $query->row;
    }

    public function getcalendars($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "calendar i LEFT JOIN " . DB_PREFIX . "calendar_description id ON (i.calendar_id = id.calendar_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";

            $sort_data = array(
                'id.title',
                'i.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY id.title";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $calendar_data = $this->cache->get('calendar.' . (int)$this->config->get('config_language_id'));

            if (!$calendar_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "calendar i LEFT JOIN " . DB_PREFIX . "calendar_description id ON (i.calendar_id = id.calendar_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

                $calendar_data = $query->rows;


            }

            return $calendar_data;
        }
    }

    public function getcalendarDescriptions($calendar_id) {
        $calendar_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "calendar_description WHERE calendar_id = '" . (int)$calendar_id . "'");

        foreach ($query->rows as $result) {
            $calendar_description_data[$result['language_id']] = array(
                'title'            => $result['title'],
                'description'      => $result['description'],
                'url'				 => $result['url'],
                'img_lan'				 => $result['img_lan'],

            );
        }

        return $calendar_description_data;
    }




    public function getTotalcalendars() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "calendar");

        return $query->row['total'];
    }

    public function getTotalcalendarsByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "calendar_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }
}
