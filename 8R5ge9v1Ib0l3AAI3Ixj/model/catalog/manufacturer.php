<?php
class ModelCatalogManufacturer extends Model {
	public function addManufacturer($data) {
		$this->event->trigger('pre.admin.manufacturer.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "', meta_robots = '" . $data['meta_robots'] . "'");

		$manufacturer_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		}

		if (isset($data['manufacturer_store'])) {
			foreach ($data['manufacturer_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		foreach ($data['manufacturer_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_description SET manufacturer_id = '" . (int)$manufacturer_id . "', language_id = '" . (int)$language_id . "', description = '" . $this->db->escape($value['description']) . "', description_2 = '" . $this->db->escape($value['description_2']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL){
			if($this->config->get('opengraph_status')){
				foreach ($data['opengraph'] as $language_id => $value){
					$this->db->query("UPDATE " . DB_PREFIX . "manufacturer_description SET og_title = '" . $this->db->escape($value['title']) . "', og_description = '" . $this->db->escape($value['description']) . "', og_image = '" . $this->db->escape($value['image']) . "' WHERE language_id = '" . (int)$language_id . "' AND manufacturer_id = '" . (int)$manufacturer_id . "'");
				}
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}

		if(isset($data['category_to_manufacturer'])){
			foreach ($data['category_to_manufacturer'] as $value) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category (product_id, category_id) SELECT p.product_id, '" . (int)$value['name_id'] . "' FROM " . DB_PREFIX . "product p INNER JOIN " . DB_PREFIX . "product_to_category ptc ON p.product_id = ptc.product_id  WHERE p.manufacturer_id = '" . (int)$manufacturer_id . "' AND ptc.category_id = '" . (int)$value['copy_from_id'] . "'");

				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_manufacturer SET category_id='" . (int)$value['name_id'] . "' ,manufacturer_id='" . (int)$manufacturer_id . "'");
			}
		}

		$this->cache->delete('manufacturer');

		$this->event->trigger('post.admin.manufacturer.add', $manufacturer_id);

		return $manufacturer_id;
	}

	public function addProductsToCategory($data, $manufacturer_id){
		foreach ($data as $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category (product_id, category_id) SELECT product_id, '" . (int)$value['name_id'] . "' FROM " . DB_PREFIX . "product p INNER JOIN" . DB_PREFIX . "product_to_category ptc ON p.product_id = ptc.product_id  WHERE p.manufacturer_id = '" . (int)$manufacturer_id . "' AND ptc.category_id = '" . (int)$value['copy_from_id'] . "'");
		}
	}


	public function editManufacturer($manufacturer_id, $data) {
		$this->event->trigger('pre.admin.manufacturer.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET name = '" . $this->db->escape($data['name']) . "', meta_robots = '" . $data['meta_robots'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		if (isset($data['manufacturer_store'])) {
			foreach ($data['manufacturer_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape(strtolower($data['keyword'])) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_manufacturer WHERE manufacturer_id='" . (int)$manufacturer_id . "'");

		if(isset($data['category_to_manufacturer'])){

			foreach ($data['category_to_manufacturer'] as $value) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_manufacturer SET category_id='" . (int)$value['name_id'] . "' ,manufacturer_id='" . (int)$manufacturer_id . "'");
			}
		}

		if(isset($data['with_copy_from'])){
			foreach ($data['with_copy_from'] as $value) {

				$arr = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_manufacturer WHERE category_id= '" . (int)$value['category_id'] . "'");

				if($arr->num_rows){
					foreach ($arr->rows as $val) {
						if($val['manufacturer_id'] != $manufacturer_id){
							$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_manufacturer WHERE category_id = '" . (int)$val['category_id'] . "' AND manufacturer_id = '" . $val['manufacturer_id'] . "'");
						}
					}
				}

				$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id='" . (int)$value['category_id'] . "'");

				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category (product_id, category_id) SELECT p.product_id, '" . (int)$value['category_id'] . "' FROM " . DB_PREFIX . "product p INNER JOIN " . DB_PREFIX . "product_to_category ptc ON p.product_id = ptc.product_id  WHERE p.manufacturer_id = '" . (int)$manufacturer_id . "' AND ptc.category_id = '" . (int)$value['copy_from_id'] . "'");
			}
		}

		if (isset($data['manufacturer_description'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_description WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

			foreach ($data['manufacturer_description'] as $language_id => $value) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_description SET manufacturer_id = '" . (int)$manufacturer_id . "', language_id = '" . (int)$language_id . "', description = '" . $this->db->escape($value['description']) . "', description_2 = '" . $this->db->escape($value['description_2']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
			}
		}

		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL){
			if($this->config->get('opengraph_status')){
				foreach ($data['opengraph'] as $language_id => $value){
					$this->db->query("UPDATE " . DB_PREFIX . "manufacturer_description SET og_title = '" . $this->db->escape($value['title']) . "', og_description = '" . $this->db->escape($value['description']) . "', og_image = '" . $this->db->escape($value['image']) . "' WHERE language_id = '" . (int)$language_id . "' AND manufacturer_id = '" . (int)$manufacturer_id . "'");
				}
			}
		}

		$this->cache->delete('manufacturer');

		$this->event->trigger('post.admin.manufacturer.edit');
	}

	public function getManufacturersInCategory($manufacturer_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_manufacturer WHERE manufacturer_id = '" . $manufacturer_id . "'");

		return $query->rows;
	}

	public function getCategoriesInManufacturer($manufacturer_id){
		$query = $this->db->query("SELECT ctm.category_id, ctm.manufacturer_id, cd.name  FROM " . DB_PREFIX . "category_to_manufacturer ctm LEFT JOIN " . DB_PREFIX . "category_description cd ON ctm.category_id = cd.category_id WHERE ctm.manufacturer_id = '" . (int)$manufacturer_id ."' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		return $query->rows;
	}

	public function deleteManufacturer($manufacturer_id) {
		$this->event->trigger('pre.admin.manufacturer.delete', $manufacturer_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_description WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");


		$this->cache->delete('manufacturer');

		$this->event->trigger('post.admin.manufacturer.delete', $manufacturer_id);
	}

	public function getManufacturer($manufacturer_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "') AS keyword FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row;
	}

	public function getManufacturers($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getManufacturerStores($manufacturer_id) {
		$manufacturer_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		foreach ($query->rows as $result) {
			$manufacturer_store_data[] = $result['store_id'];
		}

		return $manufacturer_store_data;
	}

	public function getTotalManufacturers() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "manufacturer");

		return $query->row['total'];
	}

	public function getManufacturerDescriptions($manufacturer_id){
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_description WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		foreach ($query->rows as $result) {
			$manufacturer_description_data[$result['language_id']] = array(
//				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description'],
				'description_2'		 => $result['description_2']
			);
		}

		return $manufacturer_description_data;
	}

	public function getOpengraph($manufacturer_id){

		$opengraph = array();

		$query = $this->db->query("SELECT og_description, og_title, og_image, language_id FROM " . DB_PREFIX . "manufacturer_description WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		foreach ($query->rows as $result) {
			$opengraph[$result['language_id']] = array(
				'title' => $result['og_title'],
				'description' => $result['og_description'],
				'image' => $result['og_image'],
			);

		}

		return $opengraph;
	}
}
