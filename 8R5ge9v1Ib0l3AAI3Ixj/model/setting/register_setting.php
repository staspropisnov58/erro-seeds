<?php
class ModelSettingRegisterSetting extends Model {
  public function getRegisterSettings(){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "register_setting");

    $query = $query->rows;

    foreach ($query as $value) {
      $data[$value['field_name']] = array(
        'label'       => unserialize($value['field_label']),
        'placeholder' => unserialize($value['field_placeholder']),
        'sort_order'  =>$value['field_sort_order'],
        'status'      => $value['field_status'],
        'type'        => $value['field_type'],
        'editable'    => $value['field_editable']
      );
    }

    return $data;

  }

  public function editRegisterSettings($data){
    foreach ($data as $key => $value) {
      $sql = "UPDATE " . DB_PREFIX . "register_setting SET field_label= '" . $this->db->escape(serialize($value['label'])) . "', field_placeholder= '" . $this->db->escape(serialize($value['placeholder'])) . "', field_type = '" . $this->db->escape($value['type']) . "', field_sort_order= '" . (int)$value['sort_order'] . "', field_editable= '" . $value['editable'] . "'";
      if(isset($value['status'])){
        $sql .= " , field_status = '" . $value['status'] . "'";
      }
      $sql .= " WHERE field_name= '" . $key . "'";

      $this->db->query($sql);

    }
    // die;

  }
}
