<?php
class ModelSettingRedirect extends Model
{
  public function getRedirects()
  {
    $query = $this->db->query("SELECT redirect_id, redirect_from, redirect_to, status_code FROM " . DB_PREFIX . "redirect");

    return $query->rows;
  }

  public function saveRedirects($redirects)
  {
    foreach ($redirects as $redirect) {
      $sql = "INSERT INTO " . DB_PREFIX . "redirect (redirect_id, redirect_from, redirect_to, status_code)
                        VALUES (" . (int)$redirect['redirect_id'] . ", '" . $this->db->escape(trim($redirect['redirect_from'], '/')) . "', '" . $this->db->escape(trim($redirect['redirect_to'], '/')) . "', " . (int)$redirect['code'] . ")
                        ON DUPLICATE KEY UPDATE redirect_from = '" . $this->db->escape(trim($redirect['redirect_from'], '/')) . "',
                        redirect_to = '" . $this->db->escape(trim($redirect['redirect_to'], '/')) . "',
                        status_code = " . (int)$redirect['code'];
      $this->db->query($sql);
    }
  }

  public function checkRedirectExists($from)
  {
    $query = $this->db->query("SELECT redirect_id FROM " . DB_PREFIX . "redirect WHERE redirect_from='" . $this->db->escape($from) . "'");
    return $query->num_rows;
  }

  public function checkUrlExists($url)
  {
    $query = $this->db->query("SELECT url_alias_id FROM " . DB_PREFIX . "url_alias WHERE keyword='" . $this->db->escape($url) . "'");
    return $query->num_rows;
  }
}
