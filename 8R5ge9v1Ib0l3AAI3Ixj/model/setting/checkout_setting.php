<?php
class ModelSettingCheckoutSetting extends Model {
  public function getAddressFields(){
    $query = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "address");

    return $query->rows;
  }

  public function enableNoCallback($no_callback) {
    $check = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "order");
    $no_callback_column = array_search('no_callback', array_column($check->rows, 'Field'));
    if ($no_callback && $no_callback_column === false) {
      $this->db->query("ALTER TABLE " . DB_PREFIX . "order ADD `no_callback` TINYINT(1) NOT NULL DEFAULT '0'");
    }
  }  
}
