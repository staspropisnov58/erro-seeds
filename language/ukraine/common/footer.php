<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_information']  = 'Інформація';
$_['text_service']      = 'Сервісні служби';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = 'Контакти';
$_['text_return']       = 'Повернення';
$_['text_sitemap']      = 'Мапа сайту';
$_['text_manufacturer'] = 'Бренди';
$_['text_voucher']      = 'Подарункові сертифікати';
$_['text_affiliate']    = 'Партнерська програма';
$_['text_special']      = 'Спеціальні пропозиції';
$_['text_account']      = 'Обліковий запис';
$_['text_order']        = 'Історія замовлень';
$_['text_wishlist']     = 'Список побажань';
$_['text_newsletter']   = 'Розсилання новин';
$_['text_newsletter_text']   = 'Enter your email address';
$_['text_subcribe']   = 'Subcribe';
$_['text_error_subcribe']   = 'You have already subcribed newsletter!';
$_['text_success_subcribe']   = 'You have successfuly subcribe newsletter!';
$_['text_email_not_validate']  = 'You entered an incorrect Email !';
$_['contact_us']  = '<a href="/contact-us">Контакти</a>';

$_['contact_us_button']  = 'Свяжитесь <br>с нами';

$_['join_us']  = 'ПРИСОЕДИНЯЙТЕСЬ К НАМ';

$_['about_us']  = 'О нас';
$_['questions']  = 'Вопросы';
$_['payment_methods']  = 'Способы оплаты';
$_['delivery']  = 'Доставка';
$_['replacement_and_return']  = 'Замена и возврат';

$_['text_powered']      = 'Працює на <a href="https://opencart.ua">OpenCart</a><br /> %s &copy; %s';
