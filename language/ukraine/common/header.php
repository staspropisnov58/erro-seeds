<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_home']          = 'Головна';
$_['text_wishlist']      = '%s';
$_['text_compare']      = '%s'; //Сравнение
$_['text_shopping_cart'] = 'Кошик';
$_['text_category']      = 'Категорії';
$_['text_account']       = 'Обліковий запис';
$_['text_register']      = 'Реєстрація';
$_['text_login']         = 'Вхід';
$_['text_order']         = 'Історія замовлень';
$_['text_transaction']   = 'Оплати';
$_['text_download']      = 'Завантаження';
$_['text_logout']        = 'Вихід';
$_['text_checkout']      = 'Оформлення замовлення';
$_['text_search']        = 'Пошук';
$_['text_all']           = 'Переглянути всі';


$_['text_about_us']       = 'Про нас';
$_['text_about_us_feedbacs']       = 'Відгуки';
$_['text_about_us_contacts']       = 'Контакти';
$_['text_about_us_sequrity']       = 'Безпека';
$_['text_about_us_faq']       = 'FAQ';
$_['text_compare_catalog']       = 'Каталог';
$_['text_compare_partnership']       = 'Партнерство';
$_['text_compare_news']       = 'Новини';
$_['text_compare_promotions']       = 'Акції';
$_['text_compare_forum']       = 'Форум';
$_['text_about_us_contacts']       = 'Контакти';