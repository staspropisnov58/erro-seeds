<?php
$_['text_new_subject'] = 'Уведомление о поступлениии %s';
$_['text_admission'] = 'Рады сообщить, что на склад поступили товары, которыми Вы интересовались:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_related'] = 'Также рекомендуем:';

$_['text_success'] = 'Сообщение успешно отправлено';
$_['error_email'] = 'Ошибка отправки email';
