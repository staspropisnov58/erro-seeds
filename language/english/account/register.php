<?php
// Heading
$_['heading_title']        = 'Register Account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = '<a href="%s">login</a><span class="ic fa fa-user-o"></span>';
$_['text_your_details']    = 'Your Personal Details';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Telephone';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Способы упаковки';
$_['entry_address_1']      = 'Address 1';
$_['entry_address_2']      = 'Address 2';
$_['entry_postcode']       = 'Post Code';
$_['entry_city']           = 'City';
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Region / State';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';

$_['all_fields_are_required']  = 'All fields are required';
$_['button_register_acc']  = 'Sign Up';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_g-recaptcha-response']      = 'Captcha';
$_['error_g-recaptcha-response-from-server']      = 'Вы точно человек?';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'Warning: You must agree to the %s!';