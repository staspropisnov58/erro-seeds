<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = '%s';
$_['text_compare']      = '%s'; //Сравнение
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'See All';



$_['text_about_us']       = 'О нас';
$_['text_about_us_feedbacs']       = 'Отзывы';
$_['text_about_us_contacts']       = 'Контакты';
$_['text_about_us_sequrity']       = 'Безопасность';
$_['text_about_us_faq']       = 'FAQ';
$_['text_compare_catalog']       = 'Каталог';
$_['text_compare_partnership']       = 'Партнерство';
$_['text_compare_news']       = 'Новости';
$_['text_compare_promotions']       = 'Акции';
$_['text_compare_forum']       = 'Форум';
$_['text_about_us_contacts']       = 'Контакты';