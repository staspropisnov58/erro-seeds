<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_newsletter_text']   = 'Enter your email address';
$_['text_subcribe']   = 'Subcribe';
$_['text_error_subcribe']   = 'You have already subcribed newsletter!';
$_['text_success_subcribe']   = 'You have successfuly subcribe newsletter!';
$_['text_email_not_validate']  = 'You entered an incorrect Email !';
$_['contact_us']  = '<a href="/contact-us">Contacts</a>';

$_['contact_us_button']  = 'Contact <br>us';
$_['join_us']  = 'JOIN US';

$_['about_us']  = 'About us';
$_['questions']  = 'Questions';
$_['payment_methods']  = 'Payment methods';
$_['delivery']  = 'Delivery';
$_['replacement_and_return']  = 'Replacement and return';

$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';