<?php

// Text

$_['text_information']  = 'Информация';

$_['text_service']      = 'Служба поддержки';

$_['text_extra']        = 'Дополнительно';

$_['text_contact']      = 'Связаться с нами';

$_['text_return']       = 'Возврат товара';

$_['text_sitemap']      = 'Карта сайта';

$_['text_manufacturer'] = 'Производители';

$_['text_voucher']      = 'Подарочные сертификаты';

$_['text_affiliate']    = 'Партнерская программа';

$_['text_special']      = 'Акции';

$_['text_account']      = 'Личный Кабинет';

$_['text_order']        = 'История заказов';

$_['text_wishlist']     = 'Закладки';

$_['text_newsletter']   = 'Рассылка';
$_['text_newsletter_text']   = 'Enter your email address';
$_['text_subcribe']   = 'Subcribe';
$_['text_error_subcribe']   = 'You have already subcribed newsletter!';
$_['text_success_subcribe']   = 'You have successfuly subcribe newsletter!';
$_['text_email_not_validate']  = 'You entered an incorrect Email !';
$_['contact_us']  = '<a href="/contact-us">Contacts</a>';

$_['contact_us_button']  = 'Свяжитесь <br>с нами';

$_['join_us']  = 'ПРИСОЕДИНЯЙТЕСЬ К НАМ';

$_['about_us']  = 'О нас';
$_['questions']  = 'Вопросы';
$_['payment_methods']  = 'Способы оплаты';
$_['delivery']  = 'Доставка';
$_['replacement_and_return']  = 'Замена и возврат';

$_['text_powered']      = 'Работает на <a href="http://opencart.com/">OpenCart</a>';

