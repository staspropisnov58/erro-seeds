<?php

// Text

$_['text_home']          = 'Главная';

$_['text_wishlist']      = '%s';

$_['text_compare']      = '%s'; //Сравнение

$_['text_shopping_cart'] = 'Корзина';

$_['text_category']      = 'Категории';

$_['text_account']       = 'Личный кабинет';

$_['text_register']      = 'Регистрация';

$_['text_login']         = 'Авторизация';

$_['text_order']         = 'История заказов';

$_['text_transaction']   = 'Транзакции';

$_['text_download']      = 'Загрузки';

$_['text_logout']        = 'Выход';

$_['text_checkout']      = 'Оформление заказа';

$_['text_search']        = 'Поиск';

$_['text_all']           = 'Смотреть Все';



$_['text_about_us']       = 'О нас';
$_['text_about_us_feedbacs']       = 'Отзывы';
$_['text_about_us_contacts']       = 'Контакты';
$_['text_about_us_sequrity']       = 'Безопасность';
$_['text_about_us_faq']       = 'FAQ';
$_['text_compare_catalog']       = 'Каталог';
$_['text_compare_partnership']       = 'Партнерство';
$_['text_compare_news']       = 'Новости';
$_['text_compare_promotions']       = 'Акции';
$_['text_compare_forum']       = 'Форум';
$_['text_about_us_contacts']       = 'Контакты';
