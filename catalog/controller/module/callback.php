<?php
class ControllerModuleCallback extends Controller
{
  public function send()
  {
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('module/validator');
      //add file
      $this->load->language('module/callback');
      $errors = $this->model_module_validator->validate($this->request->post);
      if (!$errors) {
        $mail_config = $this->config->get('config_mail');
  			$mail = new Mail($mail_config);
  			$mail->setTo($this->config->get('config_email'));
  			if ($mail_config['smtp_username']) {
  				$mail->setFrom($mail_config['smtp_username']);
  			} else {
  				$mail->setFrom($this->request->post['email']);
  			}
        $mail->setSender($this->request->post['firstname']);
        $mail->setSubject($this->language->get('subject'));
        $telephone = $this->config->get('enebled_tel_code') ? $this->request->post['telephone_country_id'] . $this->request->post['telephone'] : $this->request->post['telephone'];
        $mail->setText(sprintf($this->language->get('text'), $this->request->post['firstname'], $telephone));
        if ($mail->send()) {
          $json['message'] = ['type' => 'success',
                              'text' => $this->language->get('success')];
        } else {
          $json['message'] = ['type' => 'warning',
                              'text' => $this->language->get('warning')];
        }
      } else {
        $json['errors'] = $errors;
      }

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
  }
}
