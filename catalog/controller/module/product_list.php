<?php
class ControllerModuleProductList extends Controller {
  public function index($setting) {

    $this->load->model('tool/image');
    $this->load->model('module/product_list');
    $this->load->model('catalog/category');
    $this->load->model('catalog/product');
    $category = array();
    $data['heading_title'] = $setting['title'][$this->config->get('config_language_id')];
    $category = $this->model_catalog_category->getCategory($setting['category_id']);
    if($setting['category_id'] && !empty($category)){
      $data['href'] = $this->url->link('product/category', 'path=' . $setting['category_id']);
    }else{
      $data['href'] = isset($setting['main_stickers']) && $setting['main_stickers'] ?
                      $this->url->link('product/category', 'path=' . $this->config->get('product_stickers_' . $setting['main_stickers'] . '_category'))
                      : '';
    }
    $data['text_tax']         = $this->language->get('text_tax');
    $data['button_cart']      = $this->language->get('button_cart');
    $data['button_wishlist']  = $this->language->get('button_wishlist');
    $data['button_compare']   = $this->language->get('button_compare');
    $data['button_entrance']  = $this->language->get('button_entrance');
    $data['text_in_compare']  = $this->language->get('text_in_compare');
    $data['text_in_wishlist'] = $this->language->get('text_in_wishlist');

    if(!isset($setting['attributes'])){
      $setting['attributes'] = 0;
    }

      // $catgory_data_header = $this->cache->get('product_home.block.'.$setting['category_id'].'-' . $this->config->get('config_store_id') . '.' . $this->config->get('config_language_id'));
       $catgory_data_header = '';

      if($catgory_data_header){
          $data['products'] = $catgory_data_header;
      }else{
          $products_data = $this->model_module_product_list->getProductListData($setting);
          if($products_data){
              foreach ($products_data as $key => $product_data){

                  $price = (isset($product_data['discount']) && $product_data['discount']  ? $product_data['discount'] : $product_data['price']);


                  if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                      $products_data[$key]['price'] = $this->currency->format($this->tax->calculate($price, $product_data['tax_class_id'], $this->config->get('config_tax')));
                  } else {
                      $products_data[$key]['price'] = false;
                  }

                  $product_special_data = 0;

                  $product_special = $this->model_catalog_product->getProductSpecial($product_data['product_id']);
                  if(isset($product_data['special2'])&& $product_data['special2']>0){
                     $product_special['price'] =$product_data['special2'];
                  }

                  $product_special_data = $product_special ? $product_special['price'] : 0;

                  $products_data[$key]['special'] = $product_special ? $this->currency->format($this->tax->calculate($product_special['price'], $product_data['tax_class_id'], $this->config->get('config_tax'))) : 0;

                  if ($this->config->get('config_tax')) {
                      $products_data[$key]['tax'] = $this->currency->format((float)$product_special_data ? $product_special_data : $product_data['price']);
                  } else {
                      $products_data[$key]['tax'] = false;
                  }
                  $products_data[$key]['product_special_id'] =$product_special['product_special_id'];
//                  echo '<pre style="display: none">';
//                  print_r($product_special);
//                  echo '</pre>';
                  $products_data[$key]['options2'] = $this->getProductOptions($products_data[$key]);

                  unset($products_data[$key]['options2']['required']);
                  unset($products_data[$key]['options2']['update']);





                  $products_data[$key]['in_compare'] = isset($this->session->data['compare']) && in_array($product_data['product_id'], $this->session->data['compare']) ? $this->url->link('product/compare') : '';
                  $products_data[$key]['in_wishlist'] = isset($this->session->data['wishlist']) && in_array($product_data['product_id'], $this->session->data['wishlist']) ? $this->url->link('account/wishlist') : '';
                  if ($product_data['image']) {
                      $products_data[$key]['thumb'] = $this->model_tool_image->resize($product_data['image'], $setting['width'], $setting['height']);
                  } else {
                      $products_data[$key]['thumb'] = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                  }
              }
              $data['products'] = $products_data;
          //    $this->cache->set('product_home.block.'.$setting['category_id'].'-' . $this->config->get('config_store_id') . '.' . $this->config->get('config_language_id'), $products_data);

          }

      }


    if ($data['products']) {
      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/product_list/'. $setting['appearance'].'.tpl')) {
        return $this->load->view($this->config->get('config_template') . '/template/module/product_list/'. $setting['appearance'] . '.tpl', $data);
      } else {
        return $this->load->view('default/template/module/featured.tpl', $data);
      }
    }
  }
    public function getProductOptions($product_info) {
        $this->load->model('catalog/product');

        $options = array();
        if(isset($product_info['product_id'])) {
            $product_price = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'];



            $product_old_price = $product_info['price'];
            $options['required'] = 0;
            $first_option = 0;



            foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {



                $product_option_value_data = array();
                foreach ($option['product_option_value'] as $key => $option_value) {
                    if (!empty($product_info['show_with_bulk_option'])) {
                        if (in_array((int)$option_value['option_value_id'], $product_info['show_with_bulk_option'])) {
                            $first_option = $key;
                        } else {
                            continue;
                        }
                    }

                    if ($option_value['subtract'] && $option_value['quantity'] <= 0) {
                        $first_option++;
                        continue;
                    } else {
                        $price = false;
                        $old_price = false;

                        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                            $option_price = (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price;
                            $option_old_price = 0;

                            if ($product_info['product_special_id']) {
                                $special = $this->model_catalog_product->getProductOptionSpecial($option_value['product_option_value_id'], $product_info['product_special_id']);

                                if ($special) {
                                    if ((int)$special['price']) {
                                        $option_price = (float)($option_value['price_prefix'] . $special['price']) + (float)$product_price;
                                        $option_old_price = (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price;
                                    } else {
                                        $option_price = (float)$option_value['price'] ? (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price : $product_price;
                                        $option_old_price = (float)$option_value['price'] ? 0 : $product_old_price;
                                    }
                                } else {
                                    $option_price = (float)$option_value['price'] ? (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price : $product_price;
                                    $option_old_price = (float)$option_value['price'] ? 0 : $product_old_price;
                                }
                            } else {

                            }

                            $price = $this->currency->format($this->tax->calculate($option_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            if ($option_old_price) {
                                $old_price = $this->currency->format($this->tax->calculate($option_old_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            }

                            if ($key === $first_option && $option_value['price']) {
                                if ($product_info['special'] && $option_old_price) {
                                    $options['update']['special'] = $option_price;
                                    $options['update']['price'] = $option_old_price;
                                } else {
                                    $options['update']['special'] = 0;
                                    $options['update']['price'] = $option_price;
                                }
                            } else {
                            }
                        }

                        $image = [];
                        $trigger_image = '';

                        if ($option_value['image']) {
                            $image['extension'] = pathinfo($option_value['image'], PATHINFO_EXTENSION);

                            if ($image['extension'] === 'svg') {
                                $image['value'] = $this->model_tool_image->renderSVG($option_value['image']);
                            } else {
                                $image['value'] = $this->model_tool_image->resize($option_value['image'], 60, 36);
                            }

                            $trigger_image = pathinfo($option_value['image'], PATHINFO_FILENAME);
                        }

                        $product_option_value_data[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'quantity' => $option_value['quantity'],
                            'image' => $image,
                            'trigger_image' => $trigger_image,
                            'price' => $price,
                            'price_dev' => $option_value['price_dev'],
                            'old_price' => isset($old_price) ? $old_price : '',
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                if ($product_option_value_data) {
                    $options[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );
                    if ($option['required']) {
                        $options['required'] = 1;
                    }
                }
            }
        }

        return $options;
    }
  public function updateCache($settings_data = array()){


    $json = 'false';

    if(isset($this->request->get['module_id'])){
      $this->load->model('extension/module');
      $module_info = $this->model_extension_module->getModule($this->request->get['module_id']);

      if($module_info){
        if(!isset($module_info['attributes'])){
          $module_info['attributes'] = 0;
        }
        $this->cache->delete('product_list.' . $this->request->get['module_id']);
        $products = array_slice($module_info['product'], 0, (int)$module_info['limit']);
        $this->load->model('module/product_list');
        $this->load->model('checkout/customer');
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        $this->load->model('setting/store');

        $stores = $this->model_setting_store->getStores();
        $all_stores = array();
        if($stores){
          foreach($stores as $store){
            $all_stores[] = $store['store_id'];
          }
        }else{
          $all_stores[] = 0;
        }

        $customers_groups_id = $this->model_checkout_customer->getCustomersGroupsId();
        foreach ($all_stores as $store_id){
          $this->config->set('config_store_id', $store_id);
          foreach($customers_groups_id as $customer_group){
            $this->config->set('config_customer_group_id', $customer_group['customer_group_id']);
            foreach ($languages as $language){
              $this->session->data['language'] = $language['code'];
              $this->config->set('config_language_id', $language['language_id']);

              $this->model_module_product_list->getProductListData($module_info);
            }
          }
        }
      }
    }
    $this->response->setOutput($json);

  }
}
