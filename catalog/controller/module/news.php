<?php  
class ControllerModuleNews extends Controller {
	public function index() {
		$this->language->load('module/news');
		$this->load->model('extension/news');
		
		$filter_data = array(
			'page' => 1,
			'limit' => 5,
			'start' => 0,
		);
	 
		$data['heading_title'] = $this->language->get('heading_title');
		$data['read_more'] = $this->language->get('read_more');
		$data['read_all_news'] = $this->language->get('read_all_news');

		$all_news = $this->model_extension_news->getModuleNews($filter_data);
	 
		$data['all_news'] = array();
        $this->load->model('tool/image');

		foreach ($all_news as $news) {
			$data['all_news'][] = array (
				'title' 		=> $news['title'],
                'image'			=> $this->model_tool_image->resize($news['image'], 380, 380),
                'image_2'			=> $this->model_tool_image->resize($news['image_2'], 380, 380),
				'description' 	=> (strlen(strip_tags(html_entity_decode($news['short_description']))) > 700 ? substr(strip_tags(html_entity_decode($news['short_description'])), 0, 700) . '...' : strip_tags(html_entity_decode($news['short_description']))),
				'view' 			=> $this->url->link('information/news/news', 'news_id=' . $news['news_id']),
				'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))
			);
		}
	 
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/news.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/news.tpl', $data);
		} else {
			return $this->load->view('default/template/module/news.tpl', $data);
		}
	}
}