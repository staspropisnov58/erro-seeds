<?php
class ControllerModuleCategory extends Controller {
	public function index() {
		$this->load->language('module/category');//text_category
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_show_all'] = $this->language->get('text_show_all');
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$categories = array();

        $catgory_data_home = $this->cache->get('catgory_data_home.menu.' . $this->config->get('config_store_id') . '.' . $this->config->get('config_language_id'));

if($catgory_data_home){
    $data['categories'] = $catgory_data_home;
}else{
    $categories_id = $this->config->get('category_enabled');
    foreach ($categories_id as $category_id){
        $category = $this->model_catalog_category->getCategory($category_id);

        if($category){
            $children_data = array();

            $this->load->model('tool/image');

            if ($category['image'] && is_file(DIR_IMAGE . $category['image'])) {
                $image['extension'] = pathinfo($category['image'], PATHINFO_EXTENSION);

                if($image['extension'] === 'svg') {
                    $image['value'] = $this->model_tool_image->renderSVG($category['image']);
                } else {
                    $image['value'] = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                }
            } else {
                $image['extension'] = 'png';
                $image['value'] = $this->model_tool_image->resize('small-placeholder.png', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
            }

            $category['image'] = $image;

            if ($category['category_id'] == $data['category_id']) {
                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach($children as $child) {
                    $filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),

                    );
                }
            }

            $filter_data = array(
                'filter_category_id'  => $category['category_id'],
                'filter_sub_category' => true
            );

            $categories[] = array(
                'category_id' => $category['category_id'],
                'name'        => $category['name'],
                'children'    => $children_data,
                'image'       => $image,
                'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
            );
        }
    }

    $data['categories'] = array_chunk($categories, 16);

    $this->cache->set('catgory_data_home.menu.' . $this->config->get('config_store_id') . '.' . $this->config->get('config_language_id'), $data['categories']);

}



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category.tpl', $data);
		}
	}
}
