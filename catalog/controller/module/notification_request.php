<?php
class ControllerModuleNotificationRequest extends Controller
{
  public function saveRequest()
  {
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('module/notification_request');
      $this->load->language('module/notification_request');
      $this->load->language('common/errors');
      if ($this->model_module_notification_request->checkRequestNotExists($this->request->post['email'], $this->request->post['product_id'])) {
        $this->load->model('module/validator');
        $errors = $this->model_module_validator->validate($this->request->post, ['email' => 'contact']);
        if (!$errors) {
          $this->model_module_notification_request->saveRequest($this->request->post, $this->request->post['product_id']);
          $product_name = $this->model_module_notification_request->getProductName($this->request->post['product_id']);
          $date_expired = new DateTime(implode('-', $this->request->post['date_expired']));
          $json['message'] = ['text' => sprintf($this->language->get('success'), $product_name, $date_expired->format('d.m.Y')),
                              'type' => 'success'];
        } else {
          $json['errors'] = $errors;
        }
      } else {
        $json['message'] = ['text' => $this->language->get('warning'),
                            'type' => 'warning'];
      }

      if (isset($this->request->post['redirect'])) {
        $this->session->data['notification_request'] = $json;
        $this->session->data['notification_request']['post'] = $this->request->post;
        $this->response->redirect($this->request->post['redirect']);
      } else {
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
      }
    }
  }

  public function renderForm($product_data)
  {
    $form_data['button_submit_entrance'] = $this->language->get('button_submit_entrance');

    $customer_info = [];

    if ($this->customer->isLogged()) {
      $customer_info['email'] = $this->customer->getEmail();
    }

    $message = [];
    $post = [];
    $errors = [];
    $form_data['form'] = [];
    $form_data['message'] = [];

    if (isset($this->session->data['notification_request'])) {
      $message = isset($this->session->data['notification_request']['message']) ? $this->session->data['notification_request']['message'] : [];
      $post = isset($this->session->data['notification_request']['post']) ? $this->session->data['notification_request']['post'] : [];
      $errors = isset($this->session->data['notification_request']['errors']) ? $this->session->data['notification_request']['errors'] : [];

      unset($this->session->data['notification_request']);
    }

    if ($message) {
      $form_data['message'] = $message;
    } else {
      $form = new Form('notification_request', $this->url->link('module/notification_request/saveRequest', '', 'SSL'));

      $sort_order = 0;

      $form->addField(['name' => 'email', 'label' => $this->language->get('entry_email'), 'placeholder' => '', 'sort_order' => $sort_order++, 'value' => '', 'type' => 'text']);

      $form->addField(['name' => 'minus_sign', 'label' => '', 'placeholder' => '',
                      'sort_order' => $sort_order++, 'value' => ' - ', 'type' => 'button',
                      'event_listeners' => ['onclick' => 'plus(this, \'-1\')']]);
      $form->addField(['name' => 'quantity', 'label' => $this->language->get('entry_quantity'), 'placeholder' => '',
                       'sort_order' => $sort_order++, 'value' => 1, 'type' => 'text',
                       'event_listeners' => ['onchange' => 'recount(this)'],
                       'dataset' => ['data-quantity' => 1]]);
      $form->addField(['name' => 'plus_sign', 'label' => '', 'placeholder' => '',
                       'sort_order' => $sort_order++, 'value' => ' + ', 'type' => 'button',
                       'event_listeners' => ['onclick' => 'plus(this, \'1\')']]);

      $form->addField(['name' => 'date_expired[day]', 'label' => '', 'placeholder' => '', 'sort_order' => $sort_order++,
                       'value' => date('d', strtotime('+1 month')), 'type' => 'text']);
      $form->addField(['name' => 'date_expired[month]', 'label' => '', 'placeholder' => '', 'sort_order' => $sort_order++,
                       'value' => date('m', strtotime('+1 month')), 'type' => 'text']);
      $form->addField(['name' => 'date_expired[year]', 'label' => '', 'placeholder' => '', 'sort_order' => $sort_order++,
                       'value' => date('Y', strtotime('+1 month')), 'type' => 'text']);

      $form->setFieldsValues($customer_info);
      $form->setFieldsValues($post);

      $form->groupFields('quantity', 'minus_sign', 'quantity', 'plus_sign');

      $form->addField(['name' => 'date_expired', 'label' => $this->language->get('entry_date_expired'), 'placeholder' => '', 'sort_order' => $sort_order++, 'value' => '', 'type' => '']);

      $form->groupFields('date_expired', 'date_expired', 'date_expired[day]', 'date_expired[month]', 'date_expired[year]');
      $form->setFieldsErrors($errors);

      $form->addField(['name' => 'form_group', 'label' => '', 'placeholder' => '', 'sort_order' => $sort_order++, 'value' => '', 'type' => '']);

      $form->groupFields('form_group', 'form_group', 'quantity', 'date_expired');


      $form_data['form'] = $form->build();
      $form_data = array_merge($form_data, $product_data);

      $data['form']['name'] = $form_data['form']['name'];
      $data['form']['action'] = $form_data['form']['action'];
    }

    $data['form']['content'] = $this->load->view($this->config->get('config_template') . '/template/module/notificationRequestForm.tpl', $form_data);
    $data['form']['title'] = sprintf($this->language->get('title'), $product_data['name']);
    $data['form']['title2'] = $this->language->get('title2');

    return $data;
  }
}
