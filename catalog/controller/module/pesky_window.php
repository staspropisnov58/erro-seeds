<?php
class ControllerModulePeskyWindow extends Controller
{
  public function index($settings)
  {
    $this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/modules/pesky.js?v=1.2');

    $this->load->language('module/pesky_window');
    $data = [];
    $data['heading_title'] = $this->language->get('heading_title');

    $now = new DateTime();
    $end = new DateTime('2019-11-22');

    if (isset($this->request->cookie['votedc']) || $now->getTimestamp() > $end->getTimestamp()) {
      $data['poll_question'] = '';
    } else {
      $this->load->model('tool/image');

      $data['poll_question'] = $this->language->get('poll_question');
      $data['poll_yes'] = $this->language->get('poll_yes');
      $data['poll_no'] = $this->language->get('poll_no');
      $data['image'] = $this->model_tool_image->resize('catalog/cannabis_candies.jpg', 420, 267.5);
    }

    $data['first_timeout'] = $this->config->get('pesky_window_first_timeout');
    $data['min_timeout'] = $this->config->get('pesky_window_min_timeout');
    $data['max_timeout'] = $this->config->get('pesky_window_max_timeout');
    $data['img_height'] = $this->config->get('config_image_cart_height');
    $data['img_width'] = $this->config->get('config_image_cart_width');

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/pesky_window.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/module/pesky_window.tpl', $data);
    }
  }

  public function getProductToPeskyWindow()
  {
    $json = [];
    $products = [];

    $products = $this->cache->get('product.pesky_window.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id'));

    if (!$products) {
      $this->load->model('catalog/product');

      $pesky_products = $this->config->get('pesky_window_products');

      foreach ($pesky_products as $product_id) {
        $product = $this->model_catalog_product->getProduct($product_id);

        $this->load->model('tool/image');

        if ($product['image']) {
  				$thumb = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
  			} else {
  				$thumb = '';
  			}

        if ($product['quantity'] > 0) {
          $products[] = ['product_id' => $product_id,
                         'name' => $product['name'],
                         'href' => $this->url->link('product/product', 'product_id=' . $product_id),
                         'thumb' => $thumb,
                       ];
        }
      }

      $this->cache->set('product.pesky_window.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id'), $products);
    }

    $json['product'] = $products[array_rand($products)];

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function votePoll()
  {
    $json = [];

    if (isset($this->request->get['vote']) && in_array($this->request->get['vote'], ['yes', 'no'])) {
      $this->load->language('module/pesky_window');

      $this->db->query("UPDATE " . DB_PREFIX . "poll_friday
                        SET answer_" . $this->request->get['vote'] . " = answer_" . $this->request->get['vote'] . " + 1
                        WHERE poll_id = 2");

      $json['success'] = $this->language->get('success');
    } else {
      $json['error'] = 'Ooops! You have wrong vote value!';
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

}
