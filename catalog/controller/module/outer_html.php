<?php
class ControllerModuleOuterHtml extends Controller
{
  public function index($setting)
  {
    $data = [];
    if (isset($setting['file_name'][$this->config->get('config_language_id')])) {
      return $this->load->view($this->config->get('config_template') . '/template/module/outer_html/' . $setting['file_name'][$this->config->get('config_language_id')], $data);
    }
  }
}
