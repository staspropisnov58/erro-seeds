<?php
class ControllerModuleCustomerData extends Controller
{
  public function sendPasswordChanged ($email) {
    $this->load->model('account/customer');
    $this->load->language('mail/password');
    $customer_info = $this->model_account_customer->getCustomerByEmail($email);
    $text_main = sprintf($this->language->get('text_main'), $this->config->get('config_url'),  html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
    $subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
    $data['store_url']      = $this->config->get('config_url');
    $data['text_greeting']  = sprintf($this->language->get('text_greeting'), html_entity_decode($customer_info['firstname'] . ' ' . $customer_info['lastname'], ENT_QUOTES, 'UTF-8')) . "\n\n";
    $data['text_main']  = explode('</br>', $text_main);
    $data['text_note']  = sprintf($this->language->get('text_note'), $this->url->link('account/forgotten', '', true));

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/message.tpl')) {
      $html = $this->load->view($this->config->get('config_template') . '/template/mail/message.tpl', $data);
    } else {
      $html = $this->load->view('default/template/mail/order.tpl', $data);
    }


    $mail_config = $this->config->get('config_mail');
    $mail = new Mail($mail_config);
		$mail->setTo($email);
    if ($mail_config['smtp_username']) {
      $mail->setFrom($mail_config['smtp_username']);
      $mail->setReplyTo($this->config->get('config_email'));
    } else {
      $mail->setFrom($this->config->get('config_email'));
    }
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
		$mail->send();
  }
}
