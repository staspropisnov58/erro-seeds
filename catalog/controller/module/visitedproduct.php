<?php
class ControllerModuleVisitedproduct extends Controller {
	public function index($setting) {
		$this->load->language('module/visitedproduct');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['text_in_compare'] = $this->language->get('text_in_compare');
		$data['text_in_wishlist'] = $this->language->get('text_in_wishlist');

		$data['href'] = '';
		$data['products'] = [];

		if(isset($this->session->data['products_id'])){
			$this->session->data['products_id'] = array_unique($this->session->data['products_id']);
		}

		if(isset($this->session->data['products_id']))
			$this->session->data['products_id'] = array_slice($this->session->data['products_id'], -$setting['limit']);
		if (isset($this->session->data['products_id'])) {
			foreach ($this->session->data['products_id'] as $result_id) {
				if((int)$result_id === (int)$this->request->get['product_id']){
					continue;
				}
				$result = $this->model_catalog_product->getProduct($result_id);
				$options = $this->load->controller('product/product/getProductOptions', $result);
				if (array_key_exists('update', $options)) {
					foreach($options['update'] as $key => $value) {
						$result[$key] = $value;
					}
				}

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = '';
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				$stickers = $this->model_catalog_product->getStickers($result);

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
				if(utf8_strlen($result['name']) < 80){
					$sub_name = $result['name'];
				}else{
					$sub_name = utf8_substr($result['name'], 0, 80) . '..';
				}


				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $sub_name ,
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
          'attribute_groups' => $this->model_catalog_product->getProductAttributes($result['product_id']),
          'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'stickers'		=> $stickers,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					'in_compare' => isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare']) ? $this->url->link('product/compare') : '',
					'in_wishlist' => isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist']) ? $this->url->link('account/wishlist') : ''
				);
			}
		}
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/product_list/horizontal_slider.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/product_list/horizontal_slider.tpl', $data);
		} else {
			return $this->load->view('default/template/module/visitedproduct.tpl', $data);
		}
	}
}
