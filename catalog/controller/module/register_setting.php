<?php
class ControllerModuleRegisterSetting extends Controller {
	public function index() {
		$this->language->load('module/register_setting');
		$this->load->model('module/register_setting');

		$data['heading_title'] = $this->language->get('heading_title');

    if($this->config->get('show_register_setting') == 1){
      $fields = $this->model_module_register_setting->getCustomerRegisterFields();
    }else{
      $fields=array();
    }

    if($fields){

      foreach ($fields as $field) {
        if($field['field_name'] == 'password'){
          $type = $field['field_name'];
        }elseif($field['field_name'] == 'telephone'){
          $type = 'tel';
        }else{
          $type = 'text';
        }
        $data['fields'][] = array(
          'name' => $field['field_name'],
          'type' => $type,
          'label' => unserialize($field['field_label'])[$this->config->get('config_language_id')],
          'placeholder' => unserialize($field['field_placeholder'])[$this->config->get('config_language_id')],

        );
      }

    }



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/register_setting.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/register_setting.tpl', $data);
		} else {
			return $this->load->view('default/template/module/register_setting.tpl', $data);
		}
	}
}
