<?php
class ControllerModuleSlideshow extends Controller {
	public function index($setting) {
		$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/owl.carousel.min.js');
		$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/owl.carousel2.thumbs.js');
		$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/css/owl.carousel.min.css');
		$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/css/owl.theme.default.min.css');
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  =>  $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		$data['module'] = $module++;

		if($this->isMobile()){

        }else{
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/slideshow.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/slideshow.tpl', $data);
            } else {
                return $this->load->view('default/template/module/slideshow.tpl', $data);
            }
        }



	}
	public function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
