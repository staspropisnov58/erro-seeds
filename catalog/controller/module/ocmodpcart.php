<?php
class ControllerModuleOcmodpcart extends Controller {
	public function index() {
		
		$data = array();

		$this->load->language('module/ocmodpcart');

        $data['currency'] = $this->load->controller('common/currency');
        $data['text_currency'] = $this->language->get('text_currency');

        $data['action'] = $this->url->link('common/currency/currency', '', $this->request->server['HTTPS']);

        $data['code'] = $this->currency->getCode();

        $this->load->model('localisation/currency');

        $data['currencies'] = array();

        $results = $this->model_localisation_currency->getCurrencies();

        foreach ($results as $result) {
            if ($result['status']) {
                $data['currencies'][] = array(
                    'title'        => $result['title'],
                    'code'         => $result['code'],
                    'symbol_left'  => $result['symbol_left'],
                    'symbol_right' => $result['symbol_right'],
                    'value'        => $result['value']
                );
            }
        }


        foreach($data['currencies'] as $val){
            if ($val["code"] == $_SESSION['currency']){
                $coof = $val['value'];
                $symbol_left = $val['symbol_left'];
                $symbol_right = $val['symbol_right'];
            }
        }



		$data['button_shopping'] = $this->language->get('button_shopping');
		$data['button_checkout'] = $this->language->get('button_checkout');
		$data['heading_cartpopup_title_empty'] = $this->language->get('heading_cartpopup_title_empty');
		$data['text_cartpopup_empty'] = $this->language->get('text_cartpopup_empty');
		$data['heading_cartpopup_title_nalogka'] = $this->language->get('heading_cartpopup_title_nalogka');

		if ( isset( $this->request->request['remove'] ) ) {
			$this->cart->remove( $this->request->request['remove'] );
			unset( $this->session->data['vouchers'][$this->request->request['remove']] );
		}

		if ( isset( $this->request->request['update'] ) ) {
			$this->cart->update( $this->request->request['update'], $this->request->request['quantity'] );
		}

		if ( isset( $this->request->request['add'] ) ) {
			$this->cart->add( $this->request->request['add'], $this->request->request['quantity'] );
		}

//        if ($customer_group_value['min_order_total'] && $this->cart->getTotal() < $customer_group_value['min_order_total']) {
//            $error = sprintf($this->language->get('error_min_order_total'), $customer_group_value['min_order_total']);
//        }
//        if ($customer_group_value['max_order_total'] && $this->cart->getTotal() > $customer_group_value['max_order_total']) {
//            $error = sprintf($this->language->get('error_max_order_total'), $customer_group_value['max_order_total']);
//        }

		if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
			$data['error_warning'] = $this->language->get('error_stock');
		} elseif (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		}elseif ($this->cart->getTotal() < 100) {
            $data['error_warning_min_sum'] = $this->language->get('error_warning_min_summ') . ' ' . $symbol_left . ' ' . round((100 * $coof),2)/*$symbol_right */;
            $data['error'] = true;
        } else {
			$data['error_warning'] = '';
			$data['error_warning_min_sum'] = '';
		}

		if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
			$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
		} else {
			$data['attention'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();

		$products = $this->cart->getProducts();



		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_in_cart) {
				if ($product_in_cart['product_id'] == $product['product_id']) {
					$product_total += $product_in_cart['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
			}

			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], 80, 80);
			} else {
				$image = $this->model_tool_image->resize("placeholder.png", 80, 80);
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}



			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);

			} else {
				$total = false;
			}

			$data['products'][] = array(
				'key'         => $product['key'],
				'product_id'  => $product['product_id'],
				'thumb'       => $image,
				'name'        => $product['name'],
				'model'       => $product['model'],
				'option'      => $option_data,
				'quantity'    => $product['quantity'],
				'stock'       => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
				'reward'      => ($product['reward'] ? sprintf($this->language->get('text_cartpopup_points'), $product['reward']) : ''),
				'price'       => $price,
				'total'       => $total,
				'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}

		$this->load->model('extension/extension');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$data['totals'] = array();


		foreach ($total_data as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value']),

			);
		}

		$data['checkout_link'] = $this->url->link('checkout/checkout');
		
		$cart_number = $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0);
		function getcartword($number, $suffix) {
			$keys = array(2, 0, 1, 1, 1, 2);
			$mod = $number % 100;
			$suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
			return $suffix[$suffix_key];
		}
		$textcart_array = array('textcart_1', 'textcart_2', 'textcart_3');
		$textcart = getcartword($cart_number, $textcart_array);

		$data['heading_cartpopup_title'] = sprintf($this->language->get($textcart), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/ocmodpcart.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/ocmodpcart.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/module/ocmodpcart.tpl', $data));
		}
	
	}

	public function status_cart() {
		
		$json = array();

		$this->load->language('module/ocmodpcart');
		$this->load->model('extension/extension');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

//		$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
		$json['total'] = sprintf( $this->cart->countProducts() );

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}