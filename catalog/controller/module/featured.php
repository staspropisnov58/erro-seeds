 <?php
class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('module/featured');

    // Menu
    $this->load->model('catalog/category');
    $this->load->model('catalog/product');
    $data['categories'] = array();
    $categories = $this->model_catalog_category->getCategories(0);
    foreach ($categories as $category) {
        if ($category["category_id"] == 102){
            continue;
        }
        $data['categories'][] = array(
            'name'     => $category['name'],
            'column'   => $category['column'] ? $category['column'] : 1,
            'href'     => $this->url->link('product/category', 'path=' . $category['category_id']),
        );
    }

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
    $data['button_entrance'] = $this->language->get('button_entrance');
    $data['text_in_compare'] = $this->language->get('text_in_compare');
    $data['text_in_wishlist'] = $this->language->get('text_in_wishlist');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		$products = array_slice($setting['product'], 0, (int)$setting['limit']);
		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
        $options = $this->load->controller('product/product/getProductOptions', $product_info);
        if (array_key_exists('update', $options)) {
          foreach($options['update'] as $key => $value) {
            $product_info[$key] = $value;
          }
        }

				$product_attribute = $this->model_catalog_product->getProductAttributes($product_id);
        foreach ($product_attribute as &$product_attribut) {
					foreach ($product_attribut['attribute'] as &$attribute) {
						if(isset($attribute['image']) && (!is_file(DIR_IMAGE . $attribute['image']))){
							$attribute['image'] = $this->model_tool_image->resize('small-placeholder.png', $this->config->get('config_image_attribute_product_width'), $this->config->get('config_image_attribute_product_height'));
						}else{
							$type = pathinfo($attribute['image']);
							if($type['extension'] == 'svg'){
								$attribute['svg'] = $this->model_tool_image->renderSVG($attribute['image']);
							}else{
							$attribute['image'] = $this->model_tool_image->resize($attribute['image'], $this->config->get('config_image_attribute_product_width'), $this->config->get('config_image_attribute_product_height'));
							}
						}
					}
				}
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
//                    $price = explode(' ', $price);
//                    $price = $price[1]. ' '.$price[2];
				} else {
					$price = false;
				}



				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

        $stickers = $this->model_catalog_product->getStickers($product_info);


				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $product_info['product_id'],
          'quantity'    => $product_info['quantity'],
					'thumb'       => $image,
					'name'        => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
          'stickers'		=> $stickers,
					'rating'      => $rating,
          'attributes'  => $product_attribute,
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
          'in_compare' => isset($this->session->data['compare']) && in_array($product_info['product_id'], $this->session->data['compare']) ? $this->url->link('product/compare') : '',
					'in_wishlist' => isset($this->session->data['wishlist']) && in_array($product_info['product_id'], $this->session->data['wishlist']) ? $this->url->link('account/wishlist') : ''
				);
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
			} else {
				return $this->load->view('default/template/module/featured.tpl', $data);
			}
		}
	}
}
