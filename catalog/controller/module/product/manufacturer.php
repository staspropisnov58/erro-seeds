<?php
class ControllerProductManufacturer extends Controller {
	public function index() {

		$this->load->language('product/manufacturer');

		$this->load->model('catalog/manufacturer');

		$this->load->model('tool/image');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/' . $this->session->data['language'] . '.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_index'] = $this->language->get('text_index');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_show_all'] = $this->language->get('text_show_all');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['show_products'] = $this->language->get('show_products');
		$data['text_default'] = $this->language->get('text_default');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_brand'),
			'microdata' => $this->url->link('product/manufacturer'),
		);

		foreach ($data['breadcrumbs'] as $key =>  $value){
			$breadcrumbs_array[$key] = array(
				'@type' => "ListItem",
				"position" => $key,
				"item" => array(
					'id' => $value['microdata'],
					'name' => $value['text'],
				),
			);
		}


		$breadcrumbs = array(
			'@context' => 'https://schema.org',
			'@type' => 'BreadcrumbList',
			'itemListElement'=> $breadcrumbs_array,
		);

		$this->document->addMicrodata($breadcrumbs);

		$data['categories'] = array();

			 // $url = '';
			 //
			 // if (isset($this->request->get['sort'])) {
				//  $url .= '&sort=' . $this->request->get['sort'];
			 // }
			 //
			 // if (isset($this->request->get['order'])) {
				//  $url .= '&order=' . $this->request->get['order'];
			 // }
			 //
			 // if (isset($this->request->get['limit'])) {
				//  $url .= '&limit=' . $this->request->get['limit'];
			 // }

			 if(isset($this->request->get['page'])){
				 $url = '&page='. $this->request->get['page'];
			 }else{
				 $url = '';
			 }



		$limit = $this->config->get('config_manufacturer_limit');

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'href'  => $this->url->link('product/manufacturer', $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'href'  => $this->url->link('product/manufacturer', '&sort=m.name&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'href'  => $this->url->link('product/manufacturer', '&sort=m.name&order=DESC' . $url)
		);

		if(isset($this->request->get['sort']) && isset($this->request->get['order'])){
			if(isset($this->request->get['page']) && isset($this->request->get['limit'])){
				$data['url'] =($this->url->link('product/manufacturer'. '&sort=' . $this->request->get['sort'] . '&order=' . $this->request->get['order'] . '&page=' . $this->request->get['page'] . '&limit=' . $this->request->get['limit']));
			}elseif(isset($this->request->get['page'])){
				$data['url'] =($this->url->link('product/manufacturer'. '&sort=' . $this->request->get['sort'] . '&order=' . $this->request->get['order'] . '&page=' . $this->request->get['page']));
			}else{
			$data['url'] =($this->url->link('product/manufacturer'. '&sort=' . $this->request->get['sort'] . '&order=' . $this->request->get['order']));
			}
		}else{
			if(isset($this->request->get['page']) && isset($this->request->get['limit'])){
				$data['url'] = 	($this->url->link('product/manufacturer' . '&page=' . $this->request->get['page'] . '&limit=' . $this->request->get['limit']));
			}elseif(isset($this->request->get['page'])){
				$data['url'] = 	($this->url->link('product/manufacturer' . '&page=' . $this->request->get['page']));
			}else{
				$data['url'] = 	($this->url->link('product/manufacturer'));
			}
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		}else{
			$sort='';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		}else{
			$order = '';
		}

		if(isset($this->request->get['path'])){
			$data['path'] = $this->request->get['path'];
		}else{
			$data['path'] = '';
 		}

		$total_manufacturers = $this->model_catalog_manufacturer->getTotalManufacturers();

		if(isset($this->request->get['sort']) && isset($this->request->get['order'])){
			$data['all'] = $this->url->link('product/manufacturer', '&sort=' . $this->request->get['sort'] . '&order=' . $this->request->get['order'] . '&limit=' . $total_manufacturers);
		}else{
			$data['all'] = $this->url->link('product/manufacturer', '&limit=' . $total_manufacturers);
		}
		if(isset($this->request->get['limit']) && $this->request->get['limit'] == $total_manufacturers){
			$limit = $total_manufacturers;
		}

		$filter_data = array(
			'start'              => ($page - 1) * $limit,
			'limit'              => $limit,
			'sort'               => $sort,
			'order' 						 => $order
		);


		$results = $this->model_catalog_manufacturer->getManufacturers($filter_data);

		foreach ($results as $result) {
			if (is_numeric(utf8_substr($result['name'], 0, 1))) {
				$key = '0 - 9';
			} else {
				$key = utf8_substr(utf8_strtoupper($result['name']), 0, 1);
			}

			if (!isset($data['categories'][$key])) {
				$data['categories'][$key]['name'] = $key;
			}

			if ($result['image']) {
				$thumb = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_big_manufacturer_width'), $this->config->get('config_image_big_manufacturer_height'));
			} else {
				$thumb = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_big_manufacturer_width'), $this->config->get('config_image_big_manufacturer_height'));
			}

			$data['categories'][$key]['manufacturer'][] = array(
				'name' => $result['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']),
				'description_2' => html_entity_decode($result['description_2'], ENT_QUOTES, 'UTF-8'),
				'image' => $thumb
			);

		}

		$pagination = new Pagination();
		$pagination->total = $total_manufacturers;
		$pagination->page = $page;
		$pagination->limit = $limit;

		if(isset($this->request->get['sort'])&&isset($this->request->get['order'])){
			$pagination->url = $this->url->link('product/manufacturer','&sort=' . $this->request->get['sort'] . '&order=' . $this->request->get['order'] . '&page={page}');
		}else{
			$pagination->url = $this->url->link('product/manufacturer', '&page={page}');
		}
		$data['pagination'] = $pagination->render();

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/manufacturer_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/manufacturer_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/manufacturer_list.tpl', $data));
		}
	}

	public function info() {
		$this->load->language('product/manufacturer');

		$this->load->model('catalog/manufacturer');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$no_description = false;

		if (isset($this->request->get['manufacturer_id'])) {
			$manufacturer_id = (int)$this->request->get['manufacturer_id'];
		} else {
			$manufacturer_id = 0;
		}

		if (isset($this->request->get['filter_ocfilter'])) {
			$no_description = true;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			$no_description = true;
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_brand'),
			'href' => $this->url->link('product/manufacturer'),
			'microdata'=>$this->url->link('product/manufacturer'),
		);

		$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

		if ($manufacturer_info) {
			if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

				$this->load->model('tool/opengraph');

				$this->model_tool_opengraph->addOpengraphForManufacturer($manufacturer_info);

			}

			$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/category.js');

			$meta_page = ($page != 1) ? unicode_ucfirst($this->language->get('text_page')) . ' ' . $page . ' - ' : '';

			$this->document->setTitle($meta_page . $manufacturer_info['meta_title']);
			$this->document->setDescription($meta_page . $manufacturer_info['meta_description']);
			$this->document->setKeywords($manufacturer_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id']), 'canonical');
			$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/category.js');

			if($manufacturer_info['meta_robots']!==''){
				$this->document->setRobots($manufacturer_info['meta_robots']);
			}

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $manufacturer_info['name'],
				'microdata'=>$this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id']),
			);

			foreach ($data['breadcrumbs'] as $key =>  $value){
				$breadcrumbs_array[$key] = array(
					'@type' => "ListItem",
					"position" => $key,
					"item" => array(
						'id' => $value['microdata'],
						'name' => $value['text'],
					),
				);
			}


			$breadcrumbs = array(
				'@context' => 'https://schema.org',
				'@type' => 'BreadcrumbList',
				'itemListElement'=> $breadcrumbs_array,
			);

			$this->document->addMicrodata($breadcrumbs);

			$data['heading_title'] = $manufacturer_info['name'];

			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');
			$data['text_buy'] = $this->language->get('text_buy');
			$data['text_show_all'] = $this->language->get('text_show_all');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');
			$data['button_entrance'] = $this->language->get('button_entrance');

			$data['compare'] = $this->url->link('product/compare');
			$data['description'] = $no_description ? '' : html_entity_decode($manufacturer_info['description'], ENT_QUOTES, 'UTF-8');
			$data['manufacturer_name'] = $manufacturer_info['name'];

			$manufacturer_categories = $this->model_catalog_manufacturer->getManufacturerCategories($manufacturer_id);

			if (!empty($manufacturer_categories)){
				foreach ($manufacturer_categories as  $value) {
					$filter_data = array(
						'filter_category_id'  => $value['category_id'],
						'filter_sub_category' => false
					);

					$data['categories'][] = array(
						'name'  => $value['name'],
						'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $value['category_id'] . $url),
						'total' => $this->model_catalog_product->getTotalProducts($filter_data)
					);
				}
			}

			$data['products'] = array();

			$filter_data = array(
				'filter_manufacturer_id' => $manufacturer_id,
				'sort'                   => $sort,
				'order'                  => $order,
				'start'                  => ($page - 1) * $limit,
				'limit'                  => $limit
			);

			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$display_settings['display'] = isset($this->request->cookie['display_list']) ? 'list' : 'grid';
			$display_settings['filter_data'] = $filter_data;

			$data['products'] = $this->load->controller('product/product/displayProducts', $display_settings);

			$data['filter_data'] = json_encode($filter_data);

			$url = '';

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . '&sort=p.model&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] .  $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));
			$data['all'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] .  $url . '&limit=' . $product_total);
			$data['all_products'] =  $product_total;

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/manufacturer_info.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/manufacturer_info.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/manufacturer_info.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/manufacturer/info', $url),
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['header'] = $this->load->controller('common/header');
			$data['footer'] = $this->load->controller('common/footer');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
}
