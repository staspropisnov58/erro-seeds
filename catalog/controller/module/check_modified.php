<?php
class ControllerModuleCheckModified extends Controller {
  	public function index($page) {
      $this->load->model('module/check_modified');
      $this->request->server['HTTP_IF_MODIFIED_SINCE'];
        $date_modified = $this->model_module_check_modified->getDateModified($page);
          if(isset($this->session->data['last_modified']) && $date_modified){
            if ($date_modified >= $this->session->data['last_modified']){
            }else{
            $date_modified = $this->session->data['last_modified'];
            }
          }
      if($date_modified){
        $LastModified_unix = new DateTime("$date_modified");
        $LastModified_unix = (date_timestamp_get($LastModified_unix));
        $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
        $IfModifiedSince = false;

        if (isset($this->request->server['HTTP_IF_MODIFIED_SINCE']) && $this->request->server['HTTP_IF_MODIFIED_SINCE'] != ''){
            $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));

        if ($IfModifiedSince && $LastModified_unix && $IfModifiedSince >= $LastModified_unix) {
          $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 304 Not Modified');
        }

        $this->response->addHeader('Last-Modified: '. $LastModified);

    }else {
      $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 200 Success');
      $this->response->addHeader('Last-Modified: '. gmdate("D, d M Y H:i:s \G\M\T", time()));
    }
    }else {
      $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 200 Success');
      $this->response->addHeader('Last-Modified: '. gmdate("D, d M Y H:i:s \G\M\T", time()));
    }
  }
}
