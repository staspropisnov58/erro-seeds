<?php
class ModelModuleCustomerIsAffiliate extends Model {
	public function addAffiliate($data) {
		$this->event->trigger('pre.affiliate.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "affiliate SET code = '" . $this->db->escape(uniqid()) . "', status = '1', commission = '" . (float)$this->config->get('config_affiliate_commission') . "',  approved = '" . (int)!$this->config->get('config_affiliate_approval') . "', date_added = NOW()");

		$affiliate_id = $this->db->getLastId();

		// $this->load->language('mail/affiliate');
		//
		// $subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
		//
		// $message  = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
		// $message .= $this->language->get('text_approval') . "\n";
		//
		// if (!$this->config->get('config_affiliate_approval')) {
		// 	$message .= $this->language->get('text_login') . "\n";
		// } else {
		// 	$message .= $this->language->get('text_approval') . "\n";
		// }

		// $message .= $this->url->link('affiliate/login', '', 'SSL') . "\n\n";
		// $message .= $this->language->get('text_services') . "\n\n";
		// $message .= $this->language->get('text_thanks') . "\n";
		// $message .= $this->config->get('config_name');
		//
		// $mail = new Mail($this->config->get('config_mail'));
		// $mail->setTo($this->request->post['email']);
		// $mail->setFrom($this->config->get('config_email'));
		// $mail->setSender($this->config->get('config_name'));
		// $mail->setSubject($subject);
		// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		// $mail->send();

		// Send to main admin email if new affiliate email is enabled
	/*	if ($this->config->get('config_affiliate_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_store') . ' ' . $this->config->get('config_name') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";

			if ($data['website']) {
				$message .= $this->language->get('text_website') . ' ' . $data['website'] . "\n";
			}

			if ($data['company']) {
				$message .= $this->language->get('text_company') . ' '  . $data['company'] . "\n";
			}

			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_affiliate'), ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			// Send to additional alert emails if new affiliate email is enabled
			$emails = explode(',', $this->config->get('config_mail_alert'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}*/

		$this->event->trigger('post.affiliate.add', $affiliate_id);

		return $affiliate_id;

	}

  public function editAffiliate($data) {
		$this->event->trigger('pre.customer.edit', $data);

		$customer_id = $this->customer->getId();

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . trim($this->db->escape($data['email'])) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->event->trigger('post.customer.edit', $customer_id);
	}

  public function updateCustomerIsAffiliate($customer_id, $affiliate_id){

    $this->db->query("UPDATE " . DB_PREFIX . "customer SET affiliate_id = '" . $this->db->escape($affiliate_id) . "' WHERE customer_id = '" . (int)$customer_id . "'");
  }

	public function getAffiliate($affiliate_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE affiliate_id = '" . (int)$affiliate_id . "'");

		return $query->row;
	}


}
