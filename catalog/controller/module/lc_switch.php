<?php
class ControllerModuleLcSwitch extends Controller {
  public function index(){
	  $data['language'] = $this->load->controller('common/language');
    $data['currency'] = $this->load->controller('common/currency');



    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/lc_switch.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/lc_switch.tpl', $data);
		}


  }
}
