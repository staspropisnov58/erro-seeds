<?php

class ControllerModuleAccount extends Controller
{
    public function index()
    {
        $this->load->language('module/account');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['menu'] = [];

        $current_page = $this->request->get['route'];

        $data['menu']['my_account'] = ['class' => ''];

        $data['menu']['my_account']['links'] = [
            ['text' => $this->language->get('text_account'),
                'link' => $this->url->link('account/account', '', 'SSL'),
                'class' => strpos($current_page, 'account/account') !== false ? 'active' : ''],

            ['text' => $this->language->get('text_edit'),
                'link' => $this->url->link('account/edit', '', 'SSL'),
                'class' => strpos($current_page, 'account/edit') !== false ? 'active' : ''],

            ['text' => $this->language->get('text_password'),
                'link' => $this->url->link('account/password', '', 'SSL'),
                'class' => strpos($current_page, 'account/password') !== false ? 'active' : ''],

            ['text' => $this->language->get('text_address'),
                'link' => $this->url->link('account/address', '', 'SSL'),
                'class' => strpos($current_page, 'account/address') !== false ? 'active' : ''],

            ['text' => $this->language->get('text_order'),
                'link' => $this->url->link('account/order', '', 'SSL'),
                'class' => strpos($current_page, 'account/order') !== false ? 'active' : ''],

            ['text' => $this->language->get('text_vip'),
                'link' => $this->url->link('account/vip', '', 'SSL'),
                'class' => strpos($current_page, 'account/vip') !== false ? 'active' : ''],

            ['text' => $this->language->get('text_newsletter'),
                'link' => $this->url->link('account/newsletter', '', 'SSL'),
                'class' => strpos($current_page, 'account/newsletter') !== false ? 'active' : ''],
        ];

        if ($this->config->get('customer_is_affiliate_status')) {
            $data['menu']['my_affiliate'] = ['class' => 'affiliate'];
            $data['menu']['my_affiliate']['links'] = [
                ['text' => $this->language->get('text_affiliate'),
                    'link' => $this->url->link('account/affiliate_tracking', '', 'SSL'),
                    'class' => strpos($current_page, 'account/affiliate_tracking') !== false ? 'active' : ''],
            ];

            if ($this->customer->getAffiliateId()) {
                $affiliate_links = [
                    ['text' => $this->language->get('text_bonuses'),
                        'link' => $this->url->link('account/affiliate_transaction', '', 'SSL'),
                        'class' => strpos($current_page, 'account/affiliate_transaction') !== false ? 'active' : ''],

                    ['text' => $this->language->get('text_payouts'),
                        'link' => $this->url->link('account/payouts', '', 'SSL'),
                        'class' => strpos($current_page, 'account/payouts') !== false ? 'active' : ''],

                    ['text' => $this->language->get('text_unclosed_orders'),
                        'link' => $this->url->link('account/unclosed_orders', '', 'SSL'),
                        'class' => strpos($current_page, 'account/unclosed_orders') !== false ? 'active' : ''],
                ];

                $data['menu']['my_affiliate']['links'] = array_merge($data['menu']['my_affiliate']['links'], $affiliate_links);
            }
        }


        $data['text_register'] = $this->language->get('text_register');
        $data['text_my_account_order_old'] = $this->language->get('text_my_account_order_old');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_forgotten'] = $this->language->get('text_forgotten');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_password'] = $this->language->get('text_password');
        $data['text_address'] = $this->language->get('text_address');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_my_account'] = $this->language->get('text_my_account');
        $data['text_bonuses'] = $this->language->get('text_bonuses');
        $data['text_payouts'] = $this->language->get('text_payouts');
        $data['text_unclosed_orders'] = $this->language->get('text_unclosed_orders');
        $data['text_affiliate'] = $this->language->get('text_affiliate');
        $data['text_my_account_group'] = $this->language->get('text_my_account_group');
        $data['text_my_account_sale'] = $this->language->get('text_my_account_sale');
        $data['text_my_account_bonus'] = $this->language->get('text_my_account_bonus');

        $data['logged'] = $this->customer->isLogged();
        $data['register'] = $this->url->link('account/register', '', 'SSL');
        $data['login'] = $this->url->link('account/login', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        $data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['edit'] = $this->url->link('account/edit', '', 'SSL');
        $data['password'] = $this->url->link('account/password', '', 'SSL');
        $data['address'] = $this->url->link('account/address', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
        $data['bonuses'] = $this->url->link('account/affiliate_transaction', '', 'SSL');
        $data['payouts'] = $this->url->link('account/payouts', '', 'SSL');
        $data['affiliate'] = $this->url->link('account/affiliate_tracking', '', 'SSL');
        $data['unclosed_orders'] = $this->url->link('account/unclosed_orders', '', 'SSL');
        $data['affiliate_id'] = $this->customer->getAffiliateId();
        //$data['balance'] =  $this->currency->format($this->tax->calculate($this->customer->getBonus(), 0, $this->config->get('config_tax')));
        $data['group_name'] = $this->customer->getGroupName();

        $data['balance'] = $this->currency->format($this->tax->calculate($this->customer->getUserBonus(), 0, $this->config->get('config_tax')));
        $summ_sale = $this->customer->getSummeSale();
        $summ_sale_group = (float)$this->customer->getSummeSaleGroup();
        $procent_sale = $this->customer->getProcentSale();
        $procent_sale_group = $this->customer->getProcentSaleGroup();
        $customer_balance = (float)$this->customer->getSummBalance();

        $getProcentSaleBonus = $this->customer->getProcentSaleBonus();
        $getProcentSaleBonusGroup = $this->customer->getProcentSaleBonusGroup();

        $data['old_price'] = $this->currency->format($this->tax->calculate($customer_balance, 0, $this->config->get('config_tax')));
        $data['customer_group_id'] = $this->customer->getGroupId();

// print_r($procent_sale);

        if ($summ_sale < $customer_balance && $summ_sale != 0) {
            $data['my_sale'] = $procent_sale;
        } elseif ($summ_sale_group < $customer_balance && $summ_sale_group != 0 && $summ_sale == 0) {
            $data['my_sale'] = $procent_sale_group;
        } else {
            $data['my_sale'] = 0;
        }


        $data['tuning'] = $this->config->get('customer_is_affiliate_status');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/account.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/account.tpl', $data);
        } else {
            return $this->load->view('default/template/module/account.tpl', $data);
        }
    }
}
