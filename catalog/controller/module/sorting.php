<?php
class ControllerModuleSorting extends Controller {
	private $route_params = ['product/category' => 'path',
												 'product/search' => 'search',
												 'product/manufacturer/info' => 'manufacturer_id'];

	private $db_columns = ['name' => 'pd.name',
											 'price' => 'p.price',
											 'quantity' => 'p.quantity',
											 'rating' => 'rating',
											 'model' => 'p.model',
											 'date_added' => 'p.date_added',
											 'sort_order' => 'p.sort_order',
										   'date_start' => 'ps.date_start'];

	public function index($setting)
	{
			if ($this->config->get('sorting_status')) {
					$this->load->language('module/sorting');
					$data['sort_order'] = $this->language->get('sort_order');
					$data['name_ASC']=$this->language->get('name_ASC');
					$data['name_DESC']=$this->language->get('name_DESC');
					$data['price_ASC']=$this->language->get('price_ASC');
					$data['price_DESC']=$this->language->get('price_DESC');
					$data['rating_ASC']=$this->language->get('rating_ASC');
					$data['rating_DESC']=$this->language->get('rating_DESC');
					$data['model_ASC']=$this->language->get('model_ASC');
					$data['model_DESC']=$this->language->get('model_DESC');
					$data['quantity_ASC']=$this->language->get('quantity_ASC');
					$data['quantity_DESC']=$this->language->get('quantity_DESC');
					$data['date_start_DESC']=$this->language->get('date_start_DESC');

					$page_route = $this->request->get['route'];

					$current_query = isset($this->route_params[$page_route]) ? '&' . $this->route_params[$page_route] . '=' . $this->request->get[$this->route_params[$page_route]] : '';
					$data['sort_order_href'] = $this->url->link($page_route, $current_query, true);

					$current_url_array = ['sort' => isset($this->request->get['sort']) ? $this->request->get['sort'] : '',
				                  			'order' => isset($this->request->get['order']) ? strtoupper($this->request->get['order']) : '',
																'route' => $page_route];

					$configuration_sorts = $this->config->get('sorting_'.(str_replace('/', '_', $page_route)));
					$data['current_sort'] = $data['sort_order'];

					foreach ($configuration_sorts['enabled'] as $config_sort) {
							$order = substr($config_sort, strrpos($config_sort, "_", -0) + 1);
							$sort = $this->db_columns[stristr($config_sort, '_' . $order, true)];

							$query = '&sort=' . $sort . '&order=' . $order;
							$query .= isset($this->route_params[$page_route]) ? '&' . $this->route_params[$page_route] . '=' . $this->request->get[$this->route_params[$page_route]] : '';



							$sorting_url = $this->url->link($page_route, $query, true);
							$url='';
                        // OCFilter start
                        if (isset($this->request->get['filter_ocfilter'])) {
                            $url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
                        }
                        // OCFilter end
                       
							$sorting_url_array = ['sort' => $sort,
											'order' => $order,
											'route' => $page_route];

							$data['sortings'][$config_sort]['url'] = $sorting_url.$url;
							if (!array_diff_assoc($current_url_array, $sorting_url_array)) {
								$data['sortings'][$config_sort]['selected'] = 'selected';
								$data['current_sort'] = $data[$config_sort];
							} else {
								$data['sortings'][$config_sort]['selected'] = '';
							}
					}

					return $this->load->view($this->config->get('config_template') . '/template/module/sorting.tpl', $data);
			}
	}
}
