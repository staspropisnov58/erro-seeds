<?php
class ControllerProductSpecial extends Controller {

	private $route = 'product/special';

	public function index() {
		$this->load->language('product/special');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		// OCFilter start
    if (isset($this->request->get['filter_ocfilter'])) {
      $filter_ocfilter = $this->request->get['filter_ocfilter'];
			$show_filter = true;
    } else {
      $filter_ocfilter = '';
			$show_filter = false;
    }

		// OCFilter end

		$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/category.js');

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

		$meta_page = ($page != 1) ? unicode_ucfirst($this->language->get('text_page')) . ' ' . $page . ' - ' : '';
		$this->document->setTitle($meta_page . $this->language->get('heading_title'));
		$this->document->setDescription($meta_page . $this->language->get('meta_description'));
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'microdata' => $this->url->link('product/spesial')
		);

		foreach ($data['breadcrumbs'] as $key =>  $value){
			$breadcrumbs_array[$key] = array(
				'@type' => "ListItem",
				"position" => $key,
				"item" => array(
					'id' => $value['microdata'],
					'name' => $value['text'],
				),
			);
		}

		$breadcrumbs = array(
			'@context' => 'https://schema.org',
			'@type' => 'BreadcrumbList',
			'itemListElement'=> $breadcrumbs_array,
		);

		$microdates = array($breadcrumbs);

		$this->document->addMicrodata($microdates);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_in_compare'] = $this->language->get('text_in_compare');
		$data['text_in_wishlist'] = $this->language->get('text_in_wishlist');
		$data['text_buy']	= $this->language->get('text_buy');
		$data['text_show_all'] = $this->language->get('text_show_all');
		$data['text_special'] = $this->language->get('text_special');
		$data['full_text'] = $this->language->get('full_text');
		$data['text_filter'] = $this->language->get('text_filter');
		$data['button_entrance'] = $this->language->get('button_entrance');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['alt'] = $this->language->get('alt');
		$data['title'] = $this->language->get('title');

		$data['compare'] = $this->url->link('product/compare');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit,
			'path'  => str_replace('/','_',$this->request->get['route'])
		);

		// OCFilter start
		$filter_data['filter_ocfilter'] = $filter_ocfilter;
		// OCFilter end

		$product_total = $this->model_catalog_product->getTotalProductSpecials($filter_data);

		$display_settings['display'] = isset($this->request->cookie['display_list']) ? 'list' : 'grid';
		$display_settings['filter_data'] = $filter_data;

		$data['products'] = $this->load->controller('product/product/displayProducts', $display_settings);

		$data['filter_data'] = json_encode($filter_data);

		$url = '';

		// OCFilter start
		if (isset($this->request->get['filter_ocfilter'])) {
			$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
		}
		// OCFilter end

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href'  => $this->url->link('product/special', 'sort=p.sort_order&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'value' => 'pd.name-ASC',
			'href'  => $this->url->link('product/special', 'sort=pd.name&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'value' => 'pd.name-DESC',
			'href'  => $this->url->link('product/special', 'sort=pd.name&order=DESC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_asc'),
			'value' => 'ps.price-ASC',
			'href'  => $this->url->link('product/special', 'sort=ps.price&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_desc'),
			'value' => 'ps.price-DESC',
			'href'  => $this->url->link('product/special', 'sort=ps.price&order=DESC' . $url)
		);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC',
				'href'  => $this->url->link('product/special', 'sort=rating&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => 'rating-ASC',
				'href'  => $this->url->link('product/special', 'sort=rating&order=ASC' . $url)
			);
		}

		$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/special', 'sort=p.model&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_model_desc'),
			'value' => 'p.model-DESC',
			'href'  => $this->url->link('product/special', 'sort=p.model&order=DESC' . $url)
		);

		$url = '';

		// OCFilter start
		if (isset($this->request->get['filter_ocfilter'])) {
			$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
		}
		// OCFilter end

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['limits'] = array();

		$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('product/special', $url . '&limit=' . $value)
			);
		}

		$url = '';

		// OCFilter start
		if (isset($this->request->get['filter_ocfilter'])) {
			$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
		}
		// OCFilter end

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('product/special', $url . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['all'] = $this->url->link('product/special', $url . '&limit=' . $product_total);

		//HACK to show sorts and filter when product_total is greater than limit
		$data['config_limit'] = $this->config->get('config_product_limit');
		$data['product_total'] = $product_total;

		//Canonization

			if( 1 >= ceil($product_total/$limit)){
			$this->document->addLink($this->url->link('product/special'), 'canonical');
			}else{
			if(($page == 1) ){
				$this->document->addLink($this->url->link('product/special'), 'canonical');
				$this->document->addLink($this->url->link('product/special' . '&page=2'), 'next');
			}else{
				if($page == ceil($product_total/$limit)){
					$this->document->addLink($this->url->link('product/special' . '&page=' . ($page-1)), 'prev');
					$this->document->addLink($this->url->link('product/special' . '&page='.$page), 'canonical');
				}else{
					if($page === '2'){
						$this->document->addLink($this->url->link('product/special'), 'prev');
						$this->document->addLink($this->url->link('product/special' . '&page='.$page), 'canonical');
						$this->document->addLink($this->url->link('product/special' . '&page='. ($page+1)), 'next');
					}else{
						$this->document->addLink($this->url->link('product/special' . '&page=' . ($page-1)), 'prev');
						$this->document->addLink($this->url->link('product/special' . '&page='.$page), 'canonical');
						$this->document->addLink($this->url->link('product/special' . '&page='.($page+1)), 'next');
					}
				}
			}
			}

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/special.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/special.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/special.tpl', $data));
		}
	}
}
