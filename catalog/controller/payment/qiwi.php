<?php
class ControllerPaymentQiwi extends Controller {
	public function index() {
		$this->load->language('payment/qiwi');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['bank'] = nl2br($this->config->get('qiwi_bank' . $this->config->get('config_language_id')));

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/qiwi.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/qiwi.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/qiwi.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'qiwi') {
			$this->load->language('payment/qiwi');

			$this->load->model('checkout/order');

			$comment  = $this->language->get('text_instruction') . "\n\n";
			$comment .= $this->config->get('qiwi_bank' . $this->config->get('config_language_id')) . "\n\n";
			$comment .= $this->language->get('text_payment');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('qiwi_order_status_id'), $comment, true);
		}
	}

	public function getPaymentMessage($comment = ''){
		if ($this->session->data['payment_method']['code'] == 'qiwi') {
			$this->load->language('payment/qiwi');

			$this->load->model('checkout/order');

			$comment = $this->config->get('qiwi_payment_details') . "\n\n";
		}else{
			$comment = '';
		}
		return $comment;
	}
}
