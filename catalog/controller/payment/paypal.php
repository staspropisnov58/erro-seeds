<?php
class ControllerPaymentPaypal extends Controller {
	public function index() {
		$this->load->language('payment/paypal');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['bank'] = nl2br($this->config->get('paypal_bank' . $this->config->get('config_language_id')));

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/paypal.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/paypal.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/paypal.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'paypal') {
			$this->load->language('payment/paypal');

			$this->load->model('checkout/order');

			$comment  = $this->language->get('text_instruction') . "\n\n";
			$comment .= $this->config->get('paypal_bank' . $this->config->get('config_language_id')) . "\n\n";
			$comment .= $this->language->get('text_payment');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('paypal_order_status_id'), $comment, true);
		}
	}

	public function getPaymentMessage($comment = ''){
		if ($this->session->data['payment_method']['code'] == 'paypal') {
			$this->load->language('payment/paypal');

			$this->load->model('checkout/order');

			$comment = $this->config->get('paypal_payment_details') . "\n\n";
		}else{
			$comment = '';
		}
		return $comment;
	}
}
