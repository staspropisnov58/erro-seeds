<?php
class ControllerInformationPosts extends Controller {

	private $route = 'information/posts';

	public function index() {
		$this->language->load('information/posts');

		$this->load->model('extension/posts');


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' 		=> $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' 		=> $this->language->get('heading_title'),
			'microdata' 		=> $this->url->link('information/posts')
		);

		foreach ($data['breadcrumbs'] as $key =>  $value){
			$breadcrumbs_array[$key] = array(
				'@type' => "ListItem",
				"position" => $key,
				"item" => array(
					'id' => $value['microdata'],
					'name' => $value['text'],
				),
			);
		}

		$breadcrumbs = array(
			'@context' => 'https://schema.org',
			'@type' => 'BreadcrumbList',
			'itemListElement'=> $breadcrumbs_array,
		);

		$this->document->addMicrodata($breadcrumbs);

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'page' 	=> $page,
			'limit' => 2,
			'start' => 2 * ($page - 1),
		);

		$meta_page = ($page != 1) ? ' - ' . unicode_ucfirst($this->language->get('text_page')) . ' ' . $page : '';
		$this->document->setTitle($this->language->get('heading_title') . $meta_page);
		$total = $this->model_extension_posts->getTotalposts();

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

			$this->load->model('tool/opengraph');

			$this->model_tool_opengraph->addOpengraphForPage($this->route);

		}

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = 2;
		$pagination->url = $this->url->link('information/posts', 'page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * 2) + 1 : 0, ((($page - 1) * 2) > ($total - 2)) ? $total : ((($page - 1) * 2) + 2), $total, ceil($total / 2));

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_title'] = $this->language->get('text_title');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_view'] = $this->language->get('text_view');
		$data['text_posts'] = $this->language->get('text_posts');
        $data['read_more'] = $this->language->get('read_more');

		$all_posts = $this->model_extension_posts->getAllposts($filter_data);

		$data['all_posts'] = array();

		$this->load->model('tool/image');

		foreach ($all_posts as $posts) {
			$data['all_posts'][] = array (
				'title' 		=> $posts['title'],
				'image'			=> $this->model_tool_image->resize($posts['image'], 1140, 400),
				'description' 	=> strip_tags(html_entity_decode($posts['short_description'])),
				'view' 			=> $this->url->link('information/posts/posts', 'posts_id=' . $posts['posts_id']),
				'date_added' 	=> date($this->language->get('date_format_short'), strtotime($posts['date_added']))
			);
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/posts_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/posts_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/information/posts_list.tpl', $data));
		}
	}

	public function posts() {
		$this->load->model('extension/posts');

		$this->language->load('information/posts');

		if (isset($this->request->get['posts_id']) && !empty($this->request->get['posts_id'])) {
			$posts_id = $this->request->get['posts_id'];
		} else {
			$posts_id = 0;
		}

		$posts = $this->model_extension_posts->getposts($posts_id);

        $next_posts = $this->model_extension_posts->getNextPosts($posts_id);
        if ($next_posts && $next_posts['status'] != 0){
            $next_posts = $this->url->link('information/posts/posts', 'posts_id=' . $next_posts['posts_id']);
        }else{
            $next_posts = 1;
        }

        $prev_posts = $this->model_extension_posts->getPrevPosts($posts_id);
        if ($prev_posts && $prev_posts['status'] != 0){
            $prev_posts = $this->url->link('information/posts/posts', 'posts_id=' . $prev_posts['posts_id']);
        }else{
            $prev_posts = 1;
        }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' 			=> $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/posts'),
			'microdata' => $this->url->link('information/posts'),
		);

		if ($posts) {
			$data['breadcrumbs'][] = array(
				'text' 		=> $posts['title'],
				'microdata' 		=> $this->url->link('information/posts/posts', 'posts_id=' . $posts_id)
			);

			foreach ($data['breadcrumbs'] as $key =>  $value){
				$breadcrumbs_array[$key] = array(
					'@type' => "ListItem",
					"position" => $key,
					"item" => array(
						'id' => $value['microdata'],
						'name' => $value['text'],
					),
				);
			}

			$breadcrumbs = array(
				'@context' => 'https://schema.org',
				'@type' => 'BreadcrumbList',
				'itemListElement' => $breadcrumbs_array,
			);

			$microdates = array($breadcrumbs);

			$this->document->addMicrodata($microdates);

			$this->document->setTitle($posts['title']);

			$this->load->model('tool/image');

			$data['image'] = $this->model_tool_image->resize($posts['image'], 1140, 400);

			$data['heading_title'] = $posts['title'];
			$data['description'] = html_entity_decode($posts['description']);
			$data['date_added'] = date('d.m.Y', strtotime($posts['date_added']));

            $data['next_posts'] = $next_posts;
            $data['prev_posts'] = $prev_posts;



            $this->load->model('catalog/reviewposts');


            $data['reviews'] = array();

//            $review_total = $this->model_catalog_reviewposts->getTotalReviewsByProductId($posts_id);



            $results = $this->model_catalog_reviewposts->getReviewsByProductId($posts_id);



            foreach ($results as $result) {
                $data['reviews'][] = array(
                    'author'     => $result['author'],
                    'text'       => nl2br($result['text']),
                    'rating'     => (int)$result['rating'],
                    'rew_id'    => $result['rew_id'],
                    'review_id'    => $result['review_id'],
                    'customer_id'    => $result['customer_id'],
                    'clike'    => $result['clike'],
                    'cdislike'    => $result['cdislike'],
                    'review_id'    => $result['review_id'],
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
                );
            }

            $data['all_rew'] =  $data['reviews'];

            $data['product_id'] =  $posts_id;


			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/posts.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/posts.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/information/posts.tpl', $data));
			}
		} else {
			$data['breadcrumbs'][] = array(
				'text' 		=> $this->language->get('text_error'),
				'href' 		=> $this->url->link('information/posts', 'posts_id=' . $posts_id),
				'microdata' 		=> $this->url->link('information/posts/posts', 'posts_id=' . $posts_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');
			$data['text_error'] = $this->language->get('text_error');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

    public function update_like(){
        $this->load->model('catalog/reviewposts');
        $like = $_POST['like'];
        $this->model_catalog_reviewposts->updateLike((int)$_POST['review_id'],$like);
        echo 1;
    }
    public function addReviews(){

        $this->load->model('catalog/reviewposts');

				$reCaptcha = new ReCaptcha(GOOGLE_SECRET);

				if ($_POST["g-recaptcha-response"]) {
						$response = $reCaptcha->verifyResponse(
								$_SERVER["REMOTE_ADDR"],
								$_POST["g-recaptcha-response"]
						);
				}else{
						$response = null;
				}

				if ($response != null && $response->success) {
					//  echo "Все хорошо.";
				} else {
						$json['error'] = $this->language->get('error_captcha');
				}

				if(empty($_POST['g-recaptcha-response'])){
						$json['error'] = $this->language->get('error_captcha');
				}

				if (!isset($json['error'])) {
					$this->load->model('catalog/review');

					$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

					$json['success'] = $this->language->get('text_success');
				}

        $data = array(
            'name' =>  $_POST['name'],
            'review_id' =>  $_POST['review_id'],
            'text' =>  $_POST['text'],
            'rating' =>  $_POST['rating'],
        );

        $this->model_catalog_reviewposts->addReview($_POST['product_id'], $data);

				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
		}
}
