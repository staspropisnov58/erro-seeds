<?php
class ControllerInformationContact extends Controller {
	use Autocorrect;

	protected $data = [];
	private $route = 'information/contact';

	public function index() {
		$this->load->language('information/contact');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setDescription($this->language->get('meta_description'));

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

			$this->load->model('tool/opengraph');

			$this->model_tool_opengraph->addOpengraphForPage($this->route);

		}

		$errors = [];

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$reCaptcha = new ReCaptcha(GOOGLE_SECRET);
			if ($this->request->post['g-recaptcha-response'] && $reCaptcha->verifyResponse($this->request->server['REMOTE_ADDR'], $this->request->post['g-recaptcha-response'])) {
				unset($this->request->post['g-recaptcha-response']);
				$this->correctValuesOf($this->request->post);
				$this->data = $this->request->post;
				$this->load->model('module/validator');

				$errors = $this->model_module_validator->validate($this->request->post, ['email' => 'contact', 'comment' => 'required']);
				if ($errors) {
					//page reloads with errors
				} else {
					$mail_config = $this->config->get('config_mail');
					$mail = new Mail($mail_config);
					$mail->setTo($this->config->get('config_email'));
					if ($mail_config['smtp_username']) {
						$mail->setFrom($mail_config['smtp_username']);
						$mail->setReplyTo($this->request->post['email']);
					} else {
						$mail->setFrom($this->request->post['email']);
					}
					$mail->setSender($this->request->post['firstname']);
					$mail->setSubject(sprintf($this->language->get('email_subject'), $this->request->post['fistname']));
					$mail->setText(strip_tags($this->request->post['comment']));
					$mail->send();

					$this->response->redirect($this->url->link('information/contact/success'));
				}
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'microdata' => $this->url->link('information/contact')
		);

		foreach ($data['breadcrumbs'] as $key =>  $value){
			$breadcrumbs_array[$key] = array(
				'@type' => "ListItem",
				"position" => $key,
				"item" => array(
					'id' => $value['microdata'],
					'name' => $value['text'],
				),
			);
		}

		$breadcrumbs = array(
			'@context' => 'https://schema.org',
			'@type' => 'BreadcrumbList',
			'itemListElement'=> $breadcrumbs_array,
		);

		$microdates = array($breadcrumbs);

		$this->document->addMicrodata($microdates);

		$this->document->addScript('https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_location'] = $this->language->get('text_location');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_phone_free'] = $this->language->get('text_phone_free');
		$data['text_social'] = $this->language->get('text_social');
		$data['text_internet'] = $this->language->get('text_internet');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_open'] = $this->language->get('text_open');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_enquiry'] = $this->language->get('entry_enquiry');
		$data['entry_captcha'] = $this->language->get('entry_captcha');
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['button_map'] = $this->language->get('button_map');

		if (isset($this->error['captcha'])) {
			$data['error_captcha'] = $this->error['captcha'];
		} else {
			$data['error_captcha'] = '';
		}

		$data['button_submit'] = $this->language->get('button_submit');

		$data['action'] = $this->url->link('information/contact');

		$this->load->model('tool/image');

		if ($this->config->get('config_image')) {
			$data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get('config_image_location_width'), $this->config->get('config_image_location_height'));
		} else {
			$data['image'] = false;
		}

		$data['store'] = $this->config->get('config_name');
		$data['address'] = nl2br($this->config->get('config_address'));
		$data['geocode'] = $this->config->get('config_geocode');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['fax'] = $this->config->get('config_fax');
		$data['open'] = nl2br($this->config->get('config_open'));
		$data['comment'] = $this->config->get('config_comment');

		$data['locations'] = array();

		$this->load->model('localisation/location');

		foreach((array)$this->config->get('config_location') as $location_id) {
			$location_info = $this->model_localisation_location->getLocation($location_id);

			if ($location_info) {
				if ($location_info['image']) {
					$image = $this->model_tool_image->resize($location_info['image'], $this->config->get('config_image_location_width'), $this->config->get('config_image_location_height'));
				} else {
					$image = false;
				}

				$data['locations'][] = array(
					'location_id' => $location_info['location_id'],
					'name'        => $location_info['name'],
					'address'     => nl2br($location_info['address']),
					'geocode'     => $location_info['geocode'],
					'telephone'   => $location_info['telephone'],
					'fax'         => $location_info['fax'],
					'image'       => $image,
					'open'        => nl2br($location_info['open']),
					'comment'     => $location_info['comment']
				);
			}
		}

		if (isset($this->request->post['captcha'])) {
			$data['captcha'] = $this->request->post['captcha'];
		} else {
			$data['captcha'] = '';
		}

		$form = new Form('contact', $this->url->link('information/contact', '', 'SSL'));

    $form->addField(['name' => 'firstname', 'label' => $this->language->get('entry_name'), 'placeholder' => $this->language->get('entry_name'),
		                'sort_order' => 0, 'value' => $this->customer->getFirstName(), 'type' => 'text']);
    $form->addField(['name' => 'email', 'label' => $this->language->get('entry_email'), 'placeholder' => $this->language->get('entry_email'),
		                 'sort_order' => 1, 'value' => $this->customer->getEmail(), 'type' => 'text']);
		$form->addField(['name' => 'comment', 'label' => $this->language->get('entry_comment'), 'placeholder' => $this->language->get('entry_comment'),
		                 'sort_order' => 2, 'value' => '', 'type' => 'textarea']);

		$form->setFieldsValues($this->data);
		$form->setFieldsErrors($errors);

    $data['form'] = $form->build();

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/contact.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/contact.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/information/contact.tpl', $data));
		}
	}

	public function success() {
		$this->load->language('information/contact');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/contact')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_success');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}

	public function send(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('module/validator');
      $this->load->language('information/contact');

      $json = [];

      $this->correctValuesOf($this->request->post);
      $errors = $this->model_module_validator->validate($this->request->post, ['email' => 'contact', 'comment' => 'required']);
      if (!$errors) {
				$mail_config = $this->config->get('config_mail');
				$mail = new Mail($mail_config);
				$mail->setTo('director@errors-seeds.com');
				if ($mail_config['smtp_username']) {
					$mail->setFrom($mail_config['smtp_username']);
					$mail->setReplyTo($this->request->post['email']);
				} else {
					$mail->setFrom($this->request->post['email']);
					$mail->setReplyTo($this->request->post['email']);
				}
				$mail->setSender('Guest');
				$mail->setSubject($this->language->get('email_modal_subject'));
				$mail->setText(strip_tags($this->request->post['comment']));
				$mail->send();

        $json['message'] = ['text' =>$this->language->get('text_success_modal'), 'type' => 'success'];
      } else {
        $json['errors'] = $errors;
      }

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
  }
}
