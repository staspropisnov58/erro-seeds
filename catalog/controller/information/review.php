<?php
class ControllerInformationReview extends Controller
{
  use Autocorrect;

  public function addReview(){
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('catalog/review');
      $this->load->model('module/validator');
      $this->load->language('product/product');

      $json = [];

      $this->correctValuesOf($this->request->post);
      $errors = $this->model_module_validator->validate($this->request->post, ['email' => 'contact', 'comment' => 'required']);
      if (!$errors) {
        $this->model_catalog_review->addReview($this->request->post);
        $json['message'] = ['text' =>$this->language->get('text_success'), 'type' => 'success'];
      } else {
        $json['errors'] = $errors;
      }

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
  }
}
