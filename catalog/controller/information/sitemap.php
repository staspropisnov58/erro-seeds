<?php
class ControllerInformationSitemap extends Controller {
	public $limit = 100;
	private $route = 'information/sitemap';

	public function index() {
		$this->load->language('information/sitemap');

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

			$this->load->model('tool/opengraph');

			$this->model_tool_opengraph->addOpengraphForPage($this->route);

		}
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['robots'] = $this->document->getRobots();
        $data['scripts'] = $this->document->getScripts();
        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $data['icon'] = $server . 'image/' . $this->config->get('config_icon');
        } else {
            $data['icon'] = '';
        }

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex, noarchive');

		if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
		} else {
				$page = 1;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_history'] = $this->language->get('text_history');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_cart'] = $this->language->get('text_cart');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_search'] = $this->language->get('text_search');
		$data['text_information'] = $this->language->get('text_information');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_back_home'] = $this->language->get('text_back_home');
		$data['text_blog'] = $this->language->get('text_blog');

		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('catalog/manufacturer');
		$this->load->model('extension/news');

		$links = array();
		$limit = $this->limit;
		$categories_1 = $this->model_catalog_category->getCategories(0);
		$href = array('controller' => 'product/category', 'item' => 'category_id');

		foreach ($categories_1 as $category_1) {
			$categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);

			if (!empty($categories_2)) {
					$category = array('name' => $category_1['name'], 'href' => $this->url->link('product/category', 'path=' . $category_1['category_id']));
					$this->getLinks($categories_2, $limit, $links, $href, $category);
			}
		}

		$articles = $this->model_extension_news->getAllNews(array('order' => 'DESC'));
		$href = array('controller' => 'inforamtion/news', 'item' => 'news_id');
		$blog = array('name' => $data['text_blog'], 'href' => $this->url->link('information/news', ''));
		if (!empty($blog)) {
			$this->getLinks($articles, $limit, $links, $href, $blog);
		}

		$limit = $this->limit;
		$pages = array();
		$pages[1] = array();

		foreach ($links as $category) {
			$limit -= $category['num_links'];
			if ($limit >= 0) {
					$pages[key($pages)][] = $category;
			} else {
				$pages[] = array($category);
				end($pages);
				$limit = $this->limit - $category['num_links'];
			}
		}

		$pagination = new Pagination();
		$pagination->total = count($pages);
		$pagination->page = $page;
		$pagination->limit = 1;
		$pagination->url = $this->url->link('information/sitemap', '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['map'] = $pages[$page];

		if ($pagination->total - $pagination->limit > 0) {
			if ($page !== 1) {
				$this->document->addLink($this->url->link('information/sitemap', '&page=' . $page), 'canonical');
				if ((int)$page === 2) {
					$this->document->addLink($this->url->link('information/sitemap'), 'prev');
				} else {
					$this->document->addLink($this->url->link('information/sitemap', '&page=' . ($page - 1)), 'prev');
				}
				if ($page < ($pagination->total / $pagination->limit)) {
					$this->document->addLink($this->url->link('information/sitemap', '&page=' . ($page + 1)), 'next');
				}
			} else {
				$this->document->addLink($this->url->link('information/sitemap'), 'canonical');
				$this->document->addLink($this->url->link('information/sitemap', '&page=2'), 'next');
			}
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/sitemap.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/sitemap.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/information/sitemap.tpl', $data));
		}
	}

	private function getLinks ($items = array(), &$limit, &$links = array(), $href = array(), $parent) {
		$result = array();
		foreach ($items as $item) {
			$name = isset($item['name']) ? $item['name'] : $item['title'];
			$result[] = array(
				'name' => $name,
				'href' => $this->url->link($href['controller'],  $href['item'] . '=' . $item[$href['item']])
			);
		}

		if (count($result) > $limit - 1) {
			$chunks = array_chunk(array_slice($result, $limit - 2), $this->limit);
			$result = array_slice($result, 0, $limit - 1);
			$links[] = array(
				'name'     => $parent['name'],
				'content' => $result,
				'href'     => $parent['href'],
				'num_links' => count($result) + 1
			);
			foreach ($chunks as $chunk) {
				$links[] = array(
					'content' => $chunk,
					'num_links' => count($chunk)
				);
			}

			$limit = $this->limit - count(end($chunks));
		} else {
			$links[] = array(
				'name'     => $parent['name'],
				'content' => $result,
				'href'     => $parent['href'],
				'num_links' => count($result) + 1
			);

			$limit = $this->limit - (count($result) + 1);
		}
	}
}
