<?php
class ControllerInformationNews extends Controller {

	private $route = 'information/news';

	public $limit = 2;

	public function index() {
		$this->language->load('information/news');

		$this->load->model('extension/news');


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' 		=> $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' 		=> $this->language->get('heading_title'),
			'microdata' => $this->url->link('information/news')
		);

		foreach ($data['breadcrumbs'] as $key =>  $value){
			$breadcrumbs_array[$key] = array(
				'@type' => "ListItem",
				"position" => $key,
				"item" => array(
					'id' => $value['microdata'],
					'name' => $value['text'],
				),
			);
		}

		$breadcrumbs = array(
			'@context' => 'https://schema.org',
			'@type' => 'BreadcrumbList',
			'itemListElement'=> $breadcrumbs_array,
		);

		$this->document->addMicrodata([$breadcrumbs]);

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
			'page' 	=> $page,
			'limit' => 12,
			'start' => 12 * ($page - 1),
		);
		$meta_page = ($page != 1) ? unicode_ucfirst($this->language->get('text_page')) . ' ' . $page . ' - ': '';
		$this->document->setTitle($meta_page . $this->language->get('heading_title'));
		$this->document->setDescription($meta_page . $this->language->get('meta_description'));

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

			$this->load->model('tool/opengraph');

			$this->model_tool_opengraph->addOpengraphForPage($this->route);

		}


		$total = $this->model_extension_news->getTotalNews();
		$limit = $filter_data['limit'];

		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = 8;
		$pagination->url = $this->url->link('information/news', 'page={page}');

		$data['pagination'] = $pagination->render();

		$xml = $this->config->get('xml');

		foreach ($xml as $key => &$xml1){
				$xml1 = $xml1.'.xml';
		}

		$data['xml_href'] = $this->config->get('config_url') . 'rss/'.$xml[$this->config->get('config_language_id')];

				//Canonization

		if( 1 >= ceil($total/$limit)){
		$this->document->addLink($this->url->link('information/news'), 'canonical');
		}else{
		if(($page == 1) ){
			$this->document->addLink($this->url->link('information/news'), 'canonical');
			$this->document->addLink($this->url->link('information/news' . '&page=2'), 'next');
		}else{
			if($page == ceil($total/$limit)){
				$this->document->addLink($this->url->link('information/news' . '&page=' . ($page-1)), 'prev');
				$this->document->addLink($this->url->link('information/news' . '&page='.$page), 'canonical');
			}else{
				if($page === '2'){
					$this->document->addLink($this->url->link('information/news'), 'prev');
					$this->document->addLink($this->url->link('information/news' . '&page='.$page), 'canonical');
					$this->document->addLink($this->url->link('information/news' . '&page='. ($page+1)), 'next');
				}else{
					$this->document->addLink($this->url->link('information/news' . '&page=' . ($page-1)), 'prev');
					$this->document->addLink($this->url->link('information/news' . '&page='.$page), 'canonical');
					$this->document->addLink($this->url->link('information/news' . '&page='.($page+1)), 'next');
				}
			}
		}
		}

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * 8) + 1 : 0, ((($page - 1) * 8) > ($total - 8)) ? $total : ((($page - 1) * 8) + 8), $total, ceil($total / 8));

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_title'] = $this->language->get('text_title');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_view'] = $this->language->get('text_view');
        $data['read_more'] = $this->language->get('read_more');
        $data['text_news'] = $this->config->get('blog_names')[$this->config->get('config_language_id')];

		$all_news = $this->model_extension_news->getAllNews($filter_data);

		$data['all_news'] = array();

		$this->load->model('tool/image');

		foreach ($all_news as $news) {
			if($news['image']){
				$small_image = $this->model_tool_image->resize($news['image'], $this->config->get('config_image_small_news_width'), $this->config->get('config_image_small_news_height'));
			}else{
				$small_image = $this->model_tool_image->resize('ES_small_logo.png', $this->config->get('config_image_small_news_width'), $this->config->get('config_image_small_news_height'));
			}
			$data['all_news'][] = array (
				'title' 		=> $news['title'],
				'image'			=> $small_image,
				'description' 	=> strip_tags(html_entity_decode($news['short_description'])),
				'view' 			=> $this->url->link('information/news/news', 'news_id=' . $news['news_id']),
				'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))
			);
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/news_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/news_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/information/news_list.tpl', $data));
		}
	}

	public function news() {
		$this->load->model('extension/news');

		$this->language->load('information/news');

		if (isset($this->request->get['news_id']) && !empty($this->request->get['news_id'])) {
			$news_id = $this->request->get['news_id'];
		} else {
			$news_id = 0;
		}

		$news = $this->model_extension_news->getNews($news_id);



        $next_news = $this->model_extension_news->getNextNews($news_id);
        if ($next_news && $next_news['status'] != 0){
            $next_news = $this->url->link('information/news/news', 'news_id=' . $next_news['news_id']);
        }else{
            $next_news = 1;
        }

        $prev_news = $this->model_extension_news->getPrevNews($news_id);
        if ($prev_news && $prev_news['status'] != 0){
            $prev_news = $this->url->link('information/news/news', 'news_id=' . $prev_news['news_id']);
        }else{
            $prev_news = 1;
        }

        $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' 			=> $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/news'),
			'microdata' => $this->url->link('information/news')
		);

		if ($news) {
			$data['breadcrumbs'][] = array(
				'text' 		=> $news['title'],
				'microdata' 		=> $this->url->link('information/news', 'news_id=' . $news_id),
			);

			foreach ($data['breadcrumbs'] as $key =>  $value){
				$breadcrumbs_array[$key] = array(
					'@type' => "ListItem",
					"position" => $key,
					"item" => array(
						'id' => $value['microdata'],
						'name' => $value['text'],
					),
				);
			}

			$breadcrumbs = array(
				'@context' => 'https://schema.org',
				'@type' => 'BreadcrumbList',
				'itemListElement'=> $breadcrumbs_array,
			);

			$microdates = array($breadcrumbs);

			$this->document->addMicrodata($microdates);



			$this->document->setTitle($news['meta_title']);
			$this->document->setDescription($news['meta_description']);
			$this->document->setKeywords($news['meta_keyword']);
			if ($news['meta_robots'] !== ''){
				$this->document->setRobots($news['meta_robots']);
			}
			$this->load->model('tool/image');


			if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

				$this->load->model('tool/opengraph');

				$this->model_tool_opengraph->addOpengraphForNews($news);

			}

			$data['image_2'] = $this->model_tool_image->resize($news['image_2'], $this->config->get('config_image_big_news_width'), $this->config->get('config_image_big_news_height'));

            $data['next_news'] = $next_news;
            $data['prev_news'] = $prev_news;


            $this->load->model('catalog/reviewnews');


            $data['reviews'] = array();

//            $review_total = $this->model_catalog_reviewnews->getTotalReviewsByProductId($news_id);



            $reviews = $this->model_catalog_reviewnews->getReviewsByProductId($news_id);

						foreach ($reviews as $review) {
							$child_reviews_data = $this->model_catalog_reviewnews->getReviewsByProductId($news_id, $review['review_id']);
							$child_reviews = [];

							if ($child_reviews_data) {
								foreach ($child_reviews_data as $child_review) {
									$child_reviews[] = array(
											'author'     => $child_review['author'],
											'title'      => $child_review['title'],
											'text'       => nl2br($child_review['text']),
											'rating'     => (int)$child_review['rating'],
											'review_id'    => $child_review['review_id'],
											'date_added' => date($this->language->get('date_format_short'), strtotime($child_review['date_added']))
										);
								}

							}
								$data['reviews'][] = array(
										'author'     => $review['author'],
										'title'      => $review['title'],
										'text'       => nl2br($review['text']),
										'rating'     => (int)$review['rating'],
										'review_id'    => $review['review_id'],
										'date_added' => date($this->language->get('date_format_short'), strtotime($review['date_added'])),
										'child_reviews' => $child_reviews,
								);
						}

						$data['write_review'] = 'getModal(\'modal=review&news_id=' . $news_id . '\', event)';


						$data['quantity_comments'] = count($data['reviews']);

            $data['all_rew'] =  $data['reviews'];

            $data['product_id'] =  $news_id;



			$data['heading_title'] = $news['title'];
			$data['description'] = html_entity_decode($news['description']);
			$data['date_added'] = date('d.m.Y', strtotime($news['date_added']));
			$data['read_more'] = $this->language->get('read_more');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['url_all_news'] = $this->url->link('information/news');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/news.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/news.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/information/news.tpl', $data));
			}
		} else {
			$data['breadcrumbs'][] = array(
				'text' 		=> $this->language->get('text_error'),
				'href' 		=> $this->url->link('information/news', 'news_id=' . $news_id),
				'microdata' 		=> $this->url->link('information/news', 'news_id=' . $news_id),
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');
			$data['text_error'] = $this->language->get('text_error');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

    public function update_like(){
        $this->load->model('catalog/reviewnews');
        $like = $_POST['like'];
        $this->model_catalog_reviewnews->updateLike((int)$_POST['review_id'],$like);
        echo 1;
    }
    public function addReviews(){
//				$this->load->language('product/product');
        $this->load->model('catalog/reviewnews');

				$reCaptcha = new ReCaptcha(GOOGLE_SECRET);

				if ($_POST["g-recaptcha-response"]) {
						$response = $reCaptcha->verifyResponse(
								$_SERVER["REMOTE_ADDR"],
								$_POST["g-recaptcha-response"]
						);
				}else{
						$response = null;
				}

				if ($response != null && $response->success) {
					//  echo "Все хорошо.";
				} else {
						$json['error'] = $this->language->get('error_captcha');
				}

				if(empty($_POST['g-recaptcha-response'])){
						$json['error'] = $this->language->get('error_captcha');
				}

				if (!isset($json['error'])) {
					$this->load->model('catalog/review');

					$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

					$json['success'] = $this->language->get('text_success');
				}

        $data = array(
            'name' =>  $_POST['name'],
            'review_id' =>  $_POST['review_id'],
            'text' =>  $_POST['text'],
            'rating' =>  $_POST['rating'],
        );

        $this->model_catalog_reviewnews->addReview($_POST['product_id'], $data);

				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			}
}
