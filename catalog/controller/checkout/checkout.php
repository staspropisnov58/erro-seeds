<?php
class ControllerCheckoutCheckout extends Controller
{
  private $steps = ['customer', 'shipping_method', 'shipping_address', 'payment_method', 'confirm'];

  public function index()
  {
    $errors = array();
    $this->load->language('checkout/checkout');
    $data['heading_title'] = $this->language->get('heading_title');
    $data['text_h1']       = $this->language->get('text_checkout');

    if (isset($this->session->data['order']['pending']) || !$this->session->data['cart']) {
      $this->response->redirect($this->url->link('common/home', '', true));
    }

    if (isset($this->session->data['order']['next_step'])) {
      $data['text_next_step'] = $this->language->get('text_' . $this->session->data['order']['next_step']);
    }

    if (isset($this->session->data['order']['prev_step'])) {
      $data['prev_step'] = $this->session->data['order']['prev_step'];
      $data['prev_step_href'] = $this->url->link('checkout/checkout', '&back_to=' . $this->session->data['order']['prev_step'], true);
      $data['text_prev_step'] = $this->language->get('text_' . $this->session->data['order']['prev_step']);
    } else {
      $data['prev_step'] = '';
    }

    $data['coupon'] = '';
    $data['start_checkout'] = '';

    if ($this->request->server['REQUEST_METHOD'] === 'POST') {
      $current_step = $this->session->data['order']['current_step'];

      $this->load->model('checkout/' . $current_step);
      $model_name = 'model_checkout_' . $current_step;
      $errors = $this->{$model_name}->save($this->request->post);
    } else if (isset($this->request->get['back_to'])) {
      $this->session->data['order']['current_step'] = $this->request->get['back_to'];
    }

    $current_step = isset($this->session->data['order']['current_step']) && $this->session->data['order']['current_step'] ? $this->session->data['order']['current_step'] : 'customer';

    $data['steps'] = $this->updateSteps($current_step);


    $data['current_step'] = $current_step;
    $data['text_go_back'] = $this->language->get('text_go_back');
    $this->load->model('checkout/' . $current_step);
    $model_name = 'model_checkout_' . $current_step;
    $data['content'] = $this->{$model_name}->load($this->request->post);

    $e_commerce_data = ['currency_code' => $this->currency->getCode(),
                         'current_step_id' => (int)array_search($current_step, $this->steps) + 1,
                         'option' => in_array($current_step, ['shipping_address', 'confirm']) ? $this->session->data[$this->session->data['order']['prev_step']]['title'] : '',
                       'products' => $this->load->controller('checkout/cart/getProductsForEcommerce')];

    $data['messages'] = $this->getMessages();

    if ($current_step === 'confirm') {
      $data['cart'] = '';
    } else {
      $data['cart'] = $this->load->controller('checkout/cart', true);
    }

    $data['e_commerce'] = $this->load->view($this->config->get('config_template') . '/template/module/e_commerce_step.tpl', $e_commerce_data);

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/index.tpl')) {
      $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/index.tpl', $data));
    }
  }

  public function remove_addition()
  {
    if ($this->request->server['REQUEST_METHOD'] === 'POST') {
      if (isset($this->request->get['addition_id'])) {
        $json['uncheck'] = $this->request->get['addition_id'];
        unset($this->session->data['order']['additions'][$this->request->get['addition_id']]);
      }

      if (isset($this->request->post['redirect'])) {
        $this->session->data['cart_is_open'] = 1;
        $this->response->redirect($this->request->post['redirect']);
      } else {

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
      }
    }
  }

  public function sendForm()
  {
    $json = array();
    $this->load->language('checkout/checkout');

    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $current_step = $this->session->data['order']['current_step'];

      $this->load->model('checkout/' . $current_step);
      $model_name = 'model_checkout_' . $current_step;
      $json['errors'] = $this->{$model_name}->save($this->request->post);

      if ($json['errors']) {
        $json['form'] = $this->{$model_name}->data;
        $json['gaData'] = [];
      } else {
        $current_step = $this->session->data['order']['current_step'];
        $json['hide_cart'] = $current_step === 'confirm';
        $json['current_step'] = $this->session->data['order']['current_step'];
        $json['steps'] = $this->updateSteps($current_step);

        $json['content'] = $this->getStepContent($current_step);

        $products = [];
        foreach ($this->load->controller('checkout/cart/getProductsForEcommerce') as $product) {
          $products[] = ['name' => $product['name'],
          'id' => $product['product_id'],
          'price' => $product['price'],
          'brand' => $product['manufacturer'],
          'category' => '',
          'variant' => $product['option'],
          'quantity' => $product['quantity'],
          'coupon' => '',
        ];
      }

        $json['gaData'] = [ 'ecommerce' => [
           'currencyCode' => $this->currency->getCode(),
           'checkout' => [
             'actionField' => [
               'step' => (int)array_search($current_step, $this->steps) + 1,
               'option' => in_array($current_step, ['shipping_address', 'confirm']) ? $this->session->data[$this->session->data['order']['prev_step']]['title'] : '',
             ],
             'products' => [$products]
           ]
         ],
         'event' => 'gtm-ee-event',
         'gtm-ee-event-category' => 'Enhanced Ecommerce',
         'gtm-ee-event-action' => 'Checkout Step ' . (int)array_search($current_step, $this->steps) + 1,
         'gtm-ee-event-non-interaction' => 'False',
       ];
     }
      $json['messages'] = $this->getMessages();

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
  }



  public function updateCart()
  {
    $json = array();
    $this->load->language('checkout/checkout');

    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $current_step = $this->session->data['order']['current_step'];
      $this->load->model('checkout/' . $current_step);
      $model_name = 'model_checkout_' . $current_step;
      $this->{$model_name}->update_current_step = false;
      $json['errors'] = $this->{$model_name}->save($this->request->post);
      $json['messages'] = $this->getMessages();

      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
  }

  public function getPrevStep()
  {
    $json = [];

    if (isset($this->request->get['back_to'])) {
      $this->session->data['order']['current_step'] = $this->request->get['back_to'];
      $json['steps'] = $this->updateSteps($this->request->get['back_to']);

      $json['current_step'] = $this->request->get['back_to'];

      $json['content'] = $this->getStepContent($this->request->get['back_to']);
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  protected function getStepContent($current_step)
  {
    $this->load->model('checkout/' . $current_step);
    $model_name = 'model_checkout_' . $current_step;
    return $this->{$model_name}->load($this->request->post);
  }

/**
* update steps in session
* @return array $steps with breadcrumbs
*/
  public function updateSteps($current_step)
  {
    $this->load->language('checkout/checkout');

    $current_position = array_search($current_step, $this->steps);
    $this->session->data['order']['current_step'] = $current_step;

    if ($current_position > 0) {
      $this->session->data['order']['prev_step'] = $this->steps[$current_position - 1];
    } else {
      unset($this->session->data['order']['prev_step']);
    }

    if ($current_position >= count($this->steps)) {
      unset($this->session->data['order']['next_step']);
    } else {
      $this->session->data['order']['next_step'] = $this->steps[$current_position + 1];
    }


    foreach ($this->steps as $position => $step) {
      $class = '';
      $href = '';
      if ($position < $current_position) {
        $class = 'done';
        $href = $this->url->link('checkout/checkout', '&back_to=' . $step, 'SSL');
      } elseif ($position === $current_position) {
        $class = 'active';
      }
      $steps_data[] = ['text' => $this->language->get('text_' . $step),
                      'class' => $class,
                      'name' => $step,
                      'href' => $href];
    }

    return $steps_data;
  }

  public function getSteps()
  {
    return $this->steps;
  }

  protected function getMessages()
  {
    //Each message is array with text and type. Type of message should be equal to one of bootstrap alert class
    if (isset($this->session->data['order']['messages'])) {
      $messages = $this->session->data['order']['messages'];
      unset($this->session->data['order']['messages']);
    } else {
      $messages = array();
    }

    return $messages;
  }
}
