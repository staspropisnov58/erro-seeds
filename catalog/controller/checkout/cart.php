<?php

class ControllerCheckoutCart extends Controller
{
    public function index($in_checkout = true)
    {
        $data = array();

        $this->load->language('checkout/cart');

        //In ControllerCommonHeader getScripts ad getStyles calls should be moved below getting the cart
        $this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/checkout/cart.js');

        $data['text_empty'] = $this->language->get('text_empty');
        $data['text_checkout'] = $this->language->get('text_checkout');

        $data['text_bonus_balans'] = $this->language->get('text_bonus_balans');
        $data['text_bonus_balans_sel'] =  sprintf($this->language->get('text_bonus_balans_sel'),'30%');

        $data['text_bonus_balans_but'] = $this->language->get('text_bonus_balans_but');

        $data['text_quick_order'] = $this->language->get('text_quick_order');
        $data['text_no_js'] = $this->language->get('text_no_js');

        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_update'] = $this->language->get('button_update');

        $data['current_page'] = $this->request->server['REQUEST_URI'];

        $cart_content = $this->getCartContent();

        $data = array_merge($data, $cart_content);

        if (!$this->validateMinOrder($this->cart->getTotal(), $this->config->get('checkout_min_order'))) {
            $data['error_min_order'] = sprintf($this->language->get('error_min_order'), $this->currency->format($this->config->get('checkout_min_order')));
            $data['error_payment_min_order'] = '';
        } else {
            $data['error_payment_min_order'] = $this->validatePaymentsMinOrder($this->cart->getTotal());
            $data['error_min_order'] = '';
        }

        $data['error_stock'] = $this->cart->hasStock() ? '' : $this->language->get('error_stock');

        $data['edit_action'] = $this->url->link('checkout/cart/edit', '', true);
        $data['remove_action'] = $this->url->link('checkout/cart/remove', '', true);

        $data['current_step'] = isset($this->session->data['order']['current_step']) ? $this->session->data['order']['current_step'] : '';

        if ($in_checkout) {
            $data['coupon'] = '';
            $data['start_checkout'] = '';
        } else {
            $data['text_next_step'] = $this->language->get('text_normal_order');
            $data['start_checkout'] = $this->url->link('checkout/checkout', '', true);

            $data['fast_order'] = $this->config->get('checkout_fast_order_status') ?
                $this->url->link('checkout/fast_order', '', true) :
                '';

            $data['coupon'] = $this->load->controller('checkout/coupon');
        }
        $data['customer_group_id'] = $this->customer->getGroupId();

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/cart.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/checkout/cart.tpl', $data);
        } else {
            return $this->load->view('default/template/checkout/cart.tpl', $data);
        }
    }

    public function add()
    {
        $this->load->language('checkout/cart');

        $json = array();
        $scripts = array();

        if (!$this->cart->countProducts()) {
            $scripts[] = 'catalog/view/theme/' . $this->config->get('config_template') . '/js/checkout/cart.js';
        }

        if (isset($this->request->post['product_id'])) {
            $product_id = (int)$this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if (isset($this->request->post['quantity'])) {
                $quantity = (int)$this->request->post['quantity'];
            } else {
                $quantity = 1;
            }

            if (isset($this->request->post['option'])) {
                $option = $this->request->post['option'];
            } else {
                $option = array();
            }

            //TODO: switch to ModelCatalogOption
            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
                }
            }

            if (isset($this->request->post['recurring_id'])) {
                $recurring_id = $this->request->post['recurring_id'];
            } else {
                $recurring_id = 0;
            }

            $recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

            if ($recurrings) {
                $recurring_ids = array();

                foreach ($recurrings as $recurring) {
                    $recurring_ids[] = $recurring['recurring_id'];
                }

                if (!in_array($recurring_id, $recurring_ids)) {
                    $json['error']['recurring'] = $this->language->get('error_recurring_required');
                }
            }

            if (!$json) {
                $product_key = $this->cart->add($this->request->post['product_id'], $this->request->post['quantity'], $option, $recurring_id);

                if ($product_key) {
                    $product = array_merge($product_info, $this->cart->getProducts()[$product_key]);
                    $product['quantity'] = $quantity;
                    if (isset($this->request->post['price'])) {
                        $product['price'] = $this->request->post['price'];
                    }

                    $this->load->model('builder/product');

                    $product_data = $this->model_builder_product->buildForAddToCartMessage($product);
                    $product_data['text_success'] = $this->language->get('text_success');
                    $product_data['text_quantity'] = $this->language->get('text_quantity');
                    $product_data['button_continue_shopping'] = $this->language->get('button_continue_shopping');
                    $product_data['button_to_cart'] = $this->language->get('button_to_cart');

                    $json['success_cart'] = $this->load->view($this->config->get('config_template') . '/template/modal/success_cart.tpl', $product_data);
                    $json['scripts'] = $scripts;
                    $json['gaData'] = ['event' => 'gtm-ee-event',
                        'gtm-ee-event-category' => 'Enhanced Ecommerce',
                        'gtm-ee-event-action' => 'Adding a Product to a Shopping Cart',
                        'gtm-ee-event-non-interaction' => 'False',
                        'ecommerce' => [
                            'currencyCode' => $this->currency->getCode(),
                            'add' => [
                                'products' => [
                                    $this->model_builder_product->buildForEcommerce($product),
                                ],
                            ],
                        ]];
                } else {
                    $json['message'] = ['type' => 'danger',
                        'text' => sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'))];

                }
            } else {
                $json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
            }
        }

        if (isset($this->request->post['redirect'])) {
            $this->session->data['cart_is_open'] = 1;
            $this->response->redirect($this->request->post['redirect']);
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function edit()
    {
        $this->load->language('checkout/cart');

        $json = array();

        // Update
        if (!empty($this->request->post['quantity'])) {
            foreach ($this->request->post['quantity'] as $key => $value) {
                $this->cart->update($key, $value);
            }

            $json['success'] = true;
        }

        if (isset($this->request->post['redirect'])) {
            $this->session->data['cart_is_open'] = 1;
            $this->response->redirect($this->request->post['redirect']);
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function remove()
    {
        $this->load->language('checkout/cart');

        $json = array();

        // Remove
        if (isset($this->request->post['key'])) {
            $this->cart->remove($this->request->post['key']);

            unset($this->session->data['vouchers'][$this->request->post['key']]);

            $this->session->data['success'] = $this->language->get('text_remove');
            $json['success'] = true;

            // unset($this->session->data['shipping_method']);
            // unset($this->session->data['shipping_methods']);
            // unset($this->session->data['payment_method']);
            // unset($this->session->data['payment_methods']);
            // unset($this->session->data['reward']);

        }

        if (!$this->cart->getProducts()) {
            $this->session->data['product_deleted'] = sprintf($this->language->get('text_product_deleted'), $this->url->link('common/home', '', 'SSL'));
        }

        if (isset($this->request->post['redirect'])) {
            $this->session->data['cart_is_open'] = 1;
            $this->response->redirect($this->request->post['redirect']);
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function reloadCart()
    {
        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'GET') {
            $json['cart_popup'] = $this->index(!empty($this->request->get['in_checkout']));
            $json['count'] = $this->cart->countProducts() ? $this->cart->countProducts() : '';
            $json['total'] = $this->cart->getTotal() ? $this->currency->format($this->cart->getTotal()) : $this->language->get('text_empty');

            if (isset($this->session->data['order']['current_step']) && $this->session->data['order']['current_step'] === 'payment_method') {
                $this->load->language('checkout/checkout');

                $current_step = $this->session->data['order']['current_step'];
                $this->load->model('checkout/' . $current_step);
                $model_name = 'model_checkout_' . $current_step;

                $json['reload_step'] = ['name' => $current_step,
                    'content' => $this->{$model_name}->load([])];
            } else {
                // $json['reload_step'] = [];
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function getCartContent()
    {
        $this->load->language('checkout/cart');

        $data['text_items'] = $this->language->get('text_items');
        $data['text_quantity_pack'] = $this->language->get('text_quantity_pack');
        $data['text_quantity'] = $this->language->get('text_quantity');
        $data['text_size'] = $this->language->get('text_size');
        $data['text_color'] = $this->language->get('text_color');
        $data['text_in_package'] = $this->language->get('text_in_package');

        $data['button_remove_item'] = $this->language->get('button_remove_item');

        $this->load->model('tool/image');
        $this->load->model('tool/upload');

        $data['products'] = array();

        $total_price = 0;

        foreach ($this->cart->getProducts() as $product) {
            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
            }

            $option_data = array();

            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name' => $option['name'],
                    'value' => $value,
                    'type' => $option['type']
                );
            }

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }


            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
            } else {
                $total = false;
            }

            if ($product['maximum_exeeded']) {
                $error_maximum_exeeded = sprintf($this->language->get('error_maximum_exeeded'), $product['maximum']);
            } else {
                $error_maximum_exeeded = '';
            }

            $data['products'][] = array(
                'key' => $product['key'],
                'sku' => $product['sku'],
                'thumb' => $image,
                'name' => $product['name'],
                'model' => $product['model'],
                'option' => $option_data,
                'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
                'quantity' => $product['quantity'],
                'stock' => $product['stock'],
                'price' => $price,
                'total' => $total,
                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                'error_maximum_exeeded' => $error_maximum_exeeded,
            );
        }

        $data['totals'] = array();

        $this->load->model('extension/extension');

        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        // Display prices
        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');


            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {

                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);

                }
            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);
        }

        foreach ($total_data as $result) {
            if ($result['code'] === 'total') {
                $data['total'] = array(
                    'title' => $result['title'],
                    'text' => $this->currency->format($result['value']),
                );
            } else {
                $data['subtotals'][$result['code']] = array(
                    'code' => $result['code'],
                    'title' => $result['title'],
                    'value' => $result['value'],
                    'text' => $this->currency->format($result['value']),
                    'remove' => isset($result['remove']) ? $result['remove'] : '',
                );
            }
        }
//      if($this->customer->getGroupId()==3){
//          print_r($data);
//      }
        $data['cart_gifts'] = [];

        if ($this->config->get('gift_status')) {
            $this->load->model('checkout/gift');

            $data['cart_gifts'] = $this->model_checkout_gift->getGifts($data['subtotals']);
        }
        $data['balance'] = 0;
        if ($this->customer->isLogged()) {
            if (isset($this->session->data['sale_bonuse'])) {
                $bonus_session = $this->customer->getUserBonus() - $this->session->data['sale_bonuse'];

                $data['balance'] = $this->currency->format($this->tax->calculate($bonus_session, 0, $this->config->get('config_tax')));
                $data['balance_old'] = $bonus_session;
            } else {
                $data['balance'] = $this->currency->format($this->tax->calculate($this->customer->getUserBonus(), 0, $this->config->get('config_tax')));
                $data['balance_old'] = $this->customer->getUserBonus();
            }

        }


        if ($data['products'] === []) {
            $data['text_empty'] = $this->language->get('text_empty');
            if (isset($this->session->data['product_deleted'])) {
                $data['text_empty_cart'] = $this->session->data['product_deleted'];
                unset($this->session->data['product_deleted']);
            } else {
                $data['text_empty_cart'] = sprintf($this->language->get('text_empty_cart'), $this->url->link('account/register', '', 'SSL'), $this->url->link('account/login', '', 'SSL'));
            }
        }

        return $data;
    }

    public function addBonuse()
    {
        $data['balance_old'] = $this->customer->getUserBonus();
        $json = array();
        $bonuse = $this->request->post['sale_price'];
        $json['success'] = true;
        if (isset($this->session->data['sale_bonuse']) && $this->session->data['sale_bonuse'] + $bonuse <= $data['balance_old']) {
            $this->session->data['sale_bonuse'] = $this->session->data['sale_bonuse'] + $bonuse;
        } else {
            $this->session->data['sale_bonuse'] = $bonuse;
        }
        if ($this->request->post['sale_price'] == 0) {
            unset($this->session->data['sale_bonuse']);
        }
//

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getProductsForEcommerce()
    {
        $data = [];

        $total_price = 0;

        foreach ($this->cart->getProducts() as $product) {
            $option_data = '';

            foreach ($product['option'] as $option) {
                $option_data .= $option['name'] . ': ' . $option['value'] . '; ';
            }

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }


            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
            } else {
                $total = false;
            }

            $this->load->model('catalog/category');
            $main_category = $this->model_catalog_category->getMainCategory($product['product_id']);

            $data[] = array(
                'product_id' => $product['product_id'],
                'key' => $product['key'],
                'sku' => $product['sku'],
                'name' => $product['name'],
                'manufacturer' => $product['model'],
                'option' => $option_data,
                'quantity' => $product['quantity'],
                'stock' => $product['stock'],
                'price' => $price,
                'total' => $total,
                'category' => $main_category['category_path_name'],
            );
        }

        return $data;
    }

    protected function validateMinOrder($subtotal, $minimum)
    {
        if ($subtotal < $minimum) {
            return false;
        } else {
            return true;
        }
    }

    protected function validatePaymentsMinOrder($subtotal)
    {
        $payments = $this->model_extension_extension->getExtensions('payment');
        $payments_minimum = array();
        $minimum = 0;
        $status = 0;
        $error = '';

        foreach ($payments as $payment) {
            $minimum = $this->config->get('checkout_' . $payment['code'] . '_min_order');
            $status = $this->config->get($payment['code'] . '_status');
            if ($minimum && $status) {
                $payments_minimum[$payment['code']] = $minimum;
            }
        }

        asort($payments_minimum);

        if ($payments_minimum) {
            foreach ($payments_minimum as $code => $minimum) {
                if (!$this->validateMinOrder($subtotal, $minimum)) {
                    $this->load->language('payment/' . $code);
                    $error = sprintf($this->language->get('error_min_order'), $this->currency->format($minimum));
                    break;
                }
            }
        }

        return $error;
    }
}
