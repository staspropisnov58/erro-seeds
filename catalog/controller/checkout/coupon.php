<?php
class ControllerCheckoutCoupon extends Controller {
	public function index() {
		if ($this->config->get('coupon_status')) {
			$this->load->language('checkout/coupon');

			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_coupon'] = $this->language->get('entry_coupon');

			$data['button_coupon'] = $this->language->get('button_coupon');

			$data['coupon_action'] = $this->url->link('checkout/coupon/coupon', '', true);

			$data['current_page'] = $this->request->server['REQUEST_URI'];

      if (isset($this->session->data['success_coupon'])) {
        $data['success_coupon'] = $this->session->data['success_coupon'];
        unset($this->session->data['success_coupon']);
      } else {
        $data['success_coupon'] = '';
      }

      if (isset($this->session->data['error_coupon'])) {
        $data['error_coupon'] = $this->session->data['error_coupon'];
        unset($this->session->data['error_coupon']);
      } else {
        $data['error_coupon'] = '';
      }

      if (isset($this->session->data['coupon'])) {
        $data['text_edit_coupon'] = $this->language->get('text_edit_coupon');
      } else {
        $data['text_edit_coupon'] = '';
      }

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/coupon.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/checkout/coupon.tpl', $data);
			} else {
				return $this->load->view('default/template/checkout/coupon.tpl', $data);
			}
		} else {
			return '';
		}
	}

	public function coupon() {
		$this->load->language('checkout/coupon');

		$json = array();

		$this->load->model('checkout/coupon');

		if (isset($this->request->post['coupon'])) {
			$coupon = $this->request->post['coupon'];
		} else {
			$coupon = '';
		}

		$coupon_info = $this->model_checkout_coupon->getCoupon($coupon);

		if (empty($this->request->post['coupon'])) {
			$this->session->data['error_coupon'] = $this->language->get('error_empty');
		} elseif ($coupon_info) {
			$this->session->data['coupon'] = $this->request->post['coupon'];

			$this->session->data['success_coupon'] = $this->language->get('text_success');
		} else {
			$this->session->data['error_coupon'] = $this->language->get('error_coupon');
		}

    if (isset($this->request->post['redirect'])) {
      $this->session->data['cart_is_open'] = 1;
      $this->response->redirect($this->request->post['redirect']);
    } else {
      $json['total'] = $this->cart->getTotal() ? $this->currency->format($this->cart->getTotal()) : $this->language->get('text_empty');
      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
	}

  public function remove_coupon() {
    $this->load->language('checkout/coupon');

    $json = array();

    unset($this->session->data['coupon']);
    $this->session->data['success_coupon'] = $this->language->get('text_removed');

    if (isset($this->request->post['redirect'])) {
      $this->session->data['cart_is_open'] = 1;
      $this->response->redirect($this->request->post['redirect']);
    } else {
      $json['total'] = $this->cart->getTotal() ? $this->currency->format($this->cart->getTotal()) : $this->language->get('text_empty');
      $json['cart_popup'] = $this->load->controller('checkout/cart');
      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
    }
  }
}
