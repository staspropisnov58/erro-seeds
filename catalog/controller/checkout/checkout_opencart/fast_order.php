<?php
class ControllerCheckoutFastOrder extends Controller
{
  public function index()
  {
    if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
      $telephone = $this->request->post['telephone'];
      $errors = $this->validate($telephone);
      $json = array();
      if (!empty($errors)) {
        $json['errors'] = $errors;
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
      } else {
        $this->addOrder($telephone);
        $json['redirect'] = $this->url->link('checkout/success', '', 'SSL');
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
      }
    }
  }

  private function addOrder($telephone) {
    // connect models array
    $models = array('extension/extension', 'account/customer', 'affiliate/affiliate', 'checkout/marketing', 'checkout/order');
    foreach ($models as $model) {
        $this->load->model($model);
    }

    $this->load->language('checkout/checkout');

    $form_data = (array)$this->config->get('ocdev_smart_one_page_checkout_form_data');

    //HACK: guest data in session emulation
    $this->session->data['guest'] = array(
      'customer_group_id' => $this->config->get('customer_group_id'),
      'firstname' => 'Unknown',
      'lastname'  => 'Jigglypuff',
    );

    $order_data = array();
    $order_data['totals'] = array();
    $total = 0;
    $taxes = $this->cart->getTaxes();
    $sort_order = array();
    $results = $this->model_extension_extension->getExtensions('total');

    foreach ($results as $key => $value) {
        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
    }

    array_multisort($sort_order, SORT_ASC, $results);

    foreach ($results as $result) {
        if ($this->config->get($result['code'] . '_status') && $result['code'] !== 'shipping') {
            $this->load->model('total/' . $result['code']);

            $this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
        }
    }

    $sort_order = array();

    foreach ($order_data['totals'] as $key => $value) {
        $sort_order[$key] = $value['sort_order'];
    }

    array_multisort($sort_order, SORT_ASC, $order_data['totals']);

    $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
    $order_data['store_id'] = $this->config->get('config_store_id');
    $order_data['store_name'] = $this->config->get('config_name');
    $order_data['store_url'] = ($order_data['store_id']) ? $this->config->get('config_url') : HTTP_SERVER;

    if ($this->customer->isLogged()) {
        $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

        $order_data['customer_id'] = $this->customer->getId();
        $order_data['customer_group_id'] = $customer_info['customer_group_id'];
        $order_data['firstname'] = $customer_info['firstname'];
        $order_data['lastname'] = $customer_info['lastname'];
        $order_data['email'] = $customer_info['email'];
        $order_data['telephone'] = $customer_info['telephone'];
        $order_data['fax'] = $customer_info['fax'];
        $order_data['custom_field'] = unserialize($customer_info['custom_field']);
    } else {
        $order_data['customer_id'] = 0;
        $order_data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
        $order_data['firstname'] = '';
        $order_data['lastname'] = '';
        $order_data['email'] = '';
        $order_data['telephone'] = $telephone;
        $order_data['fax'] = '';
        $order_data['custom_field'] = [];
    }

    $order_data['payment_firstname'] = '';
    $order_data['payment_lastname'] = '';
    $order_data['payment_company'] = '';
    $order_data['payment_address_1'] = '';
    $order_data['payment_address_2'] = '';
    $order_data['payment_city'] = '';
    $order_data['payment_postcode'] ='';
    $order_data['payment_zone'] = '';
    $order_data['payment_zone_id'] = 0;
    $order_data['payment_country'] = '';
    $order_data['payment_country_id'] = 0;
    $order_data['payment_address_format'] = '';
    $order_data['payment_custom_field'] = array();
    $order_data['payment_method'] = '';
    $order_data['payment_code'] = '';

    $order_data['shipping_firstname'] = '';
    $order_data['shipping_lastname'] = '';
    $order_data['shipping_company'] = '';
    $order_data['shipping_address_1'] = '';
    $order_data['shipping_address_2'] = '';
    $order_data['shipping_city'] = '';
    $order_data['shipping_postcode'] = '';
    $order_data['shipping_zone'] = '';
    $order_data['shipping_zone_id'] = '';
    $order_data['shipping_country'] = '';
    $order_data['shipping_country_id'] = '';
    $order_data['shipping_address_format'] = '';
    $order_data['shipping_custom_field'] = array();
    $order_data['shipping_method'] = '';
    $order_data['shipping_code'] = '';

    $order_data['products'] = array();

    foreach ($this->cart->getProducts() as $product) {
        $option_data = array();

        foreach ($product['option'] as $option) {
            $option_data[] = array(
                'product_option_id' => $option['product_option_id'],
                'product_option_value_id' => $option['product_option_value_id'],
                'option_id' => $option['option_id'],
                'option_value_id' => $option['option_value_id'],
                'name' => $option['name'],
                'value' => $option['value'],
                'type' => $option['type']
            );
        }

        $order_data['products'][] = array(
            'product_id' => $product['product_id'],
            'name' => $product['name'],
            'model' => $product['model'],
            'option' => $option_data,
            'download' => $product['download'],
            'quantity' => $product['quantity'],
            'subtract' => $product['subtract'],
            'price' => $product['price'],
            'total' => $product['total'],
            'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
            'reward' => $product['reward'],
            'child' => !empty($product['child']) ? $product['child'] : '',
            'parent_id' => !empty($product['parent_id']) ? $product['parent_id'] : '',
        );
    }

    // Gift Voucher
    $order_data['vouchers'] = array();

    if (!empty($this->session->data['vouchers'])) {
        foreach ($this->session->data['vouchers'] as $voucher) {
            $order_data['vouchers'][] = array(
                'description' => $voucher['description'],
                'code' => substr(md5(mt_rand()), 0, 10),
                'to_name' => $voucher['to_name'],
                'to_email' => $voucher['to_email'],
                'from_name' => $voucher['from_name'],
                'from_email' => $voucher['from_email'],
                'voucher_theme_id' => $voucher['voucher_theme_id'],
                'message' => $voucher['message'],
                'amount' => $voucher['amount']
            );
        }
    }

    $order_data['comment'] = isset($this->session->data['comment']) ? $this->session->data['comment'] : '';
    $order_data['total'] = $total;

    if (isset($this->request->cookie['tracking'])) {
        $order_data['tracking'] = $this->request->cookie['tracking'];

        $subtotal = $this->cart->getSubTotal();

        // affiliate
        $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

        if ($affiliate_info) {
            $order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
            $order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
        } else {
            $order_data['affiliate_id'] = 0;
            $order_data['commission'] = 0;
        }

        // marketing
        $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);
        $order_data['marketing_id'] = ($marketing_info) ? $marketing_info['marketing_id'] : 0;
    } else {
        $order_data['affiliate_id'] = 0;
        $order_data['commission'] = 0;
        $order_data['marketing_id'] = 0;
        $order_data['tracking'] = '';
    }

    $order_data['language_id'] = $this->config->get('config_language_id');
    $order_data['currency_id'] = $this->currency->getId();
    $order_data['currency_code'] = $this->currency->getCode();
    $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
    $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

    if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
        $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
    } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
        $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
    } else {
        $order_data['forwarded_ip'] = '';
    }

    $order_data['user_agent'] = (isset($this->request->server['HTTP_USER_AGENT'])) ? $this->request->server['HTTP_USER_AGENT'] : '';
    $order_data['accept_language'] = (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) ? $this->request->server['HTTP_ACCEPT_LANGUAGE'] : '';
    $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
    $data['payment'] = "";
    $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $form_data['order_status_id']);
  }

  private function validate($telephone) {
    $errors = array();
    $language_code = $this->session->data['language'];

    if (isset($this->request->request['customer_group_id'])) {
        $customer_group_id = $this->request->request['customer_group_id'];
    } else {
        $customer_group_id = ($this->customer->isLogged()) ? (int)$this->customer->getGroupId() : (int)$this->config->get('config_customer_group_id');
    }

    $field_data = (array)$this->config->get('ocdev_smart_one_page_checkout_field_data');

    foreach ($field_data as $key => $field) {
        if (!isset($field['customer_groups']) || !isset($field['address_blocks']) || ($field['activate'] == 0 || ($this->customer->isLogged() && ($field['view'] == 'email' || $field['view'] == 'fax' /*|| $field['view'] == 'telephone'*/)) || !in_array($customer_group_id, $field['customer_groups']))) {
            unset($field_data[$key]);
        }
        if ($field['view'] === 'telephone') {
          if (empty($telephone) && $field['check'] == 1) {
              $errors['telephone'] = $field['error_text'][$language_code];
          } elseif (!empty($field['check_rule']) && !preg_match($field['check_rule'], $telephone) && $field['check'] == 2) {
              $errors['telephone'] = $field['error_text'][$language_code];
          } elseif (utf8_strlen(str_replace(array('_', '-', '(', ')', '+'), "", $telephone)) < $field['check_min'] || utf8_strlen(str_replace(array('_', '-', '(', ')', '+'), "", $telephone)) > $field['check_max'] && $field['check'] == 3) {
              $errors['telephone'] = $field['error_text'][$language_code];
          } else {
              unset($errors['telephone']);
          }
        }
    }
    $cart_errors = $this->load->controller('checkout/ocdev_smart_one_page_checkout/validate', 'cart');
    if ($cart_errors) {
      $errors['cart'] = $cart_errors;
    }

    return $errors;
  }
}
