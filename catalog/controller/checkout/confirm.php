<?php
class ControllerCheckoutConfirm extends Controller {
	public function index($data = []) {
    $data = array_merge($data, $this->load->controller('checkout/cart/getCartContent'));

		$data['gift_programs'] = '';
		$data['messages'] = [];
		$data['current_step'] = '';
		unset($this->session->data['cart_is_open']);

		if ($this->config->get('gift_status')) {
			$this->load->model('checkout/gift');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/gift.tpl')) {
				$data['gift_programs'] = $this->load->view(
					                           $this->config->get('config_template') . '/template/checkout/gift.tpl',
																		 $this->model_checkout_gift->getGifts($data['subtotals']));
			} else {
				$data['gift_programs'] = '';
			}
		}

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/confirm.tpl')) {
      $data['content'] = $this->load->view($this->config->get('config_template') . '/template/checkout/confirm.tpl', $data);
    } else {
      $data['content'] = $this->load->view('default/template/checkout/confirm.tpl', $data);
    }

		$data['cart'] = '';

		$data['e_commerce'] = '';

		$all_steps = $this->load->controller('checkout/checkout/getSteps');

		$e_products = $this->load->controller('checkout/cart/getProductsForEcommerce');

		foreach ($all_steps as $number => $step) {
			$e_commerce_data = ['currency_code' => $this->currency->getCode(),
	                         'current_step_id' => $number + 1,
	                         'option' => 'Быстрый заказ',
	                       'products' => $e_products,
											 ];

			$data['e_commerce'] .= $this->load->view($this->config->get('config_template') . '/template/module/e_commerce_step.tpl', $e_commerce_data);
		}

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/index.tpl')) {
      $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/index.tpl', $data));
    }
	}

	public function saveOrder()
	{

	}
}
