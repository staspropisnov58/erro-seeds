<?php
class ControllerAccountNewsletter extends Controller {
	private $route = 'account/newsletter';
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/newsletter', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/newsletter');

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			$this->load->model('account/customer');

		if(isset($this->request->post['default'])){

			$this->model_account_customer->addDefaultSettingNotifications($this->customer->getId());

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('account/account', '', 'SSL'));

		}else{

			$sms = array(
				'sms_new_order' => $this->request->post['sms_new_order'],
				'sms_edit_order' => $this->request->post['sms_edit_order'],
				'sms_review_moderation' => $this->request->post['sms_review_moderation'],
				'sms_new_review_reply' => $this->request->post['sms_new_review_reply'],
				'sms_affiliate_order' => $this->request->post['sms_affiliate_order'],
				'sms_affiliate_bonus' => $this->request->post['sms_affiliate_bonus'],
				'sms_affiliate_payout' => $this->request->post['sms_affiliate_payout']
			);
			$this->model_account_customer->editSmsNotifications($sms, $this->customer->getId());

			$email = array(
				'email_new_order' => $this->request->post['email_new_order'],
				'email_edit_order' => $this->request->post['email_edit_order'],
				'email_review_moderation' => $this->request->post['email_review_moderation'],
				'email_new_review_reply' => $this->request->post['email_new_review_reply'],
				'email_affiliate_order' => $this->request->post['email_affiliate_order'],
				'email_affiliate_bonus' => $this->request->post['email_affiliate_bonus'],
				'email_affiliate_payout' => $this->request->post['email_affiliate_payout']
			);
			$this->model_account_customer->editEmailNotifications($email, $this->customer->getId());

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('account/account', '', 'SSL'));

		}
	}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_newsletter')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');

		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_affiliate_order'] = $this->language->get('entry_affiliate_order');
		$data['entry_affiliate_bonus'] = $this->language->get('entry_affiliate_bonus');
		$data['entry_affiliate_payout'] = $this->language->get('entry_affiliate_payout');
		$data['text_notification_settings'] = $this->language->get('text_notification_settings');
		$data['text_notification_title'] = $this->language->get('text_notification_title');
		$data['text_notification_sms'] = $this->language->get('text_notification_sms');
		$data['text_notification_email'] = $this->language->get('text_notification_email');
		$data['text_for_default'] = $this->language->get('text_for_default');




		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');


		$data['action'] = $this->url->link('account/newsletter', '', 'SSL');

		$data['newsletter'] = $this->customer->getNewsletter();

		$data['back'] = $this->url->link('account/account', '', 'SSL');

		$data['affiliate_id'] = $this->customer->getAffiliateId();

		$customer_id = $this->customer->getId();

	 	$this->load->model('account/customer');

	  $email_results = $this->model_account_customer->getEmailNotifications($customer_id);

		$sms_results = $this->model_account_customer->getSmsNotifications($customer_id);

		foreach($sms_results as $sms_result){
			foreach($sms_result as $key=>$value){
				if($data['affiliate_id'] != 0 || !stristr($key,'affiliate')){
					$notifications_sms[$key] = $value;
				}
			}
		}
		if(!empty($this->config->get('config_sms'))){
			$data['sms_customer_notification'] = $this->config->get('config_sms');
	}

		foreach($email_results as $email_result){
			foreach($email_result as $key=>$value){
				if($data['affiliate_id'] != 0 || !stristr($key,'affiliate')){
					$notifications[$key] = array(
							'entry' => $this->language->get( 'entry_'. $key),
							'email_value' => $value,
							'sms_value'	=> 	$notifications_sms[$key],
							'name'  => $key
					);
				}
			}
		}

		$data['notifications'] = $notifications;

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/newsletter.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/newsletter.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/newsletter.tpl', $data));
		}
	}
}
