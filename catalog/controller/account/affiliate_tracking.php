<?php
class ControllerAccountAffiliateTracking extends Controller {
  private $route = 'account/affiliate_tracking';

  public function index() {
    if (!$this->customer->isLogged()) {
      $this->session->data['redirect'] = $this->url->link('account/newsletter', '', 'SSL');

      $this->response->redirect($this->url->link('account/login', '', 'SSL'));
    }

    if (!$this->config->get('customer_is_affiliate_status')) {
      $this->response->redirect($this->url->link('account/account', '', 'SSL'));
    }

    $this->load->language('affiliate/tracking');

    $this->document->setTitle($this->language->get('heading_title'));

    if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_account'),
      'href' => $this->url->link('account/account', '', 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title')
    );

    $data['affiliate_id'] = $this->customer->getAffiliateId();

    if (!empty($data['affiliate_id'])){
      $data['code'] = $this->customer->getAffiliateCode();

      $this->load->model('affiliate/affiliate');
      $affiliate = $this->model_affiliate_affiliate->getAffiliate($this->customer->getAffiliateId());
      $data['commission'] = $affiliate['commission'];
    }



    if (isset($this->request->post['affiliate'])){
      $this->load->model('account/customer');
      $this->load->model('module/customer_is_affiliate');

      $customer_id = $this->customer->getId();

      $customer = $this->model_account_customer->getCustomer($customer_id);

      $email = $customer['email'];

      $affiliate_id = $this->model_module_customer_is_affiliate->addAffiliate($customer_id);

      $this->model_account_customer->sendCustomerEmail('affiliate');

      $telephone = '';
      $firstname =  $customer['firstname'];

      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('account/affiliate_tracking'));

    }

    if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

    $this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/account.js');

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_description'] = sprintf($this->language->get('text_description'), $this->config->get('config_name'));

    $data['entry_code'] = $this->language->get('entry_code');
    $data['entry_generator'] = $this->language->get('entry_generator');
    $data['entry_link'] = $this->language->get('entry_link');
    $data['text_new_affiliate_button'] = $this->language->get('text_new_affiliate_button');
    $data['text_new_my_affiliate'] = $this->language->get('text_new_my_affiliate');
    $data['help_generator'] = $this->language->get('help_generator');
    $data['button_continue'] = $this->language->get('button_continue');
    $data['text_affiliate_comission'] = $this->language->get('text_affiliate_comission');



    $data['continue'] = $this->url->link('account/account', '', 'SSL');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['column_right'] = $this->load->controller('common/column_right');
    $data['content_top'] = $this->load->controller('common/content_top');
    $data['content_bottom'] = $this->load->controller('common/content_bottom');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');


    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/affiliate_tracking.tpl')) {
      $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/affiliate_tracking.tpl', $data));
    } else {
      $this->response->setOutput($this->load->view('default/template/account/affiliate_tracking.tpl', $data));
    }
  }

  public function autocomplete() {
    $json = array();

    if (isset($this->request->get['filter_name'])) {
      $this->load->model('catalog/product');

      $filter_data = array(
        'filter_name' => $this->request->get['filter_name'],
        'start'       => 0,
        'limit'       => 5,
        'path'        => 'product_search',
        'sort'        => 'default',
      );

      $results = $this->model_catalog_product->getProducts($filter_data);

      foreach ($results as $result) {
        $json[] = array(
          'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
          'link' => str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $result['product_id'] . '&tracking=' . $this->customer->getAffiliateCode()))
        );

      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}
