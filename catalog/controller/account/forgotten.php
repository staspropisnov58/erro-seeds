<?php
class ControllerAccountForgotten extends Controller {
	private $error = array();

	private $route = 'account/forgotten';


	public function index() {
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		}

		$this->load->language('account/forgotten');

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setDescription($this->language->get('meta_description'));

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->language('mail/forgotten');

			$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

			// Add to activity log

			if ($customer_info) {
				$code = bin2hex(openssl_random_pseudo_bytes(16));

				$this->model_account_customer->editCode($this->request->post['email'], $code);

				$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$telephones = explode(';', $this->config->get('config_telephone'));
				$text_main  = sprintf($this->language->get('text_main'), $this->config->get('config_url'),  html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $this->url->link('account/reset', 'code=' . $code, true), $this->request->server['REMOTE_ADDR']);

				$data['store_url']      = $this->config->get('config_url');
				$data['text_greeting']  = sprintf($this->language->get('text_greeting'), html_entity_decode($customer_info['firstname'] . ' ' . $customer_info['lastname'], ENT_QUOTES, 'UTF-8')) . "\n\n";
				$data['text_main'] = explode('</br>', $text_main);
				$data['text_note']  = sprintf($this->language->get('text_note'), $this->url->link('account/forgotten', '', 'SSL'));

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/message.tpl')) {
					$html = $this->load->view($this->config->get('config_template') . '/template/mail/message.tpl', $data);
				} else {
					$html = $this->load->view('default/template/mail/order.tpl', $data);
				}

				$mail_config = $this->config->get('config_mail');
				$mail = new Mail($mail_config);
				$mail->setTo($this->request->post['email']);
				if ($mail_config['smtp_username']) {
					$mail->setFrom($mail_config['smtp_username']);
					$mail->setReplyTo($this->config->get('config_email'));
				} else {
					$mail->setFrom($this->config->get('config_email'));
				}
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject($subject);
				$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
				$mail->send();

				$this->session->data['success'] = $this->language->get('text_success');

				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $customer_info['customer_id'],
					'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
				);

				$this->model_account_activity->addActivity('forgotten', $activity_data);
			}

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten')
		);

		$data['heading_title'] = $this->language->get('text_forgotten');

		$data['text_your_email'] = $this->language->get('text_your_email');
		$data['text_email'] = $this->language->get('text_email');

		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten', '', 'SSL');

		$data['back'] = $this->url->link('account/login', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/forgotten.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/forgotten.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/forgotten.tpl', $data));
		}
	}

	protected function validate() {
		if (!isset($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail(trim($this->request->post['email']))) {
			$this->error['warning'] = $this->language->get('error_email');
		}

		return !$this->error;
	}
}
