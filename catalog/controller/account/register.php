<?php
class ControllerAccountRegister extends Controller {
	use Autocorrect;
	private $error = array();

	private $route = 'account/register';


	public function index() {
		if ($this->config->get('socnetauth2_status')) {
			$data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array("buttons_only"=>1));
		}

		$errors = array();

		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		}

		$this->load->language('account/register');

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setDescription($this->language->get('meta_description'));

		$this->document->addScript('https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit');

		$this->load->model('account/customer');
		$this->load->model('module/customer_is_affiliate');
		$this->load->model('module/validator');
		$this->load->model('module/register_setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$reCaptcha = new ReCaptcha(GOOGLE_SECRET);
			if ($this->request->post['g-recaptcha-response'] && $reCaptcha->verifyResponse($this->request->server['REMOTE_ADDR'], $this->request->post['g-recaptcha-response'])) {
				unset($this->request->post['g-recaptcha-response']);
					$this->correctValuesOf($this->request->post);
					$errors = $this->model_module_validator->validate($this->request->post, ['email' => 'customer']);
					if (!$errors) {
						$this->request->post['referred_by'] = $this->model_account_customer->getAffiliateIdByLogin($this->request->post['referred_by']);
						$customer_id = $this->model_account_customer->addCustomer($this->request->post);

						if(isset($this->request->post['affiliate'])){
							$message = 'customer&affiliate';
						}else{
							$message = 'customer';
						}

						$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

						$this->customer->login($this->request->post['email'], $this->request->post['password']);

						unset($this->session->data['guest']);

						// Add to activity log
						$this->load->model('account/activity');


						$activity_data = array(
							'customer_id' => $customer_id,
						);
						$this->model_account_activity->addActivity('register', $activity_data);


						$this->model_account_customer->sendCustomerEmail($message);

						if($this->request->post['newsletter']){

			 				$required_data = array(
			 					'route' => $this->request->get['route'],
			 					'email' => $this->request->post['email']
			 				);

			 				$this->load->model('module/send_mail');
			 				$this->model_module_send_mail->SendAdminEmail($required_data);
			 			}


						$this->response->redirect($this->url->link('account/success'));

						$data['success'] = $this->language->get('text_success');
					}
				} else {
					$data['error_captcha'] = $this->language->get('error_captcha');
				}

			}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_register')
		);

		$data['heading_title'] = $this->language->get('text_register');

		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_your_password'] = $this->language->get('text_your_password');
		$data['entry_review_moderation'] = $this->language->get('entry_review_moderation');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_error_login'] = $this->language->get('text_error_login');
		$data['text_agree'] = $this->language->get('text_agree');
		$data['text_warranty'] = $this->language->get('text_warranty');

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_login'] = $this->language->get('entry_login');
		$data['entry_referred_by'] = $this->language->get('entry_referred_by');
		$data['text_referred_by'] = $this->language->get('text_referred_by');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
		$data['text_affiliate'] = $this->language->get('text_affiliate');

		$data['all_fields_are_required'] = $this->language->get('all_fields_are_required');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		$data['button_register_acc'] = $this->language->get('button_register_acc');

		$data['enebled_tel_code'] = $this->config->get('enebled_tel_code');

		if ($this->config->get('show_register_setting') == 1) {
      $fields = $this->model_module_register_setting->getCustomerRegisterFields();
    } else {
      $fields = array();
    }

    if ($fields) {
			$form = new Form('register', $this->url->link('account/register', '', 'SSL'));
      foreach ($fields as $field) {
				$field['value'] = null;
				$form->addField($field);
      }

			if ($this->config->get('enebled_tel_code')) {
				$this->load->model('localisation/country');
				$data['countries'] = $this->model_localisation_country->getCountries();
				$data['country_id'] = $this->config->get('config_country_id');

				$form->addField(['name' => 'telephone_country_id', 'label' => '', 'placeholder' => '', 'sort_order' => 1, 'value' => $data['country_id'], 'type' => 'select']);

				$form->setFieldsValues($this->request->post);

				$form->groupFields('telephone', 'telephone_country_id', 'telephone');
			} else {
				$form->setFieldsValues($this->request->post);
			}

			$form->setFieldsErrors($errors);
			$data['form'] = $form->build();
    }

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['g_recaptcha_response'])) {
				$data['error_g_recaptcha_response'] = $this->error['g_recaptcha_response'];
		} else {
				$data['error_g_recaptcha_response'] = '';
		}

		if (isset($this->error['g_recaptcha_response-from-server'])) {
				$data['error_g_recaptcha_response_from_server'] = $this->error['g_recaptcha_response-from-server'];
		} else {
				$data['error_g_recaptcha_response_from_server'] = '';
		}

		// $data['action'] = $this->url->link('account/register', '', 'SSL');

		$data['customer_groups'] = array();

		if (is_array($this->config->get('config_customer_group_display'))) {
			$this->load->model('account/customer_group');

			$customer_groups = $this->model_account_customer_group->getCustomerGroups();

			foreach ($customer_groups as $customer_group) {
				if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
					$data['customer_groups'][] = $customer_group;
				}
			}
		}

		if (isset($this->request->post['customer_group_id'])) {
			$data['customer_group_id'] = $this->request->post['customer_group_id'];
		} else {
			$data['customer_group_id'] = $this->config->get('config_customer_group_id');
		}

		// Custom Fields
		$this->load->model('account/custom_field');

		$data['custom_fields'] = $this->model_account_custom_field->getCustomFields();

		if (isset($this->request->post['custom_field'])) {
			if (isset($this->request->post['custom_field']['account'])) {
				$account_custom_field = $this->request->post['custom_field']['account'];
			} else {
				$account_custom_field = array();
			}
			$data['register_custom_field'] = $account_custom_field + $address_custom_field;
		} else {
			$data['register_custom_field'] = array();
		}


		if (isset($this->request->post['newsletter'])) {
			$data['newsletter'] = $this->request->post['newsletter'];
		} else {
			$data['newsletter'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}

		//set checkbox for add affiliate

		if (isset($this->request->post['affiliate'])){
			$data['affiliate'] = $this->request->post['affiliate'];
		}else{
			$data['affiliate'] = '';
		}

		$data['tuning'] = $this->config->get('customer_is_affiliate_status');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/register.tpl')) {

			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/register.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/register.tpl', $data));
		}
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
