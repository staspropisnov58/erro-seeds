<?php
class ControllerAccountEdit extends Controller {
	private $error = array();

	private $route = 'account/edit';

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/edit', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$errors = array();

		$this->load->language('account/edit');

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->load->model('module/validator');

			$errors = $this->model_module_validator->validate($this->request->post, ['email' => 'customer']);
			if (!$errors) {
				$this->model_account_customer->editCustomer($this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');

				// Add to activity log
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('edit', $activity_data);

				$this->response->redirect($this->url->link('account/account', '', 'SSL'));
			}

		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_edit')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_additional'] = $this->language->get('text_additional');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_patronymic'] = $this->language->get('entry_patronymic');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_upload'] = $this->language->get('button_upload');

		$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

		$this->load->model('module/register_setting');

		if ($this->config->get('show_register_setting') == 1) {
			$fields = $this->model_module_register_setting->getCustomerEditFields();
		} else {
			$fields = array();
		}

		if ($fields) {
			$form = new Form('edit', $this->url->link('account/edit', '', 'SSL'));
      foreach ($fields as $field) {
				$field['value'] = null;
				$form->addField($field);
      }

			if ($this->config->get('enebled_tel_code')) {
				$this->load->model('localisation/country');
				$data['countries'] = $this->model_localisation_country->getCountries();
				$data['country_id'] = $this->config->get('config_country_id');

				$form->addField(['name' => 'telephone_country_id', 'label' => '', 'placeholder' => '', 'sort_order' => 1, 'value' => $data['country_id'], 'type' => 'select']);

				$form->setFieldsValues($customer_info);
				$form->setFieldsValues($this->request->post);

				$form->groupFields('telephone', 'telephone_country_id', 'telephone');
			} else {
				$form->setFieldsValues($customer_info);
				$form->setFieldsValues($this->request->post);
			}

			$form->setFieldsErrors($errors);
			$data['form'] = $form->build();
    }

		// Custom Fields
		$this->load->model('account/custom_field');

		$data['custom_fields'] = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

		if (isset($this->request->post['custom_field'])) {
			$data['account_custom_field'] = $this->request->post['custom_field'];
		} elseif (isset($customer_info)) {
			$data['account_custom_field'] = unserialize($customer_info['custom_field']);
		} else {
			$data['account_custom_field'] = array();
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} elseif (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}

		$data['back'] = $this->url->link('account/account', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/edit.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/edit.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/edit.tpl', $data));
		}
	}
}
