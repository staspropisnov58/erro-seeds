<?php
class ControllerAccountUnclosedOrders extends Controller {

  private $route = 'account/unclosed_orders';


  public function index() {

    if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/unclosed_orders', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

    if (!$this->config->get('customer_is_affiliate_status')) {
      $this->response->redirect($this->url->link('account/account', '', 'SSL'));
    }

    $this->load->language('affiliate/unclosed_order');

    if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

    $this->document->setTitle($this->language->get('heading_title'));

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_account'),
      'href' => $this->url->link('account/account', '', 'SSL')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_transaction')
    );

    //потом поменять
    $this->load->model('affiliate/transaction');

    $data['heading_title'] = $this->language->get('heading_title');

    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_description'] = $this->language->get('column_description');
    $data['column_amount'] = sprintf($this->language->get('column_amount'), $this->config->get('config_currency'));
    //
     $data['text_balance'] = $this->language->get('text_balance');



    $data['button_continue'] = $this->language->get('button_continue');

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $customer_id = $this->customer->getId();

    $data['affiliate_id'] = $this->customer->getAffiliateId();

    $data['transactions'] = array();

    $order_status_id = implode(',', $this->config->get('config_complete_status'));

    $quantities = $this->model_affiliate_transaction->countAffiliateOrders($this->customer->getAffiliateId(), $order_status_id);

    foreach ($quantities  as $quantity) {
      $data['quantity']= implode($quantity);
    }
    $data['continue'] = $this->url->link('account/account', '', 'SSL');

    $data['column_left'] = $this->load->controller('common/column_left');
    $data['column_right'] = $this->load->controller('common/column_right');
    $data['content_top'] = $this->load->controller('common/content_top');
    $data['content_bottom'] = $this->load->controller('common/content_bottom');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/unclosed_orders.tpl')) {
      $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/unclosed_orders.tpl', $data));
    } else {
      $this->response->setOutput($this->load->view('default/template/account/unclosed_orders.tpl', $data));
    }
  }
}
