<?php
class ControllerAccountVip extends Controller
{
    private $route = 'account/vip';
    private $error = array();

    public function index()
    {
        $this->load->language('account/vip');

        if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
            $this->load->model('tool/opengraph');
            $this->model_tool_opengraph->addOpengraphForPage($this->route);
        }
        $this->document->setTitle($this->language->get('heading_title'));

        $data['button_checkout'] = $this->language->get('button_checkout');
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_name'] = $this->language->get('text_name');
        $data['text_tel'] = $this->language->get('text_tel');
        $data['text_mes'] = $this->language->get('text_mes');

        $data['firstname'] = $this->customer->getFirstName();
        $data['email'] = $this->customer->getEmail();

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_vip')
        );

        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getCountries();
        $data['country_id'] = $this->config->get('config_country_id');

        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/vip', '', 'SSL');

            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/vip.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/vip.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/vip.tpl', $data));
        }
    }
    public function addMessenger(){
        $json = array();
        $this->load->model('localisation/country');

        $json['test'] =$this->request->post;
        $json['test2'] = $this->model_localisation_country->getCountryTelephoneCode($this->request->post['tel_phone']);

        if ((utf8_strlen(trim($this->request->post['name'])) < 2) || (utf8_strlen(trim($this->request->post['name'])) > 32)) {
            $json['error']['name'] = $this->language->get('error_name');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $json['error']['email'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 8) || (utf8_strlen($this->request->post['telephone']) > 21) ) {
            $json['error']['telephone'] = $this->language->get('error_telephone');
        }

        if (!isset($json['error'])) {
//        $mail = new Mail($this->config->get('config_mail_engine'));
//        $mail->parameter = $this->config->get('config_mail_parameter');
//        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
//        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
//        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
//        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
//        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
//
//        $mail->setTo($order_info['email']);
//        $mail->setFrom($from);
//        $mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
//        $mail->setSubject(html_entity_decode(sprintf($language->get('text_subject'), $order_info['store_name'], $order_info['order_id']), ENT_QUOTES, 'UTF-8'));
//        $mail->setHtml($this->load->view('mail/order_add', $data));
//        $mail->send();

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}