<?php
class ControllerAccountPayouts extends Controller {
  private $route = 'account/payouts';

  private $limit = 10;
  	public function index() {

      if (!$this->customer->isLogged()) {
  			$this->session->data['redirect'] = $this->url->link('account/payouts', '', 'SSL');
  			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
  		}

      if (!$this->config->get('customer_is_affiliate_status')) {
        $this->response->redirect($this->url->link('account/account', '', 'SSL'));
      }

      //потом поменять

      if (isset($this->request->get['page'])) {
  			$page = $this->request->get['page'];
  		} else {
  			$page = 1;
  		}

    if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

  		$this->load->language('affiliate/payout');

      $data['column_amount'] = $this->language->get('column_amount');
      $data['column_date_added'] = $this->language->get('column_date_added');
      $data['column_description'] = $this->language->get('column_description');
      $data['text_empty'] = $this->language->get('text_empty');

      $data['heading_title'] = $this->language->get('heading_title');
      $data['text_balance'] = $this->language->get('text_balance');

  		$this->document->setTitle($this->language->get('heading_title'));
      $data['button_continue'] = $this->language->get('button_continue');

  		$data['breadcrumbs'] = array();

  		$data['breadcrumbs'][] = array(
  			'text' => $this->language->get('text_home'),
  			'href' => $this->url->link('common/home')
  		);

  		$data['breadcrumbs'][] = array(
  			'text' => $this->language->get('text_account'),
  			'href' => $this->url->link('account/account', '', 'SSL')
  		);

  		$data['breadcrumbs'][] = array(
  			'text' => $this->language->get('text_payout')
  		);

      $this->load->model('affiliate/transaction');

      $affiliate_id = $this->customer->getAffiliateId();

      $limit = $this->limit;


      $filter_data = array(
				'affiliate_id' => $affiliate_id,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);


      $results = $this->model_affiliate_transaction->getPayouts($filter_data);


      $data['payouts'] = array();

      foreach ($results as $result){
        $data['payouts'][] = array(
          'date_payout' => date($this->language->get('date_format_short'), strtotime($result['date_payout'])),
          'description' => $result['description'],
          'amount'      =>  $this->currency->format($result['amount'])
        );
      }
      $data['balance'] = $this->currency->format($this->model_affiliate_transaction->getBalance($affiliate_id));


    $total_payouts = $this->model_affiliate_transaction->getPayoutsTotal($affiliate_id);
    foreach ($total_payouts as $total_payout) {
      $total_payout = $total_payout;
    }
    $payout_total = implode($total_payout);



  		$pagination = new Pagination();
  		$pagination->total = $payout_total;
  		$pagination->page = $page;
  		$pagination->limit = $limit;
  		$pagination->url = $this->url->link('account/payouts', 'page={page}', 'SSL');

  		$data['pagination'] = $pagination->render();

  		$data['results'] = sprintf($this->language->get('text_pagination'), ($payout_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($payout_total - 10)) ? $payout_total : ((($page - 1) * 10) + 10), $payout_total, ceil($payout_total / 10));

//  		$data['balance'] = $this->currency->format($this->model_affiliate_transaction->getBalance());

  		$data['continue'] = $this->url->link('account/account', '', 'SSL');

  		$data['column_left'] = $this->load->controller('common/column_left');
  		$data['column_right'] = $this->load->controller('common/column_right');
  		$data['content_top'] = $this->load->controller('common/content_top');
  		$data['content_bottom'] = $this->load->controller('common/content_bottom');
  		$data['footer'] = $this->load->controller('common/footer');
  		$data['header'] = $this->load->controller('common/header');

  		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/payouts.tpl')) {
  			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/payouts.tpl', $data));
  		} else {
  			$this->response->setOutput($this->load->view('default/template/account/payouts.tpl', $data));
  		}
  	}
  }
