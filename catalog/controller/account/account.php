<?php
class ControllerAccountAccount extends Controller {
	private $route = 'account/account';

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->model('account/customer');

		$this->load->language('account/account');

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$customer_id = $this->customer->getId();

		$data['affiliate_id'] = $this->customer->getAffiliateId();

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['$text_sms_newsletter'] = $this->language->get('$text_sms_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_affiliate_information'] = $this->language->get('text_affiliate_information');
		$data['text_new_affiliate_button'] = $this->language->get('text_new_affiliate_button');
		$data['text_new_my_affiliate'] = $this->language->get('text_new_my_affiliate');
		$data['text_bonuses_only_affiliate'] = $this->language->get('text_bonuses_only_affiliate');
		$data['text_payouts_affiliate'] = $this->language->get('text_payouts_affiliate');
		$data['text_payout'] = $this->language->get('text_payout');
		$data['text_unclosed_orders'] = $this->language->get('text_unclosed_orders');


		$data['edit'] = $this->url->link('account/edit', '', 'SSL');
		$data['password'] = $this->url->link('account/password', '', 'SSL');
		$data['address'] = $this->url->link('account/address', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['return'] = $this->url->link('account/return', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['sms_newsletter'] = $this->url->link('account/sms_newsletter');
		$data['recurring'] = $this->url->link('account/recurring', '', 'SSL');
		$data['information'] = $this->url->link('account/affiliate_tracking', '', 'SSL');
		$data['bonuses'] = $this->url->link('account/affiliate_transaction', '', 'SSL');
		$data['payouts'] = $this->url->link('account/payouts', '','SSL');
		$data['unclosed_orders'] = $this->url->link('account/unclosed_orders', '','SSL');



		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', 'SSL');
		} else {
			$data['reward'] = '';
		}

		$data['tuning'] = $this->config->get('customer_is_affiliate_status');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/account.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
		}
	}


	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
