<?php
class ControllerApiProduct extends Controller
{
  public function products()
  {
    $this->load->language('api/product');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
      $this->load->model('catalog/product');

      $filter_data = $this->request->post ? $this->request->post : [];
      $filter_data['path'] = 'product_api';

      $products = $this->model_catalog_product->getProducts($filter_data);

      foreach ($products as $product) {
        if ($product['quantity'] <= 0) {
          continue;
        }
        $json['products'][] = $this->getProductData($product);
      }

      $this->response->addHeader('Content-Type: application/json');
  		$this->response->setOutput(json_encode($json));
    }
  }

  public function product()
  {
    $this->load->language('api/product');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
      $this->load->model('catalog/product');

      if (isset($this->request->post['product_id'])) {
        $product = $this->model_catalog_product->getProduct($this->request->post['product_id']);
        if ($product) {
          $json['product'] = $this->getProductData($product);
        } else {
          $json['error'] = $this->language->get('error_not_found');
        }
      } else {
        $json['error'] = $this->language->get('error_not_found');
      }

      $this->response->addHeader('Content-Type: application/json');
  		$this->response->setOutput(json_encode($json));
    }
  }

  public function category()
  {
    $this->load->language('api/product');

    $json = array();

    if (!isset($this->session->data['api_id'])) {
      $json['error'] = $this->language->get('error_permission');
    } else {
      if (isset($this->request->post['category_id'])) {
        $this->load->model('catalog/category');
        $category = $this->model_catalog_category->getCategory($this->request->post['category_id']);
        if ($category) {
          $category['children'] = $this->getChildren($category['category_id']);

          $this->load->model('catalog/product');
          $category['total_products'] = $this->model_catalog_product->getTotalProducts(['filter_category_id' => $category['category_id']]);

          $json['category'] = $category;
        } else {
          $json['error'] = $this->language->get('error_category_not_found');
        }
      } else {
        $json['error'] = $this->language->get('error_category_not_found');
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function categories()
  {
    $this->load->language('api/product');

    $json = array();

    if (!isset($this->session->data['api_id'])) {
      $json['error'] = $this->language->get('error_permission');
    } else {
      $parent_id = isset($this->request->post['parent_id']) ? $this->request->post['parent_id'] : 0;
      $json['categories'] = $this->getChildren($parent_id);
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function manufacturers()
  {
    $this->load->language('api/product');

    $json = array();

    if (!isset($this->session->data['api_id'])) {
      $json['error'] = $this->language->get('error_permission');
    } else {
      $this->load->model('catalog/manufacturer');
      $json['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  protected function getChildren($parent_id)
  {
    $this->load->model('catalog/category');
    $categories = $this->model_catalog_category->getCategories($parent_id);

    if ($categories) {
      foreach ($categories as &$category) {
        $category['children'] = $this->getChildren($category['category_id']);

        $this->load->model('catalog/product');
        $category['total_products'] = $this->model_catalog_product->getTotalProducts(['filter_category_id' => $category['category_id']]);
      }
    }

    return $categories;
  }

  protected function getProductData($product)
  {
    if ($product['image']) {
      $image = HTTPS_SERVER . 'image/' . $product['image'];
    } else {
      $image = HTTPS_SERVER . 'image/placeholder.png';
    }

    $price = new Price($product['price']);

    //TODO этот костыль можно будет убрать после обновления с бонусами
    $special_info = ['price' => $product['special'], 'product_special_id' => $product['product_special_id']];
    $price->setSpecial($special_info);

    $discounts = $this->model_catalog_product->getProductDiscounts($product['product_id']);

    $price->setDiscount($product_info['discount']);

    $this->load->model('catalog/option');
    $option_data = $this->model_catalog_option->getOptionsWithPreparedPrices($price, $product['product_id']);

    $first_option_prices = $this->model_catalog_option->getFirstAvailableOptionsPrices($price, $product['product_id']);
    if ($first_option_prices) {
      foreach ($first_option_prices as $option_price) {
        $price->addOptionPrice($option_price);
      }
    }


    $attridutes_data = $this->model_catalog_product->getProductAttributes($product['product_id']);
    $categories = $this->model_catalog_product->getCategories($product['product_id']);

    $product = array(
      'product_id'      => $product['product_id'],
      'thumb'           => $image,
      'name'            => $product['name'],
      'model'           => $product['model'],
      'manufacturer_id' => $product['manufacturer_id'],
      'manufacturer'    => $product['manufacturer'],
      'description'     => $product['description'],
      'options'         => $option_data,
      'attributes'      => $attridutes_data,
      'categories'      => array_column($categories, 'category_id'),
      'quantity'        => $product['quantity'],
      'price'           => $this->currency->format($this->tax->calculate($price->getPrice(), $product['tax_class_id'], $this->config->get('config_tax')), '', '', false),
      'old_price'       => $this->currency->format($this->tax->calculate($price->getOldPrice(), $product['tax_class_id'], $this->config->get('config_tax')), '', '', false),
      'href'            => $this->url->link('product/product', 'product_id=' . $product['product_id']),
    );

    return $product;
  }
}
