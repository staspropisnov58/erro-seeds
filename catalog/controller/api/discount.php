<?php
class ControllerApiDiscount extends Controller
{
  public function add()
  {
    $this->load->language('api/discount');

		// Delete past coupon in case there is an error
		unset($this->session->data['discount']);

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {

			if (isset($this->request->post['discount'])) {
				$discount = (float) str_replace(',', '.', $this->request->post['discount']);
			} else {
				$discount = 0;
			}


			if ($discount) {
				$this->session->data['discount'] = $discount;

				$json['success'] = $this->language->get('text_success');
			} else {
				$json['error'] = $this->language->get('error_discount');
			}
		}

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}
