<?php
class ControllerApiMail extends Controller {

  public function backToStock() {
		$this->load->language('mail/request');

    $data['text_admission'] = $this->language->get('text_admission');
    $data['text_manufacturer'] = $this->language->get('text_manufacturer');
    $data['text_related'] = $this->language->get('text_related');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {

      $email = $this->request->get['email'];

      $this->load->model('module/notification_request');

      $this->load->model('tool/image');

      $requests_id = $this->request->post['requests_id'][$this->request->post['email']];

      $results = $this->model_module_notification_request->getProductsByNotificationsId(implode(",", $requests_id));

      foreach ($results as &$result){
        if ($result['image']) {
          $result['image'] = $this->model_tool_image->resize($result['image'], 168, 168);
        } else {
          $result['image'] = $this->model_tool_image->resize('placeholder.png',168, 168);
        }

        $result['href'] = $this->url->link('product/product', '&product_id=' . $result['product_id']);
        $result['price'] = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
        if((int)$result['product_special_id'] !== 0){
          $result['special'] = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
        }


        foreach ($result['related_products'] as &$related_products) {

          if ($related_products['image']) {
            $related_products['image'] = $this->model_tool_image->resize($related_products['image'], 137, 137);
          } else {
            $related_products['image'] = $this->model_tool_image->resize('placeholder.png', 137, 137);
          }

          $related_products['href'] = $this->url->link('product/product', '&product_id=' . $related_products['product_id']);

          $related_products['price'] = $this->currency->format($this->tax->calculate($related_products['price'], $result['tax_class_id'], $this->config->get('config_tax')));
          if((int)$related_products['product_special_id'] !== 0){
            $related_products['special'] = $this->currency->format($this->tax->calculate($related_products['special'], $result['tax_class_id'], $this->config->get('config_tax')));
          }

          $array_releted_products[] = $related_products;

        }
      }


      $data['related_products'] = $array_releted_products;

      $data['products'] = $results;

      if (file_exists(DIR_TEMPLATE .  $this->config->get('config_template') .'/template/mail/admission_notify.tpl')) {
  			$html = $this->load->view($this->config->get('config_template') .'/template/mail/admission_notify.tpl', $data);
  		} else {
  			$html = $this->load->view('default/template/mail/order.tpl', $data);
  		}

      $subject = sprintf($this->language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));


      // $json['error']['email'] = $this->langauage->get('error_email');

  		if ($email !== ''){
  			$mail = new Mail($this->config->get('config_mail'));
  			$mail->setTo($email);
  			$mail->setFrom($this->config->get('config_email'));

        if ($mail->verifyEmailTo()) {
          $mail->setSender('Errors Seeds');
          $mail->setSubject($subject);
          $mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
          $arr = $mail->send();
          $json['success'] = $this->language->get('text_success');
        } else {
          $json['error'] = 'Email not exists';
        }
  		} else {
        $json['error'] = 'Email is empty';
      }
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

  public function getContent(){

    $json = $this->load->controller('api/email/' . $this->request->post['template_name'] . '/getContent', $this->request->post);
    $json = json_encode($json);

    $this->response->setOutput($json);

  }

  public function sendMail(){

    $this->load->model('module/validator');

    $errors = $this->model_module_validator->validate($this->request->post, ['email' => 'contact']);
    if ($errors){
      $json['errors'] = $errors;


    }else{


      $json = $this->load->controller('api/email/' . $this->request->post['template_name'] . '/getContent', $this->request->post);

      if(isset($json['html']) && $json['html'] !== NULL){
        $mail = new Mail($this->config->get('config_mail'));
        $mail->setTo($this->request->post['email']);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender('Errors Seeds');
        $mail->setSubject($json['text_subject']);
        $mail->setHtml($json['html']);
        $mail->send();
        $json['success'] = true;
      }
    }

    $this->response->setOutput(json_encode($json));

  }


}
