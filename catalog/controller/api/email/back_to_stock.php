<?php
class ControllerApiEmailBackToStock extends Controller {
  private $errors = array();

  public function  getContent($data = array()){
    if($this->validate($data)){

     $this->load->model('catalog/product');

     $language = new Language($data['language_directory']);
		 $language->load('mail/back_to_stock');

     $this->load->model('tool/image');

     $data['text_admission'] = $language->get('text_admission');
     $data['text_manufacturer'] = $language->get('text_manufacturer');
     $data['text_related'] = $language->get('text_related');

     $data['store_url'] = $this->config->get('store_url');

     $products_id = explode(",", $data['products']);

     $products = array();
     $array_releted_products = array();
     $related_products = array();




     foreach($products_id as $product_id){
        $results[] = $this->model_catalog_product->getProduct($product_id);
     }

     foreach ($results as $key => &$result){
       if($result){
         if ($result['image']) {
           $result['image'] = $this->model_tool_image->resize($result['image'], 168, 168);
         } else {
           $result['image'] = $this->model_tool_image->resize('placeholder.png',168, 168);
         }

         $result['href'] = $this->url->link('product/product', '&product_id=' . $result['product_id']);
         $result['price'] = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
         if($result['special']){
           $result['special'] = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
         }
       }else{
         unset($results[$key]);
       }
     }

     $data['products'] = $results;

     $result_related_products = $this->model_catalog_product->getProductsRelated($data['products']);

     foreach ($result_related_products as &$related_products) {

       if ($related_products['image']) {
         $related_products['image'] = $this->model_tool_image->resize($related_products['image'], 137, 137);
       } else {
         $related_products['image'] = $this->model_tool_image->resize('placeholder.png', 137, 137);
       }

       $related_products['href'] = $this->url->link('product/product', '&product_id=' . $related_products['product_id']);

       $related_products['price'] = $this->currency->format($this->tax->calculate($related_products['price'], $related_products['tax_class_id'], $this->config->get('config_tax')));
       if($related_products['special']){
         $related_products['special'] = $this->currency->format($this->tax->calculate($related_products['special'], $related_products['tax_class_id'], $this->config->get('config_tax')));
       }

       $array_releted_products[] = $related_products;

     }

     $data['related_products'] = $array_releted_products;

     $data['title'] = $data['title'];

     if (file_exists(DIR_TEMPLATE .  $this->config->get('config_template') .'/template/mail/admission_notify.tpl')) {
       $json['html'] = $this->load->view($this->config->get('config_template') .'/template/mail/admission_notify.tpl', $data);
     } else {
       $json['html'] = $this->load->view('default/template/mail/order.tpl', $data);
     }

     $json['text_subject'] = $language->get('text_subject');

    }else{
      $json['errors'] = $this->errors;
    }

    return $json;
  }

  public function validate($data){
    $this->load->language('api/email/affiliate_start');
    if(!isset($data['products'])){
      $this->errors['errors']['warning'] = $language->get('error_empty');
    }
    return !$this->errors;
  }
}
