<?php
class ControllerApiEmailOrderEdited extends Controller {
	public function  getContent($array = array()){
		if($this->validate($array)){

			$this->load->model('checkout/order');

			$order_info = $this->model_checkout_order->getOrder($array['order_id']);

			if (isset($order_info['customer_id']) && (int)$order_info['customer_id'] !== 0){
				$this->load->model('account/customer');
				$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
			}

			$language = new Language($array['language_directory']);
			// $language->load('api/email/order_edited');

			$language->load('mail/order_edited');

			$data['title'] = $array['title'];

				$order_status_id = $order_info['order_status_id'];

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

			  	$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $array['order_id']);

				$data['store_url'] = $this->config->get('config_url');

				$data['text_greeting'] = $language->get('text_update_order') . ' ' . $array['order_id'];

				$data['text_main'][] = $language->get('text_update_date_added') . ' ' . date($this->language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";

				$data['text_main'][] = $language->get('text_update_order_status') . "\n\n";
				$data['text_main'][] = $order_status_query->row['name'] . "\n\n";

				if ($order_info['customer_id']) {
					$data['text_main'][] = $language->get('text_update_link');
					$data['text_main'][] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $array['order_id'];
				}

				$data['text_note'] = $language->get('text_update_footer');

				if (file_exists(DIR_TEMPLATE .  $this->config->get('config_template') .'/template/mail/message.tpl')) {
					$json['html'] = $this->load->view($this->config->get('config_template') .'/template/mail/message.tpl', $data);
				} else {
					$json['html'] = $this->load->view('default/template/mail/order.tpl', $data);
				}

				$json['text_subject'] = sprintf($language->get('text_subject'), $array['order_id']);

		}else{
			$json['errors'] = $this->errors;
		}
		return $json;

	}

	public function  validate($data){

		$this->load->language('api/email/order_edited');
		if(!isset($data['order_id'])){
			$this->errors['errors']['warning'] = $language->get('error_empty');
		}

		return !$this->errors;

	}
}
