<?php
class ControllerApiEmailNewOrder extends Controller {
	private $errors = array();
	public function  getContent($array = array()){


		if($this->validate($array)){

			$order_id = $array['order_id'];

					$this->load->model('checkout/order');

					$order_info = $this->model_checkout_order->getOrder($order_id);

					$this->load->model('account/customer');

					if (isset($order_info['customer_id']) && (int)$order_info['customer_id'] !== 0){
						$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
					}

					$this->session->data['payment_method']['code'] = $order_info['payment_code'];

					$customer_message = $this->load->controller('payment/' . $order_info['payment_code'] . '/getPaymentMessage');


					unset($this->session->data['payment_method']['code']);

					$comment = $customer_message . "\n";

					// Check for any downloadable products
					$download_status = false;

					$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

					foreach ($order_product_query->rows as $order_product) {
						// Check if there are any linked downloads
						$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

						if ($product_download_query->row['total']) {
							$download_status = true;
						}
					}


					$order_gift_query = $this->db->query("SELECT product_id, gift_program_id, quantity
					                                      FROM " . DB_PREFIX . "order_gift_product WHERE order_id = " . (int)$order_id);

					$language = new Language($array['language_directory']);
					// $language->load('api/email/new_order');

					$language->load('mail/new_order');

					$subject = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);

					// HTML Mail
					$data = array();

					$data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

					$data['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$data['text_link'] = $language->get('text_new_link');
					$data['text_download'] = $language->get('text_new_download');
					$data['text_order_detail'] = $language->get('text_new_order_detail');
					$data['text_instruction'] = $language->get('text_new_instruction');
					$data['text_order_id'] = $language->get('text_new_order_id');
					$data['text_date_added'] = $language->get('text_new_date_added');
					$data['text_payment_method'] = $language->get('text_new_payment_method');
					$data['text_shipping_method'] = $language->get('text_new_shipping_method');
					$data['text_email'] = $language->get('text_new_email');
					$data['text_telephone'] = $language->get('text_new_telephone');
					$data['text_ip'] = $language->get('text_new_ip');
					$data['text_order_status'] = $language->get('text_new_order_status');
					$data['text_payment_address'] = $language->get('text_new_payment_address');
					$data['text_shipping_address'] = $language->get('text_new_shipping_address');
					$data['text_product'] = $language->get('text_new_product');
					$data['text_model'] = $language->get('text_new_model');
					$data['text_quantity'] = $language->get('text_new_quantity');
					$data['text_price'] = $language->get('text_new_price');
					$data['text_total'] = $language->get('text_new_total');
					$data['comment'] = $comment;
					$data['text_footer'] = $language->get('text_new_footer');

					$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
					$data['store_name'] = $order_info['store_name'];
					$data['store_url'] = $order_info['store_url'];
					$data['customer_id'] = $order_info['customer_id'];
					$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

					$data['download'] = '';

					$data['order_id'] = $order_id;
					$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
					$data['payment_method'] = $order_info['payment_method'];
					$data['shipping_method'] = $order_info['shipping_method'];
					$data['email'] = $order_info['email'];
					$data['telephone'] = $order_info['telephone'];
					$data['ip'] = $order_info['ip'];

					if ($order_info['payment_address_format']) {
						$format = $order_info['payment_address_format'];
					} else {
						$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
					}

					$find = array(
						'{firstname}',
						'{lastname}',
						'{company}',
						'{address_1}',
						'{address_2}',
						'{city}',
						'{postcode}',
						'{zone}',
						'{zone_code}',
						'{country}'
					);

					$replace = array(
						'firstname' => $order_info['payment_firstname'],
						'lastname'  => $order_info['payment_lastname'],
						'company'   => $order_info['payment_company'],
						'address_1' => $order_info['payment_address_1'],
						'address_2' => $order_info['payment_address_2'],
						'city'      => $order_info['payment_city'],
						'postcode'  => $order_info['payment_postcode'],
						'zone'      => $order_info['payment_zone'],
						'zone_code' => $order_info['payment_zone_code'],
						'country'   => $order_info['payment_country']
					);

					$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

					if ($order_info['shipping_address_format']) {
						$format = $order_info['shipping_address_format'];
					} else {
						$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
					}

					$find = array(
						'{firstname}',
						'{lastname}',
						'{company}',
						'{address_1}',
						'{address_2}',
						'{city}',
						'{postcode}',
						'{zone}',
						'{zone_code}',
						'{country}'
					);

					$replace = array(
						'firstname' => $order_info['shipping_firstname'],
						'lastname'  => $order_info['shipping_lastname'],
						'company'   => $order_info['shipping_company'],
						'address_1' => $order_info['shipping_address_1'],
						'address_2' => $order_info['shipping_address_2'],
						'city'      => $order_info['shipping_city'],
						'postcode'  => $order_info['shipping_postcode'],
						'zone'      => $order_info['shipping_zone'],
						'zone_code' => $order_info['shipping_zone_code'],
						'country'   => $order_info['shipping_country']
					);

					$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

					$this->load->model('tool/upload');

					// Products
					$data['products'] = array();

					foreach ($order_product_query->rows as $product) {
						$option_data = array();

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

								if ($upload_info) {
									$value = $upload_info['name'];
								} else {
									$value = '';
								}
							}

							$option_data[] = array(
								'name'  => $option['name'],
								'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
							);
						}

						$bonus = '';

						if ($this->config->get('bonus_status')) {
							$order_bonus_query = $this->db->query("SELECT quantity
																										FROM " . DB_PREFIX . "order_bonus_product
																										WHERE order_id = " . (int)$order_id . "
																									  AND order_product_id = " . (int)$product['order_product_id']);
							if ($order_bonus_query->num_rows) {
								$bonus = sprintf($this->language->get('text_bonus'), $order_bonus_query->row['quantity']);
							}
						}

						$data['products'][] = array(
							'name'     => $product['name'],
							'model'    => $product['model'],
							'option'   => $option_data,
							'quantity' => $product['quantity'],
							'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
							'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
							'bonus'    => $bonus,
						);
					}

					$data['gift_programs'] = [];

					if ($order_gift_query->num_rows) {
						foreach ($order_gift_query->rows as $order_gift) {
							$gift_product_query =$this->db->query("SELECT pd.name, p.image, p.model
							                                       FROM " . DB_PREFIX . "product p
																										 INNER JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id
																									   WHERE p.product_id = " . (int)$order_gift['product_id'] . " AND pd.language_id = " . (int)$order_info['language_id']);

							if ($order_product_query->num_rows) {
								$data['gift_programs'][$order_gift['gift_program_id']]['gifts'][] = [
				                                     'product_id' => $order_gift['product_id'],
				                                     'quantity' => $order_gift['quantity'],
				                                     'name'     => $gift_product_query->row['name'],
				                                     'model'    => $gift_product_query->row['model'],
				                                     'href'     => $this->url->link('product/product', 'product_id=' . $order_gift['product_id']),
				                                   ];
							}
						}

						$this->load->language('checkout/gift');

						$data['text_gift'] = $this->language->get('text_gift');
					}

					// Vouchers
					$data['vouchers'] = array();

					$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

					foreach ($order_voucher_query->rows as $voucher) {
						$data['vouchers'][] = array(
							'description' => $voucher['description'],
							'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
						);
					}

					// Order Totals
					$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

					foreach ($order_total_query->rows as $total) {
						$data['totals'][] = array(
							'title' => $total['title'],
							'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
						);
					}

					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
						$json['html'] = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
					} else {
						$json['html'] = $this->load->view('default/template/mail/order.tpl', $data);
					}

					$json['text_subject'] = sprintf($this->language->get('text_subject'), $data['order_id']);



			}else{
				$json['errors'] = $this->errors;
			}

			return $json;
		}


	public function  validate($data){
		$this->load->language('api/email/new_order');
		if(!isset($data['order_id'])){
			$this->errors['errors']['warning'] = $this->language->get('error_empty');
		}

		return !$this->errors;

	}
}
