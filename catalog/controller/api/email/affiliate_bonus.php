<?php
class ControllerApiEmailAffiliateBonus extends Controller {
	private $errors = array();
	public function  getContent($data = array()){

		if($this->validate($data)){

		 $this->load->model('account/order');

		 $language = new Language($data['language_directory']);
		 $language->load('mail/affiliate_bonus');

		 $customer = $this->model_account_order->getAffiliateOrder($data['order_id']);

		 $data['store_url'] = $this->config->get('store_url');

		 $data['title'] = $data['title'];

		 $data['text_greeting']  = sprintf($language->get('text_hello'), $customer['firstname']) . "\n\n";

 		 $data['text_main'][]= sprintf($language->get('text_bonuses'), HTTPS_SERVER, html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

 		 $data['text_main'][]= $language->get('text_bonuses_x1') . "\n\n";

 		 $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

		 if (file_exists(DIR_TEMPLATE .  $this->config->get('config_template') .'/template/mail/message.tpl')) {
			 $json['html'] = $this->load->view($this->config->get('config_template') .'/template/mail/message.tpl', $data);
		 } else {
			 $json['html'] = $this->load->view('default/template/mail/order.tpl', $data);
		 }

		 $json['text_subject'] = sprintf($language->get('text_subject'), $data['order_id']);


		}else{
			$json['errors'] = $this->errors;
		}

		return $json;

	}

	public function  validate($data){

		$this->load->language('api/email/affiliate_bonus');
		if(!isset($data['order_id'])){
			$this->errors['warning'] = $language->get('error_empty');
		}

		return !$this->errors;

	}

}
