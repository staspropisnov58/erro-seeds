<?php
class ControllerApiEmailRegistration extends Controller {
  public function  getContent($array = array()){
    if($this->validate($array)){

      // $this->load->language('mail/customer');

      $language = new Language($array['language_directory']);
      $language->load('mail/customer');

      $data['title'] = $array['title'];
      $data['store_url']      = $this->config->get('config_url');
      $data['text_greeting']  = $language->get('text_greeting') . "\n\n";
      $data['text_main'][] = $language->get('text_welcome');
      $data['text_main'][] = $language->get('text_services');
      $data['text_main'][] = sprintf($language->get('text_link'), $this->url->link('account/newsletter')). "\n\n";
      $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/message.tpl')) {
        $json['html'] = $this->load->view($this->config->get('config_template') . '/template/mail/message.tpl', $data);
      } else {
        $json['html'] = $this->load->view('default/template/mail/order.tpl', $data);
      }

      $json['text_subject'] = $language->get('text_subject');

    }else{
      $json['errors'] = $this->errors;
    }
    return $json;

  }

  public function  validate($data){

    return !$this->errors;

  }
}
