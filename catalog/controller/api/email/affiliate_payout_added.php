<?php
class ControllerApiEmailAffiliatePayoutAdded extends Controller {
	private $errors = array();

	public function getContent($data = array()){

		if($this->validate($data)){

		 $this->load->model('account/order');

		 $language = new Language($data['language_directory']);
		 $language->load('mail/affiliate_payout_added');

		 // $this->load->language('api/mail/affiliate_payout_added');

		 $data['title'] = $data['title'];

		 $data['store_url'] = $this->config->get('store_url');

		 $data['text_greeting']  = $language->get('text_hello') . "\n\n";

 		 $data['text_main'][]= sprintf($language->get('text_bonuses'), html_entity_decode($data['sum'], ENT_QUOTES, 'UTF-8'), html_entity_decode($this->config->get('config_currency'), ENT_QUOTES, 'UTF-8'), HTTPS_SERVER,  html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

 		 $data['text_main'][]= $language->get('text_bonuses_x1') . "\n\n";


 		 $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

		 if (file_exists(DIR_TEMPLATE .  $this->config->get('config_template') .'/template/mail/message.tpl')) {
			 $json['html'] = $this->load->view($this->config->get('config_template') .'/template/mail/message.tpl', $data);
		 } else {
			 $json['html'] = $this->load->view('default/template/mail/order.tpl', $data);
		 }

		 $json['text_subject'] = $language->get('text_subject');

		}else{
			$json['errors'] = $this->errors;
		}

		return $json;
	}

	public function validate($data){

		$this->load->language('api/email/affiliate_payout_added');
		if(!isset($data['sum'])){
			$this->errors['errors']['warning'] = $language->get('error_empty');
		}

		return !$this->errors;

	}

}
