<?php
class ControllerApiEmailReviewModerated extends Controller {
  public function  getContent($data = array()){
    if($this->validate($data)){
      $this->load->model('catalog/review');

      $language = new Language($data['language_directory']);
      $language->load('mail/review_moderated');

      $data_review = $this->model_catalog_review->getReview($data['review_id']);

      $href = $this->url->link('product/product', '&product_id=' . $data_review['product_id']);

      $data['store_url'] = $this->config->get('config_url');
      $data['text_greeting'] = sprintf($language->get('text_hello'), $data_review['author']) . "\n\n";
      $data['text_main'][] = sprintf($language->get('text_review_moderated'), $data_review['product']) . "\n\n";
      $data['text_main'][] = sprintf($language->get('text_review_moderated_x1'), $href) . "\n\n";
      $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/message.tpl')) {
        $json['html'] = $this->load->view($this->config->get('config_template') . '/template/mail/message.tpl', $data);
      } else {
        $json['html'] = $this->load->view('default/template/mail/order.tpl', $data);
      }
      $json['text_subject'] = $language->get('text_subject');
    }else{
      $json['errors'] = $this->errors;
    }
    return $json;
  }


  public function  validate($data){
    $this->load->language('api/email/review_moderated');
    if(!isset($data['review_id'])){
      $this->errors['errors']['warning'] = $language->get('error_empty');
    }

    return !$this->errors;
	}
}
