<?php
class ControllerApiCoupon extends Controller {
	public function index() {
		$this->load->language('api/coupon');

		// Delete past coupon in case there is an error
		unset($this->session->data['coupon']);

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$this->load->model('checkout/coupon');

			if (isset($this->request->post['coupon'])) {
				$coupon = $this->request->post['coupon'];
			} else {
				$coupon = '';
			}

			$coupon_info = $this->model_checkout_coupon->getCoupon($coupon);

			if ($coupon_info) {
				$this->session->data['coupon'] = $this->request->post['coupon'];

				$json['success'] = $this->language->get('text_success');
			} else {
				$json['error'] = $this->language->get('error_coupon');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function coupon_list() {
		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$json['coupon_list'] = array();

			$this->load->model('checkout/coupon');

			$coupons = $this->model_checkout_coupon->getCoupons();

			foreach ($coupons as $coupon) {
				$json['coupon_list'][] = array(
					'code' => $coupon['code'],
					'discount' => $coupon['discount'],
					'type' => $coupon['type'],
					'total' => $coupon['total'],
					'shipping' => $coupon['shipping'],
					'products' => $this->model_checkout_coupon->getCouponProducts($coupon['coupon_id']),
					'categories' => $this->model_checkout_coupon->getCouponCategories($coupon['coupon_id']),
					'uses_total' => $coupon['uses_total'],
					'uses_customer' => $coupon['uses_customer'],
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
