<?php
class ControllerCommonAjaxLoader extends Controller
{
  public function index()
  {
    $json = array();
    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      if (isset($this->request->post['controller'])) {
        $controller = $this->request->post['controller'];
        $data = $this->request->post;
        unset($data['controller']);
        $json['html'] = $this->load->controller('product/product/displayProducts', $data);

        $this->session->data['last_modified'] = date('Y-m-d H:i:s');
      } else {
        $json['error'] = 'There is no controller path in $_POST';
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}
