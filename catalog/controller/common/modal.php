<?php
class ControllerCommonModal extends Controller
{
  /**
   * calls method by manes from $_GET['modal']
   * other $_GET content is processed in called method
   */
  public function index()
  {
    if (isset($this->request->get['modal'])) {
      $modal = (string)$this->request->get['modal'];
      $loader = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $modal)));
      if (method_exists($this, $loader)) {
        $this->load->language('module/' . $modal);
        $data = $this->$loader();
        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/modal/' . $modal . '.tpl', $data));
      }
    }
  }

  /**
   * get data for callback form processed by ControllerModuleCallback::send()
   * @return array only localisation
   */

  private function getCallback()
  {
    $data['title'] = $this->language->get('title');
    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_phone'] = $this->language->get('entry_phone');

    $form = new Form('callback', $this->url->link('module/callback/send', '', 'SSL'));

    $form->addField(['name' => 'firstname', 'label' => $this->language->get('entry_name'), 'placeholder' => '', 'sort_order' => 0, 'value' => '', 'type' => 'text']);
    $form->addField(['name' => 'telephone', 'label' => $this->language->get('entry_phone'), 'placeholder' => '', 'sort_order' => 1, 'value' => '', 'type' => 'tel']);

    if ($this->config->get('enebled_tel_code')) {
      $this->load->model('localisation/country');
      $data['countries'] = $this->model_localisation_country->getCountries();
      $data['country_id'] = $this->config->get('config_country_id');

      $form->addField(['name' => 'telephone_country_id', 'label' => '', 'placeholder' => '', 'sort_order' => 1, 'value' => $data['country_id'], 'type' => 'select']);
      $form->groupFields('telephone', 'telephone_country_id', 'telephone');
    }

    $data['form'] = $form->build();

    $data['button_submit'] = $this->language->get('button_submit');
    return $data;
  }

  private function getContact()
  {
    $data['title'] = $this->language->get('title');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_comment'] = $this->language->get('entry_comment');

    $form = new Form('callback', $this->url->link('module/callback/send', '', 'SSL'));

    $form->addField(['name' => 'email', 'label' => $this->language->get('entry_email'), 'placeholder' => '', 'sort_order' => 0, 'value' => $this->customer->getEmail(), 'type' => 'text']);
    $form->addField(['name' => 'comment', 'label' => $this->language->get('entry_comment'), 'placeholder' => '', 'sort_order' => 1, 'value' => '', 'type' => 'textarea']);

    $data['form'] = $form->build();

    $data['button_submit'] = $this->language->get('button_submit');
    return $data;
  }

  private function getReview()
  {
    if (!empty($this->request->get['product_id'])) {
      $this->load->model('catalog/product');
      $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);

      $data['title'] = sprintf($this->language->get('title_product'), $product_info['name']);
    } else if (!empty($this->request->get['news_id'])) {
      $this->load->model('extension/news');
      $news = $this->model_extension_news->getNews($this->request->get['news_id']);

      $data['title'] = sprintf($this->language->get('title_news'), $news['title']);
    } else {
      $data['title'] = sprintf($this->language->get('title_store'), $this->config->get('config_name'));
    }

    $customer_name = $this->customer->isLogged() ? $this->customer->getFirstName() . ' ' . $this->customer->getLastName() : '';

    if (!empty($this->request->get['news_id'])) {
      $data['news_id'] = $this->request->get['news_id'];
      $entry_commnent = $this->language->get('entry_comment');
      $data['button_submit'] = $this->language->get('button_submit_comment');
    } else {
      $data['product_id'] = $this->request->get['product_id'];
      $entry_comment = $this->language->get('entry_review');
      $data['button_submit'] = $this->language->get('button_submit');
    }


    $form = new Form('review', $this->url->link('information/review/addReview', '', 'SSL'));

    $form->addField(['name' => 'rating', 'label' => $this->language->get('entry_rating'), 'placeholder' => '', 'sort_order' => 0, 'value' => '', 'type' => 'radio', 'options' => [1, 2, 3, 4, 5]]);
    $form->addField(['name' => 'firstname', 'label' => $this->language->get('entry_name'), 'placeholder' => '', 'sort_order' => 1,
                     'value' => $customer_name, 'type' => 'text']);
    $form->addField(['name' => 'email', 'label' => $this->language->get('entry_email'), 'placeholder' => '', 'sort_order' => 2, 'value' => $this->customer->getEmail(), 'type' => 'text']);
		$form->addField(['name' => 'comment', 'label' => $this->language->get('entry_comment'), 'placeholder' => '', 'sort_order' => 3, 'value' => '', 'type' => 'textarea']);

    $data['form'] = $form->build();

    return $data;
  }

  /**
   * get data for callback form processed by ControllerModuleNotificationRequiest::sendRequiest()
   * @return array localisation, product id and customer's email if logged
   */

  private function getNotificationRequest()
  {
    $data = [];
    $data['title'] = $this->language->get('button_entrance');

    $product_data = [];
    $product_data['product_id'] = $this->request->get['product_id'];

    $this->load->model('catalog/product');
    $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);

    if ($product_info) {
      $product_data['name'] = $product_info['name'];
      $data = array_merge($data, $this->load->controller('module/notification_request/renderForm', $product_data));
    }

    return $data;
  }

  private function getOptions()
  {
    $data = array();
    $this->load->model('catalog/product');
    $this->load->language('product/product');

    $data['button_cart'] = $this->language->get('button_cart');
    $data['button_continue_shopping'] = $this->language->get('button_continue_shopping');
    $data['entry_qty'] = $this->language->get('entry_qty');
    $data['text_economy'] = $this->language->get('text_economy');

    $data['product_id'] = $this->request->get['product_id'];

    $product_info = $this->model_catalog_product->getProduct($data['product_id']);

    $data['name'] = $product_info['name'];

    if ($product_info) {
      $data['options'] = $this->load->controller('product/product/getProductOptions', $product_info);
      if (array_key_exists('update', $data['options'])) {
				foreach($data['options']['update'] as $key => $value) {
					$product_info[$key] = $value;
				}
				unset($data['options']['update']);
			}

      if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['price'] = false;
			}

      if ((float)$product_info['special']) {
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				$data['saving'] = $this->currency->format($this->tax->calculate((float)$product_info['price'] - (float)$product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['special'] = false;
				$data['saving'] = '';
			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$data['tax'] = false;
			}

      if ($product_info['image']) {
        $data['image'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
      } else {
        $data['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
      }

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

      $data['manufacturer'] = $product_info['manufacturer'];

      $data['sku'] = '';
    }

    return $data;
  }
}
