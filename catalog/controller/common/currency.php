<?php
class ControllerCommonCurrency extends Controller {
	public function index() {
		$this->load->language('common/currency');
//        if($this->session->data['language']=='en'){
//            $this->session->data['currency']='USD';
//            $this->currency->set('USD');
//            $this->session->data['currency']='USD';
//
//        }

		$data['text_currency'] = $this->language->get('text_currency');

		$data['exchange_rates'] = $this->language->get('exchange_rates');

		$data['action'] = $this->url->link('common/currency/currency', '', $this->request->server['HTTPS']);

		$data['code'] = $this->currency->getCode();

		$this->load->model('localisation/currency');

		$data['currencies'] = array();

		$results = $this->model_localisation_currency->getCurrencies();


        foreach ($results as $result) {
			if ($result['status']) {
				$data['currencies'][] = array(
					'title'        => $result['title'],
					'code'         => $result['code'],
					'symbol_left'  => $result['symbol_left'],
					'symbol_right' => $result['symbol_right'],
          'value'        => round($result['value'], 3),
				);
			}
		}

		$data['config_currency'] = $this->config->get('config_currency');



		if (!isset($this->request->get['route'])) {
			$data['redirect'] = $this->url->link('common/home');
		} else {
			$url_data = $this->request->get;

			unset($url_data['_route_']);

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/currency.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/currency.tpl', $data);
		} else {
			return $this->load->view('default/template/common/currency.tpl', $data);
		}
	}

	public function currency() {

    



		if (isset($this->request->post['code'])) {
			$this->currency->set($this->request->post['code']);

			$this->session->data['last_modified'] = date('Y-m-d H:i:s');		

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
		}

		if (isset($this->request->post['redirect'])) {
			$this->response->redirect($this->request->post['redirect']);
		} else {
			$this->response->redirect($this->url->link('common/home'));
		}
	}
}
