<?php
class ControllerCommonCart extends Controller {
	private $route = 'common/cart';

	public function index() {
		$this->load->language('common/cart');

		// Totals
		$this->load->model('extension/extension');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		// Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_cart'] = $this->language->get('text_cart');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_empty_cart']= $this->language->get('text_empty_cart');

		$data['text_quantity_pack'] = $this->language->get('text_quantity_pack');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_size'] = $this->language->get('text_size');
		$data['text_color'] = $this->language->get('text_color');
		$data['text_enter_promocode'] = $this->language->get('text_enter_promocode');
		$data['text_remove_item'] = $this->language->get('text_remove_item');
		$data['text_refresh'] = $this->language->get('text_refresh');
		$data['text_apply_coupon'] = $this->language->get('text_apply_coupon');
		$data['text_quick_order'] = $this->language->get('text_quick_order');
		$data['text_normal_order'] = $this->language->get('text_normal_order');
		$data['text_in_package'] = $this->language->get('text_in_package');
		$data['text_no_js'] = $this->language->get('text_no_js');
		$data['text_telephone'] = $this->language->get('text_telephone');

		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->session->data['cart_is_open'])) {
      $data['cart_is_open'] = $this->session->data['cart_is_open'];
      unset($this->session->data['cart_is_open']);
    } else {
      $data['cart_is_open'] = 0;
    }

		$total_price = 0;

		$data['total_price'] = $this->currency->format($this->cart->getTotal());

		$data['cart_count'] = $this->cart->countProducts();

		// Gift Voucher
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'])
				);
			}
		}

		$data['cart_popup'] = $this->load->controller('checkout/cart', false);

		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/cart.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/cart.tpl', $data);
		} else {
			return $this->load->view('default/template/common/cart.tpl', $data);
		}
	}

	public function info() {
		$this->response->setOutput($this->index());
	}
}
