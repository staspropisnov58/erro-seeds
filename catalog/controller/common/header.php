<?php

class ControllerCommonHeader extends Controller
{
    public function index()
    {
        if (preg_match('/^mail./', $this->request->server['HTTP_HOST'])) {
            $this->document->setRobots('noindex,nofollow,noarchive');
        }
        if($this->session->data['language']=='en'){
         //   if(!isset($this->session->data['currency_ol'])){
                $this->session->data['currency']='USD';
                $this->currency->set('USD');
          //  }

         //   $this->session->data['currency']='USD';

        }
        $this->load->model('module/ajaxzoom');
        $data['ajaxzoom'] = $this->model_module_ajaxzoom->hookHeader();

        $this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/common.js?v=2.3.9');
        $this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/owl.carousel.min.js');
        $this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/owl.carousel2.thumbs.js');
        $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/css/owl.carousel.min.css');
        $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/css/owl.theme.default.min.css');

        $data['title'] = $this->document->getTitle();

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['robots'] = $this->document->getRobots();
        $data['microdata'] = $this->document->getMicrodata();
        $data['opengraph'] = $this->document->getOpengraph();

        $data['e_commerce'] = $this->document->getEcommerce();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');
        $data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');
        $data['name'] = $this->config->get('config_name');

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $data['icon'] = $server . 'image/' . $this->config->get('config_icon');
        } else {
            $data['icon'] = '';
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $this->load->language('common/header');

        $data['text_home'] = $this->language->get('text_home');
        $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
        $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
        $data['cart_count'] = $this->cart->countProducts();


        $data['user_account_x'] = $this->language->get('user_account_x');
        $data['text_tariffs'] = $this->language->get('text_tariffs');
        $data['text_fast_order'] = $this->language->get('text_fast_order');
        $data['text_about_shop'] = $this->language->get('text_about_shop');
        $data['text_queshions'] = $this->language->get('text_queshions');
        $data['text_schedule'] = $this->language->get('text_schedule');
        $data['text_delivery'] = $this->language->get('text_delivery');
        $data['text_payment'] = $this->language->get('text_payment');
        $data['text_aur_address'] = $this->language->get('text_aur_address');
        $data['text_buy_cannabis_seeds'] = $this->language->get('text_buy_cannabis_seeds');
        $data['text_replacement_and_return'] = $this->language->get('text_replacement_and_return');
        $data['text_for_individuals'] = $this->language->get('text_for_individuals');
        $data['text_for_shops'] = $this->language->get('text_for_shops');
        $data['text_seeds'] = $this->language->get('text_seeds');
        $data['text_cooperation'] = $this->language->get('text_cooperation');
        $data['text_law'] = $this->language->get('text_law');
        $data['text_private_office'] = $this->language->get('Кабинет');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
        $data['text_category'] = $this->language->get('text_category');
        $data['text_all'] = $this->language->get('text_all');
        $data['text_search'] = $this->language->get('text_search');
        $data['title_compare'] = $this->language->get('title_compare');
        $data['text_sale'] = $this->language->get('text_sale');
        $data['title_wishlist'] = $this->language->get('title_wishlist');
        $data['text_about_us'] = $this->language->get('text_about_us');
        $data['text_about_us_feedbacs'] = $this->language->get('text_about_us_feedbacs');
        $data['text_about_us_contacts'] = $this->language->get('text_about_us_contacts');
        $data['text_about_us_sequrity'] = $this->language->get('text_about_us_sequrity');
        $data['text_about_us_faq'] = $this->language->get('text_about_us_faq');
        $data['text_compare_catalog'] = $this->language->get('text_compare_catalog');
        $data['text_compare_partnership'] = $this->language->get('text_compare_partnership');
        $data['text_compare_news'] = $this->language->get('text_compare_news');
        $data['text_compare_promotions'] = $this->language->get('text_compare_promotions');
        $data['text_compare_forum'] = $this->language->get('text_compare_forum');
        $data['text_about_us_contacts'] = $this->language->get('text_about_us_contacts');
        $data['text_tarif'] = $this->language->get('text_tarif');

        $data['button_callback'] = $this->language->get('button_callback');
        // Модалка Соглашение
        $data['text_modal_agreement_h1'] = $this->language->get('text_modal_agreement_h1');
        $data['text_modal_agreement'] = $this->language->get('text_modal_agreement');
        $data['text_modal_agreement_yes'] = $this->language->get('text_modal_agreement_yes');
        $data['text_modal_agreement_no'] = $this->language->get('text_modal_agreement_no');


        $data['home'] = $this->url->link('common/home');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['compare'] = $this->url->link('product/compare', '', 'SSL');
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['register'] = $this->url->link('account/register', '', 'SSL');
        $data['login'] = $this->url->link('account/login', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
        $data['download'] = $this->url->link('account/download', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['jivochate_code'] = $this->config->get('config_jivochate')[$this->config->get('config_language_id')];

        $status = true;

        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

            foreach ($robots as $robot) {
                if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
                    $status = false;

                    break;
                }
            }
        }
        //agreement

        $is_bot = false;
        $robots_agents = preg_split('/\r\n|\r|\n/', strtolower($this->config->get('config_robots')));
        array_walk($robots_agents, function ($user_agent, $key) use (&$is_bot) {
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), $user_agent) !== false) {
                    $is_bot = true;
                }
            }
        });

        if ($is_bot || !empty($this->request->cookie['agreement'])) {
            $data['agreement_popup'] = '';
        } else {
            $this->load->model('extension/module');
            $popups = $this->model_extension_module->getModule(43);
            if (!empty($popups)) {
                if ($popups['status'] == 1) {
                    if ($popups['for_all'] === 'on') {
                        $popups['module_description'][$data['lang']]['description'] = html_entity_decode($popups['module_description'][$data['lang']]['description']);
                        $data['agreement_popup'] = $popups['module_description'][$data['lang']];
                    }
                }
            }
        }

        //menu


        // links for SEO
        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        $array_lang_id = array();

        foreach ($languages as $language) {

            $array_lang_id[] = array(
                'code' => $language['code']
            );
        }

        $this->load->model('localisation/language');
        $this->load->model('localisation/country');
        $languages = $this->model_localisation_language->getLanguages();
        $country = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));
        $request_uri = $this->request->server['REQUEST_URI'];
        $page = preg_match('/^\/[a-z]{2}\//', $request_uri) ? substr($request_uri, 3) : $request_uri;
        $data['hreflangs'] = array();
        foreach ($languages as $lang) {
            $code = $lang['code'];
            if ($lang['code'] !== $this->config->get('default_language')) {
                $href = $data['base'] . $lang['code'] . $page;
            } else {
                $href = $data['base'] . ltrim($page, '/');
            }
            if ($code !== 'en') {
                $this->document->addLink($href, 'alternate', $code);
            }
            // $this->document->addLink($href, 'alternate', $code . '-' . strtolower($country['iso_code_2']));

        }

        $data['links'] = $this->document->getLinks();

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $catgory_data_header = $this->cache->get('catgory_data_header.menu.' . $this->config->get('config_store_id') . '.' . $this->config->get('config_language_id'));
        if ($catgory_data_header) {
            $data['categories'] = $catgory_data_header;
        } else {
            $categories = $this->model_catalog_category->getCategories(0);
            foreach ($categories as $category) {
                if ($category['top']) {
                    // Level 2
                    $children_data = array();

                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach ($children as $child) {
                        $filter_data = array(
                            'filter_category_id' => $child['category_id'],
                            'filter_sub_category' => true
                        );

                        $children_data[] = array(
                            'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                        );
                    }

                    // Level 1
                    $data['categories'][] = array(
                        'name' => $category['name'],
                        'children' => $children_data,
                        'column' => $category['column'] ? $category['column'] : 1,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }
            $this->cache->set('catgory_data_header.menu.' . $this->config->get('config_store_id') . '.' . $this->config->get('config_language_id'), $data['categories']);

        }
        $data['mobile_menu']= false;
        if($this->isMobile()){
            $data['mobile_menu']= true;
        }

        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        $data['cart'] = $this->load->controller('common/cart');


        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } else {
                $class = '';
            }

            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'common-home';
        }

        $menu_module = 82;
        $header_menu = $this->model_extension_module->getModule($menu_module);
        $data['header_menu'] = $this->load->controller('module/outer_html', $header_menu);



        $data['config_language'] = $this->config->get('config_language_id');
        $data['lincks_hed'] = '';
        if (!isset($this->request->get['product_id']) && !isset($this->request->get['path'])) {

            $lincks_hed = $this->cache->get('lincks_hed_.' . (int)$this->config->get('config_language_id'));
            if (!$lincks_hed) {
                $data['lincks_hed'] = $this->PereLinkHeader();
                $this->cache->set('lincks_hed_.' . (int)$this->config->get('config_language_id'), $data['lincks_hed']);
            } else {
                $data['lincks_hed'] = $this->cache->get('lincks_hed_.' . (int)$this->config->get('config_language_id'));
            }

        }

        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $data['banners'] = array();

        $results = $this->model_design_banner->getBanner(10);

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners'] = array(
                    'title' => $result['title'],
                    'link'  => $result['link'],
                    'image' => $this->model_tool_image->resize($result['image'], 736, 50)
                );
            }
        }


        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
        } else {
            return $this->load->view('default/template/common/header.tpl', $data);
        }
    }

    public function PereLinkHeader()
    {
        $data = file("https://errors-seeds.com.ua/published/perelink/file/links-Header.txt");
        $total = count($data);
        srand((double)microtime() * 1000000);
        $mn = 5; // сколько выводить минимум
        $mx = 5; // сколько выводить максимум
        $crosslinks = "";
        for ($i = 0; $i < mt_rand($mn, $mx); $i++) {
            $s = mt_rand(0, $total - 1);
            $crosslinks .= "" . $data[$s];
        }

        return "$crosslinks";
    }

    public function isMobile()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
