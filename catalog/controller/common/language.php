<?php
class ControllerCommonLanguage extends Controller {
	public function index() {
		$this->load->language('common/language');



		$data['text_language'] = $this->language->get('text_language');

		$data['action'] = $this->url->link('common/language/language', '', $this->request->server['HTTPS']);

		$data['code'] = $this->session->data['language'];



		$this->load->model('localisation/language');

		$data['languages'] = array();

		$results = $this->model_localisation_language->getLanguages();

		foreach ($results as $result) {
			if ($result['status']) {
				$data['languages'][] = array(
					'directory' => $result['directory'],
					'name'  => $result['name'],
					'code'  => $result['code'],
					'image' => $result['image']
				);
			}
		}

		$request_uri = $this->request->server['REQUEST_URI'];
		$page = preg_match('/^\/[a-z]{2}\//', $request_uri) ? substr($request_uri, 3) : $request_uri;
		$data['redirects'] = [];

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}
		foreach ($results as $lang) {
			$code = $lang['code'];
			if ($lang['code'] !== $this->config->get('default_language')) {
				$href = $server . $lang['code'] . $page;
			} else {
				$href = $server . ltrim($page, '/');
			}

			$data['redirects'][$lang['code']] = $href;
		}

		if (!isset($this->request->get['route'])) {
			$data['redirect'] = $this->url->link('common/home');
		} else {
			$url_data = $this->request->get;

			unset($url_data['_route_']);

			$route = $url_data['route'];

			unset($url_data['route']);

			$url = '';

			if ($url_data) {
				$url = '&' . urldecode(http_build_query($url_data, '', '&'));
			}

			$data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/language.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/language.tpl', $data);
		} else {
			return $this->load->view('default/template/common/language.tpl', $data);
		}
	}

	public function language() {
//        if ($this->request->post['code'] =='en'){  // ваше условие
//            $this->session->data['currency'] = 'USD'; // код валюты которую ставим
//            $this->currency->set('USD');
//        }

//        print_r(1);
//        exit();
		if (isset($this->request->post['code'])) {
			$this->session->data['language'] = $this->request->post['code'];

			$this->session->data['last_modified'] = date('Y-m-d H:i:s');

			if ($this->customer->isLogged()) {
				$this->load->model('account/customer');
				$this->model_account_customer->addCustomerLanguage($this->request->post['code'], $this->customer->getId());
			}
		}

		if (isset($this->request->post['redirect'])) {
			$this->response->redirect($this->request->post['redirect']);
		} else {
			$this->response->redirect($this->url->link('common/home'));
		}
	}
}
