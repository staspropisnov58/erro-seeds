<?php

class  ControllerCommonFooter extends Controller
{

    public function addToNewsletter()
    {

        $email = $this->request->post['email'];

        $this->load->language('common/footer');
        $this->load->model('account/customer');

        $this->createNewsletterTables();

        $count = $this->checkEmailSubscribe($email);

        if ($count == 0) {

            $newsletter_id = $this->model_account_customer->addToNewsletter($email);
            $msg = $this->language->get('text_success_subcribe');

        } else {

            $msg = $this->language->get('text_error_subcribe');
        }

        echo $msg;

    }


    public function checkEmailSubscribe($email)
    {

        $this->load->model('account/customer');

        $count = $this->model_account_customer->checkEmailSubscribe($email);

        return $count;

    }

    public function index()
    {
        $this->load->language('common/footer');
        $this->load->model('extension/news');

        $data['text_information'] = $this->language->get('text_information');
        $data['text_service'] = $this->language->get('text_service');
        $data['text_extra'] = $this->language->get('text_extra');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_return'] = $this->language->get('text_return');
        $data['text_sitemap'] = $this->language->get('text_sitemap');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_voucher'] = $this->language->get('text_voucher');
        $data['text_affiliate'] = $this->language->get('text_affiliate');
        $data['text_special'] = $this->language->get('text_special');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_newsletter_text'] = $this->language->get('text_newsletter_text');
        $data['text_subcribe'] = $this->language->get('text_subcribe');
        $data['text_error_subcribe'] = $this->language->get('text_error_subcribe');
        $data['text_success_subcribe'] = $this->language->get('text_success_subcribe');
        $data['text_email_not_validate'] = $this->language->get('text_email_not_validate');
        $data['text_new_and_popular'] = $this->language->get('text_new_and_popular');
        $data['text_seeds'] = $this->language->get('text_seeds');
        $data['text_abuot_shop'] = $this->language->get('text_abuot_shop');
        $data['text_queshions'] = $this->language->get('text_queshions');
        $data['text_buy_cannabis_seeds'] = $this->language->get('text_buy_cannabis_seeds');
        $data['text_our_address'] = $this->language->get('text_our_address');
        $data['text_delivery'] = $this->language->get('text_delivery');
        $data['text_pay'] = $this->language->get('text_pay');
        $data['text_schedule'] = $this->language->get('text_schedule');
        $data['text_blog'] = $this->language->get('text_blog');
        $data['text_social_networks'] = $this->language->get('text_social_networks');
        $data['text_footer'] = $this->language->get('text_footer');
        $data['text_ES'] = $this->language->get('text_ES');
        $data['text_ES2'] = $this->language->get('text_ES2');
        $data['text_lamp'] = $this->language->get('text_lamp');
        $data['text_medical'] = $this->language->get('text_medical');
        $data['text_help'] = $this->language->get('text_help');
        $data['contact_us'] = $this->language->get('contact_us');
        $data['feedbacs_x'] = $this->language->get('feedbacs_x');
        $data['text_compare_forum'] = $this->language->get('text_compare_forum');
        $data['about_us'] = $this->language->get('about_us');
        $data['questions'] = $this->language->get('questions');
        $data['payment_methods'] = $this->language->get('payment_methods');
        $data['delivery'] = $this->language->get('delivery');
        $data['replacement_and_return'] = $this->language->get('replacement_and_return');
        $data['contact_us_button'] = $this->language->get('contact_us_button');
        $data['join_us'] = $this->language->get('join_us');
        $data['address_2'] = $this->language->get('address_2');
        $data['text_contact'] = $this->language->get('text_contact');

        $data['entry_firstname'] = $this->language->get('entry_firstname');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_comment'] = $this->language->get('entry_comment');
        $data['title_feedback'] = $this->language->get('title_feedback');
        $data['button_modal_save'] = $this->language->get('button_modal_save');
        $data['rating'] = $this->language->get('rating');
        $data['ocmodpcart'] = $this->config->get('pcart_pcart');


        if ($this->customer->isLogged()) {
            $this->load->model('account/customer');
            $this->load->model('account/activity');

            $activity_data = array(
                'name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
                'email' => $this->customer->getEmail(),
            );
        } else {
            $activity_data = array(
                'name' => '',
                'email' => '',
            );
        }

        $news = array();

        $news = $this->model_extension_news->getAllNews($news);

        foreach ($news as $key => $newses) {
            $data['newses'][] = array(
                'href' => $this->url->link('information/news/news', 'news_id=' . $newses['news_id']),
                'title' => $newses['title']
            );
            if ($key == 7) {
                break;
            }
        }
        $data['user_info'] = $activity_data;

        $this->load->model('catalog/information');

        $data['informations'] = array();
        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }

        $data['contact'] = $this->url->link('information/contact');
        $data['return'] = $this->url->link('account/return/add', '', 'SSL');
        $data['sitemap'] = $this->url->link('information/sitemap');
        $data['manufacturer'] = $this->url->link('product/manufacturer');
        $data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
        $data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
        $data['special'] = $this->url->link('product/special');
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

        $data['telephone'] = $this->config->get('config_telephone');
        $data['fax'] = $this->config->get('config_fax');
        $data['address'] = $this->config->get('config_address');
        $data['comment'] = $this->config->get('config_comment');


        $data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        $data['soc'] = explode(' ; ', $this->config->get('config_comment'));
        $this->load->model('tool/online');
        // Whos Online
        if ($this->config->get('config_customer_online')) {


            if (isset($this->request->server['REMOTE_ADDR'])) {
                $ip = $this->request->server['REMOTE_ADDR'];
            } else {
                $ip = '';
            }

            if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
                $url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
            } else {
                $url = '';
            }

            if (isset($this->request->server['HTTP_REFERER'])) {
                $referer = $this->request->server['HTTP_REFERER'];
            } else {
                $referer = '';
            }

            $this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
        }



        $data['calendars'] = array();
        if($this->isMobile()){

        }else{



            $calendars = $this->model_tool_online->getCalendar();



            $this->load->model('tool/online');
            if ($calendars) {
                foreach ($calendars as $calendar) {

                 //   (int)$this->config->get('config_language_id');

                    if(!empty($calendar['img_lan']) && is_file(DIR_IMAGE . $calendar['img_lan'])){
                        $image = $this->model_tool_image->resize($calendar['img_lan'], 300, 100);
                    }else{
                        if(!empty($calendar['image']) && is_file(DIR_IMAGE . $calendar['image'])){
                            $image = $this->model_tool_image->resize($calendar['image'], 300, 100);
                        }else{
                            $image='';
                        }
                    }




                    if($calendar['date_available'] <= $calendar['date_available_end']){

                    //    while (strftime($calendar['date_available']) <= strftime($calendar['date_available_end'])){


                       //     $calendar['date_availabletest'][] = date('Y-m-d',strftime($calendar['date_available'],'+ 1 days'));
                    //    }

                    }

                    while (strftime($calendar['date_available']) <= strftime($calendar['date_available_end'])) {



                        $data['calendars'][] = array(
                            'calendar_id' => $calendar['calendar_id'],
                            'date_available' => $calendar['date_available'],
                            'date_available_end' => $calendar['date_available_end'],
                            'image' => $image,
                            'title' => $calendar['title'],
                            'description' => html_entity_decode($calendar['description'], ENT_QUOTES, 'UTF-8'),
                            'url' => $calendar['url'],
                        );
                        $calendar['date_available']=date("Y-m-d", strtotime($calendar['date_available'].'+ 1 days'));

                    }


                  //  $calendar['date_availabletest'][] = date('Y-m-d',strftime($calendar['date_available'].'+ 1 days'));
               //       $calendar['date_availabletest'][] = date("Y-m-d", strtotime($calendar['date_available'].'+ 1 days'));



                }
            }
        }

//        echo '<pre style="display: none">';
//        print_r($data['calendars']);
//        echo '</pre>';


        if (isset($this->request->get['route'])) {
            $data['class'] = str_replace('/', '-', $this->request->get['route']);
        } else {
            $data['class'] = 'common-home';
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
        } else {
            return $this->load->view('default/template/common/footer.tpl', $data);
        }
    }
    public function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
