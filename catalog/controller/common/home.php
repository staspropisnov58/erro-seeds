<?php
class ControllerCommonHome extends Controller {
	private $route = 'common/home';

	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'. $this->config->get('config_language_id')));
		$this->document->setDescription($this->config->get('config_meta_description'. $this->config->get('config_language_id')));
		$this->document->setKeywords($this->config->get('config_meta_keyword'. $this->config->get('config_language_id')));

		if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){
			$this->load->model('tool/opengraph');
			$this->model_tool_opengraph->addOpengraphForPage($this->route);
		}

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->url->link('common/home', '', 'SSL'), 'canonical');
		}

		$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/home.js');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        $this->load->language('common/header');

        $data['text_H1_all'] = $this->language->get('text_H1_all');

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);



        foreach ($categories as $category) {
            if ($category["category_id"] == 102){

                $data['all_category_href'] = $this->url->link('product/category', 'path=' . $category['category_id']);
                continue;
            }

//            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    $filter_data = array(
                        'filter_category_id'  => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                        'image'  => $child['image']
                    );
                }

                if ($category['image']) {
                    $image = $category['image'];
                } else {
                    $image = 'placeholder.png';
                }
                // Level 1
                $data['categories'][] = array(
                    'name'     => $category['name'],
                    'children' => $children_data,
                    'column'   => $category['column'] ? $category['column'] : 1,
                    'href'     => $this->url->link('product/category', 'path=' . $category['category_id']),
                    'image'     => $image,
                );
            }
		// if (file_exists(DIR_TEMPLATE . 'old/template/common/home.tpl')) {
		// 	$this->response->setOutput($this->load->view('old/template/common/home.tpl', $data));

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}
