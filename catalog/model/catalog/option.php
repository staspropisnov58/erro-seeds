<?php
/**
* В первую очередь модель создана для того, чтобы уменьшить количество кода в ModelCatalogProduct
* Для реализации хитровыдуманной схемы продаж семян Errors Seeds нужно держать сразу 2 метода:
* getOptionsWithPreparedPrices — возвращает массив, с опциями и значениями подобный тому,
* который вернул бы родной ModelCatalogProduct::getProductOptions до 2.2
* getOptionsWithRawPrices — здесь цены вытягиваются из базы данных как есть, вся математика находится на витрине
*/
class ModelCatalogOption extends Model
{
  public function getFirstAvailableOptionsPrices(Price $product_price, int $product_id)
  {
    $prices = [];

    $product_option_query = $this->db->query("SELECT po.product_option_id, o.multiply FROM " . DB_PREFIX . "product_option po
                                              LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id)
                                              WHERE po.product_id = '" . (int)$product_id . "' AND required = 1 ORDER BY o.sort_order");

    if ($product_option_query->num_rows) {
      foreach ($product_option_query->rows as $product_option) {
        $product_option_value_query = $this->db->query("SELECT pov.price, pov.price_prefix, pov.product_option_value_id, pov.quantity FROM " . DB_PREFIX . "product_option_value pov
                                                        LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id)
                                                        WHERE pov.product_id = '" . (int)$product_id . "'
                                                        AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "'
                                                        AND pov.status = 1 AND pov.quantity > 0
                                                        ORDER BY ov.sort_order LIMIT 1");

        if ($product_option_value_query->row) {
          $option_value = $product_option_value_query->row;

          if ($product_option['multiply']) {
            $prices[] = $this->countMultiplyOptionValuePrice($option_value, $product_price, $product_id);
          } else {
            $prices[] = $this->countRegularOptionValuePrice($option_value, $product_price);
          }
        }
      }
    }

    return $prices;
  }

  public function getOptionsWithPreparedPrices(Price $product_price, int $product_id)
  {
    $options = $this->getOptions($product_id);

    $options_data = [];
    $options['required'] = 0;

    foreach ($options as $option) {
      $option_data[$option['product_option_id']] = $option;
      $option_value_data = [];

      foreach ($option['product_option_value'] as $option_value) {
        if ($option_value['subtract'] && $option_value['quantity'] <= 0) {
          continue;
        } else {
          if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            if ($option['multiply']) {
              $option_price = $this->countMultiplyOptionValuePrice($option_value, $product_price, $product_id);
            } else {
              $option_price = $this->countRegularOptionValuePrice($option_value, $product_price);
            }
          }

          $option_value_data[] = [
                      'product_option_value_id' => $option_value['product_option_value_id'],
                      'option_value_id'         => $option_value['option_value_id'],
                      'name'                    => $option_value['name'],
                      'quantity'              	=> $option_value['quantity'],
                      'price'                   => $this->currency->format($this->tax->calculate($option_price->getPrice(), 0, $this->config->get('config_tax')), '', '', false),
                      'old_price'								=> $this->currency->format($this->tax->calculate($option_price->getOldPrice(), 0, $this->config->get('config_tax')), '', '', false),
                      'price_prefix'            => $option_value['price_prefix'],
                    ];
        }
      }

      if ($option_value_data) {
        $options_data[] = [
          'product_option_id'    => $option['product_option_id'],
          'product_option_value' => $option_value_data,
          'option_id'            => $option['option_id'],
          'name'                 => $option['name'],
          'type'                 => $option['type'],
          'value'                => $option['value'],
          'required'             => $option['required'],
          'multiply'             => $option['multiply'],
        ];
        if ($option['required']) {
          $options_data['required'] = 1;
        }
      }

      return $options_data;
    }
  }

  public function getOptionsWithRawPrices(Price $product_price, int $product_id)
  {
    $options = $this->getOptions($product_id);

    $options_data = [];
    $options['required'] = 0;

    foreach ($options as $option) {
      $option_data[$option['product_option_id']] = $option;
      $option_value_data = [];

      foreach ($option['product_option_value'] as $option_value) {
        if ($option_value['subtract'] && $option_value['quantity'] <= 0) {
          continue;
        } else {
          if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            if ($option['multiply']) {
              $option_price = new Price($option_value['price']);
            } else {
              $option_price = $this->countRegularOptionValuePrice($option_value, $product_price);
            }
          }

          $image = [];
          $trigger_image = '';

          if ($option_value['image']) {
            $image['extension'] = pathinfo($option_value['image'], PATHINFO_EXTENSION);

            if($image['extension'] === 'svg') {
              $image['value'] = $this->model_tool_image->renderSVG($option_value['image']);
            } else {
              $image['value'] = $this->model_tool_image->resize($option_value['image'], 60, 36);
            }

            $trigger_image = pathinfo($option_value['image'], PATHINFO_FILENAME);
          }

          $option_value_data[] = [
                      'product_option_value_id' => $option_value['product_option_value_id'],
                      'option_value_id'         => $option_value['option_value_id'],
                      'name'                    => $option_value['name'],
                      'quantity'              	=> $option_value['quantity'],
                      'image'                   => $image,
                      'trigger_image'           => $trigger_image,
                      'price'                   => $this->currency->format($this->tax->calculate($option_price->getPrice(), 0, $this->config->get('config_tax')), '', '', false),
                      'old_price'								=> $this->currency->format($this->tax->calculate($option_price->getOldPrice(), 0, $this->config->get('config_tax')), '', '', false),
                      'price_prefix'            => $option_value['price_prefix'],
                      'multiply'                => $option['multiply'],
                    ];
        }
      }

      if ($option_value_data) {
        $options_data[] = [
          'product_option_id'    => $option['product_option_id'],
          'product_option_value' => $option_value_data,
          'option_id'            => $option['option_id'],
          'name'                 => $option['name'],
          'type'                 => $option['type'],
          'value'                => $option['value'],
          'required'             => $option['required'],
          'multiply'             => $option['multiply'],
        ];
        if ($option['required']) {
          $options_data['required'] = 1;
        }
      }

      return $options_data;
    }
  }

  private function getOptions($product_id)
  {
    $product_option_data = [];

    $product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");

    foreach ($product_option_query->rows as $product_option) {
      $product_option_value_data = [];

      $product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pov.status = 1 ORDER BY ov.sort_order");

      foreach ($product_option_value_query->rows as $product_option_value) {
        $product_option_value_data[] = $product_option_value;
      }

      if ($product_option_value_data) {
        $product_option_data[$product_option['product_option_id']] = $product_option;
        $product_option_data[$product_option['product_option_id']]['product_option_value'] = $product_option_value_data;
      }
    }

    return $product_option_data;
  }

  private function getOptionSpecial($product_option_value_id, $special_id)
  {
    $query = $this->db->query("SELECT price, special_id FROM " . DB_PREFIX . "product_option_special WHERE product_option_value_id='" . $product_option_value_id . "' AND special_id='" . (int)$special_id . "'");

    return $query->row;
  }

  private function countRegularOptionValuePrice(array $option_value, Price $product_price)
  {
    if ($product_price->getSpecialId()) {
      $option_special = $this->getOptionSpecial($option_value['product_option_value_id'], $product_price->getSpecialId());

      //Здесь проверяем, что скидка на опцию не равна нулю,
      // что полностью отключает скидку на товар при выборе данной опции
      if ($option_special && (float)$option_special['price'] <= 0) {
        //Чтобы цена отображалась коррекно, добавляем к цене на опцию разницу между основной ценой и скидкой товара
        $option_value['price'] += $product_price->getOldPrice() - $product_price->getPrice();
      }
    } else {
      $option_special = null;
    }

    $option_price = new Price((float)($option_value['price_prefix'] . $option_value['price']));
    $option_price->setSpecial($option_special);

    return $option_price;
  }

  private function countMultiplyOptionValuePrice(array $option_value, Price $product_price, int $product_id)
  {
    $option_price = new Price($product_price->getPrice());
    $option_price->setQuantity((int)$option_value['quantity']);

    $discounts = $this->model_catalog_product->getProductDiscounts($product_id);
    foreach ($discounts as $discount) {
      if ($discount['quantity'] === $option_value['quantity']) {
        $option_special = (($product_price->getPrice() - (float)$discount['price']) + ((int)$option_value['quantity'] - 1) * (float)$discount['price']) / ((int)$option_value['quantity'] - 1);
        $option_price->setSpecial(['price' => $option_special, 'special_id' => 0]);
        break;
      }
    }

    $option_price->setQuantity((int)$option_value['quantity'] - 1);

    return $option_price;
  }
}
