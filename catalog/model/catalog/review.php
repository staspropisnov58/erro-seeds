<?php
class ModelCatalogReview extends Model {
	public function addReview($data) {
		$this->event->trigger('pre.review.add', $data);

		if (isset($data['news_id'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "review_for_news SET author = '" . $this->db->escape($data['firstname']) . "', customer_id = '" . (int)$this->customer->getId() . "', email= '". trim($this->db->escape($data['email'])) ."', product_id = '" . (int)$data['news_id'] . "', text = '" . $this->db->escape($data['comment']) . "', rating = '" . (int)$data['rating'] . "', date_added = NOW()");
		} elseif (isset($data['product_id'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "review SET author = '" . $this->db->escape($data['firstname']) . "', customer_id = '" . (int)$this->customer->getId() . "', email= '". trim($this->db->escape($data['email'])) ."', product_id = '" . (int)$data['product_id'] . "', text = '" . $this->db->escape($data['comment']) . "', rating = '" . (int)$data['rating'] . "', date_added = NOW()");
		}


		$review_id = $this->db->getLastId();

		if ($this->config->get('config_review_mail')) {
			$this->load->language('mail/review');

			$message = '#Отзыв номер ' . $this->db->getLastId() . "\n" . $this->language->get('text_waiting') . "\n";

			if (isset($data['news_id'])) {
				$subject = sprintf($this->language->get('text_subject_news'), $this->config->get('config_name'))  . ' #ua #blog';

				$this->load->model('extension/news');
				$news = $this->model_extension_news->getNews($this->request->get['news_id']);

				$message .= sprintf($this->language->get('text_news'), $this->db->escape(strip_tags($news['title']))) . "\n";
			} else {
				if ($data['product_id']) {
					$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name')) . ' #ua #product';
					$this->load->model('catalog/product');

					$product_info = $this->model_catalog_product->getProduct($data['product_id']);
					$message .= sprintf($this->language->get('text_product'), $this->db->escape(strip_tags($product_info['name']))) . "\n";
				} else {
						$subject = sprintf($this->language->get('text_subject_store'), $this->config->get('config_name')) . ' #ua #shop';
					$message .= $this->language->get('text_store') . "\n";
				}
			}

			$message .= sprintf($this->language->get('text_reviewer'), $this->db->escape(strip_tags($data['firstname']))) . "\n";
			$message .= sprintf($this->language->get('text_rating'), $this->db->escape(strip_tags($data['rating']))) . "\n\n";
			$message .= '##' . $this->language->get('text_review') . "\n";
			$message .= $this->db->escape(strip_tags($data['comment'])) . "\n\n";

			$mail_config = $this->config->get('config_mail');
			$mail = new Mail($mail_config);
			$mail->setTo('seoboost+hbyltk9eqy4wdq7ypgde@boards.trello.com');
			if ($mail_config['smtp_username']) {
				$mail->setFrom($mail_config['smtp_username']);
				$mail->setReplyTo($this->request->post['email']);
			} else {
				$mail->setFrom($this->request->post['email']);
			}
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject($subject);
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}

		$this->event->trigger('post.review.add', $review_id);
	}

	public function getReviewsByProductId($product_id, $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 100;
		}

		$query = $this->db->query("SELECT r.review_id, r.author, r.title, r.rating, r.text, r.date_added FROM " . DB_PREFIX . "review r WHERE r.product_id = '" . (int)$product_id . "' AND r.status = '1' AND r.rew_id = 0 ORDER BY r.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalReviewsByProductId($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r LEFT JOIN " . DB_PREFIX . "product p ON (r.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND p.date_available <= NOW() AND p.status = '1' AND r.status = '1' AND r.rew_id = 0 AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row['total'];
	}

    public function updateLike($review_id,$like) {
	    if ($like == 1){
            $this->db->query("UPDATE " . DB_PREFIX . "review SET clike = (clike + 1) WHERE review_id = '" . (int)$review_id . "'");
        }else{
            $this->db->query("UPDATE " . DB_PREFIX . "review SET cdislike = (cdislike + 1) WHERE review_id = '" . (int)$review_id . "'");
        }

    }
		public function getReview($review_id) {
			$query = $this->db->query("SELECT DISTINCT *, (SELECT pd.name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = r.product_id AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS product FROM " . DB_PREFIX . "review r WHERE r.review_id = '" . (int)$review_id . "'");

			return $query->row;
		}


}
