<?php
class ModelTotalCommission extends Model
{
  public function getTotal(&$total_data, &$total, &$taxes) {
    if (isset($this->session->data['payment_method']) && $this->session->data['payment_method']['commission']) {
      $commission = 0;
      if ($this->config->get($this->session->data['payment_method']['code'] . '_type') === 'F') {
        $commission = $this->config->get($this->session->data['payment_method']['code'] . '_payment_commission');
      } else {
        $commission = (int)$total / 100 * (int)$this->config->get($this->session->data['payment_method']['code'] . '_payment_commission');
      }
      $total_data['commission'] = array(
        'code'       => 'commission',
        'title'      => sprintf($this->language->get('text_commission'), $this->session->data['payment_method']['title']),
        'value'      => $commission,
        'sort_order' => $this->config->get('commission_sort_order')
      );

      $total += $commission;
    }
  }
}
