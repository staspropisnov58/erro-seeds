<?php
class ModelTotalBonuse extends Model
{
    public function getTotal(&$total_data, &$total, &$taxes) {
if(isset($this->session->data['sale_bonuse'])&&$this->session->data['sale_bonuse']>0){
    //$bonuse =$this->customer->getUserBonus();
    $bonuse =$this->session->data['sale_bonuse'];

    $total_data['bonuse'] = array(
        'code'       => 'bonuse',
        'title'      => 'Списание бонусов',
        'value'      => $this->session->data['sale_bonuse'],
        'sort_order' => $this->config->get('bonuse_sort_order')
    );
    $total -= $bonuse;
}

    }
}