<?php
class ModelTotalDiscount extends Model
{
  public function getTotal(&$total_data, &$total, &$taxes) {
    if (isset($this->session->data['discount'])) {
      $total_data['discount'] = array(
        'code'       => 'discount',
        'title'      => 'Скидка',
        'value'      => - ($total - $this->session->data['discount']),
        'sort_order' => 8,
      );

      $total = $this->session->data['discount'];
    }
  }
}
