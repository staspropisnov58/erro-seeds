<?php
class ModelTotalShipping extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		if ($this->cart->hasShipping() && isset($this->session->data['shipping_method'])) {
			$total_data['shipping'] = array(
				'code'       => 'shipping',
				'title'      => $this->session->data['shipping_method']['title'],
				'value'      => $this->session->data['shipping_method']['cost'],
				'sort_order' => $this->config->get('shipping_sort_order')
			);

			$shipping_cost = $this->session->data['shipping_method']['cost'];

			if (isset($this->session->data['order']['additions']) && $this->session->data['order']['additions']) {
				foreach ($this->session->data['order']['additions'] as $addition_id => $product_id) {
					$shipping_cost += (int)$this->session->data['shipping_method']['additions'][$addition_id]['products'][$product_id]['raw_cost'];
					$total_data['addition'] = array(
						'code'       => 'addition',
						'title'      => $this->session->data['shipping_method']['additions'][$addition_id]['products'][$product_id]['name'],
						'value'      => $this->session->data['shipping_method']['additions'][$addition_id]['products'][$product_id]['raw_cost'],
						'remove'     => $this->url->link('checkout/checkout/remove_addition', '&addition_id=' . $addition_id, true),
						'sort_order' => $this->config->get('shipping_sort_order')
					);
				}
			}

			if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
				$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($taxes[$tax_rate['tax_rate_id']])) {
						$taxes[$tax_rate['tax_rate_id']] = $tax_rate['amount'];
					} else {
						$taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
					}
				}
			}

			$total += $shipping_cost;
		}
	}
}
