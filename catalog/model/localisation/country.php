<?php
class ModelLocalisationCountry extends Model {
	public function getCountry($country_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND status = '1'");

		return $query->row;
	}

	public function getCountryTelephoneCode($country_id){
		$query = $this->db->query("SELECT tel_code FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "'" );

		return $query->row['tel_code'];
	}

	public function getCountries() {
		$country_data = $this->cache->get('country.status');

		if (!$country_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE status = '1' ORDER BY sort_order ASC, name ASC");

			$country_data = $query->rows;
			foreach ($country_data as &$country) {
				$country['image'] = '/catalog/view/theme/' . $this->config->get('config_template') . '/images/flags/' .  strtolower(strtolower($country['iso_code_2'])) . '.png';
			}
			$this->cache->set('country.status', $country_data);
		}

		return $country_data;
	}
	public function getConfigCounryCode(){
			$query = $this->db->query("SELECT tel_code FROM " . DB_PREFIX . "country WHERE country_id = '" . $this->config->get('config_country_id') . "'");

			return $query->row['tel_code'];
	}
}
