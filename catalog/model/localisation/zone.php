<?php
class ModelLocalisationZone extends Model {
	public function getZone($zone_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND status = '1'");

		return $query->row;
	}

	public function getZonesByCountryId($country_id) {
		$zone_data = $this->cache->get('zone.' . (int)$country_id);

		if (!$zone_data) {
			$sql = "SELECT z.zone_id, z.country_id, z.code, z.status, zd.name, zd.language_id FROM " . DB_PREFIX . "zone z LEFT JOIN " . DB_PREFIX . "zone_description zd ON(z.zone_id = zd.zone_id) WHERE country_id = '" . (int)$country_id . "' AND status = '1' AND zd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY zd.name ";

			$query = $this->db->query($sql);

			$zone_data = $query->rows;

			$this->cache->set('zone.' . (int)$country_id, $zone_data);
		}

		return $zone_data;
	}
}
