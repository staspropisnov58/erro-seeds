<?php
class ModelAffiliateTransaction extends Model {
	public function getTransactions($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "affiliate_transaction` WHERE affiliate_id = '" . (int)$this->affiliate->getId() . "'";

		$sort_data = array(
			'amount',
			'description',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date_added";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getPayouts($data = array()){
			$sql = "SELECT GROUP_CONCAT(description SEPARATOR ',') AS description, SUM(amount) AS amount, date_payout AS date_payout FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . $data['affiliate_id'] ."' AND date_payout !='0000-00-00 00:00:00' GROUP BY date_payout";

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 10;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			$query = $this->db->query($sql);

			return $query->rows;
	}

	public function	getPayoutsTotal($affiliate_id){
		$query = $this->db->query("SELECT COUNT(DISTINCT date_payout)  FROM " . DB_PREFIX . "affiliate_transaction WHERE affiliate_id = '" . $affiliate_id ."' AND date_payout !='0000-00-00 00:00:00'");

		return $query->rows;
	}


	public function getTotalTransactions() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "affiliate_transaction` WHERE affiliate_id = '" . (int)$this->affiliate->getId() . "'");

		return $query->row['total'];
	}

	public function getBalance($affiliate_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "affiliate_transaction WHERE date_payout != '0000-00-00 00:00:00' AND affiliate_id = '" . (int)$affiliate_id . "'");

		if ($query->num_rows) {
			return $query->row['total'];
		} else {
			return 0;
		}
	}
	 public function getBonuses($affiliate_id, $start = false, $limit = false){




		 				if ($start < 0) {
		 					$start = 0;
		 				}

		 				if ($limit < 1) {
		 					$limit = 10;
		 				}
							$sql = "SELECT * FROM " . DB_PREFIX . "affiliate_transaction at
							INNER JOIN " . DB_PREFIX . "order o ON at.order_id = o.order_id
							WHERE at.affiliate_id = '" . (int)$affiliate_id . "' AND at.date_payout = '0000-00-00 00:00:00' AND o.order_status_id IN(" . implode(',', $this->config->get('config_complete_status')) . ") ORDER BY at.date_added DESC";

							if ($limit >= 10){

		 					$sql .= " LIMIT " . (int)$start . "," . (int)$limit;
						}
		 		$query = $this->db->query($sql);

		 		return $query->rows;
	 }

	 public function getTotalCountBonuses($affiliate_id){

		 $query =	$this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "affiliate_transaction at
		 INNER JOIN " . DB_PREFIX . "order o ON at.order_id = o.order_id
		 WHERE at.affiliate_id = '" . (int)$affiliate_id . "' AND at.date_payout = '0000-00-00 00:00:00' AND o.order_status_id IN(" . implode(',', $this->config->get('config_complete_status')) . ")");

		 return $query->row;

	 }

	 public function getBalanseBonuses($affiliate_id){
		 $query =	$this->db->query("SELECT SUM(amount) FROM " . DB_PREFIX . "affiliate_transaction at
		 INNER JOIN " . DB_PREFIX . "order o ON at.order_id = o.order_id
		 WHERE at.affiliate_id = '" . (int)$affiliate_id . "' AND at.date_payout = '0000-00-00 00:00:00' AND o.order_status_id IN(" . implode(',', $this->config->get('config_complete_status')) . ")");

		 return $query->row;

	 }


    public function countAffiliateOrders($affiliate_id, $order_status_id){
		$query = $this->db->query("SELECT COUNT(*) FROM " . DB_PREFIX . "order INNER JOIN " . DB_PREFIX . "order_status ON oc_order.order_status_id=" . DB_PREFIX . "order_status.order_status_id WHERE " . DB_PREFIX . "order.affiliate_id ='" . $affiliate_id ."' AND " . DB_PREFIX . "order_status.order_status_id NOT IN(". $order_status_id .") AND " . DB_PREFIX . "order_status.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->rows;

		}
	}
