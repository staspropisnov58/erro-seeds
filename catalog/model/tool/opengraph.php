<?php
class ModelToolOpengraph extends Model {

  public function addOpengraphForProduct($product){
    //opengraph

    $this->load->model('tool/image');

    $opengraph = new Opengraph($this->config->get('config_name'), $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height'));

    $opengraph->setUrl($this->url->link('product/product','&product_id=' . $product['product_id']));

    if($product['og_title'] !== ''){
      $og_title = $product['og_title'];
      $opengraph->setTitle($product['og_title']);
    }elseif(isset($product['meta_title']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $og_title = $product['meta_title'];
      $opengraph->setTitle($product['meta_title']);
    }else{
      $og_title = $this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title'];
      $opengraph->setTitle($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title']);
    }

    if($product['og_description'] !== ''){
      $opengraph->setDescription($product['og_description']);
    }elseif(isset($product['meta_description']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $opengraph->setDescription($product['meta_description']);
    }else{
      $opengraph->setDescription($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['description']);
    }


    if($product['og_image'] !== ''){
      $type = pathinfo($product['og_image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($product['og_image'], $this->config->get('config_image_thumb_height'), $this->config->get('config_image_thumb_width')));
    }elseif(isset($product['image']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $type = pathinfo($product['image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($product['image'], $this->config->get('config_image_thumb_height'), $this->config->get('config_image_thumb_width')));
    }
    $opengraph->SetImageWidth($this->config->get('config_image_thumb_width'));
    $opengraph->SetImageHeight($this->config->get('config_image_thumb_height'));

    if(isset($type)){
      $opengraph->SetImageType($type);
    }

    if(isset($og_title)){
      $opengraph->SetImageAlt($og_title);
    }

    $this->document->addOpengraph($opengraph->getData());
  }

  public function addOpengraphForInformation($information){

    $this->load->model('tool/image');

    $opengraph = new Opengraph($this->config->get('config_name'), $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height'));
   //   $this->log->write(print_r($information['information_id'],true));
  //  $opengraph->setUrl($this->url->link('information/information', 'information_id=' . $$information['information_id']));
    $opengraph->setUrl($this->url->link('information/information', 'information_id=' . $information['information_id']));

    if($information['og_title'] !== ''){
      $og_title = $information['og_title'];
      $opengraph->setTitle($information['og_title']);
    }elseif(isset($information['meta_title']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $og_title = $information['meta_title'];
      $opengraph->setTitle($information['meta_title']);
    }else{
      $og_title = $this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title'];
      $opengraph->setTitle($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title']);
    }

    if($information['og_description'] !== ''){
      $opengraph->setDescription($information['og_description']);
    }elseif(isset($information['meta_description']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $opengraph->setDescription($information['meta_description']);
    }else{
      $opengraph->setDescription($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['description']);
    }


    if($information['og_image'] !== ''){
      $type = pathinfo($information['og_image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($information['og_image'], $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }else{
      $type = pathinfo($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'],$this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }
    $opengraph->SetImageWidth($this->config->get('opengraph_image_width'));
    $opengraph->SetImageHeight($this->config->get('opengraph_image_height'));

    if(isset($type)){
      $opengraph->SetImageType($type);
    }

    if(isset($og_title)){
      $opengraph->SetImageAlt($og_title);
    }

    $this->document->addOpengraph($opengraph->getData());
  }

  public function addOpengraphForPage($route){
    $this->load->model('tool/image');

    // Opengraph

    $opengraph = new Opengraph($this->config->get('config_name'), $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height'));


    $this->load->model('tool/image');
    $this->load->model('module/opengraph');

    $opengraph->setUrl($this->url->link($route));

    $page_result = $this->model_module_opengraph->getOpengraphData($route);

    if(isset($page_result['og_title']) && $page_result['og_title'] !== ''){
      $og_title = $page_result['og_title'];
      $opengraph->setTitle($og_title);
    }else{
      $og_title = $this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title'];
      $opengraph->setTitle($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title']);
    }

    if(isset($page_result['og_description']) && $page_result['og_description'] !== ''){
      $og_description = $page_result['og_description'];
      $opengraph->setDescription($og_description);
    }else{
      $og_description = $this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['description'];
      $opengraph->setDescription($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title']);
    }

    if(isset($page_result['og_image']) && $page_result['og_image'] !== ''){
      $type = pathinfo($page_result['og_image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($page_result['og_image'], $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }else{

      $type = pathinfo($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'],$this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }

    $opengraph->SetImageWidth($this->config->get('opengraph_image_width'));
    $opengraph->SetImageHeight($this->config->get('opengraph_image_height'));

    if(isset($type)){
      $opengraph->SetImageType($type);
    }

    if(isset($og_title)){
      $opengraph->SetImageAlt($og_title);
    }

    $this->document->addOpengraph($opengraph->getData());

  }

  public function addOpengraphForNews($news){
    $this->load->model('tool/image');

    //opengraph

      $opengraph = new Opengraph($this->config->get('config_name'), $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height'));

    //  $opengraph->setUrl($this->url->link('information/blog', 'article_id=' . $$news['article_id']));
      if(isset($news['article_id'])){
          $opengraph->setUrl($this->url->link('information/blog', 'article_id=' . $news['article_id']));
      }


      if($news['og_title'] !== ''){
        $og_title = $news['og_title'];
        $opengraph->setTitle($news['og_title']);
      }elseif(isset($news['meta_title']) && $this->config->get('opengraph_use_meta_when_no_og')){
        $og_title = $news['meta_title'];
        $opengraph->setTitle($news['meta_title']);
      }else{
        $og_title = $this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title'];
        $opengraph->setTitle($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title']);
      }

      if($news['og_description'] !== ''){
        $opengraph->setDescription($news['og_description']);
      }elseif(isset($news['meta_description']) && $this->config->get('opengraph_use_meta_when_no_og')){
        $opengraph->setDescription($news['meta_description']);
      }else{
        $opengraph->setDescription($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['description']);
      }


      if($news['og_image'] !== ''){
        $type = pathinfo($news['og_image'])['extension'];
        $opengraph->SetImage($this->model_tool_image->resize($news['og_image'], $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
      }else{
        $type = pathinfo($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'])['extension'];
        $opengraph->SetImage($this->model_tool_image->resize($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'],$this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
      }
      $opengraph->SetImageWidth($this->config->get('opengraph_image_width'));
      $opengraph->SetImageHeight($this->config->get('opengraph_image_height'));

      if(isset($type)){
        $opengraph->SetImageType($type);
      }

      if(isset($og_title)){
        $opengraph->SetImageAlt($og_title);
      }

      $this->document->addOpengraph($opengraph->getData());
  }

  public function addOpengraphForCategory($category, $path){
    $this->load->model('tool/image');

    $opengraph = new Opengraph($this->config->get('config_name'), $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height'));


    $opengraph->setUrl($this->url->link('product/category','&path=' . $path));

    if($category['og_title'] !== ''){
      $og_title = $category['og_title'];
      $opengraph->setTitle($category['og_title']);
    }elseif(isset($category['meta_title']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $og_title = $category['meta_title'];
      $opengraph->setTitle($category['meta_title']);
    }else{
      $og_title = $this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title'];
      $opengraph->setTitle($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title']);
    }

    if($category['og_description'] !== ''){
      $opengraph->setDescription($category['og_description']);
    }elseif(isset($category['meta_description']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $opengraph->setDescription($category['meta_description']);
    }else{
      $opengraph->setDescription($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['description']);
    }


    if($category['og_image'] !== ''){
      $type = pathinfo($category['og_image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($category['og_image'], $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }else{
      $type = pathinfo($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'],$this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }
    $opengraph->SetImageWidth($this->config->get('opengraph_image_width'));
    $opengraph->SetImageHeight($this->config->get('opengraph_image_height'));

    if(isset($type)){
      $opengraph->SetImageType($type);
    }

    if(isset($og_title)){
      $opengraph->SetImageAlt($og_title);
    }

    $this->document->addOpengraph($opengraph->getData());
  }

  public function addOpengraphForManufacturer($manufacturer){
    // opengraph

    $this->load->model('tool/image');

    $opengraph = new Opengraph($this->config->get('config_name'), $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height'));

    $opengraph->setUrl($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']));

    if($manufacturer['og_title'] !== ''){
      $og_title = $manufacturer['og_title'];
      $opengraph->setTitle($manufacturer['og_title']);
    }elseif(isset($manufacturer['meta_title']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $og_title = $manufacturer['meta_title'];
      $opengraph->setTitle($manufacturer['meta_title']);
    }else{
      $og_title = $this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title'];
      $opengraph->setTitle($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['title']);
    }

    if($manufacturer['og_description'] !== ''){
      $opengraph->setDescription($manufacturer['og_description']);
    }elseif(isset($manufacturer['meta_description']) && $this->config->get('opengraph_use_meta_when_no_og')){
      $opengraph->setDescription($manufacturer['meta_description']);
    }else{
      $opengraph->setDescription($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['description']);
    }


    if($manufacturer['og_image'] !== ''){
      $type = pathinfo($manufacturer['og_image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($manufacturer['og_image'], $this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }else{
      $type = pathinfo($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'])['extension'];
      $opengraph->SetImage($this->model_tool_image->resize($this->config->get('og_homepage')['page_description'][$this->config->get('config_language_id')]['image'],$this->config->get('opengraph_image_width'), $this->config->get('opengraph_image_height')));
    }
    $opengraph->SetImageWidth($this->config->get('opengraph_image_width'));
    $opengraph->SetImageHeight($this->config->get('opengraph_image_height'));

    if(isset($type)){
      $opengraph->SetImageType($type);
    }

    if(isset($og_title)){
      $opengraph->SetImageAlt($og_title);
    }

    $this->document->addOpengraph($opengraph->getData());

  }

}
