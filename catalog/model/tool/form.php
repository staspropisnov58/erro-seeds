<?php
class ModelToolForm extends Model
{
  private $name = '';
  private $action = '';
  private $countries = array();
  private $fields = array();

  public function init($name, $action)
  {
  }

  public function addField($field_data)
  {
    $this->fields[$field_data['name']] = $field_data;
  }

  public function build()
  {
    $form = array();

    $form['countries'] = $this->countries;
    $form['fields'] = $this->fields;
  }
}
