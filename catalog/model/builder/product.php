<?php
class ModelBuilderProduct extends Model
{
  public function buildForAddToCartMessage($product)
  {
    $builded_product = [];

    $builded_product['name'] = $product['name'];
    $builded_product['quantity'] = $product['quantity'];
    $builded_product['manufacturer'] = $product['manufacturer'];
    $builded_product['price'] = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), '', '', false);

    if ($product['image']) {
      $builded_product['image'] = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
    } else {
      $builded_product['image'] = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
    }

    $builded_product['option'] = $product['option'];
    $builded_product['sku'] = '';


    return $builded_product;
  }

  public function buildForEcommerce($product)
  {
    $bulded_product = [];

    $this->load->model('catalog/category');

    $builded_product['id'] = $product['product_id'];
    $builded_product['name'] = $product['name'];
    $builded_product['quantity'] = $product['quantity'];

    $main_category = $this->model_catalog_category->getMainCategory($product['product_id']);
    $builded_product['category'] = $main_category['category_path_name'];

    $builded_product['brand'] = $product['manufacturer'];


    $builded_product['variant'] = '';

    foreach ($product['option'] as $option) {
      if ($option['type'] != 'file') {
        $value = $option['value'];
      } else {
        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

        if ($upload_info) {
          $value = $upload_info['name'];
        } else {
          $value = '';
        }
      }

      $builded_product['variant'] .= $option['name'] . ': ' . $value . '; ';
    }

    $builded_product['price'] = '';

    return $builded_product;
  }
}
