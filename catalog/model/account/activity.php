<?php

class ModelAccountActivity extends Model
{
    public function addActivity($key, $data)
    {
        if (isset($data['customer_id'])) {
            $customer_id = $data['customer_id'];
        } else {
            $customer_id = 0;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "customer_activity` SET `customer_id` = '" . (int)$customer_id . "', `key` = '" . $this->db->escape($key) . "', `data` = '" . $this->db->escape(serialize($data)) . "', `ip` = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', `date_added` = NOW()");
    }

    public function saleBonus($customer, $bonuse)
    {

	   $query = $this->db->query("SELECT `custom_bonus` FROM `oc_customer` WHERE `customer_id`=".$customer." ");
	   $last_bonus = ($query->row['custom_bonus'] - $bonuse);
        $this->db->query("UPDATE `oc_customer` set custom_bonus = ".$last_bonus." WHERE `customer_id`=".$customer." ");

	    return $last_bonus;
    }
}