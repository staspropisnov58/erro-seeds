<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data) {
		$this->event->trigger('pre.customer.add', $data);

		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$customer_query = "INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? serialize($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()";

		$this->load->model('module/register_setting');

		$fields = $this->model_module_register_setting->getAllRegisterFields();

		foreach ($fields as $field) {
			if ($field['field_status']){
				$customer_query .= ", " . $field['field_name'] ." = '" . $this->db->escape($data[$field['field_name']]) . "'";
				if ($field['field_name'] === 'telephone' && $this->config->get('enebled_tel_code')) {
					$customer_query .= ", telephone_country_id = '" . (int)$data['telephone_country_id'] . "'";
				}
			} else {
				$customer_query .= ", " . $field['field_name'] ." = ''";
			}
		}

		$this->db->query($customer_query);

		$customer_id = $this->db->getLastId();

		if (isset($data['affiliate'])){
			$this->load->model('module/customer_is_affiliate');
			$affiliate_id = $this->model_module_customer_is_affiliate->addAffiliate($customer_id);
		}


		$this->addDefaultSettingNotifications($customer_id);

		$this->event->trigger('post.customer.add', $customer_id);

		return $customer_id;
	}

			public function getCustomersLogin(){
				$query = $this->db->query("SELECT login FROM " . DB_PREFIX . "customer");

				return $query->rows;
			}


			public function addDefaultSettingNotifications($customer_id){
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_sms_notification WHERE customer_id='" . (int) $customer_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "customer_email_notification WHERE customer_id='" . (int) $customer_id . "'");

				$this->db->query("INSERT INTO " . DB_PREFIX . "customer_sms_notification (customer_id) VALUES (" . (int)$customer_id . ")");
				$this->db->query("INSERT INTO " . DB_PREFIX . "customer_email_notification (customer_id) VALUES ('" . (int)$customer_id . "')");
			}

			public function addCustomerLanguage($language_code, $customer_id){
				 $language_id = $this->db->query("SELECT language_id FROM " . DB_PREFIX . "language WHERE code='" . $language_code . "'");
				 $language_id = $language_id->row['language_id'];

				$this->db->query("UPDATE " . DB_PREFIX . "customer SET language_id = '" . (int)$language_id . "' WHERE customer_id= '" . (int)$customer_id . "'");
			}

			public function sendCustomerEmail($string){

				$this->load->language('mail/customer');

				// $subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

				$customer = $this->model_account_customer->getCustomer($this->customer->getId());

				$email = $this->customer->getEmail();
				$telephone = $this->customer->getTelephone();


				$data['store_url']      = $this->config->get('config_url');

				$data['text_greeting']  = $this->language->get('text_greeting') . "\n\n";




				if($string == 'customer'){

					$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

					$data['text_main'][] = $this->language->get('text_welcome');
					$data['text_main'][] = $this->language->get('text_services');
					$data['text_main'][] = sprintf($this->language->get('text_link'), $this->url->link('account/newsletter')). "\n\n";

				}elseif($string == 'customer&affiliate'){
					$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					$data['text_main'][] = $this->language->get('text_welcome');
					$data['text_main'][] = $this->language->get('text_services');
					$data['text_main'][] = sprintf($this->language->get('text_welcome_affiliate'),  html_entity_decode($this->config->get('config_url')), html_entity_decode($this->config->get('config_name'))) . "\n\n";
					$data['text_main'][] = $this->language->get('text_services_affiliate'). "\n\n";
					$data['text_main'][] = $this->language->get('text_services_affiliate_x2'). "\n\n";
				}elseif($string == 'affiliate') {
					$subject = sprintf($this->language->get('text_affiliate_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					$data['text_main'][] = sprintf($this->language->get('text_welcome_affiliate'), html_entity_decode($this->config->get('config_url')), html_entity_decode($this->config->get('config_name'))) . "\n\n";
					$data['text_main'][] = $this->language->get('text_services_affiliate'). "\n\n";
				}

				$data['text_note'] = sprintf($this->language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/message.tpl')) {
					$html = $this->load->view($this->config->get('config_template') . '/template/mail/message.tpl', $data);
				} else {
					$html = $this->load->view('default/template/mail/order.tpl', $data);
				}


				$mail_config = $this->config->get('config_mail');
				$mail = new Mail($mail_config);
				$mail->setTo($email);
				if ($mail_config['smtp_username']) {
					$mail->setFrom($mail_config['smtp_username']);
					$mail->setReplyTo($this->config->get('config_email'));
				} else {
					$mail->setFrom($this->config->get('config_email'));
				}
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject($subject);
				$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
				$mail->send();


		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . $this->config->get('config_name') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $firstname . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $email . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $telephone . "\n";

			$mail->setTo($this->config->get('config_email'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_mail_alert'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

	}

	public function getEmailNotifications($customer_id){
			$query = $this->db->query("SELECT  new_order, edit_order, review_moderation, new_review_reply, affiliate_order, affiliate_bonus, affiliate_payout FROM " . DB_PREFIX . "customer_email_notification WHERE customer_id='" .$customer_id. "'");

		return $query->rows;
	}

	public function getAffiliateIdByLogin($login){
		$query = $this->db->query("SELECT affiliate_id FROM " . DB_PREFIX . "customer WHERE login = '" . $this->db->escape(utf8_strtolower($login)) ."'");

		return $query->row['affiliate_id'];
	}

	public function getSmsNotifications($customer_id){
		$query = $this->db->query("SELECT new_order, edit_order, review_moderation, new_review_reply, affiliate_order, affiliate_bonus, affiliate_payout FROM " . DB_PREFIX . "customer_sms_notification WHERE customer_id='" .$customer_id. "'");

		return $query->rows;
	}

public function editEmailNotifications($email = array(), $customer_id){
	$this->db->query("UPDATE " . DB_PREFIX . "customer_email_notification SET affiliate_order='" . (int)$email['email_affiliate_order'] ."', affiliate_bonus= '" . (int)$email['email_affiliate_bonus'] ."', review_moderation='" . (int)$email['email_review_moderation'] ."', affiliate_payout='" . (int)$email['email_affiliate_payout'] ."', new_order='" . (int)$email['email_new_order'] . "', edit_order= '" . (int)$email['email_edit_order'] . "', new_review_reply='" . (int)$email['email_new_review_reply'] . "' WHERE customer_id= '" . $customer_id ."'");
}

public function editSmsNotifications($sms = array(), $customer_id){
	$this->db->query("UPDATE " . DB_PREFIX . "customer_sms_notification SET affiliate_order='" . (int)$sms['sms_affiliate_order'] ."', affiliate_bonus= '" . (int)$sms['sms_affiliate_bonus'] ."', affiliate_payout='" . (int)$sms['sms_affiliate_payout'] ."', review_moderation='" . (int)$sms['sms_review_moderation'] . "', new_order='" . (int)$sms['sms_new_order'] . "', edit_order= '" . (int)$sms['sms_edit_order'] . "', new_review_reply='" . (int)$sms['sms_new_review_reply'] . "' WHERE customer_id= '" . $customer_id ."'");
}

	public function editCustomer($data) {
		$this->event->trigger('pre.customer.edit', $data);

		$customer_id = $this->customer->getId();

		$sql = "UPDATE " . DB_PREFIX . "customer SET ";

		$this->load->model('module/register_setting');

		$fields = $this->model_module_register_setting->getCustomerEditFields();

		foreach ($fields as $field) {
			$sql_parts[] = $field['name'] ." = '" . $this->db->escape($data[$field['name']]) . "'";

			if ($field['name'] === 'telephone' && $this->config->get('enebled_tel_code')) {
				$sql_parts[] = "telephone_country_id = '" . (int)$data['telephone_country_id'] . "'";
			}
		}

		$sql .= implode(', ', $sql_parts);

		$sql .= " WHERE customer_id = '" . (int)$customer_id . "'";


		$this->db->query($sql);

		$this->event->trigger('post.customer.edit', $customer_id);
	}

	public function editPassword($email, $password) {
		$this->event->trigger('pre.customer.edit.password');

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		$this->event->trigger('post.customer.edit.password', $email);
	}

	public function editNewsletter($newsletter) {
		$this->event->trigger('pre.customer.edit.newsletter');

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		$this->event->trigger('post.customer.edit.newsletter');
	}

	public function getCustomer($customer_id) {
		if ($customer_id) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

			if ((int)$query->row['telephone_country_id'] === 0) {
				$query->row['telephone'] = $this->editTelephone($query->row['telephone']);
			}

			return $query->row;
		}
	}

	public function editTelephone($telephone) {
		$this->load->model('localisation/country');
		$config_country_code = $this->model_localisation_country->getConfigCounryCode();


		if (strlen($telephone) > strlen(str_replace($config_country_code, '', $telephone))) {
			 $this->db->query("UPDATE " . DB_PREFIX . "customer
												SET telephone_country_id = '" . $this->config->get('config_country_id') . "', telephone = '" . $this->db->escape(str_replace($config_country_code, '', $telephone)) . "'
												WHERE customer_id = '" . $this->customer->getId() . "'");

			$telephone = str_replace($config_country_code, '', $telephone);
		} else {
			$this->load->language('common/errors');
			$this->session->data['warning'] = $this->language->get('warning_telephone_and_code');
		}

		return $telephone;
	}

	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCustomerByLogin($login) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(login) = '" . $this->db->escape(utf8_strtolower($login)) . "'");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

		return $query->row;
	}

	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}

	public function getCustomersByLogin($login) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE login = '" . $this->db->escape(utf8_strtolower($login)) . "'");

		return $query->row['login'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	public function isBanIp($ip) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ban_ip` WHERE ip = '" . $this->db->escape($ip) . "'");

		return $query->num_rows;
	}

	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCustomerEmailByLogin($login){
		$query = $this->db->query("SELECT email FROM " . DB_PREFIX . "customer WHERE login = '" . $this->db->escape($login) . "'");

		return $query->row['email'];
	}

    public function addToNewsletter($email) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "newsletter SET email = '" . $email . "', `group` = 'newsletter', date_added = NOW()");

        $newsletter_id = $this->db->getLastId();

        return $newsletter_id;
    }

    public function checkEmailSubscribe($email) {

        $query = $this->db->query("SELECT COUNT(email) AS total FROM " . DB_PREFIX . "newsletter WHERE email = '" . $email . "'");

        return $query->row['total'];
    }

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function getCustomerByCode($code) {
		$query = $this->db->query("SELECT customer_id, firstname, lastname, email FROM `" . DB_PREFIX . "customer` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

}
