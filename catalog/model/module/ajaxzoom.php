<?php
/**
* @version     3.1
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2018 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

class ModelModuleAjaxZoom extends Model {

	// AZ 5
	public function hookProduct($id_product) {

		if ($this->config->get('ajaxzoom_ENABLEINFRONTDETAIL') == 'false' || !$this->isOnlyProductActive($id_product) || !$this->isProductActive($id_product)) {
			return;
		}

		$config = $this->getSetting('ajaxzoom');

		/*if(!$this->is2()) {
			foreach ($config as $key => $value) {
				$config[$key] = addslashes($value);
			}
		}*/

		include dirname(DIR_SYSTEM).'/ajaxzoom/AzMouseoverSettings.php';
		$mouseover_settings = new AzMouseoverSettings(array(
			'vendor' => 'Opencart',
			'exclude_opt_vendor' => array(
				'axZmPath',
				'lang',
				'images',
				'images360',
				'videos',
				'enableNativeSlider',
				'enableCssInOtherPages',
				'default360settingsEmbed',
				'defaultVideoYoutubeSettings',
				'defaultVideoVimeoSettings',
				'defaultVideoDailymotionSettings',
				'defaultVideoVideojsSettings',
				'defaultVideoVideojsJS'
			),
			'exclude_cat_vendor' => array('video_settings'),
			'config_extend' => array()
		));

		$ajaxzoom_initParam = $mouseover_settings->getInitJs(array(
			'cfg' => $config,
			'window' => 'window.',
			'holder_object' => 'jQuery.axZm_psh',
			'exclude_opt' => array(),
			'exclude_cat' => array('video_settings'),
			'ovrprefix' => 'ajaxzoom',
			'differ' => true,
			'min' => true
		));

		$data = array(
			'ajaxzoom_images360JSON' => $this->images360Json($id_product),
			'ajaxzoom_imagesJSON' => $this->imagesJson($id_product),
			'axZm_lang' => $this->language->get('code'),
			'axZm_config' => $config,
			'ajaxzoom_initParam' => $ajaxzoom_initParam,
			'axZm_base_uri' => $this->getUri() . 'ajaxzoom/',
			'axZm_is2' => $this->is2()
			);

		$template_path = $this->config->get('config_template');

		if (!$template_path){
			$template_path = 'default';
		}

		if (file_exists(DIR_TEMPLATE . $template_path . '/template/extension/product/ajaxzoom.tpl')) {
			$this->template = $template_path . '/template/extension/product/ajaxzoom.tpl';
			if (version_compare(VERSION, '2.2.0.0', '>=')) {
				$this->template = 'extension/product/ajaxzoom.tpl';
			}
		} else {
			$this->template = 'default/template/product/ajaxzoom.tpl';
		}

		if($this->is2()) {
			return $this->load->view($this->template, $data);
		} else {

			extract($data);

			ob_start();

			require(DIR_TEMPLATE . $this->template);

			$output = ob_get_contents();

			ob_end_clean();

			return $output;
		}
	}

	// AZ 5
	public function hookHeader() {

		if ((isset($this->request->get['route']) && $this->request->get['route'] == 'product/product')
			|| (isset($this->request->request['route']) && $this->request->request['route'] == 'product/product')
		) {

			if ($this->config->get('ajaxzoom_ENABLEINFRONTDETAIL') == 'false') {
				return;
			}

			$url = $this->getUrl() . 'ajaxzoom/';

			// styles
			$this->document->addStyle($url . 'axZm/axZm.css');
			$this->document->addStyle($url . 'axZm/extensions/axZmThumbSlider/skins/default/jquery.axZm.thumbSlider.css');
			$this->document->addStyle($url . 'axZm/extensions/axZmMouseOverZoom/jquery.axZm.mouseOverZoom.5.css');
			$this->document->addStyle($url . 'axZm/extensions/jquery.axZm.imageCropLoad.css');
			$this->document->addStyle($url . 'axZm/extensions/jquery.axZm.expButton.css');

			if ($this->is2()) {
				$this->document->addStyle($url . 'AjaxZoom.css');
			} else {
				$this->document->addStyle($url . 'AjaxZoom15.css');
			}

			if ($this->config->get('ajaxzoom_AJAXZOOMOPENMODE') == 'fancyboxFullscreen') {
				$this->document->addStyle($url . 'axZm/plugins/demo/jquery.fancybox/jquery.fancybox-1.3.4.css');
			}

			if ($this->config->get('ajaxzoom_PNGMODECSSFIX') == 'true') {
				$this->document->addStyle($url . 'axZm/extensions/axZmMouseOverZoom/jquery.axZm.mouseOverZoomPng.5.css');
			}

			// scripts
			$this->document->addScript($url . 'axZm/jquery.axZm.js');
			$this->document->addScript($url . 'axZm/extensions/axZmThumbSlider/lib/jquery.mousewheel.min.js');
			$this->document->addScript($url . 'axZm/extensions/axZmThumbSlider/lib/jquery.axZm.thumbSlider.js');

			if ($this->config->get('ajaxzoom_SPINNER') == 'true') {
				$this->document->addScript($url . 'axZm/plugins/spin/spin.min.js');
			}

			$this->document->addScript($url . 'axZm/extensions/axZmMouseOverZoom/jquery.axZm.mouseOverZoom.5.js');
			$this->document->addScript($url . 'axZm/extensions/axZmMouseOverZoom/jquery.axZm.mouseOverZoomInit.5.js');
			$this->document->addScript($url . 'axZm/extensions/jquery.axZm.expButton.min.js');
			$this->document->addScript($url . 'axZm/extensions/jquery.axZm.imageCropLoad.min.js');

			if ($this->config->get('ajaxzoom_AJAXZOOMOPENMODE') == 'fancyboxFullscreen') {
				$this->document->addScript($url . 'axZm/plugins/demo/jquery.fancybox/jquery.fancybox-1.3.4.js');
				$this->document->addScript($url . 'axZm/extensions/jquery.axZm.openAjaxZoomInFancyBox.js');
			}

			$this->document->addScript($url . 'axZm/plugins/JSON/jquery.json-2.3.min.js');
		}
	}

	public function isProductActive($id_product) {
		return !$this->db->query('SELECT *
			FROM `ajaxzoomproducts`
			WHERE id_product = '.(int)$id_product)->num_rows;
	}

	public function isOnlyProductActive($id) {
		$csv = $this->config->get('ajaxzoom_DISPLAYONLYFORTHISPRODUCTID');

		if (empty($csv)) {
			return true;
		}

		$arr = $this->getCSV($csv);

		if (in_array($id, $arr)) {
			return true;
		}

		return false;
	}

	public function getCSV($input, $delimiter = ',', $enclosure = '"', $escape = '\\')
	{
		if (function_exists('str_getcsv')) {
			return str_getcsv($input, $delimiter, $enclosure, $escape);
		} else {
			$temp = fopen('php://memory', 'rw');
			fwrite($temp, $input);
			fseek($temp, 0);
			$r = fgetcsv($temp, 0, $delimiter, $enclosure);
			fclose($temp);
			return $r;
		}
	}

	public function imagesJson($id_product) {
		$images = $this->getProductImages($id_product);
		$json = '{';
		$cnt = 1;

		foreach ($images as $image) {
			$json .= '"'.$cnt.'": {"img": "'.$image.'", "order": "'.$cnt.'", "title": ""}';
			$cnt++;

			if ($cnt != count($images) + 1)
				$json .= ', ';
		}
		$json .= '}';

		return $json;
	}

	public function images360Json($id_product) {
		$sets_groups = $this->getSetsGroups($id_product);

		$arr = array();

		foreach ($sets_groups as $group) {
			if ($group['status'] == 0) {
				continue;
			}

			$settings = $this->prepareSettings($group['settings']);

			if (!empty($settings)) {
				$settings = ', '.$settings;
			}

			if ($group['qty'] > 0) {

				$crop = (!isset($group['crop']) || empty($group['crop'])) ? '[]' : trim(preg_replace('/\s+/', ' ', $group['crop']));
				$hotspots = (!isset($group['hotspots']) || empty($group['hotspots'])) ? '{}' : trim(preg_replace('/\s+/', ' ', $group['hotspots']));

				if ($group['qty'] == 1) {
					$str = '"'.$group['id_360'].'": {"path": "' . $this->getUri() . 'ajaxzoom/pic/360/'.$id_product.'/'.$group['id_360'].'/'.$group['id_360set'].'"'.$settings;
					$str .= ', "combinations": ['.$group['combinations'].']';
				} else {
					$str = '"'.$group['id_360'].'": {"path": "' . $this->getUri() . 'ajaxzoom/pic/360/'.$id_product.'/'.$group['id_360'].'"'.$settings;
					$str .= ', "combinations": ['.$group['combinations'].']';
				}

				if ($crop && $crop != '[]'){
					$str .= ', "crop": '.$crop;
				}
				if ($hotspots && $hotspots != '{}'){
					$str .= ', "hotspotFilePath": '.$hotspots;
				}
				$str .= '}';

				$arr[] = $str;
			}
		}

		$ret = '{'.implode(',', $arr).'}';
		$ret = str_replace('\\n', '', $ret);
		return $ret;
	}

	public function prepareSettings($str)
	{
		$res = array();
		$settings = (array)$this->jsonDecode($str);

		foreach ($settings as $key => $value) {
			if ($value == 'false' || $value == 'true' || $value == 'null' || is_numeric($value)
				|| substr($value, 0, 1) == '{' || substr($value, 0, 1) == '[') {
				$res[] = '"'.$key.'": '.$value;
			} else {
				$res[] = '"'.$key.'": "'.$value.'"';
			}
		}

		return implode(', ', $res);
	}

	public function getSetsGroups($id_product, $variant_id='')
	{
		$filter = '';
		if(!empty($variant_id)) {
			$filter = " AND (combinations = '' OR FIND_IN_SET(" . intval($variant_id) . ", combinations)) ";
		}

		return $this->db->query('SELECT g.*, COUNT(g.id_360)
			AS qty, s.id_360set
			FROM `ajaxzoom360` g
			LEFT JOIN `ajaxzoom360set` s ON g.id_360 = s.id_360
			WHERE g.id_product = '.(int)$id_product.'
			' . $filter . '
			GROUP BY g.id_360')->rows;
	}

	public function getProductImages($id_product) {

		$path = $this->getUri() . 'image/';

		$this->load->model('catalog/product');
		$data = $this->model_catalog_product->getProduct($id_product);
		$data['product_image'] = $this->model_catalog_product->getProductImages($id_product);

		$files = array();
		if (!empty($data['image'])) {
			array_push($files, $path . $data['image']);
		}
		if (!empty($data['product_image'])) {
			foreach ($data['product_image'] as $image) {
				array_push($files, $path . $image['image']);
			}
		}
		return $files;
	}

	public function tablePrefix() {
		return DB_PREFIX;
	}

	public function jsonEncode($data) {
		return json_encode($data);
	}

	public function jsonDecode($data) {
		return json_decode($data);
	}

	public function getUrl() {
		if (isset($this->request->server['HTTPS']) && $this->request->server['HTTPS']) {
			if ($this->config->get('config_ssl')){
				$server = $this->config->get('config_ssl');
			}else{
				$server = $this->config->get('config_url');
				$server = str_replace('http:', 'https:', $server);
			}
		} else {
			$server = $this->config->get('config_url');
		}

		return $server;
	}

	public function getUri() {
		$p = parse_url($this->getUrl());
		return $p['path'];
	}

	public function getSetting($code, $store_id = 0) {
		$setting_data = array();

		$query = $this->db->query("SELECT * FROM " . $this->tablePrefix() . "setting WHERE store_id = '" . (int)$store_id . "' AND `" . ($this->is2() ? 'code' : 'group') . "` = '" . $this->db->escape($code) . "'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$setting_data[$result['key']] = $result['value'];
			} else {
				$setting_data[$result['key']] = json_decode($result['value'], true);
			}
		}

		return $setting_data;
	}

	public function is2() {
		if (version_compare ( VERSION , '2.0' ) >= 0) {
			return true;
		}
	}
}
