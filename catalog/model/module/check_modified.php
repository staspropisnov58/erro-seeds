<?php
class ModelModuleCheckModified extends Model {
  public function getDateModified($page){
    $date_modified = $this->db->query("SELECT date_modified FROM " . DB_PREFIX . "date_modified WHERE page='" . $page . "'");
    if($date_modified->num_rows){
      return $date_modified->row['date_modified'];
    }else{
      return '';
    }
  }
}
