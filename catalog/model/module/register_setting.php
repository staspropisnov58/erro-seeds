<?php
class ModelModuleRegisterSetting extends Model
{
  public function getCustomerRegisterFields(){
    $fields = array();

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "register_setting WHERE field_status = 1 ORDER BY field_sort_order ASC");

    if ($query->num_rows) {
      foreach ($query->rows as $field) {
        $fields[] = [
                    'name' => $field['field_name'],
                    'label' => unserialize($field['field_label'])[$this->config->get('config_language_id')],
                    'placeholder' => unserialize($field['field_placeholder'])[$this->config->get('config_language_id')],
                    'sort_order' => $field['field_sort_order'],
                    'type' => $field['field_type'],
                  ];
      }
    }

    return $fields;
  }

  public function getAllRegisterFields(){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "register_setting WHERE field_name != 'password' ORDER BY field_sort_order ASC");

    return $query->rows;
  }

  public function getCustomerEditFields(){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "register_setting WHERE field_status = 1 AND field_editable = 1 AND field_name != 'password' ORDER BY field_sort_order ASC");

    if ($query->num_rows) {
      foreach ($query->rows as $field) {
        $fields[] = [
                    'name' => $field['field_name'],
                    'label' => unserialize($field['field_label'])[$this->config->get('config_language_id')],
                    'placeholder' => unserialize($field['field_placeholder'])[$this->config->get('config_language_id')],
                    'sort_order' => $field['field_sort_order'],
                    'type' => $field['field_type'],
                  ];
      }
    }

    return $fields;
  }

}
