<?php
class ModelModuleValidator extends Model
{
  /**
   *  gets all data, calls other methods and returns errors
   * @param array $data
   * @return array $errors if $errors is empty validation is successful
   */
  public function validate($data, $fields_roles = []) {
    $errors = array();

    $this->load->model('module/validatorFieldSettings');
    array_walk($fields_roles, function($role, $field) {
      $this->model_module_validatorFieldSettings->$field = $role;
    });
    $settings = $this->model_module_validatorFieldSettings->getFieldSettings($data);

    foreach ($data as $item => $value) {
      if (isset($settings[$item])) {
        foreach ($settings[$item] as $method => $context) {
          if (!$this->$method($value, $context['rule'])) {
            $errors[$item] = $context['error'];
            break;
          }
        }
      }
    }

    return $errors;
  }

  protected function validateRangelength($value, $rangelength)
  {
    $length = strlen($value);
    if (($length < $rangelength[0]) || ($length > $rangelength[1])) {
      return false;
    } else {
      return true;
    }
  }

  protected function validateRegexp($value, $regexp)
  {
    if (preg_match($regexp, $value)) {
      return true;
    } else {
      return false;
    }
  }

  protected function validateMinmax($value, $range = [])
  {
    if (((int)$value < $range[0]) || ((int) $value > $range[1])) {
      return false;
    } else {
      return true;
    }
  }

  protected function validateDate($value, $time)
  {
    if (!checkdate($value['month'], $value['day'], $value['year'])) {
      return false;
    } elseif (strtotime(implode('-', $value)) <= $time) {
      return false;
    } else {
      return true;
    }
  }

  protected function validateNotExist($value, $check_in = [])
  {
    $check = false;
    $customer_id = $this->customer->getId();
    if ($customer_id) {
      $check_exist = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . $check_in['table'] . " WHERE LOWER(" . $check_in['column'] . ") = '" . $this->db->escape(utf8_strtolower($value)) . "'");
      $check = !($check_exist->num_rows && $check_exist->row['customer_id'] !== $customer_id);
    } else {
      $check_exist = $this->db->query("SELECT * FROM " . DB_PREFIX . $check_in['table'] . " WHERE LOWER(" . $check_in['column'] . ") = '" . $this->db->escape(utf8_strtolower($value)) . "'");
      $check = !$check_exist->num_rows;
    }

    return $check;
  }

  protected function validateExist($value, $check_in = [])
  {
    $check = false;
    $check_exist = $this->db->query("SELECT DISTINCT " . $check_in['value'] . " FROM " . DB_PREFIX . $check_in['table'] . " WHERE LOWER(" . $check_in['column'] . ") = '" . $this->db->escape(utf8_strtolower($value)) . "'");
    $check = $check_exist->num_rows && $check_exist->row[$check_in['value']];

    return $check;
  }
}
