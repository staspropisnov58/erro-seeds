<?php
class ModelModuleNotificationRequest extends Model
{
  public function saveRequest($data, $product_id)
  {
      if(!isset($data['telephone'])){
          $data['telephone'] ='';
      }
      if(!isset($data['pre_order'])){
          $data['pre_order'] =0;
      }
    $sql = "INSERT INTO " . DB_PREFIX . "notification_request SET
            product_id = " . (int)$product_id . ",
            quantity = " . (int)$data['quantity'] . ",
            email = '" . trim($this->db->escape($data['email'])) . "',
            telephone = '" . trim($this->db->escape($data['telephone'])) . "',
            pre_order = '" . trim($this->db->escape($data['pre_order'])) . "',
            date_added = NOW(),
            date_expired = '" . $this->db->escape(implode('-', [$data['date_expired']['year'], $data['date_expired']['month'], $data['date_expired']['day']])) . "',
            language_id = " . (int)$this->config->get('config_language_id');

    if ($this->customer->isLogged()) {
      $sql .= ", customer_id = " . $this->customer->getId();
    }

    $this->db->query($sql);
  }

  public function checkRequestNotExists($email, $product_id)
  {
    $query = $this->db->query("SELECT request_id FROM " . DB_PREFIX . "notification_request WHERE email = '" . $this->db->escape($email) . "' AND product_id = " . (int)$product_id);
    if ($query->num_rows) {
      return false;
    } else {
      return true;
    }
  }

  public function getProductName($product_id)
  {
    $query = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id = " . (int)$product_id . " AND language_id = ". (int)$this->config->get('config_language_id'));
    return $query->row['name'];
  }

  public function getProductsByNotificationsId($requests_id){
		$query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "notification_request WHERE request_id IN(" . $requests_id . ")");

		$this->load->model('catalog/product');

		$products = array();

		if($query->num_rows){
			foreach ($query->rows as $product_id){
				$products[$product_id['product_id']] = $this->model_catalog_product->getProduct($product_id['product_id']);
				$raleted_products = $this->model_catalog_product->getProductRelated($product_id['product_id']);
				$count = 0;
				$raleted = array();

  				foreach ($raleted_products as $key => $raleted_product) {
  					if($count >= 4){
  						break;
  					}else{
  						$raleted[$key] = $raleted_product;
  					}
  					$count = $count + 1;
  				}
				}
				$products[$product_id['product_id']]['related_products'] = $raleted;
				$products[$product_id['product_id']]['option'] = $this->model_catalog_product->getProductAttributes($product_id['product_id']);
			}
		return $products;
	}
}
