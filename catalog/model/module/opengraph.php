<?php
class ModelModuleOpengraph extends Model
{
  public function getOpengraphData($url){
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "page p LEFT JOIN " . DB_PREFIX . "page_description pd ON (p.page_id = pd.page_id) WHERE route = '" . $url . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
    return $query->row;
  }
}
