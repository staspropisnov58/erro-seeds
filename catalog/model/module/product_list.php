<?php
class ModelModuleProductList extends Model {

  public function getProductData($product_id, $setting){

    $this->load->model('catalog/product');
    $this->load->model('tool/image');
    $product_array = array();


		$sql = "SELECT DISTINCT *,";

		if($this->config->get('product_stickers_status')){
			$sql .= " p.main_stickers,";
		}

		$sql .= " p.meta_robots, pd.name AS name, ";


		$sql .=" p.image, m.name AS manufacturer,
			(SELECT price FROM " . DB_PREFIX . "product_discount pd2
				WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
				AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW())
				AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW()))
				ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1)
			AS discount,
			(SELECT price FROM " . DB_PREFIX . "product_special ps
				WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
				AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))
				ORDER BY ps.priority ASC, ps.price ASC LIMIT 1)
			AS special,
			(SELECT points FROM " . DB_PREFIX . "product_reward pr
				WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "')
			AS reward,
			(SELECT ss.name FROM " . DB_PREFIX . "stock_status ss
				WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "')
			AS stock_status,

			(SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1
				WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id)
			AS rating,
			(SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2
				WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id)
			AS reviews,
			p.sort_order
			FROM " . DB_PREFIX . "product p
			LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
			LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)
			WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

			$query = $this->db->query($sql);

      if($query->rows){

        if ($query->row['image']) {
          $image = $this->model_tool_image->resize($query->row['image'], $setting['width'], $setting['height']);
        } else {
          $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
        }

    		if ($query->num_rows) {
          $stickers = [];





    			if ((float)$query->row['special']) {
            $sticker_class = 'sticker-sale';

    	      $percent = round(100 - ((float)$query->row['special'] * 100 / (float)$query->row['price']));
    	      $stickers[] = array('class' => 'sticker ' . $sticker_class, 'text' => '-' . $percent . '%');
    		  }





    			if($this->config->get('product_stickers_status')){

            $main_stickers_array = array();

            if($query->row['main_stickers']){
              $main_stickers_array = unserialize($query->row['main_stickers']);
            }

  					foreach ($main_stickers_array as $main_stickers) {
  						if($main_stickers === 'recommended'){
  							$stickers[] = array('class' => 'sticker sticker-top', 'text' => 'TOP');
  						}else{
  							$stickers[] = array('class' => 'sticker sticker-' . $main_stickers, 'text' => mb_strtoupper($main_stickers));
  						}
  					}
    			}

          if ($this->config->get('config_tax')) {
            $tax = $this->currency->format((float)$query->row['special'] ? $query->row['special'] : $query->row['price']);
          } else {
            $tax = false;
          }

          $price = ($query->row['discount'] ? $query->row['discount'] : $query->row['price']);

          if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            $price = $this->currency->format($this->tax->calculate($price, $query->row['tax_class_id'], $this->config->get('config_tax')));
          } else {
            $price = false;
          }

          $url = $this->url->link('product/product', 'product_id=' . $product_id);

    			$product_special = $this->model_catalog_product->getProductSpecial($product_id);


                if($this->customer->isLogged() && $this->customer->getGroupId()==3  ){

                    $sale_price_category = $this->getCategoriesMain($product_id);
                    if($sale_price_category['sale_price']>0){
                        $percent = $sale_price_category['sale_price'];
                        $sticker_class = 'sticker-sale';
                        $stickers[] = array('class' => 'sticker ' . $sticker_class, 'text' => '-' . $percent . '%');

                        $pr = $this->percent($query->row['price'],$sale_price_category['sale_price']);
                        $product_special['price'] = $pr;
                        //   $price=$pr;

                    }else {
                        $summ_sale = $this->customer->getSummeSale();
                        $summ_sale_group = $this->customer->getSummeSaleGroup();
                        $procent_sale = $this->customer->getProcentSale();
                        $procent_sale_group = $this->customer->getProcentSaleGroup();
                        $procent_sale_bonus = $this->customer->getProcentSaleBonus();
                        $customer_balance = $this->customer->getSummBalance();
                        if ($summ_sale < $customer_balance && $summ_sale != 0) {
                            $percent = $procent_sale;
                            $sticker_class = 'sticker-sale';
                            $stickers[] = array('class' => 'sticker ' . $sticker_class, 'text' => '-' . $percent . '%');

                            $pr = $this->percent($query->row['price'], $procent_sale);
                            $product_special['price'] = $pr;
                            $price = $pr;
                        } elseif ($summ_sale_group < $customer_balance && $summ_sale_group != 0) {
                            $percent = $procent_sale_group;
                            $sticker_class = 'sticker-sale';
                            $stickers[] = array('class' => 'sticker ' . $sticker_class, 'text' => '-' . $percent . '%');

                            $pr = $this->percent($query->row['price'], $procent_sale_group);
                            $product_special['price'] = $pr;
                            $price = $pr;
                        }
                    }



                }


    			$customers_rating = isset($query->row['customers_rating']) ? $query->row['customers_rating'] : 0;
          if($setting['attributes']){
            $attribute_array = $this->getAttributes($product_id);
          }else{
            $attribute_array = array();
          }
    			$product_array = array(
    				'product_id'       => $query->row['product_id'],
            'quantity'         => $query->row['quantity'],
    				'name'             => $query->row['name'],
    				'description'      => html_entity_decode($query->row['description'], ENT_QUOTES, 'UTF-8'),
    				'alt_image'				 => $query->row['alt_image'],
    				'title_image'			 => $query->row['title_image'],
            'price'            => $query->row['price'],
            'special2'            => $product_special['price'] ? $product_special['price'] : 0,
            'discount'         => $query->row['discount'],
            'tax_class_id'     => $query->row['tax_class_id'],
            'stickers'         => $stickers,
            'tax'              => '',
            'rating'           => round($query->row['rating'] + $customers_rating),
            'href'             => $url,
            'attributes'       => $attribute_array,
            'image'            => $query->row['image']
    		);
        if($this->config->get('seoboost_catalog_upgrade_short_description')){
  				$product_array['short_description'] = $query->row['short_description'];
  			}

    	}
    }

    return $product_array;

  }
    public function percent($number,$procent) {

        $percent = $procent; // Необходимый процент
        $number_percent = $number / 100 * $percent;

        return $number-$number_percent;

    }
    public function getCategoriesMain($product_id) {
        $query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' and main_category=1 ");
        if($query->row['category_id'] > 0){
            $query = $this->db->query("SELECT sale_price FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$query->row['category_id'] . "' ");
        }
        return $query->row;
    }
  public function getAttributes($product_id){
    $this->load->model('catalog/product');
    $this->load->model('tool/image');
    $attribute_groups = $this->model_catalog_product->getProductAttributes($product_id);

    $attributes = array();
    if($attribute_groups) {
      foreach($attribute_groups as $attribute_group){
        foreach($attribute_group['attribute'] as $attribute){
          $attribute['icon']  = '';
          $attribute['svg']   = '';
          if (!is_file(DIR_IMAGE . $attribute['image'] )){
            $attrtibute['icon'] = $this->model_tool_image->resize('small-placeholder.png', $this->config->get('config_image_attribute_product_width'), $this->config->get('config_image_attribute_product_height'));
          }else{

          $type = pathinfo($attribute['image']);
            if($type['extension'] == 'svg'){
              $attribute['svg'] = $this->model_tool_image->renderSVG($attribute['image']);
            }else{
              $attribute['icon'] = $this->model_tool_image->resize($attribute['image'], $this->config->get('config_image_attribute_product_width'), $this->config->get('config_image_attribute_product_height'));
            }
          }
          $attributes[] = array(
            'attribute_id'    => $attribute['attribute_id'],
            'name'            => $attribute['name'],
            'text'            => $attribute['text'],
            'icon'            => $attribute['icon'],
            'svg'							=> $attribute['svg'],
          );
        }
      }
    }
    return $attributes;
  }

  public function getProductListData($setting){

    $products_data = $this->cache->get('product_list.' . $setting['module_id'] . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_customer_group_id') . '.' . $this->config->get('config_language_id'));

    if(!$products_data){


      $this->load->model('catalog/category');
      $this->load->model('catalog/product');

      $data['products'] = array();

      if (!$setting['limit']) {
        $setting['limit'] = 4;
      }

      $products = array_slice($setting['product'], 0, (int)$setting['limit']);
      foreach ($products as $product_id) {
        $product_info = $this->getProductData($product_id, $setting);

        if ($product_info) {
          $products_data[] = $product_info;
        }
      }

      $this->cache->set('product_list.' . $setting['module_id'] . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$this->config->get('config_customer_group_id')  . '.' . $this->config->get('config_language_id'), $products_data);
    }

    return $products_data;
  }

}
