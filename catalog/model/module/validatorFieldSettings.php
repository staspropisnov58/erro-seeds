<?php
class ModelModuleValidatorFieldSettings extends Model
{
  /**
  * 'contact'
  * 'customer'
  */
  public $email = 'contact';

  /**
  * 'optional'
  * 'required'
  */
  public $comment = 'optional';

  public function getFieldSettings($fields)
  {
    $this->load->language('common/errors');
    $field_settings = [];

    array_walk($fields, function($field, $field_name) use (&$field_settings) {
      $get_field_settings = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $field_name))) . 'Settings';
      if (method_exists($this, $get_field_settings)) {
        $field_settings[$field_name] = $this->$get_field_settings();
      }
		});

    return $field_settings;
  }

  protected function getEmailSettings()
  {
    $email_settings = ['validateRangelength' => [
                          'rule' => [3, 100],
                          'error' => sprintf($this->language->get('error_email_rangelength'), 3, 100)],
                       'validateRegexp' => [
                          'rule' => '/^[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}))*\.{1}(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/i',
                          'error' => $this->language->get('error_email_regexp')]];

    if ($this->email === 'customer') {
      $email_settings['validateNotExist'] = [
         'rule' => [
           'table' =>'customer',
           'column' =>'email'],
         'error' => $this->language->get('error_customer_email')];
    }

    return $email_settings;
  }

  protected function getTelephoneSettings()
  {
    return ['validateRangelength' => [
                        'rule' => [5, 15],
                        'error' => sprintf($this->language->get('error_phone_rangelength'), 5, 11)],
                    'validateRegexp' => [
                        'rule' => '/^[\+]?[(]?[0-9]{0,3}[)]?[\-\s\. ]?[0-9]{3}[\-\s\. ]?([0-9]{2,6}|[0-9]{2}[\-\s\.]?[0-9]{2})$/',
                        'error' => $this->language->get('error_phone_regexp')]
                      ];
  }

  protected function getPasswordSettings()
  {
    return ['validateRangelength' => [
                          'rule' => [4, 40],
                          'error' => sprintf($this->language->get('error_password_rangelength'), 4, 40)]];
  }

  protected function getLoginSettings()
  {
    return ['validateRangelength' => [
                          'rule' => [1, 40],
                          'error' => sprintf($this->language->get('error_login_rangelength'), 1, 40)],
                       'validateNotExist' =>[
                          'rule' => [
                            'table' =>'customer',
                            'column' =>'login'],
                          'error' => $this->language->get('error_customer_login')]];
  }

  protected function getRefferedBySettings()
  {
    return ['validateExist' =>[
                          'rule' => [
                            'value' =>'affiliate_id',
                            'table' =>'customer',
                            'column' =>'login'],
                          'error' => $this->language->get('error_affiliate_login')]];
  }

  protected function getFirstNameSettings()
  {
    return ['validateRangelength' => [
                          'rule' => [1, 100],
                          'error' => sprintf($this->language->get('error_firstname_rangelength'), 1, 100)],
                          'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic} \'\-:]+$/u',
                          'error' => $this->language->get('error_firstname_regexp')]];
  }

  protected function getLastNameSettings()
  {
    return ['validateRangelength' => [
                          'rule' => [1, 100],
                          'error' => sprintf($this->language->get('error_lastname_rangelength'), 1, 100)],
                            'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic} \'\-:]+$/u',
                          'error' => $this->language->get('error_lastname_regexp')]];
  }

  protected function getPatronymicSettings()
  {
    return ['validateRangelength' => [
                          'rule' => [1, 100],
                          'error' => sprintf($this->language->get('error_patronymic_rangelength'), 1, 100)],
                        'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic} \'\-:]+$/u',
                          'error' => $this->language->get('error_patronymic_regexp')]];
  }

  protected function getCompanySettings()
  {
    return ['validateRangelength' => [
                          'rule' => [1, 100],
                          'error' => sprintf($this->language->get('error_company_rangelength'), 1, 40)],
                        'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic} \'\-:]+$/u',
                          'error' => $this->language->get('error_company_regexp')]];
  }

  protected function getAddress1Settings()
  {
    return ['validateRangelength' => [
                          'rule' => [2, 120],
                          'error' => sprintf($this->language->get('error_address_1_rangelength'), 2, 120)],
                        'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic}\x{2116}\x{0023} \'\-:,\/\.]+$/u',
                          'error' => $this->language->get('error_address_1_regexp')]];
  }

  protected function getAddress2Settings()
  {
    return $this->getAddress1Settings();
  }

  protected function getCitySettings()
  {
    return ['validateRangelength' => [
                          'rule' => [2, 120],
                          'error' => sprintf($this->language->get('error_city_rangelength'), 2, 120)],
                        'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic} \'\-:,\/\.]+$/u',
                          'error' => $this->language->get('error_city_regexp')]];
  }

  protected function getPostcodeSettings()
  {
    return ['validateRangelength' => [
                          'rule' => [4, 9],
                          'error' => sprintf($this->language->get('error_postcode_rangelength'), 4, 9)],
                        'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9 \-]+$/u',
                          'error' => $this->language->get('error_postcode_regexp')]];
  }

  protected function getCountrySettings()
  {
    return ['validateRangelength' => [
                          'rule' => [2, 120],
                          'error' => sprintf($this->language->get('error_country_rangelength'), 2, 120)],
                        'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic} \'\-:,\/]+$/u',
                          'error' => $this->language->get('error_country_regexp')]];
  }

  protected function getZoneSettings()
  {
    return ['validateRangelength' => [
                          'rule' => [2, 120],
                          'error' => sprintf($this->language->get('error_zone_rangelength'), 2, 120)],
                        'validateRegexp' => [
                          'rule' => '/^[a-zA-Z0-9\p{Cyrillic} \'\-:,\/]+$/u',
                          'error' => $this->language->get('error_zone_regexp')]];
  }

  protected function getCommentSettings()
  {
    if ($this->comment === 'required') {
      $commetn_settings = ['validateRangelength' => [
                            'rule' => [3, 2000],
                            'error' => sprintf($this->language->get('error_text'),3, 2000)]];
    } else {
      $commetn_settings = ['validateRangelength' => [
        'rule' => [0, 3000],
        'error' => sprintf($this->language->get('error_comment_length'), 3000)]];
    }

    return $commetn_settings;
  }

  protected function getQuantitySettings()
  {
    return ['validateMinmax' => [
                          'rule' => [1, 100000],
                          'error' => sprintf($this->language->get('error_minmax'), 1, 100000)],
                      'validateRegexp' => [
                          'rule' => '/^(\d)*$/',
                          'error' => $this->language->get('error_phone_regexp')]];
  }

  protected function getDateExpiredSettings()
  {
    return ['validateDate' => [
                          'rule' => time(),
                          'error' => $this->language->get('error_date')]];
  }

  protected function getRatingSettings()
  {
    return ['validateMinmax' => [
                          'rule' => [1, 5],
                          'error' => $this->language->get('error_rating')]];
  }
}
