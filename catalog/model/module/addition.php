<?php
class ModelModuleAddition extends Model
{
  public function getAdditionForShipping($shipping_code)
  {
    $additions = [];
    $additions = $this->getAdditions('shipping');
    if ($additions) {
      foreach ($additions as $addition_id => &$addition) {
        $addition['products'] = $this->getAdditionProducts($addition_id, $shipping_code);
      }
    }
    return $additions;
  }

  private function getAdditions($block)
  {
    $data = [];
    $additions = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE code = 'addition'");
    if ($additions->num_rows) {
      foreach ($additions->rows as $addition) {
        $addition_setting = unserialize($addition['setting']);
        if ($addition_setting['block'] === $block) {
          $data[$addition['module_id']] = [
            'title'       => $addition_setting['module_description'][$this->config->get('config_language_id')]['title'],
            'description' => $addition_setting['module_description'][$this->config->get('config_language_id')]['description']
          ];
        }
      }
    }
    return $data;
  }

  private function getAdditionProducts($addition_id, $shipping_code)
  {
      $addition_products = [];
      $addition_products_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "addition_product WHERE addition_id = " . (int)$addition_id);
      if ($addition_products_query->num_rows) {
        foreach ($addition_products_query->rows as $addition_product) {
          $dependencies_value = unserialize($addition_product['dependencies_value']);
          if ($dependencies_value['shipping'] === [] || in_array($shipping_code, $dependencies_value['shipping'])) {
            $product = $this->prepareProduct($addition_product['product_id']);
            if ($product) {
              $checked = isset($this->session->data['order']['additions'][$addition_id])
                         && $this->session->data['order']['additions'][$addition_id] === $addition_product['product_id']
                         && $this->session->data['shipping_method']['code'] === $shipping_code;
              $addition_products[$addition_product['product_id']] = [
                'dependencies_value' => $dependencies_value,
                'name'               => $product['name'],
                'description'        => html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'),
                'cost'               => $this->currency->format($this->tax->calculate($product['cost'], $product['tax_class_id'], $this->config->get('config_tax'))),
                'raw_cost'           => $product['cost'],
                'checked'            => $checked,
              ];
            }
          }
        }
      }

      return $addition_products;
  }

  private function checkCategories($categories)
  {
    $products = $this->cart->getProducts();
    foreach ($products as $product) {
      $query = $this->db->query("SELECT TRUE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product['product_id'] . "' AND category_id IN(" . $categories . ")");
      if (!$query->num_rows) {
        return false;
      }
    }
    return true;
  }

  public function checkCartDependencies($addition_id, $product_id, $payment_code)
  {
    $check = true;
    $addition_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "addition_product
                                          WHERE addition_id = " . (int)$addition_id . " AND product_id = " . (int)$product_id);

    if ($addition_product->row) {
      $dependencies_value = unserialize($addition_product->row['dependencies_value']);

      if (isset($dependencies_value['payment'][$payment_code]) && $dependencies_value['payment'][$payment_code]) {
        $products = $this->cart->getProducts();
        foreach ($products as $product) {
          if ($dependencies_value['payment'][$payment_code] === 'manufacturer_strict') {
            $query = $this->db->query("SELECT TRUE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "' AND manufacturer_id IN(" . implode(',', $dependencies_value['manufacturer_strict']) . ")");
            if ($query->num_rows === 0) {
              $check = false;
            }
          }

          if ($dependencies_value['payment'][$payment_code] === 'category_strict') {
            $query = $this->db->query("SELECT TRUE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product['product_id'] . "' AND category_id IN(" . implode(',', $dependencies_value['category_strict']) . ")");
            if ($query->num_rows === 0) {
              $check = false;
            }
          }
        }
      }
    }
    return $check;
  }

  private function prepareProduct($product_id, $show_description = false)
  {
    $this->load->model('tool/image');
    $data = array();
    $sql = "SELECT pd.name, pd.description, p.image, p.price, p.tax_class_id,
            (SELECT price FROM " . DB_PREFIX . "product_special ps
              WHERE ps.product_id = p.product_id AND ps.customer_group_id = 0 AND
              ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))
              ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
            FROM " . DB_PREFIX . "product p LEFT JOIN oc_product_description pd ON p.product_id = pd.product_id
            INNER JOIN " . DB_PREFIX . "product_to_store p2s ON p.product_id = p2s.product_id
            LEFT JOIN " . DB_PREFIX . "product_special ps ON p.product_id = ps.product_id";
    $sql .= " WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND p.quantity > 0";
    $product_data = $this->db->query($sql);
    if ($product_data->num_rows) {
      $data['product_id'] = $product_id;
      $data['name'] = $product_data->row['name'];
      $data['description'] = $product_data->row['description'];
      $data['tax_class_id'] = $product_data->row['tax_class_id'];
      $data['image'] = $product_data->row['image'];

      if ($product_data->row['image']) {
        $data['thumb'] = $this->model_tool_image->resize($product_data->row['image'], 86, 43);
      } else {
        $data['thumb'] = $this->model_tool_image->resize('no_image.png', 86, 43);
      }

      if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
        $data['price'] = $this->currency->format($this->tax->calculate($product_data->row['price'], $product_data->row['tax_class_id'], $this->config->get('config_tax')));
      } else {
        $data['price'] = false;
      }

      if ((float)$product_data->row['special']) {
        $data['special'] = $this->currency->format($this->tax->calculate($product_data->row['special'], $product_data->row['tax_class_id'], $this->config->get('config_tax')));
        $data['cost'] = $product_data->row['special'];
      } else {
        $data['special'] = false;
        $data['cost'] = $product_data->row['price'];
      }

    }
    return $data;
  }

  private function removeFromCart($product_id) {
    $products = $this->cart->getProducts();
    foreach ($products as $product) {
      if ($product['product_id'] === $product_id) {
        $this->cart->remove($product['key']);
      }
    }
  }
}
