<?php

class ModelModuleSendMail extends Model
{

  public function SendAdminEmail($data){

  $query = $this->db->query("SELECT directory FROM " . DB_PREFIX . "language WHERE code = '" . $this->config->get('config_admin_language') . "'");

  $language = new Language($query->row['directory']);
  $language->load('mail/send_mail');

    if(isset($data['route']) && $data['route'] == 'account/newsletter'){

      $email = $this->config->get('config_mail_subscription');

      if($data['newsletter'] == 1){
        $message = $language->get('text_newsletter_subscription');
        $subject = sprintf($language->get('theme_newsletter_subscription'));
      }elseif($data['newsletter'] == 0){
        $message = $language->get('text_newsletter_unsubscription');
        $subject = sprintf($language->get('theme_newsletter_unsubscription'));
      }

       $data['text_greeting']  = $language->get('text_hello')."\n\n";
       $data['text_main'][]= sprintf($language->get('text_subscription'), $this->customer->getEmail(), $message , $this->config->get('config_url'), $this->config->get('config_url')) . "\n\n";
       $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";
   } elseif(isset($data['route']) && $data['route'] == 'account/register') {

     $email = $this->config->get('config_mail_subscription');

     $message = $language->get('text_newsletter_subscription');
     $subject = sprintf($language->get('theme_newsletter_subscription'));

     $data['text_greeting']  = $language->get('text_hello')."\n\n";
     $data['text_main'][]= sprintf($language->get('text_subscription'), $data['email'], $message , $this->config->get('config_url'), $this->config->get('config_url')) . "\n\n";
     $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";
   }elseif(isset($data['low_balance']) || isset($data['message'])){
     $email = $this->config->get('config_email');
     $subject = sprintf($language->get('theme_answer_sms_service'), $this->config->get('config_sms'));
     $data['text_greeting']  = $language->get('text_hello')."\n\n";
     if(isset($data['message'])){
       $data['text_main'][] = sprintf($language->get('text_answer_message'), $data['message'] . "\n\n");
     }
     if(array_key_exists('low_balance',$data)){
       $data['text_main'][]= sprintf($language->get('text_low_balance_on_site'), $this->config->get('config_sms'), $this->config->get('config_url'), $this->config->get('config_url')) . "\n\n";
     }
     $data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ."\n\n";
   }

   $data['store_url'] = $this->config->get('store_url');

   $html = $this->load->view($this->config->get('config_template').'/template/mail/message.tpl', $data);


   $mail_config = $this->config->get('config_mail');
   $mail = new Mail($mail_config);
   $mail->setTo($email);
   if ($mail_config['smtp_username']) {
     $mail->setFrom($mail_config['smtp_username']);
   } else {
     $mail->setFrom($this->request->post['email']);
   }
   $mail->setSender($this->config->get('config_name'));
   $mail->setSubject($subject);
   $mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
   $mail->send();
  }

  public function getSettingsEditOrderCustomerSms($customer_id){
    if((int)$customer_id !== 0){
      $query = $this->db->query("SELECT edit_order FROM " . DB_PREFIX . "customer_sms_notification WHERE customer_id='" . $customer_id . "'");
    }else{
      $query = $this->db->query("SELECT DISTINCT DEFAULT (edit_order) AS edit_order FROM " . DB_PREFIX . "customer_sms_notification");
    }
    return $query->row['edit_order'];
  }

  public function getSettingsEditOrderCustomerEmail($customer_id){
    if((int)$customer_id !== 0){
      $query = $this->db->query("SELECT edit_order FROM " . DB_PREFIX . "customer_email_notification WHERE customer_id='" . $customer_id . "'");
    }else{
      $query = $this->db->query("SELECT DISTINCT DEFAULT (edit_order) AS edit_order FROM " . DB_PREFIX . "customer_email_notification");
    }
    return $query->row['edit_order'];
  }

}
