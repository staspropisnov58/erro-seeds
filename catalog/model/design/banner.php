<?php
class ModelDesignBanner extends Model {
	public function getBanner($banner_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "banner_image bi LEFT JOIN " . DB_PREFIX . "banner_image_description bid ON (bi.banner_image_id  = bid.banner_image_id) WHERE bi.banner_id = '" . (int)$banner_id . "' AND bid.language_id = '" . (int)$this->config->get('config_language_id') . "' ";
		if($this->config->get('sale_events_status')!== NULL && (int)$this->config->get('sale_events_status') !== 0){
			$sql .= " AND bi.status = 1 ";
		} else {

		}
		$sql .= " ORDER BY bi.sort_order ASC";
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
