<?php
class ModelCheckoutConfirm extends Model implements CheckoutSteps
{
  use Autocorrect;
  public $data = [];
  public $errors = [];
  public $update_current_step = true;

  public function load($errors)
  {
    $data['heading_title'] = $this->language->get('heading_title');
    $data['text_h1']       = $this->language->get('text_checkout');
    $data['text_note']       = '';
    $data['text_customer'] = $this->language->get('text_customer');
    $data['text_addresse_and_address'] = $this->language->get('text_addresse_and_address');
    $data['text_shipping_method_confirm'] = $this->language->get('text_shipping_method_confirm');
    $data['text_payment_method_h2'] = $this->language->get('text_payment_method_h2');

    $this->load->model('localisation/country');

    $order_info = $this->session->data['order'];

    if ($this->customer->isLogged()) {
      $data['text_agree'] = '';
      if (isset($order_info['shipping_address']['address_id']) && $order_info['shipping_address']['address_id']) {
        $this->load->model('account/address');
        $order_info['shipping_address'] = array_merge($order_info['shipping_address'], $this->model_account_address->getAddress($order_info['shipping_address']['address_id']));
      }
    } else {
      $data['text_agree'] = $this->language->get('text_agree');
      $data['text_warranty'] = $this->language->get('text_warranty');

      if (isset($this->data['agree'])) {
        $data['agree'] = $this->data['agree'];
      } else {
        $data['agree'] = false;
      }
    }

    $data = array_merge($data, $this->load->controller('checkout/cart/getCartContent'));

    $data['gift_programs'] = '';

		if ($this->config->get('gift_status')) {
			$this->load->model('checkout/gift');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/gift.tpl')) {
				$data['gift_programs'] = $this->load->view(
					                           $this->config->get('config_template') . '/template/checkout/gift.tpl',
																		 $this->model_checkout_gift->getGifts($data['subtotals']));
			} else {
				$data['gift_programs'] = '';
			}
		}

    foreach ($order_info['customer'] as $key => $value) {
      if ($key === 'customer_group_id') {
        continue;
      } else if (in_array($key, ['telephone_country_id', 'telephone']) && isset($order_info['customer']['telephone_country_id'])) {
        $data['order_info']['customer']['telephone'] = $this->model_localisation_country->getCountryTelephoneCode($order_info['customer']['telephone_country_id']) . $order_info['customer']['telephone'];
      } else {
        $data['order_info']['customer'][$key] = $value;
      }
    }

    $addressee = [];
    $address = [];

    foreach ($order_info['shipping_address'] as $key => $value) {
      if ($value) {
        if (in_array($key, ['firstname', 'lastname', 'patronymic', 'telephone', 'company'])) {
          $addressee[$key] = $value;
        } elseif (in_array($key, ['country_id', 'zone_id', 'address_id', 'iso_code_2', 'iso_code_3', 'address_format', 'custom_field'])) {
          continue;
        } elseif ($key === 'new_address' && $value) {
          $data['text_new_address'] = sprintf($this->language->get('text_new_address'), $this->url->link('account/address', '', 'SSL'));
        } else {
          $address[$key] = $value;
        }
      }
    }

    $data['order_info']['addressee'] = implode(' ', $addressee);
    $data['order_info']['address'] = implode(', ', $address);

    $data['order_info']['shipping_method'] = $this->session->data['shipping_method']['title'];
    $data['order_info']['payment_method'] = $this->session->data['payment_method']['title'];

    $form = new Form('confirm', $this->url->link('checkout/checkout', '', 'SSL'));
    $form->addField(['name' => 'comment', 'label' => $this->language->get('entry_comment'), 'placeholder' => '', 'sort_order' => 0, 'value' => '', 'type' => 'textarea']);

    $form->setFieldsErrors($errors);
    $data['form'] = $form->build();

    $data['prev_step'] = $prev_step = $this->session->data['order']['prev_step'];
    $data['prev_step_href'] = $this->url->link('checkout/checkout', '&back_to=' . $prev_step, 'SSL');

    $data['button_submit'] = $this->language->get('button_submit');
    $data['button_back'] = $this->language->get('text_' . $prev_step);

    $data['no_callback'] = $this->config->get('checkout_no_callback');
    $data['entry_no_callback'] = $this->language->get('entry_no_callback');
    $data['help_no_callback'] = $this->language->get('help_no_callback');
    unset($this->session->data['cart_is_open']);

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/confirm.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/checkout/confirm.tpl', $data);
    } else {
      return $this->load->view('default/template/checkout/confirm.tpl', $data);
    }
  }

  public function save($data)
  {
    $errors = array();

    if (isset($this->session->data['order']['pending']) || !$this->session->data['order']) {
      $this->response->redirect($this->url->link('common/home', '', true));
    }

    if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
      $this->load->model('module/validator');
      $errors = $this->model_module_validator->validate($this->request->post, ['comment' => 'optional']);

      if (!$errors) {
        $this->session->data['order']['pending'] = true;
        $this->addOrder($this->request->post);
      }
    }

    return $errors;
  }

  private function addOrder($data) {
    // connect models array
    $models = array('extension/extension', 'account/customer', 'affiliate/affiliate', 'checkout/marketing', 'checkout/order');
    foreach ($models as $model) {
        $this->load->model($model);
    }

    $this->load->language('checkout/checkout');

    $order_data = array();
    $order_data['totals'] = array();
    $total = 0;
    $taxes = $this->cart->getTaxes();
    $sort_order = array();
    $results = $this->model_extension_extension->getExtensions('total');

    foreach ($results as $key => $value) {
        $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
    }

    array_multisort($sort_order, SORT_ASC, $results);

    foreach ($results as $result) {
        if ($this->config->get($result['code'] . '_status')) {
            $this->load->model('total/' . $result['code']);

            $this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
        }
    }

    $sort_order = array();

    foreach ($order_data['totals'] as $key => $value) {
        $sort_order[$key] = $value['sort_order'];
    }

    array_multisort($sort_order, SORT_ASC, $order_data['totals']);


    $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
    $order_data['store_id'] = $this->config->get('config_store_id');
    $order_data['store_name'] = $this->config->get('config_name');
    $order_data['store_url'] = ($order_data['store_id']) ? $this->config->get('config_url') : HTTP_SERVER;

    $customer_data = $this->session->data['order']['customer'];

    if ($this->customer->isLogged()) {
        $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

        $order_data['customer_id'] = $this->customer->getId();
        $order_data['customer_group_id'] = $customer_info['customer_group_id'];
        $order_data['firstname'] = $customer_info['firstname'];
        $order_data['lastname'] = $customer_info['lastname'];
        $order_data['email'] = $customer_info['email'];
        $order_data['telephone'] = $customer_info['telephone'];
        $order_data['fax'] = $customer_info['fax'];
        $order_data['custom_field'] = unserialize($customer_info['custom_field']);
    } else {
        $order_data['customer_id'] = 0;
        $order_data['customer_group_id'] = $this->config->get('config_cutomer_group_id');
        $order_data['firstname'] = '';
        $order_data['lastname'] = '';
        $order_data['email'] = '';
        $order_data['telephone'] = '';
        $order_data['fax'] = '';
        $order_data['custom_field'] = [];
    }

    $order_data = array_merge($order_data, $customer_data);

    $address_data = $this->session->data['order']['shipping_address'];

    if ($this->customer->isLogged()) {
      $this->load->model('account/address');

      if ($address_data['new_address']) {
        $address_fields = array_keys($this->config->get('checkout_' . $this->session->data['shipping_method']['code'] . '_fields'));
        foreach ($address_fields as $field) {
          if (isset($address_data[$field])) {

          } else {
            $address_data[$field] = '';
          }
        }

        $this->model_account_address->addAddress($address_data);
      } elseif ($address_data['address_id']) {
        $address_data = $this->model_account_address->getAddress($address_data['address_id']);
      } else {
        $errors['error_checkout'] = $this->language->get('error_checkout');
      }
    }

    $order_data['payment_firstname'] = isset($address_data['firstname']) ? $address_data['firstname'] : '';
    $order_data['payment_lastname'] = isset($address_data['lastname']) ? $address_data['lastname'] : '';
    $order_data['payment_company'] = isset($address_data['company']) ? $address_data['company'] : '';
    $order_data['payment_address_1'] = isset($address_data['address_1']) ? $address_data['address_1'] : '';
    $order_data['payment_address_2'] = isset($address_data['address_2']) ? $address_data['address_2'] : '';
    $order_data['payment_city'] = isset($address_data['city']) ? $address_data['city'] : '';
    $order_data['payment_postcode'] =isset($address_data['postcode']) ? $address_data['postcode'] : '';
    $order_data['payment_zone'] = isset($address_data['zone']) ? $address_data['zone'] : '';
    $order_data['payment_zone_id'] = isset($address_data['zone_id']) ? $address_data['zone_id'] : 0;
    $order_data['payment_country'] = isset($address_data['country']) ? $address_data['country'] : '';
    $order_data['payment_country_id'] = isset($address_data['country_id']) ? $address_data['country_id'] : 0;
    $order_data['payment_address_format'] = '';
    $order_data['payment_custom_field'] = array();
    $order_data['payment_method'] = isset($this->session->data['payment_method']['title']) ? $this->session->data['payment_method']['title'] : '';
    $order_data['payment_code'] = isset($this->session->data['payment_method']['code']) ? $this->session->data['payment_method']['code'] : '';

    $order_data['shipping_firstname'] = isset($address_data['firstname']) ? $address_data['firstname'] : '';
    $order_data['shipping_lastname'] = isset($address_data['lastname']) ? $address_data['lastname'] : '';
    $order_data['shipping_company'] = isset($address_data['company']) ? $address_data['company'] : '';
    $order_data['shipping_address_1'] = isset($address_data['address_1']) ? $address_data['address_1'] : '';
    $order_data['shipping_address_2'] = isset($address_data['address_2']) ? $address_data['address_2'] : '';
    $order_data['shipping_city'] = isset($address_data['city']) ? $address_data['city'] : '';
    $order_data['shipping_postcode'] = isset($address_data['postcode']) ? $address_data['postcode'] : '';
    $order_data['shipping_zone'] = isset($address_data['zone']) ? $address_data['zone'] : '';
    $order_data['shipping_zone_id'] = isset($address_data['zone_id']) ? $address_data['zone_id'] : '';
    $order_data['shipping_country'] = isset($address_data['country']) ? $address_data['country'] : '';
    $order_data['shipping_country_id'] = isset($address_data['country_id']) ? $address_data['country_id'] : '';
    $order_data['shipping_address_format'] = '';
    $order_data['shipping_custom_field'] = array();
    $order_data['shipping_method'] = isset($this->session->data['shipping_method']['title']) ? $this->session->data['shipping_method']['title'] : '';
    $order_data['shipping_code'] = isset($this->session->data['shipping_method']['code']) ? $this->session->data['shipping_method']['code'] : '';

    $order_data['products'] = array();

    foreach ($this->cart->getProducts() as $product) {
        $option_data = array();

        foreach ($product['option'] as $option) {
            $option_data[] = array(
                'product_option_id' => $option['product_option_id'],
                'product_option_value_id' => $option['product_option_value_id'],
                'option_id' => $option['option_id'],
                'option_value_id' => $option['option_value_id'],
                'name' => $option['name'],
                'value' => $option['value'],
                'type' => $option['type']
            );
        }

        $order_data['products'][] = array(
            'product_id' => $product['product_id'],
            'name' => $product['name'],
            'model' => $product['model'],
            'option' => $option_data,
            'download' => $product['download'],
            'quantity' => $product['quantity'],
            'subtract' => $product['subtract'],
            'price' => $product['price'],
            'total' => $product['total'],
            'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
            'reward' => $product['reward'],
            'child' => !empty($product['child']) ? $product['child'] : '',
            'parent_id' => !empty($product['parent_id']) ? $product['parent_id'] : '',
        );
    }

    // Gift Voucher
    $order_data['vouchers'] = array();

    if (!empty($this->session->data['vouchers'])) {
        foreach ($this->session->data['vouchers'] as $voucher) {
            $order_data['vouchers'][] = array(
                'description' => $voucher['description'],
                'code' => substr(md5(mt_rand()), 0, 10),
                'to_name' => $voucher['to_name'],
                'to_email' => $voucher['to_email'],
                'from_name' => $voucher['from_name'],
                'from_email' => $voucher['from_email'],
                'voucher_theme_id' => $voucher['voucher_theme_id'],
                'message' => $voucher['message'],
                'amount' => $voucher['amount']
            );
        }
    }

    $order_data['gifts'] = isset($data['gifts']) ? $data['gifts'] : [];

    $order_data['comment'] = $data['comment'];
    $order_data['no_callback'] = isset($data['no_callback']);    
    $order_data['total'] = $total;

    if (isset($this->request->cookie['tracking'])) {
        $order_data['tracking'] = $this->request->cookie['tracking'];

        $subtotal = $this->cart->getSubTotal();

        // affiliate
        $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

        if ($affiliate_info) {
            $order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
            $order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
        } else {
            $order_data['affiliate_id'] = 0;
            $order_data['commission'] = 0;
        }

        // marketing
        $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);
        $order_data['marketing_id'] = ($marketing_info) ? $marketing_info['marketing_id'] : 0;
    } else {
        $order_data['affiliate_id'] = 0;
        $order_data['commission'] = 0;
        $order_data['marketing_id'] = 0;
        $order_data['tracking'] = '';
    }

    $order_data['language_id'] = $this->config->get('config_language_id');
    $order_data['currency_id'] = $this->currency->getId();
    $order_data['currency_code'] = $this->currency->getCode();
    $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
    $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

    if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
        $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
    } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
        $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
    } else {
        $order_data['forwarded_ip'] = '';
    }

    $order_data['user_agent'] = (isset($this->request->server['HTTP_USER_AGENT'])) ? $this->request->server['HTTP_USER_AGENT'] : '';
    $order_data['accept_language'] = (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) ? $this->request->server['HTTP_ACCEPT_LANGUAGE'] : '';
    $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
    $data['payment'] = "";
    $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('config_order_status_id'));
    $this->response->redirect($this->url->link('checkout/success', '', true));
  }
}
