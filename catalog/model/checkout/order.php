<?php
class ModelCheckoutOrder extends Model {
	public function addOrder($data) {
		$this->event->trigger('pre.order.add', $data);

		if (!$this->config->get('customer_is_affiliate_status')) {
      $data['affiliate_id'] = 0;
			$data['commission'] = 0;
    }

		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . trim($this->db->escape($data['email'])) . "', telephone = '" . $this->db->escape($data['telephone']) . "', telephone_country_id = '" . trim($this->db->escape($data['telephone_country_id'])) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($data['payment_custom_field']) ? serialize($data['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($data['shipping_custom_field']) ? serialize($data['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', marketing_id = '" . (int)$data['marketing_id'] . "', tracking = '" . $this->db->escape($data['tracking']) . "', language_id = '" . (int)$data['language_id'] . "', currency_id = '" . (int)$data['currency_id'] . "', currency_code = '" . $this->db->escape($data['currency_code']) . "', currency_value = '" . (float)$data['currency_value'] . "', ip = '" . $this->db->escape($data['ip']) . "', forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', user_agent = '" . $this->db->escape($data['user_agent']) . "', accept_language = '" . $this->db->escape($data['accept_language']) . "', date_added = NOW(), date_modified = NOW()");

		$order_id = $this->db->getLastId();

		if ($this->config->get('checkout_no_callback')) {
			$this->db->query("UPDATE " . DB_PREFIX . "order SET no_callback = " . (int)$data['no_callback'] . " WHERE order_id = " . $order_id);
		}

		// Products
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");

				$order_product_id = $this->db->getLastId();

				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}

		// Gift Voucher
		$this->load->model('checkout/voucher');

		// Vouchers
		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");

				$order_voucher_id = $this->db->getLastId();

				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $voucher);

				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}

		//Gifts
		if ($this->config->get('gift_status')) {
			if ($data['gifts']) {
				foreach ($data['gifts'] as $gift_program_id => $gifts) {
					foreach ($gifts as $gift_product_id) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_gift_product (order_id, product_id, gift_program_id, quantity)
						                  SELECT " . (int)$order_id . ", " . (int)$gift_product_id . ", " . (int)$gift_program_id . ", gpp.quantity
															FROM " . DB_PREFIX . "gift_tier gt
															INNER JOIN " . DB_PREFIX . "gift_product gpp ON gt.gift_tier_id = gpp.gift_tier_id
															WHERE gt.gift_program_id = " . (int)$gift_program_id . "
														  AND gpp.product_id = " . (int)$gift_product_id . "
															AND gt.price_start < " . (float)$data['totals']['sub_total']['value'] . "
															AND (gt.price_end > " . (float)$data['totals']['sub_total']['value'] . " OR gt.price_end  IS NULL)");
					}
				}
			}
		}

		// Totals
		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}

		if (isset($data['customer_id']) && (int)$data['customer_id'] !== 0){
			$notifications_sms = $this->db->query("SELECT new_order FROM " . DB_PREFIX . "customer_sms_notification WHERE customer_id='" . $data['customer_id'] . "'");
			$notifications_email = $this->db->query("SELECT new_order FROM " . DB_PREFIX . "customer_email_notification WHERE customer_id='" . $data['customer_id'] . "'");
		}else{
			$notifications_sms = $this->db->query("SELECT DISTINCT DEFAULT (new_order) AS new_order FROM " . DB_PREFIX . "customer_sms_notification");
			$notifications_email = $this->db->query("SELECT DISTINCT DEFAULT (new_order) AS new_order FROM " . DB_PREFIX . "customer_email_notification");
		}

		if(isset($data['affiliate_id']) && (int)$data['affiliate_id'] !== 0){
			$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE affiliate_id='" . $data['affiliate_id'] . "'");
			$notifications_affiliate_sms = $this->db->query("SELECT affiliate_order FROM " . DB_PREFIX . "customer_sms_notification WHERE customer_id='" . $customer->row['customer_id'] . "'");
			$notifications_affiliate_email = $this->db->query("SELECT affiliate_order FROM " . DB_PREFIX . "customer_email_notification WHERE customer_id='" . $customer->row['customer_id'] . "'");
		}else{
			$notifications_affiliate_email = $this->db->query("SELECT DISTINCT DEFAULT (affiliate_order) AS affiliate_order FROM " . DB_PREFIX . "customer_email_notification");
			$notifications_affiliate_sms = $this->db->query("SELECT DISTINCT DEFAULT (affiliate_order) AS affiliate_order FROM " . DB_PREFIX . "customer_sms_notification");
		}


		$notification_affiliate_sms = $notifications_affiliate_sms->row['affiliate_order'];
		$notification_affiliate_email = $notifications_affiliate_email->row['affiliate_order'];
		$notification_sms = $notifications_sms->row['new_order'];
		$notification_email = $notifications_email->row['new_order'];

		if((int)$notification_sms === 1 || $notification_email === 1 || isset($data['comment']) && trim($data['comment']) !== ''){
			$notify = 1;
		}else{
			$notify = 0;
		}

		$order_history_id = $this->addOrderHistory($order_id, 0, $data['comment'], $notify);

		$this->getNewOrderAdminEmail($order_id, $order_history_id);

		if((int)$notification_affiliate_sms === 1){
			$this->getAffiliateSms($order_id);
		}

		if((int)$notification_affiliate_email === 1){
			$this->getAffiliateEmail($order_id);
		}

		if((int)$notification_sms === 1){
			$this->getNewOrderCustomerSms($order_id, $order_history_id, $data['comment'],$notify);
		}

		if((int)$notification_email === 1 || isset($data['comment']) && trim($data['comment']) !== ''){
			$this->getNewOrderCustomerEmail($order_id, $order_history_id, $data['comment'],$notify);
		}


		$this->event->trigger('post.order.add', $order_id);

		return $order_id;
	}

	public function editOrder($order_id, $data) {

		$old_status_order = $this->db->query("SELECT order_status_id FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");

		$order_datetime = $this->db->query("SELECT date_added FROM " . DB_PREFIX . "order WHERE order_id = " . (int)$order_id);
		$current_datetime = "'" . $order_datetime->row['date_added'] . "'";

		$old_status_order = $old_status_order->row['order_status_id'];

		$this->event->trigger('pre.order.edit', $data);

		if (!$this->config->get('customer_is_affiliate_status')) {
      $data['affiliate_id'] = 0;
			$data['commission'] = 0;
    }
		// Void the order first
		//No notification
		$this->addOrderHistory($order_id, 0);

		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . trim($this->db->escape($data['email'])) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(serialize($data['custom_field'])) . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', telephone_country_id = '" . $data['telephone_country_id'] . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(serialize($data['payment_custom_field'])) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(serialize($data['shipping_custom_field'])) . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_gift_product WHERE order_id = '" . (int)$order_id . "'");

		// Products
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");

				$order_product_id = $this->db->getLastId();

				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}

		if ($this->config->get('gift_status')) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_gift_product (order_id, product_id, gift_program_id, quantity)
																				SELECT " . (int)$order_id . ", gpp.product_id, gp.gift_program_id, gpp.quantity
																				FROM " . DB_PREFIX . "gift_program gp
																				INNER JOIN " . DB_PREFIX . "gift_tier gt ON gp.gift_program_id = gt.gift_program_id
																				INNER JOIN " . DB_PREFIX . "gift_product gpp ON gt.gift_tier_id = gpp.gift_tier_id
																				INNER JOIN " . DB_PREFIX . "product p ON gpp.product_id = p.product_id
																				INNER JOIN " . DB_PREFIX . "product_to_store pts ON p.product_id = pts.product_id
																				WHERE gp.status = 1
																				AND (gp.datetime_start < " . $current_datetime . " OR gp.datetime_start = '0000-00-00 00:00:00')
																				AND (gp.datetime_end > " . $current_datetime . " OR gp.datetime_end = '0000-00-00 00:00:00')
																				AND gt.price_start < " . (float)$data['totals']['sub_total']['value'] . "
																				AND (gt.price_end > " . (float)$data['totals']['sub_total']['value'] . " OR gt.price_end  IS NULL)
																				AND p.status = 1 AND p.quantity > gpp.quantity
																				AND pts.store_id = " . (int)$this->config->get('config_store_id'));
		}

		// Gift Voucher
		$this->load->model('checkout/voucher');

		$this->model_checkout_voucher->disableVoucher($order_id);

		// Vouchers
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");

				$order_voucher_id = $this->db->getLastId();

				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $voucher);

				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}

		// Totals
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");

		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}

		if((int)$old_status_order !== (int)$data['order_status_id'] || $data['comment'] !== ''){

			if (isset($data['customer_id']) && (int)$data['customer_id'] !== 0){
				$notifications_sms = $this->db->query("SELECT edit_order FROM " . DB_PREFIX . "customer_sms_notification WHERE customer_id='" . $data['customer_id'] . "'");
				$notifications_email = $this->db->query("SELECT edit_order FROM " . DB_PREFIX . "customer_email_notification WHERE customer_id='" . $data['customer_id'] . "'");
			}else{
				$notifications_sms = $this->db->query("SELECT DISTINCT DEFAULT (edit_order) AS edit_order FROM " . DB_PREFIX . "customer_sms_notification");
				$notifications_email = $this->db->query("SELECT DISTINCT DEFAULT (edit_order) AS edit_order FROM " . DB_PREFIX . "customer_email_notification");
			}

			$notification_sms = $notifications_sms->row['edit_order'];
			$notification_email = $notifications_email->row['edit_order'];

			if((int)$notification_sms === 1 || (int)$notification_email === 1 || isset($data['comment']) && trim($data['comment']) !== ''){
				$notify = 1;
			}else{
				$notify = 0;
			}

			$order_history_id = $this->addOrderHistory($order_id, 0, $data['comment'], $notify);

			if((int)$notification_sms === 1){
				$this->getEditOrderCustomerSms($order_id, $order_history_id, $data['comment'], $data['order_status_id']);
			}

			if((int)$notification_email === 1 || isset($data['comment']) && trim($data['comment']) !== ''){

				$this->getEditOrderCustomerEmail($order_id, $order_history_id, $data['comment'], $notify, $old_status_order, $data['order_status_id']);
			}

		}

		$this->event->trigger('post.order.edit', $order_id);
	}

	public function deleteOrder($order_id) {
		$this->event->trigger('pre.order.delete', $order_id);

		// Void the order first
		$this->addOrderHistory($order_id, 0);

		$this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_option` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_history` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_fraud` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE `or`, ort FROM `" . DB_PREFIX . "order_recurring` `or`, `" . DB_PREFIX . "order_recurring_transaction` `ort` WHERE order_id = '" . (int)$order_id . "' AND ort.order_recurring_id = `or`.order_recurring_id");

		$this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_transaction` WHERE order_id = '" . (int)$order_id . "'");

		// Gift Voucher
		$this->load->model('checkout/voucher');

		$this->model_checkout_voucher->disableVoucher($order_id);

		$this->event->trigger('post.order.delete', $order_id);
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_directory = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'telephone_country_id'		=> $order_query->row['telephone_country_id'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => unserialize($order_query->row['custom_field']),
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => unserialize($order_query->row['payment_custom_field']),
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_custom_field'   => unserialize($order_query->row['shipping_custom_field']),
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added']
			);
		} else {
			return false;
		}
	}

	public function addOrderHistory($order_id, $order_status_id, $comment = '', $notify = 0) {

		$this->event->trigger('pre.order.history.add', $order_id);

		$order_info = $this->getOrder($order_id);

		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);

		if ($order_info) {
			// Fraud Detection

			if ($customer_info && $customer_info['safe']) {
				$safe = true;
			} else {
				$safe = false;
			}

			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');

				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);

				if (!$safe && $risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Ban IP
			if (!$safe) {
				$status = false;

				if ($order_info['customer_id']) {
					$results = $this->model_account_customer->getIps($order_info['customer_id']);

					foreach ($results as $result) {
						if ($this->model_account_customer->isBanIp($result['ip'])) {
							$status = true;

							break;
						}
					}
				} else {
					$status = $this->model_account_customer->isBanIp($order_info['ip']);
				}

				if ($status) {
					$order_status_id = $this->config->get('config_order_status_id');
				}
			}

			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
			//, notify = '" . (int)$notify . "'
			if((int)$order_status_id !== 0){
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
			}
			$order_history_id = $this->db->getLastId();


			// If current order status is not processing or complete but new status is processing or complete then commence completing the order
			if (!in_array($order_info['order_status_id'], array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && in_array($order_status_id, array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
				// Stock subtraction
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {
					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");

					$quantity_to_subtract_option = $this->db->query("SELECT quantity_to_subtract_option FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . (int)$order_option_query->row['product_option_id'] . "'");

					$product_option_value_subtract = $this->db->query("SELECT subtract FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . $order_option_query->row['product_option_value_id'] . "'");


					if($quantity_to_subtract_option->row['quantity_to_subtract_option'] == 'one_to_many'){
						$product_option_value_quantity = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . $order_option_query->row['product_option_value_id'] . "'" );

						$quantity = $order_product['quantity'] * $product_option_value_quantity->row['quantity'];
					}else{
						$quantity = $order_product['quantity'];
					}

					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");

					if($quantity_to_subtract_option->row['quantity_to_subtract_option'] != 'one_to_many' && $product_option_value_subtract->row['subtract']){
						foreach ($order_option_query->rows as $option) {
							$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
						}
					}
				}

				if ($this->config->get('gift_status')) {
					$order_gift_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_gift_product WHERE order_id = '" . (int)$order_id . "'");

					if ($order_gift_query->num_rows) {
						foreach ($order_gift_query->rows as $gift) {
							$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$gift['quantity'] . ") WHERE product_id = '" . (int)$gift['product_id'] . "' AND subtract = '1'");
						}
					}
				}

				// Redeem coupon, vouchers and reward points
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
						$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
					}
				}

				if ($this->config->get('customer_is_affiliate_status')) {
					// Add commission if sale is linked to affiliate referral.
					if ($order_info['affiliate_id'] && $this->config->get('config_affiliate_auto')) {
						$this->load->model('affiliate/affiliate');

						$this->model_affiliate_affiliate->addTransaction($order_info['affiliate_id'], $order_info['commission'], $order_id, $notify);
					}
		    }
			}

			// If old order status is the processing or complete status but new status is not then commence restock, and remove coupon, voucher and reward history
			if (in_array($order_info['order_status_id'], array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && !in_array($order_status_id, array_merge($this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
				// Restock
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach($product_query->rows as $product) {
					$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					$quantity_to_subtract_option = $this->db->query("SELECT quantity_to_subtract_option FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . (int)$option_query->row['product_option_id'] . "'");

					$product_option_value_subtract = $this->db->query("SELECT subtract FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . $option_query->row['product_option_value_id'] . "'");

					if($quantity_to_subtract_option->row['quantity_to_subtract_option'] == 'one_to_many'){
						$product_option_value_quantity = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . $option_query->row['product_option_value_id'] . "'" );

						$quantity = $product['quantity'] * $product_option_value_quantity->row['quantity'];
					}else{
						$quantity = $product['quantity'];
					}

					$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . (int)$quantity . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

					if($quantity_to_subtract_option->row['quantity_to_subtract_option'] != 'one_to_many' && $product_option_value_subtract->row['subtract'] == 1){

						foreach ($option_query->rows as $option) {
							$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int)$quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
						}
					}
				}

				if ($this->config->get('gift_status')) {
					$order_gift_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_gift_product WHERE order_id = '" . (int)$order_id . "'");
					if ($order_gift_query->num_rows) {
						foreach ($order_gift_query->rows as $gift) {
							$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int)$gift['quantity'] . ") WHERE product_id = '" . (int)$gift['product_id'] . "' AND subtract = '1'");
						}
					}
				}

				// Remove coupon, vouchers and reward points history
				$this->load->model('account/order');

				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'unconfirm')) {
						$this->{'model_total_' . $order_total['code']}->unconfirm($order_id);
					}
				}

				// Remove commission if sale is linked to affiliate referral.
				if ($order_info['affiliate_id']) {
					$this->load->model('affiliate/affiliate');

					$this->model_affiliate_affiliate->deleteTransaction($order_id);
				}
			}

			$this->cache->delete('product');

			// If order status is 0 then becomes greater than 0 send main html email
			if (!$order_info['order_status_id'] && $order_status_id) {
				// Check for any downloadable products
				$download_status = false;

				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {
					// Check if there are any linked downloads
					$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if ($product_download_query->row['total']) {
						$download_status = true;
					}
				}

				$language = new Language($order_info['language_directory']);
				$language->load('default');
				$language->load('mail/order');

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$order_status = $order_status_query->row['name'];
				} else {
					$order_status = '';
				}
			}
		}

		// If order status in the complete range create any vouchers that where in the order need to be made available.
		if (in_array($order_info['order_status_id'], $this->config->get('config_complete_status'))) {
			// Send out any gift voucher mails
			$this->load->model('checkout/voucher');
			$this->model_checkout_voucher->confirm($order_id);
		}
        $order_total_sale_get = $this->db->query("SELECT `value` FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' and `code`='total' ORDER BY sort_order ASC");
        $getProcentSaleBonus = $this->customer->getProcentSaleBonus();
        $getProcentSaleBonusGroup = $this->customer->getProcentSaleBonusGroup();
        if($getProcentSaleBonus>0){
          $add_bonuse =   $this->calc_percent($order_total_sale_get->row['value'],$getProcentSaleBonus);
            $this->db->query("UPDATE `" . DB_PREFIX . "customer` set custom_bonus =".$add_bonuse." WHERE customer_id=".$order_info['customer_id']."  ");
        }elseif ($getProcentSaleBonus==0&&$getProcentSaleBonusGroup>0){
            $add_bonuse =   $this->calc_percent($order_total_sale_get->row['value'],$getProcentSaleBonusGroup);
            $this->db->query("UPDATE `" . DB_PREFIX . "customer` set custom_bonus =".$add_bonuse." WHERE customer_id=".$order_info['customer_id']."  ");

        }

		$this->event->trigger('post.order.history.add', $order_id);

		return $order_history_id;

	}
	private function calc_percent($value, $percent) {
        return $value * ($percent / 100);
    }

	public function getNewOrderCustomerEmail($order_id, $order_history_id, $comment = ''){

		$order_info = $this->getOrder($order_id);

		$this->load->model('account/customer');

		if (isset($order_info['customer_id']) && (int)$order_info['customer_id'] !== 0){
			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
		}

		$customer_message = $this->load->controller('payment/' . $order_info['payment_code'] . '/getPaymentMessage');

		$comment = $customer_message;

		// Check for any downloadable products
		$download_status = false;

		$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		foreach ($order_product_query->rows as $order_product) {
			// Check if there are any linked downloads
			$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

			if ($product_download_query->row['total']) {
				$download_status = true;
			}
		}


		$order_gift_query = $this->db->query("SELECT product_id, gift_program_id, quantity
		                                      FROM " . DB_PREFIX . "order_gift_product WHERE order_id = " . (int)$order_id);

			$language = new Language($order_info['language_directory']);
			$language->load('default');
			$language->load('mail/order');

		$subject = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);

		// HTML Mail
		$data = array();

		$data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

		$data['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
		$data['text_link'] = $language->get('text_new_link');
		$data['text_download'] = $language->get('text_new_download');
		$data['text_order_detail'] = $language->get('text_new_order_detail');
		$data['text_instruction'] = $language->get('text_new_instruction');
		$data['text_order_id'] = $language->get('text_new_order_id');
		$data['text_date_added'] = $language->get('text_new_date_added');
		$data['text_payment_method'] = $language->get('text_new_payment_method');
		$data['text_shipping_method'] = $language->get('text_new_shipping_method');
		$data['text_email'] = $language->get('text_new_email');
		$data['text_telephone'] = $language->get('text_new_telephone');
		$data['text_ip'] = $language->get('text_new_ip');
		$data['text_order_status'] = $language->get('text_new_order_status');
		$data['text_payment_address'] = $language->get('text_new_payment_address');
		$data['text_shipping_address'] = $language->get('text_new_shipping_address');
		$data['text_product'] = $language->get('text_new_product');
		$data['text_model'] = $language->get('text_new_model');
		$data['text_quantity'] = $language->get('text_new_quantity');
		$data['text_price'] = $language->get('text_new_price');
		$data['text_total'] = $language->get('text_new_total');
		$data['comment'] = $comment;
		$data['text_footer'] = $language->get('text_new_footer');

		$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
		$data['store_name'] = $order_info['store_name'];
		$data['store_url'] = $order_info['store_url'];
		$data['customer_id'] = $order_info['customer_id'];
		$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

		$data['download'] = '';

		$data['order_id'] = $order_id;
		$data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));
		$data['payment_method'] = $order_info['payment_method'];
		$data['shipping_method'] = $order_info['shipping_method'];
		$data['email'] = $order_info['email'];
		$data['telephone'] = $order_info['telephone'];
		$data['ip'] = $order_info['ip'];

		if ($order_info['payment_address_format']) {
			$format = $order_info['payment_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);

		$replace = array(
			'firstname' => $order_info['payment_firstname'],
			'lastname'  => $order_info['payment_lastname'],
			'company'   => $order_info['payment_company'],
			'address_1' => $order_info['payment_address_1'],
			'address_2' => $order_info['payment_address_2'],
			'city'      => $order_info['payment_city'],
			'postcode'  => $order_info['payment_postcode'],
			'zone'      => $order_info['payment_zone'],
			'zone_code' => $order_info['payment_zone_code'],
			'country'   => $order_info['payment_country']
		);

		$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

		if ($order_info['shipping_address_format']) {
			$format = $order_info['shipping_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);

		$replace = array(
			'firstname' => $order_info['shipping_firstname'],
			'lastname'  => $order_info['shipping_lastname'],
			'company'   => $order_info['shipping_company'],
			'address_1' => $order_info['shipping_address_1'],
			'address_2' => $order_info['shipping_address_2'],
			'city'      => $order_info['shipping_city'],
			'postcode'  => $order_info['shipping_postcode'],
			'zone'      => $order_info['shipping_zone'],
			'zone_code' => $order_info['shipping_zone_code'],
			'country'   => $order_info['shipping_country']
		);

		$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

		$this->load->model('tool/upload');

		// Products
		$data['products'] = array();

		foreach ($order_product_query->rows as $product) {
			$option_data = array();

			$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

			foreach ($order_option_query->rows as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			$data['products'][] = array(
				'name'     => $product['name'],
				'model'    => $product['model'],
				'option'   => $option_data,
				'quantity' => $product['quantity'],
				'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
				'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
			);
		}

		$data['gift_programs'] = [];

		if ($order_gift_query->num_rows) {
			foreach ($order_gift_query->rows as $order_gift) {
				$gift_product_query =$this->db->query("SELECT pd.name, p.image, p.model
				                                       FROM " . DB_PREFIX . "product p
																							 INNER JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id
																						   WHERE p.product_id = " . (int)$order_gift['product_id'] . " AND pd.language_id = " . (int)$order_info['language_id']);

				if ($order_product_query->num_rows) {
					$data['gift_programs'][$order_gift['gift_program_id']]['gifts'][] = [
	                                     'product_id' => $order_gift['product_id'],
	                                     'quantity' => $order_gift['quantity'],
	                                     'name'     => $gift_product_query->row['name'],
	                                     'model'    => $gift_product_query->row['model'],
	                                     'href'     => $this->url->link('product/product', 'product_id=' . $order_gift['product_id']),
	                                   ];
				}
			}

			$this->load->language('checkout/gift');

			$data['text_gift'] = $this->language->get('text_gift');
		}

		// Vouchers
		$data['vouchers'] = array();

		$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

		foreach ($order_voucher_query->rows as $voucher) {
			$data['vouchers'][] = array(
				'description' => $voucher['description'],
				'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
			);
		}

		// Order Totals
		$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

		foreach ($order_total_query->rows as $total) {
			$data['totals'][] = array(
				'title' => $total['title'],
				'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
			$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
		} else {
			$html = $this->load->view('default/template/mail/order.tpl', $data);
		}

		// Can not send confirmation emails for CBA orders as email is unknown
		$this->load->model('payment/amazon_checkout');

		if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {
			// Text Mail
			$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
			$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
			$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
			// $text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

		if ($comment /*&& $notify*/) {
				$text .= $language->get('text_new_instruction') . "\n\n";
				$text .= $comment . "\n\n";
			}

			// Products
			$text .= $language->get('text_new_products') . "\n";

			foreach ($order_product_query->rows as $product) {
				$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

				foreach ($order_option_query->rows as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
				}
			}

			foreach ($order_voucher_query->rows as $voucher) {
				$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
			}

			$text .= "\n";

			$text .= $language->get('text_new_order_total') . "\n";

			foreach ($order_total_query->rows as $total) {
				$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
			}

			$text .= "\n";

			if ($order_info['customer_id']) {
				$text .= $language->get('text_new_link') . "\n";
				$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
			}

			// Comment
			if ($order_info['comment']) {
				$text .= $language->get('text_new_comment') . "\n\n";
				$text .= $order_info['comment'] . "\n\n";
			}

	//new ORDER

			$text .= $language->get('text_new_footer') . "\n\n";

			$mail_data = array(
				// 'to' => $order_info['email'],
			);

			if ($order_info['email'] !== '') {
				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject($subject);
				$mail->setHtml($html);
				$mail->setText($text);
				$mail->send();
			}
		}
	}

	public function getNewOrderAdminEmail($order_id, $order_history_id){

		$this->load->language('mail/order');


		$order_info = $this->getOrder($order_id);

		// Admin Alert Mail
		if ($this->config->get('config_order_mail')) {
			$subject = sprintf($this->language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_id);

			// HTML Mail
			$data = array();

			$data['title'] = sprintf($this->language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

			$data['text_greeting'] = sprintf($this->language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			$data['text_link'] = $this->language->get('text_new_link');
			$data['text_download'] = $this->language->get('text_new_download');
			$data['text_order_detail'] = $this->language->get('text_new_order_detail');
			$data['text_instruction'] = $this->language->get('text_new_instruction');
			$data['text_order_id'] = $this->language->get('text_new_order_id');
			$data['text_date_added'] = $this->language->get('text_new_date_added');
			$data['text_payment_method'] = $this->language->get('text_new_payment_method');
			$data['text_shipping_method'] = $this->language->get('text_new_shipping_method');
			$data['text_email'] = $this->language->get('text_new_email');
			$data['text_telephone'] = $this->language->get('text_new_telephone');
			$data['text_ip'] = $this->language->get('text_new_ip');
			$data['text_order_status'] = $this->language->get('text_new_order_status');
			$data['text_payment_address'] = $this->language->get('text_new_payment_address');
			$data['text_shipping_address'] = $this->language->get('text_new_shipping_address');
			$data['text_product'] = $this->language->get('text_new_product');
			$data['text_model'] = $this->language->get('text_new_model');
			$data['text_quantity'] = $this->language->get('text_new_quantity');
			$data['text_price'] = $this->language->get('text_new_price');
			$data['text_total'] = $this->language->get('text_new_total');
			$data['text_footer'] = $this->language->get('text_new_footer');

			$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
			$data['store_name'] = $order_info['store_name'];
			$data['store_url'] = $order_info['store_url'];
			$data['customer_id'] = $order_info['customer_id'];
			$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

			$data['download'] = '';

			$data['order_id'] = $order_id;
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			$data['payment_method'] = $order_info['payment_method'];
			$data['shipping_method'] = $order_info['shipping_method'];
			$data['email'] = $order_info['email'];
			$data['telephone'] = $order_info['telephone'];
			$data['ip'] = $order_info['ip'];
			// $data['order_status'] = $order_status;

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->load->model('tool/upload');

			// Products
			$data['products'] = array();

			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

			foreach ($order_product_query->rows as $product) {
				$option_data = array();

				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

				foreach ($order_option_query->rows as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Vouchers
			$data['vouchers'] = array();

			$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

			foreach ($order_voucher_query->rows as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			// Order Totals
			$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

			foreach ($order_total_query->rows as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			// Can not send confirmation emails for CBA orders as email is unknown
			$this->load->model('payment/amazon_checkout');

			if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {
				// Text Mail
				$text = sprintf(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $this->language->get('text_new_subject'), html_entity_decode($order_info['order_id'], ENT_QUOTES, 'UTF-8')) . "<br>";
				$text .= "<h2>" . sprintf($this->language->get('text_order_number'), html_entity_decode($order_info['order_id'], ENT_QUOTES, 'UTF-8')) . "</h2> <br>";
				$text .= sprintf($this->language->get('text_email'), html_entity_decode($order_info['email'], ENT_QUOTES, 'UTF-8')) . "<br>";
				$text .= sprintf($this->language->get('text_telephone'), html_entity_decode($order_info['telephone'], ENT_QUOTES, 'UTF-8')) . "<br>";
				$text .= sprintf($this->language->get('text_customer'), html_entity_decode($order_info['shipping_firstname'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['shipping_lastname'], ENT_QUOTES, 'UTF-8')) . "<br>";
				$text .= sprintf($this->language->get('text_ip'), html_entity_decode($order_info['ip'], ENT_QUOTES, 'UTF-8')) . "<br><br>";
				$text .= sprintf($this->language->get('text_customer_comment'), html_entity_decode($order_info['comment'], ENT_QUOTES, 'UTF-8')) . "<br>";

				// Products
				$text .= $this->language->get('text_products') . "\n";

				foreach ($order_product_query->rows as $product) {
					$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "<br>";

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "<br>";
					}
				}

				foreach ($order_voucher_query->rows as $voucher) {
					$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
				}

				$text .= "<br>";

				$text .= $this->language->get('text_shipping') ."<br>";
				$text .= sprintf($this->language->get('text_get_shipping'), html_entity_decode($order_info['shipping_firstname'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['shipping_lastname'], ENT_QUOTES, 'UTF-8')) . "<br>";
				$text .= sprintf($this->language->get('text_address'), html_entity_decode($order_info['shipping_country'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['shipping_city'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['shipping_zone'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['shipping_address_1'], ENT_QUOTES, 'UTF-8')) . "<br> <br>";

				$text .= $this->language->get('text_payment') ."<br>";
				$text .= sprintf($this->language->get('text_payment_method'), html_entity_decode($order_info['payment_method'], ENT_QUOTES, 'UTF-8')) . "<br>";
				$text .= sprintf($this->language->get('text_get_payment'), html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8')) . "<br>";
				$text .= sprintf($this->language->get('text_address_payment'), html_entity_decode($order_info['payment_country'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['payment_city'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['payment_zone'], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['payment_address_1'], ENT_QUOTES, 'UTF-8')) . "<br> <br>";





				$text .= "<b>" . $this->language->get('text_new_order_total') . "</b> <br>";

				$end_key = end($order_total_query->rows)['code'];

				foreach ($order_total_query->rows as $key => $total) {
					if( $total['code'] === 'total'){
						$text .= "<h1>" . $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "</h1><br>";
					}else{
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "<br>";
					}
				}

				$text .= "<br>";


				if ($order_info['customer_id']) {
					$text .= $this->language->get('text_new_link') . "<br>";
					$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "<br>";
				}

				// // Comment
				// if ($order_info['comment']) {
				// 	$text .= $this->language->get('text_new_comment') . "\n\n";
				// 	$text .= $order_info['comment'] . "\n\n";
				// }

		//new ORDER

				// $text .= $this->language->get('text_new_footer') . "\n\n";

				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_email'));
				if ($order_info['email'] !== '') {
					$mail->setReplyTo($order_info['email']);
				}
				$mail->setSender($order_info['store_name']);
				$mail->setSubject($subject);
				$mail->setHtml($text);
				$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
				$mail->send();

				// Send to additional alert emails
				$emails = explode(',', $this->config->get('config_mail_alert'));

				foreach ($emails as $email) {
					if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
						$mail->setTo($email);
						$mail->send();
					}
				}
			}
		}
	}

	public function getEditOrderCustomerEmail($order_id, $order_history_id, $comment = '', $notify = 0, $old_status_order, $order_status_id = false){

		$order_info = $this->getOrder($order_id);

		if (isset($order_info['customer_id']) && (int)$order_info['customer_id'] !== 0){
			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
			// $customer_language_id = $customer_info->row['language_id'];
			$language_directory = $this->db->query("SELECT directory FROM " . DB_PREFIX . "language WHERE language_id ='" . (int)$customer_info['language_id'] . "'");
			$language = new Language($language_directory->row['directory']);
			$language->load('default');
			$language->load('mail/order');
		}else{
			$language = new Language($order_info['language_directory']);
			$language->load('default');
			$language->load('mail/order');
		}

		if((int)$order_info['order_status_id'] === 0){
			$order_status_id = $order_status_id;
		}else{
			$order_status_id = $order_info['order_status_id'];
		}

			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

			if($old_status_order !== $order_info['order_status_id']){
				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
			}else{
				$subject = sprintf($language->get('text_comment'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			}
			$data['store_url'] = $this->config->get('config_url');

			if($old_status_order !== $order_info['order_status_id']){
				$data['text_greeting'] = $language->get('text_update_order') . ' ' . $order_id;
			}else{
				$data['text_greeting'] = sprintf($language->get('text_hello'), $order_info['firstname']);
			}


			if($old_status_order !== $order_info['order_status_id']){
				$data['text_main'][] = $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";
			}

			$data['text_main'][] = $language->get('text_update_order_status') . "\n\n";
			$data['text_main'][] = $order_status_query->row['name'] . "\n\n";

			if ($order_info['customer_id']) {
				$data['text_main'][] = $language->get('text_update_link');
				$data['text_main'][] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
			}
			if ($notify && $comment) {
				$data['text_main'][] = $language->get('text_update_comment');
				$data['text_main'][] = strip_tags($comment);
			}
			$data['text_note'] = $language->get('text_update_footer');

			if (file_exists(DIR_TEMPLATE .  $this->config->get('config_template') .'/template/mail/message.tpl')) {
				$html = $this->load->view($this->config->get('config_template') .'/template/mail/message.tpl', $data);
			} else {
				$html = $this->load->view('default/template/mail/order.tpl', $data);
			}

			if ($order_info['email'] !== '') {
				$mail = new Mail($this->config->get('config_mail'));
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject($subject);
				$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
				$mail->send();

			}

			$this->db->query("UPDATE " . DB_PREFIX . "order_history SET notify = 1 WHERE order_history_id = '" . (int)$order_history_id . "'");


 	}

	public function getAffiliateEmail($order_id){

		$order_info = $this->getOrder($order_id);

		$this->load->model('affiliate/affiliate');

		$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE affiliate_id='" . $order_info['affiliate_id'] . "'");

		$affiliate = $this->model_affiliate_affiliate->getAffiliate($order_info['affiliate_id']);
		$affiliate_mail = $customer->row['email'];
		$affiliate_language_id= $customer->row['language_id'];
		$language_directory = $this->db->query("SELECT directory FROM " . DB_PREFIX . "language WHERE language_id ='" . (int)$affiliate_language_id . "'");
		$language = new Language($language_directory);
		$language->load('mail/order');

		$subject = sprintf($language->get('text_greeting_affiliate_create_order'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
		$data['store_url'] = $this->config->get('config_url');
		$data['text_greeting'] = sprintf($language->get('text_hello'), $customer->row['firstname'], $customer->row['lastname']) . "\n\n";
		$data['text_main'][] = sprintf($language->get('text_create_affiliate_order'), $this->config->get('config_url'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
		$data['text_main'][] = $language->get('text_thanks_affiliate') . "\n\n";
		$data['text_note'] = sprintf($language->get('text_footer'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) ."\n\n";

		if (file_exists(DIR_TEMPLATE .  $this->config->get('config_template') .'/template/mail/message.tpl')) {
			$html = $this->load->view($this->config->get('config_template') .'/template/mail/message.tpl', $data);
		} else {
			$html = $this->load->view('default/template/mail/order.tpl', $data);
		}

		if ($affiliate_mail !== ''){
			$mail = new Mail($this->config->get('config_mail'));
			$mail->setTo( $affiliate_mail);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($order_info['store_name']);
			$mail->setSubject($subject);
			$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
			// $mail->send();
		}
	}

	public function getNewOrderCustomerSms($order_id, $order_history_id, $comment = false){

		$order_info = $this->getOrder($order_id);

		if (isset($order_info['customer_id']) && (int)$order_info['customer_id'] !== 0){
			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
			$language_directory = $this->db->query("SELECT directory FROM " . DB_PREFIX . "language WHERE language_id ='" . (int)$customer_info['language_id'] . "'");
			$language = new Language($language_directory->row['directory']);
			$language->load('default');
			$language->load('sms/affiliate');
		}else{
			$language = new Language($order_info['language_directory']);
			$language->load('default');
			$language->load('sms/affiliate');
		}

		if (!empty($order_info['telephone'])){

			$telephone = $order_info['telephone'];
			$config_sms = $this->config->get('config_sms');
			$sms_message = sprintf($language->get('config_new_order_sms'), $order_info['order_id']);
			if(trim($comment) !== ''){
				$sms_message = $sms_message . ' ' . $language->get('text_comment');
			}

			if($this->config->get('config_sms')){

				$sms_configurations = $this->config->get($config_sms);

				if(isset($sms_configurations['sms_login']) && $sms_configurations['sms_login']){
					$sms_login = $sms_configurations['sms_login'];
				}else{
					$sms_login = '';
				}

				if(isset($sms_configurations['sms_password']) && $sms_configurations['sms_password']){
					$sms_password = $sms_configurations['sms_password'];
				}else{
					$sms_password = '';
				}

				if(isset($sms_configurations['sms_from']) && $sms_configurations['sms_from']){
					$sms_from = $sms_configurations['sms_from'];
				}else{
					$sms_from = '';
				}

				if(isset($sms_configurations['sms_request']) && $sms_configurations['sms_request']){
					$sms_request = $sms_configurations['sms_request'];
				}else{
					$sms_request = '';
				}

				$sms_array = array(
					'to' 				=> $telephone,
					'message' 	=> html_entity_decode($sms_message, ENT_QUOTES, 'UTF-8'),
					'login'			=> $sms_login,
					'from'			=> $sms_from,
					'password'  => $sms_password,
					'request'		=> $sms_request,
				);

				$sms = new SMS($this->config->get('config_sms'), $sms_array);
				$answer = $sms->send();

				$this->sendAdminEmailErrorSms($answer);
			}
		}
	}

	public function getEditOrderCustomerSms($order_id, $order_history_id, $comment, $order_status_id){

		$order_info = $this->getOrder($order_id);

		if((int)$order_info['order_status_id'] === 0){
			$order_status_id = $order_status_id;
		}else{
			$order_status_id = $order_info['order_status_id'];
		}

			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

			$order_status_name = $order_status_query->row['name'];

		if (isset($order_info['customer_id']) && (int)$order_info['customer_id'] !== 0){
			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
			$language_directory = $this->db->query("SELECT directory FROM " . DB_PREFIX . "language WHERE language_id ='" . (int)$customer_info['language_id'] . "'");
			$language = new Language($language_directory->row['directory']);
			$language->load('default');
			$language->load('sms/affiliate');
		}else{
			$language = new Language($order_info['language_directory']);
			$language->load('default');
			$language->load('sms/affiliate');
		}

		if (!empty($order_info['telephone'])){

			$language->load('sms/affiliate');
			$telephone = $order_info['telephone'];
			$config_sms = $this->config->get('config_sms');
			$sms_message = sprintf($language->get('config_edit_order_sms'), $order_id, $order_status_name);

			if(trim($comment) !== ''){
				$sms_message = $sms_message . ' '. $comment;
			}

			if($this->config->get('config_sms')){
				$sms_configurations = $this->config->get($config_sms);

				if(isset($sms_configurations['sms_login']) && $sms_configurations['sms_login']){
					$sms_login = $sms_configurations['sms_login'];
				}else{
					$sms_login = '';
				}

				if(isset($sms_configurations['sms_password']) && $sms_configurations['sms_password']){
					$sms_password = $sms_configurations['sms_password'];
				}else{
					$sms_password = '';
				}

				if(isset($sms_configurations['sms_from']) && $sms_configurations['sms_from']){
					$sms_from = $sms_configurations['sms_from'];
				}else{
					$sms_from = '';
				}

				if(isset($sms_configurations['sms_request']) && $sms_configurations['sms_request']){
					$sms_request = $sms_configurations['sms_request'];
				}else{
					$sms_request = '';
				}

				$sms_array = array(
					'to' 				=> $telephone,
					'message' 	=> html_entity_decode($sms_message, ENT_QUOTES, 'UTF-8'),
					'login'			=> $sms_login,
					'from'			=> $sms_from,
					'password'  => $sms_password,
					'request'		=> $sms_request,
				);

				$sms = new SMS($this->config->get('config_sms'), $sms_array);
				$answer = $sms->send();

				$this->sendAdminEmailErrorSms($answer);
			}
		}
	}

	public function getAffiliateSms($order_id){

		$order_info = $this->getOrder($order_id);

		$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE affiliate_id='" . $order_info['affiliate_id'] . "'");

		if (!empty($customer->row['telephone'])){
			$affiliate_language_id = $customer->row['language_id'];
			$language_directory = $this->db->query("SELECT directory FROM " . DB_PREFIX . "language WHERE language_id ='" . (int)$affiliate_language_id . "'");
			$language_directory = $language_directory->row['directory'];
			$language = new Language($language_directory);
			$language->load('sms/affiliate');
			$telephone = $customer->row['telephone'];
			$config_sms = $this->config->get('config_sms');
			$affiliate_sms_message = $language->get('text_order_with_affiliate');

			if($this->config->get('config_sms')){
				$sms_configurations = $this->config->get($config_sms);

				if($sms_configurations['sms_login']){
					$sms_login = $sms_configurations['sms_login'];
				}else{
					$sms_login = '';
				}

				if($sms_configurations['sms_password']){
					$sms_password = $sms_configurations['sms_password'];
				}else{
					$sms_password = '';
				}

				if($sms_configurations['sms_from']){
					$sms_from = $sms_configurations['sms_from'];
				}else{
					$sms_from = '';
				}

				if($sms_configurations['sms_request']){
					$sms_request = $sms_configurations['sms_request'];
				}else{
					$sms_request = '';
				}

				$sms_array = array(
					'to' 				=> $telephone,
					'message' 	=> html_entity_decode($affiliate_sms_message, ENT_QUOTES, 'UTF-8'),
					'login'			=> $sms_login,
					'from'			=> $sms_from,
					'password'  => $sms_password,
					'request'		=> $sms_request,
				);

				$sms = new SMS($this->config->get('config_sms'), $sms_array);
				// $answer = $sms->send();

				// $this->sendAdminEmailErrorSms($answer);
			}
		}
	}

	public function sendAdminEmailErrorSms($answer = array()){
		if($answer){
			if(!empty($answer)){
				$this->load->model('module/send_mail');
				$this->model_module_send_mail->SendAdminEmail($answer);
			}
		}
	}

}
