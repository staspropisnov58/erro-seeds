<?php
class ModelCheckoutCustomer extends Model implements CheckoutSteps
{
  use Autocorrect;
  public $data = [];
  public $errors = [];
  public $update_current_step = true;

  public function getCustomersGroupsId(){
    $query = $this->db->query("SELECT DISTINCT customer_group_id FROM " . DB_PREFIX . "customer_group ");

    return $query->rows;
  }

  public function load($errors)
  {
    $data = array();
    $this->load->language('checkout/checkout');

    $data['text_h2'] = $this->language->get('text_customer');

    $customer_is_logged = $this->customer->isLogged();

    if ($customer_is_logged) {
      $data['text_login'] = '';
      $this->load->model('account/customer');
      $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

      $data['text_agree'] = '';
    } else {
      $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', 'redirect=checkout/checkout', true));
      $data['text_affiliate'] = $this->language->get('text_affiliate');
      $data['customer_is_affiliate_status'] = $this->config->get('customer_is_affiliate_status');

      $data['text_agree'] = $this->language->get('text_agree');
      $data['text_warranty'] = $this->language->get('text_warranty');

      if (isset($this->data['agree'])) {
  			$data['agree'] = $this->data['agree'];
  		} else {
  			$data['agree'] = false;
  		}

      $customer_info = array();
    }

    if (isset($this->session->data['order']['customer'])) {
      $customer_in_session = $this->session->data['order']['customer'];
    } else {
      $customer_in_session = [];
    }

    $guest_fields = $this->config->get('checkout_guest_order_fields');

    if ($this->config->get('show_register_setting') == 1) {
      $this->load->model('module/register_setting');
      $fields = $this->model_module_register_setting->getCustomerRegisterFields();
    } else {
      $fields = array();
    }

    if ($fields) {
			$form = new Form('customer', $this->url->link('checkout/checkout', '', 'SSL'));
      foreach ($fields as $field) {
        if (in_array($field['name'], $guest_fields)) {
          $field['value'] = null;
          $form->addField($field);
        }
      }

			if (in_array('telephone', $guest_fields) && $this->config->get('enebled_tel_code')) {
				$this->load->model('localisation/country');
				$data['countries'] = $this->model_localisation_country->getCountries();
				$data['country_id'] = $this->config->get('config_country_id');

        $form->addField(['name' => 'telephone_country_id', 'label' => '', 'placeholder' => '', 'sort_order' => 1, 'value' => $data['country_id'], 'type' => 'select']);

        $form->setFieldsValues($customer_info);
				$form->setFieldsValues($customer_in_session);
        $form->setFieldsValues($this->data);

				$form->groupFields('telephone', 'telephone_country_id', 'telephone');
			} else {
        $form->setFieldsValues($customer_info);
				$form->setFieldsValues($customer_in_session);
        $form->setFieldsValues($this->data);
			}

			$form->setFieldsErrors($errors);
			$data['guest_form'] = $form->build();

      if ($customer_is_logged) {
        $data['register_form'] = array();
      } else {
        $data['entry_register'] = $this->language->get('entry_register');


        $form = new Form('register', '');
        foreach ($fields as $field) {
          if (!in_array($field['name'], $guest_fields)) {
            $field['value'] = null;
            $form->addField($field);
          }
        }

  			if (!in_array('telephone', $guest_fields) && $this->config->get('enebled_tel_code')) {
  				$this->load->model('localisation/country');
  				$data['countries'] = $this->model_localisation_country->getCountries();
  				$data['country_id'] = $this->config->get('config_country_id');

          $form->addField(['name' => 'telephone_country_id', 'label' => '', 'placeholder' => '', 'sort_order' => 1, 'value' => $data['country_id'], 'type' => 'select']);

          $form->setFieldsValues($this->session->data['order']['customer']);
          $form->setFieldsValues($this->data);

  				$form->groupFields('telephone', 'telephone_country_id', 'telephone');
  			} else {
if(!isset($this->session->data['order']['customer'])){
    $this->session->data['order']['customer']='';
}
  				$form->setFieldsValues($this->session->data['order']['customer']);
          $form->setFieldsValues($this->data);
  			}

  			$form->setFieldsErrors($errors);
  			$data['register_form'] = $form->build();
      }
    }

    if (isset($this->session->data['order']['next_step'])) {
      $data['text_next_step'] = $this->language->get('text_' . $this->session->data['order']['next_step']);
    }

    if (isset($this->session->data['order']['prev_step'])) {
      $data['prev_step'] = $this->session->data['order']['prev_step'];
      $data['prev_step_href'] = $this->url->link('checkout/checkout', '&back_to=' . $this->session->data['order']['prev_step'], true);
      $data['text_prev_step'] = $this->language->get('text_' . $this->session->data['order']['prev_step']);
    } else {
      $data['prev_step'] = '';
    }

    if (isset($this->session->data['warning'])) {
      $this->session->data['order']['messages']['warning_telephone_and_code'] = array('text' => $this->session->data['warning'],
                                                        'type' => 'warning');
      unset($this->session->data['warning']);
    }

    if ($this->config->get('socnetauth2_status')) {
      $data['SOCNETAUTH2_DATA'] = $this->load->controller('account/socnetauth2/showcode', array("buttons_only"=>1));
    }

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/customer.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/checkout/customer.tpl', $data);
    } else {
      return $this->load->view('default/template/checkout/template/checkout/customer.tpl', $data);
    }
  }

  public function save($data)
  {
    $errors = array();
    $this->correctValuesOf($data);

    $guest_fields = $this->config->get('checkout_guest_order_fields');
    $guest_fields[] = 'telephone_country_id';

    $this->load->model('module/validator');
    if (isset($data['register'])) {
      $fields_roles['email'] = 'customer';
    } else {
      $data = array_intersect_key($data, array_flip($guest_fields));
      $fields_roles['email'] = 'contact';
    }

    $errors = $this->model_module_validator->validate($data, $fields_roles);

    if ($errors) {
      $this->data = $data;
    } else {
      if (isset($data['register'])) {
        $this->load->model('account/customer');
        $customer_id = $this->model_account_customer->addCustomer($data);
        $this->model_account_customer->deleteLoginAttempts($data['email']);
        $this->customer->login($data['email'], $data['password']);

        $this->load->language('checkout/checkout');
        $this->session->data['order']['messages']['success_register'] = array('text' => $this->language->get('success_register'),
                                                          'type' => 'success');

        unset($data['agree']);
        unset($data['login']);
        unset($data['password']);
        unset($data['referred_by']);
      }
      $this->session->data['order']['customer'] = $data;
      $this->session->data['order']['customer']['customer_group_id'] = $this->config->get('config_customer_group_id');
      $this->session->data['order']['current_step'] = 'shipping_method';
    }

    return $errors;
  }
}
