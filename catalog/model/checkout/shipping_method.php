<?php
class ModelCheckoutShippingMethod extends Model implements CheckoutSteps
{
  use Autocorrect;
  public $data = [];
  public $errors = [];
  public $update_current_step = true;

  public function load($errors)
  {
    $data = array();
    $data['text_h2'] = $this->language->get('text_shipping_method_h2');
    $data['form_name'] = 'shipping_method';
    $data['form_action'] = $this->url->link('checkout/checkout', '', 'SSL');

    $method_data = array();

    $this->load->model('extension/extension');

    $results = $this->model_extension_extension->getExtensions('shipping');


    foreach ($results as $result) {
      if ($this->config->get($result['code'] . '_status')) {
        $this->load->model('shipping/' . $result['code']);

        $quote = $this->{'model_shipping_' . $result['code']}->getQuote([]);

        if ($quote) {
          $checked = isset($this->session->data['shipping_method']) && $this->session->data['shipping_method']['code'] === $result['code'];
          $method_data[$result['code']] = array(
            'title'      => $quote['title'],
            'note'       => $quote['note'],
            'cost'       => $quote['cost'],
            'text'       => $quote['cost'] ? $quote['text'] : $this->language->get('text_free_shipping'),
            'sort_order' => $quote['sort_order'],
            'error'      => $quote['error'],
            'checked'    => $checked,
          );

          $this->load->model('module/addition');
          $method_data[$result['code']]['additions'] = $this->model_module_addition->getAdditionForShipping($result['code']);
        }
      }
    }

    $sort_order = array();

    foreach ($method_data as $key => $value) {
      $sort_order[$key] = $value['sort_order'];
    }

    array_multisort($sort_order, SORT_ASC, $method_data);

    $this->session->data['order']['shipping_methods'] = $method_data;

    $data['shipping_methods'] = $method_data;

    $data['errors'] = $errors;

    if (isset($this->session->data['order']['next_step'])) {
      $data['text_next_step'] = $this->language->get('text_' . $this->session->data['order']['next_step']);
    }

    if (isset($this->session->data['order']['prev_step'])) {
      $data['prev_step'] = $this->session->data['order']['prev_step'];
      $data['prev_step_href'] = $this->url->link('checkout/checkout', '&back_to=' . $this->session->data['order']['prev_step'], true);
      $data['text_prev_step'] = $this->language->get('text_' . $this->session->data['order']['prev_step']);
    } else {
      $data['prev_step'] = '';
    }

    $this->session->data['order']['messages']['info_shipping'] = array('text' => $this->language->get('info_shipping'),
                                                      'type' => 'info');

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/shipping_method.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/checkout/shipping_method.tpl', $data);
    } else {
      return $this->load->view('default/template/checkout/template/checkout/shipping_method.tpl', $data);
    }
  }

  public function save($data)
  {
    $errors = array();
    if (isset($data['shipping'])) {
      $this->session->data['shipping_method'] = $this->session->data['order']['shipping_methods'][$data['shipping']];
      $this->session->data['shipping_method']['code'] = $data['shipping'];
      unset($this->session->data['order']['additions']);
      if (isset($data['additions'])) {
        foreach ($data['additions'] as $addition_id => $product_id) {
          if (isset($this->session->data['shipping_method']['additions'][$addition_id]['products'][$product_id])) {
            $this->session->data['order']['additions'][$addition_id] = $product_id;
          } else {
            $errors['error_addition_shipping'] = $this->language->get('error_addition_shipping');
            $this->session->data['order']['messages']['error_addition_shipping'] = array('text' => $this->language->get('error_addition_shipping'),
                                                              'type' => 'danger');
          }
        }
      }
      $this->session->data['order']['current_step'] = $this->update_current_step && $errors === [] ? 'shipping_address' : 'shipping_method';
    } else {
      $errors['error_shipping'] = $this->language->get('error_shipping');
      $this->session->data['order']['messages']['error_shipping'] = array('text' => $this->language->get('error_shipping'),
                                                        'type' => 'danger');
    }

    return $errors;
  }
}
