<?php
class ModelCheckoutPaymentMethod extends Model implements CheckoutSteps
{
  use Autocorrect;
  public $data = [];
  public $errors = [];
  public $update_current_step = true;

  public function load($errors)
  {
    $data = array();

    $data['text_payment_method_h2'] = $this->language->get('text_payment_method_h2');
    $data['text_choose_payment_method'] = $this->language->get('text_choose_payment_method');

    $data['form_name'] = 'payment_method';
    $data['form_action'] = $this->url->link('checkout/checkout', '', 'SSL');

    // Totals
    $total_data = array();
    $total = 0;
    $taxes = $this->cart->getTaxes();

    $this->load->model('extension/extension');

    $sort_order = array();

    $results = $this->model_extension_extension->getExtensions('total');

    foreach ($results as $key => $value) {
      $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
    }

    array_multisort($sort_order, SORT_ASC, $results);

    foreach ($results as $result) {
      if ($this->config->get($result['code'] . '_status')) {
        $this->load->model('total/' . $result['code']);

        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
      }
    }

    $method_data = $this->getPaymentMethods($total);

    $this->session->data['order']['payment_methods'] = $method_data;

    $data['payment_methods'] = $method_data;

    $data['errors'] = $errors;

    if (isset($this->session->data['order']['next_step'])) {
      $data['text_next_step'] = $this->language->get('text_' . $this->session->data['order']['next_step']);
    }

    if (isset($this->session->data['order']['prev_step'])) {
      $data['prev_step'] = $this->session->data['order']['prev_step'];
      $data['prev_step_href'] = $this->url->link('checkout/checkout', '&back_to=' . $this->session->data['order']['prev_step'], true);
      $data['text_prev_step'] = $this->language->get('text_' . $this->session->data['order']['prev_step']);
    } else {
      $data['prev_step'] = '';
    }

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/payment_method.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/checkout/payment_method.tpl', $data);
    } else {
      return $this->load->view('default/template/checkout/template/checkout/payment_method.tpl', $data);
    }
  }

  public function save($data)
  {
    $errors = array();
    if (isset($data['payment'])) {
      $this->load->language('payment/' . $data['payment']);

      $minimum = $this->config->get('checkout_' . $data['payment'] . '_min_order');

			if ($minimum > $this->cart->getTotal()) {
				$errors['error_payment_min_order'] = sprintf($this->language->get('error_min_order'), $this->currency->format($minimum));
        $this->session->data['order']['messages']['error_payment_min_order'] = array('text' => $this->language->get('error_payment_min_order'),
                                                          'type' => 'danger');
			}

			if (isset($this->session->data['order']['additions']) && $this->session->data['order']['additions']) {
				$this->load->model('module/addition');
				foreach ($this->session->data['order']['additions'] as $addition_id => $product_id) {
          if ($this->model_module_addition->checkCartDependencies($addition_id, $product_id, $data['payment'])) {

          } else {
            $errors['error_addition'] = $this->language->get('error_addition');
            $this->session->data['order']['messages']['error_addition'] = array('text' => $this->language->get('error_addition'),
                                                              'type' => 'danger');
          }

				}
			}

      if ($errors) {
        $this->session->data['order']['current_step'] = 'payment_method';
      } else {
        $this->session->data['payment_method'] = $this->session->data['order']['payment_methods'][$data['payment']];
        $this->session->data['payment_method']['code'] = $data['payment'];
        $this->session->data['order']['current_step'] = $this->update_current_step ? 'confirm' : 'payment_method';
      }
    } else {
      $errors['error_payment'] = $this->language->get('error_payment');
      $this->session->data['order']['messages']['error_payment'] = array('text' => $this->language->get('error_payment'),
                                                        'type' => 'danger');
    }

    return $errors;
  }

  public function getPaymentMethods($total)
  {
    $method_data = array();

    $this->load->model('extension/extension');

    $results = $this->model_extension_extension->getExtensions('payment');

    $recurring = $this->cart->hasRecurringProducts();

    foreach ($results as $result) {
      if ($this->config->get($result['code'] . '_status')) {
        $this->load->model('payment/' . $result['code']);

        $method = $this->{'model_payment_' . $result['code']}->getMethod($this->session->data['order']['shipping_address'], $total);

        if ($method) {
          if ($recurring) {
            if (method_exists($this->{'model_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_payment_' . $result['code']}->recurringPayments()) {
              $method_data[$result['code']] = $method;
            }
          } else {
            $method_data[$result['code']] = $method;
          }
          $checked = isset($this->session->data['payment_method']) && $this->session->data['payment_method']['code'] === $result['code'];
          $method_data[$result['code']]['checked'] = $checked;
        }
      }
    }

    $sort_order = array();

    foreach ($method_data as $key => $value) {
      $sort_order[$key] = $value['sort_order'];
    }

    array_multisort($sort_order, SORT_ASC, $method_data);

    return $method_data;
  }
}
