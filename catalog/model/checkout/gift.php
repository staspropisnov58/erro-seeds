<?php
class ModelCheckoutGift extends Model
{
  public function getGifts($subtotals, $order_id = 0)
  {
    $gifts = [];

    if ($order_id) {
      $order_datetime = $this->db->query("SELECT date_added FROM " . DB_PREFIX . "order WHERE order_id = " . (int)$order_id);
      $current_datetime = "'" . $order_datetime->row['date_added'] . "'";
    } else {
      $current_datetime = "NOW()";
    }


    $gifts_query = $this->db->query("SELECT gp.gift_program_id, gpp.product_id, gpp.quantity, pd.name AS product_name, p.image, p.model
                                     FROM " . DB_PREFIX . "gift_program gp
                                     INNER JOIN " . DB_PREFIX . "gift_tier gt ON gp.gift_program_id = gt.gift_program_id
                                     INNER JOIN " . DB_PREFIX . "gift_product gpp ON gt.gift_tier_id = gpp.gift_tier_id
                                     INNER JOIN " . DB_PREFIX . "product p ON gpp.product_id = p.product_id
                                     INNER JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id
                                     INNER JOIN " . DB_PREFIX . "product_to_store pts ON p.product_id = pts.product_id
                                     WHERE gp.status = 1
                                     AND (gp.datetime_start < " . $current_datetime . " OR gp.datetime_start = '0000-00-00 00:00:00')
                                     AND (gp.datetime_end > " . $current_datetime . " OR gp.datetime_end = '0000-00-00 00:00:00')
                                     AND gt.price_start < " . (float)$subtotals['sub_total']['value'] . "
                                     AND (gt.price_end > " . (float)$subtotals['sub_total']['value'] . " OR gt.price_end  IS NULL)
                                     AND p.status = 1 AND p.quantity > gpp.quantity
                                     AND pd.language_id = " . (int)$this->config->get('config_language_id') . "
                                     AND pts.store_id = " . (int)$this->config->get('config_store_id') . "
                                     ");


    if ($gifts_query->num_rows) {
      foreach ($gifts_query->rows as $gift) {
        if ($gift['image']) {
          $image = $this->model_tool_image->resize($gift['image'], $this->config->get('gift_image_width'), $this->config->get('gift_image_height'));
        } else {
          $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('gift_image_width'), $this->config->get('gift_image_height'));
        }

        $gifts[$gift['gift_program_id']]['gifts'][] = [
                                     'product_id' => $gift['product_id'],
                                     'quantity' => $gift['quantity'],
                                     'name'     => $gift['product_name'],
                                     'image'    => $image,
                                     'model'    => $gift['model'],
                                     'href'     => $this->url->link('product/product', 'product_id=' . $gift['product_id']),
                                   ];
      }
    }

    $data['gift_programs'] = $gifts;


    $this->load->language('checkout/gift');

    $data['text_gift'] = $this->language->get('text_gift');
    $data['text_more'] = $this->language->get('text_more');
    $data['text_quantity'] = $this->language->get('text_quantity');
    $data['text_cart_gift'] = $this->language->get('text_cart_gift');

    return $data;
  }

}
