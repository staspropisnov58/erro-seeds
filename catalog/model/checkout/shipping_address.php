<?php
class ModelCheckoutShippingAddress extends Model implements CheckoutSteps
{
  use Autocorrect;
  public $data = [];
  public $errors = [];
  public $update_current_step = true;

  public function load($errors)
  {
    $data = [];
    $collections = [];
    $default_values = [];

    $language_id = $this->config->get('config_language_id');
    $data['country_id'] = $this->config->get('config_country_id');

    $data['text_shipping_address_h2'] = $this->language->get('text_shipping_address_h2');
    $data['text_new_address'] = sprintf($this->language->get('text_new_address'), $this->url->link('account/address', '', 'SSL'));

    $data['entry_select_address'] = $this->language->get('entry_select_address');
    $data['entry_new_address'] = $this->language->get('entry_new_address');
    $data['entry_addressee'] = $this->language->get('entry_addressee');
    $data['entry_address'] = $this->language->get('entry_address');

    $this->load->model('localisation/country');
    $collections['countries'] = $this->model_localisation_country->getCountries();

    $fields = $this->config->get('checkout_' . $this->session->data['shipping_method']['code'] . '_fields');

    $default_values = [];
    $form = new Form('shipping_address', $this->url->link('checkout/checkout', '', 'SSL'));
    foreach ($fields as $field_name => &$field) {
      if ((int)$field['status'] === 0) {
        continue;
      }
      $field['name'] = $field_name;

      $field['label'] = $field['label'][$language_id];
      $field['placeholder'] = $field['placeholder'][$language_id];
      $field['helper'] = $field['helper'][$language_id];

      if (in_array($field_name, ['country_id', 'zone_id'])) {
        $collection_name = $field_name === 'country_id' ? 'countries' : 'zones';

        $field['value'] = gettype($field['value']) === 'array' ? $field['value'] : [];

        if (count($field['value']) === 1) {
          if ($field_name === 'country_id') {
            $this->load->model('localisation/zone');
            $collections['zones'] = $this->model_localisation_zone->getZonesByCountryId($field['value'][0]);
          } else {

          }
          $field['type'] = 'hidden';
          $field['value'] = $field['value'][0];

          $default_key = array_search($field['value'], array_column($collections[$collection_name], $field_name));
          $default_values[$field['name']] = ['value' => $field['value'],
                                              'label' => $field['label'],
                                              'text' => $collections[$collection_name][$default_key]['name']];

        } else {
          if ($field['value']) {
            $select_only = array_intersect(array_column($collections[$collection_name], $field_name), $field['value']);

            $field['options'] = array_intersect_key($collections[$collection_name], $select_only);
          } else {
            $field['options'] = $collections[$collection_name];
          }
          $field['value'] = '';
        }
      } else {
        $field['value'] = $field['value'][$language_id];

      }

      if ($field['label'] === '') {
        $field['label'] = $this->language->get('entry_' . $field_name);
      }
      $form->addField($field);
    }

    if (isset($this->session->data['order']['shipping_address'])) {
      $form->setFieldsValues($this->session->data['order']['shipping_address']);
    }
    $form->setFieldsValues($this->request->post);
    $form->setFieldsValues($this->data);
    $form->setFieldsErrors($errors);

    $form->addField(['name' => 'addressee', 'label' => $this->language->get('entry_addressee'), 'placeholder' => '', 'sort_order' => 1, 'value' => '', 'type' => '']);
    $form->addField(['name' => 'address', 'label' => $this->language->get('entry_address'), 'placeholder' => '', 'sort_order' => 0, 'value' => '', 'type' => '']);

    $form->groupFields('addressee', 'addressee', 'firstname', 'lastname', 'patronymic', 'telephone', 'company');
    $form->groupFields('address', 'address', 'country_id', 'zone_id', 'city', 'postcode', 'address_1', 'address_2');

    $data['form'] = $form->build();

    $data['default_values'] = $default_values;

    $data['addresses'] = [];
    if ($this->customer->isLogged()) {
      $this->load->model('account/address');
      $results = $this->model_account_address->getAddresses(0, $this->model_account_address->getTotalAddresses(), $default_values);
      if ($results) {
        if (isset($this->session->data['order']['shipping_address']['address_id'])) {
          $selected_address = $this->session->data['order']['shipping_address']['address_id'];
        } else {
          $selected_address = $this->customer->getAddressId();
        }
        foreach ($results as $address) {
          $text = implode(', ', [$address['country'], $address['zone'], $address['city'], $address['address_1'], $address['postcode'], $address['firstname'], $address['lastname']]);
          $text = str_replace(', , ', ', ', $text);
          $data['addresses'][] = ['address_id' => $address['address_id'],
                                  'text'       => $text,
                                  'selected'   => $address['address_id'] === $selected_address ? 'selected' : ''];
        }
      } else {
        $data['new_address_required'] = 1;
      }
    } else {
      $data['new_address_required'] = 0;
    }

    if (isset($this->session->data['order']['next_step'])) {
      $data['text_next_step'] = $this->language->get('text_' . $this->session->data['order']['next_step']);
    }

    if (isset($this->session->data['order']['prev_step'])) {
      $data['prev_step'] = $this->session->data['order']['prev_step'];
      $data['prev_step_href'] = $this->url->link('checkout/checkout', '&back_to=' . $this->session->data['order']['prev_step'], true);
      $data['text_prev_step'] = $this->language->get('text_' . $this->session->data['order']['prev_step']);
    } else {
      $data['prev_step'] = '';
    }

    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/shipping_address.tpl')) {
      return $this->load->view($this->config->get('config_template') . '/template/checkout/shipping_address.tpl', $data);
    } else {
      return $this->load->view('default/template/checkout/template/checkout/shipping_address.tpl', $data);
    }
  }

  public function save($data)
  {
    $errors = array();
    $shipping_address = ['address_id' => 0];

    $language_id = $this->config->get('config_language_id');
    $fields = $this->config->get('checkout_' . $this->session->data['shipping_method']['code'] . '_fields');

    foreach ($fields as $field_name => &$field) {
      if ((int)$field['status'] === 0) {
        continue;
      }

      if (in_array($field_name, ['country_id', 'zone_id'])) {
        if (count($field['value']) === 1) {
          $shipping_address[$field_name] = $field['value'][0];
        }
      } else {
        $shipping_address[$field_name] = $field['value'][$language_id];
      }
    }

    $this->correctValuesOf($data);

    if (isset($data['new_address']) || $this->customer->isLogged() === null || isset($this->session->data['order']['customer']['register'])) {
      unset($data['address_id']);
      unset($this->session->data['order']['customer']['register']);

      $shipping_address['country_id'] = 0;
      $shipping_address['country'] = '';
      $shipping_address['zone_id'] = 0;
      $shipping_address['zone'] = '';


      $this->load->model('localisation/country');

      if ($shipping_address['country_id']) {
        $country = $this->model_localisation_country->getCountry($fields['country_id']['value'][0]);
        $shipping_address['country'] = $country['name'];
      } else {
        $countries = $this->model_localisation_country->getCountries();

        $country_key = $this->findValueInCollection($countries, $data['country_id'], 'country');

        if ($country_key === false) {
          $data['coutry'] = $data['country_id'];
        } else {
          $shipping_address['country_id'] = $countries[$country_key]['country_id'];
          $shipping_address['country'] = $countries[$country_key]['name'];
        }
      }
      unset($data['country_id']);

      if ($shipping_address['country_id']) {
        $this->load->model('localisation/zone');
        $zones = $this->model_localisation_zone->getZonesByCountryId($shipping_address['country_id']);

        if ($shipping_address['zone_id']) {
          $shipping_address['zone'] = $zones[$fields['zone_id']['value'][0]]['name'];
        } else {
          $zone_key = $this->findValueInCollection($zones, $data['zone_id'], 'zone');

          if ($zone_key === false) {
            $data['zone'] = $data['zone_id'];
          } else {
            $shipping_address['zone_id'] = $zones[$zone_key]['zone_id'];
            $shipping_address['zone'] = $zones[$zone_key]['name'];
          }
        }
      } else {
        $data['zone'] = $data['zone_id'];
      }
      unset($data['zone_id']);

      $this->load->model('module/validator');

      $errors = $this->model_module_validator->validate($data);

      $shipping_address = array_merge($shipping_address, $data);
    } else {
      $shipping_address['address_id'] = $data['address_id'];
    }


    if ($errors) {
      if (isset($errors['zone'])) {
        $errors['zone_id'] = $errors['zone'];
      }
      if (isset($errors['country'])) {
        $errors['country_id'] = $errors['country'];
      }
      $this->data = $data;
    } else {
      $this->session->data['order']['shipping_address'] = $shipping_address;
      $this->session->data['order']['current_step'] = 'payment_method';
    }

    return $errors;
  }

  protected function findValueInCollection($collection, $value, $keyword) {
    $result = false;

    $result = array_search($value, array_column($collection, $keyword . '_id'));
    if ($result === false) {
      $result = array_search($value, array_column($collection, 'name'));
      if ($result === false) {
        //TODO search in dictionary
      }
    }

    return $result;
  }
}
