<?php
class ModelShippingCod extends Model {
	function getQuote($address) {
		$this->load->language('shipping/cod');

		if (!$this->config->get('cod_geo_zone_id')) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			//$quote_data remains for api and admin scripts
			$quote_data = array();

			$quote_data['cod'] = array(
				'code'         => 'cod.cod',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('cod_cost'),
				'tax_class_id' => $this->config->get('cod_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('cod_cost'), $this->config->get('cod_tax_class_id'), $this->config->get('config_tax')))
			);

			$method_data = array(
				'code'       => 'cod',
				'title'      => $this->language->get('text_title'),
				'note'       => html_entity_decode($this->config->get('cod_note' . $this->config->get('config_language_id')), ENT_QUOTES, 'UTF-8'),
				'cost'       => $this->config->get('cod_cost'),
				'tax_class_id' => $this->config->get('cod_tax_class_id'),
				'text'       => $this->currency->format($this->tax->calculate($this->config->get('cod_cost'), $this->config->get('cod_tax_class_id'), $this->config->get('config_tax'))),
				'sort_order' => $this->config->get('cod_sort_order'),
				'quote'      => $quote_data,
				'error'      => false
			);
		}

		return $method_data;
	}
}
