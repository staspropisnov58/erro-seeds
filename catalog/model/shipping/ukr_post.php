<?php
class ModelShippingUkrPost extends Model {
	function getQuote($address) {
		$this->load->language('shipping/ukr_post');

		if (!$this->config->get('ukr_post_geo_zone_id')) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['ukr_post'] = array(
				'code'         => 'ukr_post.ukr_post',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('ukr_post_cost'),
				'tax_class_id' => $this->config->get('ukr_post_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('ukr_post_cost'), $this->config->get('ukr_post_tax_class_id'), $this->config->get('config_tax')))
			);

			$method_data = array(
				'code'       => 'ukr_post',
				'title'      => $this->language->get('text_title'),
				'note'       => html_entity_decode($this->config->get('ukr_post_note' . $this->config->get('config_language_id')), ENT_QUOTES, 'UTF-8'),
				'cost'       => $this->config->get('ukr_post_cost'),
				'tax_class_id' => $this->config->get('ukr_post_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('ukr_post_cost'), $this->config->get('ukr_post_tax_class_id'), $this->config->get('config_tax'))),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('ukr_post_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}
