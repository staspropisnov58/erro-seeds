<?php
class ModelShippingNewPost extends Model {
	function getQuote($address) {
		$this->load->language('shipping/new_post');

		if (!$this->config->get('new_post_geo_zone_id')) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			//$quote_data remains for api and admin scripts
			$quote_data = array();

			$quote_data['new_post'] = array(
				'code'         => 'new_post.new_post',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('new_post_cost'),
				'tax_class_id' => $this->config->get('new_post_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('new_post_cost'), $this->config->get('new_post_tax_class_id'), $this->config->get('config_tax')))
			);

			$method_data = array(
				'code'       => 'new_post',
				'title'      => $this->language->get('text_title'),
				'note'       => html_entity_decode($this->config->get('new_post_note' . $this->config->get('config_language_id')), ENT_QUOTES, 'UTF-8'),
				'cost'       => $this->config->get('new_post_cost'),
				'tax_class_id' => $this->config->get('new_post_tax_class_id'),
				'text'       => $this->currency->format($this->tax->calculate($this->config->get('new_post_cost'), $this->config->get('new_post_tax_class_id'), $this->config->get('config_tax'))),
				'sort_order' => $this->config->get('new_post_sort_order'),
				'quote'      => $quote_data,
				'error'      => false
			);
		}

		return $method_data;
	}
}
