<?php
class ModelPaymentCodgm extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/codgm');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('codgm_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
		if (!$this->config->get('codgm_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$shipping_requirements = $this->config->get('checkout_codgm_shippings');
		$shipping = isset($this->session->data['shipping_method']['code']) ? $this->session->data['shipping_method']['code'] : '';

		if (is_null($shipping_requirements) || in_array($shipping, $shipping_requirements)) {
			$method_data = array();

			$error = '';

			$minimum = $this->config->get('checkout_codgm_min_order');

			if ($minimum > $this->cart->getTotal()) {
				$error = sprintf($this->language->get('error_min_order'), $this->currency->format($minimum));
			}

			if (isset($this->session->data['order']['additions']) && $this->session->data['order']['additions']) {
				$this->load->model('module/addition');
				foreach ($this->session->data['order']['additions'] as $addition_id => $product_id) {
					$error = $this->model_module_addition->checkCartDependencies($addition_id, $product_id, 'codgm') ? '' : $this->language->get('error_addition');
				}
			}

			$commission = $this->config->get('codgm_payment_commission');
			if ($commission) {
				$commission = $this->config->get('codgm_type') === 'F' ? $this->currency->format($commission) : $commission . '%';
			}

			if ($status) {
				$method_data = array(
					'code'       => 'codgm',
					'title'      => $this->language->get('text_title'),
					'terms'      => $this->config->get('codgm_terms' . $this->config->get('config_language_id')),
					'sort_order' => $this->config->get('codgm_sort_order'),
					'error'      => $error,
					'commission'  => $commission,
				);

				return $method_data;
			}
		} else {
			if (isset($this->session->data['payment_method']['code']) && $this->session->data['payment_method']['code'] === 'codgm') {
				unset($this->session->data['payment_method']);
			}
		}
	}
}
