<?php
class ModelExtensionModule extends Model {
	public function getModule($module_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE module_id = '" . (int)$module_id . "'");

		if ($query->row) {
			$setting = unserialize($query->row['setting']);
			$setting['module_id'] = $module_id;
			return $setting;
		} else {
			return array();
		}
	}
}
