<?php
$_['heading_title'] = 'Register Account';
$_['meta_description'] = '';
$_['text_account'] = 'Account';
$_['text_register'] = 'Register';
$_['text_account_already'] = '<a href="%s" class="color-theme">login</a><span class="fa-user-o"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 350 350" style="width:25px; fill:#fff" xml:space="preserve">
<g>
	<path d="M175,171.173c38.914,0,70.463-38.318,70.463-85.586C245.463,38.318,235.105,0,175,0s-70.465,38.318-70.465,85.587
		C104.535,132.855,136.084,171.173,175,171.173z"/>
	<path d="M41.909,301.853C41.897,298.971,41.885,301.041,41.909,301.853L41.909,301.853z"/>
	<path d="M308.085,304.104C308.123,303.315,308.098,298.63,308.085,304.104L308.085,304.104z"/>
	<path d="M307.935,298.397c-1.305-82.342-12.059-105.805-94.352-120.657c0,0-11.584,14.761-38.584,14.761
		s-38.586-14.761-38.586-14.761c-81.395,14.69-92.803,37.805-94.303,117.982c-0.123,6.547-0.18,6.891-0.202,6.131
		c0.005,1.424,0.011,4.058,0.011,8.651c0,0,19.592,39.496,133.08,39.496c113.486,0,133.08-39.496,133.08-39.496
		c0-2.951,0.002-5.003,0.005-6.399C308.062,304.575,308.018,303.664,307.935,298.397z"/>
</g>
</svg></span>';
$_['text_your_details'] = 'Your Personal Details';
$_['text_your_address'] = 'Your Address';
$_['text_newsletter'] = 'Newsletter';
$_['text_your_password'] = 'Your Password';
$_['text_agree'] = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname'] = 'First Name';
$_['entry_lastname'] = 'Last Name';
$_['entry_email'] = 'E-Mail';
$_['entry_telephone'] = 'Telephone';
$_['entry_fax'] = 'Fax';
$_['entry_company'] = 'Способы упаковки';
$_['entry_address_1'] = 'Address 1';
$_['entry_address_2'] = 'Address 2';
$_['entry_postcode'] = 'Post Code';
$_['entry_city'] = 'City';
$_['entry_country'] = 'Country';
$_['entry_zone'] = 'Region / State';
$_['entry_newsletter'] = 'Subscribe';
$_['entry_password'] = 'Password';
$_['entry_confirm'] = 'Password Confirm';
$_['all_fields_are_required'] = 'All fields are required';
$_['button_register_acc'] = 'Sign Up';
$_['error_exists'] = 'Warning: E-Mail Address is already registered!';
$_['error_firstname'] = 'First Name must be between 1 and 32 characters!';
$_['error_g-recaptcha-response'] = 'Captcha';
$_['error_g-recaptcha-response-from-server'] = 'Вы точно человек?';
$_['error_lastname'] = 'Last Name must be between 1 and 32 characters!';
$_['error_email'] = 'E-Mail Address does not appear to be valid!';
$_['error_telephone'] = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1'] = 'Address 1 must be between 3 and 128 characters!';
$_['error_city'] = 'City must be between 2 and 128 characters!';
$_['error_postcode'] = 'Postcode must be between 2 and 10 characters!';
$_['error_country'] = 'Please select a country!';
$_['error_zone'] = 'Please select a region / state!';
$_['error_custom_field'] = '%s required!';
$_['error_password'] = 'Password must be between 4 and 20 characters!';
$_['error_confirm'] = 'Passwords do not match';
$_['error_agree'] = 'Warning: You must agree to the %s!';
$_['text_affiliate'] = 'I want to participate in the referral program';
$_['text_referred_by'] = 'Login (who sent)';
$_['entry_login'] = 'Login';
$_['entry_referred_by'] = 'Who sent (user\'s login) leave this field blank if you are in doubt';
$_['error_exists_login'] = 'This Login is already registered!';
$_['text_error_login'] = 'Login is entered incorrectly!';
$_['error_referred_by'] = 'Login can not exceed 255 characters';
$_['error_no_exists_login_referred_by'] = 'User with such login does not exist';
$_['error_login_not_affiliate'] = 'The user is not a referral!';
?>
