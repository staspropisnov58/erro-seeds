<?php
$_['heading_title'] = 'Newsletter Subscription';
$_['text_account'] = 'Account';
$_['text_newsletter'] = 'Newsletter';
$_['text_success'] = 'Your newsletter subscription has been successfully updated!';
$_['entry_newsletter'] = 'Subscribe';
$_['text_notification_sms'] = 'SMS';
$_['text_notification_email'] = 'EMAIL';
$_['text_for_default'] = 'Default settings';
$_['entry_review_moderation'] = 'Receive notifications about moderating reviews';
$_['entry_affiliate_order'] = 'Receive notifications about new orders for my referral link';
$_['entry_affiliate_bonus'] = 'Get notified about bonuses';
$_['entry_affiliate_payout'] = 'Receive notifications of new payments';
$_['entry_new_order'] = 'Receive notifications when creating a new order';
$_['entry_edit_order'] = 'Receive notifications when the order status changes';
$_['entry_new_review_reply'] = 'Receive notifications about replies to my reviews';
$_['text_notification_settings'] = 'Customize notifications';
$_['text_notification_title'] = 'Notify me in';
?>