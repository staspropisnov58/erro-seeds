<?php
$_['heading_title'] = 'Forgot Your Password?';
$_['meta_description'] = '';
$_['text_account'] = 'Account';
$_['text_forgotten'] = 'Forgotten Password';
$_['text_your_email'] = 'Your E-Mail Address';
$_['text_email'] = 'Enter the e-mail address associated with your account. Click submit to have your password e-mailed to you.';
$_['text_success'] = 'Success: A new password has been sent to your e-mail address.';
$_['entry_email'] = 'E-Mail Address';
$_['error_email'] = 'Warning: The E-Mail Address was not found in our records, please try again!';
$_['entry_password'] = 'New Password';
$_['entry_confirm'] = 'Confirm';
$_['error_approved'] = 'Warning: Your account requires approval before you can login.';
$_['error_password'] = 'Password must be between 4 and 20 characters!';
$_['error_confirm'] = 'Password and password confirmation do not match!';
?>
