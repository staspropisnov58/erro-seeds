<?php
$_['heading_title'] = 'Shopping Cart';
$_['text_success'] = 'You have added to your shopping cart!';
$_['text_remove'] = 'Success: You have modified your shopping cart!';
$_['text_login'] = 'Attention: You must <a href="%s">login</a> or <a href="%s">create an account</a> to view prices!';
$_['text_items'] = 'Products';
$_['text_points'] = 'Reward Points: %s';
$_['text_next'] = 'What would you like to do next?';
$_['text_next_choice'] = 'Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.';
$_['text_empty'] = 'Your shopping cart is empty!';
$_['text_day'] = 'day';
$_['text_week'] = 'week';
$_['text_semi_month'] = 'half-month';
$_['text_month'] = 'month';
$_['text_year'] = 'year';
$_['text_trial'] = '%s every %s %s for %s payments then ';
$_['text_recurring'] = '%s every %s %s';
$_['text_length'] = ' for %s payments';
$_['text_until_cancelled'] = 'until cancelled';
$_['text_recurring_item'] = 'Recurring Item';
$_['text_payment_recurring'] = 'Payment Profile';
$_['text_trial_description'] = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel'] = '%s every %d %s(s) until canceled';
$_['column_image'] = 'Image';
$_['column_name'] = 'Product Name';
$_['column_model'] = 'Model';
$_['column_quantity'] = 'Quantity';
$_['column_price'] = 'Unit Price';
$_['column_total'] = 'Total';
$_['error_stock'] = 'Products marked with *** are not available in the desired quantity or not in stock!';
$_['error_minimum'] = 'Minimum order amount for %s is %s!';
$_['error_required'] = '%s required!';
$_['error_product'] = 'Warning: There are no products in your cart!';
$_['error_recurring_required'] = 'Please select a payment recurring!';
$_['text_quantity'] = 'Quantity';
$_['text_customer'] = 'Customer\'s info';
$_['text_shipping_method'] = 'Shipment and packing';
$_['error_min_order'] = 'Order amount must be at least %s';
$_['button_remove_item'] = 'Delete item';
$_['text_commission'] = 'Commission (%s)';
$_['text_product_deleted'] = '<p>You have removed all items from your cart. </p> <p> <a href="%s"> Click here to return to the site </a></p>';
$_['text_checkout'] = 'Checkout';
$_['text_quick_order'] = 'Quick order';
$_['text_normal_order'] = 'Regular order';
$_['text_shipping_address'] = 'Delivery address ';
$_['text_payment_method'] = 'Payment';
$_['text_confirm'] = 'Confirm your order';
$_['text_send'] = 'Send';
$_['text_no_js'] = 'JavaScript must be enabled to place a quick order!';
$_['text_empty_cart'] = '<p>There seems to be nothing in your cart.</p>
               <p>Didn\'t find what you were looking for? <span class = "open_jivo" onclick = "jivo_api.open ()">Contact our consultant.</span></p>
               <p>To save your shopping cart until your next visit, <a href="%s"> create an account </a> or <a href="%s"> register </a>. </p>
<p class = "mt-3"> <b> Browse our <a href="/hits/"> Hits </a> sales </b> </p>';
$_['error_maximum_exeeded'] = 'You can purchase no more than %d units of this product';


$_['text_bonus_balans'] = 'Your Bonus Balance';
$_['text_bonus_balans_sel'] = '(maximum percentage for writing off %s % of the order amount)';
$_['text_bonus_balans_but'] = 'Apply';

?>