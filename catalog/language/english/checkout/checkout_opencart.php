<?php
$_['column_quantity'] = 'Quantity';
$_['column_total'] = 'Amount';
$_['entry_telephone'] = 'Phone';
$_['entry_postcode'] = 'Post Code';
$_['error_postcode'] = 'Postcode must be between 2 and 10 characters!';
$_['entry_country'] = 'Country';
?>