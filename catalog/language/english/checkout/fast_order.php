<?php
$_['entry_telephone'] = 'Phone';
$_['text_h1'] = 'Quick order';
$_['heading_title'] = 'Quick order';
$_['text_note'] = 'Leave your phone number and our manager will contact you to clarify the details of the order and calculate the cost of delivery';
$_['button_submit'] = 'Send';
?>