<?php
$_['text_order_with_affiliate'] = 'Your order was sent to your referral link. Details in the personal account on the site. Sincerely, Errors Seeds';
$_['config_new_order_sms'] = 'Your order № %s was accepted.';
$_['config_edit_order_sms'] = 'Your order № %s was updated with the status of %s';
