<?php
$_['text_refine'] = 'Refine Search';
$_['text_product'] = 'Products';
$_['text_error'] = 'Category not found!';
$_['text_empty'] = 'There are no products to list in this category.';
$_['text_quantity'] = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model'] = 'Product Code:';
$_['text_points'] = 'Reward Points:';
$_['text_price'] = 'Price:';
$_['text_tax'] = 'Ex Tax:';
$_['text_compare'] = 'Product Compare (%s)';
$_['text_sort'] = 'Sort By:';
$_['text_default'] = 'Default';
$_['text_name_asc'] = 'Name (A - Z)';
$_['text_name_desc'] = 'Name (Z - A)';
$_['text_price_asc'] = 'Price (Low &gt; High)';
$_['text_price_desc'] = 'Price (High &gt; Low)';
$_['text_rating_asc'] = 'Rating (Lowest)';
$_['text_rating_desc'] = 'Rating (Highest)';
$_['text_model_asc'] = 'Model (A - Z)';
$_['text_model_desc'] = 'Model (Z - A)';
$_['text_limit'] = 'Show:';
$_['text_file_download'] = 'Retail price list';
$_['text_all_category_x'] = 'All cannabis seeds';
$_['text_show_more'] = 'Show More';
$_['text_show_all'] = 'Show all';
$_['button_entrance'] = 'Inform when available';
$_['full_text'] = 'Read all';
$_['text_buy'] = 'Buy';
$_['text_instock'] = 'Available';
$_['sender_text'] = 'Send';
$_['entry_pre_order'] = 'Preorder';
$_['entry_tel'] = 'Phone';
$_['send_title_hed'] = 'Leave your email and we will definitely send you a notification when ES LED 300 - Full Spectrum is back in stock';
$_['send_quantity_prod'] = 'Item quantity';
?>