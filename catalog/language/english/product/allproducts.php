<?php
$_['text_refine'] = 'Select a subcategory';
$_['text_product'] = 'Items';
$_['text_error'] = 'Category not found!';
$_['text_empty'] = 'There are no products in this category.';
$_['text_quantity'] = 'Quantity:';
$_['text_manufacturer'] = 'Manufacturer:';
$_['text_model'] = 'Model:';
$_['text_points'] = 'Bonus points:';
$_['text_price'] = 'Price:';
$_['text_tax'] = 'No VAT:';
$_['text_compare'] = '%s';
$_['text_sort'] = 'Sort by:';
$_['text_default'] = 'Default order';
$_['text_name_asc'] = 'Name (A-Z)';
$_['text_name_desc'] = 'Name (Z-A)';
$_['text_price_asc'] = 'Price (low &gt; high)';
$_['text_price_desc'] = 'Price (high &gt; low)';
$_['text_rating_asc'] = 'Rating (low to high)';
$_['text_rating_desc'] = 'Rating (high to low)';
$_['text_model_asc'] = 'Model (A - Z)';
$_['text_model_desc'] = 'Model (Z - A)';
$_['text_limit'] = 'Show:';
$_['text_home'] = 'Home';
?>