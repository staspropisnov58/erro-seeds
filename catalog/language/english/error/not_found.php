<?php
// Heading
$_['heading_title'] = 'PAGE IS NOT FOUND';
$_['text_not_found'] = 'PAGE IS NOT FOUND';

// Text
$_['text_error']    = '<div class="error-form-text">
<p class="title">ERROR CAN BE CAUSED BY THE FOLLOWING:</p>
<p>- You entered the wrong address.</p>
<p>- The page is out of date and removed.</p>
<p>- Time for promotion ended.</p>
<p>- Serverfailure. We already know about it and work to fix it.</p>
</div>
<div class="error-form-text">
<p>To find any information, use the </p>
<p>- search box</p>
<p>- navigational menu</p>
<p>- or go to the <a href="/product">category page</a></p>
</div>';

$_['text_page_not_found'] = '<p class="title">DO NOT FORGET TO USE YOUR DISCOUNTCOUPON!</p>
<img src="/catalog/view/theme/errorseeds/images/404-coupon-eng.jpg" alt="Coupon 5% [404ESUKR]">
<p>This coupon can be used for purchase in our online store.</p>
<p>Do you have any questions? <a href="/contact-us">Please, contact us</a></p>';
