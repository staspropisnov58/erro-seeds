<?php
// Text
$_['text_items']    = '%s';
$_['text_empty']    = 'Your shopping cart is empty!';
$_['text_cart']     = 'View Cart';
$_['text_checkout'] = 'Checkout';
$_['text_recurring']  = 'Payment Profile';

$_['text_quantity_pack'] = 'Количество в пачке:';
$_['text_quantity'] = 'Количество';
$_['text_size'] = 'Размер:';
$_['text_color'] = 'Цвет:';
$_['text_enter_promocode'] = 'Ввести промокод';
$_['text_remove_item'] = 'Удалить товар';
$_['text_refresh'] = 'Обновить';
$_['text_apply_coupon'] = 'Применить купон';
$_['text_quick_order'] = 'Быстрый заказ';
$_['text_normal_order'] = 'Обычный заказ';
$_['text_in_package'] = 'в упаковке';
$_['text_no_js'] = 'Для оформления быстрого заказа необходимо включить JavaScript!';
$_['text_telephone'] = 'Телефон';
