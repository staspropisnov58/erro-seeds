<?php
$_['text_information'] = 'Information';
$_['text_service'] = 'Customer Service';
$_['text_extra'] = 'Extras';
$_['text_contact'] = 'Contact Us';
$_['text_return'] = 'Returns';
$_['text_sitemap'] = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher'] = 'Gift Vouchers';
$_['text_affiliate'] = 'Affiliates';
$_['text_special'] = 'Specials';
$_['text_account'] = 'My Account';
$_['text_order'] = 'Order History';
$_['text_wishlist'] = 'Wish List';
$_['text_compare_forum'] = 'Forum';
$_['text_newsletter'] = 'Newsletter';
$_['text_newsletter_text'] = 'Enter your email address';
$_['text_subcribe'] = 'Subcribe';
$_['text_error_subcribe'] = 'You have already subcribed newsletter!';
$_['text_success_subcribe'] = 'You have successfuly subcribe newsletter!';
$_['text_email_not_validate'] = 'You entered an incorrect Email !';
$_['contact_us'] = '<a href="/contact-us">Contacts</a>';
$_['feedbacs_x'] = '<a href="/testimonial">Testimonial</a>';
$_['entry_firstname'] = 'First Name';
$_['entry_email'] = 'E-Mail';
$_['entry_comment'] = 'Сomment';
$_['title_feedback'] = 'Leave a comment';
$_['button_modal_save'] = 'Send';
$_['rating'] = 'Rate this product';
$_['contact_us_button'] = 'Contact <br>us';
$_['join_us'] = 'JOIN US';
$_['about_us'] = 'About us';
$_['questions'] = 'Questions';
$_['payment_methods'] = 'Payment methods';
$_['delivery'] = 'Delivery';
$_['replacement_and_return'] = 'Replacement and return';
$_['text_powered'] = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
$_['entrance_title'] = 'Inform when available';
$_['entrance_btn'] = 'Send';
$_['entry_quantity'] = 'Quantity of products';
$_['entry_date_expired'] = 'Your request is up to date';
$_['text_blog'] = 'Our Blog';
$_['text_pay'] = 'Payment';
$_['text_delivery'] = 'Delivery';
$_['text_medical'] = 'Medicine';
$_['text_abuot_shop'] = 'About Our Store';
$_['text_seeds'] = 'Seeds';
$_['text_social_networks'] = 'Our Social Profiles';
$_['text_footer'] = 'Made with ♥ for great people';
$_['text_ES'] = '© Errors Seeds Company 2009-';
$_['text_social_networks'] = 'Follow Us';
$_['text_ES2'] = ' <a href="http://errors-seeds.com.ua/"> Cannabian Seeds </a>';
$_['address_2'] = 'Ukraine, Kharkiv,<br> Svobody Square, 25';
$_['text_contact'] = 'Contacts';

?>
