<?php
$_['text_home'] = 'Home';
$_['text_wishlist'] = '%s';
$_['text_compare'] = '%s';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category'] = 'Categories';
$_['text_account'] = 'My Account';
$_['text_register'] = 'Register';
$_['text_login'] = 'Login';
$_['text_order'] = 'Order History';
$_['text_transaction'] = 'Transactions';
$_['text_download'] = 'Downloads';
$_['text_logout'] = 'Logout';
$_['text_checkout'] = 'Checkout';
$_['text_search'] = 'Search';
$_['text_all'] = 'See All';
$_['text_H1_all'] = 'All cannabis seeds';
$_['user_account_x'] = 'Personal room';
$_['title_wishlist'] = 'Wishlist';
$_['title_compare'] = 'Compare';
$_['text_about_us'] = 'About us';
$_['text_about_us_feedbacs'] = 'Reviews';
$_['text_about_us_contacts'] = 'Contacts';
$_['text_about_us_sequrity'] = 'Security';
$_['text_about_us_faq'] = 'FAQ';
$_['text_tariffs'] = 'Operator tariffs';
$_['text_compare_catalog'] = 'Catalog';
$_['text_compare_partnership'] = 'Partnership';
$_['text_compare_news'] = 'News';
$_['text_compare_promotions'] = 'Action';
$_['text_compare_forum'] = 'Forum';
$_['text_modal_agreement_h1'] = '<p>Agreement</p>';
$_['text_modal_agreement'] = '
                                      <p> Dear visitors and customers of the site errors-seeds.com.ua, we inform you that our company specializes exclusively in selling souvenirs, and all the acquired items can be used by clients only under the current legislation of Ukraine. </ p>
                                      <p> The products of our company are intended for use exclusively as souvenirs, gifts, collectibles, pet food, for making amulets and other charms, and creating fishing lures. </ p>
                                      <p> Additionally, we note that our company is not responsible for the further use of souvenirs purchased on the site. </ p>
                                      <p> All the data contained on the site are provided by the company in the form of scientific references, are public and informative. </ p>
                                      <p> Simultaneously, we explain that the use of our products for other purposes is illegal and can lead to negative consequences (e.g. legal liability, including administrative and criminal liability). </ p>
                                      <p> We strongly recommend you read more about in the section "Law" of our website. </ p> ';
$_['text_modal_agreement_yes'] = 'Yeah, I turned 18';
$_['text_modal_agreement_no'] = 'No, I\'m not 18 yet';
$_['text_sale'] = 'SALE';
$_['button_callback'] = 'Callback';
$_['text_payment'] = 'Payment';
$_['text_delivery'] = 'Delivery';
$_['text_about_shop'] = 'About Our Store';
$_['text_fast_order'] = 'Selection of varieties';
$_['text_cooperation'] = 'Partnership Program';
$_['text_seeds'] = 'Seeds';
?>
