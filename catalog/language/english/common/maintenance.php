<?php
// Heading
$_['heading_title']    = 'Maintenance';

// Text
$_['text_maintenance'] = 'Maintenance';
$_['text_message']     = '
<p class="big">Dear customers!</p>
<p class="big">Our website is temporarily not working for technical reasons</p>
<div class="grey_holder"><p>You still can order:</p>
<p>• by phone: <a href="tel:+380930000849">+38 093 00-00-849</a> <a href="tel:+380950000849">+38 095 00-00-849</a> <a href="tel:+380960000849">+38 096 00-00-849</a><br>
• by writing to us in the messenger:
<a href="viber://chat?number=80930000849" target="_blank"><img src="/images/viberPNG.png" alt="viber" style="width:20px"> Viber (80930000849)</a>,
<a target="_blank" href="#" rel="https://t.me/Errors_Seeds"><img src="/images/teleg.png" alt="ES telegram" style="width:20px"> Telegram (@ErrorsSeeds)</a><br>
• through live chat</p>
<p>We are fixing the problem! Tomorrow we will be on full alert and fully operational!</p>
<p style="text-align:right"><i>Sincerely, Errors Seeds!</i></p></div>
';
