<?php
// Text

$_['text_subject']  = '%s - New Password';

$_['text_greeting'] = 'A new password was requested from %s.';

$_['text_main'] = 'Вами был запрошен новый пароль на сайте компании <a href="%s">%s</a>. </br> Для сброса пароля перейдите по ссылке: %s </br> Запрос нового пароля произведён с данного IP: %s';

$_['text_note'] = 'Если вы не делали этого, то свяжитесь с нами по тел. %s или напишите на почту <a href="mailto:%s">%s</a>';
