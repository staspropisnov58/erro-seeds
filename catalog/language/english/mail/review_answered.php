<?php
$_['text_subject'] = 'Пришел ответ на отзыв';
$_['text_hello']                = 'Здравствуйте, %s!';
$_['theme_review_moderated']    = 'Ваш отзыв прошел модерацию';
$_['text_review_moderated_x1']  = 'Посмотреть его вы можете нажав по этой <a href="%s">ссылке</a>';
$_['text_answear_comment']      = '%s написал комментарий к вашему комментарию';

$_['text_footer']               = 'С уважением, %s';
