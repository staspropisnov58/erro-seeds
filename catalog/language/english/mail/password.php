<?php
$_['text_subject'] = '%s - password changed';
$_['text_greeting'] = 'Hello %s.';
$_['text_main'] = 'You recently changed your password from the account in <a href="%s">%s</a>';
$_['text_note'] = 'If you did not make this change and believe that your account was hacked, <a href="%s"> reset your password </a> and email technical support <a href="mailto:%s">%s</a>';
?>
