<?php
// Heading
$_['heading_title']    = 'Affiliate Tracking';

// Text
$_['text_account']     = 'Account';
$_['text_description'] = 'To make sure you get paid for referrals you send to us we need to track the referral by placing a tracking code in the URL\'s linking to us. You can use the tools below to generate links to the %s web site.';
$_['text_new_affiliate_button'] = 'Подписаться';
$_['text_affiliate_comission'] = 'Комиссия (%)';
$_['text_balance']       = 'Ваш текущий баланс';
$_['text_success'] = 'Поздравляем, Вы зарегистрированы в реферальной программе';

// Entry
$_['entry_code']       = 'Your Tracking Code';
$_['entry_generator']  = 'Tracking Link Generator';
$_['entry_link']       = 'Tracking Link';

// Help
$_['help_generator']  = 'Type in the name of a product you would like to link to';
