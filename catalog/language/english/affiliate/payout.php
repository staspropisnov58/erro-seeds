<?php
// Heading
$_['heading_title']      = 'Payout history';

// Column
$_['column_date_added']  = 'Added';
$_['column_description'] = 'Description';
$_['column_amount']      = 'Suma (%s)';

// Text
$_['text_account']       = 'Personal Area';
$_['text_transaction']   = 'Bonuses';
$_['text_balance']       = 'Your current balance';
$_['text_empty']         = 'You did not have payments!';
$_['text_payout']        = 'Payments';
