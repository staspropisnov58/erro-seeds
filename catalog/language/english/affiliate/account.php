<?php
// Heading
$_['heading_title']        = 'Partners Department';

// Text
$_['text_account']         = 'Partner Account';
$_['text_my_account']      = 'My account';
$_['text_my_tracking']     = 'My referrals';
$_['text_my_transactions'] = 'Payment History';
$_['text_edit']            = 'Change account info';
$_['text_password']        = 'Change your password';
$_['text_payment']         = 'Change payment info';
$_['text_tracking']        = 'Referral Code';
$_['text_transaction']     = 'Check payment history';
