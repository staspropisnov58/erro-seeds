<?php
// Heading
$_['heading_cartpopup_title_empty']		    = 'Shopping basket';
$_['heading_cartpopup_title_nalogka']		    = 'Dear customers, we remind you that cash on delivery is possible with the order amount from <b> 250.00 UAH </ b>';

// Button
$_['button_shopping']		   				= 'Continue shopping';
$_['button_checkout']		   				= 'Checkout';

// Text
$_['text_cartpopup_empty']       			= 'Your shopping cart is empty.';
$_['text_cartpopup_points']      			= 'Бонусные баллы: %s';
$_['textcart_0']  			 				= 'Your shopping cart is empty.';
$_['textcart_1']   		 					= 'В корзине %s товар';
$_['textcart_2']   		 					= 'В корзине %s товара';
$_['textcart_3']  			 				= 'В корзине %s товаров';
$_['text_items']     						= '%s товар(ов) - %s';

// Error
$_['error_stock']                    		= 'Товары отмеченные *** отсутствуют в нужном количестве или их нет на складе!';
$_['error_minimum']                  		= 'Минимальное количество для заказа товара %s составляет %s!';
$_['error_warning_min_summ']                  		= 'Minimum order';
?>
