<?php
$_['email'] = 'email';
$_['title'] = 'Leave your email and we will definitely send you a notification when %s is back in stock';
$_['title2'] = 'Leave your email and our manager will contact you as soon as possible.';
$_['quantity'] = 'quantity';
$_['date'] = 'date';
$_['error_rangelength'] = 'The length of %s field must be between %d and %d symbols';
$_['error_length'] = 'Only %s';
$_['error_regexp'] = 'Check validity of %s field';
$_['error_minmax'] = 'Value of %s field must be greater than %d and smaller then %d';
$_['error_function'] = 'Value of %s field is incorrect';
$_['success'] = 'Your request has been successfully submitted. If %s appears in stock before %s, we will contact you';
$_['warning'] = 'You have already made a request to receive this product';
$_['button_submit_entrance'] = 'Send';
?>