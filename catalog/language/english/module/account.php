<?php
// Heading
$_['heading_title']    = 'Account';

// Text
$_['text_register']    = 'Register';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Forgotten Password';
$_['text_account']     = 'My Account';
$_['text_edit']        = 'Edit Account';
$_['text_password']    = 'Password';
$_['text_address']     = 'Address Books';
$_['text_wishlist']    = 'Wish List';
$_['text_order']       = 'Order History';
$_['text_download']    = 'Downloads';
$_['text_reward']      = 'Reward Points';
$_['text_return']      = 'Returns';
$_['text_transaction'] = 'Transactions';
$_['text_newsletter']  = 'Newsletter';
$_['text_recurring']   = 'Recurring payments';
$_['text_bonuses'] = 'Bonuses';
$_['text_payouts'] = 'Payouts';
$_['text_unclosed_orders'] = 'Unclosed orders';
$_['text_affiliate'] = 'Referral program';
$_['text_my_account']   = 'MY ACCOUNT';
$_['text_my_account_group'] = 'Account Type';
$_['text_my_account_sale'] = 'Your Discount';
$_['text_my_account_bonus'] = 'Bonus Account';
$_['text_my_account_order_old'] = 'Total amount of all orders:';
