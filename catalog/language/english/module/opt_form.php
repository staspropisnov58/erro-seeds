<?php
$_['title'] = 'Leave a request right now!';

$_['entry_name'] = 'Your name';
$_['entry_telephone'] = 'Your phone number';
$_['entry_message'] = 'Your question or message';
$_['button_submit_entrance'] = 'Send message';
$_['success'] = 'Your application has been successfully sent';
