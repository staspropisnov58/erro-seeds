<?php
$_['heading_title'] = 'User partner program';
$_['text_register'] = 'Registration';
$_['text_login'] = 'Login';
$_['text_logout'] = 'Logout';
$_['text_forgotten'] = 'Forgot the password?';
$_['text_account'] = 'My information';
$_['text_edit'] = 'Edit personal information';
$_['text_password'] = 'Password';
$_['text_payment'] = 'Payment methods';
$_['text_tracking'] = 'Referral code';
$_['text_transaction'] = 'Transactions';
?>