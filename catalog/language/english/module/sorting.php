<?php
$_['sort_order'] = 'Default';
$_['name_ASC'] = 'Name (A-Z)';
$_['name_DESC'] = 'Name (Z-A)';
$_['price_ASC'] = 'Price (low -> high)';
$_['price_DESC'] = 'Price (high -> low)';
$_['rating_ASC'] = 'Rating (starting from high)';
$_['rating_DESC'] = 'Rating (starting from low)';
$_['model_ASC'] = 'Model (A-Z)';
$_['model_DESC'] = 'Model (Z-A)';
$_['quantity_ASC'] = 'Availability (available first)';
$_['quantity_DESC'] = 'Availability (not available first)';
