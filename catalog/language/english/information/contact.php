<?php
$_['heading_title'] = 'Contact Us';
$_['text_contact'] = 'Communication:';
$_['text_social'] = 'We are in social networks:';
$_['text_location'] = 'Our Location';
$_['text_store'] = 'Our Stores';
$_['text_address'] = 'Write to us';
$_['text_telephone'] = 'Technical issues';
$_['text_telephone'] = '(According to operator tariffs)';
$_['text_phone_free'] = 'Free from any operator:';
$_['text_internet'] = 'Internet:';
$_['text_fax'] = 'Fax';
$_['text_open'] = 'Opening Times';
$_['text_comment'] = 'You can send us a request by email using the form.';
$_['text_success'] = '<p>Your enquiry has been successfully sent to the store owner!</p>';
$_['entry_name'] = 'Your Name';
$_['entry_email'] = 'E-Mail Address';
$_['entry_enquiry'] = 'Your question or message';
$_['entry_captcha'] = 'Enter the code in the box below';
$_['button_submit'] = 'Send';
$_['email_subject'] = 'Enquiry %s';
$_['error_name'] = 'Name must be between 3 and 32 characters!';
$_['error_email'] = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry'] = 'Enquiry must be between 10 and 3000 characters!';
$_['error_captcha'] = 'Verification code does not match the image!';
$_['error_g-recaptcha-response-from-server'] = 'Are you a human?';
$_['entry_comment'] = 'Your question or message';
?>
