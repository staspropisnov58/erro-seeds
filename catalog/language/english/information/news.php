<?php
$_['heading_title'] = 'News';
$_['text_news'] = 'NEWS OF OUR STORE';
$_['text_title'] = 'Title';
$_['text_description'] = 'Description';
$_['text_date'] = 'Date Added';
$_['text_view'] = 'View';
$_['text_error'] = 'The page you are looking for cannot be found.';
$_['read_more'] = 'Read more';
$_['text_comment'] = 'Write a review';
?>
