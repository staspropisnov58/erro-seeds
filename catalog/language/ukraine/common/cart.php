<?php

$_['text_items']    = 'Товари';
$_['text_empty']    = 'Ваш кошик порожній!';
$_['text_cart']     = 'Переглянути кошик';
$_['text_checkout'] = 'Оформити замовлення';
$_['text_recurring']  = 'Профіль оплати';
$_['text_empty_cart'] = 'Кошик порожній';


$_['text_quantity_pack'] = 'Кількість в упаковці:';
$_['text_quantity'] = 'Кількість';
$_['text_size'] = 'Розмір:';
$_['text_color'] = 'Колір:';
$_['text_enter_promocode'] = 'Ввести промокод';
$_['text_remove_item'] = 'Видалити товар';
$_['text_refresh'] = 'Оновити';
$_['text_apply_coupon'] = 'Застосувати купон';
$_['text_quick_order'] = 'Швидке замовлення';
$_['text_normal_order'] = 'Звичайне замовлення';
$_['text_in_package'] = 'в упаковці';
$_['text_no_js'] = 'Для оформлення швидкого замовлення необхідно включити JavaScript!';
$_['text_telephone'] = 'Телефон';
