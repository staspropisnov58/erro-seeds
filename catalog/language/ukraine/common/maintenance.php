<?php
$_['heading_title'] = 'Сайт на технічному обслуговуванні';
$_['text_maintenance'] = 'Сайт знаходиться на технічному обслуговуванні';
$_['text_message'] = '
<p class="big">Шановні клієнти!</p>
<p class="big">На превеликий жаль, сайт не працює з технічних причин!</p>
<div class="grey_holder"><p>Але ви можете оформити своє замовлення:</p>
<p>• за телефоном: <a href="tel:+380930000849">+38 093 00-00-849</a> <a href="tel:+380950000849">+38 095 00-00-849</a> <a href="tel:+380960000849">+38 096 00-00-849</a><br>
• написавши нам в мессенджер:
<a href="viber://chat?number=80930000849" target="_blank"><img src="/images/viberPNG.png" alt="viber" style="width:20px"> вайбер (80930000849)</a>,
<a target="_blank" href="https://t.me/ErrorsSeedsComUA_bot"><img src="/images/teleg.png" alt="ES telegram" style="width:20px"> телеграм (@ErrorsSeeds)</a><br>
• через живочат</p>
<p>Не сумуйте! Незабаром ми будемо працювати по-повній програмі!</p>
<p style="text-align:right"><i>З повагою, Errors Seeds!</i></p></div>
';
?>
