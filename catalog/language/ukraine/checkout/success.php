<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']        = 'Ви успішно оформили замовлення!';

// Text
$_['word_success']          = 'Успiх';
$_['text_basket']          = 'Кошик';
$_['text_checkout']        = 'Оформлення замовлення';
$_['text_success']         = 'Успішно';
$_['text_customer']        = '
<p class="text-big">Ваше замовлення успішно створено!</p>
<p>Ваш номер замовлення <span class="green">№%d</span></p>
<p>Історія замовлення знаходиться в <a href="%s" class="green">Особистому кабінеті</a>. Для перегляду історії, перейдіть за посиланням <a href="%s" class="green">Історія замовлень</a>.</p>
<div class="success_soc col-10 mt-5">
  <p>А поки наші працівники складу збирають Вашу посилку, подивіться цікаві відео про канабіс</p>
  <p><a href="https://www.youtube.com/channel/UCiiuf9qa23p_6CSTmhXtmlQ/" target="_blank"><img src="/catalog/view/theme/old/images/success_youtube.png" alt="youtube"></a></p>
  <p>Або поспілкуйтеся з досвідченими гроверами в нашому затишному чаті <a href="https://t.me/ESGroverclub" target="_blank" class="green">@ESGroverclub</a></p>
</div>
';

$_['text_guest']           = '

<p class="text-big">Ваше замовлення успішно створено!</p>
<p>Ваш номер замовлення <span class="green">№%d</span></p>
<div class="success_soc col-10 mt-5">
  <p>А поки наші працівники складу збирають Вашу посилку, подивіться цікаві відео про канабіс</p>
  <p><a href="https://www.youtube.com/channel/UCiiuf9qa23p_6CSTmhXtmlQ/" target="_blank"><img src="/catalog/view/theme/old/images/success_youtube.png" alt="youtube"></a></p>
  <p>Або поспілкуйтеся з досвідченими гроверами в нашому затишному чаті <a href="https://t.me/ESGroverclub" target="_blank" class="green">@ESGroverclub</a></p>
</div>';
