<?php
$_['column_quantity'] = 'Кількість';
$_['column_total'] = 'Всього';
$_['entry_telephone'] = 'Телефон';
$_['entry_postcode'] = 'Індекс';
$_['error_postcode'] = 'Індекс повинен містити від 2 до 10 символів!';
$_['entry_country'] = 'Країна';
?>