<?php
$_['heading_title'] = 'Новини';
$_['text_news'] = 'НОВИНИ НАШОГО МАГАЗИНУ';
$_['text_title'] = 'Назва';
$_['text_description'] = 'Опис';
$_['text_date'] = 'Дата створення';
$_['text_view'] = 'Перегляд';
$_['text_error'] = 'Нічого не знайдено';
$_['read_more'] = 'Читати далі';
$_['text_comment'] = 'Написати коментар';
?>
