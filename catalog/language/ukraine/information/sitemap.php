<?php
// Heading
$_['heading_title']    = 'Мапа сайту';
$_['text_sitemap'] 	= 'Мапа сайту';
// Text
$_['text_special']     = 'Спеціальні пропозиції';
$_['text_account']     = 'Обліковий запис';
$_['text_edit']        = 'Інформація облікового запису';
$_['text_password']    = 'Пароль';
$_['text_address']     = 'Ваші адреси';
$_['text_history']     = 'Історія замовлень';
$_['text_download']    = 'Завантаження';
$_['text_cart']        = 'Кошик';
$_['text_checkout']    = 'Оформити замовлення';
$_['text_search']      = 'Пошук';
$_['text_information'] = 'Інформація';
$_['text_contact']     = 'Контакти';
$_['text_home'] = 'Головна';
$_['text_blog'] 	= 'Новости';
