<?php
// Locale
$_['code']                  = 'ua';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l, d F Y';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = '';

// Text
$_['text_home']             = 'Насіння коноплі';
$_['text_yes']              = 'Так';
$_['text_no']               = 'Ні';
$_['text_none']             = ' --- Не обрано --- ';
$_['text_select']           = ' --- Оберіть --- ';
$_['text_all_zones']        = 'Усі зони';
$_['text_pagination']       = 'Показано з %d до %d з %d (всього %d сторінок)';
$_['text_loading']          = 'Загрузка...';
$_['text_page']             = 'сторінка';
$_['text_in_compare']       = 'Товар доданий в порівняння';
$_['text_in_wishlist']      = 'Товар доданий в закладки';

$_['entry_day']             = 'день';
$_['entry_month']           = 'місяць';
$_['entry_year']            = 'рік';

// Buttons
$_['button_address_add']    = 'Додати адресу';
$_['button_back']           = 'Назад';
$_['button_continue']       = 'Продовжити';
$_['button_continue_shopping'] = 'Продовжити покупки';
$_['button_to_cart']        = 'Перейти в корзину';
$_['button_cart']           = 'Купити';
$_['button_cancel']         = 'Відміна';
$_['button_compare']        = 'До порівняння';
$_['button_wishlist']       = 'До закладок';
$_['button_checkout']       = 'Оформлення замовлення';
$_['button_confirm']        = 'Підтвердження замовлення';
$_['button_coupon']         = 'Застосувати';
$_['button_delete']         = 'Видалити';
$_['button_download']       = 'Завантажити';
$_['button_edit']           = 'Редагувати';
$_['button_filter']         = 'Пошук';
$_['button_new_address']    = 'Нова адреса';
$_['button_change_address'] = 'Змiнити адресу';
$_['button_reviews']        = 'Відгуки';
$_['button_write']          = 'Написати відгук';
$_['button_login']          = 'Увійти';
$_['button_update']         = 'Оновити';
$_['button_remove']         = 'Видалити';
$_['button_reorder']        = 'Додаткове замовлення';
$_['button_return']         = 'Повернути';
$_['button_shopping']       = 'Продовжити покупки';
$_['button_search']         = 'Пошук';
$_['button_shipping']       = 'Застосувати Доставку';
$_['button_submit']         = 'Застосувати';
$_['button_guest']          = 'Оформлення замовлення без реєстрації';
$_['button_view']           = 'Перегляд';
$_['button_voucher']        = 'Застосувати подарунковий сертифікат';
$_['button_upload']         = 'Завантажити файл';
$_['button_reward']         = 'Застосувати бонусні бали';
$_['button_quote']          = 'Вказати ціни';
$_['button_list']           = 'Список';
$_['button_grid']           = 'Сітка';
$_['button_map']            = 'Подивитися мапу';
$_['button_entrance']       = 'Повідомити про наявність';
// Error
$_['error_exception']       = 'Помилка коду(%s): %s у %s на рядку %s';
$_['error_upload_1']        = 'Попередженння: Розмір завантаження перевищує значення upload_max_filesize в php.ini!';
$_['error_upload_2']        = 'Попередженння: Розмір завантаження перевищує MAX_FILE_SIZE значення, яка була вказана в налаштуваннях!';
$_['error_upload_3']        = 'Попередженння: Завантажені файли були завантажені лише частково!!';
$_['error_upload_4']        = 'Попередженння: Немає файлів для завантаження!';
$_['error_upload_6']        = 'Попередженння: тимчасова папка!';
$_['error_upload_7']        = 'Попередженння: Помилка запису!';
$_['error_upload_8']        = 'Попередженння: Заборонено завантажувати файл з даними розширенням!';
$_['error_upload_999']      = 'Попередженння: Невідома помилка!';
