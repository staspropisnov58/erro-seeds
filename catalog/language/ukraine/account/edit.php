<?php
$_['heading_title'] = 'Обліковий запис';
$_['text_account'] = 'Обліковий запис';
$_['text_edit'] = 'Змінити інформацію';
$_['text_your_details'] = 'Ваші особисті дані';
$_['text_success'] = 'Ви успішно змінили обліковий запис.';
$_['entry_firstname'] = 'Ім\'я';
$_['entry_lastname'] = 'Прізвище';
$_['entry_patronymic'] = 'По батькові';
$_['entry_email'] = 'E-Mail';
$_['entry_telephone'] = 'Телефон';
$_['entry_fax'] = 'Факс';
$_['error_exists'] = 'Така E-Mail адреса вже зареєстрована!';
$_['error_firstname'] = 'Ім\'я повинно містити від 1 до 32 символів!';
$_['error_lastname'] = 'Прізвище повинно містити від 1 до 32 символів!';
$_['error_patronymic'] = 'По батькові має бути від 1 до 32 символів!';
$_['error_email'] = 'Неправильний E-Mail!';
$_['error_telephone'] = 'Телефон повинен містити від 3 до 32 символів!';
$_['error_custom_field'] = '%s необхідно!';
$_['error_phone_rangelength'] = 'Телефон повинен містити від% s до% s символів.';
$_['error_phone_regexp'] = 'Телефон введено некоректно.';
?>