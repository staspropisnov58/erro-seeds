<?php
$_['heading_title'] = 'Змінити пароль';
$_['text_account'] = 'Обліковий запис';
$_['text_password'] = 'Ваш пароль';
$_['text_success'] = 'Ви успішно змінили пароль.';
$_['entry_password'] = 'Пароль';
$_['entry_confirm'] = 'Підтвердження паролю';
$_['error_password'] = 'Пароль повинен містити від 4 до 20 символів!';
$_['error_confirm'] = 'Паролi не спiвпадають';
$_['text_my_account'] = 'МІЙ ОБЛІКОВИЙ ЗАПИС';
?>