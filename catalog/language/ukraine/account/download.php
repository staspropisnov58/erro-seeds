<?php
$_['heading_title'] = 'Завантаження облікового запису';
$_['text_account'] = 'Обліковий запис';
$_['text_downloads'] = 'Завантаження';
$_['text_empty'] = 'У вас немає замовлень, які можна завантажити!';
$_['column_order_id'] = '№ замовлення';
$_['column_name'] = 'Назва';
$_['column_size'] = 'Розмір';
$_['column_date_added'] = 'Дата додавання';
?>