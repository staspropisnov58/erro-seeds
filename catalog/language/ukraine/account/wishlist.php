<?php
$_['heading_title'] = 'Мій список бажань';
$_['text_account'] = 'Обліковий запис';
$_['text_instock'] = 'В наявності';
$_['text_wishlist'] = '%s';
$_['text_login'] = 'Ви мусите <a href="%s">увійти</a> чи <a href="%s">створити обліковий запис</a> для збереження <a href="%s">%s</a> Вашого <a href="%s">списку бажань</a>!';
$_['text_success'] = 'Ви успішно додали <a href="%s">%s</a> до Вашого <a href="%s">списку бажань</a>!';
$_['text_remove'] = 'Ви успішно змінили список бажань!';
$_['text_empty'] = 'Ваш список бажать порожній.';
$_['text_exists'] = '<a href="%s">%s</a> вже є в <a href="%s"> закладках </a>!';
$_['column_image'] = 'Зображення';
$_['column_name'] = 'Назва товару';
$_['column_model'] = 'Модель';
$_['column_stock'] = 'Наявність';
$_['column_price'] = 'Ціна за одиницю';
$_['column_action'] = 'Дія';
?>