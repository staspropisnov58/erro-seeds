<?php
// Text
$_['heading_title']	           = 'Testimonials';
$_['meta_description'] = '';

$_['text_testimonials']	           = 'Відгуки о магазині Errors Seeds';
$_['text_write']               = 'Написати відгук';
$_['text_login']               = 'Будь-ласка <a  href="%s">ввійдіть</a> чи <a href="%s">зареєструйтесь</a> для написання відгуку';
$_['text_no_reviews']          = 'Немає відгуків';
$_['text_note']                = '<span class="text-danger">Зверніть увагу:</span> HTML не перекладено!';
$_['text_success']             = 'Дякуємо з Ваш відгук. Він направлений для затвердження модератору.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';
$_['text_h1_testimonials']	       = 'ОТЗЫВЫ О МАГАЗИНЕ';

// Entry
$_['entry_name']               = 'Ваше Ім\'я';
$_['entry_review']             = 'Ваш відгук';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Хороший';
$_['entry_bad']                = 'Поганий';

// Button
$_['button_continue']          = 'Продовжити';

// Error
$_['error_name']               = 'Назва відгуку повинна містити від 3 до 25 символів!';
$_['error_text']               = 'Текст відгуку повинен містити від 25 до 3000 символів!';
$_['error_rating']             = 'Оберіть ретинг відгуку!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';
