<?php
$_['title_product'] = 'Вiдгук про товар %s';
$_['title_store'] = 'Відгук про магазин %s';
$_['title_news'] = 'Написати коментар до новини «%s»';
$_['entry_name'] = 'Ваше Ім\'я';
$_['entry_comment'] = 'Ваш коментар';
$_['entry_review'] = 'Ваш відгук';
$_['entry_email'] = 'E-Mail';
$_['entry_rating'] = 'Оберіть оцінку';
$_['button_submit'] = 'Надіслати вiдгук';
$_['button_submit_comment'] = 'Надіслати коментар';
