<?php
$_['text_register'] = 'Реєстрація';
$_['text_login'] = 'Вхід';
$_['text_logout'] = 'Вихід';
$_['text_forgotten'] = 'Забули пароль?';
$_['text_bonuses'] = 'Бонуси';
$_['text_my_account'] = 'МIЙ АККАУНТ';
$_['heading_title'] = 'Мій Обліковий Запис';
$_['text_account'] = 'Обліковий запис';
$_['text_my_orders'] = 'Мої замовлення';
$_['text_my_newsletter'] = 'Розсилання новин';
$_['text_edit'] = 'Редагувати обліковий запис';
$_['text_password'] = 'Змінити пароль';
$_['text_address'] = 'Змінити мої адреси';
$_['text_wishlist'] = 'Список бажаних товарів';
$_['text_order'] = 'Історія замовлень';
$_['text_download'] = 'Файли для завантаження';
$_['text_reward'] = 'Бонусні бали';
$_['text_return'] = 'Запити на повернення';
$_['text_transaction'] = 'Історія транзакцій';
$_['text_newsletter'] = 'Налаштування повідомлень';
$_['text_recurring'] = 'Регулярні платежі';
$_['text_transactions'] = 'Транзакції';
$_['text_my_account_exit'] = 'Вихiд';
$_['text_affiliate'] = 'Кабінет аффіліата';
$_['text_affiliate_information'] = 'Інформація про реферальну програму';
$_['text_bonuses_only_affiliate'] = 'Мої бонуси';
$_['text_new_my_affiliate'] = 'Приєднатися до реферальной програми';
$_['text_new_affiliate_button'] = 'Приєднатися';
$_['text_payouts_affiliate'] = 'Виплати по реферальній програмі';
$_['text_payout'] = 'Виплати';
$_['text_unclosed_orders'] = 'Незакриті замовлення';
$_['text_payouts'] = 'Виплати';
$_['text_my_account_order_old'] = 'Загальна сума всіх замовлень:';
$_['text_my_account_group'] = 'Тип аккаунта';
$_['text_my_account_sale'] = 'Ваша знижка';
$_['text_my_account_bonus'] = 'Бонусний рахунок';
?>
