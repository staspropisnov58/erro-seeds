<?php
// Heading
$_['heading_cartpopup_title_empty']		    = 'Корзина покупок';
$_['heading_cartpopup_title_nalogka']		    = 'Шановні клієнти, нагадуємо, що накладений платіж можливий при сумі замовлення від <b> 250.00 грн </ b>';

// Button
$_['button_shopping']		   				= 'Продолжить покупки';
$_['button_checkout']		   				= 'Оформить заказ';

// Text
$_['text_cartpopup_empty']       			= 'В корзине пусто';
$_['text_cartpopup_points']      			= 'Бонусные баллы: %s';
$_['textcart_0']  			 				= 'В корзине пусто';
$_['textcart_1']   		 					= 'В корзине %s товар';
$_['textcart_2']   		 					= 'В корзине %s товара';
$_['textcart_3']  			 				= 'В корзине %s товаров';
$_['text_items']     						= '%s товар(ов) - %s';

// Error
$_['error_stock']                    		= 'Товары отмеченные *** отсутствуют в нужном количестве или их нет на складе!';
$_['error_minimum']                  		= 'Минимальное количество для заказа товара %s составляет %s!';
$_['error_warning_min_summ']                  		= 'Минимальный заказ';
?>
