<?php
$_['sort_order'] = 'За замовчуванням';
$_['name_ASC'] = 'Назва (А-Я)';
$_['name_DESC'] = 'Назва (Я-А)';
$_['price_ASC'] = 'Ціна (низька -> висока)';
$_['price_DESC'] = 'Ціна (висока -> низька)';
$_['rating_DESC'] = 'Рейтинг (починаючи с високого)';
$_['rating_ASC'] = 'Рейтинг (починаючи с низького)';
$_['model_ASC'] = 'Модель (А-Я)';
$_['model_DESC'] = 'Модель (Я-А)';
$_['quantity_ASC'] = 'Наявність (спочатку не в наявності)';
$_['quantity_DESC'] = 'Наявність (спочатку в наявності)';
$_['date_start_DESC'] = 'Знижки (спочатку нові)';
