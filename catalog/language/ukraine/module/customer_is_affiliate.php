<?php
$_['heading_title'] = 'Партнерська програма користувача';
$_['text_register'] = 'Реєстрація';
$_['text_login'] = 'Вхід';
$_['text_logout'] = 'Вихід';
$_['text_forgotten'] = 'Забули пароль?';
$_['text_account'] = 'Моя інформація';
$_['text_edit'] = 'Редагувати особисту інформацію';
$_['text_password'] = 'Пароль';
$_['text_payment'] = 'Спосіб оплати';
$_['text_tracking'] = 'Реферальний код';
$_['text_transaction'] = 'Транзакції';
?>