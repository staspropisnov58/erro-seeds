<?php
// Text
$_['text_title'] = 'Накладна плата';
$_['error_min_order'] = 'Накладна плата можлива при сумі заказу від %s';
$_['error_addition'] = 'Только семена сидбанка Errors Seeds могут быть доставлены в стел-упаковке и наложенным платежом. Семена других сидбанков доставляются только по полной предоплате';
