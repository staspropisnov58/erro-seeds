<?php


// Heading
$_['heading_title'] = 'Сторінка відсутня!';
$_['text_not_found'] = 'Сторінка відсутня!';

// Text
$_['text_error']    = '<div class="error-form-text">
<p class="title">Сторінка відсутня!</p>
<div class="error-form-text">
<p>Для того, щоб знайти інформацію, що вас цікавила, скористайтеся </p>
<p>- строкою пошуку</p>
<p>- навігаційним меню</p>
<p>- чи перейдіть на <a href="/ua/product" style="color:#65bd00">сторінку категорій</a></p>
</div></div>';

$_['text_page_not_found'] = '<p class="title">НЕ ЗАБУДЬТЕ СВІЙ КУПОН НА ЗНИЖКУ <span style="color:#fff">404ESUKR</span>!</p>
<img src="/catalog/view/theme/old/images/Cupon-ukr.svg" alt="Купон 5% [404ESUKR]" style="margin-bottom:15px;">
<p>Даний купон можна використовувати при кожному замовленні в нашому інтернет-магазин.</p>
<p>Виникли питання? <a href="/ua/feedback" style="color:#65bd00">Звертайтеся до нас</a></p>';
