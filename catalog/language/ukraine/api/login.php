<?php

//Update for OpenCart 2.3.x by OpenCart Ukrainian Community http://opencart.ua
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_success'] = 'API сесія успішно розпочата!';

// Error
$_['error_login']  = 'Попередження: Немає відповідності для Ім\'я користувача та/або Пароль.';
