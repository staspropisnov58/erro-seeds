<?php
// Heading
$_['heading_title']      = 'Бонуси';

// Column
$_['column_date_added']  = 'Додано';
$_['column_description'] = 'Опис';
$_['column_amount']      = 'Сума (%s)';

// Text
$_['text_account']       = 'Особистий кабінет';
$_['text_transaction']   = 'Бонуси';
$_['text_balance']       = 'Невиплачених бонусів:';
$_['text_empty']         = 'У Вас не було виплат!';
$_['text_payout']        = 'Виплати';
