<?php
$_['text_refine'] = 'Оберіть підкатегорію';
$_['text_product'] = 'Товари';
$_['text_error'] = 'Категорію не знайдено!';
$_['text_empty'] = 'В даній категорії немає товарів.';
$_['text_quantity'] = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model'] = 'Модель:';
$_['text_points'] = 'Бонусні бали:';
$_['text_price'] = 'Ціна:';
$_['text_tax'] = 'Без ПДВ:';
$_['text_compare'] = '%s';
$_['text_sort'] = 'Сортування:';
$_['text_default'] = 'За стандартом';
$_['text_name_asc'] = 'Назва (А - Я)';
$_['text_name_desc'] = 'Назва (Я - А)';
$_['text_price_asc'] = 'Ціна (низька &gt; висока)';
$_['text_price_desc'] = 'Ціна (высока &gt; низька)';
$_['text_rating_asc'] = 'Рейтинг (від низького)';
$_['text_rating_desc'] = 'Рейтинг (від високого)';
$_['text_model_asc'] = 'Модель (А - Я)';
$_['text_model_desc'] = 'Модель (Я - А)';
$_['text_limit'] = 'Показати:';
$_['text_home'] = 'Головна';
?>