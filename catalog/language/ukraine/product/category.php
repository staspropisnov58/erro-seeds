<?php
$_['text_refine'] = 'Уточнити пошук';
$_['text_product'] = 'Товари';
$_['text_error'] = 'Категорія не знайдена!';
$_['text_empty'] = 'В цій категорії немає товарів.';
$_['text_quantity'] = 'Кількість:';
$_['text_manufacturer'] = 'Бренд:';
$_['text_model'] = 'Артикул:';
$_['text_points'] = 'Бонусні бали:';

$_['text_instock'] = 'Є в наявності';
$_['text_buy'] = 'Купити';
$_['text_price'] = 'Ціна:';
$_['text_tax'] = 'Без ПДВ:';
$_['text_compare'] = 'Порівняти товари (%s)';
$_['text_sort'] = 'Сортувати за:';
$_['text_default'] = 'За замовчуванням';
$_['text_name_asc'] = 'Ім\'я (А - Я)';
$_['text_name_desc'] = 'Ім\'я (Я - A)';
$_['text_price_asc'] = 'Ціна (Низька)';
$_['text_price_desc'] = 'Ціна (Висока)';
$_['text_rating_asc'] = 'Рейтинг (Низький)';
$_['text_rating_desc'] = 'Рейтинг (Високий)';
$_['text_model_asc'] = 'Модель (A - Я)';
$_['text_model_desc'] = 'Модель (Я - A)';
$_['text_limit'] = 'Показати:';
$_['text_file_download'] = 'Розничный прайс-лист';
$_['text_all_category_x'] = 'Все семена конопли';
$_['text_show_more'] = 'Показати бiльше';
$_['text_show_all'] = 'Показати все';
$_['button_entrance'] = 'Повідомити про наявність';
$_['full_text'] = 'Читати повністю';
$_['sender_text'] = 'Відправити';
$_['entry_pre_order'] = 'Предзаказ';
$_['entry_tel'] = 'Телефон';
$_['send_title_hed'] = 'Залишіть свій email і ми обов\'язково надішлемо вам повідомлення, коли ES LED 300 - Full Spectrum знову з\'явиться на складі';
$_['send_quantity_prod'] = 'Кількість товару';
$_['text_filter'] = 'Фільтри <i class="fas fa-chevron-right"></i>';
?>
