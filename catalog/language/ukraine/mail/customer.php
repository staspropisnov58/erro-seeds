<?php
// Text
$_['text_subject']        = '%s - Дякуємо Вам за реєстрацію';
$_['text_welcome']        = 'Ласкаво просимо і дякуємо за реєстрацію в %s!';
$_['text_login']          = 'Ваш обліковий запис створено і тепер Ви можете увійти використавши Ваш e-mail і пароль на нашому сайті або за наступним посиланням:';
$_['text_approval']       = 'Ваш обліковий запис має бути підтвержено до того як Ви зможете увійти. Після підтвердження Ви зможете увійти використавши Ваш e-mail і пароль відвідавши наш сайт або за наступним посиланням:';
$_['text_services']       = 'Після входу на сайт Ви отримаєте доступ до додаткових сервісів, а саме перегляду попередніх замовлень, друку рахунків і зміни деталей облікового запису.';
$_['text_thanks']         = 'Дякуємо';
$_['text_new_customer']   = 'Новий покупець';
$_['text_signup']         = 'Новий покупець увійшов:';
$_['text_website']        = 'Сайт:';
$_['text_customer_group'] = 'Група покупців:';
$_['text_firstname']      = 'Ім\'я:';
$_['text_lastname']       = 'Прізвище:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Телефон:';
$_['text_link'] = 'Поміняти налаштування повідомлень ви завжди можете перейшовши за <a href = "%s">посиланням </a> у своєму Особистому кабінеті.';
$_['text_welcome_affiliate']		        = 'Вітаємо, Ви зареєстровані в реферальной програмі на нашому сайті <a href="%s">% s </a>!';

$_['text_services_affiliate']		        = 'Ваш особистий реферальний код і генератор посилань, а також суму бонусів і зроблені виплати ви знайдете в особистому кабінеті в розділі "Реферальная програма".';

$_['text_services_affiliate_x2'] = 'Дякую за участь в нашій реферальной програмі! Залишайтеся з нами!';

$_['text_greeting'] = 'Добрий день!';

$_['text_footer'] = 'З повагою, %s';


$_['text_link'] = 'Поменять настройки уведомлений Вы всегда можете перейдя по <a href = "%s">ссылке </a> в своем личном кабинете.';



$_['text_welcome_affiliate']		        = 'Поздравляем, Вы зарегистрированы в реферальной программе на нашем сайте <a href="%s">%s</a>!';
