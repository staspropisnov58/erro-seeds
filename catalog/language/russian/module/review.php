<?php
$_['title_product'] = 'Отзыв о товаре %s';
$_['title_store'] = 'Отзыв о магазине %s';
$_['title_news'] = 'Написать комментарий к новости «%s»';
$_['entry_name'] = 'Ваше имя';
$_['entry_comment'] = 'Ваш комментарий';
$_['entry_review'] = 'Ваш отзыв';
$_['entry_email'] = 'E-Mail';
$_['entry_rating'] = 'Выберите оценку';
$_['button_submit'] = 'Отправить отзыв';
$_['button_submit_comment'] = 'Отправить комментарий';
