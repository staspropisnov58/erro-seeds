<?php
// Text
$_['heading_title']	           = 'Отзывы о магазине';
$_['meta_description'] = '';

$_['text_testimonials']	           = 'Отзывы о магазине Errors Seeds';
$_['text_write']               = 'Добавить свой отзыв';
$_['description']               = '<p>Администрация сайта не несет ответственности за информацию размещенную в комментариях, так как она является субъективным мнением пользователей и вносится ними по собственному усмотрению.</p>';
$_['text_login']               = 'Пожалуйста <a href="%s">авторизируйтесь</a> или <a href="%s">создайте учетную запись</a> перед тем как написать отзыв';
$_['text_no_reviews']          = 'Пока нет отзывов';
$_['text_note']                = '<span style="color: #FF0000;">Примечание:</span> HTML разметка не поддерживается! Используйте обычный текст.';
$_['text_success']             = 'Спасибо за ваш отзыв. Он поступил администратору для проверки на спам и вскоре будет опубликован.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Author: %s';
$_['text_mail_rating']	       = 'Rating: %s';
$_['text_mail_text']	       = 'Text:';
$_['text_h1_testimonials']	       = 'ОТЗЫВЫ О МАГАЗИНЕ';

// Entry
$_['entry_name']               = 'Ваше имя';
$_['entry_review']             = 'Ваш отзыв';
$_['entry_rating']             = 'Оценка';
$_['entry_good']               = 'Хорошо';
$_['entry_bad']                = 'Плохо';

// Button
$_['button_continue']          = 'Continue';

// Error
$_['error_name']               = 'Имя должно быть от 3 до 25 символов!';
$_['error_text']               = 'Текст отзыва должен быть от 25 до 3000 символов!';
$_['error_rating']             = 'Пожалуйста, выберите оценку!';
$_['error_captcha']            = 'Warning: Verification code does not match the image!';
