<?php
$_['heading_title'] = 'Личный кабинет';
$_['text_register'] = 'Регистрация';
$_['text_login'] = 'Вход';
$_['text_logout'] = 'Выход';
$_['text_forgotten'] = 'Забыли пароль?';
$_['text_account'] = 'Моя информация';
$_['text_edit'] = 'Изменить контактную информацию';
$_['text_password'] = 'Пароль';
$_['text_address'] = 'Адресная книга';
$_['text_wishlist'] = 'Закладки';
$_['text_order'] = 'История заказов';
$_['text_download'] = 'Файлы для скачивания';
$_['text_reward'] = 'Бонусные баллы';
$_['text_return'] = 'Возвраты';
$_['text_transaction'] = 'История транзакций';
$_['text_newsletter'] = 'Настройка уведомлений';
$_['text_recurring'] = 'Регулярные платежи';
$_['text_bonuses'] = 'Бонусы';
$_['text_payouts'] = 'Выплаты';
$_['text_unclosed_orders'] = 'Незакрытые заказы';
$_['text_affiliate'] = 'Реферальная программа';
$_['text_my_account'] = 'Мой аккаунт';
$_['text_my_account_group'] = 'Тип аккаунта';
$_['text_my_account_sale'] = 'Ваша скидка';
$_['text_my_account_bonus'] = 'Бонусний счет';
$_['text_my_account_order_old'] = 'Общая сума всех заказов:';
?>
