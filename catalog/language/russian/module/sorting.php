<?php
$_['sort_order'] = 'По умолчанию';
$_['name_ASC'] = 'Название (А-Я)';
$_['name_DESC'] = 'Название (Я-А)';
$_['price_ASC'] = 'Цена (низкая -> высокая)';
$_['price_DESC'] = 'Цена (высокая -> низкая)';
$_['rating_DESC'] = 'Рейтинг (начиная с высокого)';
$_['rating_ASC'] = 'Рейтинг (начиная с низкого)';
$_['model_ASC'] = 'Модель (А-Я)';
$_['model_DESC'] = 'Модель (Я-А)';
$_['quantity_ASC'] = 'Наличие (сначала не в наличии)';
$_['quantity_DESC'] = 'Наличие (сначала в наличии)';
$_['date_start_DESC'] = 'Скидки (сначала новые)';
?>
