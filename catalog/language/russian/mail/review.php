<?php
// Text
$_['text_subject']	= '%s - отзыв о товаре';
$_['text_subject_store']	= '%s - отзыв о магазине';
$_['text_subject_news'] = 'Комментарий к новости - %s';
$_['text_waiting']	= 'Новые отзывы ожидают вашей проверки .';
$_['text_product']	= 'Товар: %s';
$_['text_news'] = 'Новость: %s';
$_['text_store']	= 'Отзыв о магазине';
$_['text_reviewer']	= 'Отзыв оставил: %s';
$_['text_rating']	= 'Оценка: %s';
$_['text_review']	= 'Отзыв:';
