<?php
$_['text_subscription'] = 'Пользователь с email %s %s. Сайт: <a href="%s">%s</a> ';
$_['text_low_balance_on_site'] = 'На счету %s Низкий баланс, необходимо пополнить, обратитесь в отдел разработки SEOboost. Сайт: <a href="%s">%s</a> ';
$_['theme_newsletter_subscription'] = 'Подписка на рассылку';
$_['theme_answer_sms_service'] = 'Сообщение об ошибке с SMS сервиса %s';
$_['theme_newsletter_unsubscription'] = 'Отписка от рассылки';


$_['text_hello'] = 'Привет!';

$_['text_newsletter_subscription'] = 'подписался на рассылку';
$_['text_low_balance'] = 'Обратите Внимание!!! На счету %s Низкий баланс, необходимо пополнить, обратитесь в отдел разработки SEOboost';
$_['text_newsletter_unsubscription'] = 'отписался от рассылки';
$_['text_answer_message'] = 'Сообщение об ошибке - "%s"';
$_['text_footer'] = 'С уважением, %s';
