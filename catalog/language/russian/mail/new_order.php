<?php

$_['text_new_subject'] = '%s - Заказ %s';
$_['text_new_greeting'] = 'Благодарим за интерес к товарам %s . Ваш заказ получен и поступит в обработку в ближайшее время';
$_['text_new_received'] = 'Вы получили заказ.';
$_['text_new_link'] = 'Для просмотра Вашего заказа перейдите по ссылке:';
$_['text_new_order_detail'] = 'Детализация заказа';
$_['text_new_instruction'] = 'Реквизиты для оплаты';
$_['text_new_order_id'] = '№ заказа:';
$_['text_new_date_added'] = 'Дата заказа:';
$_['text_new_order_status'] = 'Состояние заказа:';
$_['text_new_payment_method'] = 'Способ оплаты:';
$_['text_new_shipping_method'] = 'Способ доставки:';
$_['text_new_email'] = 'E-mail:';
$_['text_new_telephone'] = 'Телефон:';
$_['text_new_ip'] = 'IP-адрес:';
$_['text_new_payment_address'] = 'Адрес плательщика';
$_['text_new_shipping_address'] = 'Адрес доставки';
$_['text_new_products'] = 'Товары';
$_['text_new_product'] = 'Товар';
$_['text_new_model'] = 'Модель';
$_['text_new_quantity'] = 'Количество';
$_['text_new_price'] = 'Цена';
$_['text_new_order_total'] = 'Заказ итого:';
$_['text_order_number'] = 'Заказ № %s';
$_['text_new_total'] = 'Итого:';
$_['text_new_download'] = 'После подтверждения оплаты, загружаемые товары будут доступны по ссылке:';
$_['text_new_comment'] = 'Комментарий к Вашему заказу:';
$_['text_new_footer'] = 'Если у Вас есть какие-либо вопросы, ответьте на это сообщение.';
$_['text_hello'] = 'Добрый день, %s!';
$_['text_create_affiliate_order'] = 'По вашей реферальной ссылке был сделан заказ. Бонусы будут начислены, как только заказ будет оплачен. Сумму бонусов и выплат Вы всегда можете проверить в своём личном кабинете в разделе "Реферальная программа" на нашем сайте <a href="%s"> %s </a>.';
$_['text_thanks_affiliate'] = 'Спасибо за участие в нашей реферальной программе! Оставайтесь с нами!';
$_['text_footer'] = 'С уважением, %s.';
$_['text_comment'] = '%s - Новое сообщение';
$_['text_customer'] = 'Покупатель: %s %s';
$_['text_email'] = 'Email: %s';
$_['text_telephone'] = 'Телефон: %s';
$_['text_ip'] = 'IP покупателя: %s';
$_['text_customer_comment'] = "<b>". 'Комментарии или пожелания по заказу:'. "</b>" .  "<br>" . ' %s' . "<br>" ;
$_['text_products'] =  "<b>" . 'Заказанные продукты:' . "</b> <br>"  ;
$_['text_shipping'] = "<b>" . 'Доставка:' . "</b>";
$_['text_get_shipping'] = 'Получатель: %s %s';
$_['text_address'] = 'Адрес доставки заказа:' . "<br>" .  '%s, %s, %s, %s.';
$_['text_payment'] = "<b>" . 'Оплата:' . "</b>";
$_['text_payment_method'] = 'Способ оплаты - %s';
$_['text_get_payment'] = 'Платильщик: %s %s';
$_['text_address_payment'] = 'Адрес платильщика: %s, %s, %s, %s'
?>
