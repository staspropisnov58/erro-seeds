<?php
$_['text_subject'] = 'Отзыв прошел модерацию';
$_['text_hello']                = 'Здравствуйте, %s!';
$_['theme_review_moderated']    = 'Ваш отзыв прошел модерацию';
$_['text_review_moderated']     = 'Ваш отзыв к товару "%s", прошёл модерацию.';
$_['text_review_moderated_x1']  = 'Посмотреть его вы можете нажав по этой <a href="%s">ссылке</a>';
$_['text_footer']               = 'С уважением, %s';
