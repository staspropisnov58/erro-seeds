<?php

// Heading

$_['heading_title']    = 'Режим обслуживания';



// Text

$_['text_maintenance'] = 'Магазин временно закрыт';

$_['text_message']     = '
<p class="big">Уважаемые покупатели!</p>
<p class="big">Сайт по техническим причинам на ремонте!</p>
<div class="grey_holder"><p>Но вы можете сделать заказ:</p>
<p>• по телефону: <a href="tel:+380930000849">+38 093 00-00-849</a> <a href="tel:+380950000849">+38 095 00-00-849</a> <a href="tel:+380960000849">+38 096 00-00-849</a><br>
• написав нам в мессенджер:
<a href="viber://chat?number=80930000849" target="_blank"><img src="/images/viberPNG.png" alt="viber" style="width:20px"> вайбер (80930000849)</a>,
<a target="_blank" href="https://t.me/ErrorsSeedsComUA_bot"><img src="/images/teleg.png" alt="ES telegram" style="width:20px"> телеграм (@ErrorsSeeds)</a><br>
• через живочат</p>
<p>Не расстраивайтесь! В скором времени мы будем в полной боевой готовности и полностью исправны!</p>
<p style="text-align:right"><i>С уважением, Errors Seeds!</i></p></div>
';
