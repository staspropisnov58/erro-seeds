<?php
$_['text_information'] = 'Информация';
$_['text_service'] = 'Служба поддержки';
$_['text_extra'] = 'Дополнительно';
$_['text_contact'] = 'Связаться с нами';
$_['text_return'] = 'Возврат товара';
$_['text_sitemap'] = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher'] = 'Подарочные сертификаты';
$_['text_affiliate'] = 'Партнерская программа';
$_['text_special'] = 'Акции';
$_['text_new_and_popular'] = 'Новые и популярные';
$_['text_help'] = 'Помощь';
$_['text_seeds'] = 'Семена';
$_['text_abuot_shop'] = 'О магазине';
$_['text_queshions'] = 'Часто задаваемые вопросы';
$_['text_buy_cannabis_seeds'] = 'Как купить семена конопли';
$_['text_our_address'] = 'Как нас найти';
$_['text_delivery'] = 'Доставка';
$_['text_pay'] = 'Оплата';
$_['text_schedule'] = 'График работы';
$_['text_blog'] = 'Блог';
$_['text_social_networks'] = 'Мы в соцсетях';
$_['text_footer'] = 'Made with ♥ for great people';
$_['text_ES'] = '© Errors Seeds Company 2009-';
$_['text_ES2'] = ' <a href="http://errors-seeds.com.ua/"> Семена конопли </a>';
$_['text_lamp'] = 'Лампы для растений';
$_['text_medical'] = 'Медицина';
$_['text_account'] = 'Личный Кабинет';
$_['text_order'] = 'История заказов';
$_['text_wishlist'] = 'Закладки';
$_['text_newsletter'] = 'Рассылка';
$_['text_compare_forum'] = 'Форум';
$_['text_newsletter_text'] = 'Введите ваш Email';
$_['text_subcribe'] = 'Подписаться';
$_['text_error_subcribe'] = 'Ваш Email уже есть в списке подписчиков!';
$_['text_success_subcribe'] = 'Вы успешно подписались на рассылку!';
$_['text_email_not_validate'] = 'Вы ввели некоректный Email !';
$_['contact_us'] = '<a href="/contact-us">Контакты</a>';
$_['contact_us_button'] = 'Свяжитесь <br>с нами';
$_['feedbacs_x'] = '<a href="/testimonial">Отзывы о магазине</a>';
$_['entry_firstname'] = 'Ваше имя';
$_['entry_email'] = 'E-Mail';
$_['entry_comment'] = 'Комментарий';
$_['title_feedback'] = 'Оставить комментарий';
$_['button_modal_save'] = 'Отправить';
$_['rating'] = 'Оценить товар ';
$_['join_us'] = 'ПРИСОЕДИНЯЙТЕСЬ К НАМ';
$_['about_us'] = 'О нас';
$_['questions'] = 'Вопросы';
$_['payment_methods'] = 'Способы оплаты';
$_['delivery'] = 'Доставка';
$_['replacement_and_return'] = 'Замена и возврат';
$_['address_2'] = 'Украина, г. Харьков,<br> площадь Свободы, 25';
$_['text_contact'] = 'Контакты';
$_['text_powered'] = 'Работает на <a href="http://opencart.com/">OpenCart</a>';
?>
