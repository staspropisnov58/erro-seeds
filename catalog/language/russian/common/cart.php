<?php
$_['text_empty'] = 'Ваша корзина пуста!';
$_['text_cart'] = 'Посмотреть корзину';
$_['text_recurring'] = 'Платежный профиль';
$_['text_empty_cart'] = 'Ваша корзина пуста';
$_['text_quantity_pack'] = 'Количество в пачке:';
$_['text_size'] = 'Размер:';
$_['text_color'] = 'Цвет:';
$_['text_in_package'] = 'в упаковке';
$_['text_telephone'] = 'Телефон';
?>