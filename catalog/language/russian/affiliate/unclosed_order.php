<?php
// Heading
$_['heading_title']      = 'Незакрытые заказы';

// Column
$_['column_date_added']  = 'Добавлено';
$_['column_description'] = 'Описание';
$_['column_amount']      = 'Сумма (%s)';

// Text
$_['text_account']       = 'Личный кабинет';
$_['text_transaction']   = 'Бонусы';
$_['text_balance']       = 'Количество незакрытых заказов:';
$_['text_empty']         = 'У Вас не было выплат!';
$_['text_payout']        = 'Выплаты';
$_['column_date_added']  = 'Добавлено';
$_['column_description']='Описание';
$_['column_amount']='Сума';
$_['text_empty']='У Вас не было выплат!';
$_['text_order_id']='Номер заказа';
$_['text_order_status']='Статус заказа';
$_['text_order_price']='Сумма заказа';
$_['text_affiliate_bonus']='Бонус партнеру';
$_['text_date_added']='Дата создания заказа';
$_['']='';
$_['']='';
$_['']='';
