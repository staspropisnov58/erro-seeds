<?php
// Heading
$_['heading_title']    = 'Настройка уведомлений';

// Text
$_['text_account']     = 'Личный Кабинет';
$_['text_newsletter']  = 'Рассылка и уведомления';
$_['text_success']     = 'Настройки уведомлений успешно обновлены!';

// Entry
$_['entry_newsletter'] = 'Подписаться на email рассылку новостей';
$_['entry_review_moderation'] = 'Получать уведомления о прохождении модерации отзывов';
$_['entry_affiliate_order'] = 'Получать уведомления о новых заказах по моей реферальной ссылке';
$_['entry_affiliate_bonus'] = 'Получать  уведомления о начислении бонусов';
$_['entry_affiliate_payout'] = 'Получать уведомления о новых выплатах';
$_['entry_new_order'] = 'Получать уведомления при создании нового заказа';
$_['entry_edit_order'] = 'Получать уведомления при изменении статуса заказа';
$_['entry_new_review_reply'] = 'Получать уведомления об ответах на мои отзывы';
$_['text_notification_settings'] = 'Настройка уведомлений';
$_['text_notification_title'] = 'Уведомляйте меня в';
$_['text_notification_sms'] = 'SMS';
$_['text_notification_email'] = 'EMAIL';
$_['text_for_default'] = 'Настройки по умолчанию';
