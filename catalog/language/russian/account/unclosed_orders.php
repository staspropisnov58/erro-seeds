<?php
// Heading
$_['heading_title']      = 'Бонусы';

// Column
$_['column_date_added']  = 'Добавлено';
$_['column_description'] = 'Описание';
$_['column_amount']      = 'Сумма (%s)';

// Text
$_['text_account']       = 'Личный Кабинет';
$_['text_transaction']   = 'Бонусы';
$_['text_balance']       = 'Невыплаченых бонусов:';
$_['text_empty']         = 'У Вас не было выплат!';
$_['text_payout']        = 'Выплаты';
$_['column_date_added']  = 'Добавлено';
$_['column_description']='Описание';
$_['column_amount']='Сума';
$_['text_empty']='У Вас не было выплат!';
