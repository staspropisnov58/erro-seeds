<?php

// Heading

$_['heading_title']      = 'Учетная запись';



// Text

$_['text_account']       = 'Личный Кабинет';

$_['text_edit']          = 'Редактировать информацию';

$_['text_your_details']  = 'Ваша учетная запись';

$_['text_success']       = 'Ваша учетная запись была успешно обновлена!';



// Entry

$_['entry_firstname']    = 'Имя';

$_['entry_lastname']     = 'Фамилия';
$_['entry_patronymic']   = 'Отчество';
$_['entry_email']        = 'E-Mail';

$_['entry_telephone']    = 'Телефон';

$_['entry_fax']          = 'Факс';



// Error

$_['error_exists']       = 'Данный E-Mail уже зарегистрирован!';

$_['error_firstname']    = 'Имя должно быть от 1 до 32 символов!';

$_['error_lastname']     = 'Фамилия должна быть от 1 до 32 символов!';
$_['error_patronymic']   = 'Отчество должно быть от 1 до 32 символов!';
$_['error_email']        = 'E-Mail адрес введен неверно!';

$_['error_telephone']    = 'Номер телефона должен быть от 3 до 32 символов!';

$_['error_custom_field'] = '%s необходим!';

$_['error_email_rangelength'] = 'Email должен быть от %s-х до %s-х символов.';
$_['error_email_regexp'] = 'Email введен неверно!.';
$_['error_customer_login'] = 'Данный email уже используется.';
$_['error_firstname_rangelength'] = 'Имя должно быть от %s до %s символов.';
$_['error_firstname_regexp'] = 'Имя содрежит недопустимые символы.';
$_['error_lastname_rangelength'] = 'Фамилия должна быть от %s до %s символов. ';
$_['error_lastname_regexp'] = 'Фамилия содрежит недопустимые символы.';
$_['error_patronymic_rangelength'] = 'Отчество должно быть от %s до %s символов. ';
$_['error_patronymic_regexp'] = 'Отчество содрежит недопустимые символы.';
$_['error_phone_rangelength'] = 'Телефон должен содержать от %s до %s символов.';
$_['error_phone_regexp'] = 'Телефон введен некорректно.';
$_['error_password_rangelength'] = 'Пароль дожен быть от %s до %s символов.';
$_['error_login_rangelength'] = 'Логин должен быть от %s до %s символов. ';
$_['error_customer_login'] = 'Логин уже занят';
$_['error_affiliate_login'] = 'Партнера с таким логином не найдено.';
$_['error_telehpone'] = 'Пожалуйста, выберите код страны и отредактируйте номер телефона';
