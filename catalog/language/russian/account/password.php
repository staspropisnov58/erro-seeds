<?php
$_['heading_title'] = 'Изменить пароль';
$_['text_account'] = 'Личный Кабинет';
$_['text_password'] = 'Ваш пароль';
$_['text_success'] = 'Ваш пароль успешно изменен!';
$_['entry_password'] = 'Пароль';
$_['entry_confirm'] = 'Подтвердите пароль';
$_['error_password'] = 'Пароль должен быть от 4 до 40 символов!';
$_['error_confirm'] = 'Пароли не совпадают';
?>
