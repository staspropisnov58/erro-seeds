<?php
// Heading
$_['heading_title'] = 'CТРАНИЦА НЕ НАЙДЕНА';
$_['text_not_found'] = 'CТРАНИЦА НЕ НАЙДЕНА';
// Text
$_['text_error']    = '<div class="error-form-text">
<p class="title">CТРАНИЦА НЕ НАЙДЕНА</p>
<div class="error-form-text">
<p>Для того, чтобы найти интересующую Вас информацию, воспользуйтесь</p>
<p>- строкой поиска</p>
<p>- навигационным меню</p>
<p>- или перейдите на <a href="/product" style="color:#65bd00">страницу категорий</a></p>
</div></div>';

$_['text_page_not_found'] = '<p class="title">НЕ ЗАБУДЬТЕ СВОЙ КУПОН НА СКИДКУ <span style="color:#fff">404ESUKR</span>!</p>
<img src="/catalog/view/theme/old/images/Cupon-ru.svg" alt="Купон 5% [404ESUKR]" style="margin-bottom:15px;">
<p>Данный купон можно использовать при каждой покупке в нашем интернет-магазине.</p>
<p>Возникли вопросы? <a href="/feedback" style="color:#65bd00">Обращайтесь к нам</a></p>';
