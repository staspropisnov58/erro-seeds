<?php
// Text
$_['text_success']           = 'Заказ обновлен';

// Error
$_['error_permission']       = 'Внимание! Доступ запрещен к API!';
$_['error_not_found']        = 'Внимание! Товар не найден';
$_['error_category_not_found'] = 'Внимание! Категория не найдена';
