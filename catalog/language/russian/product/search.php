<?php

// Heading

$_['heading_title']     = 'Поиск';

$_['heading_tag']		= 'По тегу - ';



// Text

$_['text_search']       = 'Товары, соответствующие критериям поиска';

$_['text_keyword']      = 'Ключевые слова';
$_['button_entrance'] = 'Уведомить о поступлении';
$_['text_category']     = 'Все категории';

$_['text_sub_category'] = 'Поиск в подкатегориях';

$_['text_empty']        = '<p>Нет товаров, которые соответствуют критериям поиска.</p><br>
<p>Попробуйте изменить запрос и посмотрите наши <a href="/auxpage_action_list/" style="color:#65bd00">акционные предложения</a></p>';

$_['text_quantity']     = 'Количество:';

$_['text_manufacturer'] = 'Производитель:';

$_['text_model']        = 'Модель:';

$_['text_points']       = 'Бонусные баллы:';

$_['text_price']        = 'Цена:';

$_['text_tax']          = 'Без НДС:';

$_['text_reviews']      = 'Всего отзывов: %s';

$_['text_compare']      = '%s';

$_['text_buy']          = 'Заказать';

$_['text_instock'] = 'Есть в наличии';

$_['text_sort']         = 'Сортировка:';

$_['text_default']      = 'По умолчанию';

$_['text_name_asc']     = 'Название (А - Я)';

$_['text_name_desc']    = 'Название (Я - А)';

$_['text_price_asc']    = 'Цена (низкая &gt; высокая)';

$_['text_price_desc']   = 'Цена (высокая &gt; низкая)';

$_['text_rating_asc']   = 'Рейтинг (начиная с низкого)';

$_['text_rating_desc']  = 'Рейтинг (начиная с высокого)';

$_['text_model_asc']    = 'Модель (А - Я)';

$_['text_model_desc']   = 'Модель (Я - А)';

$_['text_limit']        = 'Показать:';

$_['text_show_all']     = 'Показать все';




// Entry

$_['entry_search']      = 'Поиск:';

$_['entry_description'] = 'Искать в описании товаров';
