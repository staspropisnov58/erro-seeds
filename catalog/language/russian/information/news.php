<?php
$_['heading_title'] = 'Новости';
$_['text_news'] = 'Блог / Новости';
$_['text_title'] = 'Название';
$_['text_description'] = 'Описание';
$_['text_date'] = 'Дата добавления';
$_['text_view'] = 'Просмотр';
$_['text_error'] = 'Ничего нет';
$_['read_more'] = 'Смотреть все...';
$_['text_comment'] = 'Написать комментарий';
?>
