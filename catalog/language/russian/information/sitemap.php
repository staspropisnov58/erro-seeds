<?php
$_['heading_title'] = 'Карта сайта';
$_['text_sitemap'] 	= 'Карта сайта';
$_['text_special'] = 'Акции';
$_['text_account'] = 'Личный кабинет';
$_['text_edit'] = 'Личная информация';
$_['text_password'] = 'Пароль';
$_['text_address'] = 'Мои адреса';
$_['text_history'] = 'История заказов';
$_['text_download'] = 'Файлы для скачивания';
$_['text_cart'] = 'Корзина';
$_['text_checkout'] = 'Оформление заказа';
$_['text_search'] = 'Поиск';
$_['text_information'] = 'Информация';
$_['text_contact'] = 'Наши контакты';
$_['text_home'] = 'Главная';
$_['text_blog'] = 'Новости';
$_['text_back_home'] = 'Вернуться на главную страницу';
?>
