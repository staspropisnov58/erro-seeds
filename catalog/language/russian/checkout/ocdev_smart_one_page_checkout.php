<?php

// @category  : OpenCart
// @module    : Smart One Page Checkout
// @author    : OCdevWizard <ocdevwizard@gmail.com>
// @copyright : Copyright (c) 2015, OCdevWizard
// @license   : http://license.ocdevwizard.com/Licensing_Policy.pdf

// Heading
$_['heading_title']                 = 'Оформление заказа';
$_['meta_description'] = '';
$_['heading_login_block']           = 'Логин';
$_['heading_user_block']            = 'Контактные данные';
$_['heading_delivery_block']        = 'Детали доставки';
$_['heading_payment_block']         = 'Способ оплаты';
$_['heading_shipping_block']        = 'Выбор способов доставки и оплаты';
$_['heading_cart_block']            = 'Ваша корзина ';
$_['heading_coupon_block']          = 'Ввести промокод';
$_['heading_voucher_block']         = 'Воспользуйтесь подарочным сертификатом ';
$_['heading_reward_block']          = 'Используйте бонусные баллы (доступно %s) ';

// Button
$_['button_login']                  = 'Авторизация';
$_['button_logout']                 = 'Выход';
$_['button_register']               = 'С регистрацией';
$_['button_guest']                  = 'Без регистрации';
$_['button_remove_coupon']          = 'Удалить купон ';
$_['button_remove_voucher']         = 'Удалить сертификат ';
$_['button_remove_reward']          = 'Удалить бонусы';
$_['button_continue_shopping']      = 'Продолжить покупки ';
$_['button_print_order']            = 'Распечатайте заказ ';

// Text
$_['text_loading']                  = 'Обрабатывается...';
$_['text_day']                      = 'день';
$_['text_week']                     = 'неделя';
$_['text_semi_month']               = '2 недели';
$_['text_month']                    = 'месяц ';
$_['text_year']                     = 'год ';
$_['text_your_details']             = 'Ваши Личные Данные';
$_['text_your_address']             = 'Адрес ';
$_['text_shipping_method']          = 'Выберите метод доставки Вашего заказа.';
$_['text_payment_method']           = 'Выберите способ оплаты Вашего заказа';
$_['text_i_am_returning_customer']  = 'Я зарегистрированный клиент';
$_['text_forgotten']                = 'Забыли пароль ';
$_['text_address_new']              = 'Добавить новый адрес доставки';
$_['text_address_existing']         = 'Доставка по адресу ';
$_['text_next']                     = 'Выберите следующее действие?';
$_['text_next_choice']              = 'Вы хотите воспользоваться бонусными кодами или рассчитать стоимость доставки.';
$_['text_agree']                    = 'Я прочитал и соглашаюсь с <a href="%s" class="agree" target="_blank"><b>%s</b></a>';
$_['text_agree_checkout']           = 'Я прочитал и соглашаюсь с <a href="%s" class="agree" target="_blank"><b>%s</b></a>';
$_['text_points']                   = 'Бонусные Баллы: %s';
$_['text_items']                    = '%s товар(и) - %s';
$_['text_trial_description']        = '%s каждый %d  за %d оплату';
$_['text_payment_description']      = '%s каждый %d  за %d оплату';
$_['text_payment_cancel']           = '%s каждый %d  за %d оплату';
$_['text_empty']                    = 'В корзине нет товаров!';
$_['text_logged']                   = 'Здравствуйте, %s';
$_['text_success_coupon']           = 'Поздравляем: Купон на скидку учтен! !';
$_['text_success_voucher']          = 'Поздравляем: Ваш подарочный сертификат учтен! ';
$_['text_success_reward']           = 'Поздравляем: ваши бонусные баллы использованы!  ';
$_['text_order_detail']             = 'Детали заказа ';
$_['text_invoice_no']               = 'Номер счета:';
$_['text_order_id']                 = 'Номер заказа:';
$_['text_status']                   = 'Статус:';
$_['text_date_added']               = 'Дата создания:';
$_['text_customer']                 = 'Клиент:';
$_['text_shipping_address']         = 'Адрес доставки:';
$_['text_shipping_method']          = 'Способ доставки:';
$_['text_payment_address']          = 'Адрес оплаты:';
$_['text_payment_method']           = 'Метод оплаты:';
$_['text_products']                 = 'Товары:';
$_['text_total']                    = 'В общем:';
$_['text_comment']                  = 'Комментарии к заказу ';
$_['text_history']                  = 'История заказов';
$_['text_empty']                    = 'У вас нет предыдущих заказов! ';
$_['text_error']                    = 'Мы не можем найти такой заказ!';
$_['text_success']                  = 'Поздравляем!';
$_['text_tax']                      = 'Ex Tax:';
$_['text_gift_coupon_code']         = 'Используйте этот подарочный купон для следующего заказа. ';
$_['text_make_a_choice']            = '-- Сделай выбор  --';
$_['text_payment_instructions']			= 'Указания по оплате';

$_['text_close_x']			= 'Скрыть ';
$_['text_select_n_p']			= 'Выбрать отделение';
$_['text_n_p_secession']			= 'Отделение';
$_['ways_of_packing']			= 'Способы упаковки';
$_['ways_of_packing_firm_1']			= 'Фирменная';
$_['ways_of_packing_firm_2']			= '(упаковка)';
$_['ways_of_packing_stels_1']			= 'Стелс-посылка';
$_['ways_of_packing_stels_2']			= '(+30-40 грн)';
$_['text_promo_productions']			= 'Высылать промо продукцию';
$_['text_order_confirm_x']			= 'Заказ подтверждаю';
$_['text_terms_x_1']			= 'Подтверждая заказ, я принимаю условия';
$_['text_terms_x_2']			= 'пользовательского соглашения';
$_['text_add_comment_x']			= 'Добавить комментарий к заказу';
$_['text_add_comment_x_placeholder'] = 'Текст коментария';
$_['text_delete_from_cart']          = 'Удалить из корзины';
$_['text_quick_order']               = 'Быстрый заказ';

$_['packing'] = 'Фасовка';
$_['pieces'] = 'шт';
// Entry
$_['entry_email']                   = 'E-Mail';
$_['entry_customer_group']          = 'Клиентская Группа ';
$_['entry_password']                = 'Пароль ';
$_['entry_confirm']                 = 'Подтвердить Пароль ';
$_['entry_shipping']                = 'Мои адресы доставки и оплаты одинаковые';
$_['entry_newsletter']              = 'Я хочу подписаться на  %s новости.';
$_['entry_coupon']                  = 'Введите купон тут';
$_['entry_voucher']                 = 'Введите подарочный код тут ';
$_['entry_reward']                  = 'Баллы для использования (Максимум %s)';

// Column
$_['column_image']                  = 'Фото';
$_['column_name']                   = 'Название товара';
$_['column_quantity']               = 'Количество';
$_['column_price']                  = 'Цена товара';
$_['column_total']                  = 'В общем';
$_['column_remove']                 = 'Удалить';
$_['column_model']                  = 'Модель';
$_['column_action']                 = 'Акция';
$_['column_date_added']             = 'Дата создания ';
$_['column_status']                 = 'Статус ';
$_['column_comment']                = 'Комментарий ';

// Error
$_['error_postcode']                = 'Индекс должен содержать от 2 до 10 знаков!';
$_['error_agree']                   = 'Внимание: Вы должны согласиться с %s!';
$_['error_agree_checkout']          = 'Внимание: Вы должны согласиться с %s!';
$_['error_shipping']                = 'Внимание: Выберите метод доставки!';
$_['error_no_shipping']             = 'Внимание: Достака невозможна. Напишите нам <a href="%s">contact us</a> и мы поможем!  ';
$_['error_payment']                 = 'Внимание: выберите метод оплаты!';
$_['error_no_payment']              = 'Внимание: Нет вариантов оплаты. <a href="%s">Напишите нам</a> и мы поможем!';
$_['error_stock']                   = 'Продукты, отмеченные *** недоступны в желаемом количестве или не в наличии!';
$_['error_minimum']                 = 'Минимальный заказ на  %s есть %s!';
$_['error_attempts']                = 'Внимание: Превышено количество попыток входа в Ваш аккаунт. Попробуйте через 1 час.';
$_['error_login']                   = 'Внимание: Неправильный email или пароль ';
$_['error_approved']                = 'Внимание: Подтвердите свой аккаунт перед входом.';
$_['error_password']                = 'Пароль должен содержать от 3 до 20 знаков!';
$_['error_confirm']                 = 'Подтверждение пароля и пароль не совпадают!';
$_['error_exists']                  = 'Внимание: такой email уже зарегистрирован!';
$_['error_coupon']                  = 'Внимание: Купон недействительный, старый или уже использован!';
$_['error_empty_coupon']            = 'Внимание: Введите код купона!';
$_['error_voucher']                 = 'Внимание: Подарочный сертификат недействительный, старый или уже использован!';
$_['error_empty_voucher']           = 'Внимание: Введите код сертификата!';
$_['error_reward']                  = 'Внимание: Введите количество бонусных баллов!';
$_['error_points']                  = 'Внимание: У Вас нет бонусных баллов!';
$_['error_maximum']                 = 'Внимание: Максимально можно применить %s баллов!';
$_['error_min_order_total']         = 'Внимание: Минимальная стоимость заказа %s!';
$_['error_max_order_total']         = 'Внимание: Максимальная стоимость заказа %s!';
$_['error_min_weight_total']        = 'Внимание: Минимальный вес товара %s!';
$_['error_max_weight_total']        = 'Внимание: Максимальный вес товара  %s!';
$_['error_min_quantity_total']      = 'Внимание: Минимальное количество товара  %s! ';
$_['error_max_quantity_total']      = 'Внимание: Максимальный вес товара  %s!';
$_['error_newsletter']              = 'Внимание: Подпишитесь на наши новости!';
$_['error_email_not_isset']         = 'Внимание: такой email не найдено! ';
$_['error_custom_field']            = '%s необходимо!';
$_['error_address']                 = 'Заполните все поля формы заказа!';
