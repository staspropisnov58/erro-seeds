<?php

// Heading

$_['heading_title']        = 'Ваш заказ сформирован!';



// Text

$_['text_basket']          = 'Корзина';
$_['word_success']          = 'Успех';
$_['text_checkout']        = 'Оформить заказ';

$_['text_success']         = 'Операция выполнена успешно';

$_['text_customer']        = '
<p class="text-big">Ваш заказ успешно создан!</p>
<p>Ваш номер заказа <span class="green">№%d</span></p>
<p>История заказа находится в <a href="%s" class="green">Личном кабинете</a>. Для просмотра истории, перейдите по ссылке <a href="%s" class="green">История заказов</a>.</p>
<div class="success_soc col-10 mt-5">
  <p>А пока наши работники склада собирают Вашу посылку, посмотрите интересные видео про каннабис</p>
  <p><a href="https://www.youtube.com/channel/UCiiuf9qa23p_6CSTmhXtmlQ/" target="_blank"><img src="/catalog/view/theme/old/images/success_youtube.png" alt="youtube"></a></p>
  <p>Или пообщайтесь с опытными гроверами в нашем уютном чате <a href="https://t.me/ESGroverclub" target="_blank" class="green">@ESGroverclub</a></p>
</div>
';

$_['text_guest']           = '

<p class="text-big">Ваш заказ успешно создан!</p>
<p>Ваш номер заказа <span class="green">№%d</span></p>
<div class="success_soc col-10 mt-5">
  <p>А пока наши работники склада собирают Вашу посылку, посмотрите интересные видео про каннабис</p>
  <p><a href="https://www.youtube.com/channel/UCiiuf9qa23p_6CSTmhXtmlQ/" target="_blank"><img src="/catalog/view/theme/old/images/success_youtube.png" alt="youtube"></a></p>
  <p>Или пообщайтесь с опытными гроверами в нашем уютном чате <a href="https://t.me/ESGroverclub" target="_blank" class="green">@ESGroverclub</a></p>
</div>';
