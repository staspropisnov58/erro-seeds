<style>
  #cart_edit_button {
    display: none
  }
</style>
<form id="comment" name="<?php echo $form['name']; ?>" action="<?php echo $form['action']; ?>" method="post">
  <div class="confirm d-flex flex-md-row flex-column flex-wrap justify-content-between">
    <div class="confirm_item col-12 col-md-5 p-0">
      <?php echo $gift_programs; ?>
      <h2><?php echo $text_items; ?></h2>
      <div class="confirm_info">
        <div class="checkout_prod_block">
          <?php foreach ($products as $product) { ?>
          <div class="checkout_product_item d-flex justify-content-between flex-row">
            <div class="d-flex flex-column w-100">
              <div class="d-flex flex-row justify-content-between">
                <div class="product_name w-75"><?php echo $product['name']; ?>
                  <?php if (isset($product['bonus'])&&$product['bonus']) { ?>
                  <p class="bonus"><?php echo sprintf($text_bonus, $product['bonus']['quantity']); ?></p>
                  <?php } ?>
                </div>
                <div class="product_price"><?php echo $product['total']; ?></div>
              </div>
              <div class="d-flex flex-row justify-content-sm-between justify-content-start mt-2 flex-wrap">
                <div class="d-flex flex-row product_option justify-content-between flex-wrap">
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <span><?php echo $option['name']; ?>:
                    <span><?php echo $option['value']; ?></span>
                  </span>
                  <?php } ?>
                  <?php } ?>
                  <span><?php echo $text_quantity; ?>: <span><?php echo $product['quantity']; ?></span></span>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <?php if ($subtotals) { ?>
        <div class="checkout_prices d-flex flex-column">
          <?php foreach ($subtotals as $subtotal) { ?>
          <div class="d-flex flex-row justify-content-between mb-1">
            <span class="price_text"><?php echo $subtotal['title']; ?>:</span>
            <span class="price"><?php echo $subtotal['text']; ?></span>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
        <div class="checkout_total_price">
          <div class="d-flex flex-row justify-content-between">
            <span class="price_text"><?php echo $total['title']; ?>:</span>
            <span class="price"><?php echo $total['text']; ?></span>
          </div>
        </div>
      </div>

    </div>
    <div class="col-12 col-md-6 d-flex flex-wrap flex-column p-0 mt-5 mt-md-0">
      <?php if ($order_info) { ?>
      <div class="confirm_item mb-4 p-0">
        <h2><?php echo $text_customer; ?>:</h2>
        <div class="confirm_info">
          <?php foreach ($order_info['customer'] as $customer_data) { ?>
          <p><?php echo $customer_data; ?></p>
          <?php } ?>
        </div>
      </div>
      <div class="confirm_item mb-4 p-0 ">
        <h2><?php echo $text_addresse_and_address; ?>:</h2>
        <div class="confirm_info">
          <p><?php echo $order_info['addressee']; ?></p>
          <p><?php echo $order_info['address']; ?></p>
        </div>
        <?php if (isset($text_new_address)) { ?>
        <div class="confirm_info"><?php echo $text_new_address;?></div>
        <?php } ?>
      </div>
      <div class="confirm_item mb-4 p-0 ">
        <h2><?php echo $text_shipping_method_confirm; ?>:</h2>
        <div class="confirm_info">
          <?php echo $order_info['shipping_method']; ?>
        </div>
      </div>
      <div class="confirm_item mb-4  p-0">
        <h2><?php echo $text_payment_method_h2; ?>:</h2>
        <div class="confirm_info">
          <?php echo $order_info['payment_method']; ?>
        </div>
      </div>
      <div class="confirm_item mb-4 p-0">
        <?php } else { ?>
        <div class="d-flex flex-column position-relative">
          <p style=" font-size: 21px;"><?php echo $text_note; ?></p>
        </div>
        <?php } ?>
        <div class="textarea_checkout d-flex flex-column">
          <?php foreach ($form['fields'] as $field_name => $field) { ?>
          <div>
            <label><?php echo $field['label'] ?>
              <?php if ($field['is_group']) { ?>
              <div class="d-flex flex-row position-relative">
                <?php foreach ($field['fields'] as $groupped_field_name => $groupped_field) { ?>
                <?php if ($groupped_field['type'] === 'select') { ?>
                <select name="<?php echo $groupped_field_name; ?>" class="form-control w-100 mr-2" id="customselect">
                  <?php foreach($countries as $country){ ?>
                  <?php if ($country['country_id'] === $groupped_field['value']) { ?>
                  <option selected value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?>
                  </option>
                  <?php }else{ ?>
                  <option value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
                <?php } else { ?>
                <input type="<?php echo $groupped_field['type']; ?>" name="<?php echo $groupped_field_name; ?>" class="m-0 form-control trans-input form-control-success" value="<?php echo $groupped_field['value']; ?>"
                  id="<?php echo $groupped_field_name; ?>" placeholder="<?php echo $groupped_field['placeholder']; ?>">
                <?php } ?>
                <?php } ?>
              </div>
              <?php } else { ?>
              <?php if ($field['type'] === 'textarea') { ?>
              <textarea class="w-100 form-control" name="<?php echo $field_name; ?>"></textarea>
              <?php } else { ?>
              <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control" value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>">
              <?php } ?>
              <?php } ?>
              <?php if ($field['error']) { ?>
              <span class="label_error"><?php echo $field['error']; ?></span>
              <?php } ?>
            </label>
          </div>
          <?php } ?>
          <?php if ($no_callback) { ?>
          <div class="checkbox-with-helper">
            <input type="checkbox" name="no_callback" id="no-callback" class="checkout_checkbox">
            <label for="no-callback" class="for_checkbox"><?php echo $entry_no_callback; ?></label>
            <span class="js-tooltip-trigger tooltip-trigger" data-target="no-callback-tooltip"><i class="fas fa-info-circle"></i></span>
          </div>
          <div class="js-tooltip nobs-tooltip" id="no-callback-tooltip"><?php echo $help_no_callback; ?>
            <button type="button" class="close js-close" aria-label="Close">
              <img src="/catalog/view/theme/old/images/close.svg" alt="">
            </button>
          </div>
          <?php } ?>
          <div class="d-flex flex-row justify-content-between mt-3">
            <?php if ($prev_step) { ?>
            <div class="button_step_back"><a href="<?php echo $prev_step_href; ?>" onclick="checkout.loadStep('<?php echo $prev_step; ?>', event)"><?php echo $button_back; ?></a></div>
            <?php } else { ?>
            <div class="button_step_back"><a href="<?php echo $href_back; ?>"><?php echo $button_back; ?></a></div>
            <?php } ?>
            <div class="button_step_forward justify-content-end d-flex active">
              <button type="submit" class="button_order"><?php echo $button_submit; ?></button>
            </div>
          </div>
        </div>
        <?php if ($order_info) {?>
      </div>
      <?php } ?>
    </div>
  </div>
</form>
