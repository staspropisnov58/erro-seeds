<!DOCTYPE html>
<html>
<head>
	<title><?php echo $heading_title; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T36VTNM');
  </script>
	<?php echo $e_commerce; ?>
	<link rel="preload" href="/catalog/view/theme/old/fonts/roboto-regular-webfont.woff" as="font" type="font/woff" crossorigin>
	<link rel="preload" href="/catalog/view/theme/old/fonts/AGLettericaCondensed-Roman.woff" as="font" type="font/woff" crossorigin>
	<link rel="preload" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="preload" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" as="style" onload="this.onload=null;this.rel='stylesheet'" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/catalog/view/theme/old/css/checkout/style_new_checkout.css?v=1.2.2">
	<noscript>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	</noscript>
	<script src="/catalog/view/theme/old/js/jquery-3.3.1.min.js"></script>
	<script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script type="text/javascript" defer src="/catalog/view/theme/old/js/common.js"></script>
	<script type="text/javascript" defer src="/catalog/view/theme/old/js/checkout/cart.js"></script>
	<script type="text/javascript" defer src="/catalog/view/theme/old/js/checkout/checkout.js?v=1.2.1"></script>
	<style>
	html, body {
    width: 100%;
    height: 100%;
	}
	.hidden, .shown {
	    max-height: 0;
	    overflow-y: hidden;
	    -webkit-transition: max-height 0.8s ease-in-out;
	    -moz-transition: max-height 0.8s ease-in-out;
	    -o-transition: max-height 0.8s ease-in-out;
	    transition: max-height 0.8s ease-in-out;
	}
	.shown {
	    max-height: 50em;
	}
	</style>
	<noscript>
	  <style>
	  .hidden {
	      max-height: 50em;
	  }
	  </style>
	</noscript>
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T36VTNM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
	<div class="d-flex justify-content-between mb-3">
		<div class="col-12 d-flex flex-row align-items-stretch checkout_left_block <?php if($cart) { ?>col-lg-7 flex-column flex-lg-row<?php } ?>">
			<div class="checkout_logo">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 71 60" style="enable-background:new 0 0 71 60;" xml:space="preserve">
					<style type="text/css">
						.logo0{fill:#09B50C;}
						.logo1{enable-background:new ;}
					</style>
					<g>
						<g>
							<path class="logo0" d="M25.6,39.8H7.1V28.4h16.6c1.2,0,2.2-0.9,2.2-2.2c0-1.2-1-2.2-2.2-2.2h-13H7.1c-2.9,0-4.7,0.7-4.7,3.9v13.9
								c0,1.3,1,2.3,2.3,2.3h20.9c1.2,0,2.2-0.9,2.2-2.2C27.8,40.8,26.8,39.8,25.6,39.8L25.6,39.8z M7.1,12.7h3.6h14.9
								c1.2,0,2.2-0.9,2.2-2.2c0-1.2-1-2.2-2.2-2.2H4.7c-1.3,0-2.3,1.1-2.3,2.4C2.3,13.3,5.6,12.8,7.1,12.7L7.1,12.7L7.1,12.7z"/>
							<path class="logo0" d="M37,18c0-3.8,4-5.8,9.3-5.8c3.2,0,7,2.2,9.1,3.2c0.3,0.1,0.6,0.2,0.8,0.2c1.3,0,2.1-1.1,2.1-2.2
								c0-1.2-0.8-1.7-1.5-2c-2.5-1.1-5.9-3.5-11-3.5c-8.8,0-13.5,4.8-13.5,10.5c0,12.6,22.1,8.4,22.1,16c0,4.5-4,5.9-9.1,5.9
								c-4.5,0-7.8-3.3-9.3-4.2c-0.3-0.2-0.7-0.2-1-0.2c-1.3,0-2.1,1-2.1,2.2c0,0.9,0.8,1.6,1.5,2.1c2.6,1.9,6.2,4.6,11.4,4.6
								c8.1,0,13.3-4.2,13.3-10C59.1,21.2,37,26.9,37,18L37,18L37,18z"/>
						</g>
						<g>
							<g>
								<path class="logo0" d="M67.7,8.5c0.1,0.1-0.1,0.4-0.5,0.4s-0.5-0.1-0.6-0.3L65,6.5h-0.8v2.1c0,0.3-0.1,0.3-0.4,0.3h-0.1
									c-0.3,0-0.4,0-0.4-0.3V3.3c0-0.3,0.2-0.5,0.5-0.5h1.7c1.5,0,2.3,0.6,2.3,1.8c0,1-0.6,1.7-1.7,1.8L67.7,8.5z M65.4,5.8
									c0.9,0,1.3-0.4,1.3-1.1s-0.4-1.1-1.3-1.1h-1.2v2.2H65.4z"/>
							</g>
							<g>
								<path class="logo0" d="M67.7,8.5c0.1,0.1-0.1,0.4-0.5,0.4s-0.5-0.1-0.6-0.3L65,6.5h-0.8v2.1c0,0.3-0.1,0.3-0.4,0.3h-0.1
									c-0.3,0-0.4,0-0.4-0.3V3.3c0-0.3,0.2-0.5,0.5-0.5h1.7c1.5,0,2.3,0.6,2.3,1.8c0,1-0.6,1.7-1.7,1.8L67.7,8.5z M65.4,5.8
									c0.9,0,1.3-0.4,1.3-1.1s-0.4-1.1-1.3-1.1h-1.2v2.2H65.4z"/>
							</g>
							<g>
								<path class="logo0" d="M65.2,11.2C62.4,11.2,60,8.9,60,6s2.3-5.2,5.2-5.2c2.9,0,5.2,2.3,5.2,5.2C70.5,8.9,68.1,11.2,65.2,11.2z
									 M65.2,1.6c-2.4,0-4.4,2-4.4,4.4s2,4.4,4.4,4.4s4.4-2,4.4-4.4S67.7,1.6,65.2,1.6z"/>
							</g>
							<g>
								<path class="logo0" d="M65.2,11.2C62.4,11.2,60,8.9,60,6s2.3-5.2,5.2-5.2c2.9,0,5.2,2.3,5.2,5.2C70.5,8.9,68.1,11.2,65.2,11.2z
									 M65.2,1.6c-2.4,0-4.4,2-4.4,4.4s2,4.4,4.4,4.4s4.4-2,4.4-4.4S67.7,1.6,65.2,1.6z"/>
							</g>
						</g>
						<g class="logo1">
							<path class="logo0" d="M3.6,56.7c0,1.7,1.1,2.4,2.4,2.4c0.9,0,1.5-0.2,1.9-0.4l0.2,0.9c-0.4,0.2-1.2,0.4-2.3,0.4
								c-2.1,0-3.4-1.4-3.4-3.5s1.2-3.8,3.3-3.8c2.3,0,2.9,2,2.9,3.3c0,0.3,0,0.5,0,0.6H3.6z M7.3,55.8c0-0.8-0.3-2.1-1.8-2.1
								c-1.3,0-1.8,1.2-1.9,2.1H7.3z"/>
							<path class="logo0" d="M9.9,58.6c0.4,0.2,1,0.5,1.7,0.5c0.9,0,1.4-0.5,1.4-1c0-0.6-0.4-0.9-1.3-1.3c-1.3-0.4-1.8-1.1-1.8-2
								c0-1.1,0.9-2,2.4-2c0.7,0,1.3,0.2,1.7,0.4l-0.3,0.9c-0.3-0.2-0.8-0.4-1.4-0.4c-0.7,0-1.2,0.4-1.2,0.9c0,0.6,0.4,0.8,1.3,1.2
								c1.2,0.5,1.8,1.1,1.8,2.1c0,1.2-0.9,2.1-2.6,2.1c-0.8,0-1.5-0.2-2-0.5L9.9,58.6z"/>
							<path class="logo0" d="M17.3,51v2h1.8v1h-1.8v3.8c0,0.9,0.2,1.4,0.9,1.4c0.3,0,0.6,0,0.7-0.1l0.1,1c-0.2,0.1-0.6,0.2-1.1,0.2
								c-0.6,0-1.1-0.2-1.4-0.5c-0.4-0.4-0.5-1-0.5-1.8v-3.8H15v-1h1.1v-1.7L17.3,51z"/>
							<path class="logo0" d="M20.2,59.2c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.8,0.4,0.8,0.9c0,0.5-0.3,0.9-0.9,0.9
								C20.6,60.1,20.2,59.7,20.2,59.2z"/>
						</g>
						<g class="logo1">
							<path class="logo0" d="M26.1,59.9v-1.3l1.2-1.1c2-1.8,3-2.9,3.1-4c0-0.8-0.5-1.4-1.5-1.4c-0.8,0-1.5,0.4-2,0.8l-0.6-1.6
								c0.7-0.5,1.8-1,3.1-1c2.1,0,3.3,1.2,3.3,2.9c0,1.6-1.1,2.8-2.5,4l-0.9,0.7v0h3.5v1.8H26.1z"/>
							<path class="logo0" d="M41,55.2c0,2.9-1.2,4.9-3.6,4.9c-2.4,0-3.5-2.2-3.5-4.8c0-2.7,1.2-4.8,3.6-4.8C40.1,50.4,41,52.6,41,55.2z
								 M36.2,55.3c0,2.1,0.5,3.2,1.4,3.2c0.8,0,1.3-1.1,1.3-3.2c0-2.1-0.4-3.2-1.3-3.2C36.7,52.1,36.2,53.1,36.2,55.3z"/>
							<path class="logo0" d="M49,55.2c0,2.9-1.2,4.9-3.6,4.9c-2.4,0-3.5-2.2-3.5-4.8c0-2.7,1.2-4.8,3.6-4.8C48,50.4,49,52.6,49,55.2z
								 M44.2,55.3c0,2.1,0.5,3.2,1.4,3.2s1.3-1.1,1.3-3.2c0-2.1-0.4-3.2-1.3-3.2C44.7,52.1,44.2,53.1,44.2,55.3z"/>
							<path class="logo0" d="M50.8,58.3c0.3,0,0.5,0,0.9,0c0.7,0,1.3-0.2,1.8-0.6c0.6-0.4,1-1,1.2-1.7l0,0c-0.4,0.4-1,0.7-1.9,0.7
								c-1.6,0-2.9-1.1-2.9-2.9c0-1.8,1.5-3.4,3.5-3.4c2.4,0,3.5,1.8,3.5,4c0,1.9-0.6,3.4-1.6,4.3c-0.9,0.8-2.1,1.3-3.6,1.3
								c-0.4,0-0.7,0-1,0V58.3z M52.1,53.6c0,0.8,0.4,1.5,1.3,1.5c0.6,0,1-0.3,1.2-0.6c0.1-0.1,0.1-0.3,0.1-0.5c0-1-0.4-2-1.4-2
								C52.7,52,52.1,52.7,52.1,53.6z"/>
						</g>
					</g>
					</svg>
			</div>
			<div class="d-flex flex-column checkout_center ">
				<div class="checkout_top d-flex flex-row justify-content-between align-items-start">
					<div class="checkout_head w-100">
						<div class="checkout_title d-flex justify-content-between align-items-center">
							<a href="/" class="small_logo">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 71 60" style="enable-background:new 0 0 71 60;" xml:space="preserve">
									<style type="text/css">
										.logo0{fill:#09B50C;}
										.logo1{enable-background:new ;}
									</style>
									<g>
										<g>
											<path class="logo0" d="M25.6,39.8H7.1V28.4h16.6c1.2,0,2.2-0.9,2.2-2.2c0-1.2-1-2.2-2.2-2.2h-13H7.1c-2.9,0-4.7,0.7-4.7,3.9v13.9
												c0,1.3,1,2.3,2.3,2.3h20.9c1.2,0,2.2-0.9,2.2-2.2C27.8,40.8,26.8,39.8,25.6,39.8L25.6,39.8z M7.1,12.7h3.6h14.9
												c1.2,0,2.2-0.9,2.2-2.2c0-1.2-1-2.2-2.2-2.2H4.7c-1.3,0-2.3,1.1-2.3,2.4C2.3,13.3,5.6,12.8,7.1,12.7L7.1,12.7L7.1,12.7z"/>
											<path class="logo0" d="M37,18c0-3.8,4-5.8,9.3-5.8c3.2,0,7,2.2,9.1,3.2c0.3,0.1,0.6,0.2,0.8,0.2c1.3,0,2.1-1.1,2.1-2.2
												c0-1.2-0.8-1.7-1.5-2c-2.5-1.1-5.9-3.5-11-3.5c-8.8,0-13.5,4.8-13.5,10.5c0,12.6,22.1,8.4,22.1,16c0,4.5-4,5.9-9.1,5.9
												c-4.5,0-7.8-3.3-9.3-4.2c-0.3-0.2-0.7-0.2-1-0.2c-1.3,0-2.1,1-2.1,2.2c0,0.9,0.8,1.6,1.5,2.1c2.6,1.9,6.2,4.6,11.4,4.6
												c8.1,0,13.3-4.2,13.3-10C59.1,21.2,37,26.9,37,18L37,18L37,18z"/>
										</g>
										<g>
											<g>
												<path class="logo0" d="M67.7,8.5c0.1,0.1-0.1,0.4-0.5,0.4s-0.5-0.1-0.6-0.3L65,6.5h-0.8v2.1c0,0.3-0.1,0.3-0.4,0.3h-0.1
													c-0.3,0-0.4,0-0.4-0.3V3.3c0-0.3,0.2-0.5,0.5-0.5h1.7c1.5,0,2.3,0.6,2.3,1.8c0,1-0.6,1.7-1.7,1.8L67.7,8.5z M65.4,5.8
													c0.9,0,1.3-0.4,1.3-1.1s-0.4-1.1-1.3-1.1h-1.2v2.2H65.4z"/>
											</g>
											<g>
												<path class="logo0" d="M67.7,8.5c0.1,0.1-0.1,0.4-0.5,0.4s-0.5-0.1-0.6-0.3L65,6.5h-0.8v2.1c0,0.3-0.1,0.3-0.4,0.3h-0.1
													c-0.3,0-0.4,0-0.4-0.3V3.3c0-0.3,0.2-0.5,0.5-0.5h1.7c1.5,0,2.3,0.6,2.3,1.8c0,1-0.6,1.7-1.7,1.8L67.7,8.5z M65.4,5.8
													c0.9,0,1.3-0.4,1.3-1.1s-0.4-1.1-1.3-1.1h-1.2v2.2H65.4z"/>
											</g>
											<g>
												<path class="logo0" d="M65.2,11.2C62.4,11.2,60,8.9,60,6s2.3-5.2,5.2-5.2c2.9,0,5.2,2.3,5.2,5.2C70.5,8.9,68.1,11.2,65.2,11.2z
													 M65.2,1.6c-2.4,0-4.4,2-4.4,4.4s2,4.4,4.4,4.4s4.4-2,4.4-4.4S67.7,1.6,65.2,1.6z"/>
											</g>
											<g>
												<path class="logo0" d="M65.2,11.2C62.4,11.2,60,8.9,60,6s2.3-5.2,5.2-5.2c2.9,0,5.2,2.3,5.2,5.2C70.5,8.9,68.1,11.2,65.2,11.2z
													 M65.2,1.6c-2.4,0-4.4,2-4.4,4.4s2,4.4,4.4,4.4s4.4-2,4.4-4.4S67.7,1.6,65.2,1.6z"/>
											</g>
										</g>
										<g class="logo1">
											<path class="logo0" d="M3.6,56.7c0,1.7,1.1,2.4,2.4,2.4c0.9,0,1.5-0.2,1.9-0.4l0.2,0.9c-0.4,0.2-1.2,0.4-2.3,0.4
												c-2.1,0-3.4-1.4-3.4-3.5s1.2-3.8,3.3-3.8c2.3,0,2.9,2,2.9,3.3c0,0.3,0,0.5,0,0.6H3.6z M7.3,55.8c0-0.8-0.3-2.1-1.8-2.1
												c-1.3,0-1.8,1.2-1.9,2.1H7.3z"/>
											<path class="logo0" d="M9.9,58.6c0.4,0.2,1,0.5,1.7,0.5c0.9,0,1.4-0.5,1.4-1c0-0.6-0.4-0.9-1.3-1.3c-1.3-0.4-1.8-1.1-1.8-2
												c0-1.1,0.9-2,2.4-2c0.7,0,1.3,0.2,1.7,0.4l-0.3,0.9c-0.3-0.2-0.8-0.4-1.4-0.4c-0.7,0-1.2,0.4-1.2,0.9c0,0.6,0.4,0.8,1.3,1.2
												c1.2,0.5,1.8,1.1,1.8,2.1c0,1.2-0.9,2.1-2.6,2.1c-0.8,0-1.5-0.2-2-0.5L9.9,58.6z"/>
											<path class="logo0" d="M17.3,51v2h1.8v1h-1.8v3.8c0,0.9,0.2,1.4,0.9,1.4c0.3,0,0.6,0,0.7-0.1l0.1,1c-0.2,0.1-0.6,0.2-1.1,0.2
												c-0.6,0-1.1-0.2-1.4-0.5c-0.4-0.4-0.5-1-0.5-1.8v-3.8H15v-1h1.1v-1.7L17.3,51z"/>
											<path class="logo0" d="M20.2,59.2c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.8,0.4,0.8,0.9c0,0.5-0.3,0.9-0.9,0.9
												C20.6,60.1,20.2,59.7,20.2,59.2z"/>
										</g>
										<g class="logo1">
											<path class="logo0" d="M26.1,59.9v-1.3l1.2-1.1c2-1.8,3-2.9,3.1-4c0-0.8-0.5-1.4-1.5-1.4c-0.8,0-1.5,0.4-2,0.8l-0.6-1.6
												c0.7-0.5,1.8-1,3.1-1c2.1,0,3.3,1.2,3.3,2.9c0,1.6-1.1,2.8-2.5,4l-0.9,0.7v0h3.5v1.8H26.1z"/>
											<path class="logo0" d="M41,55.2c0,2.9-1.2,4.9-3.6,4.9c-2.4,0-3.5-2.2-3.5-4.8c0-2.7,1.2-4.8,3.6-4.8C40.1,50.4,41,52.6,41,55.2z
												 M36.2,55.3c0,2.1,0.5,3.2,1.4,3.2c0.8,0,1.3-1.1,1.3-3.2c0-2.1-0.4-3.2-1.3-3.2C36.7,52.1,36.2,53.1,36.2,55.3z"/>
											<path class="logo0" d="M49,55.2c0,2.9-1.2,4.9-3.6,4.9c-2.4,0-3.5-2.2-3.5-4.8c0-2.7,1.2-4.8,3.6-4.8C48,50.4,49,52.6,49,55.2z
												 M44.2,55.3c0,2.1,0.5,3.2,1.4,3.2s1.3-1.1,1.3-3.2c0-2.1-0.4-3.2-1.3-3.2C44.7,52.1,44.2,53.1,44.2,55.3z"/>
											<path class="logo0" d="M50.8,58.3c0.3,0,0.5,0,0.9,0c0.7,0,1.3-0.2,1.8-0.6c0.6-0.4,1-1,1.2-1.7l0,0c-0.4,0.4-1,0.7-1.9,0.7
												c-1.6,0-2.9-1.1-2.9-2.9c0-1.8,1.5-3.4,3.5-3.4c2.4,0,3.5,1.8,3.5,4c0,1.9-0.6,3.4-1.6,4.3c-0.9,0.8-2.1,1.3-3.6,1.3
												c-0.4,0-0.7,0-1,0V58.3z M52.1,53.6c0,0.8,0.4,1.5,1.3,1.5c0.6,0,1-0.3,1.2-0.6c0.1-0.1,0.1-0.3,0.1-0.5c0-1-0.4-2-1.4-2
												C52.7,52,52.1,52.7,52.1,53.6z"/>
										</g>
									</g>
									</svg>
							</a>
							<h1><?php echo $text_h1; ?></h1>
							<button id="cart_edit_button" onclick="checkout.openCart()">
								<svg viewBox="0 0 23 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" clip-rule="evenodd" d="M1.92639 2.47358H1.46323C0.966176 2.47358 0.563232 2.07064 0.563232 1.57358C0.563232 1.07653 0.966176 0.673584 1.46323 0.673584H1.92639C3.51011 0.673584 4.80739 1.77545 5.13303 3.39616L5.14164 3.45609H19.9982C21.4661 3.45609 22.58 4.73418 22.3933 6.21235L21.6833 10.8581C21.2253 12.6732 19.6822 13.9475 17.8001 13.9475H6.64857L6.88394 15.5862C6.9429 16.112 7.43712 16.5076 8.07376 16.5076H19.6527C20.1498 16.5076 20.5527 16.9105 20.5527 17.4076C20.5527 17.9047 20.1498 18.3076 19.6527 18.3076H8.07376C6.56002 18.3076 5.26125 17.2681 5.0987 15.8145L4.83069 13.9475H4.82863L4.29024 10.1826L3.35997 3.70221C3.20982 2.95828 2.63917 2.47358 1.92639 2.47358ZM6.39003 12.1475H17.8001C18.8358 12.1475 19.6768 11.4531 19.9208 10.5028L20.6106 5.96454C20.6592 5.57907 20.3777 5.25609 19.9982 5.25609H5.40404L6.27774 11.3658L6.39003 12.1475Z" fill="white"></path>
									<path d="M17.1053 23.0611C18.0645 23.0611 18.8421 22.2905 18.8421 21.34C18.8421 20.3895 18.0645 19.6189 17.1053 19.6189C16.146 19.6189 15.3684 20.3895 15.3684 21.34C15.3684 22.2905 16.146 23.0611 17.1053 23.0611Z" fill="#65BD00"></path>
									<path d="M8.26321 23.0611C9.22244 23.0611 10.0001 22.2905 10.0001 21.34C10.0001 20.3895 9.22244 19.6189 8.26321 19.6189C7.30398 19.6189 6.52637 20.3895 6.52637 21.34C6.52637 22.2905 7.30398 23.0611 8.26321 23.0611Z" fill="#65BD00"></path>
								</svg>
								<i class="fas fa-chevron-right"></i>
							</button>
						</div>
            <?php if ($steps) { ?>
              <div class="checkout_step d-md-flex flex-row align-items-center mb-5 flex-wrap d-none ">
								<?php foreach ($steps as $step) { ?>
									<span class="label_step <?php echo $step['class']; ?>" id="to_<?php echo $step['name']; ?>">
									<?php if ($step['href']) { ?>
										<a href="<?php echo $step['href']; ?>" onclick="checkout.loadStep('<?php echo $step['name']; ?>', event)">
										<?php echo $step['text']; ?>
										</a>
									<?php } else { ?>
										<?php echo $step['text']; ?>
									<?php } ?>
								</span>
								<?php } ?>
              </div>
            <?php } ?>
						<?php if ($messages) { ?>
							<div id="messages">
								<?php foreach ($messages as $message) { ?>
									<div class="alert alert-<?php echo $message['type']; ?> col-12">
										<?php echo $message['text']; ?>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="checkout_content" id="checkout_content">
					<div id="step_<?php echo $current_step; ?>">
						<?php echo $content; ?>
					</div>
				</div>
			</div>
			<?php echo $cart; ?>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('#customselect').SelectCustomizer();
	});
</script>

</html>
