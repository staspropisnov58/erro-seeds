<div class="checkout_right_block flex-column d-flex justify-content-between" id="cart-content">
  <div class="top_cart 1">
    <div class="checkout_prod_title d-flex justify-content-between align-items-center">
      <p class="title"><?php echo $text_items; ?></p>
      <?php if ($start_checkout) { ?>
      <a class="close" href="<?php echo $current_page; ?>" onclick="cart.close(event);">
        <img src="/catalog/view/theme/old/images/close.svg" alt="">
      </a>
      <?php } else { ?>
      <a class="close close_in_checkout" href="/">
        <img src="/catalog/view/theme/old/images/close.svg" alt="">
      </a>
      <button class="close close_cart" onclick="checkout.closeincheckout();">
        <img src="/catalog/view/theme/old/images/close.svg" alt="">
      </button>
      <?php } ?>
    </div>
    <?php if ($products) { ?>
    <?php $error_warning = false; ?>
    <?php if ($error_min_order) { ?>
      <?php $error_warning = true; ?>
    <div class="cash_on_dilivery">
      <?php echo $error_min_order; ?>
    </div>
    <?php } ?>
    <?php if ($error_payment_min_order) { ?>
    <div class="cash_on_dilivery">
      <?php echo $error_payment_min_order; ?>
    </div>
    <?php } ?>
    <div class="checkout_prod_block">
      <?php if ($cart_gifts) { ?>
      <?php foreach ($cart_gifts['gift_programs'] as $gift_program_is => $program) { ?>
      <?php if (count($program['gifts']) === 1) { ?>
      <div class="checkout_product_item cart_gifts justify-content-between flex-row">
        <div class="">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" class="checkout_prod_img" style="enable-background:new 0 0 512 512;min-width:50px; max-width:50px;"
            xml:space="preserve" fill="#2a2e32">
            <g>
              <g>
                <path d="M456.348,100.174h-59.071c5.702-9.832,8.984-21.231,8.984-33.391C406.261,29.959,376.302,0,339.478,0
            			C304.672,0,273.961,17.847,256,44.867C238.039,17.847,207.328,0,172.522,0c-36.824,0-66.783,29.959-66.783,66.783
            			c0,12.16,3.282,23.56,8.984,33.391H55.652c-27.618,0-50.087,22.469-50.087,50.087v77.913c0,9.22,7.475,16.696,16.696,16.696
            			h16.696v217.043c0,27.618,22.469,50.087,50.087,50.087c13.969,0,318.531,0,333.913,0c27.618,0,50.087-22.469,50.087-50.087V244.87
            			h16.696c9.22,0,16.696-7.475,16.696-16.696v-77.913C506.435,122.643,483.966,100.174,456.348,100.174z M172.522,33.391
            			c36.824,0,66.783,29.959,66.783,66.783c-7.312,0-58.286,0-66.783,0c-18.412,0-33.391-14.979-33.391-33.391
            			C139.13,48.371,154.11,33.391,172.522,33.391z M205.913,478.609H89.044c-9.206,0-16.696-7.49-16.696-16.696V244.87h133.565
            			V478.609z M205.913,211.478c-6.963,0-159.941,0-166.956,0v-61.217c0-9.206,7.49-16.696,16.696-16.696c3.017,0,146.966,0,150.261,0
            			V211.478z M272.696,478.609h-33.391V244.87h33.391V478.609z M272.696,211.478h-33.391v-77.913c12.083,0,21.308,0,33.391,0V211.478
            			z M339.478,33.391c18.412,0,33.391,14.979,33.391,33.391c0,18.412-14.979,33.391-33.391,33.391c-7.672,0-59.585,0-66.783,0
            			C272.696,63.35,302.654,33.391,339.478,33.391z M439.652,461.913c0,9.206-7.49,16.696-16.696,16.696h-116.87V244.87h133.565
            			V461.913z M473.043,211.478c-7.018,0-159.998,0-166.956,0v-77.913c3.296,0,147.246,0,150.261,0c9.206,0,16.696,7.49,16.696,16.696
            			V211.478z" />
              </g>
            </g>
          </svg>
        </div>
        <div class="d-flex flex-column w-100 ml-3">
          <div class="product_name">
            <a href="<?php echo $program['gifts'][0]['href']; ?>"><?php echo $program['gifts'][0]['name']; ?></a>
          </div>
          <div class="d-flex flex-wrap justify-content-between">
            <div class="product_option d-flex flex-row flex-wrap align-items-center">
              <span class="mr-3"><?php echo $program['gifts'][0]['model']; ?></span>
              <span class="mr-3"><?php echo $cart_gifts['text_quantity']; ?>: <?php echo $program['gifts'][0]['quantity']; ?></span>
            </div>
            <p class="bonus"><?php echo $cart_gifts['text_cart_gift']; ?></p>
          </div>
        </div>
      </div>
      <?php } ?>
      <?php } ?>
      <?php } ?>
      <?php foreach ($products as $product) { ?>
      <div class="checkout_product_item flex-column js-product-container <?php if ($product['error_maximum_exeeded'] || !$product['stock'])  { ?>alert-warning<?php } ?>">
        <?php if ($product['error_maximum_exeeded']) { ?>
        <?php $error_warning = true; ?>
        <div class="cash_on_dilivery">
          <?php echo $product['error_maximum_exeeded']; ?>
        </div>
        <?php } ?>
        <?php if (!$product['stock']) { ?>
        <?php if ($error_stock) { ?>
        <?php $error_warning = true; ?>
        <div class="cash_on_dilivery">
          <?php echo $error_stock; ?>
        </div>
        <?php } ?>
        <?php } ?>
        <div class="justify-content-between flex-row d-flex">
          <div class="">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="checkout_prod_img">
          </div>
          <div class="d-flex flex-row ml-3 align-items-end w-100 flex-wrap">
            <div class="d-flex flex-row justify-content-between w-100">
              <div class="product_name">
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              </div>
              <div class="d-flex align-items-center justify-content-end delete_block">
                <form class="d-flex flex-column cart-action" action="<?php echo $remove_action; ?>" name="remove" method="post">
                  <input type="hidden" name="key" value="<?php echo $product['key']; ?>">
                  <noscript>
                    <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
                  </noscript>
                  <button class="product_delete" onclick="checkout_cart.remove('<?php echo $product['key']; ?>', event)"><i class="far fa-trash-alt"></i></button>
                </form>
              </div>
            </div>
            <div class="product_option d-flex flex-row align-items-center flex-wrap w-100 mb-2">
              <span class="mr-3"><?php echo $product['model']; ?></span>
              <span class="mr-3"><?php echo $product['sku']; ?></span>
              <?php if ($product['option']) { ?>
              <span class="mr-3">
                <?php foreach ($product['option'] as $option) { ?>
                <span><?php echo $option['name']; ?>: <span><?php echo $option['value']; ?></span></span><br>
                <?php } ?>
              </span>
              <?php } ?>
            </div>
            <div class="product_quantity w-50 d-flex flex-wrap flex-row align-items-center justify-content-start js-quantity-container">
              <form class="d-flex flex-row cart-action" action="<?php echo $edit_action; ?>" name="edit" method="post" onsubmit="checkout_cart.edit(this, event)">
                <span class="minus" onclick="checkout_cart.plus('-1', event)">-</span>
                  <input type="tel" class="quantity" value="<?php echo $product['quantity']; ?>" name="quantity[<?php echo $product['key']; ?>]" onchange="checkout_cart.edit(this.form, event)">
                  <span class="plus" onclick="checkout_cart.plus('1', event)">+</span>
                  <noscript>
                    <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
                    <button name="refresh"><?php echo $button_update; ?></button>
                  </noscript>
              </form>
            </div>
            <div class="w-50 text-right">
              <div class="product_price">
                <span class="js-current-price" data-unit-value="<?php echo $product['price']; ?>"><?php echo $product['total']; ?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php echo $coupon; ?>
    <div class="prices">
      <?php if ($subtotals) { ?>
      <div class="checkout_prices d-flex flex-column mb-2">
        <?php foreach ($subtotals as $subtotal) { ?>
        <div class="d-flex flex-row justify-content-between">
          <span class="price_text"><?php echo $subtotal['title']; ?>:</span>
          <div class="d-flex">
            <?php if ($subtotal['remove']) { ?>
            <form class="d-flex flex-column cart-action" action="<?php echo $subtotal['remove']; ?>" name="remove_<?php echo $subtotal['code']; ?>" method="post">
              <noscript>
                <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
              </noscript>
              <button class="product_delete" onclick="checkout_cart.remove_<?php echo $subtotal['code']; ?>(event)"><?php echo $button_remove; ?></button>
            </form>
            <?php } ?>
            <span class="price">
              <span class=""><?php echo $subtotal['text']; ?></span>
            </span>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <div class="checkout_total_price">
        <div class="d-flex flex-row justify-content-between">
          <span class="price_text"><?php echo $total['title']; ?>:</span>
          <span class="price">
            <span class=""><?php echo $total['text']; ?></span>
          </span>
        </div>
      </div>
    </div>
    <div <?php if( $customer_group_id !=3) { ?> style="display: none" <?php } ?> class="prices">
      <div class="checkout_total_price">
        <div class="d-flex flex-row justify-content-between">
          <span class="price_text"><?=$text_bonus_balans?>:</span>
          <span class="price">
            <span class=""><?=$balance?></span>
          </span>
        </div>
      </div>
      <div style="margin: 20px 0px;" class="checkout_total_price">
        <div class="d-flex flex-row justify-content-between">
          <span class="price_text">
            <label class="lab_bonus" for="input-bonuses"><?=$text_bonus_balans_sel?></label>
            <input class="form-control" id="input-bonuses" type="text" name="writing_off_bonuses" value="0" />
          </span>
          <span style="line-height: 93px;" class="price">
            <span class="">
              <button style="width: 100%;" class="button_order sale_bonuse active" type="submit"><?=$text_bonus_balans_but ?></button>
            </span>
          </span>
        </div>
      </div>
    </div>
  </div>
  <?php if (!$error_warning) { ?>
  <?php if ($start_checkout) { ?>
  <div class="checkout_buttons " id="cart_index">
    <?php if ($start_checkout) { ?>
    <?php if ($fast_order) { ?>
    <div class="button_step_back">
      <a id="fast_order" href="<?php echo $fast_order; ?>"><?php echo $text_quick_order; ?></a>
    </div>
    <?php } ?>
    <a class="button_order" href="<?php echo $start_checkout; ?>"><?php echo $text_next_step; ?></a>
    <?php } ?>
  </div>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
  <div class="checkout_no_product">
    <div class="d-flex justify-content-between flex-row align-items-center">
      <figure>
        <svg aria-hidden="true" data-prefix="fas" data-icon="frown-open" class="svg-inline--fa fa-frown-open fa-w-16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512">
          <path fill="currentColor"
            d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zM136 208c0-17.7 14.3-32 32-32s32 14.3 32 32-14.3 32-32 32-32-14.3-32-32zm187.3 183.3c-31.2-9.6-59.4-15.3-75.3-15.3s-44.1 5.7-75.3 15.3c-11.5 3.5-22.5-6.3-20.5-18.1 7-40 60.1-61.2 95.8-61.2s88.8 21.3 95.8 61.2c2 11.9-9.1 21.6-20.5 18.1zM328 240c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z" />
        </svg>
        <figcaption>
          <?php echo $text_empty_cart; ?>
        </figcaption>
      </figure>
    </div>
  </div>
</div>
<?php }?>
</div>


<style>
  .lab_bonus{
    font-size: 11px;
    color: #e36565;
    font-weight: 100;
    width:100%;
  }

</style>

<script>
  $(document).ready(function (){

    var balans = Number('<?=$balance_old?>');
    $('.sale_bonuse').on('click',function () {
      var bonuse = Number($('input[name=writing_off_bonuses]').val());
//if(bonuse>0){

      $.ajax({
        url: '/index.php?route=checkout/cart/addBonuse',
        type: 'post',
        data: 'sale_price=' + bonuse,
        dataType: 'json',
        success: function (json) {

 cart.reload();


        }
      });
  //  }

    })
    $('input[name=writing_off_bonuses]').keyup(function () {
      var bonuse = Number($('input[name=writing_off_bonuses]').val());
    if(bonuse>balans){
      $('input[name=writing_off_bonuses]').val(balans)
    }
    })
  })
</script>
