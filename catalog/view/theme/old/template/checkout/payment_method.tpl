<div class="d-flex justify-content-sm-between flex-column checkout_content_top">
  <h2><?php echo $text_payment_method_h2; ?>:</h2>
  <p><?php echo $text_choose_payment_method; ?></p>
</div>
<form class="d-flex flex-column" id="payment_method" name="<?php echo $form_name; ?>" method="post" action="<?php echo $form_action; ?>">
  <?php foreach ($payment_methods as $payment_method) { ?>
  <div class="payment_item d-flex justify-content-between flex-column align-items-start <?php if ($payment_method['error']) { ?>disabled<?php } ?>">
    <div class="d-flex flex-row w-100 align-items-center justify-content-between">
      <input type="radio" name="payment" class="checkout_checkbox" id="<?php echo $payment_method['code']; ?>" value="<?php echo $payment_method['code']; ?>" onclick="checkout.saveSelected(this)"
        <?php if ($payment_method['error']) { ?>disabled<?php } ?> <?php if ($payment_method['checked']) { ?>checked<?php } ?>>
      <label class="for_checkbox payment_name" for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label>
      <?php if ($payment_method['commission']) { ?>
      <div class="payment_cost text-right">
        <?php echo $payment_method['commission']; ?>
      </div>
      <?php } ?>
    </div>
    <div class="payment_info">
      <div class="payment_description">
        <?php if ($payment_method['error']) { ?>
        <?php echo $payment_method['error']; ?>
        <?php } else { ?>
        <?php echo $payment_method['terms']; ?>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php } ?>
  <div class="checkout_buttons " id="cart_index">
    <div class="button_step_back">
      <?php if ($prev_step) { ?>
      <a href="<?php echo $prev_step_href; ?>" onclick="loadStep('<?php echo $prev_step; ?>', event)"><?php echo $text_prev_step; ?></a>
      <?php } else { ?>
      <a href="/">На главную</a>
      <?php } ?>
    </div>
    <button class="button_order active" type="submit" onclick="checkout.sendForm(event)"><?php echo $text_next_step; ?></button>
  </div>
</form>
