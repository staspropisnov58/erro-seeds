<div>
  <h2><?php echo $text_h2; ?>:</h2>
  <?php echo $text_login; ?>
</div>
<form class="mt-5 d-flex w-100 flex-column " id="<?php echo $guest_form['name']; ?>" name="<?php echo $guest_form['name']; ?>" method="post" action="<?php echo $guest_form['action']; ?>" novalidate>
  <div class="input-group d-flex flex-row flex-wrap justify-content-between">
    <?php foreach ($guest_form['fields'] as $field_name => $field) { ?>
    <label><?php echo $field['label']; ?>
      <?php if ($field['is_group']) { ?>
      <div class="d-flex w-100 flex-row">
        <?php foreach ($field['fields'] as $groupped_field_name => $groupped_field) { ?>
        <?php if ($groupped_field['type'] === 'select') { ?>
        <select name="<?php echo $groupped_field_name; ?>" class="form-control mr-2 w-50" id="customselect">
          <?php foreach ($countries as $country) { ?>
          <?php if ($country['country_id'] === $groupped_field['value']) { ?>
          <option selected value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?>
          </option>
          <?php } else { ?>
          <option value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
        <?php } else { ?>
        <input type="<?php echo $groupped_field['type']; ?>" name="<?php echo $groupped_field_name; ?>" class="form-control" value="<?php echo $groupped_field['value']; ?>" id="<?php echo $groupped_field_name; ?>"
          placeholder="<?php echo $groupped_field['placeholder']; ?>">
        <?php } ?>
        <?php } ?>
      </div>
      <?php } else { ?>
      <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control" value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>">
      <?php } ?>
    </label>
    <?php } ?>
  </div>
  <?php if ($register_form) { ?>
  <div class="with_register ">
    <input type="checkbox" name="register" id="register_check" class="checkout_checkbox need_agree" onchange="needAgree(this)">
    <label for="register_check" class="for_checkbox"><?php echo $entry_register; ?></label>
    <div class="form_register d-flex flex-wrap mt-3 justify-content-between">
      <?php foreach ($register_form['fields'] as $field_name => $field) { ?>
      <label><?php echo $field['label']; ?>
        <?php if ($field['is_group']) { ?>
        <?php foreach ($field['fields'] as $groupped_field_name => $groupped_field) { ?>
        <?php if ($groupped_field['type'] === 'select') { ?>
        <select name="<?php echo $groupped_field_name; ?>" class="form-control mr-2 w-50" id="customselect">
          <?php foreach($countries as $country){ ?>
          <?php if ($country['country_id'] === $groupped_field['value']) { ?>
          <option selected value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?>
          </option>
          <?php }else{ ?>
          <option value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
        <?php } else { ?>
        <input type="<?php echo $groupped_field['type']; ?>" name="<?php echo $groupped_field_name; ?>" class="form-control" value="<?php echo $groupped_field['value']; ?>" id="<?php echo $groupped_field_name; ?>"
          placeholder="<?php echo $groupped_field['placeholder']; ?>">
        <?php } ?>
        <?php } ?>
        <?php } else { ?>
        <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control" value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>">
        <?php } ?>
      </label>
      <?php if ($field['error']) { ?>
      <span class="label_error"><?php echo $field['error']; ?></span>
      <?php } ?>
      <?php } ?>
      <div class="mt-3">
        <?php if ($customer_is_affiliate_status) { ?>
        <div class="form-group d-flex flex-column">
          <input type="checkbox" name="affiliate" id="affiliate" class="checkout_checkbox">
          <label for="affiliate" class="for_checkbox">
            <?php echo $text_affiliate; ?>
          </label>
        </div>
        <?php } ?>
        <?php if ($text_agree) { ?>
        <?php echo $text_agree; ?>
        <?php echo $text_warranty; ?>
        <?php } ?>
        <?php if (isset($SOCNETAUTH2_DATA)) { ?>
          <?php echo $SOCNETAUTH2_DATA['code']; ?>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
    <div class="checkout_buttons " id="cart_index">
      <div class="button_step_back">
        <?php if ($prev_step) { ?>
        <a href="<?php echo $prev_step_href; ?>" onclick="loadStep('<?php echo $prev_step; ?>', event)"><?php echo $text_prev_step; ?></a>
        <?php } else { ?>
        <a href="/">На главную</a>
        <?php } ?>
      </div>
      <button class="button_order active" type="submit" onclick="checkout.sendForm(event)"><?php echo $text_next_step; ?></button>
    </div>
</form>
