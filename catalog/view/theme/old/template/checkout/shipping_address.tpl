<div>
  <h2><?php echo $text_shipping_address_h2; ?></h2>
</div>
<form class="d-flex w-100 flex-column" id="<?php echo $form['name']; ?>" name="<?php echo $form['name']; ?>" method="post" action="<?php echo $form['action']; ?>" novalidate>
  <?php if ($addresses) { ?>
  <div class="input-group d-flex flex-row flex-wrap">
    <label class="w-100">
      <?php echo $entry_select_address; ?>
      <select class="custom-select" id="shipping" name="address_id">
        <?php foreach ($addresses as $address) { ?>
        <option value="<?php echo $address['address_id']; ?>" <?php echo $address['selected']; ?>><?php echo $address['text']; ?></option>
        <?php } ?>
      </select>
    </label>
  </div>
  <div class="with_register ">
    <input type="checkbox" name="new_address" id="register_check" class="checkout_checkbox">
    <label for="register_check" class="for_checkbox"><?php echo $entry_new_address; ?></label>
    <p><?php echo $text_new_address; ?></p>
    <div class="form_register d-flex flex-wrap mt-3 justify-content-between">
      <?php } else { ?>
      <?php if ($new_address_required) { ?>
      <input type="hidden" name="new_address" id="register_check" value="1">
      <?php } ?>
      <div class="with_register ">
        <div class="d-flex flex-wrap mt-3 justify-content-between">
          <?php } ?>
          <?php if ($default_values) { ?>
          <div class="d-flex flex-wrap flex-row w-100 mb-2">
            <?php foreach ($default_values as $value) { ?>
            <p style="color:#82b523;font-weight: 600;"><?php echo $value['label']; ?>: <?php echo $value['text']; ?></p>
            <?php } ?>
          </div>
          <?php } ?>
          <?php foreach ($form['fields'] as $field_name => $field) { ?>
          <?php if ($field['is_group']) { ?>
          <h3><?php echo $field['label']; ?>:</h3>
          <div class="d-flex flex-wrap flex-row w-100 justify-content-between mb-4">
            <?php foreach ($field['fields'] as $fieldset_field_name => $fieldset_field) { ?>
            <?php if ($fieldset_field['type'] === 'hidden') { ?>
            <input type="hidden" name="<?php echo $fieldset_field_name; ?>" value="<?php echo $fieldset_field['value']; ?>">
            <?php } else { ?>
            <label>
              <?php echo $fieldset_field['label']; ?>
              <?php if ($fieldset_field['type'] === 'select') { ?>
              <select name="<?php echo $fieldset_field_name; ?>" class="form-control" id="<?php echo $fieldset_field_name; ?>">
                <?php foreach($countries as $country){ ?>
                <?php if ($country['country_id'] === $fieldset_field['value']) { ?>
                <option selected value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php }else{ ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php } elseif ($fieldset_field['type'] === 'datalist') { ?>
              <input type="text" name="<?php echo $fieldset_field_name; ?>" list="list-<?php echo $fieldset_field_name; ?>" class="form-control trans-input form-control-success" id="<?php echo $fieldset_field_name; ?>">
              <datalist id="list-<?php echo $fieldset_field_name; ?>">
                <?php foreach ($fieldset_field['options'] as $option) { ?>
                <option value="<?php echo $option['name']; ?>"><?php echo $option['name']; ?></option>
                <?php } ?>
              </datalist>
              <?php } else { ?>
              <input type="<?php echo $fieldset_field['type']; ?>" name="<?php echo $fieldset_field_name; ?>" class="form-control trans-input form-control-success" value="<?php echo $fieldset_field['value']; ?>"
                id="<?php echo $fieldset_field_name; ?>" placeholder="<?php echo $fieldset_field['placeholder']; ?>">
              <?php } ?>
              <?php if ($fieldset_field['error']) { ?>
              <span class="label_error"><?php echo $fieldset_field['error']; ?></span>
              <?php } ?>
            </label>
            <?php } ?>
            <?php } ?>
          </div>
          <?php } else { ?>
          <label>
            <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control trans-input form-control-success" value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>"
              placeholder="<?php echo $field['placeholder']; ?>">
          </label>
          <?php if ($field['error']) { ?>
          <span class="label_error"><?php echo $field['error']; ?></span>
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </div>
      </div>
      <div class="checkout_buttons " id="cart_index">
        <div class="button_step_back">
          <?php if ($prev_step) { ?>
          <a href="<?php echo $prev_step_href; ?>" onclick="loadStep('<?php echo $prev_step; ?>', event)"><?php echo $text_prev_step; ?></a>
          <?php } else { ?>
          <a href="/">На главную</a>
          <?php } ?>
        </div>
        <button class="button_order active" type="submit" onclick="checkout.sendForm(event)"><?php echo $text_next_step; ?></button>
      </div>
</form>
