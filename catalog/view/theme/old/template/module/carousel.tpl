<div id="slides_mobile" class="slides">
    <div class="slides_container owl-carousel">
      <?php foreach ($banners as $banner) { ?>
      <div class="slide">
        <?php if ($banner['link']) { ?>
          <a href="<?php echo $banner['link']; ?>" target="_blank">
            <img alt="<?php echo $banner['title'];?>"  data-src="<?php echo $banner['image']; ?>" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 2'%3E%3C/svg%3E" class="owl-lazy"/>
          </a>
        <?php } else { ?>
          <img alt="<?php echo $banner['title'];?>"  data-src="<?php echo $banner['image']; ?>" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 2'%3E%3C/svg%3E" class="owl-lazy" />
        <?php } ?>
      </div>
    <?php } ?>
    </div>
  </div>
