<nav class="navbar navbar-expand-md navbarNavaccount p-0">
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNavaccount" aria-controls="navbarNavaccount" aria-expanded="false" aria-label="Toggle navigation">
    <span>Меню</span>
    <span class="acc_arrow"><i class="fas fa-chevron-up"></i></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavaccount">
    <div class="navbar-nav w-100">
      <?php foreach ($menu as $group) { ?>
        <div class="list-group <?php echo $group['class']; ?>">
          <?php foreach ($group['links'] as $link) { ?>
            <a href="<?php echo $link['link']; ?>" class="list-group-item <?php echo $link['class']; ?>"><?php echo $link['text']; ?></a>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
  </div>
</nav>

<?php if ($logged) { ?>
<a href="<?php echo $logout; ?>" class="logout"><?php echo $text_logout; ?></a>
<?php } ?>

<div <?php if( $customer_group_id !=3) { ?> style="display: none" <?php } ?>  class="account__profile">
  <div class="account__profile-top">
    <div class="account__profile-image">
      <img src="/image/success.png" width="50" height="50" alt="image">
    </div>
    <div class="account__profile-info">
      <div class="account__profile-row">
        <div class="account__profile-title"><?=$text_my_account_group?>:</div>
        <div class="account__profile-text"><?=$group_name?></div>
      </div>
      <div class="account__profile-row">
        <div class="account__profile-title"><?=$text_my_account_sale?>:</div>
        <div class="account__profile-text"><?=$my_sale?>%</div>
      </div>

      <div class="account__profile-row">
        <div class="account__profile-title"><?=$text_my_account_order_old?></div>
        <div class="account__profile-text"><?=$old_price?></div>
      </div>

    </div>
  </div>
  <div class="account__profile-bottom">
    <div class="account__profile-ico">
      <img src="/image/honor.png" width="18" height="18" alt="image">
    </div>
    <div class="account__profile-bonus"><?=$text_my_account_bonus?>:</div>
    <div class="account__profile-bonus-sum"><?=$balance?></div>
  </div>
</div>

<style>
  .account__profile-top {
    background: #485860;
    border-radius: 6px 6px 0 0;
    padding: 18px 20px 16px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
  }
  .account__profile-image {
    margin-right: 10px;
  }
  .account__profile-row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }
  .account__profile-title {
    font-size: 13px;
    line-height: 16px;
    color: rgba(255, 255, 255, 0.7);
    margin-right: 9px;
  }
  .account__profile-text {
    font-weight: bold;
    font-size: 16px;
    line-height: 18px;
    color: #fff;
  }
  .account__profile-row + .account__profile-row {
    margin-top: 10px;
  }
  .account__profile-bottom {
    background: -webkit-gradient(linear, left top, right top, from(#126d07), to(#eace7e));
    background: linear-gradient(
            90deg
            , #227d07 0%, #eace7e 100%);
    border-radius: 0 0 6px 6px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 10px 23px;
  }
  .account__profile-ico {
    margin-right: 8px;
    height: 18px;
  }
  .account__profile-ico img {
    vertical-align: top;
  }
  .account__profile-bonus {
    font-size: 13px;
    line-height: 18px;
    color: #fff;
    margin-right: 11px;
  }
  .account__profile-bonus-sum {
    font-size: 16px;
    line-height: 18px;
    color: #fff;
    font-weight: bold;
  }
</style>
