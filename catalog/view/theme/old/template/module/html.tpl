<div>
  <?php if ($heading_title) { ?>
  <div class="h1">
    <span style="margin-top:25px;"><span id="hh2" style="display: block; font-size: 1.5em; margin-block-start: 0.83em; margin-block-end: 0.83em; margin-inline-start: 0px; margin-inline-end: 0px; font-weight: 500; line-height: 1.2; color: inherit; margin: 15px 0; margin-top: 0; font-size: 70%; text-align: center; font-family: 'AGLettericaCondensed'; margin-bottom: 0;"><?php echo $heading_title; ?></span>
  </div>
  <?php } ?>
  <?php echo $html; ?>
</div>
