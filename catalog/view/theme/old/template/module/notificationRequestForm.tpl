<?php if ($form) { ?>
  <div class="form_stock">
    <?php foreach ($form['fields'] as $field_name => $field) { ?>
      <div class="d-flex flex-row justify-content-between flex-wrap">
        <?php if ($field['is_group']) { ?>
          <?php foreach ($field['fields'] as $field_group_name => $field_group) { ?>
            <?php if ($field_group['is_group']) { ?>
              <div class="form-group">
                <label for="<?php echo $field_group_name; ?>" class="for_input"><?php echo $field_group['label']; ?></label>
                <div class="input-group input-date" id="<?php echo $field_group_name; ?>">
                  <?php foreach ($field_group['fields'] as $sub_field_name => $sub_field) { ?>
                    <input type="<?php echo $sub_field['type']; ?>" name="<?php echo $sub_field_name; ?>" class="form-control"
                    value="<?php echo $sub_field['value']; ?>" placeholder=""
                    <?php if ($sub_field['dataset']) { echo $sub_field['dataset']; } ?>
                    <?php if ($sub_field['event_listeners']) { echo $sub_field['event_listeners']; } ?>>
                  <?php } ?>
                </div>
                <?php if ($field_group['error']) { ?>
                  <label class="error" for="<?php echo $field_group_name; ?>">&#10008;
                    <?php echo $field_group['error']; ?>
                  </label>
                <?php } ?>
              </div>
            <?php } ?>
          <?php } ?>
        <?php } else { ?>
          <div class="row form-field">
            <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label']; ?></label>
            <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control"
            value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="">
            <?php if ($field['error']) { ?>
              <label class="error" for="<?php echo $field_name; ?>">&#10008;
                <?php echo $field['error']; ?>
              </label>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    <?php } ?>
    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
    <button type="submit" alt="<?php echo $button_submit_entrance; ?>" title="" rel="" onclick="sendNotificationRequest(event)" class="green_button cpt_product_add2cart_button">
      <?php echo $button_submit_entrance; ?>
    </button>
  </div>
<?php } ?>
<?php if ($message) { ?>
  <div class="message message-<?php echo $message['type']; ?>">
    <span class="symbol-<?php echo $message['type']; ?>">
      <?php if ($message['type'] === 'success') {
        echo '&#10003;';
      } else {
        echo '&#9888;';
      } ?>
    </span>
    <?php echo $message['text']; ?>
  </div>
<?php } ?>
