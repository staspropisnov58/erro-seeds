<?php if (isset($order_id) && isset($products)) { ?>
<script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  dataLayer.push({
   'ecommerce': {
     'currencyCode': 'UAH',
     'purchase': {
       'actionField': {
         'id': '<?=$order_id?>',
         'affiliation': '<?=$store_url?>',
         'revenue': '<?=$total?>',
         'tax': '',
         'shipping': '<?=$shipping?>',
         'coupon': '<?=$coupon?>'
       },
       'products': [
         <?php foreach ($products as $product) { ?>
           {'name': '<?=$product['name']?>',
           'id': '<?=$product['product_id']?>',
           'price': '<?=$product['price']?>',
           'brand': '<?php echo (isset($product['manufacturer'])?$product['manufacturer']:""); ?>',
           'category': '<?=$product['category']?>',
           'variant': '<?=$product['option']?>',
           'quantity': <?=$product['quantity']?>,
           'coupon': '<?=$coupon?>'},
          <?php } ?>
        ]
     }
   },
   'event': 'gtm-ee-event',
   'gtm-ee-event-category': 'Enhanced Ecommerce',
   'gtm-ee-event-action': 'Purchase',
   'gtm-ee-event-non-interaction': 'False',
  });
</script>
<?php } ?>
