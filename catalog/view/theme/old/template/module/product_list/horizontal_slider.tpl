<?php if ($products) { ?>
<div class="produckt-slider">
    <div class="container">
        <div class="name-prod-name">
            <?php if ($href) { ?>
            <a href="<?php echo $href; ?>"><span><?php echo $heading_title; ?></span></a>
            <?php } else { ?>
            <span><?php echo $heading_title; ?></span>
            <?php } ?>
        </div>
        <div class="related_slider<?php if(isset($setting_prod)){ echo '_new_slide'; } ?> owl-carousel">
            <?php foreach ($products as $product) { ?>
            <div class="product-wrap">
                <div class="product_img">
                    <a href="<?php echo $product['href']; ?>">
                        <?php if ($product['stickers'] && !isset($setting_prod)) { ?>
                        <div class="stickers-block">
                            <?php foreach ($product['stickers'] as $sticker){ ?>
                            <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
                            <?php } ?>
                        </div>
                        <?php } ?>
                        <img data-src="<?= $product['thumb']; ?>" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 3'%3E%3C/svg%3E" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-fluid owl-lazy">
                    </a>
                </div>

                <a href="<?php echo $product['href'] ?>" class="product_name"><?php echo $product['name']; ?> </a>
                <?php if(!isset($setting_prod)){ ?>
                <p class="product_price"><?php echo $product['price']; ?></p>
                <?php } else { ?>
                <style>
                    form#product_form{
                        padding-bottom: 30px;
                    }
                </style>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>

<script>
    $(document).ready(function() {

        $('.related_slider_new_slide').owlCarousel({
            loop: false,
            nav: true,
            lazyLoad: true,
            singleItem: true,
            pagination: false,
            dots: false,
            margin: 10,
            scrollPerPage: false,
            autoPlay: false,
            responsive:{
                0:{items:2},
                420:{items:3},
                900:{items:3},
                1000:{items:3},
                1200:{items:3}
            },
            navText: ['<a href="javascript:void(0);" class="prev" onclick><img src="catalog/view/theme/old/images/arrow-next2.png" width="31" height="85" alt="Arrow Prev"></a>',
                '<a href="javascript:void(0);" class="next"><img src="catalog/view/theme/old/images/arrow-prev2.png" width="31" height="85" alt="Arrow Next"></a>'
            ],
        });
    })
</script>
<style>
    .related_slider_new_slide.owl-carousel {

        /*width: 70%;*/
        /*margin: 0 auto;*/
        max-width: 350px;
    }
    .related_slider_new_slide .product_img
    {

        /*min-height: 150px;*/
    }
    #product .product_center .specific_block .related_slider_new_slide .product_img img

    {
        width: 100%;
        /*min-height: 150px;*/
    }
    .produckt-slider .related_slider_new_slide .owl-nav .owl-next, .produckt-slider .related_slider_new_slide .owl-nav .owl-prev
    {
        transform: rotate(0deg);
    }
    .produckt-slider .related_slider_new_slide .owl-nav .owl-next
    {
right: -23px;
    }
    .produckt-slider .related_slider_new_slide .owl-nav .owl-prev
    {
        left: -23px;
    }
    .produckt-slider .related_slider_new_slide .owl-nav
    {
        top:7%;
    }

</style>
