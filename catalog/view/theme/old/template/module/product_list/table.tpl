<div class="cpt_product_lists 2">
  <div class="name-prod-name">
    <?php if ($href) { ?>
      <!-- <a href="<?php // echo $href; ?>"><div class="h1" style="margin-top:25px;"><h2><?php // echo $heading_title; ?></h2></div></a> -->
      <a href="<?php echo $href; ?>"><div class="h1" style="margin-top:25px;"><span id="hh2" style="display: block; font-size: 1.5em; margin-block-start: 0.83em; margin-block-end: 0.83em; margin-inline-start: 0px; margin-inline-end: 0px; font-weight: 500; line-height: 1.2; color: inherit; margin: 15px 0; margin-top: 0; font-size: 70%; text-align: center; font-family: 'AGLettericaCondensed'; margin-bottom: 0;"><?php echo $heading_title; ?></span></div></a>

    <?php } else { ?>
      <span><?php echo $heading_title; ?></span>
    <?php } ?>
  </div>
  <div class="product_home">
    <ul>
      <?php foreach($products as $key => $product){ ?>
        <?php if ($key <= 19) {?>
        <li>
          <div class="product_img">
            <!-- <div class="sale_icon"></div> -->
            <div class="stickers-block">
              <?php foreach ($product['stickers'] as $sticker){ ?>
                <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
              <?php } ?>
            </div>
            <div class="openonhover">
                <?php if($product['attributes']) { ?>
                  <div class="charecteristics">
                    <?php foreach($product['attributes'] as $attribute) { ?>
                      <?php if($attribute['svg']){ ?>
                        <div class="characteristic__item">
                          <span> <?php echo $attribute["svg"]; ?></span>
                          <span><?php echo $attribute["text"]; ?></span>
                        </div>
              <?php }else{ ?>
                      <div class="characteristic__item"><span class="icon" style="background-image: url('/image/<?php echo $attribute['icon']; ?>');"></span><span class="text"><?php echo $attribute['text']; ?></span></div>
                    <?php } ?>
                  <?php } ?>
                  </div>
                <?php } ?>
              </div>
            <a href=" <?php echo $product['href']; ?>">
              <img width="auto" height="auto"
                   data-srcset="<?php echo $product['thumb']; ?>"
                   src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 2'%3E%3C/svg%3E"
                   alt = "<?php echo $product['alt_image'];?>"
                   title = "<?php echo $product['title_image']; ?>"
                   class="lazyload"
              />
            </a>
          </div>
          <a href="<?php echo $product['href'] ?>" class="product_name"><?php echo $product['name']; ?> </a>

<div style="" class="option_prev_grid pr_<?php echo $product['product_id']; ?>">
    <?php if ($product['options2']) { ?>

    <?php foreach ($product['options2'] as $option_n) { ?>
    <p class="thc_content mt-1 mb-1">
        <span><?=$option_n['name']?>:</span>
    </p>
    <div class="option_value">
        <ul>
            <?php $i=1; ?>
            <?php foreach ($option_n['product_option_value'] as $option_value) { ?>
            <li style="min-width: 28%" data-id="<?php echo $product['product_id']; ?>" <?php if($i==1) { ?> class="active" <?php } ?> ><input
                    data-price="<?=$option_value['price']?>"
                    data-price_old="<?=$option_value['old_price']?>"
                    data-price_dev="<?=$option_value['price_dev']?>"
                    data-option="<?php echo $option_value['name']; ?>"
                    type="radio"
                    name="option[<?php echo $option_n['product_option_id']; ?>]"
                    value="<?php echo $option_value['product_option_value_id']; ?>"
                     />
            <?php echo $option_value['name']; ?>
            </li>
            <?php $i++; } ?>
        </ul>
    </div>
    <?php } ?>
    <?php } ?>
</div>


<div class="prdbrief_price">
    <!-- Класс переименован для того чтобы отключить js -->
    <div class="_totalPrice d-flex st_style price_option_<?php echo $product['product_id']; ?>">
        <?php if ($product['special']){ ?>
        <span class="old_price"><?php echo $product['price']; ?></span>
        <span class="new_price"><?php echo $product['special']; ?></span>
        <?php } else { ?>
        <span class="new_price"><?php echo $product['price']; ?></span>
        <?php } ?>
    </div>
</div>
        </li>
      <?php }else{} ?>
      <?php } ?>
    </ul>
  </div>
</div>

<script>
    $(document).on('click','.option_prev_grid .option_value li',function (){
        var li  = $(this)
        var id = li.data('id');
        $('.pr_'+id+' li').removeClass('active')
        li.addClass('active')



        var price = li.find('input').data('price')
        var price_old = li.find('input').data('price_old')
        var price_dev = li.find('input').data('price_dev')
        if($('.price_option_'+id+' span').hasClass( "old_price" )){
            var price_ic = price.split(" ");
            if(price_dev>0){price_old=Number(price_dev).toFixed(2)+' '+price_ic[1];}

                $('.price_option_'+id+' .new_price').text(price);
            $('.price_option_'+id+' .old_price ').text(price_old)
        }else{
            $('.price_option_'+id+' .new_price').text(price)
        }


    })
</script>

