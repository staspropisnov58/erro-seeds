<div class="Vwidget custom-container vertical">
  <div class="VjCarouselLite1">
    <div class="cpt_product_lists">
      <div class="carousel">
        <ul class="product-list owl-carousel">
          <?php foreach($products as $product){ ?>
          <li>
            <p>
              <a href="<?php echo $product['href']; ?>" >
                <?echo $product['name']; ?>
              </a>
            </p>
            <div class="slide_product_border">
              <!-- <div class="popular_icon">
              </div> -->
              <div class="stickers-block">
                <?php foreach ($product['stickers'] as $sticker){ ?>
                  <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
                <?php } ?>
              </div>
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt = "<?php echo $product['alt_image']; ?>" title = "<?php echo $product['title_image']; ?>"/>
              </a>
            </div>
          </li>
        <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
