<div class="d-flex justify-content-center mt-5 opt_form_block">
  <div class="contact_us_new">
    <div class="contact_form">
      <p class="title"><?php echo $title; ?></p>
      <form class="" action="" method="post">
        <div class="form-group d-flex flex-column">
          <label for="firstname" class="white_label"><?php echo $entry_name; ?></label>
          <input type="text" name="firstname" class="form-control trans-input form-control-success" value="">
        </div>
        <div class="form-group d-flex flex-column">
          <label for="telephone" class="white_label"><?php echo $entry_telephone; ?></label>
          <input type="text" name="telephone" class="form-control trans-input form-control-success" value="">
        </div>
        <div class="form-group d-flex flex-column">
          <label for="comment" class="white_label"><?php echo $entry_message; ?></label>
          <textarea id="comment" name="comment" class="form-control trans-input form-control-success"></textarea>
        </div>
        <button type="submit" class="green_button" id=""><?php echo $button_submit_entrance; ?></button>
      </form>
    </div>
  </div>
</div>
