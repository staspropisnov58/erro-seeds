<div class="container-fluid" style="background:#363636;margin-top: 20px;padding: 15px 0;">
  <div class="container">
    <div class="d-flex col-12 m-auto col-lg-9 p-0 col-12 flex-column flex-md-row">
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> Новые и популярные</div>
        <ul class="sf-menu">
          <li> <a href="/202/">Ура! Errors Seeds исполнилось 9 лет! </a> <a href="/198/">Импортное обновление ассортимента </a> <a href="/197/">Праздник 4:20 с Errors Seeds </a> <a href="/196/">Скоро праздник! Не пропусти! </a> <a href="/195/">Обновление
              ассортимента к новому аутдорному сезону 2018 </a> <a href="/194/"> Стартует предзаказ на 8-й выпуск журнала JahPub! </a> <a href="/193/">Заказал Green House — получил подарок! </a> <a href="/192/">Абсолютно легально! </a></li>
        </ul>
      </div>
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> Помощь</div>
        <ul class="sf-menu">
          <li> <a href="/auxpage_faq/"> Часто задаваемые вопросы </a></li>
          <li> <a href="/feedback/">Контакты</a></li>
          <li> <a href="/auxpage_delivery/"> Доставка </a></li>
          <li> <a href="/auxpage_shipping/"> Оплата </a></li>
        </ul>
      </div>
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> Семена</div>
        <ul class="sf-menu">
          <li><a href="/category/errors-seeds/">Errors-seeds</a></li>
          <li><a href="/category/errors-seeds-import/">Errors-seeds импорт</a></li>
          <li><a href="/category/carpathians-seeds/">Carpathians seeds</a></li>
          <li><a href="/category/drugie/green-house-seeds/">Green House Seeds</a></li>
          <li><a href="/category/semena-iz-gollandii/vision-seeds/">Vision Seeds</a></li>
          <li><a href="/category/drugie/shortstuff-seeds/">Shortstuff Seeds</a></li>
          <li><a href="/category/sweet-seeds/">Sweet Seeds</a></li>
          <li><a href="/category/semena-iz-gollandii/dutch-passion-seeds/">Dutch Passion seeds</a></li>
          <li><a href="/category/drugie/flash-seeds/">Flash-seeds</a></li>
          <li><a href="/category/drugie/grass-o-matic-seeds/">Grass-o-matic seeds</a></li>
          <li><a href="/category/drugie/joint-doctor-seeds/">Joint Doctor seeds</a></li>
          <li><a href="/category/drugie/seedsman/">Seedsman</a></li>
        </ul>
      </div>
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> О магазине</div>
        <ul class="sf-menu">
          <li> <a href="/auxpage_about/"> О нас </a></li>
          <li> <a href="/auxpage_schedule/"> График работы </a></li>
          <li> <a href="/blog/"> Блог </a></li>
          <li> <a href="/sitemap/"> Карта сайта </a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
