<div class="footer_top">
  <div class="container">
    <div class="d-flex flex-wrap mb-lg-4">
      <div class="footer_sub col-12 col-md-5">
        <div class="f_sub_head f_title">About Our Store <i class="fas fa-chevron-down"></i></div>
        <div class="f_sub_cont ">
          <ul class="sf-menu">
            <li>
              <li> <a href="/en/blog/">Our Blog</a></li>
              <li> <a href="/en/auxpage_about/"> About Us </a></li>
              <li> <a href="/en/feedback/"> Contacts </a></li>
              <li> <a href="/en/auxpage_shipping/"> Payment </a></li>
              <li> <a href="/en/auxpage_delivery/"> Delivery </a></li>
              <li> <a href="/en/sitemap/"> SiteMap </a></li>
              <li> <a href="/en/auxpage_polit-konf/"> Privacy Policy </a></li>
            </li>
          </ul>
        </div>
      </div>
      <div class="footer_sub col-12 col-md-4">
        <div class="f_sub_head f_title"> Seeds <i class="fas fa-chevron-down"></i></div>
        <div class="f_sub_cont">
          <ul class="sf-menu">
            <li><a href="/en/category/errors-seeds/">Errors Seeds Silver</a></li>
            <li><a href="/en/category/errors-seeds-import/">Errors Seeds Gold</a></li>
            <li><a href="/en/category/carpathians-seeds/">Carpathians Seeds</a></li>
            <li><a href="/en/category/drugie/green-house-seeds/">Green House Seeds</a></li>
            <li><a href="/en/category/drugie/dinafem/">Dinafem</a></li>
            <li><a href="/en/category/sweet-seeds/">Sweet Seeds</a></li>
            <li><a href="/en/category/barneys-farm/">Barneys Farm</a></li>
          </ul>
        </div>
      </div>
      <div class="footer_sub col-12 col-md-3">
        <div class="payment mb-3"><img src="/img/payment.webp"></div>
        <ul class="sf-menu">
          <li>
            <a rel="http://growmarket.com.ua/catalog/osveschenie" href="#" target="_blank" class="hidli">
              Lamps
            </a>
          </li>
          <li>
            <a rel="http://medcannabis.info/" href="#" target="_blank" class="hidli">
              Medicine</a>
          </li>
          <li>
            <a rel="https://jahforum.net/" href="#" target="_blank" class="hidli">
              Forum</a>
          </li>
        </ul>
        <button type="button" name="director" class="director_btn">Contact Director</button>
      </div>
    </div>
  </div>
</div>
