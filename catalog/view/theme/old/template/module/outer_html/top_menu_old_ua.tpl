<div class="menu">
  <ul class="header_menu">
    <li>
      <p class="js-dropmenu_btn dropmenu_btn btn_menu_green">
        <span><span class="icon"></span>Каталог товарів</span>
        <i class="fas fa-chevron-right"></i>
      </p>
      <div class="js-dropmenu dropmenu">
        <ul>
          <li class="prev">
            <i class="fas fa-chevron-left"></i>
          </li>
          <li>
            <p><font color="#65bd00">Насіння</font> <i class="fas fa-chevron-right"></i></p>
            <ul class="line">
              <li class="prev">
                <i class="fas fa-chevron-left"></i>
              </li>
              <li>
                <p>
                  <a href="/ua/category/errors-seeds/">Errors Seeds Silver</a>
                  <i class="fas fa-chevron-right"></i>
                </p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li><a href="/ua/category/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
                  <li><a href="/ua/category/errors-seeds-auto-fem/">Errors Seeds Silver Auto Feminised</a></li>
                  <li><a href="/ua/category/errors-seeds/errors-seeds-auto-fem/">Errors Seeds Silver Auto Feminised</a></li>
                  <li><a href="/ua/category/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
                  <li><a href="/ua/category/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
                  <li><a href="/ua/category/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
                </ul>
              </li>
              <li>
                <p>
                  <a href="/ua/category/errors-seeds-import/">Errors Seeds Gold</a>
                  <i class="fas fa-chevron-right"></i>
                </p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li><a href="/ua/category/errors-seeds-import-feminised/">Errors Seeds Gold Feminised</a></li>
                  <li><a href="/ua/category/errors-seeds-import-auto-fem/">Errors Seeds Gold Auto Fem</a></li>
                </ul>
              </li>
              <li>
                <a href="/ua/category/komplekty-es/">Комплекти Errors Seeds</a>
              </li>
              <li>
                <a href="/ua/category/cbd-sorta/">CBD сорти</a>
              </li>
              <li>
                <p>Іспанія <i class="fas fa-chevron-right"></i></p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/victory-seeds/">Victory Seeds</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/victory-seeds/victory-seeds-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/victory-seeds/victory-seeds-fem/">Feminised</a></li>
                    </ul>
                  </li>
                  <li>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/sweet-seeds/sweet-seeds-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/sweet-seeds/sweet-seeds-feminised/">Feminised</a></li>
                    </ul>
                  </li>
                  <li><a href="/ua/category/mandala-seeds-/">Mandala Seeds</a> </li>
                  <li>
                    <p>
                      <a href="/ua/category/kannabia/">Kannabia</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/kannabia/auto-kannabia/">Auto Feminised</a></li>
                      <li><a href="/ua/category/kannabia/feminised-kannabia/">Feminised</a></li>
                      <li><a href="/ua/category/kannabia/cbd-kannabia/">CBD</a></li>
                    </ul>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/pyramid-seeds/">Pyramid Seeds</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/pyramid-seeds/auto-ps/">Auto Feminised</a></li>
                      <li><a href="/ua/category/pyramid-seeds/fem-ps/">Feminised</a></li>
                      <li><a href="/ua/category/pyramid-seeds/cbd-ps/">CBD</a></li>
                    </ul>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/drugie/dinafem/">Dinafem</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/drugie/dinafem/auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/drugie/dinafem/foto/">Feminised</a></li>
                      <li><a href="/ua/category/drugie/dinafem/cbd-auto/">Auto CBD Feminised</a></li>
                      <li><a href="/ua/category/drugie/dinafem/cbd-foto/">CBD Feminised</a></li>
                    </ul>
                  </li>
                  <li><a href="/ua/category/buddha-seeds/">Buddha Seeds</a> </li>
                </ul>
              </li>
              <li>
                <p>Англія <i class="fas fa-chevron-right"></i></p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/bulk-seed-bank/">Bulk Seed Bank</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/bulk-seed-bank/bulk-seed-bank-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/bulk-seed-bank/bulk-seed-bank-fem/">Feminised</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <p>
                  <a href="/ua/category/semena-iz-gollandii/">Голандія</a>
                  <i class="fas fa-chevron-right"></i>
                </p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/dutch-passion-seeds/">Dutch Passion</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/dutch-passion-seeds-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/dutch-passion-seeds-foto-seeds/">Feminised</a></li>
                      <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/auto-cbd-seeds/">Auto CBD Feminised</a></li>
                      <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/foto-cbd-seeds/">CBD Feminised</a></li>
                    </ul>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/green-house-seeds/">Green House Seeds</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/drugie/green-house-seeds/green-house-seeds-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/drugie/green-house-seeds/green-house-seeds-feminised/">Feminised</a></li>
                      <li><a href="/ua/category/drugie/green-house-seeds/green-house-seeds-cbd/">Auto CBD Feminised</a></li>
                    </ul>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/vision-seeds/">Vision Seeds</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/vision-seeds-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/vision-seeds-feminised/">Feminised</a></li>
                      <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/cbd/">CBD</a></li>
                    </ul>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/neuroseeds/">Neuroseeds</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/neuroseeds/neuroseeds-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/neuroseeds/neuroseeds-fem/">Feminised</a></li>
                    </ul>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/barneys-farm/">Barneys Farm</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/semena-iz-gollandii/barneys-farm/auto-fem-bf/">Auto Feminised</a></li>
                      <li><a href="/ua/category/semena-iz-gollandii/barneys-farm/foto-fem-bf/">Feminised</a></li>
                      <li><a href="/ua/category/semena-iz-gollandii/barneys-farm/cbd-bf/">CBD Feminised</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <p>США <i class="fas fa-chevron-right"></i></p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
				  <li><a href="/ua/category/420fastbuds/">Fast Buds</a> </li>
                  <li>
                    <p>
                      <a href="/ua/category/humboldt/">Humboldt</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/humboldt/humbolt-auto-fem/">Auto Feminised</a></li>
                      <li><a href="/ua/category/humboldt/humboldt-fem/">Feminised</a></li>
                      <li><a href="/ua/category/humboldt/cbd-seeds/">CBD Feminised</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <p>Чехія <i class="fas fa-chevron-right"></i></p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li>
                    <p>
                      <a href="/ua/category/carpathians-seeds/">Carpathians seeds</a>
                      <i class="fas fa-chevron-right"></i>
                    </p>
                    <ul class="line">
                      <li class="prev">
                        <i class="fas fa-chevron-left"></i>
                      </li>
                      <li><a href="/ua/category/carpathians-seeds/carpathians-seeds-auto-fem/">Auto Feminised</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            <p>Обладнання <i class="fas fa-chevron-right"></i></p>
            <ul class="line">
              <li class="prev">
                <i class="fas fa-chevron-left"></i>
              </li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/">Все обладнання</a></li>
			   <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/groyboksu/">PRO100 GrowBox by Errors Seeds</a></li>
			   <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/grouboksy/">Гроубокси</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/stolu-gorshki-potdonu/">Столи, горщики, піддони</a></li>
			  <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/ventilyaciya/">Вентиляція</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/izmeritelnaya-tehnika/">Вимірювальна техніка</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/hidroponnue-sistemu/">Гідропонні системи</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/osveshenie/">Освітлення</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/upravleniye-i-avtomatika/">Управління та автоматика</a></li>
            </ul>
          </li>
          <li>
            <p>Добрива <i class="fas fa-chevron-right"></i></p>
            <ul class="line">
              <li class="prev">
                <i class="fas fa-chevron-left"></i>
              </li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/">Всі добрива</a></li>
              <li>
                <p>
                  <a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/well-grow-by-errors-seeds/">Well Grow by Errors Seeds</a>
                  <i class="fas fa-chevron-right"></i>
                </p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/well-grow-by-errors-seeds/e-serija/">E-серія</a></li>
                  <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/well-grow-by-errors-seeds/s-serija/">S-серія</a></li>
				  <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/well-grow-by-errors-seeds/x-serija/">X-серія</a></li>
                </ul>
              </li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/biostimulyatoru/">БІОСтимулятори</a></li>
            </ul>
			<li>
			<p><a href="/ua/category/suvenirnaja-produkcija/">Сувенірна продукція</a></p>
			</li>
          </li>
        </ul>
      </div>
    </li>
    <li>
      <p class="js-dropmenu_btn dropmenu_btn old_style">Про магазин <i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i></p>
      <div class="js-dropmenu dropmenu">
        <ul>
          <li class="prev">
            <i class="fas fa-chevron-left"></i>
          </li>
          <li><a href="/ua/auxpage_about/">Про Нас</a></li>
          <li><a href="/ua/feedback/">Контакти</a></li>
          <li><a href="/ua/auxpage_kak-kupit-semena-konopli/">Як купити насiння коноплi</a></li>
          <li><a href="/ua/auxpage_faq/">Часті питання</a></li>
          <li><a href="/ua/auxpage_shipping/">Оплата</a></li>
          <li><a href="/ua/auxpage_delivery/">Доставка</a></li>
          <li><a href="/ua/auxpage_schedule/">Графік роботи</a></li>
          <li><a href="/ua/reports/">Відгуки</a></li>
		  <li><a href="/auxpage_public-offer-agreement/">Договір публічної оферти</a></li>
          <li><a href="/ua/auxpage_polit/"><font color="#65bd00">Полiтика замiни та повернення</font></a></li>
			<li>
			<p><a href="/ua/category/suvenirnaja-produkcija/">Сувенірна продукція</a></p>
			</li>
          </li>
        </ul>
      </div>
    </li>
    <li><a href="/ua/auxpage_action_list"><span style="color: red; font-weight: bold;">SALE</span></a></li>
	<li><a href="/ua/185/"><span style="color: #65bd00; font-weight: bold;">Бонусна програма</span></a></li>
	<li><a href="/ua/auxpage_law/"><span style="color: #65bd00; font-weight: bold;">Закон</span></a></li>
    <li><a rel="nofollow" href="/ua/blog/">Новини</a></li>
    <li>
      <p class="js-dropmenu_btn dropmenu_btn old_style">Співробітництво <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-down"></i></p>
      <div class="js-dropmenu dropmenu">
        <ul>
          <li class="prev">
            <i class="fas fa-chevron-left"></i>
          </li>
          <li><a href="/ua/auxpage_sotrudnichestvo/">О співробітництві</a></li>
          <li><a href="/ua/auxpage_referalnaya_programma/">Реферальна програма</a></li>
          <li><a href="/ua/auxpage_dropshipping/">Дропшиппінг</a></li>
		  <li><a href="/ua/auxpage_opt/"><font color="#65bd00">Оптові продажі</font></a></li>
          <li><a href="/ua/auxpage_interesnyy_kontent/">Цікавий контент</a></li>
          <li><a href="/ua/auxpage_smm/">SMM </a></li>
          <li><a href="/ua/auxpage_distribjucija/">Дистриб'юція</a></li>
          <li><a href="/ua/auxpage_vakansii/">Вакансії</a></li>
        </ul>
      </div>
    </li>
  </ul>
</div>
