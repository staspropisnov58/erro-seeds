<div class="menu desktop_menu order-5">
  <ul class="sf-menu header_menu d-flex flex-md-row flex-column">
    <li class="current"> <a href="/"> Главная </a></li>
    <li class="current"> <a href="#">О магазине</a>
      <ul>
        <li> <a href="/auxpage_about/"> О Нас </a></li>
		<li> <a rel="nofollow" href="/blog/"> Все новости </a></li>
        <li> <a href="/auxpage_faq/"> Часто задаваемые вопросы </a></li>
        <li> <a href="/auxpage_schedule/"> График работы </a></li>
        <li> <a href="/auxpage_delivery/"> Доставка </a></li>
        <li> <a href="/auxpage_shipping/"> Оплата </a></li>
        <li> <a href="/auxpage_adress/"> Как нас найти </a></li>
        <li> <a href="/auxpage_kak-kupit-semena-konopli/"> Как купить семена конопли </a></li>
        <li> <a href="/feedback/"> Контакты </a></li>
        <li>
          <a href="/auxpage_nashy-distribjutory/">Наши дистрибьюторы<span class="arrow">►</span></a>
          <ul>
            <li><a href="/auxpage_master-seed/">Master Seed</a></li>
            <li><a href="/auxpage_seed-bank-shop/">Seed Bank</a></li>
            <li><a href="/auxpage_good-seed/">Good Seed</a></li>
            <li><a href="/auxpage_ligalaiz-groop/">Ligalaiz Groop</a></li>
            <li><a href="/auxpage_sunny-seeds/">Sunny Seeds</a></li>
            <li><a href="/auxpage_tengo-seed/">Tengo Seed</a></li>
          </ul>
        </li>
        <li> <a href="/auxpage_polit/"> Политика замены и возврата<span class="arrow">►</span></a>
          <ul>
            <li> <a href="/auxpage_indiv/"> Для частных лиц </a></li>
            <li> <a href="/auxpage_for-shop/"> Для магазинов </a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li> <a href="#"> Семена </a>
      <ul>
        <li> <a href="/category/errors-seeds/">Errors Seeds Silver<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/errors-seeds/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
            <li><a href="/category/errors-seeds/errors-seeds-auto-fem/">Errors Seeds Silver Auto Fem</a></li>
            <li><a href="/category/errors-seeds/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
			<li><a href="/category/errors-seeds/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
			<li><a href="/category/errors-seeds/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
          </ul>
        </li>
        <li> <a href="/category/errors-seeds-import/">Errors Seeds Gold<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/errors-seeds-import/errors-seeds-import-feminised/">Errors Seeds Gold Feminised</a></li>
            <li><a href="/category/errors-seeds-import/errors-seeds-import-auto-fem/">Errors Seeds Gold Auto Fem</a></li>
          </ul>
        </li>
		<li><a href="/category/komplekty-es/">Комплекты семян от Errors Seeds</a></li>
        <li> <a href="/category/carpathians-seeds/">Carpathians seeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/carpathians-seeds/carpathians-seeds-auto-fem/">Carpathians seeds Auto Fem</a></li>
          </ul>
        </li>
        <li> <a href="/category/neuroseeds/">Neuroseeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/neuroseeds/neuroseeds-auto-fem/">Neuroseeds auto fem</a></li>
            <li><a href="/category/neuroseeds/neuroseeds-fem/">Neuroseeds fem</a></li>
          </ul>
        </li>
        <li><a href="/category/cbd-sorta/">CBD сорта</a></li>
        <li> <a href="/category/victory-seeds/">Victory Seeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/victory-seeds/victory-seeds-auto-fem/">Victory Seeds Auto Fem</a></li>
            <li><a href="/category/victory-seeds/victory-seeds-fem/">Victory Seeds Fem</a></li>
          </ul>
        </li>
        <li> <a href="/category/bulk-seed-bank/">Bulk Seed Bank<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/bulk-seed-bank/bulk-seed-bank-auto-fem/">Bulk Seed Bank Auto Fem</a></li>
            <li><a href="/category/bulk-seed-bank/bulk-seed-bank-fem/">Bulk Seed Bank Fem</a></li>
          </ul>
        </li>
        <li> <a href="/category/drugie/green-house-seeds/">Green House Seeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/drugie/green-house-seeds/green-house-seeds-auto-fem/">Green House Seeds Auto Fem</a></li>
            <li><a href="/category/drugie/green-house-seeds/green-house-seeds-feminised/">Green House Seeds Feminised</a></li>
          </ul>
        </li>
		<li><a href="/category/buddha-seeds/">Buddha Seeds</a></li>
        <li><a href="/category/mandala-seeds-/">Mandala Seeds</a></li>
        <li><a href="/category/kannabia/">Kannabia</a></li>
        <li><a href="/category/pyramid-seeds/">Pyramid Seeds</a></li>
        <li><a href="/category/humboldt/">Humboldt</a></li>
        <li><a href="/category/gsr/">Green Silk Road Seeds</a>
        </li>
         <li><a href="/category/dinafem/">Dinafem</a>
        </li>
        <li>
          <a href="/category/semena-iz-gollandii/">Семена из Голландии<span class="arrow">►</span></a>
          <ul>
            <li><a href="/category/semena-iz-gollandii/barneys-farm/">Barneys Farm</a></li>
            <li> <a href="/category/semena-iz-gollandii/dutch-passion-seeds/">Dutch Passion seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/semena-iz-gollandii/dutch-passion-seeds/dutch-passion-seeds-auto-fem/">Dutch Passion Seeds Auto Fem</a></li>
              </ul>
            </li>
            <li> <a href="/category/semena-iz-gollandii/vision-seeds/">Vision Seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/semena-iz-gollandii/vision-seeds/vision-seeds-auto-fem/">Vision Seeds Auto Fem</a></li>
                <li><a href="/category/semena-iz-gollandii/vision-seeds/vision-seeds-feminised/">Vision Seeds Feminised</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li> <a href="/category/drugie/">Другие<span class="arrow">►</span></a>
          <ul>
            <li> <a href="/category/drugie/shortstuff-seeds/">Shortstuff Seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-fem/">Shortstuff Seeds fem</a></li>
                <li><a href="/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-reg/">Shortstuff Seeds reg</a></li>
              </ul>
            </li>
            <li> <a href="/category/sweet-seeds/">Sweet Seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/sweet-seeds/sweet-seeds-auto-fem/">Sweet Seeds Auto Fem</a></li>
                <li><a href="/category/sweet-seeds/sweet-seeds-feminised/">Sweet Seeds Feminised</a></li>
              </ul>
            </li>
            <li> <a href="/category/drugie/flash-seeds/">Flash-seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/drugie/flash-seeds/flash-seeds-auto-fem/">Flash-seeds Auto Fem</a></li>
                <li><a href="/category/drugie/flash-seeds/flash-seeds-regular/">Flash-seeds Regular</a></li>
              </ul>
            </li>
            <li> <a href="/category/drugie/grass-o-matic-seeds/">Grass-o-matic seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/drugie/grass-o-matic-seeds/grass-o-maitc-seeds-auto-fem/">Grass-o-matic seeds Auto Fem</a></li>
              </ul>
            </li>
            <li> <a href="/category/drugie/joint-doctor-seeds/">Joint Doctor seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/drugie/joint-doctor-seeds/joint-doctor-seeds-regular/">Joint Doctor seeds Regular</a></li>
                <li><a href="/category/drugie/joint-doctor-seeds/joint-doctor-seeds-auto-fem/">Joint Doctor seeds Auto Fem</a></li>
              </ul>
            </li>
            <li> <a href="/category/drugie/seedsman/">Seedsman<span class="arrow">►</span></a>
              <ul>
                <li><a href="/category/drugie/seedsman/seedsman-auto-fem/">Seedsman Auto Fem</a></li>
                <li><a href="/category/drugie/seedsman/seedsman-regular/">Seedsman Regular</a></li>
                <li><a href="/category/drugie/seedsman/seedsman-feminised/">Seedsman Feminised</a></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li> <a href="/auxpage_action_list"><span style="color: red; font-weight: bold;"> SALE</span> </a></li>
    <li> <a rel="nofollow" href="/blog/"> Все новости </a></li>
    <li> <a href="/auxpage_sotrudnichestvo/"> Сотрудничество</a>
      <ul>
        <li><a href="/auxpage_referalnaya_programma/">Реферальная программа</a></li>
        <li><a href="/auxpage_opt/">Опт</a></li>
        <li><a href="/auxpage_dropshipping/">Дропшиппинг</a></li>
        <li><a href="/auxpage_interesnyy_kontent/">Интересный контент</a></li>
        <li><a href="/auxpage_smm/">SMM </a></li>
        <li><a href="/auxpage_distribjucija/">Дистрибьюция</a></li>
        <li><a href="/auxpage_vakansii/">Вакансии</a></li>
      </ul>
    </li>
  </ul>
</div>
<div class="menu mobile_menu order-1 col-1">
  <div class="navBurger" role="navigation" id="navToggle"></div>
  <div class="overlay">
    <nav class="overlayMenu js-menu">
      <ul role="menu">
        <li> <a href="/auxpage_about/"> О Нас </a></li>
        <li> <a href="/auxpage_faq/"> Часто задаваемые вопросы </a></li>
        <li> <a href="/auxpage_schedule/"> График работы </a></li>
        <li> <a href="/auxpage_delivery/"> Доставка </a></li>
        <li> <a href="/auxpage_shipping/"> Оплата </a></li>
        <li> <a href="/auxpage_adress/"> Как нас найти </a></li>
        <li> <a href="/auxpage_kak-kupit-semena-konopli/"> Как купить семена конопли </a></li>
        <li> <a href="/feedback/"> Контакты </a></li>
        <li>
          <a href="/auxpage_nashy-distribjutory/">Наши дистрибьюторы</a>
          <ul>
            <li><a href="/auxpage_master-seed/">Master Seed</a></li>
            <li><a href="/auxpage_seed-bank-shop/">Seed Bank</a></li>
            <li><a href="/auxpage_good-seed/">Good Seed</a></li>
            <li><a href="/auxpage_ligalaiz-groop/">Ligalaiz Groop</a></li>
            <li><a href="/auxpage_sunny-seeds/">Sunny Seeds</a></li>
            <li><a href="/auxpage_tengo-seed/">Tengo Seed</a></li>
          </ul>
        </li>
        <li> <a href="/auxpage_polit/"> Политика замены и возврата </a>
          <ul>
            <li> <a href="/auxpage_indiv/"> Для частных лиц </a></li>
            <li> <a href="/auxpage_for-shop/"> Для магазинов </a></li>
          </ul>
        </li>
      </ul>
      </li>
      <li> <a href="#"> Семена </a>
        <ul>
          <li> <a href="/category/errors-seeds/">Errors Seeds Silver</a>
            <ul>
              <li><a href="/category/errors-seeds/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
              <li><a href="/category/errors-seeds/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
              <li><a href="/category/errors-seeds/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
              <li><a href="/category/errors-seeds/errors-seeds-auto-fem/">Errors Seeds Silver Auto Fem</a></li>
              <li><a href="/category/errors-seeds/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
            </ul>
          </li>
          <li> <a href="/category/errors-seeds-import/">Errors Seeds Gold</a>
            <ul>
              <li><a href="/category/errors-seeds-import/errors-seeds-import-feminised/">Errors Seeds Gold Feminised</a></li>
              <li><a href="/category/errors-seeds-import/errors-seeds-import-auto-fem/">Errors Seeds Gold Auto Fem</a></li>
            </ul>
          </li>
          <li> <a href="/category/carpathians-seeds/">Carpathians seeds</a>
            <ul>
              <li><a href="/category/carpathians-seeds/carpathians-seeds-auto-fem/">Carpathians seeds Auto Fem</a></li>
            </ul>
          </li>
          <li> <a href="/category/neuroseeds/">Neuroseeds</a>
            <ul>
              <li><a href="/category/neuroseeds/neuroseeds-auto-fem/">Neuroseeds auto fem</a></li>
              <li><a href="/category/neuroseeds/neuroseeds-fem/">Neuroseeds fem</a></li>
            </ul>
          </li>
          <li><a href="/category/cbd-sorta/">CBD сорта</a></li>
          <li> <a href="/category/victory-seeds/">Victory Seeds</a>
            <ul>
              <li><a href="/category/victory-seeds/victory-seeds-auto-fem/">Victory Seeds Auto Fem</a></li>
              <li><a href="/category/victory-seeds/victory-seeds-fem/">Victory Seeds Fem</a></li>
            </ul>
          </li>
          <li> <a href="/category/bulk-seed-bank/">Bulk Seed Bank</a>
            <ul>
              <li><a href="/category/bulk-seed-bank/bulk-seed-bank-auto-fem/">Bulk Seed Bank Auto Fem</a></li>
              <li><a href="/category/bulk-seed-bank/bulk-seed-bank-fem/">Bulk Seed Bank Fem</a></li>
            </ul>
          </li>
          <li> <a href="/category/drugie/green-house-seeds/">Green House Seeds</a>
            <ul>
              <li><a href="/category/drugie/green-house-seeds/green-house-seeds-auto-fem/">Green House Seeds Auto Fem</a></li>
              <li><a href="/category/drugie/green-house-seeds/green-house-seeds-feminised/">Green House Seeds Feminised</a></li>
            </ul>
          </li>
          <li><a href="/category/mandala-seeds-/">Mandala Seeds</a></li>
          <li><a href="/category/kannabia/">Kannabia</a></li>
          <li><a href="/category/pyramid-seeds/">Pyramid Seeds</a></li>
          <li><a href="/category/humboldt/">Humboldt</a></li>
          <li><a href="/category/gsr/">Green Silk Road Seeds</a>
          </li>
           <li><a href="/category/dinafem/">Dinafem</a>
          </li>
          <li>
            <a href="/category/semena-iz-gollandii/">Семена из Голландии</a>
            <ul>
              <li><a href="/category/semena-iz-gollandii/barneys-farm/">Barneys Farm</a></li>
              <li> <a href="/category/semena-iz-gollandii/dutch-passion-seeds/">Dutch Passion seeds</a>
                <ul>
                  <li><a href="/category/semena-iz-gollandii/dutch-passion-seeds/dutch-passion-seeds-auto-fem/">Dutch Passion Seeds Auto Fem</a></li>
                </ul>
              </li>
              <li> <a href="/category/semena-iz-gollandii/vision-seeds/">Vision Seeds</a>
                <ul>
                  <li><a href="/category/semena-iz-gollandii/vision-seeds/vision-seeds-auto-fem/">Vision Seeds Auto Fem</a></li>
                  <li><a href="/category/semena-iz-gollandii/vision-seeds/vision-seeds-feminised/">Vision Seeds Feminised</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li> <a href="/category/drugie/">Другие</a>
            <ul>
              <li> <a href="/category/drugie/shortstuff-seeds/">Shortstuff Seeds</a>
                <ul>
                  <li><a href="/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-fem/">Shortstuff Seeds fem</a></li>
                  <li><a href="/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-reg/">Shortstuff Seeds reg</a></li>
                </ul>
              </li>
              <li> <a href="/category/sweet-seeds/">Sweet Seeds</a>
                <ul>
                  <li><a href="/category/sweet-seeds/sweet-seeds-auto-fem/">Sweet Seeds Auto Fem</a></li>
                  <li><a href="/category/sweet-seeds/sweet-seeds-feminised/">Sweet Seeds Feminised</a></li>
                </ul>
              </li>
              <li> <a href="/category/drugie/flash-seeds/">Flash-seeds</a>
                <ul>
                  <li><a href="/category/drugie/flash-seeds/flash-seeds-auto-fem/">Flash-seeds Auto Fem</a></li>
                  <li><a href="/category/drugie/flash-seeds/flash-seeds-regular/">Flash-seeds Regular</a></li>
                </ul>
              </li>
              <li> <a href="/category/drugie/grass-o-matic-seeds/">Grass-o-matic seeds</a>
                <ul>
                  <li><a href="/category/drugie/grass-o-matic-seeds/grass-o-maitc-seeds-auto-fem/">Grass-o-matic seeds Auto Fem</a></li>
                </ul>
              </li>
              <li> <a href="/category/drugie/joint-doctor-seeds/">Joint Doctor seeds</a>
                <ul>
                  <li><a href="/category/drugie/joint-doctor-seeds/joint-doctor-seeds-regular/">Joint Doctor seeds Regular</a></li>
                  <li><a href="/category/drugie/joint-doctor-seeds/joint-doctor-seeds-auto-fem/">Joint Doctor seeds Auto Fem</a></li>
                </ul>
              </li>
              <li> <a href="/category/drugie/seedsman/">Seedsman</a>
                <ul>
                  <li><a href="/category/drugie/seedsman/seedsman-auto-fem/">Seedsman Auto Fem</a></li>
                  <li><a href="/category/drugie/seedsman/seedsman-regular/">Seedsman Regular</a></li>
                  <li><a href="/category/drugie/seedsman/seedsman-feminised/">Seedsman Feminised</a></li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li> <a href="/category/oborydovanie-i-udobreniya/oborydovanie/"> Оборудование </a></li>
	  <li> <a href="/category/oborydovanie-i-udobreniya/udobreniya/"> Удобрения </a></li>
	  <li> <a href="/auxpage_action_list"> Акции </a></li>
      <li> <a href="/auxpage_sotrudnichestvo/"> Сотрудничество </a>
        <ul>
          <li><a href="/auxpage_referalnaya_programma/">Реферальная программа</a></li>
          <li><a href="/auxpage_opt/">Опт</a></li>
          <li><a href="/auxpage_dropshipping/">Дропшиппинг</a></li>
          <li><a href="/auxpage_interesnyy_kontent/">Интересный контент</a></li>
          <li><a href="/auxpage_smm/">SMM </a></li>
          <li><a href="/auxpage_distribjucija/">Дистрибьюция</a></li>
          <li><a href="/auxpage_vakansii/">Вакансии</a></li>
        </ul>
      </li>
      </ul>
    </nav>
  </div>
</div>
