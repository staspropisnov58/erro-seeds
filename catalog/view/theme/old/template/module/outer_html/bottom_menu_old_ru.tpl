<div class="footer_top">
  <div class="container">
    <div class="d-flex flex-wrap mb-lg-4">
      <div class="footer_sub col-12 col-md-5">
        <div class="f_sub_head f_title">О магазине <i class="fas fa-chevron-down"></i></div>
        <div class="f_sub_cont ">
          <ul class="sf-menu">
            <li>
              <li> <a href="/blog/">Блог</a></li>
              <li> <a href="/auxpage_about/"> О нас </a></li>
              <li> <a href="/feedback/"> Контакты </a></li>
              <li> <a href="/auxpage_shipping/"> Оплата </a></li>
              <li> <a href="/auxpage_delivery/"> Доставка </a></li>
              <li> <a href="/sitemap/"> Карта сайта </a></li>
              <li> <a href="/auxpage_polit-konf/"> Политика конфиденциальности </a></li>
            </li>
          </ul>
        </div>
      </div>
      <div class="footer_sub col-12 col-md-4">
        <div class="f_sub_head f_title"> Семена <i class="fas fa-chevron-down"></i></div>
        <div class="f_sub_cont">
          <ul class="sf-menu">
            <li><a href="/category/errors-seeds/">Errors Seeds Silver</a></li>
            <li><a href="/category/errors-seeds-import/">Errors Seeds Gold</a></li>
            <li><a href="/category/carpathians-seeds/">Carpathians Seeds</a></li>
            <li><a href="/category/drugie/green-house-seeds/">Green House Seeds</a></li>
            <li><a href="/category/drugie/dinafem/">Dinafem</a></li>
            <li><a href="/category/sweet-seeds/">Sweet Seeds</a></li>
            <li><a href="/category/barneys-farm/">Barneys Farm</a></li>
          </ul>
        </div>
      </div>
      <div class="footer_sub col-12 col-md-3">
        <div class="payment mb-3"><img src="/img/payment.webp"></div>
        <ul class="sf-menu">
          <li>
            <a rel="http://growmarket.com.ua/catalog/osveschenie" href="#" target="_blank" class="hidli">
              Лампы для растений
            </a>
          </li>
          <li>
            <a rel="http://medcannabis.info/" href="#" target="_blank" class="hidli">
              Медицина</a>
          </li>
          <li>
            <a rel="https://jahforum.net/" href="#" target="_blank" class="hidli">
              Форум</a>
          </li>
        </ul>
        <button type="button" name="director" class="director_btn">Связь с директором</button>
      </div>
    </div>
  </div>
</div>
