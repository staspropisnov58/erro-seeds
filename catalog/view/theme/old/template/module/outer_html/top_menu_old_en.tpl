<div class="menu">
  <ul class="header_menu">
    <li>
      <p class="js-dropmenu_btn dropmenu_btn btn_menu_green">
        <span><span class="icon"></span>Catalogue</span>
        <i class="fas fa-chevron-right"></i>
      </p>
      <div class="js-dropmenu dropmenu">
        <ul>
          <li class="prev">
            <i class="fas fa-chevron-left"></i>
          </li>
          <li>
            <p>Seeds <i class="fas fa-chevron-right"></i></p>
            <ul class="line">
              <li class="prev">
                <i class="fas fa-chevron-left"></i>
              </li>
              <li>
                <p>
                  <a href="/en/category/errors-seeds/">Errors Seeds Silver</a>
                  <i class="fas fa-chevron-right"></i>
                </p>
                <ul class="line">
                  <li class="prev">
                    <i class="fas fa-chevron-left"></i>
                  </li>
                  <li><a href="/en/category/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
                  <li><a href="/en/category/errors-seeds/errors-seeds-auto-fem/">Errors Seeds Silver Auto Feminised</a></li>
                  <li><a href="/en/category/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
                  <li><a href="/en/category/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
                  <li><a href="/en/category/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
                </ul>
              </li>
            </ul>
          </li>
      </div>
    </li>
    <li>
      <p class="js-dropmenu_btn dropmenu_btn old_style">About Our Store <i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i></p>
      <div class="js-dropmenu dropmenu">
        <ul>
          <li class="prev">
            <i class="fas fa-chevron-left"></i>
          </li>
          <li><a href="/en/auxpage_about/">About Us</a></li>
          <li><a href="/en/feedback/">Contacts</a></li>
          <li><a href="/en/auxpage_kak-kupit-semena-konopli/">How to buy cannabis seeds?</a></li>
          <li><a href="/en/auxpage_faq/">FAQ</a></li>
          <li><a href="/en/auxpage_shipping/">Payment</a></li>
          <li><a href="/en/auxpage_delivery/">Delivery</a></li>
          <li><a href="/en/auxpage_schedule/">Working Hours</a></li>
          <li><a href="/en/reports/">Reviews</a></li>
          <li><a href="/en/auxpage_polit/"><font color="#65bd00">Return and Exchange Policy</font></a></li>
        </ul>
      </div>
    </li>
    <li><a href="/en/auxpage_action_list"><span style="color: red; font-weight: bold;">SALE</span></a></li>
    <li><a rel="nofollow" href="/en/blog/">Our News</a></li>
    <li>
      <p class="js-dropmenu_btn dropmenu_btn old_style">Partnership Program <i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i></p>
      <div class="js-dropmenu dropmenu">
        <ul>
          <li class="prev">
            <i class="fas fa-chevron-left"></i>
          </li>
          <li><a href="/en/auxpage_sotrudnichestvo/">About Partnership Program</a></li>
          <li><a href="/en/auxpage_referalnaya_programma">Referral Program</a></li>
          <li><a href="/en/auxpage_opt">Wholesale</a></li>
          <li><a href="/en/auxpage_dropshipping">Dropshipping</a></li>
          <li><a href="/en/auxpage_interesnyy_kontent">Interesting Facts</a></li>
          <li><a href="/en/auxpage_smm">SMM</a></li>
          <li><a href="/en/auxpage_distribjucija">Distribution</a></li>
          <li><a href="/en/auxpage_vakansii">Job Offers</a></li>
        </ul>
      </div>
    </li>
  </ul>
</div>
