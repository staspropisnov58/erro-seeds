<div class="d-flex align-items-center custom-sel-wrapper">
  <div class="custom-sel" id="input-sort" onchange="location = this.value;">
      <div class="custom-sel__trigger">
        <i class="fas fa-sort" style="padding-right:5px;color:#65bd00;font-size:17px"></i>
        <span>
          <?php echo $current_sort; ?>
        </span>
        <i class="fas fa-chevron-down"></i>
      </div>
      <div class="custom-options">
        <span class="custom-option" data-value="<?php echo $sort_order_href; ?>"><?php echo $sort_order; ?></span>
        <?php foreach ($sortings as $key => $sorting) { ?>
          <span class="custom-option <?php echo $sorting['selected']; ?>" data-value="<?php echo $sorting['url']; ?>"><?php echo $$key; ?></span>
        <?php } ?>
      </div>
    </div>
  </div>
