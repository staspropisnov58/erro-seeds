<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">	 <!--[if !mso]><!-->
<meta http-equiv="X-Ua-Compatible" content="IE=edge">	 <!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="GENERaTOR" content="MSHTML 11.00.9600.18666">
<style type="text/css">
@media screen and (max-width: 360px){
    p{ font-size: 16px !important; }
    .container{width: 100%; max-width: 360px}
}
</style>
</head>
<body>
<div class="container" style="margin: -8px; width: 100%; background-image: url(https://res.cloudinary.com/seoboost/image/upload/v1570713055/email_border_bg/order_bg.jpg); ">
	<center>
		<table border="0" cellpadding="0" cellspacing="0" style="width: 95%; max-width: 600px; font-size: 18px; font-family: Arial, Arial, Helvetica, sans-serif;">
			<tbody>
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px; padding: 0 10px; background-color: #82b523">
							<tbody>
								<tr>
									<td style="width: 100%;max-width: 600px">
										<a href="<?php echo $store_url; ?>" style="text-decoration: none;" target="_blank"><img style="" src="https://lh3.googleusercontent.com/UYk6Pcg4UvTS61z_Vvv1QUam5wtS4WRtxTGgItJquyDBj-0DN7GSFIG7dcafnsUUWSPWOQFd4-F3xFgcoc80J6KtloIx3fuB8Rg8Qw8ZbcX2oEJzEgd6Ty4IlJqqUqEPD_wmVynXBtAsLtzDBfoxri3Ntd6J86e_vgZqg5NC0-51CpYbhkO65b7m8NpTOGyAa2Qo7FG-JmtSGCPI1VT_MJk83Rsz5TczpML5ZWRviX_F49qbeKKHGA2kJN6nDdBDrUMbKUtKgTuRVp6jEFDS2GgHdk47Fpnynad_QJF1HTFeET9mt0w_UgtWlCFhB-gf8rB3Nt9yIsFxXdryjHUUM8zQw5hy815LGVNBjn_XHpyfcJdMaL74qICTgHPSwZvl-dfHb8m6lqrCDc1l0nzUCrqvM_PUAy7V-25xqjEnzI9yVSXoZZZHeJlZMp6IOWo655k2vtqtqyPl5gAKB_TGyx7oMhqf4V9GD4x4IXylZiogxIe_OEUBo6BeFT0jY7SEjumQaydKqtE5FoM4xTBurNI9dx72RkPkPYhcCRzER3Je2ZLPe49-8eQxke8yMEkyHMmJYsIZjmTKtnPPzZyqkg0lHoifwLNLmD0lrpneGDArdiv3QwP_q-P_t8qsI0gogxs-IqznVB4DSwRNTrTBXFz7rZ35Wy6iSoM2pNnOJyr_cVOi28sQhwiv=w200-h50-no" alt=""></a>
									</td>
								</tr>
							</tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" style="background-color: #FFFFFF;width: 100%; max-width: 600px; padding: 10px">
							<tbody>
								<tr>
									<td>
										<p style="font-size: 20px"><?=$text_greeting?></p>
                    <?php foreach ($text_main as $text) { ?>
                      <p style="font-size: 20px"><?=$text?></p>
                    <?php } ?>
										<p style="font-size: 16px"><?=$text_note?></p>
									</td>
								</tr>
							</tbody>
						</table>
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px; padding-bottom: 50px; font-family: Arial, Arial, Helvetica, sans-serif; ">
							<tbody>
								<tr>
									<td style="width: 100%; text-align: center;">
										<a href="<?php echo $store_url; ?>" target="_blank" style="text-decoration: none;"><p style="color: #656464">© Errors seeds сувениры из Европы 2009-<?=date('Y');?></p>
									</td>
								</tr>
								<tr>
									<td style="width: 100%;">
										<center>
                      <div>
                        <a href="https://www.facebook.com/esuacom/" style="text-decoration:none;display:inline-block;width:50px" target="_blank">
                          <img src="https://res.cloudinary.com/seoboost/image/upload/v1570697967/email_icon/unnamed_1.png" alt="facebook" style="padding:5px; width:30px">
                        </a>
                        <a href="https://www.instagram.com/errorsseeds_ua/" style="text-decoration:none;display:inline-block;width:50px" target="_blank">
                          <img src="https://res.cloudinary.com/seoboost/image/upload/v1570697969/email_icon/unnamed_2.png" alt="instagram" style="padding:5px;  width:30px">
                        </a>
                      </div>
										</center>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</div>
</body>
</html>
