<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div class="container">
  <div class="main_content_text">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <div class="inform_page mb-5">
      <div class="cpt_maincontent">
        <div>
          <?php echo $description; ?>
          <?php echo $column_left; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
<script src="catalog/view/theme/old/js/lightbox.js" type="text/javascript" defer=""></script>
