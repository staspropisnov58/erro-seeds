<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
        <div id="content" class="col-12 col-md-8 order-1 order-md-2">
      <?php if (!empty($affiliate_id)){ ?>
      <div class="h1"><h1><?php echo $heading_title; ?></h1></div>
      <p><?php echo $text_balance; ?> <strong><?php echo $balance; ?></strong>.</p>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td class="text-left"><?php echo $column_description; ?></td>
              <td class="text-right"><?php echo $column_amount; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($transactions) { ?>
            <?php foreach ($transactions  as $transaction) { ?>
            <tr>
              <td class="text-left"><?php echo $transaction['date_added']; ?></td>
              <td class="text-left"><?php echo $transaction['description']; ?></td>
              <td class="text-right"><?php echo $transaction['amount']; ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="text-center" colspan="5"><?php echo $text_empty; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    <?php } ?>
      <div class="text-right"><?php echo $pagination; ?></div>
      <div class="buttons">
        <div><a href="<?php echo $continue; ?>" class="green_button btn"><?php echo $button_continue; ?></a></div>
      </div></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
