<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
      <div id="content" class="col-12 col-md-8 order-1 order-md-2 address_book">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="h1"><h1><?php echo $text_address_book; ?></h1></div>
        <?php if ($addresses) { ?>
        <table>
          <?php $i = 0; foreach ($addresses as $key => $result) { $i++; ?>
          <?php if ($key == $main_address){ ?>
          <?php if ($i % 2 == 0){ ?>
          <?php $color = '#363636'; ?>
          <?php }else{ ?>
          <?php  $color = '#2b2b2b'; ?>
          <?php    } ?>

          <tr style="background: <?=$color;?>">
            <td align="left" class="address"><?php echo $result['address']; ?></td>
            <td align="left" class="address_x"><?php echo $result['address_x']; ?></td>
            <td align="centr" style="width: 70px;padding-left: 15px;">
              <a href="<?php echo $result['update']; ?>"><?php  $button_edit; ?><i class="fas fa-edit" style="color:#65bd00"></i></a>
            </td>
          </tr>
          <?php } ?>

          <?php } ?>

          <?php $i = 0; foreach ($addresses as $key => $result) { $i++; ?>
          <?php if ($key == $main_address){}else{ ?>
          <?php if ($i % 2 == 0){ ?>
          <?php $color = '#363636'; ?>
          <?php }else{ ?>
          <?php $color = '#2b2b2b'; ?>
          <?php } ?>

          <tr style="background: <?=$color;?>">
            <td align="left" class="address"><?php echo $result['address']; ?></td>
            <td align="left" class="address_x"><?php echo $result['address_x']; ?></td>
            <td align="centr" style="width: 70px;padding-left: 15px;">
              <a href="<?php echo $result['update']; ?>"><?php  $button_edit; ?><i class="fas fa-edit" style="color:#65bd00"></i></a>
              <a href="<?php echo $result['delete']; ?>"><i class="fas fa-trash" style="color:#f00;padding-left:10px"></i><?php  ' '.$button_delete; ?></a>
            </td>
          </tr>
          <?php } ?>

          <?php } ?>
        </table>
        <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
        <?php } ?>
        <div class="d-flex justify-content-between">
          <a href="<?php echo $add; ?>" class="green_button"><?php echo $button_new_address; ?></a>
        </div>
      </div>
      <?php echo $column_right; ?>
    </div>
  </div>
</div>
<?php echo $pagination; ?>

<?php echo $content_bottom; ?>
<?php echo $footer; ?>
