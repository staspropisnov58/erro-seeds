<?php echo $header; ?>
<style>
#content{
  font-family: 'AGLettericaCondensed-Roman';
  color: #fff;
}
</style>
<div class="container">
  <div class="row col-12 m-auto col-lg-10 p-0 col-12">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
        <div id="content" class="col-12 col-md-8 order-1 order-md-2">
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($downloads) { ?>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-right"><?php echo $column_order_id; ?></td>
            <td class="text-left"><?php echo $column_name; ?></td>
            <td class="text-left"><?php echo $column_size; ?></td>
            <td class="text-left"><?php echo $column_date_added; ?></td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($downloads as $download) { ?>
          <tr>
            <td class="text-right"><?php echo $download['order_id']; ?></td>
            <td class="text-left"><?php echo $download['name']; ?></td>
            <td class="text-left"><?php echo $download['size']; ?></td>
            <td class="text-left"><?php echo $download['date_added']; ?></td>
            <td><a href="<?php echo $download['href']; ?>" data-toggle="tooltip" title="<?php echo $button_download; ?>" class="btn btn-primary"><i class="fa fa-cloud-download"></i></a></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div><a href="<?php echo $continue; ?>" class="btn continue"><?php echo $button_continue; ?></a></div>
      </div>
    </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
