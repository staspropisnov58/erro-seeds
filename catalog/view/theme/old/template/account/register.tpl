<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
            } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="reg-page">
      <div class="wr-reg-page">
        <div class="d-flex align-items-center header_acc registr col-12 col-sm-8 col-md-8 col-lg-7 mx-auto">
          <div class="d-flex flex-row w-100 justify-content-start">
            <div class="a-reg ">
              <?php echo $text_account_already; ?>
            </div>
            <span class="slesh">/</span>
            <div class="a-reg"><?php echo $heading_title; ?></div>
          </div>
        </div>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <form class="col-12 col-sm-8 col-md-8 col-lg-7 mx-auto" action="<?php echo $form['action']; ?>" method="post" enctype="multipart/form-data" name="<?php echo $form['name']; ?>" id="require_agree" class="protected">
          <?php foreach ($form['fields'] as $field_name => $field) { ?>
          <div class="form-group d-flex flex-column <?php if ($field_name !== 'referred_by') { echo 'required'; } ?>">
            <label for="<?php echo $field_name; ?>" class="white_label"><?php echo $field['label']; ?></label>
            <div class="d-flex flex-row position-relative">
              <?php if ($field['is_group']) { ?>
              <?php foreach ($field['fields'] as $groupped_field_name => $groupped_field) { ?>
              <?php if ($groupped_field['type'] === 'select') { ?>
              <select name="<?php echo $groupped_field_name; ?>" class="form-control mr-2 w-50" id="customselect">
                <?php foreach($countries as $country){ ?>
                <?php if ($country['country_id'] === $groupped_field['value']) { ?>
                <option selected value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?>
                </option>
                <?php }else{ ?>
                <option value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php } else { ?>
              <input type="<?php echo $groupped_field['type']; ?>" name="<?php echo $groupped_field_name; ?>" class="form-control trans-input form-control-success" value="<?php echo $groupped_field['value']; ?>"
                id="<?php echo $groupped_field_name; ?>" placeholder="<?php echo $groupped_field['placeholder']; ?>">
              <?php } ?>
              <?php } ?>
              <?php } else { ?>
              <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control trans-input form-control-success" value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>"
                placeholder="<?php echo $field['placeholder']; ?>">
              <?php } ?>
            </div>
            <?php if ($field['error']) { ?>
            <label for="<?php echo $field_name; ?>" class="error"><?php echo $field['error']; ?></label>
            <?php } ?>
          </div>

          <?php } ?>
          <?php if ($newsletter_enabled) { ?>
            <div class="form-group d-flex flex-column">
              <input class="custom_checkbox" type="checkbox" name="newsletter" id="newsletter" <?php if ($newsletter) { echo 'checked'; }?>>
              <label for="newsletter" class="white_label for_checkbox"> <?php echo $entry_newsletter; ?> </label>
            </div>
          <?php } ?>

          <?php if($tuning == 1){ ?>
          <div class="form-group d-flex flex-column">
            <input class="custom_checkbox" type="checkbox" name="affiliate" id="affiliate">
            <label for="affiliate" class="white_label for_checkbox"><?php echo $text_affiliate; ?> </label>
          </div>
          <?php } ?>
          <div class="form-group d-flex flex-column">
            <input class="custom_checkbox" type="checkbox" name="agree" id="agree" <?php if ($agree) { echo 'checked'; }?>>
            <label for="agree" class="white_label for_checkbox"><?php echo $text_agree; ?> </label>
          </div>
          <p class="text-left"><?php echo $text_warranty; ?></p>
          <?php if (isset($SOCNETAUTH2_DATA)) { ?>
            <?php echo $SOCNETAUTH2_DATA['code']; ?>
          <?php } ?>
          <button class="form-btn green_button d-flex mx-auto g-recaptcha" style="cursor: pointer" type="submit" id="submit_register"><?=$button_register_acc?></button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#customselect').SelectCustomizer();
  });
</script>

<?php echo $content_bottom; ?>
<?php echo $footer; ?>
