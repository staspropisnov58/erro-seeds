<?php echo $header; ?>
<div class="container">
	<div class="account_page">
		<div class="back-cat ">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php if (isset($breadcrumb['href'])) { ?>
			<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
			<?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
		</div>
		<?php echo $content_top; ?>
		<div class="col-12 p-0 d-flex flex-wrap">
			<div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
			<div id="content" class="col-12 col-md-8 order-1 order-md-2">
<h2><?=$heading_title?></h2>
				<form id="vip_account"  method="post" enctype="multipart/form-data" class="wr-reg-page col-12 p-0" name="vip_account">

					<div class="form-group">

						<div class="d-flex flex-column position-relative">


							<div class="d-flex flex-row">
								<div style="width: 100%" class="mr-2 ">
								<label for="input_name" class="white_label"><?=$text_name?> <span>*</span></label>
								<input
										type="text"
										name="name"
										class="form-control trans-input form-control-success "
										value="<?=$firstname?>"
										id="input_name"
										placeholder="<?=$text_name?>">
							</div>
								<div style="width: 100%">
								<label for="input_email" class="white_label"><?=$text_email?> <span>*</span></label>
								<input
										type="text"
										name="email"
										class="form-control trans-input form-control-success"
										value="<?=$email?>"
										id="input_email"
										placeholder="<?=$text_email?>">
								</div>
							</div>



						</div>
					</div>
					<div class="form-group">
						<label for="telephone" class="white_label"><?=$text_tel?> <span>*</span></label>
						<div class="d-flex flex-row">

							<select name="tel_phone" class="form-control w-100 mr-2" id="telephone">
								<?php foreach($countries as $country){ ?>
								<?php if ($country['country_id']==$country_id) { ?>
								<option selected value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>">
									<?php echo $country['name'] . ' - ' . $country['tel_code']; ?></option>
								<?php }else{ ?>
								<option value="<?php echo $country['country_id']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?>
								</option>
								<?php } ?>
								<?php } ?>
							</select>
							<input type="tel" name="telephone" class="form-control trans-input form-control-success" value="" id="telephone" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label for="masenger" class="white_label"><?=$text_mes?></label>
						<textarea id="masenger" class="form-control trans-input " name="masenger">

						</textarea>

					</div>

					<div class="buttons clearfix">
						<div class="pull-left" style="display: none"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
						<div>
							<input type="submit" value="<?php echo $button_checkout; ?>" class="green_button btn" />
						</div>
					</div>
				</form>

			</div>
			<?php echo $column_right; ?>
		</div>
	</div>
</div>

<script>
	$(document).ready(function (){
		$('#vip_account').submit(function (e){
			e.preventDefault()
			var form = $('#vip_account').serialize();

            $.ajax({
                url: 'index.php?route=account/vip/addMessenger',
                type: 'post',
                data: form,
                dataType: 'json',
                success: function(json) {

                    console.log(json);
                }
            });

		})
	})
</script>
<style>
	.form-group label span
	{
		color:red;
    }
</style>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>