<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
      <div id="content" class="col-12 col-md-8 order-1 order-md-2">
        <div class="h1"><h1><?php echo $heading_title; ?></h1></div>
        <?php if ($orders) { ?>
        <div class="table-responsive">
          <table class=" ">
            <thead>
              <tr>
                <td class="text-left"><?php echo $column_order_id; ?></td>
                <td class="text-left"><?php echo $column_status; ?></td>
                <td class="text-left"><?php echo $column_date_added; ?></td>
                <td class="text-left"><?php echo (isset($column_quantity)?$column_quantity:''); ?></td>
                <td class="text-left"><?php echo $column_total; ?></td>
                <td></td>
              </tr>
            </thead>
            <tbody>

              <?php $i=0; foreach ($orders as $order) {
                                    $i++;
                                    if ($i % 2 == 0){
                                        $color = '#363636';
                                    }else{
                                        $color = '#2b2b2b';
                                    }

                                    ?>


              <?php if ($order['show_in_order_list'] != 0) { ?>
              <tr style="background: <?=$color;?>">
                <td class="text-left">#<?php echo $order['order_id']; ?></td>
                <td class="text-left"><?php echo $order['status']; ?></td>
                <td class="text-left"><?php echo $order['date_added']; ?></td>
                <td class="text-left"><?php echo $order['products']; ?></td>
                <td class="text-left"><?php echo $order['total']; ?></td>
                <td class="text-left">
                  <a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>">
                    <i class="far fa-eye" style="color:#65bd00"></i>
                  </a>
                  <a href="#" class="delete_order" data-toggle="tooltip" data-order-id="<?php echo $order['order_id']; ?>">
                    <i class="fas fa-trash" style="color:#f00;padding-left:10px"></i>
                  </a>
                </td>
              </tr>
              <?php } ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="text-right"><?php echo $pagination; ?></div>
        <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
        <?php } ?>
        <div class="buttons clearfix" style="display: none">
          <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div>
      </div>
      <?php echo $column_right; ?>
    </div>
  </div>
</div>


    <script>
        $('.delete_order').on('click', function (e) {
            e.preventDefault();
            var order_id = $(this).data('orderId');

            console.log(order_id)
            $.ajax({
                url: 'index.php?route=account/order/delete_order_history',
                type: 'post',
                data: {'order_id': order_id},
                success: function (json) {
                    console.log(json)
                    if (json == 1) {
                        location.reload();
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    console.log('ERRORS: ' + textStatus);
                }
            });
        });


    </script>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
