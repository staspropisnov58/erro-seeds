<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap mb-3">
      <div class="col-12 col-sm-8 col-md-8 col-lg-7 mx-auto">
        <div class="h1">
          <h1><?php echo $text_reset; ?></h1>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="p-0 mx-auto col-12">
          <fieldset>
            <div class="form-group">
              <label class="white_label" for="input-password"><?php echo $text_password; ?></label>
              <div class="">
                <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control trans-input form-control-success" />
                <?php if ($error_password) { ?>
                <div class="text-danger"><?php echo $error_password; ?></div>
                <?php } ?>
              </div>
            </div>
          </fieldset>
          <div class="buttons">
            <button type="submit" class="green_button d-flex mx-auto"><?php echo $button_continue; ?></button>
          </div>
        </form>
      </div>
      <?php echo $column_right; ?>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
