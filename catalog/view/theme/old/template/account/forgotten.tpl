<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="reg-page row">
      <div class="wr-reg-page col-12 col-sm-8 col-md-8 col-lg-7 mx-auto">
        <div class="d-flex justify-content-between align-items-center">
          <div class="a-reg"><?php echo $heading_title; ?></div>
          <div class="a-reg ">
            <a href="<?php echo $back; ?>" class="color-theme"><?php echo $button_back; ?></a>
          </div>
        </div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <?php if ($error_warning) { ?>
            <p style="color: #ff3333;font-size: 18px;"><?php echo $error_warning; ?></p>
            <?php } ?>
            <label for="formEmail" class="white_label"><?php echo $entry_email; ?></label>
            <input type="text" name="email" class="form-control trans-input" id="formEmail" placeholder="<?php echo $entry_email; ?>">
          </div>
          <button class="green_button d-flex mx-auto"><?php echo $button_continue; ?></button>
        </form>
        <div class="block-soc-vhod" style="display: none">
          <div class="block-soc-vhod-title">Войти с помощью</div>
          <div class="block-soc-vhod-wr d-flex justify-content-center">
            <a href="" class="fa fa-vk"></a>
            <a href="" class="fa fa-facebook"></a>
            <a href="" class="fa fa-twitter"></a>
            <a href="" class="fa fa-google-plus"></a>
            <a href="" class="fa fa-instagram"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $('input').on('change', function() {
    if ($(this).val()) {

      $(this).parent().addClass('has-success')
      $(this).closest('.form-group')
        .find('p')
        .fadeOut('slow');

    }
  });
</script>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
