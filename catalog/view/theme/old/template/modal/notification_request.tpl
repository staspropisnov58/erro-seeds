<div class="modal-content">
  <div class="modal-header">
    <p class="modal-title" id="ModalCenterTitle"><?php echo $title; ?></p>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <img src="/catalog/view/theme/old/images/close.svg">
    </button>
  </div>
  <div class="modal-body" id="product">
    <form method="post" id="product_form" <?php if ($form) { ?> action="<?php echo $form['action']; ?>" name="<?php echo $form['name']; ?>" <?php } ?>>
      <p class="prd_out_of_stock"><?php echo $form['title']; ?></p>
      <?php echo $form['content']; ?>
    </form>
  </div>
</div>
