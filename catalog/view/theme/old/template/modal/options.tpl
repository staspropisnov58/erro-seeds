<div class="modal-content">
  <div class="modal-header">
    <span></span>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <img src="/catalog/view/theme/old/images/close.svg">
    </button>
  </div>
  <div class="modal-body" id="product">
    <form id="product_form">
      <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
      <input type="hidden" name="price" value="<?php echo $price; ?>">
      <div class="modal_product_container">
        <div class="modal_prod_img">
          <img src="<?php echo $image; ?>" alt="">
        </div>
        <div class="product_info">
          <div class="product_info_name">
            <span class="cpt_product_name"><?php echo $name; ?></span>
            <div class="d-flex">
              <?php if ($manufacturer) { ?>
                <span class="product_manufac"><?php echo $manufacturer; ?></span>
              <?php } ?>
              <?php if ($sku) { ?>
                <span class="code"><?php echo $sku; ?></span>
              <?php } ?>
            </div>
          </div>
          <div class="prices_block">
            <?php	 if(!empty($price)) {
           if(!empty($special) ) {?>
            <span id="totalprice" class="totalPrice price_sale new_price"><?=$special?></span>
            <span class="regularPrice price_sale old_price"><?=$price?></span>
            <p id="saving_text"><?php echo $text_economy; ?>&nbsp;<span class="saving_price"><?php echo $saving; ?></span></p>
            <?php } else { ?>
            <span id="totalprice" class="totalPrice new_price"><?=$price?></span>
            <?php } ?>
            <?php } ?>
          </div>
          <?php if ($options) { ?>
          <div class="product_options">
            <?php foreach ($options as $option) { ?>
            <?php if (is_array($option)) { ?>
            <div class="cpt_product_params_selectable">
              <span><?php echo $option['name']; ?>:</span>
              <div class="option_select">
                <?php $checked = 'checked'; ?>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>"
                  data-price="<?php echo $option_value['price']; ?>" <?php if($option_value['old_price']) { ?>data-old-price="<?php echo $option_value['old_price']; ?>" <?php } ?> <?php echo $checked; ?>
                  <?php if ($option_value['trigger_image']) { ?>data-image="<?php echo $option_value['trigger_image']; ?>" <?php } ?> onchange="addOption(this)">

                <label for="<?php echo $option_value['product_option_value_id']; ?>">
                  <?php if ($option_value['image']) { ?>
                  <?php if ($option_value['image']['extension'] === 'svg') { ?>
                  <?php echo $option_value['image']['value']; ?>
                  <?php } else { ?>
                  <img src="<?php echo $option_value['image']['value']; ?>" alt="<?php echo $option_value['name']; ?>" title="<?php echo $option_value['name']; ?>">
                  <?php } ?>
                  <?php } else { ?>
                  <?php echo $option_value['name']; ?>
                  <?php } ?>
                </label>
                <?php $checked = ''; ?>
                <?php  }?>
              </div>
            </div>
            <?php } ?>
            <?php } ?>
            <div class="d-flex flex-wrap justify-content-between align-items-end">
              <div class="product_quantity">
                <span><?php echo $entry_qty; ?>:</span>
                <div>
                  <input type="button" onclick="plus(this, '-1')" value=" - " class="count-icon-m">
                  <input name="quantity" id="acpro_inp_1777" class="product_qty" type="text" data-quantity="1" value="1" onchange="recount(this)">
                  <input type="button" onclick="plus(this, '1')" value=" + " class="count-icon-p">
                </div>
              </div>
              <!-- <p class="advantage"></p> -->
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="modal_btns">
        <button type="button" data-dismiss="modal" name="button" class="grey_button"><?php echo $button_continue_shopping; ?></button>
        <button type="submit" data-toggle="model" onclick="cart.add(<?php echo $product_id; ?>, 1, event)" class="cpt_product_add2cart_button green_button"><?php echo $button_cart; ?></button>
      </div>
    </form>
  </div>
</div>
