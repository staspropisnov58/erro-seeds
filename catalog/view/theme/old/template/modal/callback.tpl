<div class="modal-content">
  <div class="modal-header">
    <p class="modal-title" id="ModalCenterTitle"><?=$title?></p>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <img src="/catalog/view/theme/old/images/close.svg">
    </button>
  </div>
  <div class="modal-body">
    <form action="<?php echo $form['action']; ?>" name="<?php echo $form['name']; ?>">
      <?php foreach ($form['fields'] as $field_name => $field) { ?>
        <div class="form-group">
          <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label'] ?></label>
          <?php if ($field['is_group']) { ?>
              <div class="d-flex flex-row position-relative">
                <?php foreach ($field['fields'] as $groupped_field_name => $groupped_field) { ?>
                  <?php if ($groupped_field['type'] === 'select') { ?>
                    <select name="<?php echo $groupped_field_name; ?>" class="form-control w-100 mr-2" id="customselect_callback">
                    <?php foreach($countries as $country){ ?>
                      <?php if ($country['country_id'] === $groupped_field['value']) { ?>
                        <option selected value = "<?php echo $country['tel_code']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?></option>
                      <?php }else{ ?>
                        <option value = "<?php echo $country['tel_code']; ?>" data-content="<img src='<?php echo $country['image']; ?>'" data-code="<?php echo $country['tel_code']; ?>"><?php echo $country['name'] . ' - ' . $country['tel_code']; ?></option>
                      <?php } ?>
                    <?php } ?>
                    </select>
                  <?php } else { ?>
                    <input type="<?php echo $groupped_field['type']; ?>" name="<?php echo $groupped_field_name; ?>" class="form-control trans-input form-control-success" value="<?php echo $groupped_field['value']; ?>" id="<?php echo $groupped_field_name; ?>" placeholder="<?php echo $groupped_field['placeholder']; ?>" >
                  <?php } ?>
                <?php } ?>
              </div>
          <?php } else { ?>
            <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control" value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>" >
          <?php } ?>
        </div>
      <?php } ?>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn green_button" id="send-callback" onclick="sendCallback()"><?=$button_submit?></button>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#customselect_callback').SelectCustomizer();
	});
</script>
