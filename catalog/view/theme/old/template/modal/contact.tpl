<div class="modal-content">
  <div class="modal-header">
    <p class="modal-title" id="ModalCenterTitle"><?php echo $title; ?></p>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <img src="/catalog/view/theme/old/images/close.svg">
    </button>
  </div>
  <div class="modal-body" id="contact">
    <form method="post" id="contact_form" <?php if ($form) { ?> action="<?php echo $form['action']; ?>" name="<?php echo $form['name']; ?>" <?php } ?>>
      <?php if ($form) { ?>
        <div class="form_review">
          <?php foreach ($form['fields'] as $field_name => $field) { ?>
            <div class="d-flex flex-row justify-content-center mt-2">
              <div class="form-field w-100">
              <?php if ($field['type'] === 'textarea') { ?>
                <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label']; ?></label>
                <textarea name="<?php echo $field_name; ?>" class="form-control"
                value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder=""></textarea>
              <?php } else { ?>
                  <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label']; ?></label>
                  <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control"
                  value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="">
              <?php } ?>
            </div>
            </div>
          <?php } ?>
          <div class="modal_btns">
            <button type="submit" alt="<?php echo $button_submit; ?>" title="" rel="" onclick="sendContact(event)" class="cpt_product_add2cart_button green_button" id="submit_contact">
              <?php echo $button_submit; ?>
            </button>
          </div>
        </div>
      <?php } ?>
    </form>
  </div>
</div>
