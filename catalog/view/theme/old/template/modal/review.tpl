<div class="modal-content">
  <div class="modal-header">
    <p class="modal-title" id="ModalCenterTitle"><?php echo $title; ?></p>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <img src="/catalog/view/theme/old/images/close.svg">
    </button>
  </div>
  <div class="modal-body" id="review">
    <form method="post" id="review_form" <?php if ($form) { ?> action="<?php echo $form['action']; ?>" name="<?php echo $form['name']; ?>" <?php } ?>>
      <?php if ($form) { ?>
        <div class="form_review">
          <?php foreach ($form['fields'] as $field_name => $field) { ?>
            <div class="d-flex flex-row justify-content-center mt-2">
              <div class="form-field w-100">
              <?php if ($field['type'] === 'radio') { ?>
                <label for="rev-rating" class="for_input"><?php echo $field['label']; ?></label>
                <div class="form-group" id="rating">
                  <input type="radio" name="<?php echo $field_name; ?>" value="0" id="<?php echo $field_name; ?>_0" checked>
                  <?php foreach ($field['options'] as $radio_value) { ?>
                    <input type="radio" name="<?php echo $field_name; ?>" value="<?php echo $radio_value; ?>" id="<?php echo $field_name; ?>_<?php echo $radio_value; ?>">
                    <label for="<?php echo $field_name; ?>_<?php echo $radio_value; ?>" class="label_rating">
                      <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cannabis" class="svg-inline--fa fa-cannabis fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path fill="currentColor" d="M503.47 360.25c-1.56-.82-32.39-16.89-76.78-25.81 64.25-75.12 84.05-161.67 84.93-165.64 1.18-5.33-.44-10.9-4.3-14.77-3.03-3.04-7.12-4.7-11.32-4.7-1.14 0-2.29.12-3.44.38-3.88.85-86.54 19.59-160.58 79.76.01-1.46.01-2.93.01-4.4 0-118.79-59.98-213.72-62.53-217.7A15.973 15.973 0 0 0 256 0c-5.45 0-10.53 2.78-13.47 7.37-2.55 3.98-62.53 98.91-62.53 217.7 0 1.47.01 2.94.01 4.4-74.03-60.16-156.69-78.9-160.58-79.76-1.14-.25-2.29-.38-3.44-.38-4.2 0-8.29 1.66-11.32 4.7A15.986 15.986 0 0 0 .38 168.8c.88 3.97 20.68 90.52 84.93 165.64-44.39 8.92-75.21 24.99-76.78 25.81a16.003 16.003 0 0 0-.02 28.29c2.45 1.29 60.76 31.72 133.49 31.72 6.14 0 11.96-.1 17.5-.31-11.37 22.23-16.52 38.31-16.81 39.22-1.8 5.68-.29 11.89 3.91 16.11a16.019 16.019 0 0 0 16.1 3.99c1.83-.57 37.72-11.99 77.3-39.29V504c0 4.42 3.58 8 8 8h16c4.42 0 8-3.58 8-8v-64.01c39.58 27.3 75.47 38.71 77.3 39.29a16.019 16.019 0 0 0 16.1-3.99c4.2-4.22 5.71-10.43 3.91-16.11-.29-.91-5.45-16.99-16.81-39.22 5.54.21 11.37.31 17.5.31 72.72 0 131.04-30.43 133.49-31.72 5.24-2.78 8.52-8.22 8.51-14.15-.01-5.94-3.29-11.39-8.53-14.15z">
                        </path>
                      </svg>
                    </label>
                  <?php } ?>
                </div>
              <?php } else if ($field['type'] === 'textarea') { ?>
                <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label']; ?></label>
                <textarea name="<?php echo $field_name; ?>" class="form-control"
                value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder=""></textarea>
              <?php } else { ?>
                  <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label']; ?></label>
                  <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control"
                  value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="">
              <?php } ?>
            </div>
            </div>
          <?php } ?>
          <?php if (isset($product_id)) { ?>
            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
          <?php } ?>
          <?php if (isset($news_id)) { ?>
            <input type="hidden" name="news_id" value="<?php echo $news_id; ?>">
          <?php } ?>
          <div class="modal_btns">
            <button type="submit" alt="<?php echo $button_submit; ?>" title="" rel="" onclick="sendReview(event)" class="cpt_product_add2cart_button green_button" id="submit_review">
              <?php echo $button_submit; ?>
            </button>
          </div>
        </div>
      <?php } ?>
    </form>
  </div>
</div>
