<div class="modal-content">
  <div class="modal-header">
    <span></span>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <img src="/catalog/view/theme/old/images/close.svg">
    </button>
  </div>
  <div class="modal-body message" style="">
  <p class="title"><?php echo $text_success; ?></p>
  <div class="d-flex justify-content-between align-items-start flex-wrap">
    <div class="product_info_name">
      <span class="cpt_product_name"><?php echo $name; ?></span>
    </div>
    <div class="prices_block">
      <span class="totalPrice new_price"><?php echo $price; ?></span>
    </div>
  </div>
  <div class="d-flex mt-2">
    <span class="product_manufac"><?php echo $manufacturer; ?></span>
    <?php if ($sku) { ?>
      <span class="code"><?php echo $sku; ?></span>
    <?php } ?>
  </div>
  <div class="modal_product_container">
    <div class="modal_prod_img">
      <img src="<?php echo $image; ?>" alt="<?php echo $name; ?>">
    </div>
    <div class="product_info">
      <?php if ($option) { ?>
        <?php foreach ($option as $o) { ?>
          <span><?php echo $o['name']; ?>: <?php echo $o['value']; ?></span>
        <?php } ?>
      <?php } ?>
      <span><?php echo $text_quantity; ?>: <?php echo $quantity; ?></span>
    </div>
  </div>
  <div class="modal_btns">
    <button type="button" name="button" class="grey_button" data-dismiss="modal"><?php echo $button_continue_shopping; ?></button>
    <button type="button" onclick="cart.close(event);" class="green_button"><?php echo $button_to_cart; ?></button>
  </div>
</div>
</div>
