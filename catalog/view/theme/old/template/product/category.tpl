<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div class="container category">
  <div class="p-0">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php if ($description) { ?>
    <div class="toggle_xs">
      <div class="category_content">
        <?php if ($image_2) { ?>
        <img src="<?php echo $image_2; ?>" alt="<?php echo $category_name; ?>">
        <?php } ?>
        <?php echo $description; ?>
			</span>
		</span>
      </div>
    </div>
    <span class="show_all"><?php echo $full_text; ?> <i class="fas fa-chevron-down"></i></span>
    <?php } ?>
    <?php if ($products) { ?>
    <div id="cat_top_tree">
      <div id="cat_into_left_block">
        <?php foreach($categories as $category){ ?>
        <a href="<?php echo $category['href']; ?>" class="item">
          <?php if ($category['image']) { ?>
          <?php if ($category['image']['extension'] === 'svg') { ?>
          <?php echo $category['image']['value']; ?>
          <?php } else { ?>
          <img src="<?php echo $category['image']['value']; ?>" alt="<?php echo $category['name']; ?> - image" title="<?php echo $category['name']; ?>">
          <?php } ?>
          <?php } ?>
          <span><?php echo $category['name']; ?></span>
        </a>
        <?php } ?>
      </div>
    </div>
    <div>
      <div class="cpt_maincontent">
        <div class="d-flex flex-row align-items-center justify-content-between mt-3 mb-3">
          <button type="button" id="filter_button">
            <svg width="20" height="20" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect opacity="0.01" x="0.5" y="0.5" width="15" height="15" fill="#0F0F0F" stroke="#979797"></rect>
              <path fill-rule="evenodd" clip-rule="evenodd"
                d="M5.98918 14.5487V8.19077C5.98918 8.09963 5.95522 8.0121 5.89395 7.94531L0.804789 2.42463C0.127369 1.68864 0.649163 0.5 1.6488 0.5H14.2064C15.2057 0.5 15.7266 1.68977 15.0502 2.42487L9.96317 7.94467C9.90076 8.01327 9.86606 8.1022 9.86606 8.19221V12.1163C9.86606 12.3395 9.81307 12.5318 9.70489 12.7175C9.63285 12.8412 9.60491 12.8765 9.37455 13.1527L7.57496 15.2129C6.98561 15.7929 5.98918 15.375 5.98918 14.5487ZM6.63007 7.26845C6.86132 7.52011 6.98918 7.84932 6.98918 8.19077V14.3654L8.61371 12.5039C8.786 12.2972 8.81051 12.2661 8.84078 12.2142C8.86085 12.1797 8.86606 12.1608 8.86606 12.1163V8.19221C8.86606 7.85225 8.99428 7.52364 9.22566 7.26934L14.3146 1.74747C14.4013 1.65324 14.3342 1.5 14.2064 1.5H1.6488C1.52008 1.5 1.45316 1.65245 1.54031 1.74713L6.63007 7.26845Z"
                fill="#65BD00"></path>
            </svg>
            <?php echo $text_filter; ?>
          </button>
          <div id="cat_product_sort" class="d-flex flex-row  align-items-center">
            <?php echo $column_right; ?>
          </div>
          <div class="radio_plitka" data-filter-data='<?php echo $filter_data; ?>'>
            <span class="bg_plitka_icon" id="plitka_icon"><i class="fas fa-th-large"></i></span>
            <span class="bg_plitka_icon" id="spisok_icon"><i class="fas fa-th-list"></i></span>
          </div>
        </div>
        <div class="d-flex flex-row align-items-start mb-3">
          <div class="filter">
            <?php echo $column_left; ?>
          </div>
          <div id="products">
            <?php echo $products; ?>
          </div>
        </div>
        <div>
          <div class="navigator_product mb-3">
            <?php echo $pagination; ?>
          </div>
        </div>
      </div>
    </div>
    <?php } else { ?>
      <?php if($categories){ ?>
    <div id="cat_top_tree" class="mt-5 mb-5">
      <div id="cat_into_left_block">
        <?php foreach($categories as $category){ ?>
        <a href="<?php echo $category['href']; ?>" class="item">
          <?php if ($category['image']) { ?>
          <?php if ($category['image']['extension'] === 'svg') { ?>
          <?php echo $category['image']['value']; ?>
          <?php } else { ?>
          <img src="<?php echo $category['image']['value']; ?>" alt="<?php echo $category['name']; ?> - image" title="<?php echo $category['name']; ?>">
          <?php } ?>
          <?php } ?>
          <span><?php echo $category['name']; ?></span>
        </a>
        <?php } ?>
      </div>
    </div>
  <?php } else { ?>
    <div class="d-flex justify-content-center mb-3">
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 72" style="enable-background:new 0 0 100 72;width: 50%;max-width: 350px;" xml:space="preserve">
        <style type="text/css">
          .cat0 {fill-opacity: 0;}
          .cat1 {
            opacity: 6.000000e-002;
            fill: #9BB7CE;
            enable-background: new;
          }
          .cat2 {fill: #A2D855;}
          .cat3 {fill: #65BD00;}
          .cat4 {fill: #FFFFFF;}
          .cat5 {fill: none;}
          .cat6 {fill: #C9F688;}
          .cat7 {
            fill: none;
            stroke: #FFFFFF;
            stroke-width: 2.5;
            stroke-linecap: round;
            stroke-linejoin: round;
            stroke-miterlimit: 10;
          }
        </style>
        <g transform="translate(-185.000000, -1426.000000)">
          <g transform="translate(26.000000, 961.000000)">
            <g transform="translate(150.000000, 465.000000)">
              <g transform="translate(4.000000, 0.000000)">
                <rect x="0.2" y="2.1" class="cat0" width="110" height="77" />
                <path class="cat1" d="M10.4,25.2c4.2-8.5,12.2-15.7,21.5-19.6c7-3,18.4-3.4,24.4,2.9c9.1,9.5,5.2,26,1,36.8
                C53.3,55.4,43,67,29.9,69.1c-0.1,0-0.2,0-0.4,0.1c-7.4,1.2-15.3-1.2-19.9-7.5C2.3,51.7,5.3,35.6,10.4,25.2z" />
                <path class="cat1" d="M81.5,19.5c0.6-2.3,1.9-4.4,3.6-6.1c3.5-3.2,8.4-3.2,12.7-1.6c8.2,3.1,9.1,15.2,2.7,20.6
                c-3.4,2.9-8.7,2.8-12.6,1.1C82.8,31.3,80,24.8,81.5,19.5z" />
                <circle class="cat2" cx="100.4" cy="23.7" r="2.6" />
                <circle class="cat2" cx="37.5" cy="3.5" r="1.4" />
                <circle class="cat3" cx="46.8" cy="16.6" r="1.5" />
                <path class="cat2" d="M29.3,14c0,0.1-0.1,0.2-0.2,0.3c-0.8,0.2-1.6,0.4-2.3,0.8c-0.7,0.4-1.2,0.9-1.8,1.6
                c-0.7,1-1.1,2.1-1.4,3.3c0,0.1-0.1,0.2-0.3,0.2c-0.1,0-0.2-0.1-0.3-0.2c-0.5-3-2.3-5.1-5.4-5.6c-0.1,0-0.3-0.1-0.3-0.3
                c0-0.1,0.1-0.3,0.3-0.3c3.1-0.5,4.8-2.7,5.4-5.7c0-0.1,0.1-0.2,0.3-0.2c0.1,0,0.2,0.1,0.3,0.2c0.2,1.2,0.7,2.4,1.4,3.4
                c0.7,1,1.8,1.7,2.9,2c0.4,0.1,0.7,0.2,1.1,0.3C29.3,13.8,29.4,13.9,29.3,14z" />
                <path class="cat3" d="M29.3,14c0,0.1-0.1,0.2-0.2,0.3c-0.8,0.2-1.6,0.4-2.3,0.8c-0.7,0.4-1.2,0.9-1.8,1.6
                c-0.4-0.7-1.1-1.4-1.7-1.9c-0.1-0.1-0.2-0.2-0.3-0.2c-0.5-0.4-0.4-0.5,0.2-0.9c0.8-0.5,1.4-1.2,1.9-2c0.7,1,1.8,1.7,2.9,2
                c0.4,0.1,0.7,0.2,1.1,0.3C29.3,13.8,29.4,13.9,29.3,14z" />
                <circle class="cat2" cx="97.3" cy="3.5" r="1.4" />
                <path class="cat2" d="M87.2,14c0,0.1-0.1,0.1-0.1,0.2c-0.5,0.1-1.1,0.3-1.5,0.5c-0.5,0.2-0.8,0.6-1.2,1.1
                c-0.5,0.6-0.8,1.4-0.9,2.2c0,0.1-0.1,0.1-0.2,0.1S83.2,18,83.1,18c-0.4-2-1.5-3.4-3.6-3.8c-0.1,0-0.2-0.1-0.2-0.2
                c0-0.1,0.1-0.2,0.2-0.2c2.1-0.4,3.2-1.8,3.6-3.8c0-0.1,0.1-0.1,0.2-0.1s0.1,0.1,0.2,0.1c0.1,0.8,0.5,1.6,0.9,2.3
                c0.5,0.6,1.2,1.1,1.9,1.4c0.2,0.1,0.5,0.1,0.7,0.2C87.2,13.9,87.3,13.9,87.2,14z" />
                <path class="cat3" d="M87.2,14c0,0.1-0.1,0.1-0.1,0.2c-0.5,0.1-1.1,0.3-1.5,0.5c-0.5,0.2-0.8,0.6-1.2,1.1
                c-0.3-0.5-0.7-0.9-1.1-1.3c-0.1-0.1-0.1-0.1-0.2-0.1c-0.4-0.2-0.3-0.4,0.1-0.6c0.5-0.4,0.9-0.8,1.2-1.4c0.5,0.6,1.2,1.1,1.9,1.4
                c0.2,0.1,0.5,0.1,0.7,0.2C87.2,13.9,87.3,13.9,87.2,14z" />
                <path class="cat1" d="M95.4,71.5H62.5c-9.1,0-11.2-13.4-7.7-19.7c2.2-3.8,5.4-7,9.1-9.4c4.7-3,10.6-4,16.1-4.4
                c4.6-0.4,9.6-0.1,13.8,2.1c10.5,5.5,8.6,18.6,4,27.4C97.1,68.8,96.3,70.2,95.4,71.5z" />
              </g>
            </g>
          </g>
        </g>
        <g>
          <path class="cat4" d="M83.2,69.9L73.3,60c-1-1-1.9-1.7-0.7-2.9s1.9-0.3,2.9,0.7l9.9,9.9c1,1,1.5,2,0.7,2.9
          C85.2,71.5,84.2,70.9,83.2,69.9z" />
        </g>
        <g>
          <path class="cat5" d="M25.2,59.9c0,0,0-0.1,0-0.1L25.2,59.9L25.2,59.9z" />
          <path class="cat3" d="M32.5,26.9c-1.6-1.3-3.4-2.4-4.2-2.8l-0.1-0.1c-1.2-0.6-2.8,0.3-2.9,1.7l0,0.9c0,0.2,0,0.4,0,0.6
          c0,1.1,0.1,2.2,0.2,3.4c0.4,3.6,1.4,7.1,3.2,10.3c0.6,1,1.2,1.9,2.1,3.2c0.4,0.6,1.2,0.7,1.7,0.3c0.6-0.4,0.7-1.2,0.3-1.7l-0.4-0.6
          c-0.7-0.9-1.1-1.6-1.5-2.3c-1.6-2.9-2.5-6.1-2.9-9.4c-0.1-1.1-0.2-2.2-0.2-3.1l0-0.6c0.8,0.5,2,1.3,3.1,2.2c2.4,1.9,4.5,4.3,6,6.9
          l1.7-2.1C37,31.1,34.9,28.9,32.5,26.9z" />
          <path class="cat6" d="M36,55.3c-0.7,0.7-1.5,1.3-2.4,1.8c-1.8,1-3.8,1.6-5.9,1.9c-0.7,0.1-1.4,0.2-2,0.2l-0.1,0
          c0.2-0.5,0.5-1,0.8-1.3c0.9-1.2,2-2.4,3.7-3.3l0.4-0.2l0.2-0.1c0,0,0.1,0,0.1-0.1l0.7-0.3l0.3-0.1c0,0,0.1,0,0.1-0.1
          c1.1-0.6,0.7-2.4-0.6-2.3c-1.2,0-2.6-0.1-3.9-0.2c-1.4-0.3-2.7-0.8-3.9-1.4c-1.7-0.9-3.2-2-4.5-3.2c-0.4-0.3-0.7-0.7-1-1l-0.2-0.2
          c-0.1-0.1-0.1-0.1-0.2-0.2c1.5-0.5,5.3-1.1,9-0.5c0.7,0.1,1.3-0.4,1.4-1.1c0.1-0.7-0.4-1.3-1.1-1.4c-4.6-0.7-9.4,0.1-10.9,1
          c-0.5,0.3-1,0.8-1.1,1.4c-0.2,0.7,0,1.3,0.4,1.8l0.6,0.7c0.4,0.5,0.9,0.9,1.4,1.4c1.5,1.4,3.2,2.6,5.1,3.6c1.4,0.7,3,1.3,4.5,1.7
          l0.1,0l0.2,0c-1.1,0.9-2,1.8-2.7,2.9c-0.4,0.6-0.7,1.1-0.9,1.6l-0.2,0.4L23,58.7c0,0,0-0.1,0.1-0.1l0,0c0,0.1-0.1,0.1-0.1,0.2
          c-0.3,0.6-0.4,1.4,0,2c0.4,0.7,1.1,1.1,1.9,1.1l0,0l0.3,0l0.6,0c0.7,0,1.5-0.1,2.3-0.2c2.4-0.4,4.7-1.1,6.8-2.2
          c1.1-0.6,2.1-1.3,2.9-2.2c0.5-0.5,0.5-1.3,0-1.8C37.3,54.9,36.5,54.8,36,55.3z M25.2,59.9L25.2,59.9l0-0.1
          C25.3,59.8,25.2,59.8,25.2,59.9z" />
        </g>
        <path class="cat3" d="M64.8,45.2c-0.3-0.9-1-1.6-1.9-1.8l0.1,0c0.2,0-1.7-0.6-3-0.8c-1.6-0.3-3.2-0.3-5-0.1l0.2-0.2
        c1.3-1.8,2.2-3.7,2.8-5.8c0.8-2.5,1.2-5,1.3-7.5c0-0.6,0-1.2,0-1.8l0-1.2c0-0.1,0-0.1,0,0c0-0.1,0-0.2,0-0.3c0-0.8-0.4-1.5-1-2
        c-0.1-0.1-0.2-0.1-0.3-0.2c-0.7-0.4-1.4-0.4-2.2-0.2c-2.5,0.7-8.3,5.1-11.9,10.8c-0.2,0.4-0.5,0.8-0.7,1.2c-0.2-0.9-0.4-1.7-0.6-2.5
        c-0.3-1.1-0.7-2.3-1.2-3.3l-1.8,4.1c0.5,2.1,1,5.2,1,7.5c0,1.7,2.4,2.1,3,0.5c0.7-1.8,1.6-3.7,2.9-5.8c2.9-4.6,7.5-8.3,9.6-9.3
        l0,1.1l0,0.6c0,0.3,0,0.6,0,0.8c-0.1,2.2-0.4,4.4-1.2,6.6c-0.6,1.7-1.4,3.4-2.4,4.9c-1,1.3-2.1,2.5-3.2,3.6
        c-1.3,1.2,0.1,3.2,1.7,2.5c0.1,0,0.2-0.1,0.3-0.1l0.4-0.1c0.2-0.1,0.4-0.1,0.9-0.3c2.4-0.7,4.7-0.8,6.7-0.4c0.5,0.1,1,0.2,1.5,0.3
        l0.4,0.1l-0.7,0.6c-0.5,0.4-1,0.8-1.4,1.1c-2.1,1.5-4.4,2.8-6.9,3.5c-1.2,0.3-2.5,0.5-3.7,0.5c-0.9,0-1.5,0.7-1.5,1.6
        c0,0.9,0.7,1.5,1.6,1.5c1.5,0,3-0.2,4.5-0.7c2.9-0.8,5.5-2.2,7.9-4l0.5-0.4c1.3-1,2.7-2.3,2.4-2c0.1-0.1,0.2-0.2,0.3-0.3
        C64.8,47,65.1,46.1,64.8,45.2z M56.2,25.8l0,0.1C56.2,25.9,56.2,25.9,56.2,25.8L56.2,25.8z" />
        <path class="cat6" d="M47.9,60l-3-7c-0.3-0.7-1.2-1.1-1.9-0.8c-0.7,0.3-1.1,1.2-0.8,1.9l1.6,4.2
        c0.3,0.7,2.2,1.7,3,1.3C47.5,59.5,48.2,60.8,47.9,60z" />
        <path class="cat7" d="M66.2,62.6c-2.6,1.1-5.4,1.7-8.3,1.7C46,64.3,36.5,54.7,36.5,43S46,21.6,57.8,21.6S79.2,31.2,79.2,43
        c0,5.8-2.1,10.5-5.9,14.4" />
      </svg>
    </div>
    <div class="h3 text-center mb-5" style="color:#fff"><?php echo $text_empty; ?></div>
    <?php } ?>
    <?php } ?>
  </div>
  <?php if ($description_2) { ?>
    <div class="category_price_table">
      <?php echo $description_2; ?>
    </div>
  <?php } ?>
  <div style="display: none" class="hed_link">

    <?php echo $lincks_cat; ?>
  </div>
</div>

<div class="modal fade " id="modalmessage_pre" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" >
  <div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content">
      <div class="modal-header">
        <p class="modal-title" id="ModalCenterTitle"><?=$entry_pre_order?></p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="/catalog/view/theme/old/images/close.svg">
        </button>
      </div>
      <div class="modal-body" id="product">
        <form method="post" id="product_form" action="https://errors-seeds.com.ua/index.php?route=module/notification_request/saveRequest" name="notification_request">
          <p class="prd_out_of_stock"><?=$send_title_hed?></p>
          <div class="form_stock">
            <div class="d-flex flex-row justify-content-between flex-wrap">

              <div class="row form-field">
                <label for="telephone" class="for_input"><?=$entry_tel?></label>
                <input type="text" name="telephone" class="form-control"
                       value="" id="telephone" placeholder="" >
              </div>
              <div class="row form-field">
                <label for="email" class="for_input">Email</label>
                <input type="text" name="email" class="form-control" value="" id="email" placeholder="">
              </div>
            </div>
            <div class="d-flex flex-row justify-content-between flex-wrap">
              <div class="form-group">
                <label for="quantity" class="for_input"><?=$send_quantity_prod?></label>
                <div class="input-group input-date" id="quantity">
                  <input type="button" name="minus_sign" class="form-control" value=" - " placeholder="" onclick="plus(this, '-1')">
                  <input type="text" name="quantity" class="form-control" value="1" placeholder="" data-quantity="1" onchange="recount(this)">
                  <input type="button" name="plus_sign" class="form-control" value=" + " placeholder="" onclick="plus(this, '1')">
                </div>
              </div>

            </div>
            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
            <input type="hidden" name="pre_order" value="1">
            <button type="submit" alt="<?=$sender_text?> " title="" rel=""
                    onclick="sendNotificationRequestCat(event)"
                    class="green_button cpt_product_add2cart_button">
              <?=$sender_text?> </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>

  function getModalS(e,p){
    e.preventDefault()
    console.log(1);
    setTimeout(function () {
    $('#modalmessage_pre').show()
    $('#modalmessage_pre').addClass('show')
    $('body').addClass('modal-open')
    $('body').css('padding-right','17px')
      $('#modalmessage_pre input[name=product_id]').val(p)
    },200)
    $( ".footer" ).append('<div class="modal-backdrop fade show"></div>');
    //<div class="modal-backdrop fade show"></div>
  }
  $('#modalmessage_pre .close').on('click',function (){
    console.log(2);
      $('body').removeClass('modal-open')
      $('body').removeAttr('style')
      $('#modalmessage_pre').removeClass('show')
      $('.modal-backdrop').remove()
      $('#modalmessage_pre').hide()


  })
  $(document).on('click touchstart','.modal-open',function (e){
// console.log(5);
//     var div = $("#modalmessage_pre .modal-content");
//     if (!div.is(e.target)&& div.has(e.target).length === 0) {
//       $('body').removeClass('modal-open')
//       $('body').removeAttr('style')
//       $('#modalmessage_pre').removeClass('show')
//       $('.modal-backdrop').remove()
//       $('#modalmessage_pre').hide()
//     }
  })


  function sendNotificationRequestCat(event) {
    event.preventDefault();
    var data = $("#product_form").serializeArray();
    $.ajax({
      url: '/index.php?route=module/notification_request/saveRequest',
      type: 'post',
      data: data,

      success: function (json) {
        if (json.errors) {
          $('.cpt_product_add2cart_button').prop('disabled', false);
          $('.form_stock').removeClass('disabled');
          $.map(json.errors, function(e, t) {
            $("#product #" + t).parent().append('<label for="' + t + '" class="error">&#10008; ' + e + '</label>');
            $("#product #" + t).on("click", function() {
              $("label[for=" + t + "].error").remove();
            });
          });
        } else if (json.message) {
          var timeToHide = 400;
          $('.form_stock').animate({height:'toggle'},timeToHide);
          setTimeout(function(){
            $('.form_stock').removeClass('disabled');
            message.showEmbedded({'type' : json.message.type, 'text' : json.message.text, 'container' : $('.form_stock')});
            $('.form_stock').animate({height:'toggle'},timeToHide);
          }, timeToHide);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {

        console.log('ERRORS: ' + textStatus);
      }
    });
    return false;
  }


</script>


<?php echo $content_bottom; ?>
<?php echo $footer; ?>
