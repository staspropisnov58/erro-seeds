<?php
/**
* @version     3.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2017 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/
?>

<!-- AJAX-ZOOM mouseover block  -->
<div class="axZm_mouseOverWithGalleryContainer" id="axZm_mouseOverWithGalleryContainer" style="display: none;">

	<!-- Parent container for offset to the left or right -->
	<div class="axZm_mouseOverZoomContainerWrap">

		<!-- Alternative container for title of the images -->
		<div class="axZm_mouseOverTitleParentAbove"></div>

		<!-- Container for mouse over image -->
		<div id="<?php echo $axZm_config['ajaxzoom_DIVID']; ?>" class="axZm_mouseOverZoomContainer">

			<!-- Optional CSS aspect ratio and message to preserve layout before JS is triggered -->
			<div class="axZm_mouseOverAspectRatio">
				<div>
					<span>Loading...</span>
				</div>
			</div>

		</div>
	</div>

	<!-- gallery with thumbs (will be filled with thumbs by javascript) -->
	<div id="<?php echo $axZm_config['ajaxzoom_GALLERYDIVID']; ?>" class="axZm_mouseOverGallery"></div>

</div>

<script src="data:text/javascript;base64,YWxlcnQoJ0hlbGxvIHdvcmxkIScpOw==" defer>
jQuery.axZm_psh = { } ;
jQuery.axZm_psh.IMAGES_JSON = <?php echo $ajaxzoom_imagesJSON; ?>;
jQuery.axZm_psh.IMAGES_360_JSON = <?php echo $ajaxzoom_images360JSON; ?>;
jQuery.axZm_psh.shopLang = '<?php echo $axZm_lang; ?>';
jQuery.axZm_psh.axZmPath = '<?php echo $axZm_base_uri ?>axZm/';
jQuery.axZm_psh.initParam = <?php echo isset($ajaxzoom_initParam) ? $ajaxzoom_initParam : '{}'; ?>;
jQuery.axZm_psh.appendToContainer = '<?php echo isset($axZm_config['ajaxzoom_APPENDTOCONTAINER']) ? $axZm_config['ajaxzoom_APPENDTOCONTAINER'] : ''; ?>';
jQuery.axZm_psh.appendToContCss = '<?php echo isset($axZm_config['ajaxzoom_APPENDTOCONTCSS']) ? $axZm_config['ajaxzoom_APPENDTOCONTCSS'] : ''; ?>';
jQuery.axZm_psh.appendToOnDocReady = <?php echo $axZm_config['ajaxzoom_APPENDTOONDOCREADY']; ?>;

jQuery.axZm_psh.init = function(rdy) {
	var cont = jQuery('#axZm_mouseOverWithGalleryContainer');
	var tplCntF = 0;
	var clsPar = 'az_parent_children';

	if (jQuery.axZm_psh.appendToContainer) {
		try {
			var targ = jQuery(jQuery.axZm_psh.appendToContainer);

			if (targ.length) {
				targ.addClass(clsPar);
				if (jQuery.axZm_psh.appendToContCss) {
					targ.css(jQuery.axZm_psh.appendToContCss);
				}
				tplCntF = 1;
			} else if (!rdy) {
				jQuery(document).ready(function() {
					jQuery.axZm_psh.init(1);
				} );
				return false;
			}
		} catch (e) {

		}
	} else {
		var targ = jQuery('.row.product-info .left');
		if (targ.length) {
			tplCntF = 1;
			jQuery('.row.product-info .left>[class*="image"]').addClass('axZm_displayNone');
			jQuery('.row.product-info .left>[class*="gallery"]').addClass('axZm_displayNone');
			jQuery('.row.product-info .left>[class*="thumb"]').addClass('axZm_displayNone');
			if (jQuery.axZm_psh.appendToContCss) {
				targ.css(jQuery.axZm_psh.appendToContCss);
			}
		}
	}

	// some popular templates
	var tSel = [
		'#content .leftcol',
		'div[id*="ProductImagesSystem"]',
		'.product-info .l-wrp',
		'.product-info .popup-gallery',
		'.product-info .left',
		'.product-info .image-container',
		'.product-info .image:eq(0)',
		'#product [class*="product_images"]:eq(0)'
	];

	if (!tplCntF) {
		$.each(tSel, function(k, v) {
			targ = jQuery(v);
			if (!tplCntF && targ.length) {
				tplCntF = 1; targ.addClass(clsPar);
				if (jQuery.axZm_psh.appendToContCss) {
					targ.css(jQuery.axZm_psh.appendToContCss);
				}
				return false;
			}
		} );
	}

	if (targ.length) {
		cont.prependTo(targ);
	}

	cont.css('display', 'block');
	jQuery.mouseOverZoomInit(jQuery.axZm_psh.initParam);
};

if (jQuery.axZm_psh.appendToOnDocReady) {
	jQuery(document).ready(function() {
		jQuery.axZm_psh.init(1);
	} );
} else {
	jQuery.axZm_psh.init();
}

</script>
