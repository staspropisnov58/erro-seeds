<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div class=" container">
  <div class="main_content_text">
    <div class="h1">
      <h1><?php echo $manufacturer_name; ?></h1>
    </div>
    <?php echo $description; ?>

    <div class="left_column">
      <div class="category">
        <div class="cpt_maincontent">
          <div class="back-cat ">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if (isset($breadcrumb['href'])) { ?>
            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
            <?php } else { echo $breadcrumb['text'] . '  '; }
            } ?>
          </div>
          <div class="clearfix" id="cat_top_tree">
            <div id="cat_into_left_block">
              <p></p>
              <?php if(!empty($categories)){ ?>
              <?php foreach($categories as $category){ ?>
              <p><a href="<?php echo $category['href']; ?>"> <?php echo $category['name']; ?>(<?php echo $category['total']; ?>)</a></p>
              <?php } ?>
              <?php } ?>
              <p></p>
            </div>
          </div>
          <div class="d-flex flex-row align-items-center justify-content-between">
            <div id="cat_product_sort" class="d-flex flex-row  align-items-center">
              <?php echo $column_right; ?>
              <div class="navigation_product">
                <?php echo $pagination; ?>
                <?php if($all_products > $limit) {?>
                <a href="<?=$all;?>" class="all-vis-tovar"><?php echo $text_show_all; ?></a>
                <?php } ?>
              </div>
            </div>

            <div class="radio_plitka" data-filter-data='<?php echo $filter_data; ?>'>
              <span class="bg_plitka_icon" id="plitka_icon">
                <img src="images/bg_plitka.png">
              </span>
              <span class="bg_plitka_icon" id="spisok_icon">
                <img src="images/bg_spisok.png">
              </span>
            </div>
          </div>
          <div class="hr"></div>
          <div class="d-flex flex-row">
            <div class="filter">
              <?php echo $column_left; ?>
            </div>
            <div style="width: 1000px;" id="products">
              <?php echo $products; ?>
            </div>
          </div>
          <div>
            <div class="navigator_product">
              <?php echo $pagination; ?>
              <?php if($all_products > $limit) {?>
              <a href="<?=$all;?>" class="all-vis-tovar"><?php echo $text_show_all; ?></a>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
