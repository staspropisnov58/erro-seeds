<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div class="container">
  <div class="main_content_text" id="product_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <div class="block_product" id="product">
      <div id="prddeatailed_container">
        <div class="product_top">
          <div>
            <div class="cpt_product_name">
              <h1><?=$heading_title?></h1>
            </div>
          </div>
          <div>
            <div>
          <span class="productCodeLabel"><?php echo $text_manufacturer; ?></span>
              <span class="productCode"><?php echo $manufacturer; ?></span>
            </div>
            <div class="productCodeDiv">
              <!-- <span class="productCodeLabel">(aрт.: 123456789)</span> -->
            </div>
          </div>
        </div>
        <div class="product_center">
          <div class="cpt_product_images">
            <a name="anch_current_picture"></a>
            <div class="product_img_page_product owl-product-img">
      				<?php echo $ajaxzoom; ?>
              <?php foreach ($images as $key => $image) { ?>
              <div class="item <?php if ($key === 0) { ?>active<?php } ?>">
                <a target="_blank" href="<?php echo $image['popup'];?>" data-lightbox="example-set">
                    <img id="img-current_picture" border="0" class="owl-lazy"
                    <?php if ($key === 0) { ?>
                      src="<?php echo $image['thumb'];?>"
                      data-src="<?php echo $image['thumb'];?>"
                    <?php } else { ?>
                      data-src="<?php echo $image['thumb'];?>"
                      src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 3'%3E%3C/svg%3E"
                    <?php } ?>
                    alt="<?php echo $alt_image; ?>" title="<?php echo $title_image; ?>" style="opacity: 1">
                  </a>
                  <?php if ($stickers) { ?>
                  <div class="stickers-block">
                    <?php foreach ($stickers as $sticker){ ?>
                    <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
                    <?php } ?>
                  </div>
                  <?php } ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="specific_block">
            <div class="item-rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($rating < $i) { ?>
                  <span>
                    <svg viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M0.162695 5.23494C0.115422 5.22136 0.0645553 5.23619 0.0319787 5.27304C-0.000597835 5.30989 -0.00907458 5.36219 0.0101953 5.40744C0.910195 7.40494 2.4977 9.54994 4.4977 10.7499C3.22494 10.7317 1.96667 11.0217 0.830195 11.5949C0.778899 11.6299 0.7482 11.6879 0.7482 11.7499C0.7482 11.812 0.778899 11.87 0.830195 11.9049C2.1277 12.5424 3.6927 12.9049 5.1477 12.6799C4.82674 13.1911 4.60614 13.7587 4.4977 14.3524C4.49131 14.3995 4.51064 14.4464 4.54835 14.4754C4.58605 14.5043 4.63636 14.5108 4.6802 14.4924C5.6127 14.1999 6.5627 13.5899 6.9952 12.6799V15.9104C6.9952 16.0484 7.10712 16.1604 7.2452 16.1604C7.38327 16.1604 7.4952 16.0484 7.4952 15.9104V12.6799C7.9277 13.5874 8.8777 14.1999 9.81019 14.4924C9.85403 14.5108 9.90434 14.5043 9.94204 14.4754C9.97975 14.4464 9.99908 14.3995 9.99269 14.3524C9.88502 13.7589 9.66527 13.1913 9.34519 12.6799C10.7977 12.9074 12.3627 12.5424 13.6627 11.9049C13.714 11.87 13.7447 11.812 13.7447 11.7499C13.7447 11.6879 13.714 11.6299 13.6627 11.5949C12.527 11.022 11.2696 10.7321 9.99769 10.7499C11.9777 9.55744 13.5802 7.41494 14.4977 5.40744C14.517 5.36219 14.5085 5.30989 14.4759 5.27304C14.4433 5.23619 14.3925 5.22136 14.3452 5.23494C12.1727 5.94494 9.81769 7.34994 8.4177 9.26244H8.4052C9.1927 6.31244 8.5777 2.80244 7.3727 0.0774412C7.35334 0.0325185 7.30911 0.00341797 7.2602 0.00341797C7.21128 0.00341797 7.16705 0.0325185 7.1477 0.0774412C5.9402 2.82744 5.3302 6.32744 6.1152 9.26244H6.1027C4.7027 7.36744 2.3552 5.94994 0.162695 5.23494Z" fill="#686D74"/>
                    </svg>
                  </span>
                <?php } else { ?>
                  <span class="active">
                    <svg viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M0.162695 5.23494C0.115422 5.22136 0.0645553 5.23619 0.0319787 5.27304C-0.000597835 5.30989 -0.00907458 5.36219 0.0101953 5.40744C0.910195 7.40494 2.4977 9.54994 4.4977 10.7499C3.22494 10.7317 1.96667 11.0217 0.830195 11.5949C0.778899 11.6299 0.7482 11.6879 0.7482 11.7499C0.7482 11.812 0.778899 11.87 0.830195 11.9049C2.1277 12.5424 3.6927 12.9049 5.1477 12.6799C4.82674 13.1911 4.60614 13.7587 4.4977 14.3524C4.49131 14.3995 4.51064 14.4464 4.54835 14.4754C4.58605 14.5043 4.63636 14.5108 4.6802 14.4924C5.6127 14.1999 6.5627 13.5899 6.9952 12.6799V15.9104C6.9952 16.0484 7.10712 16.1604 7.2452 16.1604C7.38327 16.1604 7.4952 16.0484 7.4952 15.9104V12.6799C7.9277 13.5874 8.8777 14.1999 9.81019 14.4924C9.85403 14.5108 9.90434 14.5043 9.94204 14.4754C9.97975 14.4464 9.99908 14.3995 9.99269 14.3524C9.88502 13.7589 9.66527 13.1913 9.34519 12.6799C10.7977 12.9074 12.3627 12.5424 13.6627 11.9049C13.714 11.87 13.7447 11.812 13.7447 11.7499C13.7447 11.6879 13.714 11.6299 13.6627 11.5949C12.527 11.022 11.2696 10.7321 9.99769 10.7499C11.9777 9.55744 13.5802 7.41494 14.4977 5.40744C14.517 5.36219 14.5085 5.30989 14.4759 5.27304C14.4433 5.23619 14.3925 5.22136 14.3452 5.23494C12.1727 5.94494 9.81769 7.34994 8.4177 9.26244H8.4052C9.1927 6.31244 8.5777 2.80244 7.3727 0.0774412C7.35334 0.0325185 7.30911 0.00341797 7.2602 0.00341797C7.21128 0.00341797 7.16705 0.0325185 7.1477 0.0774412C5.9402 2.82744 5.3302 6.32744 6.1152 9.26244H6.1027C4.7027 7.36744 2.3552 5.94994 0.162695 5.23494Z" fill="#65BD00"/>
                    </svg>
                  </span>
              <?php } ?>
            <?php } ?>
            </div>
            <?php if ($attribute_groups) { ?>
              <?php foreach ($attribute_groups as $group) { ?>
              <div>
                <table>
                  <?php foreach ($group['attribute'] as $key => $attribute) { ?>
                  <tr>
                    <td>
                      <?php if ($attribute['image']) { ?>
                      <?php if ($attribute['image']['extension'] === 'svg') { ?>
                      <?php echo $attribute['image']['value']; ?>
                      <?php } else { ?>
                      <img src="<?php echo $attribute['image']['value']; ?>" alt="<?php echo $attribute['name']; ?>" title="<?php echo $attribute['name']; ?>">
                      <?php } ?>
                      <?php } ?>
                      <?php echo $attribute['name']; ?>:</td>
                    <td>
                      <?php if($attribute['filter_link']){ ?>
                      <a href="<?php echo $attribute['filter_link'];?>">
                        <?php echo $attribute['text']; ?> </a>
                      <?php }else{ ?>
                      <?php echo $attribute['text']; ?>
                      <?php } ?>
                    </td>
                  </tr>
                  <?php } ?>
                </table>
              </div>
              <?php } ?>
            <?php } ?>
              <?php echo $recomended_products; ?>
          </div>
          <div class="product_descript_right">
            <div class="cpt_product_price">
              <div>
                <?php	 if(!empty($price)) {
                 if(!empty($special) ) {?>
                  <span id="totalprice" class="totalPrice price_sale new_price"><?=$special?></span>
                  <span class="regularPrice price_sale old_price"><?=$price?></span>
                  <?php } else { ?>
                  <span id="totalprice" class="totalPrice new_price"><?=$price?></span>
                  <?php } ?>
                  <?php } ?>
              </div>

            </div>
            <?php	 if(!empty($price)) {
             if(!empty($special) ) {?>
              <p id="saving_text">
                <?php echo $text_economy; ?>&nbsp;<span class="saving_price"><?php echo $saving; ?></span>
              </p>
              <?php } ?>
              <?php } ?>
            <form method="post" class="<?php if ($quantity > 0) { ?> add_to_cart <?php } ?>" id="product_form" <?php if ($form) { ?> action="
              <?php echo $form['action']; ?>" name="
              <?php echo $form['name']; ?>"
              <?php } ?>>
              <noscript>
                    <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
                </noscript>
              <?php if ($quantity > 0) { ?>
              <?php if ($options) { ?>
              <div class="product_options">
                <?php foreach ($options as $option) { ?>
                <?php if (is_array($option)) { ?>
                <div class="cpt_product_params_selectable">
                  <span><?php echo $option['name']; ?>:</span>

                  <div class="option_select">
                    <?php $checked = 'checked'; ?>
                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                    <input data-option="<?php echo $option_value['name']; ?>" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" <?php if($option_value[ 'price_dev']>0) { ?> data-price="<?php echo $option_value['price']; ?>" <?php }else{ ?> data-price="<?php echo $option_value['price']; ?>"<?php } ?>
                      <?php if($option_value[ 'old_price']) { ?>data-old-price="
                    <?php echo $option_value['old_price']; ?>"
                    <?php } ?>
                    <?php if($option_value[ 'price_dev']>0) { ?> data-old-price="
                    <?php echo $option_value['price_dev']; ?>"
                    <?php } ?>
                    <?php echo $checked; ?>
                    <?php if ($option_value['trigger_image']) { ?>data-image="
                    <?php echo $option_value['trigger_image']; ?>"
                    <?php } ?> onchange="addOption(this)">

                    <label  for="<?php echo $option_value['product_option_value_id']; ?>">
                        <?php if ($option_value['image']) { ?>
                          <?php if ($option_value['image']['extension'] === 'svg') { ?>
                            <?php echo $option_value['image']['value']; ?>
                          <?php } else { ?>
                            <img src="<?php echo $option_value['image']['value']; ?>" alt="<?php echo $option_value['name']; ?>" title="<?php echo $option_value['name']; ?>">
                          <?php } ?>
                        <?php } else { ?>
                          <?php echo $option_value['name']; ?>
                        <?php } ?>
                        </label>
                    <?php $checked = ''; ?>
                    <?php  }?>
                  </div>

                </div>
                <?php } ?>
                <?php } ?>
                <div class="d-flex flex-wrap justify-content-between align-items-end">
                  <div class="product_quantity">
                    <span><?php echo $entry_qty; ?>:</span>
                    <div>
                      <input type="button" onclick="plus(this, '-1')" value=" - " class="count-icon-m">
                      <input name="quantity" id="acpro_inp_1777" class="product_qty" type="text" data-quantity="1" value="1" onchange="recount(this)">
                      <input type="button" onclick="plus(this, '1')" value=" + " class="count-icon-p">
                    </div>
                  </div>
                  <!-- <p class="advantage"></p> -->
                </div>
              </div>

              <?php } ?>
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
              <input type="hidden" name="price" value="<?php echo $price; ?>">
              <button type="submit" data-toggle="model" id="button-by-product_id-<?php echo $product_id; ?>" onclick="cart.add(<?php echo $product_id; ?>, 1, event)" class="cpt_product_add2cart_button green_button">
                <?php echo $button_cart; ?>
              </button>
              <?php } else { ?>

              <p data-test="2" class="prd_out_of_stock">
                <?php echo $stock; ?>
              </p>
              <p data-test="3" class="prd_out_of_stock">
                <?php echo $form['title']; ?>
              </p>
           <?php if($order_pre == 1) { ?>
<form id="pre_orde">
              <div class="form_stock">
                  <div class="d-flex flex-row justify-content-between flex-wrap">
                      <div class="row form-field">
                          <label for="telephone" class="for_input"><?=$entry_tel?></label>
                          <input type="text" name="telephone" class="form-control"
                                 value="" id="telephone" placeholder="" >
                      </div>
                      <div class="row form-field">
                          <label for="email" class="for_input">Email</label>
                          <input type="text" name="email" class="form-control"
                                 value="" id="email" placeholder="" >
                      </div>
                  </div>
                  <div class="d-flex flex-row justify-content-between flex-wrap">
                      <div class="form-group">
                          <label for="quantity" class="for_input"><?=$entry_quantity?></label>
                          <div class="input-group input-date" id="quantity">
                              <input type="button" name="minus_sign" class="form-control" value=" - " placeholder="" onclick="plus(this, '-1')">
                              <input type="text" name="quantity" class="form-control" value="1" placeholder="" data-quantity="2" onchange="recount(this)">
                              <input type="button" name="plus_sign" class="form-control" value=" + " placeholder="" onclick="plus(this, '1')">
                          </div>
                      </div>

                  </div>
                  <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
                  <input type="hidden" name="pre_order" value="1">
                  <button type="submit" alt="Отправить" title="" rel=""
                          onclick="sendNotificationRequest(event)"
                          class="green_button cpt_product_add2cart_button">
                      <?=$entry_pre_order?> </button>
              </div>
</form>

            <?php }else{ ?>
              <?php echo $form['content']; ?>
<?php } ?>
              <?php } ?>
            </form>
            <!-- если товар снят с производства, убрать в блоке все кроме discontinued-->
            <!--<p class="discontinued">Товар снят с производства</p>-->


            <!-- для предзаказа -->
            <div style="display:none">
              <p class="prd_out_of_stock">
                Только для Украины
              </p>
              <p class="prd_out_of_stock">
                Этот товар доступен только для предзаказа. Оставьте или телефон или почту и наш менеджер обязательно свяжется с вами, когда покупка будет возможна.
                <!-- Цей товар доступний тільки для попереднього замовлення. Залиште або телефон або пошту і наш менеджер обов'язково зв'яжеться з вами, коли покупка буде можлива. -->
              </p>
              <div class="form_stock preorder">
                <div class="preorder_item">
                  <div class="input_block">
                    <div class="form-field">
                      <label class="for_input d-flex align-items-center justify-content-between">
                        <span>Оставить</span>
                        <div class="option_select preorder_select">
                          <input type="radio" name="preorder_select" value="1" id="preorder_email" checked="">
                          <label for="preorder_email">Email </label>
                          <input type="radio" name="preorder_select" value="2" id="preorder_tel">
                          <label for="preorder_tel">Телефон </label>
                        </div>
                      </label>
                      <input type="text" name="email" class="form-control js-preorder" value="" id="preorder_email_input" placeholder="example@email.com">
                      <input type="text" name="tel" class="form-control js-preorder" value="" id="preorder_tel_input" placeholder="+38(___)___-__-__" style="display:none">
                    </div>
                  </div>
                  <div class="form-group date_block">
                    <label for="date_expired" class="for_input">Ваш запрос актуален до</label>
                    <div class="input-group input-date" id="date_expired">
                      <input type="text" name="date_expired[day]" class="form-control" value="14" placeholder="">
                      <input type="text" name="date_expired[month]" class="form-control" value="09" placeholder="">
                      <input type="text" name="date_expired[year]" class="form-control" value="2020" placeholder="">
                    </div>
                  </div>
                </div>
                <div class="preorder_item">
                  <div class="cpt_product_params_selectable form-group w-auto">
                    <span class="for_input">Семян в упаковке:</span>
                    <div class="option_select">
                    <input type="radio" name="option[4405]" value="11648" id="11648" data-price="87.00 грн" checked="" onchange="addOption(this)">
                    <label for="11648">
                    1  </label>
                    <input type="radio" name="option[4405]" value="11649" id="11649" data-price="261.00 грн" onchange="addOption(this)">
                    <label for="11649">
                    3 </label>
                    <input type="radio" name="option[4405]" value="11650" id="11650" data-price="435.00 грн" onchange="addOption(this)">
                    <label for="11650">
                    5 </label>
                    <input type="radio" name="option[4405]" value="11651" id="11651" data-price="870.00 грн" onchange="addOption(this)">
                    <label for="11651">
                    10 </label>
                    <input type="radio" name="option[4405]" value="12821" id="12821" data-price="3480.00 грн" onchange="addOption(this)">
                    <label for="12821">
                    50 </label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="quantity" class="for_input">Количество товара</label>
                    <div class="input-group input-date" id="quantity">
                      <input type="button" name="minus_sign" class="form-control" value=" - " placeholder="" onclick="plus(this, '-1')">
                      <input type="text" name="quantity" class="form-control" value="1" placeholder="" data-quantity="2" onchange="recount(this)">
                      <input type="button" name="plus_sign" class="form-control" value=" + " placeholder="" onclick="plus(this, '1')">
                    </div>
                  </div>
                </div>
                <input type="hidden" name="product_id" value="2772">
                <button type="submit" alt="Отправить" title="" rel="" onclick="sendNotificationRequest(event)" class="green_button cpt_product_add2cart_button">Отправить</button>
              </div>
            </div>
            <!-- для предзаказа end -->

          </div>
        </div>
      </div>
      <div class="description"><?php echo $description; ?></div>
      <div class="reviews_and_descript">
        <div class="accordion" id="accordionExample">

            <div class="card">
              <div class="card-header" id="review">
                  <?php if ($reviews) { ?>
                  <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#review1" aria-expanded="false" aria-controls="collapseThree">
                      <?php echo $tab_review; ?> <i class="fas fa-chevron-down"></i>
                    </button>
                  </h2>
                  <?php } else { ?>
                    <div class="d-flex justify-content-between align-items-center">
                      <h2 class="mb-0"><span class="btn-link"><?php echo $tab_review; ?></span></h2>
                      <button type="button" onclick="<?php echo $write_review; ?>" class="green_button review_btn m-0"><?php echo $text_write; ?></button>
                    </div>
                  <?php } ?>
              </div>
              <div id="review1" class="collapse" aria-labelledby="review" data-parent="#accordionExample">
                <div class="card-body">
                  <div class="review_block tab_panel_block" aria-labelledby="review">
                    <p class="atten">
                      <?php echo $text_reviews_warning; ?>
                    </p>
                    <div class="review_list">
                      <?php if ($reviews) { ?>
                      <?php foreach ($reviews as $review) { ?>
                      <div class="review_item">
                        <div class="review_date">
                          <?php echo $review['author']; ?>&nbsp;<small>(<?php echo $review['date_added']; ?>)</small></div>
                        <?php if ($review['title']) { ?>
                        <h3 class="review_title"><?php echo $review['title']; ?></h3>
                        <?php } ?>
                        <div class="review_rating">
                          <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <?php if ($review['rating'] < $i) { ?>
                            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M7.47942 1.42285L9.018 4.54041C9.12499 4.75724 9.33186 4.90748 9.57117 4.9422L13.0117 5.44216C13.6144 5.5298 13.8548 6.27021 13.4189 6.69504L10.9293 9.12171C10.7563 9.29045 10.6772 9.53374 10.7182 9.77191L11.3058 13.1985C11.4088 13.7986 10.7788 14.2562 10.2399 13.9731L7.16271 12.3554C6.94873 12.243 6.69292 12.243 6.47893 12.3554L3.40178 13.9731C2.86283 14.2565 2.23283 13.7986 2.33584 13.1985L2.92344 9.77191C2.96442 9.53374 2.88531 9.29045 2.71231 9.12171L0.222754 6.69504C-0.213181 6.26992 0.0272657 5.52951 0.629949 5.44216L4.07048 4.9422C4.30979 4.90748 4.51666 4.75724 4.62365 4.54041L6.16223 1.42285C6.43141 0.876791 7.20995 0.876791 7.47942 1.42285Z" stroke="white"/>
                            </svg>
                          <?php } else { ?>
                            <svg width="15" height="15" viewBox="0 0 15 15" fill="#65bd00" xmlns="http://www.w3.org/2000/svg">
                              <path d="M7.47942 1.42285L9.018 4.54041C9.12499 4.75724 9.33186 4.90748 9.57117 4.9422L13.0117 5.44216C13.6144 5.5298 13.8548 6.27021 13.4189 6.69504L10.9293 9.12171C10.7563 9.29045 10.6772 9.53374 10.7182 9.77191L11.3058 13.1985C11.4088 13.7986 10.7788 14.2562 10.2399 13.9731L7.16271 12.3554C6.94873 12.243 6.69292 12.243 6.47893 12.3554L3.40178 13.9731C2.86283 14.2565 2.23283 13.7986 2.33584 13.1985L2.92344 9.77191C2.96442 9.53374 2.88531 9.29045 2.71231 9.12171L0.222754 6.69504C-0.213181 6.26992 0.0272657 5.52951 0.629949 5.44216L4.07048 4.9422C4.30979 4.90748 4.51666 4.75724 4.62365 4.54041L6.16223 1.42285C6.43141 0.876791 7.20995 0.876791 7.47942 1.42285Z" stroke="white"/>
                            </svg>
                          <?php } ?>
                          <?php } ?>
                        </div>
                        <div class="review_content">
                          <?php echo $review['text']; ?>
                        </div>
                      </div>
                      <?php } ?>
                      <?php } else { ?>
                      <div class="review_item">
                        <?php echo $text_no_reviews; ?>
                      </div>
                      <?php } ?>
                    </div>
                    <button type="button" onclick="<?php echo $write_review; ?>" class="green_button review_btn"><?php echo $text_write; ?></button>
                  </div>
                </div>
              </div>
            </div>
            <?php if ($seo_text) { ?>
              <div class="more-information"><?php echo $seo_text; ?></div>
            <?php } ?>
          </div>
      </div>
    </div>
    <div style="display: none" class="hed_link"><?php echo $lincks_prod; ?></div>
  </div>
</div>
<script>
  $(document).ready(function (){
if(localStorage.getItem('option')){
  let option = localStorage.getItem('option');
$('input[data-option='+option+']').trigger('click')
  delete localStorage.option;
}

      //  localStorage.setItem('option', $(this).data('option'));
// $('#pre_orde').submit(function (e){
//     e.preventDefault()
//     console.log(1)
//
//
// })


  })
</script>
<style>
  #product_page .description img
  {
    margin: auto;
  }
</style>
<?php echo $related_products; ?>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
