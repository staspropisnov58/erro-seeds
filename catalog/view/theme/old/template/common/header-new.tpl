<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<head>
  <meta charset="UTF-8">
  <base href="<?php echo $base; ?>" />
  <title>
    <?php echo $title; ?>
  </title>
  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="/logo_favicon.png">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/bootstrap.min.css" type="text/css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/slinky.min.css" type="text/css">
  <link rel="stylesheet" href="/published/publicdata/WEBASYST/attachments/SC/themes/classic/main.css" type="text/css">
  <link rel="stylesheet" href="/published/SC/html/scripts/css/general.css" type="text/css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/style-new.css" type="text/css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/checkout/style_new_checkout.css" type="text/css">
  <link rel="stylesheet" href="/published/publicdata/WEBASYST/attachments/SC/themes/classic/animate.css" type="text/css">
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>

  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>" rel="icon" />
  <?php } ?>

  <?php foreach ($links as $link) { ?>
  <?php  if($link['hreflang']){ ?>
      <link rel="<?=$link['rel']?>" hreflang="<?=$link['hreflang']?>" href="<?=$link['href']?>">
    <?php }else{ ?>
      <link href="<?=$link['href']?>" rel="<?=$link['rel']?>">
    <?php } ?>
  <?php } ?>

  <?php if ($robots) { ?>
  <meta name="robots" content="<?php echo $robots; ?>" />
  <?php } ?>
  <script type="text/javascript" src="/catalog/view/theme/old/js/jquery-3.3.1.min.js"></script>
  <?php foreach ($scripts as $script) { ?>
      <script src="<?php echo $script; ?>" defer type="text/javascript"></script>
  <?php } ?>

  <script src="/published/SC/html/scripts/js/slides.min.jquery.js" type="text/javascript"></script>
  <script type="text/javascript" src="/published/SC/html/scripts/js/jcarousellite_1.0.1.min.js"></script>
  <script type="text/javascript" src="/catalog/view/theme/old/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="/catalog/view/theme/old/js/slinky.min.js"></script>
  <script type='text/javascript'>
    (function() {
      var widget_id = '<?php echo $jivochate_code; ?>';
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = '//code.jivosite.com/script/widget/' + widget_id;
      var ss = document.getElementsByTagName('script')[0];
      ss.parentNode.insertBefore(s, ss);
    })();
  </script>
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T36VTNM');
  </script>
  <!-- End Google Tag Manager -->
  <meta name="yandex-verification" content="3940d507abba79b6" />
</head>

<body>
  <?php if($agreement_popup) { ?>
  <div class="modal top18" id="modalagreement" data-backdrop="static" data-keyboard="false">
      <div class="agr_wrap" role="document">
          <div class="wrapper" style="">
              <div class="agr_text">
                <h4 class="modal-title"><?=$agreement_popup['title']?></h4>
                  <?=$agreement_popup['description']?>
              </div>
              <div class="choise">
                  <button type="button" id="agreement-yes" class="agr_buttons"><?=$agreement_popup['button_yes']?></button>
                  <a type="button" id="agreement-no"  class="agr_buttons" href="https://www.google.com/"><?=$agreement_popup['button_no']?></a>

              </div>
          </div>
      </div>
  </div>
  <?php } ?>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "ImageObject",
      "author": "Errors Seeds",
      "contentUrl": "/published/publicdata/WEBASYST/attachments/SC/products_pictures/semena-fotoperiodichnogo-feminizirovannogo-sorta-Somango-Feminised.jpg"
    }
  </script>
  <script defer type="text/text/javascript" async src="https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"></script>

  <div class="container">
    <div class="row col-12 m-auto col-lg-10 p-0 col-12">
      <div class="header">
        <nav class="navbar navbar-dark navbar-expand-md p-0 col-12">
          <div class="col-6 col-md-4 logo_block p-0 order-2 order-md-1">
            <a href="<?php echo $home; ?>"><img src="/published/publicdata/WEBASYST/attachments/SC/themes/images/logo.png" /></a>
          </div>
          <div class="login col-1 col-sm-4 col-md-3 d-flex order-3 order-md-3 justify-content-end p-0">
            <button id="drop_user">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   viewBox="0 0 350 350" style="fill:#85bc0d; width:25px" xml:space="preserve">
                <g>
                  <path d="M175,171.173c38.914,0,70.463-38.318,70.463-85.586C245.463,38.318,235.105,0,175,0s-70.465,38.318-70.465,85.587
                    C104.535,132.855,136.084,171.173,175,171.173z"/>
                  <path d="M41.909,301.853C41.897,298.971,41.885,301.041,41.909,301.853L41.909,301.853z"/>
                  <path d="M308.085,304.104C308.123,303.315,308.098,298.63,308.085,304.104L308.085,304.104z"/>
                  <path d="M307.935,298.397c-1.305-82.342-12.059-105.805-94.352-120.657c0,0-11.584,14.761-38.584,14.761
                    s-38.586-14.761-38.586-14.761c-81.395,14.69-92.803,37.805-94.303,117.982c-0.123,6.547-0.18,6.891-0.202,6.131
                    c0.005,1.424,0.011,4.058,0.011,8.651c0,0,19.592,39.496,133.08,39.496c113.486,0,133.08-39.496,133.08-39.496
                    c0-2.951,0.002-5.003,0.005-6.399C308.062,304.575,308.018,303.664,307.935,298.397z"/>
                </g>
                </svg>
            </button>
            <div class="drop_user_menu">
              <?php if($logged != null){ ?>
              <a class="input green_button" id="trigg" href="<?php echo $account; ?>">
                <?php echo $text_account; ?>
              </a>
              <?php }else{ ?>
              <a class="input green_button" href="<?php echo $login; ?>">
                <?php echo $text_login; ?>
              </a>
              <a href="<?php echo $register; ?>" class="registration green_button">
                <?php echo $text_register; ?>
              </a>
              <?php } ?>
            </div>
            </div>
          <div class="phone_block col-md-3 order-4 p-0">
            <div class="phone1">
              <span>Бесплатно с любого оператора</span>
              <p>0-800-750-938</p>
            </div>
            <div class="phone">
              <div class="phone_number">
                <img src="/images/phone_icon.png">
                <div class="phone3">
                  <p>
                    <span>+38 093</span>
                    <!-- No Skype -->00-00-849
                  </p>
                  <p>
                    <span>+38 095</span>
                    <!-- No Skype -->00-00-849
                  </p>
                  <p>
                    <span>+38 096</span>
                    <!-- No Skype -->00-00-849
                  </p>
                </div>
              </div>
              <button class="green_button btn" type="button" data-toggle="modal" data-target="#callbackform" id="callback">
                <?php echo $button_callback; ?>
              </button>
            </div>
          </div>
          <?php echo $header_menu; ?>

        </nav>
      </div>

      <div class="search_and_language d-flex col-12 p-0">
        <div class="search col-10 col-sm-6 col-md-7 p-0">
          <div class="cpt_product_search">
            <form action="/search/" method="get" id="autosearch">
              <input type="text" id="searchstring" name="searchstring" onclick="if(this.value=='Поиск')this.value='';" class="input_message" onkeyup="lookup(this.value);" onblur="fill();" rel="{'search_products'|transcape}" autocomplete="off">
              <input type="submit" value="⌕" class="button_search">
            </form>
            <div class="suggestionsBox" id="suggestions" style="display: none;">
              <div class="suggestionList" id="autoSuggestionsList">
                &nbsp;
              </div>
            </div>
          </div>
        </div>
        <?php echo $cart; ?>
      </div>
    </div>
  </div>
  <script>
  function windowSize(){
    if ($(window).width() <= '767'){
        $('.desktop_menu').hide();
        $('.mobile_menu').show();
    } else {
      $('.desktop_menu').show();
        $('.mobile_menu').hide();
    }
  }
  $(window).on('load resize',windowSize);
  $(document).ready(windowSize);
  $("#navToggle").click(function() {
      $(this).toggleClass("active");
      $(".overlay").toggleClass("open");
      // this line ▼ prevents content scroll-behind
      $("body").toggleClass("locked");
  });

    var slinky = $('.js-menu').slinky({
      title: true
    });
  </script>
