<style>
  .cpp-container {
    display: none;
  }
</style>
<div class="footer container-fluid">
  <div class="container">
    <div class="footer_center d-flex col-12 m-auto col-lg-9 col-12 flex-column flex-md-row">
      <div class="copyright col-12 p-0 order-2 order-md-1 col-md-7">

        <p>
          <?php echo $text_footer; ?>
        </p>
        <p>
          <?php echo $text_ES . date('Y')?>
          </a>
        </p>
      </div>
      <div class="bottom_menu col-12 p-0 order-1 order-md-2 col-md-5 d-flex justify-content-between flex-row flex-md-column">
        <div class="d-flex flex-column justify-content-md-between flex-md-row">
          <div>
            <a rel="http://growmarket.com.ua/catalog/osveschenie" href="#" target="_blank" class="hidli">
              <?php echo $text_lamp; ?>
            </a>
          </div>
          <div>
            <a rel="http://medcannabis.info/" href="#" target="_blank" class="hidli">
              <?php echo $text_medical; ?> </a>
          </div>
        </div>
        <div class="d-flex flex-column">
          <p class="soc_text">
            <?php echo $text_social_networks; ?>
          </p>

          <div class="block_counter">
            <a href="#" id="instagram" class="counter_right hidli" target="_blank" rel="<?php echo $soc[4]; ?>">
        								<img src="/images/Inst1.png" alt="instagram" />
        							</a>
            <a href="<?php echo $soc[7]; ?>" class="counter_right">
        								<img src="/images/viberPNG.png" alt="viber" />
        							</a>
            <a target="_blank" href="#" class="counter_right hidli" rel="<?php echo $soc[1]; ?>">
        								<img src="/images/Fb1.png" alt="ES facebook" />
        							</a>
            <a target="_blank" href="#" class="counter_right hidli" rel="<?php echo $soc[0]; ?>">
        								<img src="/images/vkPNG.png" alt="ES vk" />
        							</a>
            <a target="_blank" href="#" class="counter_right hidli" rel="<?php echo $soc[6]; ?>">
  								      <img src="/images/teleg.png" alt="ES telegram" />
  							      </a>
            <a target="_blank" href="#" class="counter_right hidli" rel="<?php echo $soc[5]; ?>">
  								      <img src="/images/YT-1.png" alt="ES YouTube" />
  							      </a>
          </div>
          <div>
            <div class="payment"><img src="/img/payment.png"></div>
          </div>
        </div>
      </div>
      <p id="back-top">
        <a href="#top"><span></span></a>
      </p>
    </div>
  </div>
</div>
</div>
</div>


<div class="modal fade" id="modalform" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
  </div>
</div>
<div class="modal fade" id="modalmessage" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-body message col-8 mx-auto" style="">
    </div>
  </div>
</div>
<!-- Start FastcallAgent code {literal} -->
<script type="text/javascript">
var fca_code = 'f390de1e79a18d710a7f5d00247e5d4f';
(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.charset = 'utf-8';
    po.src = '//ua.cdn.fastcallagent.com/fca.min.js?_='+Date.now();
    var s = document.getElementsByTagName('script')[0];
    if (s) { s.parentNode.insertBefore(po, s); }
    else { s = document.getElementsByTagName('head')[0]; s.appendChild(po); }
})();
</script>
<!-- {/literal} End FastcallAgent code -->
</body>

</html>
