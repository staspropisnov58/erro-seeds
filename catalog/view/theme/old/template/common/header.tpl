<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <base href="<?php echo $base; ?>" />
  <meta charset="UTF-8" />
  <title>
    <?php echo $title; ?>
  </title>
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>
  <?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>" rel="icon" />
  <?php } ?>
  <link rel="preconnect" crossorigin="crossorigin" href="https://stats.g.doubleclick.net">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.google.com">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.google.com.ua">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.google-analytics.com">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.gstatic.com">
  <link rel="preconnect" crossorigin="crossorigin" href="https://cdn.jsdelivr.net">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.googletagmanager.com">
  <?php foreach ($links as $link) { ?>
  <?php  if($link['hreflang']){ ?>
  <link rel="<?=$link['rel']?>" hreflang="<?=$link['hreflang']?>" href="<?=$link['href']?>">
  <?php }else{ ?>
  <link href="<?=$link['href']?>" rel="<?=$link['rel']?>">
  <?php } ?>
  <?php } ?>
  <?php if ($robots) { ?>
  <meta name="robots" content="<?php echo $robots; ?>" />
  <?php } ?>

  <?php if ($microdata){
        echo $microdata;
      } ?>

      <?php if($opengraph){ ?>
        <?php foreach($opengraph['opengraph'] as $key => $value){ ?>
          <meta property="<?php echo $key; ?>" content="<?php echo $value; ?>">
        <?php } ?>
      <?php } ?>


  <?php if ($e_commerce){
        echo $e_commerce;
      } ?>
    <meta name="google-site-verification" content="F7YSfW70m5IbsLBu_cjtWC43S_p9GyARfqlAwKat6MA" />
  <meta name="yandex-verification" content="" />
  <link rel="shortcut icon" href="/logo_favicon.png">
  <meta name="jivochate_code" content="<?php echo $jivochate_code; ?>">
  <meta name="gtm_code" content="GTM-T36VTNM">
  <meta name="theme-color" content="#0f0e0f">
  <link rel="preload" href="/catalog/view/theme/old/fonts/roboto-medium-webfont.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="/catalog/view/theme/old/fonts/roboto-regular-webfont.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="/catalog/view/theme/old/fonts/webfonts/fa-regular-400.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="/catalog/view/theme/old/fonts/webfonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="/catalog/view/theme/old/fonts/roboto-bold-webfont.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="/catalog/view/theme/old/fonts/AGLettericaCondensed-Roman.woff" as="font" type="font/woff" crossorigin>
  <link rel="preload" href="/catalog/view/theme/old/css/font_aw.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <link rel="preload" href="/catalog/view/theme/old/css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <?php foreach ($styles as $style) { ?>
  <link rel="preload" href="<?php echo $style['href']; ?>" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <?php } ?>
  <noscript>
    <link rel="stylesheet" href="/catalog/view/theme/old/css/font_aw.min.css">
    <link rel="stylesheet" href="/catalog/view/theme/old/css/bootstrap.min.css">
    <?php foreach ($styles as $style) { ?>
    <link rel="stylesheet" href="<?php echo $style['href']; ?>">
    <?php } ?>
  </noscript>
  <link rel="stylesheet" href="/catalog/view/theme/old/css/main.min.css?v=2.6.9">

  <script src="/catalog/view/theme/old/js/jquery-3.3.1.min.js"></script>
  <script src="/catalog/view/theme/old/js/bootstrap.min.js" defer></script>
  <script src="/catalog/view/theme/old/js/fontawesome.min.js" defer data-search-pseudo-elements searchPseudoElements></script>
  <?php foreach ($scripts as $script) { ?>
  <script defer src="<?php echo $script; ?>"></script>
  <?php } ?>

</head>

<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T36VTNM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <?php if($agreement_popup) { ?>
  <div class="modal fade bd-example-modal-lg" id="modalagreement" tabindex="-1" data-backdrop="static" role="dialog" data-keyboard="false" aria-labelledby="modalagreement" aria-hidden="true">
    <div class="modal-dialog modal-lg agr_wrap" role="document">
      <div class="modal-content">
        <div class="agr_text">
          <h4 class="modal-title"><?=$agreement_popup['title']?></h4>
          <?=$agreement_popup['description']?>
        </div>
        <div class="choise">
          <button type="button" id="agreement-yes" class="agr_buttons"><?=$agreement_popup['button_yes']?></button>
          <a id="agreement-no" class="agr_buttons" href="https://www.google.com/">
            <?=$agreement_popup['button_no']?>
          </a>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "ImageObject",
      "author": "Errors Seeds",
      "contentUrl": "/published/publicdata/WEBASYST/attachments/SC/products_pictures/semena-fotoperiodichnogo-feminizirovannogo-sorta-Somango-Feminised.jpg"
    }


  </script>

  <div class="bg_header bg_header_mob">
    <?php if($banners) { ?>
    <div class="top_header_baner">
      <?php if($banners['link']) { ?>
     <a href="<?=$banners['link']?>"> <img src="<?=$banners['image']?>" alt="<?=$banners['title']?>" /></a>
      <?php } else { ?>
      <img src="<?=$banners['image']?>" alt="<?=$banners['title']?>" />
      <?php } ?>
    </div>
    <?php } ?>
    <div class="container d-flex flex-column">
      <div class="header header_mob">
        <button type="button" class="menu_button">
          <svg viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect opacity="0.01" width="22" height="22" fill="#D8D8D8"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M20.0001 5.9001H2.0001C1.50304 5.9001 1.1001 5.49715 1.1001 5.0001C1.1001 4.50304 1.50304 4.1001 2.0001 4.1001H20.0001C20.4972 4.1001 20.9001 4.50304 20.9001 5.0001C20.9001 5.49715 20.4972 5.9001 20.0001 5.9001ZM2.0001 12.4001H14.0001C14.4972 12.4001 14.9001 11.9972 14.9001 11.5001C14.9001 11.003 14.4972 10.6001 14.0001 10.6001H2.0001C1.50304 10.6001 1.1001 11.003 1.1001 11.5001C1.1001 11.9972 1.50304 12.4001 2.0001 12.4001ZM2.0001 18.9001H17.5001C17.9972 18.9001 18.4001 18.4972 18.4001 18.0001C18.4001 17.503 17.9972 17.1001 17.5001 17.1001H2.0001C1.50304 17.1001 1.1001 17.503 1.1001 18.0001C1.1001 18.4972 1.50304 18.9001 2.0001 18.9001Z" fill="white"/>
          </svg>
        </button>
        <?php if($mobile_menu) { ?>
        <div class="mob_menu" id="mobile_menu">
          <div class="mod_block">
            <div class="top_bl">
              <button type="button" class="close">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M0.843492 15.1566C1.3093 15.6224 2.06451 15.6224 2.53032 15.1566L15.1566 2.53023C15.6224 2.06442 15.6224 1.30921 15.1566 0.843407C14.6908 0.377602 13.9356 0.377603 13.4698 0.843407L0.843492 13.4698C0.37769 13.9356 0.37769 14.6908 0.843492 15.1566Z" fill="white"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M15.1567 15.1566C14.6909 15.6224 13.9357 15.6224 13.4699 15.1566L0.843531 2.53022C0.37773 2.06442 0.377729 1.3092 0.843531 0.843402C1.30933 0.377598 2.06455 0.377598 2.53036 0.843403L15.1567 13.4698C15.6225 13.9356 15.6225 14.6908 15.1567 15.1566Z" fill="white"/>
                </svg>
              </button>
              <div class="mob_menu_item">
                <div class="mob_main_menu ">
                  <div class="mmenu"><?php echo $header_menu; ?></div>
                </div>
              </div>
            </div>
            <div class="bottom_bl">
              <div class="mob_menu_item">
                <div class="mob_language">
                  <div class="language"><?php echo $language; ?></div>
                </div>
              </div>
              <div class="mob_menu_item">
                <div class="mob_currency">
                  <div class="currency"><?php echo $currency; ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a href="<?php echo $home; ?>" class="logo small_logo">
          <img src="/catalog/view/theme/old/images/logo_sm.svg" alt="<?php echo $home; ?>">
        </a>
        <?php } ?>

        <a href="<?php echo $home; ?>" class="logo big_logo">
         <img src="/catalog/view/theme/old/images/logo_lg.svg" alt="<?php echo $home; ?>">
       <!--  <img src="/image/svg1.svg" alt="<?php echo $home; ?>">-->
        </a>

        <div class="right_block d-flex flex-row">
        <a href="tel:849" class="short_tel">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 34.262 34.261" style="enable-background:new 0 0 34.262 34.261; fill: #64696f;margin-right: 5px;width: 24px;" space="preserve"><g> <path d="M25.972,21.712c-2.18-0.51-4.961-1.842-5.971-3.496c-0.562-0.924-0.623-1.96-0.166-2.851		c0.023-0.047,0.067-0.118,0.102-0.175c-0.024-0.197,0.021-0.456,0.162-0.792c0.009-0.023,0.01-0.028,0.016-0.041H8.925		c0.006,0.012,0.006,0.012,0.013,0.028c0,0,0.001,0,0.001,0.001c0.251,0.394,0.471,0.742,0.588,0.968		c0.463,0.9,0.402,1.938-0.16,2.859c-1.01,1.656-3.795,2.99-5.973,3.498L2.792,31.81c-0.064,1.105,0.66,2.013,1.612,2.013h20.558		c0.953,0,1.679-0.905,1.612-2.013L25.972,21.712z M11.322,28.667h-1.93c-0.356,0-0.644-0.289-0.644-0.645s0.287-0.645,0.644-0.645		h1.93c0.354,0,0.644,0.289,0.644,0.645S11.677,28.667,11.322,28.667z M11.322,26.212h-1.93c-0.356,0-0.644-0.289-0.644-0.645		s0.287-0.643,0.644-0.643h1.93c0.354,0,0.644,0.285,0.644,0.643C11.966,25.923,11.677,26.212,11.322,26.212z M11.322,23.757h-1.93		c-0.356,0-0.644-0.288-0.644-0.644c0-0.354,0.287-0.645,0.644-0.645h1.93c0.354,0,0.644,0.288,0.644,0.645		C11.966,23.469,11.677,23.757,11.322,23.757z M15.648,28.667h-1.931c-0.355,0-0.644-0.289-0.644-0.645s0.287-0.645,0.644-0.645		h1.931c0.356,0,0.646,0.289,0.646,0.645S16.005,28.667,15.648,28.667z M15.648,26.212h-1.931c-0.355,0-0.644-0.289-0.644-0.645		s0.287-0.643,0.644-0.643h1.931c0.356,0,0.646,0.285,0.646,0.643C16.294,25.923,16.005,26.212,15.648,26.212z M15.648,23.757		h-1.931c-0.355,0-0.644-0.288-0.644-0.644c0-0.354,0.287-0.645,0.644-0.645h1.931c0.356,0,0.646,0.288,0.646,0.645		C16.294,23.469,16.005,23.757,15.648,23.757z M19.976,28.667h-1.93c-0.354,0-0.646-0.289-0.646-0.645s0.29-0.645,0.646-0.645h1.93		c0.355,0,0.646,0.289,0.646,0.645S20.331,28.667,19.976,28.667z M19.976,26.212h-1.93c-0.354,0-0.646-0.289-0.646-0.645		s0.29-0.643,0.646-0.643h1.93c0.355,0,0.646,0.285,0.646,0.643C20.62,25.923,20.331,26.212,19.976,26.212z M19.976,23.757h-1.93		c-0.354,0-0.646-0.288-0.646-0.644c0-0.354,0.29-0.645,0.646-0.645h1.93c0.355,0,0.646,0.288,0.646,0.645		C20.62,23.469,20.331,23.757,19.976,23.757z M27.607,10.141c0.009,0.177,0.049,0.356,0.021,0.534		c-0.164,1.044-0.711,2.007-1.043,3.014c-0.431,1.284-6.949,0.309-6.541-1.367c0.154-0.62,0.912-2.413,0.851-3.047		c-0.056-0.565-0.884-0.818-1.562-0.71c-1.797,0.279-4.019,0.951-5.888,1.414l0.001,0.003c-0.051,0.014-0.104,0.022-0.156,0.037		c-0.052,0.011-0.104,0.025-0.156,0.038v-0.003c-1.873,0.44-4.155,0.853-5.885,1.42c-0.656,0.216-1.275,0.817-1.066,1.347		c0.232,0.59,1.724,1.843,2.145,2.325c1.127,1.307-4.234,5.145-5.201,4.195c-0.754-0.743-1.679-1.353-2.301-2.207		c-0.104-0.146-0.15-0.324-0.226-0.486c-1.81-3.684,0.591-8.466,5.55-10.091c1.895-0.622,3.809-1.176,5.736-1.679l-0.002-0.01		c0.055-0.012,0.104-0.021,0.158-0.032c0.051-0.013,0.104-0.03,0.155-0.043l0.002,0.01c1.944-0.432,3.902-0.81,5.87-1.12		C23.227,2.871,27.542,6.035,27.607,10.141z M33.261,14.423l-1.375-0.647c2.115-4.484,0.185-9.851-4.297-11.963l0.645-1.374		C33.476,2.909,35.729,9.182,33.261,14.423z M30.888,13.305l-1.373-0.647c1.496-3.176,0.131-6.977-3.045-8.473l0.646-1.371		C31.05,4.666,32.741,9.373,30.888,13.305z"></path></g></svg>
        <span>849 <small style="font-size:11px"><?php echo $text_tariffs; ?></small></span>
        </a>
          <div class="phones dropdown_block">
            <div class="phone_item">
              <a href="tel:0800750849" style="width:130px">
              <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="488.919px" height="488.919px" viewBox="0 0 488.919 488.919" style="enable-background:new 0 0 488.919 488.919;fill: #64696f;min-width: 24px;height:24px" space="preserve">
                <g>
                <g>
                <path d="M173.478,365.788c-6.318,1.26-24.677,15.299-28.83,14.633c-12.698-12.642-24.349-26.343-34.637-41.016
                                c-6.297-8.981-12.053-18.33-17.258-27.975l0.057-0.029c-6.506-12.039-12.141-24.552-16.838-37.42
                                c-6.145-16.835-10.712-34.23-13.811-51.878c1.831-3.787,23.852-10.806,28.5-15.265c27.078-25.976,20.028-99.397-32.943-91.678
                                c-68.131,9.899-63.735,90.647-47.94,140.407c5.923,18.657,15.994,46.073,29.717,74.412l3.718,7.289
                                c15.386,27.471,32.103,51.421,44.012,66.956c31.766,41.433,95.464,91.251,142.575,41.049
                                C266.26,405.284,210.273,358.451,173.478,365.788z"></path>
                <path d="M233.564,264.971h-38.536l6.921-5.768c0.08-0.066,0.157-0.134,0.234-0.202c22.622-20.226,37.689-38.453,37.689-61.665
                                c0-28.142-20.187-46.324-51.429-46.324c-15.541,0-30.751,4.227-42.828,11.904c-3.583,2.278-5.047,6.778-3.489,10.728l4.699,11.91
                                c0.963,2.44,2.958,4.329,5.445,5.156c2.49,0.827,5.218,0.51,7.45-0.869c7.507-4.635,14.954-6.984,22.133-6.984
                                c17.182,0,19.783,10.752,19.796,17.198c-0.406,14.739-14.614,29.975-44.34,56.56l-15.89,14.378
                                c-1.857,1.68-2.916,4.066-2.916,6.569v11.031c0,4.895,3.967,8.859,8.859,8.859h86.202c4.894,0,8.859-3.966,8.859-8.859V273.83
                                C242.423,268.938,238.458,264.971,233.564,264.971z" style="fill: #64696f;"></path>
                <path d="M350.267,234.355h-7.726v-72.146c0-4.892-3.966-8.859-8.859-8.859h-30.354c-3.063,0-5.908,1.583-7.524,4.186
                                l-49.681,79.972c-0.872,1.404-1.333,3.023-1.333,4.675v13.458c0,4.892,3.966,8.858,8.859,8.858h52.372v24.095
                                c0,4.895,3.968,8.859,8.859,8.859h18.804c4.894,0,8.859-3.966,8.859-8.859v-24.095h7.726c4.893,0,8.859-3.967,8.859-8.858v-12.427
                                C359.126,238.322,355.159,234.355,350.267,234.355z M306.02,234.355h-23.074l23.09-40.789L306.02,234.355z" style="fill: #64696f;"></path>
                <path d="M482.78,152.624l-53.479-29.192c-12.932-22.041-30.056-41.65-50.832-57.816c-43.051-33.498-96.57-48.224-150.698-41.471
                                c-45.999,5.74-89.068,27.2-121.273,60.425c-8.143,8.4-7.933,21.81,0.467,29.953c8.399,8.143,21.81,7.933,29.953-0.467
                                c25.883-26.703,59.114-43.257,96.1-47.872c63.006-7.866,122.197,21.792,155.095,71.451l-19.874,60.241
                                c-1.434,4.343-0.224,9.122,3.101,12.26c3.325,3.139,8.166,4.07,12.418,2.391l29.082-11.489c0.304,1.893,0.587,3.793,0.827,5.709
                                c5.354,42.899-6.319,85.316-32.868,119.438c-26.549,34.119-64.796,55.858-107.694,61.212
                                c-11.609,1.449-19.846,12.034-18.396,23.644c1.338,10.719,10.463,18.562,20.993,18.561c0.874,0,1.76-0.054,2.649-0.165
                                c54.128-6.755,102.387-34.185,135.885-77.235s48.226-96.57,41.471-150.698c-0.688-5.508-1.6-10.951-2.71-16.328l28.47-11.247
                                c4.252-1.68,7.148-5.669,7.432-10.233C489.177,159.13,486.794,154.814,482.78,152.624z"></path>
                </g>
                </g>
                </svg>
                <span>0 800 750 849</span>
              </a>
            </div>
            <div class="drop_trig" id="phones_trig">
              <i class="fas fa-chevron-down"></i>
            </div>
            <div class="hidden_menu" aria-labelledby="phones_trig">
            <div>
              <a href="tel:+380930000849">
                <svg width="20" height="20" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M5.975 11.95C9.2749 11.95 11.95 9.2749 11.95 5.975C11.95 2.6751 9.2749 0 5.975 0C2.6751 0 0 2.6751 0 5.975C0 9.2749 2.6751 11.95 5.975 11.95Z" fill="none"></path> <path d="M0.00976562 5.98538C0.00976562 2.68294 2.68294 0.00976562 5.98538 0.00976562C9.28781 0.00976562 11.961 2.68294 11.961 5.98538C11.961 9.28781 9.28781 11.961 5.98538 11.961C5.33172 11.961 4.70733 11.8585 4.12196 11.6634C4.54635 9.19513 6.10245 7.18538 8.16098 6.33172C8.40489 6.61464 8.76586 6.79025 9.16586 6.79025C9.89757 6.79025 10.4927 6.19513 10.4927 5.46342C10.4927 4.73172 9.89757 4.13659 9.16586 4.13659C8.43416 4.13659 7.83903 4.73172 7.83903 5.46342C5.21464 6.25367 3.08294 8.08781 2.00489 10.4439C1.66342 10.1366 1.35611 9.79025 1.09269 9.41464C1.15611 7.12196 2.28781 5.11708 3.99025 3.93172C4.23416 4.20489 4.59025 4.3805 4.98538 4.3805C5.71708 4.3805 6.3122 3.78538 6.3122 3.04879C6.3122 2.3122 5.71708 1.72196 4.98538 1.72196C4.24879 1.72196 3.65855 2.31708 3.65855 3.04879C3.65855 3.08781 3.66342 3.12684 3.66342 3.16099C2.07806 3.71708 0.780497 4.81464 0.0195217 6.21464C0.00976562 6.14147 0.00976562 6.06342 0.00976562 5.98538Z" fill="#64696f"></path> </svg>
                <span>+38 093 00 00 849</span>
              </a>
              <a href="tel:+380950000849">
                <svg width="20" height="20" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M3.79006 0.422042C5.29622 -0.177075 7.05066 -0.135652 8.51668 0.560214C8.09893 0.496876 7.67403 0.544633 7.26053 0.6144C6.1375 0.826076 5.07026 1.35343 4.23605 2.13689C3.43618 2.92156 2.87567 3.96268 2.71736 5.07655C2.61264 5.86581 2.70971 6.69364 3.06457 7.41202C3.43025 8.16574 4.07393 8.78393 4.84975 9.10296C5.59733 9.4187 6.4645 9.41522 7.22172 9.13339C8.35789 8.71726 9.17423 7.59364 9.26759 6.39316C9.32632 5.60637 9.13457 4.76939 8.60493 4.16593C8.09931 3.57524 7.35982 3.24987 6.6198 3.06467C6.58013 2.33628 6.92398 1.6167 7.46927 1.14299C7.77209 0.870476 8.14789 0.696771 8.53651 0.585613L8.56602 0.575447C9.67849 1.10787 10.6238 1.98596 11.226 3.06394C11.7421 3.98171 12.0112 5.0393 11.9845 6.09278C11.9794 7.45506 11.4692 8.80384 10.5946 9.84547C9.76759 10.8388 8.61086 11.5534 7.35014 11.8395C6.08607 12.1306 4.72606 12.005 3.54302 11.4689C2.38544 10.9529 1.40473 10.0559 0.780842 8.95253C0.261864 8.03561 -0.0126822 6.9784 0.0101014 5.9237C0.0125765 4.60955 0.480149 3.30555 1.29652 2.27792C1.94861 1.45737 2.81369 0.804921 3.79006 0.422042Z" fill="#64696f"></path> <path d="M7.2606 0.614536C7.67407 0.544769 8.099 0.497044 8.51675 0.560382L8.57672 0.570516L8.53658 0.585717C8.14796 0.696939 7.77216 0.870613 7.46934 1.14316C6.92405 1.61683 6.5802 2.33645 6.61987 3.06484C7.35986 3.25001 8.09938 3.57535 8.605 4.16607C9.13464 4.76953 9.32639 5.60651 9.26766 6.3933C9.1743 7.59375 8.35796 8.7174 7.2218 9.13353C6.46457 9.41535 5.5974 9.41881 4.84982 9.10313C4.07397 8.78406 3.43032 8.16591 3.06464 7.41215C2.70978 6.69377 2.61271 5.86594 2.71746 5.07668C2.87574 3.96282 3.43625 2.92169 4.23616 2.13703C5.07033 1.35357 6.13757 0.826213 7.2606 0.614536Z" fill="none"></path> </svg>
                <span>+38 095 00 00 849</span>
              </a>
              <a href="tel:+380960000849">
                <svg width="20" height="20" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M5.77725 2V4.68067V2Z" fill="black"></path><path d="M5.77725 2V4.68067" stroke="#64696f" stroke-width="2.5" stroke-linecap="round"></path><path d="M10.7004 5.57685L8.15094 6.40522L10.7004 5.57685Z" fill="black"></path>
                  <path d="M10.7004 5.57685L8.15094 6.40522" stroke="#64696f" stroke-width="2.5" stroke-linecap="round"></path><path d="M8.81998 11.3642L7.24432 9.1955L8.81998 11.3642Z" fill="black"></path> <path d="M8.81998 11.3642L7.24432 9.1955" stroke="#64696f" stroke-width="2.5" stroke-linecap="round"></path><path d="M2.73465 11.3643L4.31031 9.1956L2.73465 11.3643Z" fill="black"></path> <path d="M2.73465 11.3643L4.31031 9.1956" stroke="#64696f" stroke-width="2.5" stroke-linecap="round"></path>
                  <path d="M0.854216 5.57696L3.40369 6.40534L0.854216 5.57696Z" fill="black"></path><path d="M0.854216 5.57696L3.40369 6.40534" stroke="#64696f" stroke-width="2.5" stroke-linecap="round"></path> </svg>
                <span>+38 096 00 00 849</span>
              </a>
              </div>
            </div>
          </div>
          <div class="language_currency">
            <div class="language"><?php echo $language; ?></div>
            <div class="currency"><?php echo $currency; ?></div>
          </div>
          <a href="https://docs.google.com/forms/d/e/1FAIpQLScVy9xO7r0wmuavHw5pDUUfxqPfEu6eF6jOcL7YuummY1n87g/viewform"  class="menu_item fast_order" target="_blank">
            <i class="far fa-clock"></i>
            <span><?php echo $text_fast_order; ?></span>
          </a>
          <div class="contact">
            <button type="button" class="menu_item" data-toggle="modal" data-target="#modalcontact">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="488.919px" height="488.919px" viewBox="0 0 488.919 488.919" style="enable-background:new 0 0 488.919 488.919;fill: #fff;width: 35px;height: 34px;" xml:space="preserve">
              <g>
              <g>
                <path d="M173.478,365.788c-6.318,1.26-24.677,15.299-28.83,14.633c-12.698-12.642-24.349-26.343-34.637-41.016
                  c-6.297-8.981-12.053-18.33-17.258-27.975l0.057-0.029c-6.506-12.039-12.141-24.552-16.838-37.42
                  c-6.145-16.835-10.712-34.23-13.811-51.878c1.831-3.787,23.852-10.806,28.5-15.265c27.078-25.976,20.028-99.397-32.943-91.678
                  c-68.131,9.899-63.735,90.647-47.94,140.407c5.923,18.657,15.994,46.073,29.717,74.412l3.718,7.289
                  c15.386,27.471,32.103,51.421,44.012,66.956c31.766,41.433,95.464,91.251,142.575,41.049
                  C266.26,405.284,210.273,358.451,173.478,365.788z"></path>
                <path d="M233.564,264.971h-38.536l6.921-5.768c0.08-0.066,0.157-0.134,0.234-0.202c22.622-20.226,37.689-38.453,37.689-61.665
                  c0-28.142-20.187-46.324-51.429-46.324c-15.541,0-30.751,4.227-42.828,11.904c-3.583,2.278-5.047,6.778-3.489,10.728l4.699,11.91
                  c0.963,2.44,2.958,4.329,5.445,5.156c2.49,0.827,5.218,0.51,7.45-0.869c7.507-4.635,14.954-6.984,22.133-6.984
                  c17.182,0,19.783,10.752,19.796,17.198c-0.406,14.739-14.614,29.975-44.34,56.56l-15.89,14.378
                  c-1.857,1.68-2.916,4.066-2.916,6.569v11.031c0,4.895,3.967,8.859,8.859,8.859h86.202c4.894,0,8.859-3.966,8.859-8.859V273.83
                  C242.423,268.938,238.458,264.971,233.564,264.971z" style="fill: #65bd00;"></path>
                <path d="M350.267,234.355h-7.726v-72.146c0-4.892-3.966-8.859-8.859-8.859h-30.354c-3.063,0-5.908,1.583-7.524,4.186
                  l-49.681,79.972c-0.872,1.404-1.333,3.023-1.333,4.675v13.458c0,4.892,3.966,8.858,8.859,8.858h52.372v24.095
                  c0,4.895,3.968,8.859,8.859,8.859h18.804c4.894,0,8.859-3.966,8.859-8.859v-24.095h7.726c4.893,0,8.859-3.967,8.859-8.858v-12.427
                  C359.126,238.322,355.159,234.355,350.267,234.355z M306.02,234.355h-23.074l23.09-40.789L306.02,234.355z" style="fill: #65bd00;"></path>
                <path d="M482.78,152.624l-53.479-29.192c-12.932-22.041-30.056-41.65-50.832-57.816c-43.051-33.498-96.57-48.224-150.698-41.471
                  c-45.999,5.74-89.068,27.2-121.273,60.425c-8.143,8.4-7.933,21.81,0.467,29.953c8.399,8.143,21.81,7.933,29.953-0.467
                  c25.883-26.703,59.114-43.257,96.1-47.872c63.006-7.866,122.197,21.792,155.095,71.451l-19.874,60.241
                  c-1.434,4.343-0.224,9.122,3.101,12.26c3.325,3.139,8.166,4.07,12.418,2.391l29.082-11.489c0.304,1.893,0.587,3.793,0.827,5.709
                  c5.354,42.899-6.319,85.316-32.868,119.438c-26.549,34.119-64.796,55.858-107.694,61.212
                  c-11.609,1.449-19.846,12.034-18.396,23.644c1.338,10.719,10.463,18.562,20.993,18.561c0.874,0,1.76-0.054,2.649-0.165
                  c54.128-6.755,102.387-34.185,135.885-77.235s48.226-96.57,41.471-150.698c-0.688-5.508-1.6-10.951-2.71-16.328l28.47-11.247
                  c4.252-1.68,7.148-5.669,7.432-10.233C489.177,159.13,486.794,154.814,482.78,152.624z"></path>
              </g>
              </g>
              </svg>
            </button>
          </div>
          <div class="login">
            <div class="dropdown_block">
              <button id="drop_menu_acc" class="menu_item" type="button" >
                <?php if($logged != null){ ?>
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 34 33.4" style="enable-background:new 0 0 34 33.4;" xml:space="preserve">
                    <g><path style="fill:none;stroke:#FFFFFF;stroke-width:2.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M22.9,30.1c-1.7,0.8-3.7,1.2-5.7,1.2c-7.7,0-14-6.3-14-14s6.3-14,14-14s14,6.3,14,14c0,1.3-0.2,2.6-0.5,3.8"/>
                      <circle class="st1" style="fill:none;stroke:#65BD00;stroke-width:2.5;stroke-miterlimit:10;" cx="17.3" cy="12.4" r="2.9"/>
                      <path style="fill:none;stroke:#FFFFFF;stroke-width:2.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M7.4,27.1c0,0,3.5-8.1,9.8-8.1c1.2,0,2.3,0.3,3.3,0.7c0.9,0.4,1.7,0.9,2.4,1.5"/></g>
                    <g><line style="fill:none;stroke:#65BD00;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="27" y1="21.8" x2="27" y2="29.8"/>
                      <line style="fill:none;stroke:#65BD00;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="23" y1="25.8" x2="31" y2="25.8"/>
                    </g>
                  </svg>
                  <?php }else{ ?>
                    <svg viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect opacity="0.01" y="1" width="22" height="22" fill="#D8D8D8"/>
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M14.6501 8.2501C14.6501 9.98979 13.2398 11.4001 11.5001 11.4001C9.7604 11.4001 8.3501 9.98979 8.3501 8.2501C8.3501 6.5104 9.7604 5.1001 11.5001 5.1001C13.2398 5.1001 14.6501 6.5104 14.6501 8.2501ZM10.1501 8.2501C10.1501 8.99568 10.7545 9.6001 11.5001 9.6001C12.2457 9.6001 12.8501 8.99568 12.8501 8.2501C12.8501 7.50451 12.2457 6.9001 11.5001 6.9001C10.7545 6.9001 10.1501 7.50451 10.1501 8.2501Z" fill="#65BD00"/>
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M11.5001 22.9001C17.7961 22.9001 22.9001 17.7961 22.9001 11.5001C22.9001 5.20405 17.7961 0.100098 11.5001 0.100098C5.20405 0.100098 0.100098 5.20405 0.100098 11.5001C0.100098 17.7961 5.20405 22.9001 11.5001 22.9001ZM4.38819 17.9485C2.84213 16.2444 1.9001 13.9823 1.9001 11.5001C1.9001 6.19816 6.19816 1.9001 11.5001 1.9001C16.802 1.9001 21.1001 6.19816 21.1001 11.5001C21.1001 13.9823 20.1581 16.2444 18.612 17.9485C17.7226 14.8596 14.8752 12.6001 11.5001 12.6001C8.125 12.6001 5.27764 14.8596 4.38819 17.9485ZM5.94013 19.3271C7.50898 20.4435 9.42786 21.1001 11.5001 21.1001C13.5723 21.1001 15.4912 20.4435 17.0601 19.3271C16.7276 16.5516 14.3651 14.4001 11.5001 14.4001C8.63508 14.4001 6.27258 16.5516 5.94013 19.3271Z" fill="white"/>
                    </svg>
                <?php } ?>
              </button>
              <div class="drop_menu_acc hidden_menu">
                <?php if($logged != null){ ?>
                <a href="<?php echo $account; ?>" class="green_button dropdown-item">
                  <?php echo $text_account; ?>
                </a>
                <a href="<?php echo $logout; ?>" class="logout dropdown-item">
                  <?php echo $text_logout; ?>
                </a>
                <?php }else{ ?>
                <a class="input green_button dropdown-item" href="<?php echo $login; ?>">
                  <?php echo $text_login; ?>
                </a>
                <a href="<?php echo $register; ?>" class="registration green_button dropdown-item">
                  <?php echo $text_register; ?>
                </a>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="cart d-flex">
            <?php echo $cart; ?>
          </div>
        </div>
        <div class="menu_desc">
          <div class="mmenu"><?php echo $header_menu; ?></div>
        </div>
      </div>
      <div class="top d-flex flex-row justify-content-between">
        <div class="search d-flex">
          <form action="/search/" method="get" id="autosearch">
            <input type="text" id="search" name="search" autocomplete="off">
            <ul class="result-search-autocomplete"></ul>
            <button type="submit" class="button_search">
                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect opacity="0.01" x="1" y="0.0454102" width="21" height="21" fill="#D8D8D8"/>
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M17.4878 16.2728C18.9205 14.566 19.7833 12.3648 19.7833 9.96208C19.7833 4.53798 15.3862 0.140869 9.96212 0.140869C4.53802 0.140869 0.140911 4.53798 0.140911 9.96208C0.140911 15.3862 4.53802 19.7833 9.96212 19.7833C12.3649 19.7833 14.5661 18.9205 16.2728 17.4877L19.7381 20.953C20.0736 21.2885 20.6176 21.2885 20.9531 20.953C21.2886 20.6175 21.2886 20.0736 20.9531 19.7381L17.4878 16.2728ZM15.8592 15.5194C17.2269 14.0687 18.0652 12.1133 18.0652 9.96208C18.0652 5.4869 14.4373 1.85905 9.96212 1.85905C5.48694 1.85905 1.85909 5.4869 1.85909 9.96208C1.85909 14.4373 5.48694 18.0651 9.96212 18.0651C12.1133 18.0651 14.0687 17.2268 15.5195 15.8591C15.5577 15.7896 15.6063 15.7242 15.6653 15.6652C15.7242 15.6063 15.7896 15.5577 15.8592 15.5194Z"/>
                </svg>
            </button>
          </form>
        </div>
      </div>
      <div class="hed_link" style="display: none">

        <?php echo $lincks_hed; ?>
      </div>

    </div>
  </div>
  <script type="text/javascript">
    function headerResize() {
      if ($(window).width() > '991') {
          $('.header.header_mob').removeClass('header_mob').addClass('header_desc');
          $('.bg_header').removeClass('bg_header_mob');
        }else{
          $('.header.header_desc').removeClass('header_desc').addClass('header_mob');
          $('.bg_header').addClass('bg_header_mob');
        };
    };
    headerResize();
    $(window).on('resize', headerResize);
  </script>
