<div class="footer">
    <div class="copyright container d-flex flex-column">
        <div class="d-flex flex-wrap mb-sm-4">
            <div class="footer_sub col-12 col-md-3">
                <div class="language d-flex flex-wrap ">
                    <?php echo $language; ?>
                </div>
            </div>
            <div class="footer_sub col-12 col-md-3">
                <div class="currency align-items-start flex-column">
                    <?php echo $currency; ?>
                </div>
            </div>
            <div class="footer_sub col-12 col-md-3">
                <div class="f_title"><?php echo $text_social_networks; ?></div>
                <div class="cpt_currency_selection">
                    <div class="block_counter d-flex flex-row align-items-lg-end">
                        <a href="#" id="instagram" class="counter_right hidli" target="_blank" rel="https://www.instagram.com/errorsseeds_ua/ ">
                            <img src="/catalog/view/theme/old/images/instagram.svg" alt="instagram">
                        </a>
                        <a target="_blank" href="#" class="counter_right hidli" rel="https://www.facebook.com/esuacom/">
                            <img src="/catalog/view/theme/old/images/facebook.svg" alt="ES facebook">
                        </a>
                        <a target="_blank" href="#" class="counter_right hidli" rel="https://t.me/errors_seeds420">
                            <img src="/catalog/view/theme/old/images/telegram.svg" alt="ES telegram">
                        </a>
                        <a target="_blank" href="#" class="counter_right hidli" rel="https://www.youtube.com/channel/UCiiuf9qa23p_6CSTmhXtmlQ/">
                            <img src="/catalog/view/theme/old/images/youtube.svg" alt="ES YouTube">
                        </a>
                    </div>
                </div>
            </div>
            <div class="footer_sub col-12 col-md-3">
                <div class="f_title"><?php echo $text_contact; ?></div>
                <div class="cpt_currency_selection">
                    <div class="block_counter d-flex flex-row align-items-lg-end">
                        <p>
                          <?php echo $address_2; ?>
                        </p>
                </div>
            </div>
        </div>
        <div class="d-flex flex-wrap">
            <div class="footer_sub col-12">
                <p>
                    <?php echo $text_footer; ?>
                </p>
                <p>
                    <?php echo $text_ES . date('Y')?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>


<?php if($calendars) { ?>
<link rel="stylesheet" href="/catalog/view/javascript/calendarJs/css/eventCalendar.css?v=1.0.22" as="style" onload="this.onload=null;this.rel='stylesheet'">
<script defer src="/catalog/view/javascript/calendarJs/moment.js"></script>
<script defer src="/catalog/view/javascript/calendarJs/jquery.eventCalendar.js?v=0.0.12"></script>
<div id="special_calendar" >
    <div class="security_button">
        <span>Акции</span>
        <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#FFFFFF;}
</style>
            <g>
                <path class="st0" d="M26,50c-0.3,0-0.6,0-1,0c-1.3-0.2-2.3-0.9-3.2-1.9c-0.7-0.8-1.6-1.5-2.3-2.2c-0.4-0.4-0.9-0.5-1.5-0.5
		c-1.2,0.2-2.5,0.3-3.7,0.5c-2.7,0.4-4.8-1-5.3-3.7c-0.2-1.2-0.5-2.5-0.6-3.7c-0.1-0.8-0.5-1.2-1.2-1.6c-1.1-0.5-2.2-1.2-3.3-1.7
		c-2.3-1.2-3.1-3.7-2-6c0.5-1.2,1.1-2.3,1.6-3.4c0.3-0.6,0.3-1.2,0-1.8c-0.5-1.1-1.1-2.3-1.6-3.4c-1.2-2.4-0.4-4.8,2-6.1
		c1.1-0.6,2.2-1.2,3.4-1.8c0.6-0.3,0.9-0.8,1-1.4C8.5,10.3,8.7,9.1,9,7.9c0.5-2.7,2.4-4.1,5.2-3.8c1.2,0.2,2.5,0.3,3.7,0.5
		c0.7,0.1,1.2,0,1.7-0.5c0.9-0.9,1.8-1.8,2.7-2.7c1.9-1.8,4.4-1.8,6.3,0c0.9,0.9,1.9,1.8,2.8,2.7c0.5,0.4,0.9,0.5,1.5,0.5
		c1.2-0.2,2.5-0.4,3.7-0.5c2.7-0.4,4.7,1,5.2,3.7c0.2,1.2,0.5,2.5,0.6,3.7c0.1,0.7,0.5,1.2,1.1,1.5c1.1,0.5,2.2,1.2,3.3,1.7
		c2.4,1.3,3.2,3.7,2,6.2c-0.5,1.1-1,2.2-1.6,3.3c-0.3,0.6-0.3,1.2,0,1.9c0.5,1.1,1.1,2.3,1.6,3.4c1.2,2.4,0.4,4.8-2,6
		c-1.1,0.6-2.3,1.2-3.4,1.8c-0.6,0.3-0.9,0.8-1,1.4c-0.2,1.2-0.4,2.4-0.6,3.6c-0.5,2.7-2.4,4.2-5.2,3.8c-1.2-0.2-2.5-0.3-3.7-0.5
		c-0.7-0.1-1.2,0.1-1.7,0.5c-0.7,0.7-1.5,1.4-2.2,2.2C28.3,49.1,27.3,49.8,26,50z M36.6,15.5C36.5,14.6,35.8,14,35,14
		c-0.5,0-0.9,0.2-1.2,0.6c-6.2,6.2-12.5,12.5-18.7,18.7c-0.1,0.1-0.2,0.2-0.3,0.4c-0.3,0.5-0.4,0.9-0.1,1.5C15,35.7,15.4,36,16,36
		s0.9-0.3,1.3-0.7c5.5-5.5,11-11,16.5-16.5c0.7-0.7,1.5-1.5,2.2-2.2C36.2,16.2,36.4,15.8,36.6,15.5z M31.3,27.4
		c-3,0-5.3,2.4-5.3,5.3s2.4,5.2,5.3,5.2s5.3-2.4,5.3-5.3C36.5,29.7,34.2,27.4,31.3,27.4z M19.7,22.5c2.9,0,5.3-2.4,5.3-5.3
		s-2.4-5.3-5.3-5.3s-5.2,2.4-5.2,5.3C14.4,20.2,16.8,22.5,19.7,22.5z"/>
                <path class="st0" d="M31.3,35c-1.3,0-2.4-1-2.4-2.3s1.1-2.4,2.4-2.4s2.4,1.1,2.4,2.4C33.7,34,32.6,35,31.3,35z"/>
                <path class="st0" d="M22.1,17.2c0,1.3-1.1,2.4-2.3,2.4c-1.3,0-2.4-1.1-2.4-2.4c0-1.3,1.1-2.4,2.4-2.4
		C21.1,14.9,22.1,15.9,22.1,17.2z"/>
            </g>
</svg>
    </div>
    <div style="height: 500px" class="security_content">
        <div id="eventCalendar" ></div>
    </div>








</div>


<script>

    $(document).ready(function (){
        $( "#special_calendar .security_button" ).click(function() {
            $( '#special_calendar' ).toggleClass( "open" );
        });

        $(function(){
            var data = [
            <?php foreach($calendars as $calendar) { ?>
                { "date": "<?=$calendar['date_available']?> 04:00:00", "title": "<?=$calendar['title']?>", "description": '<?=html_entity_decode($calendar["description"])?>', "url": "<?=$calendar['url']?>", "img": "<?=$calendar['image']?>" },
            <?php } ?>
        ];
            $('#eventCalendar').eventCalendar({
                jsonData: data,
                eventsjson: 'data.json',
                jsonDateFormat: 'human',
                startWeekOnMonday: false,
                openEventInNewWindow: true,
                dateFormat: 'DD-MM-YYYY',
                showDescription: false,
                locales: {
                    locale: "ru",
                    txt_noEvents: "Нет запланированных событий",
                    txt_SpecificEvents_prev: "",
                    txt_SpecificEvents_after: "события:",
                    txt_NextEvents: "Следующие события:",
                    txt_GoToEventUrl: "Смотреть",
                    moment: {
                        "months" : [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
                        "monthsShort" : [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн",
                            "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],

                        // "weekdays" : [ "Воскресенье", "Понедельник","Вторник","Среда","Четверг",
                        //     "Пятница","Суббота" ],
                        // "weekdaysShort" : [ "Вс","Пн","Вт","Ср","Чт",
                        //     "Пт","Сб" ],
                        // "weekdaysMin" : [ "Вс","Пн","Вт","Ср","Чт",
                        //     "Пт","Сб" ]

                        "weekdays" : [ "Понедельник","Вторник","Среда","Четверг",
                            "Пятница","Суббота","Воскресенье" ],
                        "weekdaysShort" : [ "Пн","Вт","Ср","Чт",
                            "Пт","Сб","Вс" ],
                        "weekdaysMin" : [ "Пн","Вт","Ср","Чт",
                            "Пт","Сб","Вс" ]
                    }
                }
            });
        });
    })



</script>


<?php } ?>

<p id="back-top">
    <a href="#top"><i class="fas fa-chevron-up"></i></a>
</p>

<div class="modal fade" id="modalform" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    </div>
</div>
<div class="modal fade" id="modalmessage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-body message col-8 mx-auto" style="">
        </div>
    </div>
</div>


<script defer src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js"></script>
<script>
    $(document).ready(function (){
        $('img.lazyload').lazyload();
    })
</script>
    <!-- BEGIN PLERDY CODE -->
    <script type="text/javascript" defer data-plerdy_code='1'>
        var _protocol="https:"==document.location.protocol?" https://":" http://"
        _site_hash_code = "446c3529dc946c5d56bb58da9b31ae2b",_suid=21639, plerdyScript=document.createElement("script");
        plerdyScript.setAttribute("defer",""),plerdyScript.dataset.plerdyMainScript="plerdyMainScript",
                plerdyScript.src="https://a.plerdy.com/public/js/click/main.js?v="+Math.random();
        var plerdyMainScript=document.querySelector("[data-plerdyMainScript='plerdyMainScript']");
        plerdyMainScript&&plerdyMainScript.parentNode.removeChild(plerdyMainScript);
        try{document.head.appendChild(plerdyScript)}catch(t){console.log(t,"unable add script tag")}
    </script>
    <!-- END PLERDY CODE -->
</body>

</html>
