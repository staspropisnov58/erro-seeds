const peskyProduct = document.getElementById('pesky-product');
const minTimeout = parseInt(peskyProduct.dataset.minTimeout);
const maxTimeout = parseInt(peskyProduct.dataset.maxTimeout);
const showTimeout = 30000;

const peskyPoll = document.getElementById('pesky-poll');
const showPollTimeout = 60000;

peskyProduct.querySelector('.close').addEventListener('click', function() {
  peskyProduct.classList.remove('slideInLeft');
  peskyProduct.classList.add('slideOutLeft');
});

if (window.innerWidth > 576) {
  animateProductWindow();
}

function votePoll(url) {
  httpGet(url)
  .then(
    response => {
      let json = JSON.parse(response);

      let dateExpires = new Date(2019, 10, 23).toGMTString();
      peskyProduct.dataset.firstTimeout = '';
      document.cookie = 'voted=true; expires=' + new Date().toGMTString() + '; path=/';
      document.cookie = 'votedc=true; expires=' + dateExpires + '; path=/';
      localStorage.removeItem('startс');
      let success = json.success;
      return success;
    },
    error => console.log(`Rejected: ${error}`)
  )
  .then(
    success => {
      showSuccess(success);
      return hidePeskyWindow(peskyPoll);
    }
  )
  .then(
    pollHidden => {
      peskyPoll.remove();
      animateProductWindow();
    });
}

function animateProductWindow() {

  httpGet('index.php?route=module/pesky_window/getProductToPeskyWindow')
  .then(
    response => {
      let json = JSON.parse(response);
      let product = json.product;
      return product;
    },
    error => console.log(`Rejected: ${error}`)
  )
  .then(
    product => {
      return showPeskyProduct(product);
    }
  )
  .then(
    productShown => {
      return hidePeskyWindow(peskyProduct);
    }
  ).then(
    productHidden => {
      peskyProduct.classList.remove('animated', 'slideOutLeft');
      animateProductWindow();
    }
  );


}

function showPeskyProduct(product) {
  return new Promise(function (resolve, reject) {
    peskyProduct.querySelector('img').setAttribute('src', product.thumb);
    peskyProduct.querySelector('a').setAttribute('href', product.href);
    peskyProduct.querySelector('a').innerText = product.name;

    let showAfter = randomTimeout(minTimeout, maxTimeout);

    if (!document.cookie.replace(/(?:(?:^|.*;\s*)pesky\s*\=\s*([^;]*).*$)|^.*$/, "$1") && peskyProduct.dataset.firstTimeout) {
      showAfter = parseInt(peskyProduct.dataset.firstTimeout) * 1000;
      let dateExpires = new Date(new Date().setDate(new Date().getDate()+1)).toGMTString();
      peskyProduct.dataset.firstTimeout = '';
      document.cookie = 'pesky=true; expires=' + dateExpires + '; path=/';
    }

    setTimeout(() => peskyProduct.classList.add('animated', 'slideInLeft'), showAfter);
    peskyProduct.addEventListener('animationend', () => resolve('shown'));
  });
}

function hidePeskyWindow(peskyWindow) {
  return new Promise(function (resolve, reject) {
    peskyWindow.classList.remove('slideInLeft');
    setTimeout(() => peskyWindow.classList.add('animated', 'slideOutLeft'), showTimeout);
    peskyWindow.addEventListener('animationend', () => resolve('hide'));
  });
}

function animatePollWindow() {
  setTimeout(() => {
    peskyPoll.classList.add('animated', 'slideInLeft');
    localStorage.removeItem('startc');
  }, calculateTimeout());
}

function showSuccess(success) {
  peskyPoll.style.height = peskyPoll.offsetHeight + 'px';
  document.getElementById('pesky-buttons').remove();

  let successDiv = document.createElement('DIV');
  successDiv.classList.add('alert', 'alert-success');
  successDiv.innerText = success;
  peskyPoll.appendChild(successDiv);
}

function randomTimeout(min, max) {
  return Math.floor((Math.random() * (max - min + 1) + min) * 60000);
}

function httpGet(url) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onload = function() {
      if (this.status == 200) {
        resolve(this.response);
      } else {
        var error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };
    xhr.onerror = function() {
      reject(new Error("Network Error"));
    };
    xhr.send();
  });
}

function calculateTimeout() {
  let start = localStorage.getItem('startс');
  let now = new Date().getTime();
  return showPollTimeout - (now - start);
}
