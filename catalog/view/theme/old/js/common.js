const botUserAgents = ['Googlebot', 'Google', 'Googlebot-News', 'Googlebot-Image', 'Googlebot-Video', 'Googlebot-Mobile', 'Mediapartners-Google', 'AdsBot-Google', 'AdsBot-Google-Mobile-Apps', 'Slurp', 'YandexBot', 'YandexMobileBot', 'YandexDirectDyn', 'YandexImages', 'YandexVideo', 'YandexMedia', 'YandexBlogs', 'YandexFavicons', 'YandexMetrika', 'YandexSitelinks', 'YandexWebmaster', 'YandexPagechecker', 'YandexImageResizer', 'YandexCalendar', 'YaDirectFetcher', 'YandexVertis', 'YandexAntivirus', 'bingbot', 'adidxbot', 'msnbot-media', 'msnbot', 'BingPreview'];

const userIsBot = isBot();

function isBot() {
  const userAgent = navigator.userAgent || '';
  for (let botAgent of botUserAgents) {
    if (userAgent.indexOf(botAgent) >= 0) {
      return true;
    }
  }

  return false;
}
function closeMenu() {
  $('.menu_desc .js-dropmenu_btn').removeClass('active');
  $('.menu_desc .js-dropmenu').removeClass('active');
};
function owlTop() {
  if ($('#slides').length) {
    $('#slides').addClass('show');
    $('#slides .slides_container').owlCarousel({
      autoPlay: 3000,
      loop: true,
      singleItem: true,
      nav: true,
      margin: 10,
      lazyLoad: true,
      autoplay: true,
      autoplayHoverPause: true,
      responsive:{
        0:{
          items:1,
          center: false
        },
        991:{
          items:1,
          center: true,
        }
      },
      navText: ['<a href="javascript:void(0);" class="prev" onclick><img src="catalog/view/theme/old/images/arrow-prev.png" width="31" height="85" alt="Arrow Prev"></a>',
        '<a href="javascript:void(0);" class="next"><img src="catalog/view/theme/old/images/arrow-next.png" width="31" height="85" alt="Arrow Next"></a>'
      ],
    });
  }
}

function owlMobTop() {
  if ($('#slides_mobile').length) {
    $('#slides_mobile').addClass('show');
    $('#slides_mobile .slides_container').owlCarousel({
      autoPlay: 3000,
      loop: true,
      singleItem: true,
      nav: false,
      margin: 10,
      lazyLoad: true,
      autoHeight:true,
      autoplay: true,
      responsive:{
        0:{
          items:1,
          center: false
        },
        991:{
          items:1,
          center: true,
        }
      }
    });
  }
}

function owlSlideTop() {
  if ($(window).width() >= '767') {
    owlTop();
    $('#slides_mobile').removeClass('show');
  } else {
    owlMobTop();
    $('#slides').removeClass('show');
  }
}
$(window).on('load resize', owlSlideTop);

$(document).ready(function() {
    $(document).mouseup(function (e){
      if($('#cart').is(':visible')){
        var block = $("#cart-content");
        if (!block.is(e.target)
            && block.has(e.target).length === 0) {
            cart.close(event);
        }
      };
    });
  if (!userIsBot) {
    document.querySelectorAll('.more-information').forEach((seoInformation) => {
      seoInformation.remove();
    });
  }
  window.dataLayer = window.dataLayer || [];
  $(function () {
    let header = $('.bg_header_mob'),
    heigh = header.height(),
    scrollPrev = 0;
    $(window).scroll(function() {
      var scrolled = $(window).scrollTop();
      if ( scrolled > 30 && scrolled > scrollPrev ) {
        header.addClass('out')
      } else {
        header.removeClass('out')
      }
      scrollPrev = scrolled;
    });
  });

  $('.js-dropmenu_btn').click(function() {
    let a = $(this);
    if(a.hasClass('active')){
      a.removeClass('active');
      a.siblings('.js-dropmenu').removeClass('active');
    } else {
      $('.js-dropmenu_btn').removeClass('active');
      $('.js-dropmenu').removeClass('active');
      a.addClass('active');
      a.siblings('.js-dropmenu').addClass('active');
    };
  });
  $('.mob_main_menu .dropmenu i').click(function() {
    let par = $(this).parents('p').siblings('ul');
    par.addClass('active')
  });
  $('.mob_main_menu .js-dropmenu .prev').click(function() {
    if ($(this).closest('ul').hasClass('line')) {
      $(this).closest('ul.line').removeClass('active');
    } else {
      $(this).parents('.js-dropmenu').removeClass('active');
      $('.js-dropmenu_btn').removeClass('active');
    }
  });

  $('#mobile_menu .all_var').removeClass('hidden_menu');
  $('#mobile_menu .cpt_currency_selection').removeClass('dropdown_block');
  $("a.hidli").on("click", function() {
    window.open($(this).attr("rel"));
    return false
  });
    $("a.hidli").on("mousedown", function() {
      window.open($(this).attr("rel"));
      return false
    });

  $('label.cart_block').click(function(){
    $('body').css('position', 'fixed')
  });
  $('.show_all').on("click", function(e) {
    e.preventDefault();
    $(this).siblings('.toggle_xs').toggleClass('show');
  });
  $('#left_tab .left_tab_button').on("click", function() {
    $('#left_tab').toggleClass('open');
    $('#security_tab').removeClass('open');
  });
  $(function () {
    var head = $('.footer_sub .f_sub_head');
    head.on("click", function() {
      $(this).siblings('.f_sub_cont').toggleClass('show');
    });
  });
  $('.footer .all_var').removeClass('hidden_menu');
  $('.footer .cpt_currency_selection').removeClass('dropdown_block');
  $('.drop_trig').click(function(q) {
    q.preventDefault();
    b = $(this).siblings('.hidden_menu');
    if (b.is(":visible")) {
      b.hide('slow');
    } else {
      $('.hidden_menu').not(b).hide('slow');
      b.toggle('slow');
    }
  });
  // для предзаказа
  $('input[name="preorder_select"]').click(function() {
    let a = $('input[name="preorder_select"]:checked').attr('id');
    $('.js-preorder').css('display','none');
    $('.js-preorder#'+ a +'_input').css('display','block');
  });

  $('.list-group-item:lt(4)').each(function() {
    $(this).children('.option-name').addClass('active')
  });
  $('.option-name').on("click", function(){
    if($(this).hasClass('active')){
      $(this).removeClass('active')
    }else{
      $('.list-group-item:eq(0)').children('.option-name').addClass('active');
      $(this).addClass('active')
    }
  });
  $('#drop_menu_acc').on("click", function(e) {
    e.preventDefault();
    $('.drop_trig').siblings('.hidden_menu').hide('slow');
    $(this).toggleClass('toggled');
    $('.drop_menu_acc').toggle('slow');
    $('.search_button').css('display', 'block');
      $('.search_button_close').css('display', 'none');
      $('.search').css('display', 'none');
  });
  $("input[name='search']").on("keydown", function(a) {
      if (a.keyCode == 13) {
          $("input[name='search']").parent().find("button").trigger("click")
      }
  });
  $('input[name=\'search\']').keyup(function(e) {
      $(".result-search-autocomplete").css({
          "overflow-x": "hidden"
      });
      var a = $("input[name=search]").val();
      $.ajax({
          method: "GET",
          url: 'index.php?route=product/product/autocomplete&search=',
          data: {
              search: a
          }
      }).done(function(json) {
          var html = "";
          if (json && a != "") {
            if (json['products']) {
              for (i = 0; i < json['products'].length; i++) {
                var price = json['products'][i]['special'] ? `${json['products'][i]['special']} <span class="old_price">${json['products'][i]['price']}</span>` : json['products'][i]['price'];
                var name = json['products'][i]['model'] ? `${json['products'][i]['name']} | ${json['products'][i]['model']}` : json['products'][i]['name'];
                html += `<li>
                          <a href="${json['products'][i]['href']}">
                            <img src="${json['products'][i]['image']}" alt="" title="">
                            <div>
                              <p class="prod_name">${name}</p>
                              <p class="price">${price}</p>
                            </div>
                          </a>
                        </li>`;
              }

              html += `<li><a href="${json['href']}" class="all">${json['text_search']}</a></li>`;
            } else {
              html += `<li>${json['text_empty']}</li>`;
            }

            $('ul.result-search-autocomplete').html(html);
            $(".result-search-autocomplete").addClass("active");
          } else {
              html = "";
              $(".result-search-autocomplete").removeClass("active");
          }
      })
  });

  $(function() {
    $(document).mouseup(function(e) {
      var div = $(".menu_desc");
      if (!div.is(e.target) &&
        div.has(e.target).length === 0) {
        closeMenu();
      }
    });
  });
  $('.drop').on("click", function() {
    $('#drop_menu_acc').removeClass('toggled');
    $('.drop_menu_acc').hide('slow');
    $('.drop_trig').siblings('.hidden_menu').hide('slow');
    var par = $(this).closest('li');
    if (par.hasClass('active')) {
      par.removeClass('active');
    } else {
      closeMenu();
      par.addClass('active');
    }
  });
  $("#back-top").hide();
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('#back-top').fadeIn();
      } else {
        $('#back-top').fadeOut();
      }
    });
    $('#back-top a').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });
  $('.accordion_header').on("click", function() {
    var par = $(this).closest('li');
    if (par.hasClass('active')) {
      par.removeClass('active');
      par.children('.accordion_block').hide('slow');
    } else {
      var s = $('.menu_desc .accordion_header').not(this);
      s.removeClass('active');
      s.closest('li').removeClass('active');
      s.closest('li').children('.accordion_block').hide('slow');
      par.addClass('active');
      par.children('.accordion_block').show('slow');
    }
  });
  $('.header .menu_button').on("click", function() {
    $('#mobile_menu').addClass('active');
    $('body').css('position', 'fixed')
  });
  $('#mobile_menu .close').on("click", function() {
    $('#mobile_menu').removeClass('active');
    $('body').css('position', 'relative')
  });
  $('#filter_button').on("click", function() {
    $('.filter').addClass('active');
    $('body').css('position','fixed')
  });
  $('.filter .close').on("click", function() {
    $('.filter').removeClass('active');
    $('body').css('position','relative')
  });
  $('#security_tab .security_button').on("click", function() {
    $('#security_tab').toggleClass('open');
  });
  $(".s-currency input[type='radio']").on("click", function(e) {
    e.preventDefault();
    $(".s-currency input[name='code']").attr("value", $(this).val());
    $(".s-currency").submit();
  });

  $("#language a").on("click", function(e) {
    e.preventDefault();
    $("#language input[name='code']").attr("value", $(this).attr("href"));
    $("#language").submit();
  });

  $("#callback").on("click", function(e) {
    e.preventDefault();
    getModal("modal=callback", e);
  });

  $(".director_btn").on("click", function(e) {
    e.preventDefault();
    getModal("modal=contact", e);
  });

  if ($('.related_slider').length) {
    $('.related_slider').owlCarousel(relatedSlider);
  }

  if ($('#modalagreement').length && !userIsBot) {
    $('#modalagreement').modal('show');
    // Соглашение ДА
    $('#agreement-yes').on('click', function(e) {
      e.preventDefault();
      var date_expires = new Date(new Date().setMonth(new Date().getMonth()+1)).toGMTString();
      document.cookie = 'agreement=1; expires=' + date_expires + '; path=/';
      $('#modalagreement').modal('hide');

    });

    // Соглашение НЕТ
    $('#agreement-no').on('click', function(e) {
      e.preventDefault();
      window.location.replace("https://www.google.com.ua");
    });
  }

  if ($('.accordion').length) {
    accordion();
  }

document.addEventListener('visibilitychange', function() {
  if (document.visibilityState === 'visible') {
    cart.reload();
  }
});

addAgreement();
if (document.querySelector('.need_agree')) {
  needAgree(document.querySelector('.need_agree'))
}

if (document.querySelector('meta[name="jivochate_code"]')) {
  setTimeout(() => addJivo(document.querySelector('meta[name="jivochate_code"]').content), 30000);
}

if (document.querySelector('meta[name="gtm_code"]')) {
  setTimeout(() => addGTM(document.querySelector('meta[name="gtm_code"]').content), 2 * 3000);
}

if (window.ConveadSettings) {
  addConvead();
}
});

function onloadCallback() {
  const sitekey = "6Leds7gUAAAAAC45MShr0QpYXe9QAYPSkplLfNdB";
  if (document.getElementById('submit_contact')) {
    grecaptcha.render('submit_contact', {
                      'sitekey' : sitekey,
                      'callback' : submitContact
                      });
  }

  if (document.getElementById('submit_register')) {
    grecaptcha.render('submit_register', {
                      'sitekey' : sitekey,
                      'callback' : submitRegister
                      });
  }
}

function submitContact() { event.preventDefault(); document.querySelector("form[name=contact]").submit(); }
function submitRegister() { event.preventDefault(); document.querySelector("form[name=register]").submit(); }

function addAgreement() {
  var agree = document.getElementById('agree');
  if (agree) {
    setSubmitState(agree);
    agree.addEventListener('change', function() {
      setSubmitState(agree);
    });
  }
}

function needAgree(e) {
  document.getElementById('agree').checked = !e.checked;
  setSubmitState(document.getElementById('agree'));
}

function setSubmitState(agree) {
  document.querySelectorAll('button[type=submit]').forEach(function(item, i, arr) {
    if (item.form.name === agree.form.name) {
      item.disabled = !agree.checked;
    }
  });
}

function getModal(get_params, event) {
  event.preventDefault();
  console.log(1);
  $('#modalform .modal-dialog').load('/index.php?route=common/modal&' + get_params);
  $('#modalform').modal('show');

  $('#modalform').on('hidden.bs.modal', function() {
    $('#modalform .modal-dialog').html('');
  });
  return false;
}

function sendCallback() {
  var data = $("#modalform form").serializeArray();
  $.ajax({
    url: "/index.php?route=module/callback/send",
    type: "post",
    data: data,
    success: function(json) {
      if (json.message) {
        $("#modalform .close").trigger("click");
        message.showModal({'type' : json.message.type, 'text' : json.message.text});
      } else if (json.errors) {
        $.map(json.errors, function(e, t) {
          $("#modalform #" + t).after('<label for="' + t + '" class="error">' + e + '</label>');
          $("#modalform #" + t).on("focus", function() {
            $("label[for=" + t + "].error").remove();
        });
        });
      }
    },
    error: function(e, t, a) {
      console.log("ERRORS: " + t)
    }
  });
  return false;
}

function sendNotificationRequest(event) {
  event.preventDefault();
  var data = $("#product form").serializeArray();
  $.ajax({
      url: '/index.php?route=module/notification_request/saveRequest',
      type: 'post',
      data: data,
      beforeSend: function () {
        $('.cpt_product_add2cart_button').prop('disabled', true);
        $('.form_stock').addClass('disabled');
      },
      success: function (json) {
        if (json.errors) {
          $('.cpt_product_add2cart_button').prop('disabled', false);
          $('.form_stock').removeClass('disabled');
          $.map(json.errors, function(e, t) {
            $("#product #" + t).parent().append('<label for="' + t + '" class="error">&#10008; ' + e + '</label>');
            $("#product #" + t).on("click", function() {
              $("label[for=" + t + "].error").remove();
          });
          });
        } else if (json.message) {
          var timeToHide = 400;
          $('.form_stock').animate({height:'toggle'},timeToHide);
          setTimeout(function(){
            $('.form_stock').removeClass('disabled');
            message.showEmbedded({'type' : json.message.type, 'text' : json.message.text, 'container' : $('.form_stock')});
            $('.form_stock').animate({height:'toggle'},timeToHide);
          }, timeToHide);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {

          console.log('ERRORS: ' + textStatus);
      }
  });
  return false;
}

function sendReview(event) {
  event.preventDefault();
  var data = $("#review form").serializeArray();
  $.ajax({
      url: '/index.php?route=information/review/addReview',
      type: 'post',
      data: data,
      beforeSend: function () {
        $('.cpt_product_add2cart_button').prop('disabled', true);
        $('.form_review').addClass('disabled');
      },
      success: function (json) {
        if (json.errors) {
          $('.cpt_product_add2cart_button').prop('disabled', false);
          $('.form_review').removeClass('disabled');
          $.map(json.errors, function(e, t) {
            $("#review #" + t).parent().append('<label for="' + t + '" class="error">&#10008; ' + e + '</label>');
            $("#review #" + t).on("click", function() {
              $("label[for=" + t + "].error").remove();
          });
          });
        } else if (json.message) {
          var timeToHide = 400;
          $('.form_review').animate({height:'toggle'},timeToHide);
          setTimeout(function(){
            $('.form_review').removeClass('disabled');
            message.showEmbedded({'type' : json.message.type, 'text' : json.message.text, 'container' : $('.form_review')});
            $('.form_review').animate({height:'toggle'},timeToHide);
          }, timeToHide);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {

          console.log('ERRORS: ' + textStatus);
      }
  });
  return false;
}

function sendContact(event) {
  event.preventDefault();
  var data = $("#contact form").serializeArray();
  $.ajax({
      url: '/index.php?route=information/contact/send',
      type: 'post',
      data: data,
      beforeSend: function () {
        $('.cpt_product_add2cart_button').prop('disabled', true);
        $('.form_review').addClass('disabled');
      },
      success: function (json) {
        if (json.errors) {
          $('.cpt_product_add2cart_button').prop('disabled', false);
          $('.form_review').removeClass('disabled');
          $.map(json.errors, function(e, t) {
            $("#contact #" + t).parent().append('<label for="' + t + '" class="error">&#10008; ' + e + '</label>');
            $("#contact #" + t).on("click", function() {
              $("label[for=" + t + "].error").remove();
          });
          });
        } else if (json.message) {
          var timeToHide = 400;
          $('.form_review').animate({height:'toggle'},timeToHide);
          setTimeout(function(){
            $('.form_review').removeClass('disabled');
            message.showEmbedded({'type' : json.message.type, 'text' : json.message.text, 'container' : $('.form_review')});
            $('.form_review').animate({height:'toggle'},timeToHide);
          }, timeToHide);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {

          console.log('ERRORS: ' + textStatus);
      }
  });
  return false;
}

var message = {
  success: "&#10003;",
  warning: "&#9888;",
  showModal: function(data) {
    $("#modalmessage").modal("show");
    var html = '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/catalog/view/theme/old/images/close.svg"></button>';
    html += this.toHtml(data);
    $("#modalmessage .modal-body").html(html);
  },
  showEmbedded: function(data) {
    $(data.container).html(this.toHtml(data));
  },
  toHtml: function(data) {
    var html = '';
    html += '<div class="message message-' + data.type + '">';
    html += '<span class="symbol-' + data.type + '">' + this[data.type] + '</span>';
    html += data.text;
    html += '</div>';
    return html;
  }
};
//update and remove functions are moved to checkout_cart object
var cart = {
  'stateInCheckout': 0,
  'add': function(product_id, quantity, event) {
    event.preventDefault();
    var data = {};
    var form = event.target.form;
    data = $(form).serializeArray();
    $.ajax({
      url: '/index.php?route=checkout/cart/add',
      type: 'post',
      data: data,
      dataType: 'json',
      beforeSend: function() {
        $('.cpt_product_add2cart_button').prop('disabled', true);
      },
      success: function(json) {
        $('.cpt_product_add2cart_button').prop('disabled', false);
        if (json.success_cart) {
          $('#modalform .modal-dialog').html(json.success_cart);
          $('#modalform').modal('show');

          $('#modalform').on('hidden.bs.modal', function() {
            $('#modalform .modal-dialog').html('');
          });
          cart.reload();
          dataLayer.push(json.gaData);
        } else if (json.redirect) {
          location = json.redirect;
        }
        if (json.scripts) {
          addScripts(json.scripts);
        }
      }
    });
    return false;
  },
  'reload': function() {
    var url = '/index.php?route=checkout/cart/reloadCart';
    url += $('#checkout_content').length ? '&in_checkout=1' : '';
    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      beforeSend: function() {

      },
      success: function(json) {
        if (json) {
          if (json.count) {
            if ($('#shpcrtga').length) {
              $('#shpcrtga').html(json.count);
            } else {
              $('.cpt_shopping_cart_info').append(`<div id="shpcrtga">${json.count}</div>`);
            }
          } else {
            $('#shpcrtga').remove();
          }
          if (json.cart_popup) {
            $('#cart-content').replaceWith(json.cart_popup);
            $('.minus, .plus').css('display', 'block');
            if (cart.stateInCheckout) {
              $('#cart-content').addClass('show');
            }
          } else {
            cart.close(event);
            $('#cart').remove();
          }
          if (json.reload_step && $('#checkout_content').length) {
            $('#checkout_content').html('<div id="step_' + json.reload_step.name + '">' + json.reload_step.content + '</div>');
          }
        }
      }
    });
  },
  'close': function(event) {
    event.preventDefault();
    $("#modalform").modal("hide");
    $('body').css('position', 'relative');
    $('#cart_block').trigger('click');
  }
};
var price = {
  'priceElement': '#product .new_price',
  'oldPriceElement': '#product .old_price',
  'string': function(e) {
    return (document.querySelector(e) !== null) ? document.querySelector(e).innerHTML : "";
  },
  'symbol': function(s) {
    return s.replace(s.match(/[0-9,\.]+/g)[0], "");
  },
  'get': function(s, q) {
    if (s !== "") {
      return this.parse(s) / q;
    } else {
      return false;
    }
  },
  'set': function(p, q, s, e) {
    document.querySelector(e).innerHTML = this.toString(p, q, s);
  },
  'setAsValue': function(p, q, s, e) {
    document.querySelector(e).value = this.toString(p, q, s);
  },
  'toString': function (p, q, s) {
    var symbol = this.symbol(s);
    var price = symbol ? s.split(symbol) : [s];
    for (var i = 0; i < price.length; i++) {
      if (price[i] === "") {
        price[i] = symbol;
      } else {
        price[i] = (p * q).toFixed(2);
      }
    }

    return price.join("");
  },
  'convert': function(p, q) {
    return (p * q).toFixed(2);
  },
  'parse': function(s) {
    return parseFloat(s.replace(this.symbol(s), ""));
  }
};

var quantity = {
  'get': function(e) {
    return parseInt(e.dataset.quantity, 10);
  },
  'set': function(q, e) {
    e.value = q > 0 ? q : 1;
    e.dataset.quantity = q > 0 ? q : 1;
  },
  'element': function(e) {
    return e.form.querySelector('input[name=quantity]')
  }
};

function plus(e, q) {
  var quantity_input = quantity.element(e);
  quantity_input.value = quantity.get(quantity_input) + parseInt(q);
  recount(quantity_input);
}

function recount(e) {
  var q = quantity.get(e);
  var p = price.get(price.string(price.priceElement), q);
  var o = price.get(price.string(price.oldPriceElement), q);

 if (!isNaN(parseInt(e.value, 10)) && parseInt(e.value, 10) !== 0) {
    q = parseInt(e.value, 10);
  }

  quantity.set(q, e);
  price.set(p, q, price.string(price.priceElement), price.priceElement);
  price.setAsValue(p, q, price.string(price.priceElement), 'input[name="price"]');
  if (o) {
    price.set(o, q, price.string(price.oldPriceElement), price.oldPriceElement);
    if (document.querySelector('.saving_price')) {
      price.set(o - p, q, document.querySelector('.saving_price').innerText, '.saving_price');
    }
  }

  return false;
}

function addOption(option) {
  if (document.querySelector('.owl-product-img') && option.dataset.image) {
    toggleImage(option.dataset.image);
  }
  var p = price.parse(option.dataset.price);
  var o = option.dataset.oldPrice ? price.parse(option.dataset.oldPrice) : 0;
  var q = quantity.element(option) ? quantity.get(quantity.element(option)) : 1;
  price.set(p, q, option.dataset.price, price.priceElement);
  price.setAsValue(p, q, price.string(price.priceElement), 'input[name="price"]');

  console.log(option.dataset);
  console.log(p+' -');
  console.log(o+' -');
  console.log(q+' -');
  if (o) {
    showSpecial();
    price.set(o, q, option.dataset.oldPrice, price.oldPriceElement);
    if (document.querySelector('.saving_price')) {
      var savingPrice = document.querySelector('.saving_price').innerText;
      document.querySelector('.saving_price').innerText = '';
      price.set(o - p, q, savingPrice, '.saving_price');
    }
  } else {
    hideSpecial();
  }
}

function showSpecial() {
  document.querySelector(price.priceElement).classList.add('price_sale');

  document.querySelector(price.oldPriceElement).style.display = 'inline-block';
  document.getElementById('saving_text').style.display = 'flex';
  if (document.querySelector('#product .sticker-sale')) {
    document.querySelector('#product .sticker-sale').style.display = 'block';
  }
}

function hideSpecial() {
  document.querySelector(price.priceElement).classList.remove('price_sale');
  document.querySelector(price.oldPriceElement).style.display = 'none';
  document.getElementById('saving_text').style.display = 'none';
  if (document.querySelector('#product .sticker-sale')) {
    document.querySelector('#product .sticker-sale').style.display = 'none';
  }
}

function addSelectOption(select) {
  var option = select.options[select.selectedIndex];
  addOption(option);
}

function toggleImage(triggerName) {
  var images, imagePath, imageName, target;
  images = document.querySelectorAll('.owl-product-img img');
  for (var i in images) {
    if (typeof images[i] === 'object') {
      imagePath = images[i].dataset.src;
      imageName = imagePath.slice(imagePath.lastIndexOf('/'), imagePath.length);
      if (imageName.indexOf(triggerName) >= 0) {
        target = document.querySelectorAll('.owl-product-img .owl-dot')[i];
        $(target).trigger('click');
      }
    }
  }
}

function findAncestor(el, cls) {
  while ((el = el.parentElement) && !el.classList.contains(cls));
  return el;
}

function addScripts(scripts) {
  scripts.map(function(src){
    var script = document.createElement('script');
    script.src = src;
    document.getElementsByTagName('head')[0].appendChild(script);
  });
}

var relatedSlider = {
  loop: false,
  nav: true,
  lazyLoad: true,
  singleItem: true,
  pagination: false,
  dots: false,
  margin: 10,
  scrollPerPage: false,
  autoPlay: false,
  responsive:{
    0:{items:2},
    420:{items:3},
    900:{items:4},
    1000:{items:5},
    1200:{items:6}
  },
  navText: ['<a href="javascript:void(0);" class="up"></a>', '<a href="javascript:void(0);" class="down"></a>'],
};
$.fn.SelectCustomizer = function() {
  return this.each(function() {
    var obj = $(this);
    var name = obj.attr('id');
    var id_slc_options = name + '_options';
    var id_icn_select = name + '_iconselect';
    var id_holder = name + '_holder';
    var custom_select = name + '_customselect';
    var selected = $('#customselect option:selected').attr("value");
    var selected_a = $('#customselect option:selected').attr("data-code");
    var selected_c = $('#customselect_callback option:selected').attr("data-code");
    obj.after("<div id=\"" + id_slc_options + "\"> </div>");
    obj.find('option').each(function(i) {
      $("#" + id_slc_options).append("<div id=\"" + $(this).attr("value") + "\" class=\"selectitems\" data-code=\"" + $(this).attr("data-code") + "\" >" + $(this).attr("data-content") + "<span>" + $(this).html() + "</span></div>");
    });

    obj.before("<input type=\"hidden\" value =\" " + selected + " \" name=\"" + this.name + "\" id=\"" + custom_select + "\"/><div id=\"" + id_icn_select + "\">" + this.title + "</div><div id=\"" + id_holder + "\"> </div>").remove();
    $('#customselect_iconselect').html(selected_a);
    $('#customselect_callback_iconselect').html(selected_c);
    $("#" + id_icn_select).click(function() {
      $("#" + id_holder).toggle(700);
    });

    $("#" + id_holder).append($("#" + id_slc_options)[0]);
    $("#" + id_holder + " .selectitems").mouseover(function() {
      $(this).addClass("hoverclass");
    });
    $("#" + id_holder + " .selectitems").mouseout(function() {
      $(this).removeClass("hoverclass");
    });
    $("#" + id_holder + " .selectitems").click(function() {
      $("#" + id_holder + " .selectedclass").removeClass("selectedclass");
      $(this).addClass("selectedclass");
      var thisselection = $(this).attr("data-code");
      $("#" + custom_select).val(this.id);
      $("#" + id_icn_select).html(thisselection);
      $("#" + id_holder).toggle(700)
    });

  });
}

function accordion() {
  $(".accordion .accordion_title").click(function() {

    var $content = $(this).next();
    if ($content.is(":visible")) {
      $content.slideUp(500, function() {});
      $(this).children().removeClass("active");
      $(this).removeClass("active");
    } else {
      $(".accordion .accordion_content").slideUp("slow");
      $(".accordion .accordion_title").children()
        .removeClass("active");
      $(".accordion_title").removeClass("active");
      $content.slideToggle("slow");
      $(this).children().addClass("active");
      $(this).addClass("active");
    }
  });
}

function addJivo(widget_id) {
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = '//code.jivosite.com/script/widget/' + widget_id;
  var ss = document.getElementsByTagName('script')[0];
  ss.parentNode.insertBefore(s, ss);
}

// function addConvead() {
//   window.convead = window.convead || function(){( window.convead.q  = window.convead.q || []).push(arguments)};
//
//   var ts = (+new Date()/86400000|0)*86400;
//   var s = document.createElement('script');
//   s.type = 'text/javascript';
//   s.async = true;
//   s.charset = 'utf-8';
//   s.src = 'https://tracker.convead.io/widgets/'+ts+'/widget-'+window.ConveadSettings.app_key+'.js';
//   var x = document.getElementsByTagName('script')[0];
//   x.parentNode.insertBefore(s, x);
//
//   if (window.ConveadSettings.view_product) {
//     convead('event', 'view_product', window.ConveadSettings.view_product);
//   }
// }

function addGTM(gtm_id) {
  (function(w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      'gtm.start': new Date().getTime(),
      event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0],
      j = d.createElement(s),
      dl = l != 'dataLayer' ? '&l=' + l : '';
    j.defer = true;
    j.src =
      'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', gtm_id);
}
