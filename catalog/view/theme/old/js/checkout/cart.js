$(document).ready(function() {
  $('#fast_order').click(function(){
    $(this).css('display','none');
    $('label.fast_order').css('display', 'block');
  });

  $('.minus, .plus').css('display','block');
});

var checkout_cart = {
  'edit': function (form, event) {
        event.preventDefault();
        var data = $(form).serializeArray();
        $.ajax({
            url: '/index.php?route=checkout/cart/edit',
            type: 'post',
            data: data,
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (json) {
              cart.reload();
            }
        });
        return false;
    },
    'remove': function (key, event) {
        event.preventDefault();
        $.ajax({
            url: '/index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (json) {
              cart.reload();
            }
        });
        return false;
    },
    'plus': function (quantity, event) {
      var form = $(event.target).parent('form');
      var quantity_input = $(form).children('.quantity');
      $(quantity_input).val(parseInt($(quantity_input).val(), 10) + parseInt(quantity));
      checkout_cart.edit(form, event);
    },
    'coupon': function (event) {
      event.preventDefault();
      var data = $(event.target.form).serializeArray();
      $.ajax({
          url: '/index.php?route=checkout/coupon/coupon',
          type: 'post',
          data: data,
          dataType: 'json',
          beforeSend: function () {

          },
          success: function (json) {
            cart.reload();
          }
      });
      return false;
    },
    'remove_coupon': function (event) {
      event.preventDefault();
      var form = $(event.target).parent('form');
      var data = form.serializeArray();
      $.ajax({
          url: '/index.php?route=checkout/coupon/remove_coupon',
          type: 'post',
          data: data,
          dataType: 'json',
          beforeSend: function () {

          },
          success: function (json) {
            cart.reload();
          }
      });
      return false;
    },
    'remove_addition': function (event) {
      event.preventDefault();
      var form = $(event.target).parent('form');
      var url = $(form).prop('action');
      $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          beforeSend: function () {

          },
          success: function (json) {
            cart.reload();
            if (json.uncheck && document.getElementsByName('additions[' + json.uncheck + ']')) {
              document.getElementsByName('additions[' + json.uncheck + ']').forEach(function(elem) {
                elem.checked = false;
              });
            }
          }
      });
      return false;
    }
  };
