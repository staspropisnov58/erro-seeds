delegateEvent('click', '.js-tooltip-trigger', showTooltip);
delegateEvent('click', '.js-tooltip .close', closeTooltip);

var checkout = {
  'prev_step' : {},
  'current_step' : {},
  'next_step' : {},
  'sendForm': function(event){
    event.preventDefault();
    var form = $('.button_order').prop('form');
    var data = $(form).serializeArray();
    $.ajax({
        url: '/index.php?route=checkout/checkout/sendForm',
        type: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function () {

        },
        success: function (json) {
          window.dataLayer = window.dataLayer || [];
          dataLayer.push(json.gaData);
          checkout.showErrors(json);
          checkout.fillFields(json);
          checkout.animateStep(json);
          checkout.showMessages(json);
        }
    });
    return false;
  },
  'closeincheckout': function(){
    $('#cart-content').removeClass('show');
    cart.stateInCheckout = 0;
  },
  'saveSelected': function(elem) {
    var form = $(elem).prop('form');
    var data = $(form).serializeArray();
    $.ajax({
        url: '/index.php?route=checkout/checkout/updateCart&in_checkout=1',
        type: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function () {

        },
        success: function (json) {
          if (json) {
            cart.reload();
          }
          checkout.showErrors(json);
          checkout.showMessages(json);
        }
    });
    return false;
  },
  'showErrors': function (data) {
    if (data.errors) {
      $.map(data.errors, function(e, t) {
        var input = $('#checkout_content #' + t);
        if ($(input).parent('label').length) {
          $(input).parent('label').append('<span class="label_error">' + e + '</span>');
        } else {
          $(input).parent('div').after('<span class="label_error">' + e + '</span>');
        }
        $(input).on('focus', function() {
          $(this).parent('label').children('span.label_error').remove();
      });
      });
    }
  },
  'showMessages': function (data) {
    if (data.messages) {
      var messages = '';
      $.map(data.messages, function(message) {
        messages += '<div class="alert alert-' + message.type + ' col-12">' + message.text + '</div>';
      });
      if (messages) {
        $('.checkout_head').append('<div id="messages">' + messages + '</div>');
        $('.alert').on('click', function() {
          $(this).remove();
      });
        $('.checkout_left_block').animate({scrollTop:0},0);
      }
    }
  },
  'fillFields': function (data) {
    if (data.form) {
      $.map(data.form, function(e, t) {
        var input = $('#checkout_content #' + t);
        $(input).val(e);
      });
    }
  },
  'selectShipping': function (elem) {
    $(".shipping_additions").map(function() {
      if(this.id === 'additions_' + elem.id) {
        $(this).removeClass("hidden");
        $(this).addClass("shown");
      } else {
        $(this).addClass("hidden");
        $(this).removeClass("shown");
        $(this).find(".addition").each(function() {
          $(this).prop("checked", false);
        });
      }
    });
    checkout.saveSelected(elem);
  },
  'animateStep': function (data) {
    if (data.content) {
      checkout.setCurrentPosition(data.steps);
      $('.alert').remove();
      var div_to_remove = $('#checkout_content div');
      if (window.innerWidth < 576) {
        $('#checkout_content').css('height', $('#checkout_content').css('height'));
        $(div_to_remove).animate({height:'toggle'},200);
      } else {
        $(div_to_remove).animate({width:'toggle'},200);
      }
      setTimeout(function(){
        $('#checkout_content').append('<div id="step_' + checkout.current_step.name + '">' + data.content+ '</div>');
        if (checkout.next_step) {
          $('.button_order').text(checkout.next_step.text);
        }
        $('#checkout_content').css('height', 'auto');
        $('.button_step_back').html(checkout.prev_step.href);
      }, 400);
      if (data.hide_cart) {
        $('#cart-content').animate({right:'-=1000px'},400);
        setTimeout(function(){ $('#cart-content').remove();
        $('.checkout_left_block').removeClass('col-lg-7');
        }, 500);
      } else {
        if(!$('#cart-content').length) {
          $('.checkout_left_block').addClass('col-lg-7');
          $('.checkout_left_block').append('<div id="cart-content"></div>');
          cart.reload();
        }
      }

      setTimeout(function(){
        $(div_to_remove).remove();
        addAgreement();
        if ($('.need_agree').length) {
          needAgree($('.need_agree'));
        }
       }, 500);
    }
  },
  'loadStep': function (step, event) {
    event.preventDefault();
    $('#checkout_content').append('<div id="step_' + checkout.step + '"></div>');
    $.ajax({
        url: '/index.php?route=checkout/checkout/getPrevStep&back_to=' + step,
        type: 'get',
        dataType: 'json',
        beforeSend: function () {

        },
        success: function (json) {
          checkout.animateStep(json);
        }
    });
    return false;
  },
  'setCurrentPosition': function(steps) {
    $.map(steps, function(step, position) {
      if (step.class === 'active') {
        checkout.current_step = step;
        checkout.next_step = steps[position + 1];
        $('#to_' + checkout.current_step.name).addClass('active');
      } else if (step.class === 'done') {
        checkout.prev_step = step;
        checkout.prev_step.href = '<a href="' + checkout.prev_step.href +'" onclick="checkout.loadStep(\'' + checkout.prev_step.name + '\', event)">' + checkout.prev_step.text + '</a>';
        $('#to_' + checkout.prev_step.name).addClass('done').removeClass('active').html(checkout.prev_step.href);
      } else {
        $('#to_' + step.name).removeClass('active done').html(step.text);
      }
    });
  },
  'saveOrder': function(event) {
    var form = event.target;
    form.querySelector('button').disabled = true
    form.submit();
  },
  'openCart': function(){
    $('#cart-content').addClass('show');
    cart.stateInCheckout = 1;
  }
};

function delegateEvent(eventName, elementSelector, handler) {
  document.addEventListener(eventName, function(e) {
      for (let target = e.target; target && target != this; target = target.parentNode) {
          if (target.matches(elementSelector)) {
              handler.call(target, e);
              break;
          }
      }
  }, false);
}

function showTooltip(event) {
  const tooltipTrigger = findAncestor(event.target, 'js-tooltip-trigger');
  const tooltip = document.getElementById(tooltipTrigger.dataset.target);
  tooltip.style.setProperty('display', 'block');
}

function closeTooltip(event) {
  const tooltip = findAncestor(event.target, 'js-tooltip');
  tooltip.style.setProperty('display', 'none');
}
