$(document).ready(function() {
  if ($('.related_slider').length) {
    $('.related_slider').owlCarousel(relatedSlider);
  }

  if ($('.owl-product-img').length) {
    $('.owl-product-img').addClass('owl-carousel');
    $('.owl-product-img').owlCarousel(productImageCarousel);
  }

  $(".reviews_and_descript .tab_link").click(function() {
    var a = $(this).attr('id');
    $(this).siblings(".tab_link").removeClass("active");
    $(".tab_panel_block").removeClass("active");
    $(this).addClass("active");
    $('.descript_product').find('.tab_panel_block[aria-labelledby="' + a + '"]').addClass("active");
  });

  $('#descript').click(function () {
    $('.descript_product_full').show();
    $('.reviews_block').css("display","none");
    $("#descript").css({
    color:"85bc0d",
    display:"block",
    borderBottom:"1px solid",
    borderColor:"85bc0d",
     paddingLeft:"7px",
     paddingRight:"7px",
     paddingBottom:"2px"
    });
    $('#review').css({
    color:"fff",
    borderBottom:"none"
    });
  });
  $('#review').click(function () {
    $('.reviews_block').show();
    $('.descript_product_full').hide();
    $('#review').css("color","85bc0d");
    $("#review").css({
    color:"85bc0d",
    display:"block",
    borderBottom:"1px solid",
    borderColor:"85bc0d",
     paddingLeft:"7px",
     paddingRight:"7px",
     paddingBottom:"2px"
    });
    $('#descript').css({
    color:"fff",
    borderBottom:"none"
    });
  });
});

var productImageCarousel = {
  loop:false,
  navigation:false,
  lazyLoad: true,
  singleItem: true,
  pagination: false,
  dots:true,
  scrollPerPage: false,
  autoPlay: false,
  items:1
};
