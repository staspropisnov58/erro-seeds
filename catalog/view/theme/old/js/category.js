$(document).ready(function() {
  $('#spisok_icon').click(function () {
    view.load('list');
  });
  $('#plitka_icon').click(function () {
    view.load('grid');
  });
  $(function(){
    var heig = $('.toggle_xs').height();
    if(heig < 150){
      $('.toggle_xs').hide();
      $('.toggle_xs').siblings('.show_all').hide();
    }
  });
  if ($('.carousel .product-list').length) {
    $('.carousel .product-list').owlCarousel({
       items: 1,
       autoPlay: 3000,
       loop: true,
       singleItem: true,
       nav: true,
       navText: ['<a href="javascript:void(0);" class="up"></a>', '<a href="javascript:void(0);" class="down"></a>'],
       animateIn:'slideInUp',
       animateOut: 'slideOutUp',
    });
    $('a.up').on('click', function(e) {
      e.preventDefault();
    });
    $('a.down').on('click', function(e) {
      e.preventDefault();
    });
  }
});
document.querySelector('.custom-sel-wrapper').addEventListener('click', function() {
  this.querySelector('.custom-sel').classList.toggle('open');
});
for (const option of document.querySelectorAll(".custom-option")) {
  option.addEventListener('click', function() {
      if (!this.classList.contains('selected')) {
          this.classList.add('selected');
          this.closest('.custom-sel').querySelector('.custom-sel__trigger span').textContent = this.textContent;
          location = this.dataset.value
      }
  })
};
window.addEventListener('click', function(e) {
  const select = document.querySelector('.custom-sel')
  if (!select.contains(e.target)) {
      select.classList.remove('open');
  }
});
var view = {
  'load' : function(display) {
    var data = {};
    data.filter_data = JSON.parse($('.radio_plitka').attr('data-filter-data'));
    data.controller = 'product/product/displayProducts';
    data.display = display;
    $.ajax({
      url: 'index.php?route=common/ajax_loader',
      type: 'post',
      data: data,
      dataType: 'json',
      success: function(json) {
        $('#products').html(json.html);
      },
      error: function(e, t, a) {
        console.log('ERRORS: ' + t)
      }
    });
  },
}
