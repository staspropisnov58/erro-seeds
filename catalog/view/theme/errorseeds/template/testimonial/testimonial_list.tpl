<?php if ($reviews) { ?>
    <div class="all-wr-otzivi row my-5">
        <div class="otziv col-10">
            <?php foreach ($reviews as $review) { ?>


                <div class="top-otziv d-flex mb-2">
                    <div class="otz-name color-theme"><?php echo $review['author']; ?>
                        <span class="otz-date color-theme">(<?php echo $review['date_added']; ?>) </span>
                    </div>
                    <div class="wr-rating">

                        <div class="rating">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($review['rating'] < $i) { ?>
                                    <span class="fa fa-stack"><img src="image/grey-icon.png"></span>
                                <?php } else { ?>
                                    <span class="fa fa-stack"><img src="image/green-icon.png"></span>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="desc-otz mb-2"><?php echo $review['text']; ?></div><br/><br/>


            <?php } ?>
        </div>
    </div>
<!--    <div class="row">-->
<!--        <div class="col-sm-6 text-left">--><?php //echo $pagination; ?><!--</div>-->
<!--        <div class="col-sm-6 text-right">--><?php //echo $results; ?><!--</div>-->
<!--    </div>-->
    <div class="pag-all-page d-flex flex-wrap justify-content-center">
    <nav aria-label="Page navigation example">
        <?php echo $pagination; ?>
    </nav>

</div>
    <div class="wr-all-news">
        <a href="#" id="createTestimonial" class="btn-all-news">Написать отзыв</a>
    </div>
<?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?>

<script>
    $('#createTestimonial').on('click', function (e) {
        e.preventDefault();
        $('#form-review').fadeIn('slow');

    });
    $('.page-link').on('click',function () {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
    })
</script>