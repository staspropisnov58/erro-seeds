<?php echo $header; ?>


    <div class="wrapper bg-black-page">
        <div class="container">

            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat back-cat2 col-12 d-flex">
                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> / </span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>



            <div class="bg-grey-black">
                <h1><?=$text_h1_testimonials;?></h1>
                <div class="container">
                    <div id="review"></div>
                </div>
            </div>
            <div class="reg-page row">

                <div class="wr-reg-page col-xs-12 col-md-5 px-3 mx-auto testimonial">

                    <form class="form-my" id="form-review" style="display: none">
                        <div class="form-group"> <!-- has-success-->
                            <label class="color-label" for="input-name"><?php echo $entry_name; ?></label>
                            <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name"
                                   class="form-control trans-input"/>
                        </div>
                        <div class="form-group">
                            <label class="color-label" for="input-review"><?php echo $entry_review; ?></label>
                            <textarea name="text" rows="5" id="input-review" class="form-control trans-input"></textarea>

                            <!--                            <div class="help-block">-->
                            <?php //echo $text_note; ?><!--</div>-->
                        </div>
                        <div class="form-group ">
                          <label class="color-label"><?php echo $entry_rating; ?></label>
                                                <input type="radio" id="c1" name="rating" value="1"/>
                                                &nbsp;<label id="c1-label" data-value="1" for="c1">
                                                  <span>
                                                  <i class="fas fa-cannabis"></i>
                                                </span></label>
                                                <input type="radio" id="c2" name="rating" value="2"/>
                                                &nbsp;<label id="c2-label" data-value="2" for="c2"><span>
                                                <i class="fas fa-cannabis"></i>
                                              </span></label>
                                                <input type="radio" id="c3" name="rating" value="3"/>
                                                &nbsp;<label id="c3-label" data-value="3" for="c3"><span>
                                                <i class="fas fa-cannabis"></i>
                                              </span></label>
                                                <input  type="radio" id="c4" name="rating" value="4"/>
                                                &nbsp;<label id="c4-label" data-value="4" for="c4"><span>
                                                <i class="fas fa-cannabis"></i>
                                              </span></label>
                                                <input type="radio" id="c5" name="rating" value="5"/>
                                                <label id="c5-label" data-value="5" for="c5"><span>
                                                <i class="fas fa-cannabis"></i>
                                              </span></label>
                                                &nbsp;
                                            </div>

                        <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>"
                                class="form-btn btn-reg"><?php echo $button_continue; ?></button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript"><!--



        $('#review').delegate('.pagination a', 'click', function (e) {
            e.preventDefault();
            $('#review').load(this.href);
        });

        $('#review').load('<?php echo html_entity_decode($review); ?>');

        $('#button-review').on('click', function () {
            $.ajax({
                url: '<?php echo html_entity_decode($write); ?>',
                type: 'post',
                dataType: 'json',
                data: $("#form-review").serialize(),
                beforeSend: function () {
                    if ($("textarea").is("#g-recaptcha-response")) {
                        grecaptcha.reset();
                    }
                    $('#button-review').button('loading');
                },
                complete: function () {
                    $('#button-review').button('reset');
                },
                success: function (json) {
                    $('.alert-success, .alert-danger').remove();
                    if (json['error']) {
                        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }
                    if (json['success']) {
                        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                        $('input[name=\'name\']').val('');
                        $('textarea[name=\'text\']').val('');
                        $('input[name=\'rating\']:checked').prop('checked', false);
                    }
                }
            });
        });
        //--></script>
<?php echo $footer; ?>
