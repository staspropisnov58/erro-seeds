<?php echo $header; ?>
<div class="wrapper" style="">
  <div class="bg-grey-black">
    <div class="container">
      <div class="sort-wr">
        <div class="sort row flex-wrap justify-content-between">
          <div class="back-cat back-cat2 col-12 d-flex">
            <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
            <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="prod-page row">
        <div class="col-12 col-md-5">
          <div class="row flex-wrap justify-content-between">
            <div class="col-9">
              <div class="top-otziv d-flex mb-2">
                <div class="nalichie"><i class="fa fa-check" aria-hidden="true"></i>
                  <?php echo $stock; ?>
                </div>
                <div class="wr-rating">
                  <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($rating < $i) { ?>
                    <span class="fa fa-stack"><img src="image/grey-icon.png"></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><img src="image/green-icon.png"></span>
                    <?php } ?>
                    <?php } ?>
                  </div>
                </div>
              </div>

              <!--                            -->
              <?php //echo '<pre style="color: whitesmoke; z-index: 999;position: absolute">';?>

              <div class="bl-prod-1">
                <div class="label-prod">
                  <img src="image/label.png" alt="">
                                </div>
                  <div class="owl-carousel owl-carousel2" data-slider-id="1">
                    <?php if ($images) { ?>
                                    <?php foreach ($images as $image) { ?>
                                        <div><img src="<?=$image['my_thumb'];?>" class="img-fluid" alt="<?=$heading_title?> фото" title="<?=$heading_title?>"/></div>
                                    <?php } ?>
                                        <?php } else { ?>
                                          <div><img src="<?=$thumb;?>" class="img-fluid" alt="<?=$heading_title?> фото" title="<?=$heading_title?>"/></div>
                                        <?php } ?>
                  </div>

                          <div class="vis-opt">
                            <div class="dop-prod-btn d-flex">
                              <?php if ($in_compare) {?>
                                <button type="button" style="border: none;cursor: pointer"
                                        data-toggle="tooltip" class="sr-btn-prod active"
                                        title="<?php echo $text_in_compare; ?>"
                                        onclick="window.location.href='<?=$in_compare?>';"><i class="fas fa-balance-scale"></i></button>
                              <?php } else { ?>
                                <button type="button" style="border: none;cursor: pointer"
                                        data-toggle="tooltip" class="sr-btn-prod"
                                        title="<?php echo $button_compare; ?>"
                                        onclick="compare.add('<?php echo $product_id; ?>', this);"><i class="fas fa-balance-scale"></i></button>
                              <?php } ?>
                              <?php if ($in_wishlist) { ?>
                                <button type="button" style="border: none;cursor: pointer"
                                        data-toggle="tooltip" class="wish-btn-prod active"
                                        title="<?php echo $text_in_wishlist; ?>"
                                        onclick="window.location.href='<?=$in_wishlist?>';"><i class="far fa-heart"></i></button>
                              <?php } else { ?>
                                <button type="button" style="border: none;cursor: pointer"
                                        data-toggle="tooltip" class="wish-btn-prod"
                                        title="<?php echo $button_wishlist; ?>"
                                        onclick="wishlist.add('<?php echo $product_id; ?>', this);"><i class="far fa-heart"></i></button>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php if ($images){ ?>
                      <div class="bl-prod-1 wr-owl-thumbs col-3">
                        <div class="owl-thumbs" data-slider-id="1">
                          <?php
                                foreach ($images as $image) { ?>
                          <button class="owl-thumb-item"><img src="<?=$image['my_thumb_2'];?>"/></button>
                          <?php
                                }?>

                        </div>
                      </div>
                      <?php }?>
                    </div>
                  </div>

                  <div class="col-12 col-md-7">
                    <div class="tab-att">
                      <h1><?= $data["heading_title"]; ?></h1>

                      <?php foreach ($attribute_groups[0]["attribute"] as $key => $attribute_group) { ?>
                        <?php if ($key <= 7){ ?>

                      <div class="wr-opt-prd">
                        <div class="opt-prd">
                          <?php if (isset($attribute_group["svg"])){ ?>
                            <?php echo $attribute_group["svg"]; ?>
                          <?php }else{ ?>
                            <?php if(isset($attribute["image"])){ ?>
                            <img src="<?php echo $attribute_group["image"] ?>" alt="">
                          <?php } ?>
                          <?php } ?>
                          <span><span class="tex-opt"><?= $attribute_group["name"] ?> </span>
                          <?= $attribute_group["text"] ?></span>
                        </div>
                      </div>
                    <?php }else{} ?>
                      <?php } ?>
                    </div>
                    <div class="prod-kol p-4 d-flex justify-content-between align-items-center" id="product-product-xtz">
                      <div class="p-0">
                        <?php if ($options) { ?>
                        <?php foreach ($options as $option) { ?>
                        <div class="input-group input-group-sm">
                          <select name="option[<?php echo $option['product_option_id']; ?>]"
                                              id="input-option<?php echo $option['product_option_id']; ?>"
                                              class="form-control input-prod option-selector"
                                              style="width: 225px;padding: 10px;font-size: 15px;border-radius: 5px;background: transparent;border: 2px solid #fff;color: #fff;height: auto;margin-bottom:3px;">
<!--                                            <option value="">--><?php //echo $text_select; ?><!--</option>-->
                                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                            <option  data-price="<?php echo $option_value['price']; ?>"
                                              <?php if($option_value['old_price']) { ?>data-old-price="<?php echo $option_value['old_price']; ?>"<?php } ?>
                                              value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                    (<?php echo $option_value['price']; ?>)
                                                <?php } ?>
                                            </option>
                                          <?php } ?>
                                      </select>
                        </div>
                        <?php } ?>
                        <?php } ?>
                      </div>


                      <div class="prod-count-holder flex">
                        <span class="count-oper count-minus flex flex-center flex-middle">-</span>
                        <input type="hidden" name="quantity" value="1">
                        <span class="count">x<span>1</span></span>
                        <span class="count-oper count-plus flex flex-center flex-middle">+</span>
                      </div>
                      <div class="price-prod" data-price="<?php if ($special) { echo $special; } else { echo $price; } ?>">
                        <?php if ($special) { echo $special; } else { echo $price; } ?>
                      </div>
                      <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                    </div>
                      <div class="d-flex flex-wrap justify-content-end align-items-center btn-mar2">
                        <?php if ($special) { ?>
                        <div class="old-price" data-price="<?php echo mb_substr($price, 1); ?>">
                          <?php echo mb_substr($price, 1); ?>
                        </div>
                        <?php } ?>
                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                        <?php if ($quantity > 0) { ?>
                        <button type="button" id="button-cart-xtz" data-loading-text="<?php echo $text_loading; ?>" data-id="<?php echo $product_id; ?>" class="prod-btn xtz_button" >
                              <div class="d-flex">
                                  <div class="icon-prod-btn"></div>
                                  <div class="text-prod-btn"><?php echo $button_cart; ?></div>
                              </div>
                          </button>
                        <?php } else { ?>
                        <button type="button" class="entrance-btn xtz_button" onclick="return false;"  data-toggle="modal" data-target="#modalentrance" >
                              <div class="d-flex align-items-center">
                                  <div class="text-prod-btn">
                                      <span><?php echo $button_entrance; ?></span>
                                  </div>
                              </div>
                          </button>
                        <?php } ?>


                        <!--                        <button type="button" class="prod-btn xtz_button"-->
                        <!--                                data-id="-->
                        <?php //echo $product_id; ?>
                        <!--" onclick="return false;"  data-toggle="modal" data-target="#modalcat" >-->
                        <!--                            <div class="d-flex align-items-center">-->
                        <!--                                <div class="icon-prod-btn"></div>-->
                        <!--                                <div class="text-prod-btn">-->
                        <!---->
                        <!--                                    <span>-->
                        <?php //echo $button_cart; ?>
                        <!--</span>-->
                        <!--                                </div>-->
                        <!--                            </div>-->
                        <!--                        </button>-->

                      </div>
                    </div>

                    <div class="col-12 prod-desc-page p-4">
                      <?php if ($description) {
                        echo $description;
                    } ?>
                      <div class="l-soc-desc mt-3">
                        <!--                        <span class="d-block mb-3 l-soc-desc-name">Нравится:</span>-->
                        <!-- AddThis Button BEGIN -->
                        <!--                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>-->
                        <!--                        <script src="//yastatic.net/share2/share.js"></script>-->
                        <!--                        <div class="ya-share2" data-services="vkontakte,facebook" data-counter=""></div>-->
                        <!-- AddThis Button ENDasdasd -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php if ($column_left) {
        echo $column_left;
    } ?>


              <div class="bg-grey-black">
                <div class="container">
                  <div class="wr-otzivi">
                    <div class="sort-otzivi row">
                      <div class="back-cat col-5 d-flex align-self-center">
                        <a <i class="fa fa-arrow-left" aria-hidden="true"></i> Отзывы о Товаре</a>
                      </div>
                      <div class="col-7 d-flex justify-content-end">
                        <a href="" class="btn-my btn-all-otzivi"><img src="image/fly.png" alt=""> Написать отзыв</a>
                        <!--                        <div class="input-group-sm">-->
                        <!--                            <select id="input-sort" class="form-control" onchange="location = this.value;">-->
                        <!--                                <option value="" selected="selected">Сортировать</option>-->
                        <!--                                <option value="">по дате</option>-->
                        <!--                                <option value="">самые полезные</option>-->
                        <!--                                <option value="">от купивших этот товар</option>-->
                        <!--                            </select>-->
                        <!--                        </div>-->
                      </div>
                    </div>
                  </div>
                  <div class="all-wr-otzivi row my-5">
                    <?php foreach ($all_rew as $rew) { ?>
                    <?php if ($rew['rew_id'] == 0) { ?>
                    <?php if ($rew['customer_id'] == 1) {
                            $class = 'admin';
                        } else {
                            $class = '';
                        } ?>
                    <div class="otziv col-12 qrew">
                      <div class="top-otziv d-flex mb-2">
                        <div class="otz-name color-theme">
                          <?= $rew["author"]; ?>
                          <span class="otz-date color-theme">(<?= $rew["date_added"]; ?>) </span>
                        </div>
                        <div class="wr-rating">
                          <div class="rating">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($rew['rating'] < $i) { ?>
                            <span class="fa fa-stack"><img src="image/grey-icon.png"></span>
                            <?php } else { ?>
                            <span class="fa fa-stack"><img src="image/green-icon.png"></span>
                            <?php } ?>
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                      <div class="desc-otz mb-2">
                        <?= $rew['text']; ?>
                      </div>
                      <div class="bottom-otziv d-flex mb-2">
                        <div class="reply-btn color-theme">
                          <i class="fa fa-reply" aria-hidden="true"></i>
                          <a href="" data-review-id-xtz="<?= $rew["review_id"] ?>" class="color-theme">Ответить</a>
                        </div>
                        <div class="btn-ot-group d-flex">
                          <div class="btn-ot-cool">
                            <a href="#" class="like-dislike" data-review-id="<?= $rew["review_id"] ?>"
                                           data-like="1"></a>
                            <span><?= $rew["clike"]; ?></span>
                          </div>
                          <div class="btn-ot-pool">
                            <a href="#" class="like-dislike" data-review-id="<?= $rew["review_id"] ?>"
                                           data-like="0"></a>
                            <span><?= $rew['cdislike'] ?></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php foreach ($all_rew as $rew1) {
                            if ($rew['review_id'] == $rew1['rew_id']) {
                                ?>
                    <?php if ($rew1['customer_id'] == 1) {
                                    $class = 'admin';
                                } else {
                                    $class = 'user';
                                } ?>
                    <div class="otziv col-10 <?= $class ?>">
                      <div class="wr-ad-bg">
                        <div class="top-otziv d-flex mb-2">
                          <div class="otz-name-admin">
                            <?= $rew1["author"]; ?>
                          </div>
                        </div>
                        <div class="desc-otz mb-2">
                          <?= $rew1['text']; ?>
                        </div>
                      </div>
                      <div class="bottom-otziv d-flex mb-2">
                        <div class="reply-btn color-theme">
                          <i class="fa fa-reply" aria-hidden="true"></i>
                          <a href="" class="color-theme"
                                               data-review-id-xtz="<?= $rew1["review_id"] ?>">Ответить</a>
                        </div>
                        <div class="btn-ot-group d-flex">
                          <div class="btn-ot-cool">
                            <a href="#" class="like-dislike"
                                                   data-review-id="<?= $rew1["review_id"] ?>"
                                                   data-like="1"></a>
                            <span><?= $rew1["clike"]; ?></span>
                          </div>
                          <div class="btn-ot-pool">
                            <a href="#" class="like-dislike"
                                                   data-review-id="<?= $rew1["review_id"] ?>"
                                                   data-like="0"></a>
                            <span><?= $rew1['cdislike'] ?></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php foreach ($all_rew as $rew2) {
                                    if ($rew1['review_id'] == $rew2['rew_id']) { ?>
                    <?php if ($rew2['customer_id'] == 1) {
                                            $class = 'admin';
                                        } else {
                                            $class = 'user';
                                        } ?>
                    <div class="otziv col-10 <?= $class ?>">
                      <div class="wr-ad-bg">
                        <div class="top-otziv d-flex mb-2">
                          <div class="otz-name-admin">
                            <?= $rew2["author"]; ?>
                          </div>
                        </div>
                        <div class="desc-otz mb-2">
                          <?= $rew2['text']; ?>
                        </div>
                      </div>
                      <div class="bottom-otziv d-flex mb-2">
                        <div class="reply-btn color-theme">
                          <i class="fa fa-reply" aria-hidden="true"></i>
                          <a href="" class="color-theme"
                                                       data-review-id-xtz="<?= $rew2["review_id"] ?>">Ответить</a>
                        </div>
                        <div class="btn-ot-group d-flex">
                          <div class="btn-ot-cool">
                            <a href="#" class="like-dislike"
                                                           data-review-id="<?= $rew2["review_id"] ?>"
                                                           data-like="1"></a>
                            <span><?= $rew2["clike"]; ?></span>
                          </div>
                          <div class="btn-ot-pool">
                            <a href="#" class="like-dislike"
                                                           data-review-id="<?= $rew2["review_id"] ?>"
                                                           data-like="0"></a>
                            <span><?= $rew2['cdislike'] ?></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php foreach ($all_rew as $rew3) {
                                            if ($rew2['review_id'] == $rew3['rew_id']) { ?>
                    <?php if ($rew2['customer_id'] == 1) {
                                                    $class = 'admin';
                                                } else {
                                                    $class = 'user-2';
                                                } ?>
                    <div class="otziv col-10 user <?= $class ?>">
                      <div class="wr-ad-bg">
                        <div class="top-otziv d-flex mb-2">
                          <div class="otz-name-admin">
                            <?= $rew3["author"]; ?>
                          </div>
                        </div>
                        <div class="desc-otz mb-2">
                          <?= $rew3["text"]; ?>
                        </div>
                      </div>
                      <div class="bottom-otziv d-flex mb-2">
                        <div class="reply-btn color-theme">
                          <i class="fa fa-reply" aria-hidden="true"></i>
                          <a href="" class="color-theme"
                                                               data-review-id-xtz="<?= $rew3["review_id"] ?>">Ответить</a>
                        </div>
                        <div class="btn-ot-group d-flex">
                          <div class="btn-ot-cool">
                            <a href="#" class="like-dislike"
                                                                   data-review-id="<?= $rew3["review_id"] ?>"
                                                                   data-like="1"></a>
                            <span><?= $rew3["clike"]; ?></span>
                          </div>
                          <div class="btn-ot-pool">
                            <a href="#" class="like-dislike"
                                                                   data-review-id="<?= $rew3["review_id"] ?>"
                                                                   data-like="0"></a>
                            <span><?= $rew3['cdislike'] ?></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php foreach ($all_rew as $rew4) {
                                                    if ($rew3['review_id'] == $rew4['rew_id']) { ?>
                    <?php if ($rew2['customer_id'] == 1) {
                                                            $class = 'admin';
                                                        } else {
                                                            $class = 'user-2';
                                                        } ?>
                    <div class="otziv col-10 user <?= $class ?>">
                      <div class="wr-ad-bg">
                        <div class="top-otziv d-flex mb-2">
                          <div class="otz-name-admin">
                            <?= $rew4["author"]; ?>
                          </div>
                        </div>
                        <div class="desc-otz mb-2">
                          <?= $rew4["text"]; ?>
                        </div>
                      </div>
                      <div class="bottom-otziv d-flex mb-2">
                        <div class="reply-btn color-theme">
                          <i class="fa fa-reply" aria-hidden="true"></i>
                          <!--                                                        <a href="" class="color-theme">Ответить</a>-->
                        </div>
                        <div class="btn-ot-group d-flex">
                          <div class="btn-ot-cool">
                            <a href="#" class="like-dislike"
                                                                           data-review-id="<?= $rew4["review_id"] ?>"
                                                                           data-like="1"></a>
                            <span><?= $rew4["clike"]; ?></span>
                          </div>
                          <div class="btn-ot-pool">
                            <a href="#" class="like-dislike"
                                                                           data-review-id="<?= $rew4["review_id"] ?>"
                                                                           data-like="0"></a>
                            <span><?= $rew4['cdislike'] ?></span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
                  </div>
                  <?php if (!empty ($quantity_comments)){ ?>
                  <?php if ($quantity_comments >= $limit){ ?>
                  <div class="col-12">
                    <div class="wr-refresh d-flex justify-content-center mb-3">
                      <span class="fa fa-refresh"></span>
                      <a href="#" id="show_all_reviews">Посмотреть все отзывы</a>
                    </div>
                  </div>
                  <?php } ?>

                  <?php if ($quantity_comments >=3){ ?>)
                  <div class="wr-all-otzivi">
                    <a href="" class="btn-my btn-all-otzivi"><img src="image/fly.png" alt=""> Написать отзыв</a>

                  </div>
                  <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </div>
            <input type="button" style="display: none" value="QEWQWEQWEQWEQWE" id="trigger-modal" data-toggle="modal"
       data-target="#exampleModal">



            <link href="catalog/view/javascript/ocmod/magnific-popup.css" rel="stylesheet" media="screen" />
            <link href="catalog/view/theme/default/stylesheet/ocmodpcart.css" rel="stylesheet" media="screen" />
            <script src="catalog/view/javascript/ocmod/jquery.magnific-popup.min.js" type="text/javascript"></script>
            <script type="text/javascript">
              <!--
              $(function() {
                $.each($("[onclick^='cart.add']"), function() {
                  var product_id = $(this).attr('onclick').match(/[0-9]+/);
                  $(this).attr('onclick', 'get_ocmodpcart(\'' + $(this).attr('onclick').match(/[0-9]+/) + '\',\'' + 'catalog' + '\');');
                });
                var main_product_id = $('input[name=\'product_id\']').val();
                $('#button-cart-xtz').unbind('click').attr('onclick', 'get_ocmodpcart(\'' + main_product_id + '\',\'' + 'product' + '\');');
                $('#cart > button').removeAttr('data-toggle').attr('onclick', 'get_ocmodpcart(false,\'' + 'show_cart' + '\');');
              });

              function get_ocmodpcart(product_id, action, quantity) {
                quantity = typeof(quantity) != 'undefined' ? quantity : 1;
                if (action == "catalog") {
                  $.ajax({
                    url: 'index.php?route=checkout/cart/add',
                    type: 'post',
                    data: 'product_id=' + product_id + '&quantity=' + quantity,
                    dataType: 'json',
                    success: function(json) {
                      $('.alert, .text-danger').remove();
                      if (json['redirect']) {
                        location = json['redirect'];
                      }
                      if (json['success']) {
                        $.magnificPopup.open({
                          removalDelay: 300,
                          callbacks: {
                            beforeOpen: function() {
                              this.st.mainClass = 'mfp-zoom-in';
                            }
                          },
                          tLoading: '',
                          items: {
                            src: 'index.php?route=module/ocmodpcart',
                            type: 'ajax'
                          }
                        });
                        //                            $('#cart-total').html(json['total']);
                        //                            $('#cart-total-popup').html(json['total']);
                        $('#cart-total').html(' <img src="image/basket.png"> <div class="count-top-header-bl">' + json['total'] + '</div>');

                        $('#cart-total-xtz-left').text(json['total']);

                        $('#cart > ul').load('index.php?route=common/cart/info ul li');
                      }
                    }
                  });
                }
                if (action == "product") {
                  $.ajax({
                    url: 'index.php?route=checkout/cart/add',
                    type: 'post',
                    data: $(
                      '#product-product-xtz input[type=\'text\'], #product-product-xtz input[type=\'hidden\'], #product-product-xtz input[type=\'radio\']:checked, #product-product-xtz input[type=\'checkbox\']:checked, #product-product-xtz select, #product-product-xtz textarea'
                    ),
                    dataType: 'json',
                    success: function(json) {
                      $('.alert, .text-danger').remove();
                      $('.form-group').removeClass('has-error');
                      $('.success, .warning, .attention, information, .error').remove();
                      if (json['error']) {
                        if (json['error']['option']) {
                          for (i in json['error']['option']) {
                            $('#input-option' + i).before('<span class="error bg-danger">' + json['error']['option'][i] + '</span>');
                          }
                        }
                      }
                      if (json['success']) {
                        $.magnificPopup.open({
                          removalDelay: 300,
                          callbacks: {
                            beforeOpen: function() {
                              this.st.mainClass = 'mfp-zoom-in';
                            }
                          },
                          tLoading: '',
                          items: {
                            src: 'index.php?route=module/ocmodpcart',
                            type: 'ajax'
                          }
                        });
                        $('#cart-total').html(' <img src="image/basket.png"> <div class="count-top-header-bl">' + json['total'] + '</div>');

                        $('#cart-total-xtz-left').text(json['total']);
                        $('.close').trigger('click');
                        $('#cart > ul').load('index.php?route=common/cart/info ul li');
                      }
                    }
                  });
                }
                if (action == "show_cart") {
                  $.magnificPopup.open({
                    removalDelay: 300,
                    callbacks: {
                      beforeOpen: function() {
                        this.st.mainClass = 'mfp-zoom-in';
                      }
                    },
                    tLoading: '',
                    items: {
                      src: 'index.php?route=module/ocmodpcart',
                      type: 'ajax'
                    }
                  });
                }
              }
              //-->
            </script>


            <script type="text/javascript">
              <!--



              $('#button-cart').on('click', function() {
                $.ajax({
                  url: 'index.php?route=checkout/cart/add',
                  type: 'post',
                  data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                  dataType: 'json',
                  beforeSend: function() {
                    $('#button-cart').button('loading');
                  },
                  complete: function() {
                    $('#button-cart').button('reset');
                  },
                  success: function(json) {
                    $('.alert, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['error']) {
                      if (json['error']['option']) {
                        for (i in json['error']['option']) {
                          var element = $('#input-option' + i.replace('_', '-'));

                          if (element.parent().hasClass('input-group')) {
                            element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                          } else {
                            element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                          }
                        }
                      }

                      if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                      }

                      // Highlight any found errors
                      $('.text-danger').parent().addClass('has-error');
                    }

                    if (json['success']) {
                      $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                      $('#cart-total').html(json['total']);

                      $('html, body').animate({
                        scrollTop: 0
                      }, 'slow');

                      $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    }
                  }
                });
              });


              $('.option-selector').on('change', function() {
                var quantity = parseInt($("input[name=quantity]").val());
                var priceString = $(".option-selector option:selected").attr('data-price') !== "" ? $(".option-selector option:selected").attr('data-price') : $(".price-prod").attr('data-price');
                parsePrice(priceString, quantity, $(".price-prod"));
                if ($('.old-price').length) {
                  var oldPriceString = $(".option-selector option:selected").attr('data-old-price') !== "" ? $(".option-selector option:selected").attr('data-old-price') : $(".old-price").attr('data-price');
                  parsePrice(oldPriceString, quantity, $(".old-price"));
                }
              });

              $(".count-oper").on("click", function() {
                var price = $(".option-selector option:selected").attr('data-price') !== "" ? $(".option-selector option:selected").attr('data-price') : $(".price-prod").attr('data-price');
                var quantity = count(price, $(this).text(), $(".price-prod"));
                if ($('.old-price').length) {
                  var oldPrice = $(".option-selector option:selected").attr('data-old-price') !== "" ? $(".option-selector option:selected").attr('data-old-price') : $(".old-price").attr('data-price');
                  count(oldPrice, $(this).text(), $(".old-price"));
                }
                $("input[name=quantity]").val(quantity);
                $(".count span").text(quantity);
              });

              function count(priceString, text, element) {
                var quantity = parseInt($("input[name=quantity]").val());
                if (text === "-") {
                  if (quantity !== 1) {
                    parsePrice(priceString, --quantity, element);
                  }
                } else {
                  parsePrice(priceString, ++quantity, element);
                }
                return quantity;
              }

              function parsePrice(string, quantity, element) {
                var price = {};
                var priceString = string.split(/(\d*[,\.]\d*)/);
                element.text(priceString[0] + (parseFloat(priceString[1]) * quantity).toFixed(2) + priceString[2]);
              }

              $(document).ready(function() {

                $('#button-cart').trigger('click');

                var count = 1;
                $('.qrew').each(function() {
                  if (count > 5) {
                    $(this).css('display', 'none');
                    $('.user').css('display', 'none');
                  }
                  count++;
                });

                // Показать часть описания
                $('.hidden_qwe').css('display', 'none');


                var i = $('.col-12.prod-desc-page.p-4');
                console.log(i);



              });

              // Показать все описание
              $('#view_all_description').on('click', function(e) {
                e.preventDefault();
                $(this).toggleClass('toggled');
                $('.hidden_qwe').toggle('slow');
              });

              $('#show_all_reviews').on('click', function(e) {
                e.preventDefault();

                $('.qrew').each(function() {
                  $(this).css('display', 'block');
                  $('.user').css('display', 'block');
                });


                // $(this).toggle('slow');
                // $('.user').toggle('slow');

              });

              // Модалка для добавления отзыва

              $('.btn-my.btn-all-otzivi').on('click', function(e) {
                e.preventDefault();
                $('#trigger-modal').trigger('click');
                $('input[name="product_id"]').val(<?=$data['product_id']?>);
                $('input[name="review_id"]').val(0);
              });

              $('a.color-theme').on('click', function(e) {
                e.preventDefault();
                $('#trigger-modal').trigger('click');
                var review_id = $(this).data('reviewIdXtz');
                $('input[name="review_id"]').val(review_id);
              });

              // лайки
              $('.like-dislike').on('click', function(e) {
                e.preventDefault();
                var id = $(this).data('reviewId');
                var like = $(this).data('like');
                var x = $(this).parent().find('span').text();
                $(this).parent().find('span').text(++x);
                $(this).parent().parent().css({
                  'cursor': 'default',
                  'pointer-events': 'none'
                });
                $.ajax({
                  url: 'index.php?route=product/product/update_like',
                  type: 'post',
                  data: {
                    'review_id': id,
                    'like': like
                  },
                  success: function(json) {},
                  error: function(jqXHR, textStatus, errorThrown) {

                    console.log('ERRORS: ' + textStatus);
                  }
                });
              });


              $('.owl-thumbs').bxSlider({
                mode: 'vertical',
                speed: 500,
                slideMargin: 10,
                infiniteLoop: true,
                pager: false,
                controls: true,
                slideWidth: 100,
                minSlides: 3,
                maxSlides: 3,
                moveSlides: 1,
                adaptiveHeight: false,
                nextText: '<i class="fa fa fa-angle-up"></i>',
                prevText: '<i class="fa fa fa-angle-down"></i>'
              });
              $(document).ready(function() { // Ждём загрузки страницы

                $(".img-fluid").click(function() { // Событие клика на маленькое изображение
                  var img = $(this); // Получаем изображение, на которое кликнули

                  var src = img.attr('src'); // Достаем из этого изображения путь до картинки

                  $("body").append("<div class='popup'>" + //Добавляем в тело документа разметку всплывающего окна
                    "<div class='popup_bg'></div>" + // Блок, который будет служить фоном затемненным
                    "<img src='" + src + "' class='popup_img' />" + // Само увеличенное фото
                    "</div>");
                  $(".popup").fadeIn(100); // Медленно выводим изображение
                  $(".popup_bg").click(function() { // Событие клика на затемненный фон
                    $(".popup").fadeOut(100); // Медленно убираем всплывающее окно
                    setTimeout(function() { // Выставляем таймер
                      $(".popup").remove(); // Удаляем разметку всплывающего окна
                    }, 100);
                  });
                });

              });


              //-->
            </script>

            <?php echo $footer; ?>
