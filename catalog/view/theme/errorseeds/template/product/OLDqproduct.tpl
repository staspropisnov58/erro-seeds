<?php //echo $header; ?>
<style>
    .prod-kol{
        border-bottom: none;
    }
    .input-group-sm{
        margin-left: -6px;
    }
    .price-prod{
        color: #82b523;
        margin-left: 50px;
    }
    .close_modal:hover{
        transform: rotate(90deg);
    }

    .modal-dialog{
       top: 30%;
    }
</style>
<h1 style="display: none"><?= $data["heading_title"]; ?></h1>
<div class="wrapper">

    <button type="button" class="close close_modal" data-dismiss="modal" style="color: red;cursor: pointer;" aria-label="Close">
        <span style="color: #ff3333;"  aria-hidden="true">&times;</span>
    </button>
    <div class="bg-grey-black">
        <div class="container">

            <div class="prod-page row">


                <div class="col-12 col-md-7">

                    <div class="prod-kol p-4 d-flex justify-content-between align-items-left">

                        <div class="p-0">
                            <div class="input-group input-group-sm">
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                                <?php if ($options) { ?>
                                    <?php $option_price_xtz = 0; ?>
                                    <?php foreach ($options as $option) { ?>
                                        <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                id="input-option<?php echo $option['product_option_id']; ?>"
                                                class="form-control input-prod zlp"
                                                style="width: 225px;padding: 10px;font-size: 15px;border-radius: 5px;background: transparent;border: 2px solid #fff;color: #fff;height: auto;">
<!--                                            <option value="">--><?php //echo $text_select; ?><!--</option>-->
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <?php $option_price_xtz = explode(' ', $option_value['price']); ?>

                                                <option data-price="<?= $option_price_xtz[1]; ?>"
                                                        value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </option>

                                            <?php } ?>
                                        </select>
                                    <?php } ?>
                                <?php } ?>

                                <!--                                <select id="input-sort" class="form-control input-prod" onchange="location = this.value;">-->
                                <!--                                    <option value="" selected="selected">1 семя - (37 грн)</option>-->
                                <!--                                    <option value="">1 семя - (37 грн)</option>-->
                                <!--                                    <option value="">1 семя - (37 грн)</option>-->
                                <!--                                </select>-->
                            </div>
                        </div>


                        <div class="prod-count-holder flex flex-center flex-middle js-quantity" style="display: none">
                            <span id="minus" class="count-oper count-minus flex flex-center flex-middle ng-hide"
                                  ng-show="product.quantity > 1"
                                  ng-click="product.quantity = product.quantity - 1">-</span>
                            <input type="hidden" name="quantity" value="1">
                            <span class="count">x<span class="ng-binding quan">1</span></span>
                            <span id="plus" class="count-oper count-plus flex flex-center flex-middle"
                                  ng-click="product.quantity = product.quantity + 1">+</span>
                        </div>
                        <div class="price-prod">

                            <?php if ($special) {
                                $sum = $special;
                            } else {
                                $sum = $price;
                            } ?>
                            <?php

                            $amount = explode(' ', $sum);


                            echo  $amount[1] . ' ' . $amount[2] ; ?>

                        </div>
                        <input type="hidden" id="amount_ferst" style="display: none" value="<?= $amount[1] ?>">
                        <input type="hidden" id="currency_ferst" style="display: none" value="<?= $amount[2] ?>">
                    </div>
                    <div class="d-flex flex-wrap justify-content-end align-items-center btn-mar2" style="margin-right: 37px">
                        <div class="old-price">

                            <?php if ($special) {
                                echo mb_substr($price, 1);
                            } ?>

                        </div>

                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>

                        <button type="button" id="button-cart" class="prod-btn"
                                style="background: #2a2a2a;cursor: pointer">
                            <div class="d-flex align-items-center">
                                <div class="icon-prod-btn"></div>
                                <div class="text-prod-btn">
                                    <?php /*$sum;*/?> <span>купить</span>
                                </div>
                            </div>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>



</div>
<input type="button" style="display: none" value="QEWQWEQWEQWEQWE" id="trigger-modal" data-toggle="modal" data-target="#exampleModal"  >


<script type="text/javascript"><!--



    var  price = 0;
    $('.zlp').on('change', function () {
        price = $(".zlp option:selected").attr('data-price');
        var ferst_amount = $('#amount_ferst').val();
        var currency = $('#currency_ferst').val();
        var quntyti_x = $('input[name="quantity"]').val();

        if (isNaN(price)) {
            $('.price-prod').text((ferst_amount * quntyti_x) + '.00 ' + currency)
        } else {
            $('.price-prod').text((parseInt(ferst_amount) + parseInt(price)) * quntyti_x  + '.00 ' + currency)
        }

    });


    var quntyti = 1;




    var ferst_amount = $('#amount_ferst').val();
    var currency = $('#currency_ferst').val();
    $('#minus').on('click', function () {
        --quntyti;
        if (quntyti < 1) {
            return false;
        }
        $('input[name="quantity"]').val(quntyti);
        $('.quan').text(quntyti);
        if (isNaN(price)) {
            $('.price-prod').text((ferst_amount * quntyti) + '.00 ' + currency)
        } else {
            $('.price-prod').text((parseInt(ferst_amount) + parseInt(price)) * quntyti + '.00 ' + currency)
        }

    });
    $('#plus').on('click', function () {
        ++quntyti;
        console.log(price);

        $('input[name="quantity"]').val(quntyti);
        $('.quan').text(quntyti);
        if (isNaN(price)) {
            $('.price-prod').text((ferst_amount * quntyti) + '.00 ' + currency)
        } else {
            $('.price-prod').text((parseInt(ferst_amount) + parseInt(price)) * quntyti + '.00 ' + currency)
        }

    });



    // Модалка для добавления отзыва

    $('.btn-my.btn-all-otzivi').on('click',function (e) {
        e.preventDefault();
        $('#trigger-modal').trigger('click');
        $('input[name="product_id"]').val(<?=$data['product_id']?>);
        $('input[name="review_id"]').val(0);
    });

    $('a.color-theme').on('click',function (e) {
        e.preventDefault();
        $('#trigger-modal').trigger('click');
        var review_id = $(this).data('reviewIdXtz');
        $('input[name="review_id"]').val(review_id);
    });

    // лайки
    $('.like-dislike').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('reviewId');
        var like = $(this).data('like');
        var x = $(this).parent().find('span').text();
        $(this).parent().find('span').text(++x);
        $(this).parent().parent().css({'cursor':'default','pointer-events':'none'});
        $.ajax({
            url: 'index.php?route=product/product/update_like',
            type: 'post',
            data: {'review_id': id, 'like': like},
            success: function (json) {
            },
            error: function (jqXHR, textStatus, errorThrown) {

                console.log('ERRORS: ' + textStatus);
            }
        });
    });

    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            //data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            data: $('.prod-kol input[type=\'text\'], .prod-kol select, .prod-kol input[type=\'hidden\']'),
            //data: 'product_id=' + $('input[name="product_id"]').val()+'&quantity='+$('.quan').html(),
            //data: 'product_id=' + $('input[name="product_id"]').val()+'&quantity='+$('.quan').html()+'&option='+select
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

//                    $('#cart-total').html(json['total']);
                    $('#cart-total').html(' <img src="image/basket.png"> <div class="count-top-header-bl">'+json['total']+'</div>');

                    $('#cart-total-xtz-left').text(json['total']);
                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            }
        });
    });
    //--></script>


