<?php echo $header; ?>

<style>
    .alert-success, .alert-info{
        display: none!important;
    }
    .product-wrap {
        overflow: hidden!important;
    }
</style>
<div class="wrapper" id="content">
    <div class="container">
        <div class="sort-wr">
            <div class="sort row flex-wrap justify-content-between">
                <div class="back-cat col-12 col-md-5 align-self-center">
                    <?php $i = 0;
                    foreach ($breadcrumbs as $breadcrumb) {
                        $i++; ?>
                        <?php if ($i == 1) {
                            $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                        } else {
                            $class = '';
                        } ?>
                        <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>

                            <span> /</span></a>
                    <?php } ?>
                </div>

                <div class="sort-block col-12 col-md-7 align-self-center">
                    <div class="d-flex flex-wrap justify-content-end">
                      <?php if ($product_total > $config_limit){ ?>

                        <div class="pl-2 pr-0">
                            <div class="form-group input-group input-group-sm">
                                <label class="input-group-addon" for="input-limit">Показать:</label>
                                <select id="input-limit" class="form-control" onchange="location = this.value;">

                                    <?php foreach ($limits as $limits) { ?>
                                        <?php if ($limits['value'] == $limit) { ?>
                                            <option value="<?php echo $limits['href']; ?>"
                                                    selected="selected"><?php echo $limits['text']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                          <?php echo $column_right; ?>
                        <?php } ?>

                        <div class="p-2 xtz-grid-view">
                            <button style="background-color: transparent;border: none;cursor: pointer" type="button" id="grid-view"
                                    class="sort-icon" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i
                                        class="fa fa-th-large"></i></button>
                        </div>
                        <div class="p-2 xtz-list-view">
                            <button style="background-color: transparent;border: none;cursor: pointer" type="button" id="list-view"
                                    class="sort-icon" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i
                                        class="fa fa-th-list"></i></button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php if (!$description){?>
        <div class="catalog-pdf d-flex justify-content-end">
            <a href="/file_for_download/retail_price_list.txt" download=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i><?=$text_file_download;?></a>
        </div>
        <?php } ?>
        <div class="wrapper-wwr row flex-wrap justify-content-between">
            <div class="col-12 col-md-12 col-lg-9 content-wrapper">
                <div class="H1-catalog-x">
                    <?php if ($category_id == 102){ ?>
                        <h1><?=$text_all_category_x;?></h1>
                    <?php }else{?>
                        <h1><?=$category_name;?></h1>
                    <?php }?>

                </div>

                <?php if ($image_2 || $description) { ?>
                    <?php
                    if ($description && $description != '<p><br></p>'){
                        $style = 'block';
                    }else{
                        $style = 'none';
                    }

                    ?>
                <div class="content-catalog" style="display: <?=$style;?>">
                    <div class="row justify-content-between">


                            <?php if ($image_2) { ?>
                                <div class="col-12 col-md-12 col-lg-4 img_content">
                                    <img src="<?php echo $image_2; ?>" alt="" class="img-fluid">
                                </div>
                            <?php } ?>

                            <?php if ($description) { ?>
                                <div class="col-12 col-md-12 col-lg-8"><?php echo $description; ?></div>
                            <?php } ?>

                    </div>

                </div>
                <?php } ?>
                <div class="catalog-item row">
                    <?php

                    $i = 0;
                    foreach ($products as $product) {$i++; ?>


                        <div class="col-12 col-md-6 col-lg-4 wr-page-product-wrap product-layout">
                            <div class="product-wrap">
                                <div class="foto-prod">
                                    <a href="<?php echo $product['href']; ?>"><img
                                                src="<?php echo $product['thumb']; ?>"
                                                alt="<?php echo $product['name']; ?>"
                                                title="<?php echo $product['name']; ?>" class="d-block w-100 img-fluid"/>
                                        <div class="hiden-opt">
                                            <div class="dop-opt category-x">
                                              <?php if (isset($product['attribute_groups'][0])) { ?>
                                                <?php foreach ($product['attribute_groups'][0]["attribute"] as $key => $attribute) { ?>
                                                  <?php if ($key <= 7){ ?>
                                                    <div class="wr-opt-prd">
                                                        <div class="opt-prd">
                                                          <?php if (isset($attribute["svg"])){ ?>
                                                            <?php echo $attribute["svg"]; ?>
                                                          <?php }else{ ?>
                                                            <img src="<?php echo $attribute["image"] ?>" alt="">
                                                          <?php } ?>
                                                          <span><?= $attribute["text"]; ?></span>
                                                        </div>
                                                    </div>
                                                  <?php }else{} ?>

                                                <?php } ?>
                                              <?php } ?>
                                            </div>
                                            <div class="dop-prod-btn d-flex">
                                              <?php if ($product['in_compare']) {?>
                                                <button type="button" style="border: none;cursor: pointer"
                                                        data-toggle="tooltip" class="sr-btn-prod active"
                                                        title="<?php echo $text_in_compare; ?>"
                                                        onclick="window.location.href='<?=$product['in_compare']?>';return false;"><i class="fas fa-balance-scale"></i></button>
                                              <?php } else { ?>
                                                <button type="button" style="border: none;cursor: pointer"
                                                        data-toggle="tooltip" class="sr-btn-prod"
                                                        title="<?php echo $button_compare; ?>"
                                                        onclick="compare.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="fas fa-balance-scale"></i></button>
                                              <?php } ?>
                                              <?php if ($product['in_wishlist']) { ?>
                                                <button type="button" style="border: none;cursor: pointer"
                                                        data-toggle="tooltip" class="wish-btn-prod active"
                                                        title="<?php echo $text_in_wishlist; ?>"
                                                        onclick="window.location.href='<?=$product['in_wishlist']?>';return false;"><i class="far fa-heart"></i></button>
                                              <?php } else { ?>
                                                <button type="button" style="border: none;cursor: pointer"
                                                        data-toggle="tooltip" class="wish-btn-prod"
                                                        title="<?php echo $button_wishlist; ?>"
                                                        onclick="wishlist.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="far fa-heart"></i></button>
                                              <?php } ?>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="view-horizontal-options" >
                                      <?php if (isset($product['attribute_groups'][0])) { ?>
                                        <?php foreach ($product['attribute_groups'][0]["attribute"] as $attribute) { ?>
                                            <span><?= $attribute["name"]; ?></span> <?= $attribute["text"]; ?> <br/>
                                        <?php } ?>
                                      <?php } ?>
                                    </div>
                                </div>
                                <a style="text-decoration: none" href="<?php echo $product['href']; ?>">  <div class="name-prod"><?php echo $product['name']; ?> </div></a>

                                <div class="d-flex flex-wrap justify-content-center align-items-center btn-mar">
                                    <?php if ($product['quantity'] > 0) { ?>
                                      <button type="button" class="prod-btn xtz_button"
                                              data-id="<?php echo $product['product_id']; ?>" onclick="return false;"  data-toggle="modal" data-target="#modalcat" >
                                          <div class="d-flex align-items-center">
                                              <div class="icon-prod-btn"></div>
                                              <div class="text-prod-btn">
                                                  <?php echo mb_substr( $product['price'], 1) ; ?>
                                                  <span><?php echo $button_cart; ?></span>
                                              </div>
                                          </div>
                                      </button>
                                    <?php } else { ?>
                                      <button type="button" class="entrance-btn xtz_button" onclick="modalEntrance(<?php echo $product['product_id']; ?>)">
                                          <div class="d-flex align-items-center">
                                              <div class="text-prod-btn">
                                                  <span><?php echo $button_entrance; ?></span>
                                              </div>
                                          </div>
                                      </button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>


                <div class="pag-all-page d-flex flex-wrap justify-content-center">

                    <nav aria-label="Page navigation example">

                        <?php echo $pagination; ?>


                    </nav>

                    <?php if ((int)$all_products > 9 ){?>
                    <a href="<?=$all;?>" class="all-vis-tovar">показать все</a>
                    <?php } ?>
                </div>
                <div class="wr-all-news">
                    <?php if ((int)$all_products > 9 ){?>
                    <a href="" class="btn-all-news">Показать больше</a>
                    <?php } ?>
                </div>

                <?php
                if ($description_2  && $description_2 != '<p><br></p>'){
                    $style = 'block';
                }else{
                    $style = 'none';
                }
                ?>
                <?php if ($description_2) { ?>
                <div class="content-catalog" style="display: <?=$style;?>">
                    <div class="row justify-content-between">

                    <div class="col-12 col-md-12 col-lg-8"><?php echo $description_2; ?></div>

                    </div>
                </div>
                <?php } ?>

            </div>
            <div class="col-12 col-md-12 col-lg-3 sidebar-wrapper">
                <?php echo $column_left; ?>

            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function () {
        $('.prod-btn').click(function () {
           var id = $(this).attr('data-id');
           $.ajax({
               url: '/index.php?route=product/qproduct',
               type: 'get',
               data: 'product_id='+id,
               success: function (data) {
                   $('.qwerty').html(data);
               }
           });
        });


        $$('button.btn.btn-default.btn-block').css({'display':'none'});
        $('.ocfilter-option').css({'display':'block','border':'none'});
        $('div#ocfilter-hidden-options').css({'display':'block'});
        $('.option-values').css({'display':'block'});
    });

    $('div.option-name').on('click',function () {


        var content = $(this).next();
        if (content.is(":visible")) {
            //если нажали на title аккордеона,
            content.slideUp(500, function () {//и если контент аккордеона видимый, то
            }); //убираем его
            $(this).children().removeClass("active"); //убираем активный класс у стрелки к примеру
            $(this).removeClass("active");
        } else {
            $(".div.option-name").slideUp("slow"); //если невидимый, прячем все скрытые
            $(".accordion .accordion_title").children() //убираем активный класс у стрелки к примеру
                .removeClass("active");
            $(".accordion_title").removeClass("active"); //убираем активный класс у стрелки к примеру
            content.slideToggle("slow"); //открываем скрытый блок у того что нажали
            $(this).children().addClass("active"); //добавляем активный класс у стрекли к примеру
            $(this).addClass("active");
        }


        // var div = $(this).next();
        // div.fadeIn("slow");
    });

</script>


<?php echo $footer; ?>
