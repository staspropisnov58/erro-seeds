<?php echo $header; ?>

    <div class="wrapper bg-black-page">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">
                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="compare-page">
                <?php if ($products) { ?>
                    <table class="table compare-table">
                        <tbody>
                        <tr>
                            <td><?php echo $text_name; ?></td>
                            <?php foreach ($products as $product) { ?>
                                <td class="text-center name-com"><a
                                            href="<?php echo $products[$product['product_id']]['href']; ?>"><strong><?php echo $products[$product['product_id']]['name']; ?></strong></a>
                                </td>
                            <?php } ?>

                        </tr>
                        <tr>


                            <td><?php echo $text_image; ?></td>
                            <?php foreach ($products as $product) { ?>
                                <td class="text-center"><?php if ($products[$product['product_id']]['thumb']) { ?>
                                        <img src="<?php echo $products[$product['product_id']]['thumb']; ?>"
                                             alt="<?php echo $products[$product['product_id']]['name']; ?>"
                                             title="<?php echo $products[$product['product_id']]['name']; ?>"
                                             class="img-fluid"/>
                                    <?php } ?></td>
                            <?php } ?>
                        </tr>

                        <?php if ($review_status) { ?>
                            <td><?php echo $text_rating . ':'; ?></td>
                            <?php foreach ($products as $product) { ?>
                                <td class="rating">


                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($products[$product['product_id']]['rating'] < $i) { ?>
                                            <span class="fa fa-stack"><img src="image/grey-icon.png"></span>
                                        <?php } else { ?>
                                            <span class="fa fa-stack"><img src="image/green-icon.png"></span>
                                        <?php } ?>
                                    <?php } ?>

                                </td>
                            <?php } ?>
                        <?php } ?>
                        </tr>


                        <tr>
                            <td><?php echo $text_price; ?></td>
                            <?php foreach ($products as $product) { ?>
                                <?php if ($products[$product['product_id']]['special']) {
                                    $sum = $products[$product['product_id']]['special'];
                                } else {
                                    $sum = $products[$product['product_id']]['price'];
                                } ?>

                                <?php if ($products[$product['product_id']]['special']) {
                                    $old_sum = mb_substr($products[$product['product_id']]['price'], 1);
                                } else {
                                    $old_sum = '';
                                } ?>

                                <td class="text-center">
                                    <div class="old-price" style="display:inline;"><?= $old_sum; ?> </div>
                                    <div class="price-prod" style="display:inline;"><?= mb_substr($sum, 1); ?></div>
                                </td>

                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?php echo $text_availability; ?></td>
                            <?php foreach ($products as $product) { ?>
                                <td class="text-center bg1-td"><?php echo $products[$product['product_id']]['availability']; ?></td>
                            <?php } ?>
                        </tr>

                        <?php foreach ($attribute_groups as $attribute_group) { ?>

                            <?php $i = 0;
                            foreach ($attribute_group['attribute'] as $key => $attribute) {
                                $i++ ?>

                                <tr>
                                    <td><?php echo $attribute['name']; ?></td>
                                    <?php foreach ($products as $product) { ?>
                                        <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>

                                            <?php if ($i % 2 == 0) {
                                                $z = 1;
                                            } else {
                                                $z = 2;
                                            } ?>

                                            <td class="text-center bg<?= $z ?>-td"><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>
                                        <?php } else { ?>
                                            <td></td>
                                        <?php } ?>
                                    <?php } ?>
                                </tr>

                            <?php } ?>
                        <?php } ?>


                        <tr>
                            <td></td>

                            <?php foreach ($products as $product) { ?>
                                <td>
                                    <div class="compare-btn d-flex justify-content-between align-items-center">


                                        <div class="btn-cart-cop p-2">
                                          <?php if($product['quantity'] > 0) { ?>
                                            <span class="prod-btn" style="cursor: pointer"
                                                  data-id="<?php echo $product['product_id']; ?>" onclick="return false;"  data-toggle="modal" data-target="#modalcat" >
                                                <input type="hidden" value="<?php echo $button_cart; ?>"/>
                                                <div class="d-flex align-items-center">
                                                    <div class="icon-prod-btn"></div>
                                                    <div class="text-prod-btn">
                                                        <?php echo $button_cart; ?>
                                                    </div>
                                                </div>
                                            </span>
                                          <?php } else { ?>
                                            <button type="button" class="entrance-btn xtz_button" onclick="modalEntrance(<?php echo $product['product_id']; ?>)"  data-toggle="modal" data-target="#modalentrance" >
                                                <div class="d-flex align-items-center">
                                                    <div class="text-prod-btn">
                                                        <span><?php echo $button_entrance; ?></span>
                                                    </div>
                                                </div>
                                            </button>
                                          <?php } ?>
                                        </div>

                                        <div class="del-compare p-2">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                            <a href="<?php echo $product['remove']; ?>"><?php echo $button_remove; ?></a>
                                        </div>

                                    </div>
                                </td>
                            <?php } ?>


                        </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.prod-btn').click(function () {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: '/index.php?route=product/qproduct',
                    type: 'get',
                    data: 'product_id='+id,
                    success: function (data) {
                        $('.qwerty').html(data);
                    }
                });
            });
        });
    </script>
<?php echo $footer; ?>
