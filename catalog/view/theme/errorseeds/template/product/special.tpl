<?php echo $header; ?>
<style>
    .product-wrap {
        overflow: hidden;
    }
</style>
    <div class="wrapper" >
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                  <div class="back-cat back-cat2 col-12 d-flex">
                    <?php $i = 0;
                                foreach ($breadcrumbs as $breadcrumb) {
                                    $i++; ?>
                    <?php if ($i == 1) {
                                        $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                                    } else {
                                        $class = '';
                                    } ?>
                    <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                        <span> /</span></a>
                    <?php } ?>
                  </div>
                    <?php if (!empty($products)) { ?>
                    <div class="sort-block col-12 col-md-7 align-self-center">
                        <div class="d-flex justify-content-end">
                            <div class="p-2">
                                <div class="form-group input-group input-group-sm">
                                  <?php if ($product_total > $config_limit){ ?>

                                    <label class="input-group-addon" for="input-limit">Показать:</label>
                                    <select id="input-limit" class="form-control" onchange="location = this.value;">
                                        <?php foreach ($limits as $limits) { ?>
                                            <?php if ($limits['value'] == $limit) { ?>
                                                <option value="<?php echo $limits['href']; ?>"
                                                        selected="selected"><?php echo $limits['text']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                  <?php } ?>
                                </div>
                            </div>
                            <?php if ($product_total > $config_limit){ ?>

                              <?php echo $column_right; ?>

                            <?php } ?>


<!--                            <div class="p-2">-->
<!--                                <button style="background-color: transparent;border: none" type="button" id="grid-view"-->
<!--                                        class="sort-icon" data-toggle="tooltip" title="--><?php //echo $button_grid; ?><!--"><i-->
<!--                                            class="fa fa-th-large"></i></button>-->
<!--                            </div>-->
<!--                            <div class="p-2">-->
<!--                                <button style="background-color: transparent;border: none" type="button" id="list-view"-->
<!--                                        class="sort-icon" data-toggle="tooltip" title="--><?php //echo $button_list; ?><!--"><i-->
<!--                                            class="fa fa-th-list"></i></button>-->
<!--                            </div>-->

                        </div>
                    </div>
                  <?php } else { ?>
                    <div class="sort-block col-12 col-md-7 align-self-center"></div>
                  <?php } ?>
                </div>
            </div>
            <?php if (!empty($products)) { ?>
            <div class="wrapper-wwr row flex-wrap justify-content-between">
                <div class="col-12 col-md-9 content-wrapper">
                  <div class="H1-catalog-x">
                          <h1><?=$heading_title;?></h1>
                  </div>
<!--                    <div class="content-catalog">-->
<!--                        <div class="row justify-content-between">-->
<!--                            --><?php //if ($thumb || $description) { ?>
<!--                                --><?php //if ($thumb) { ?>
<!--                                    <div class="col-12 col-md-4 align-self-center">-->
<!--                                        <img src="--><?php //echo $thumb; ?><!--" alt="" class="img-fluid">-->
<!--                                    </div>-->
<!--                                --><?php //} ?>
<!--                                --><?php //if ($description) { ?>
<!--                                    <div class="col-12 col-md-8">--><?php //echo $description; ?><!--</div>-->
<!--                                --><?php //} ?>
<!--                            --><?php //} ?>
<!--                        </div>-->
<!--                    </div>-->
                    <div class="catalog-item row">
                        <?php
                        $i = 0;
                        foreach ($products as $product) {$i++; ?>

                            <div class="col-12 col-md-4 wr-page-product-wrap">
                                <div class="product-wrap" style="overflow: hidden">
                                    <div class="foto-prod">
                                        <a href="<?php echo $product['href']; ?>"><img
                                                    src="<?php echo $product['thumb']; ?>"
                                                    alt="<?php echo $product['name']; ?>"
                                                    title="<?php echo $product['name']; ?>" class="iimg-fluid"/>
                                            <div class="hiden-opt">
                                                <div class="dop-opt">
                                                  <?php if (isset($product['attribute_groups'][0])) { ?>
                                                    <?php foreach ($product['attribute_groups'][0]["attribute"] as $key => $attribute) { ?>
                                                      <?php if ($key <= 7){ ?>
                                                        <div class="wr-opt-prd">
                                                            <div class="opt-prd">
                                                              <?php if (isset($attribute["svg"])){ ?>
                                                                <?php echo $attribute["svg"]; ?>
                                                              <?php }else{ ?>
                                                                <img src="<?php echo $attribute["image"] ?>" alt="">
                                                              <?php } ?>
                                                              <span><?= $attribute["text"]; ?></span>
                                                            </div>
                                                        </div>
                                                      <?php }else{} ?>
                                                    <?php } ?>
                                                  <?php } ?>
                                                </div>
                                                <div class="dop-prod-btn d-flex">
                                                  <?php if ($product['in_compare']) {?>
                                                    <button type="button" style="border: none;cursor: pointer"
                                                            data-toggle="tooltip" class="sr-btn-prod active"
                                                            title="<?php echo $text_in_compare; ?>"
                                                            onclick="window.location.href='<?=$product['in_compare']?>';return false;"><i class="fas fa-balance-scale"></i></button>
                                                  <?php } else { ?>
                                                    <button type="button" style="border: none;cursor: pointer"
                                                            data-toggle="tooltip" class="sr-btn-prod"
                                                            title="<?php echo $button_compare; ?>"
                                                            onclick="compare.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="fas fa-balance-scale"></i></button>
                                                  <?php } ?>
                                                  <?php if ($product['in_wishlist']) { ?>
                                                    <button type="button" style="border: none;cursor: pointer"
                                                            data-toggle="tooltip" class="wish-btn-prod active"
                                                            title="<?php echo $text_in_wishlist; ?>"
                                                            onclick="window.location.href='<?=$product['in_wishlist']?>';return false;"><i class="far fa-heart"></i></button>
                                                  <?php } else { ?>
                                                    <button type="button" style="border: none;cursor: pointer"
                                                            data-toggle="tooltip" class="wish-btn-prod"
                                                            title="<?php echo $button_wishlist; ?>"
                                                            onclick="wishlist.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="far fa-heart"></i></button>
                                                  <?php } ?>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="name-prod"><?php echo $product['name']; ?></div>
                                    <div class="d-flex flex-wrap justify-content-center align-items-center btn-mar">
                                      <?php if ($product['quantity'] > 0) { ?>
                                        <button type="button" class="prod-btn xtz_button"
                                                data-id="<?php echo $product['product_id']; ?>" onclick="return false;"  data-toggle="modal" data-target="#modalcat" >
                                            <div class="d-flex align-items-center">
                                                <div class="icon-prod-btn"></div>
                                                <div class="text-prod-btn">
                                                    <?php echo mb_substr( $product['price'], 1) ; ?>
                                                    <span><?php echo $button_cart; ?></span>
                                                </div>
                                            </div>
                                        </button>
                                      <?php } else { ?>
                                        <button type="button" class="entrance-btn xtz_button" onclick="modalEntrance(<?php echo $product['product_id']; ?>)">
                                            <div class="d-flex align-items-center">
                                                <div class="text-prod-btn">
                                                    <span><?php echo $button_entrance; ?></span>
                                                </div>
                                            </div>
                                        </button>
                                      <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($pagination){ ?>

                    <div class="pag-all-page d-flex flex-wrap justify-content-center">

                        <nav aria-label="Page navigation example">

                            <?php echo $pagination; ?>


                        </nav>
                        <a href="<?=$all?>" class="all-vis-tovar">показать все</a>
                    </div>
                  <?php } ?>
                  <?php if ($pagination){ ?>

                    <div class="wr-all-news">
                        <a href="" class="btn-all-news">Показать больше</a>
                    </div>
                  <?php } ?>

                </div>
                <div class="col-12 col-md-3 sidebar-wrapper">
                    <?php echo $column_left; ?>
                </div>
            </div>
          <?php } else { ?>
            <div class="wrapper-wwr no_products">
              <div class="H1-catalog-x">
                      <h1><?=$heading_title;?></h1>
              </div>
              <img src="catalog/view/theme/errorseeds/images/no_products.png" alt="no_products">
              <p><?php echo $text_no_products; ?></p>
            </div>
          <?php } ?>
        </div>
    </div>
    <script>

    $(document).ready(function () {
        $('.prod-btn').click(function () {
           var id = $(this).attr('data-id');
           $.ajax({
               url: '/index.php?route=product/qproduct',
               type: 'get',
               data: 'product_id='+id,
               success: function (data) {
                   $('.qwerty').html(data);
               }
           });
        });



        $('button.btn.btn-default.btn-block').css({'display':'none'});
        $('.ocfilter-option').css({'display':'block','border':'none'});
        $('div#ocfilter-hidden-options').css({'display':'block'});
    });

    $('div.option-name').on('click',function () {


        var content = $(this).next();
        if (content.is(":visible")) {
            //если нажали на title аккордеона,
            content.slideUp(500, function () {//и если контент аккордеона видимый, то
            }); //убираем его
            $(this).children().removeClass("active"); //убираем активный класс у стрелки к примеру
            $(this).removeClass("active");
        } else {
            $(".div.option-name").slideUp("slow"); //если невидимый, прячем все скрытые
            $(".accordion .accordion_title").children() //убираем активный класс у стрелки к примеру
                .removeClass("active");
            $(".accordion_title").removeClass("active"); //убираем активный класс у стрелки к примеру
            content.slideToggle("slow"); //открываем скрытый блок у того что нажали
            $(this).children().addClass("active"); //добавляем активный класс у стрекли к примеру
            $(this).addClass("active");
        }


        // var div = $(this).next();
        // div.fadeIn("slow");
    });



</script>
<?php echo $footer; ?>
