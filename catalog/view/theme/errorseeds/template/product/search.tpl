<?php echo $header; ?>
<style>
    .product-wrap {
        overflow: hidden;
    }
</style>
<div class="wrapper" >
    <div class="container">
        <div class="sort-wr">
            <div class="sort row flex-wrap justify-content-between">
                <div class="back-cat col-12 col-md-5 align-self-center">
                    <?php $i = 0;
                    foreach ($breadcrumbs as $breadcrumb) {
                        $i++; ?>
                        <?php if ($i == 1) {
                            $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                        } else {
                            $class = '';
                        } ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                            <span> /</span></a>
                    <?php } ?>
                </div>
            <?php if ($products){?>
                <div class="sort-block col-12 col-md-7 align-self-center">
                    <div class="d-flex justify-content-end">
                        <div class="p-2">
                            <div class="form-group input-group input-group-sm">
                                <label class="input-group-addon" for="input-limit">Показать:</label>
                                <select id="input-limit" class="form-control" onchange="location = this.value;">
                                    <?php foreach ($limits as $limits) { ?>
                                        <?php if ($limits['value'] == $limit) { ?>
                                            <option value="<?php echo $limits['href']; ?>"
                                                    selected="selected"><?php echo $limits['text']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php echo $column_right; ?>

<!--                        <div class="p-2">-->
<!--                            <button style="background-color: transparent;border: none" type="button" id="grid-view"-->
<!--                                    class="sort-icon" data-toggle="tooltip" title="--><?php //echo $button_grid; ?><!--"><i-->
<!--                                        class="fa fa-th-large"></i></button>-->
<!--                        </div>-->
<!--                        <div class="p-2">-->
<!--                            <button style="background-color: transparent;border: none" type="button" id="list-view"-->
<!--                                    class="sort-icon" data-toggle="tooltip" title="--><?php //echo $button_list; ?><!--"><i-->
<!--                                        class="fa fa-th-list"></i></button>-->
<!--                        </div>-->

                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php if ($products){?>
        <div class="wrapper-wwr row flex-wrap justify-content-between">
            <div class="col-12 col-md-9 content-wrapper">

                <div class="catalog-item row">
                    <?php
                    $i = 0;
                    foreach ($products as $product) {$i++; ?>

                        <div class="col-12 col-md-4 wr-page-product-wrap">
                            <div class="product-wrap">
                                <div class="foto-prod">
                                    <a href="<?php echo $product['href']; ?>"><img
                                                src="<?php echo $product['thumb']; ?>"
                                                alt="<?php echo $product['name']; ?>"
                                                title="<?php echo $product['name']; ?>" class="iimg-fluid"/>
                                        <div class="hiden-opt">
                                            <div class="dop-opt">
                                              <?php if (isset($product['attribute_groups'][0])) { ?>
                                                <?php foreach ($product['attribute_groups'][0]["attribute"] as $key => $attribute) { ?>
                                                  <?php if ($key <= 7){ ?>
                                                    <div class="wr-opt-prd">
                                                        <div class="opt-prd">
                                                          <?php if (isset($attribute["svg"])){ ?>
                                                            <?php echo $attribute["svg"]; ?>
                                                          <?php }else{ ?>
                                                            <img src="<?php echo $attribute["image"] ?>" alt="">
                                                          <?php } ?>
                                                          <span><?= $attribute["text"]; ?></span>
                                                        </div>
                                                    </div>
                                                  <?php }else{} ?>

                                                <?php } ?>
                                              <?php } ?>
                                            </div>
                                              <div class="dop-prod-btn d-flex">
                                                <?php if ($product['in_compare']) {?>
                                                  <button type="button" style="border: none;cursor: pointer"
                                                          data-toggle="tooltip" class="sr-btn-prod active"
                                                          title="<?php echo $text_in_compare; ?>"
                                                          onclick="window.location.href='<?=$product['in_compare']?>';return false;"><i class="fas fa-balance-scale"></i></button>
                                                <?php } else { ?>
                                                  <button type="button" style="border: none;cursor: pointer"
                                                          data-toggle="tooltip" class="sr-btn-prod"
                                                          title="<?php echo $button_compare; ?>"
                                                          onclick="compare.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="fas fa-balance-scale"></i></button>
                                                <?php } ?>
                                                <?php if ($product['in_wishlist']) { ?>
                                                  <button type="button" style="border: none;cursor: pointer"
                                                          data-toggle="tooltip" class="wish-btn-prod active"
                                                          title="<?php echo $text_in_wishlist; ?>"
                                                          onclick="window.location.href='<?=$product['in_wishlist']?>';return false;"><i class="far fa-heart"></i></button>
                                                <?php } else { ?>
                                                  <button type="button" style="border: none;cursor: pointer"
                                                          data-toggle="tooltip" class="wish-btn-prod"
                                                          title="<?php echo $button_wishlist; ?>"
                                                          onclick="wishlist.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="far fa-heart"></i></button>
                                                <?php } ?>
                                              </div>
                                        </div>
                                    </a>

                                </div>
                                <div class="name-prod"><?php echo $product['name']; ?></div>
                                <div class="d-flex flex-wrap justify-content-center align-items-center btn-mar">
                                  <?php if ($product['quantity'] > 0) { ?>
                                    <button type="button" class="prod-btn" style="background: #2a2a2a;cursor: pointer"
                                            data-id="<?php echo $product['product_id']; ?>" onclick="return false;"  data-toggle="modal" data-target="#modalcat" >
                                        <div class="d-flex align-items-center">
                                            <div class="icon-prod-btn"></div>
                                            <div class="text-prod-btn">
                                                <?php echo mb_substr( $product['price'], 1) ; ?>
                                                <span><?php echo $button_cart; ?></span>
                                            </div>
                                        </div>
                                    </button>
                                  <?php } else { ?>
                                    <button type="button" class="entrance-btn xtz_button" onclick="modalEntrance(<?php echo $product['product_id']; ?>)"  data-toggle="modal" data-target="#modalentrance" >
                                        <div class="d-flex align-items-center">
                                            <div class="text-prod-btn">
                                                <span><?php echo $button_entrance; ?></span>
                                            </div>
                                        </div>
                                    </button>
                                  <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="pag-all-page d-flex flex-wrap justify-content-center">

                    <nav aria-label="Page navigation example">

                        <?php echo $pagination; ?>


                    </nav>
                    <a href="<?=$all;?>" class="all-vis-tovar">показать все</a>
                </div>
<!--                <div class="wr-all-news">-->
<!--                    <a href="" class="btn-all-news">Показать больше</a>-->
<!--                </div>-->
            </div>
            <div class="col-12 col-md-3 sidebar-wrapper">
<!--                --><?php //echo $column_left; ?>
<!--                <img src="images/filter.jpg" class="img-fluid">-->
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.prod-btn').click(function () {
            var id = $(this).attr('data-id');
            $.ajax({
                url: '/index.php?route=product/qproduct',
                type: 'get',
                data: 'product_id='+id,
                success: function (data) {
                    $('.qwerty').html(data);
                }
            });
        });
    });
</script>

<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>
