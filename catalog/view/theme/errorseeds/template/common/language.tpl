<?php if (count($languages) > 1) { ?>
    <div class="lang-bl ic-top-r2 drop-wr">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" id="wr-lang-bl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php foreach ($languages as $language) { ?>
                        <?php if ($language['code'] == $code) { ?>
                            <?=$code;?>
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                        <?php } ?>
                    <?php } ?>
                   </button>
                <div class="dropdown-menu" aria-labelledby="wr-lang-bl">
                    <?php foreach ($languages as $language) { ?>
                        <a href="<?php echo $language['code']; ?>" class="dropdown-item<?php echo $language['code'] == $code ? ' active' : ''; ?>"> <?php echo $language['name']; ?></a>
                    <?php } ?>
                </div>
            </div>
            <input type="hidden" name="code" value="" />
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        </form>
    </div>
<?php } ?>
