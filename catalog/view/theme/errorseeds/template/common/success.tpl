<?php echo $header; ?>
<div class="container" style="display: none">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } else { ?>
        <li><?php echo $breadcrumb['text']; ?></li>
      <?php } ?>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>


    <div class="wrapper">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">
                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="404-page text-center">
              <div class="img_success">
                <img src="catalog/view/theme/errorseeds/images/success.png" alt="" class="d-block mx-auto img-fluid">
                <span><?php echo $word_success; ?></span>
              </div>
                <div class="con-404-page col-10 col-md-5 mx-auto">
                    <?php echo $text_message; ?>
                    <?php if(isset($text_link)){ ?>
                      <?php echo $text_link; ?>
                    <?php } ?>
                    <div class="btn-zv-green">
                        <div class="d-flex align-items-center">
                            <img src="image/ic-cat2.png" alt="кабинет">
                            <a href="<?php echo $continue; ?>" style="color: white" ><?php echo $button_continue; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo $footer; ?>
