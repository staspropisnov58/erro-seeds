<?php echo $header; ?>

<?php if ($categories) { ?>
    <div class="container">
        <nav id="menu" class="navbar">
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <?php foreach ($categories as $category) { ?>
                        <?php if ($category['children']) { ?>
                            <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle"
                                                    data-toggle="dropdown"><?php echo $category['name']; ?></a>
                                <div class="dropdown-menu">
                                    <div class="dropdown-inner">
                                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                            <ul class="list-unstyled">
                                                <?php foreach ($children as $child) { ?>
                                                    <li>
                                                        <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </div>
                                    <a href="<?php echo $category['href']; ?>"
                                       class="see-all"><?php echo $category['name']; ?></a>
                                </div>
                            </li>
                        <?php } else { ?>
                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </nav>
    </div>
<?php } ?>

<!--    <div class="container">-->
<!--    <div class="row">--><?php //echo $column_left; ?>
<?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
<?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
<?php } ?>
<!--<div id="content" class="<?php /*echo $class; */ ?>">--><?php /*echo $content_top;*/ ?><?php echo $content_bottom; ?><!--</div>-->
<?php echo $column_right; ?>


<!--</div>-->
<!--</div>-->
<?php echo $footer; ?>
