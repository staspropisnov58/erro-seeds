<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<!--<![endif]-->


<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <?php if(empty($robots)) { ?>
    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
    <?php } ?>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if ($icon) { ?>
        <link href="<?php echo $icon; ?>" rel="icon"/>
    <?php } ?>
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php if ($robots) { ?>
      <meta name="robots" content="<?php echo $robots; ?>"/>
    <?php } ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php
    $x = explode('/', trim(utf8_strtolower($_SERVER['REQUEST_URI']), '/'));
    $url = "";
    for ($i = 0; $i < count($x); $i++) {
        if ($x[$i] == 'ua' || $x[$i] == 'es' || $x[$i] == 'en') {
            continue;
        }
        $url .= '/' . $x[$i];
    }
    $base_xtz = 'http://' . $_SERVER['HTTP_HOST'];
    ?>
      <?php if (isset($linkes)){ ?>
      <?php foreach($linkes['hreflang'] as $key => $linkeses){ ?>
        <link rel="alternate" hreflang="<?php echo $linkeses['hreflang']; ?>" href="<?php echo $linkes['href'][$key]; ?>" />
      <?php } ?>
    <?php } ?>






    <link href="/catalog/view/javascript/ocfilter/nouislider.min.css" type="text/css" rel="stylesheet"
          media="screen"/>
    <link href="/catalog/view/theme/default/stylesheet/ocfilter/ocfilter.css" type="text/css" rel="stylesheet"
          media="screen"/>

    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/bootstrap.min.css">
    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/font-awesome.min.css">
    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/owl.carousel.min.css">
    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/animate.css">
    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/slicknav.css">
    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/style.css">
    <link rel="stylesheet" href="catalog/view/theme/errorseeds/css/jquery.bxslider.css">
    <link rel='stylesheet'
          href='https://apimgmtstorelinmtekiynqw.blob.core.windows.net/content/MediaLibrary/Widget/Map/styles/map.css'/>
    <link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css"
          rel="stylesheet"
          media="screen"/>
    <link href="catalog/view/theme/default/stylesheet/ocdev_smart_one_page_checkout/stylesheet.css" type="text/css"
          rel="stylesheet"
          media="screen"/>


    <script src="/catalog/view/theme/errorseeds/js/jquery-3.3.1.min.js" defer></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.1.js" defer></script>
    <script src="/catalog/view/theme/errorseeds/js/popper.min.js" defer></script>
    <script src="/catalog/view/theme/errorseeds/js/bootstrap.min.js" defer></script>
    <script src="/catalog/view/theme/errorseeds/js/owl.carousel.min.js" defer></script>
    <script src="/catalog/view/theme/errorseeds/js/owl.carousel2.thumbs.js" defer></script>
    <script src="/catalog/view/theme/errorseeds/js/jquery.bxslider.min.js" defer></script>
    <script src="/catalog/view/theme/errorseeds/js/jquery.slicknav.js" defer></script>

    <script src="/catalog/view/theme/errorseeds/js/common.js" defer></script>
    <script src="/catalog/view/theme/errorseeds/js/device.js" defer></script>
    <script src="/catalog/view/javascript/common.js" type="text/javascript" defer></script>


    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>

    <?php echo $google_analytics; ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php if ($e_commerce) {
      echo $e_commerce;
    } ?>
</head>

<body>
  <?php if(empty($robots)) { ?>
      <!-- Google Tag Manager (noscript) -->
      <!-- End Google Tag Manager (noscript) -->
  <?php } ?>

<header id="sticker" class="header">
    <div class="top-header">
        <div class="container">
            <div class="d-flex justify-content-end flex-wrap">
                <div class="top-header-bl">
                    <div class="d-flex align-items-center align-items-center">
                        <div class="loop ic-top-r">
                            <a href="" id="do-search" ><i class="fa fa-search fa-search-none" aria-hidden="true" title="<?php echo $text_search; ?>" ></i></a>
                            <div id="input-search-q" ><?php echo $search; ?></div>
                        </div>
                        <div class="srav ic-top-r">


                            <a href="<?php echo $compare; ?>" id="compare-total" title="<?php echo $title_compare; ?>">

                                <div class="count-top-header-bl"><?= $text_compare; ?></div>

                            </a>
                        </div>
                        <div class="wish ic-top-r">
                            <a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $title_wishlist; ?>">
                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                                <div class="count-top-header-bl" style="top: 8px;"><?php echo $text_wishlist; ?></div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="top-header-bl-2">
                    <div class="d-flex justify-content-between">
                        <?= $language; ?>

                        <?= $currency; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wr-menu">
        <div class="container">
            <div class="row flex-wrap justify-content-start">
                <div class="col-5 col-md-2 logo">
                    <a href="">
                        <?php

                        if ($logo) { ?>
                            <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                                                alt="<?php echo $name; ?>" class="img-responsive"/></a>
                        <?php } else { ?>
                            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                        <?php } ?>
                    </a>
                </div>
                <div class="menu col-4 col-md-9">
                    <ul class="d-flex justify-content-start menu-all">
                        <li>
                            <a href=""><?= $text_home ?></a>
                        </li>
                        <li>
                            <a href="/about_us#h5"><?= $text_about_us; ?></a>
                            <!--                            <ul class="sub-menu">-->
                            <!--                                <li><a href="/testimonial">-->
                            <? //= $text_about_us_feedbacs; ?><!--</a></li>-->
                            <!--                                <li><a href="/contact-us/">-->
                            <? //= $text_about_us_contacts; ?><!--</a></li>-->
                            <!--                                <li><a href="/security">-->
                            <? //= $text_about_us_sequrity; ?><!--</a></li>-->
                            <!--                                <li><a href="/faq">-->
                            <? //= $text_about_us_faq; ?><!--</a></li>-->
                            <!--                            </ul>-->
                        </li>
                        <li><a href="/product/"><?= $text_compare_catalog; ?></a></li>
                        <li><a href="/partnership#h0"><?= $text_compare_partnership; ?></a></li>
                        <li><a href="/news/"><?= $text_compare_news; ?></a></li>
                        <li><a href="/contact-us/"><?= $text_about_us_contacts; ?></a></li>
                        <li><a href="#" target="_blank" rel="https://jahforum.org/" class="hidli"><?= $text_compare_forum; ?></a></li>
                    </ul>
                </div>

                <div class="col-3 col-md-1">
                    <div class="top-header-bl2">
                        <div class="d-flex justify-content-end">
                            <div class="basket ic-top-r">
                                <a href="/index.php?route=checkout/ocdev_smart_one_page_checkout" id="cart-total" title="<?php echo $text_basket; ?>">
                                    <img src="image/basket.png">
                                    <div class="count-top-header-bl"
                                         id="cart-total-xtz"><?php echo $cart_count; ?></div>
                                </a>
                            </div>

                            <div class="users ic-top-r">

                                <?php if ($logged) { ?>
                                    <a href="#" id="user_account_x"><i class="fa fa-user-o" style="color: #82b523;"
                                                                       aria-hidden="true"
                                                                       title="<?= $user_account_x; ?>"></i> </a>
                                <?php } else { ?>
                                    <a href="#" id="user_account_x"><i class="fa fa-user-o" aria-hidden="true"
                                                                       title="<?= $user_account_x; ?>"></i> </a>
                                <?php } ?>
                                <ul class="dropdown-menu dropdown-menu-right" style="display: none;background: #2a2a2a">
                                    <?php if ($logged) { ?>
                                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                        <!--                                        <li><a href="--><?php //echo $order; ?><!--">--><?php //echo $text_order; ?><!--</a></li>-->
                                        <!--                                        <li><a href="--><?php //echo $transaction; ?><!--">--><?php //echo $text_transaction; ?><!--</a>-->
                                        <!--                                        </li>-->
                                        <!--                                        <li><a href="--><?php //echo $download; ?><!--">--><?php //echo $text_download; ?><!--</a></li>-->
                                        <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                                    <?php } else { ?>
                                        <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                                        <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                                    <?php } ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php
if (isset($content_top)) {
    echo $content_top;
}


?>

<?php if(isset($popup)) { ?>
<!-- Модалка Соглашение   agrBg.png-->
<div class="modal " id="modalagreement" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog agreement" role="document">
        <div class="wrapper " style="text-align: center!important;">
            <div style="border:none;text-align: center!important;">
                <h4 class="modal-title" style="font-size: 2.5rem;"><?=$popup['title']?></h4>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="display:none">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="qwerty_xtz" style="border:none;text-align: left;padding-left: 15px;padding-right: 15px;">
                <?=$popup['description']?>
            </div>
            <div class="agreement-button-block">
                <button type="button" id="agreement-yes"
                        class="btn btn-primary"><div class="QWE"><?=$popup['button_yes']?></div></button>
                <button type="button" id="agreement-no"
                        class="btn btn-danger"><?=$popup['button_no']?></button>

            </div>
        </div>
    </div>
</div>
<input type="button" style="display: none" value="QEWQWEQWEQWEQWE" id="trigger-modal-agreement" data-toggle="modal"
       data-target="#modalagreement">

<?php

if ($agreement == 0) {
    ?>
    <script>
        $('#trigger-modal-agreement').trigger('click');
    </script>
    <?php
}

?>

<script>
    // Соглашение ДА
    $('button#agreement-yes').on('click', function () {
      var date_expires = new Date(new Date().setMonth(new Date().getMonth()+1)).toGMTString();
      document.cookie = 'agreement=1; expires=' + date_expires + '; location="/"';
      $('.close').trigger('click');
    });

    // Соглашение НЕТ
    $('#agreement-no').on('click', function () {
        window.location.replace("https://www.google.com.ua");
    });


    $('#do-search').on('click', function () {
        $('#input-search-q').css('display', 'block');
        $('#do-search').css('display', 'none');

        return false;
    });
</script>
<?php } ?>
