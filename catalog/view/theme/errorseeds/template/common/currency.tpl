<?php if (count($currencies) > 1) {?>
    <div class="money-bl ic-top-r2 drop-wr">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="currency">
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" id="wr-money-bl" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php foreach ($currencies as $currency) { ?>
                        <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?>
                           <?php echo $currency['symbol_left']; ?>
                        <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?>
                           <?php echo $currency['symbol_right']; ?>
                        <?php } ?>
                    <?php } ?>
                    <span class="hidden-xs hidden-sm hidden-md"><?php /*$text_currency.'asd'*/; ?></span> <i
                            class="fa fa-angle-down" aria-hidden="true"></i></button>
                <div class="dropdown-menu" aria-labelledby="wr-money-bl" >
                    <?php foreach ($currencies as $currency) { ?>
                        <?php if ($currency['symbol_left']) { ?>

                                <button class="dropdown-item<?php echo $currency['code'] == $code ? ' active' : ''; ?>" type="button" style="cursor: pointer"
                                        name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?><?php echo 1 . ' = '; ?> <?= round( (1 /$currency['value']), 2) . ' Uah'; ?></button>

                        <?php } else { ?>
                            <button class="dropdown-item<?php echo $currency['code'] == $code ? ' active' : ''; ?>" type="button"
                                    name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?><?php echo 1 . ' = '; ?> <?= round((1 /$currency['value']), 2) . ' Uah'; ?></button>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <input type="hidden" name="code" value=""/>
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
        </form>
    </div>
<?php } ?>
