<div class="copy-wr">
    <div class="container">
      <div class="logo-footer">
        <svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" viewBox="0 0 328.5 34.2" xml:space="preserve" class="logo-ua">
          <g>
          	<path d="M105.7,4.6c-8.2,0-13.4,5.3-13.5,12.7c0.2,7.4,5.3,12.8,13.5,12.8c8.2,0,13.4-5.3,13.5-12.8C119.1,10,113.9,4.6,105.7,4.6L105.7,4.6zM105.7,27.1c-6.4,0-10-4.3-10-9.7c0-5.5,3.6-9.7,10-9.7c6.4,0,10,4.3,10,9.7C115.7,22.9,112.1,27.1,105.7,27.1L105.7,27.1zM142.6,19c1.1-0.2,1.7-0.5,1.8-0.5c2.9-1.1,4.4-3.7,4.4-6.3c0-4.1-3-7.2-9.8-7.2h-8.4c-0.9,0-1.6,0.7-1.6,1.6v21.7c0,0.9,0.7,1.6,1.6,1.6c0.9,0,1.7-0.7,1.7-1.6v-8.2h7.5c0.1,0.2,0.3,0.5,0.4,0.7l4.7,8.1l0,0l0.1,0.2l0.3,0.4c0.5,0.6,1.3,0.7,2,0.3c0.7-0.5,1-1.4,0.5-2.1l-0.3-0.4L142.6,19L142.6,19z M138.7,17.1h-6.5v-9h6.4c5.3,0,6.9,1.8,6.9,4.4C145.5,15.5,142.8,17.1,138.7,17.1L138.7,17.1zM160.8,11.7c0-2.7,2.8-4,6.4-4c2.2,0,4.8,1.5,6.3,2.2c0.2,0.1,0.4,0.1,0.6,0.1c0.9,0,1.4-0.8,1.4-1.5c0-0.9-0.5-1.2-1-1.4c-1.7-0.8-4.1-2.4-7.6-2.4c-6.1,0-9.3,3.3-9.3,7.3c0,8.7,15.3,5.8,15.3,11.1c0,3.1-2.7,4.1-6.3,4.1c-3.1,0-5.4-2.3-6.4-2.9c-0.2-0.1-0.5-0.1-0.7-0.1c-0.9,0-1.5,0.7-1.5,1.5c0,0.6,0.5,1.1,1,1.5c1.8,1.3,4.3,3.2,7.9,3.2c5.6,0,9.2-2.9,9.2-6.9C176.1,13.8,160.8,17.8,160.8,11.7L160.8,11.7L160.8,11.7zM243,26.8h-12.8v-7.9h11.5c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5h-11.5V8H243c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5h-14.5c-0.9,0-1.6,0.7-1.6,1.6v21.6c0,0.9,0.7,1.6,1.6,1.6H243c0.9,0,1.6-0.6,1.6-1.5C244.5,27.5,243.9,26.8,243,26.8L243,26.8zM267.4,26.8h-12.8v-7.9h11.5c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5h-11.5V8h12.8c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5H253c-0.9,0-1.6,0.7-1.6,1.6v21.6c0,0.9,0.7,1.6,1.6,1.6h14.5c0.9,0,1.6-0.6,1.6-1.5C269,27.5,268.3,26.8,267.4,26.8L267.4,26.8zM284.2,5h-6.7c-0.8,0-1.5,0.7-1.5,1.6v21.6c0,0.9,0.7,1.6,1.5,1.6h6.7c7.9,0,11.7-6.4,11.7-12.4C295.9,11.4,292.1,5,284.2,5L284.2,5zM283.8,26.8h-4.8V8h4.8c5.5,0,9.1,3.8,9.1,9.4C292.9,22.9,289.4,26.8,283.8,26.8L283.8,26.8zM77.5,19c1.1-0.2,1.7-0.5,1.8-0.5c2.9-1.1,4.4-3.7,4.4-6.3c0-4.1-3-7.2-9.8-7.2h-8.4c-0.9,0-1.6,0.7-1.6,1.6v21.7c0,0.9,0.7,1.6,1.6,1.6c0.9,0,1.7-0.7,1.7-1.6v-8.2h7.5c0.1,0.2,0.3,0.5,0.4,0.7l4.7,8.1l0,0l0.1,0.2l0.3,0.4c0.5,0.6,1.3,0.7,2,0.3c0.7-0.5,1-1.4,0.5-2.1l-0.3-0.4L77.5,19L77.5,19zM73.6,17.1h-6.5v-9h6.4c5.3,0,6.9,1.8,6.9,4.4C80.4,15.5,77.7,17.1,73.6,17.1L73.6,17.1zM47.3,19c1.1-0.2,1.7-0.5,1.8-0.5c2.9-1.1,4.4-3.7,4.4-6.3c0-4.1-3-7.2-9.8-7.2h-8.4c-0.9,0-1.6,0.7-1.6,1.6v21.7c0,0.9,0.7,1.6,1.6,1.6s1.7-0.7,1.7-1.6v-8.2h7.5c0.1,0.2,0.3,0.5,0.4,0.7l4.7,8.1l0,0l0.1,0.2l0.3,0.4c0.5,0.6,1.3,0.7,2,0.3c0.7-0.5,1-1.4,0.5-2.1l-0.3-0.4L47.3,19L47.3,19zM43.5,17.1h-6.5v-9h6.4c5.3,0,6.9,1.8,6.9,4.4C50.2,15.5,47.5,17.1,43.5,17.1L43.5,17.1zM204.1,11.7c0-2.7,2.8-4,6.4-4c2.2,0,4.8,1.5,6.3,2.2c0.2,0.1,0.4,0.1,0.6,0.1c0.9,0,1.4-0.8,1.4-1.5c0-0.9-0.5-1.2-1-1.4c-1.7-0.8-4.1-2.4-7.6-2.4c-6.1,0-9.3,3.3-9.3,7.3c0,8.7,15.3,5.8,15.3,11.1c0,3.1-2.7,4.1-6.3,4.1c-3.1,0-5.4-2.3-6.4-2.9c-0.2-0.1-0.5-0.1-0.7-0.1c-0.9,0-1.5,0.7-1.5,1.5c0,0.6,0.5,1.1,1,1.5c1.8,1.3,4.3,3.2,7.9,3.2c5.6,0,9.2-2.9,9.2-6.9C219.4,13.8,204.1,17.8,204.1,11.7L204.1,11.7L204.1,11.7zM305.8,11.7c0-2.7,2.8-4,6.4-4c2.2,0,4.8,1.5,6.3,2.2c0.2,0.1,0.4,0.1,0.6,0.1c0.9,0,1.4-0.8,1.4-1.5c0-0.9-0.5-1.2-1-1.4c-1.7-0.8-4.1-2.4-7.6-2.4c-6.1,0-9.3,3.3-9.3,7.3c0,8.7,15.3,5.8,15.3,11.1c0,3.1-2.7,4.1-6.3,4.1c-3.1,0-5.4-2.3-6.4-2.9c-0.2-0.1-0.5-0.1-0.7-0.1c-0.9,0-1.5,0.7-1.5,1.5c0,0.6,0.5,1.1,1,1.5c1.8,1.3,4.3,3.2,7.9,3.2c5.6,0,9.2-2.9,9.2-6.9C321.1,13.8,305.8,17.8,305.8,11.7L305.8,11.7L305.8,11.7zM21.2,26.8H8.4v-7.9h11.5c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5h-9H8.4c-2,0-3.3,0.5-3.3,2.7v9.6c0,0.9,0.7,1.6,1.6,1.6h14.5c0.9,0,1.6-0.6,1.6-1.5C22.8,27.5,22.1,26.8,21.2,26.8L21.2,26.8z M8.4,8h2.5h10.3c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5H6.8C5.8,5,5.1,5.8,5.1,6.6C5.1,8.4,7.4,8,8.4,8L8.4,8L8.4,8z"/>
          </g>
          </svg>
      </div>
      <div class="copy-text text-center">Made with <i class="fa fa-heart"></i> for great people © Errors Seeds Company 2009-2018</div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row flex-wrap">
            <div class="col-12 col-md-6 col-lg-3">
                <ul class="menu-footer">
                    <li><a href="/about_us#h5"><?= $about_us; ?></a></li>
                    <li><a href="/about_us#h1"><?= $questions; ?></a></li>
                    <li><a href="/about_us#h3"><?= $payment_methods; ?></a></li>
                    <li><a href="/about_us#h2"><?= $delivery; ?></a></li>
                    <li><a href="/about_us#h4"><?= $replacement_and_return; ?></a></li>
                    <li><?= $contact_us; ?></li>
                    <li><?= $feedbacs_x; ?></li>
                    <li><a href="#" target="_blank" rel="https://jahforum.org/" class="hidli"><?= $text_compare_forum; ?></a></li>

                </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <button class="btn-zv-green" onclick="OpenJivoChat('80%'); return false;">
                    <div class="d-flex align-items-center">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span><?= $contact_us_button; ?></span>
                    </div>
                </button>
            </div>

            <div class="col-12 col-md-6 col-lg-3">
                <ul class="cont-footer">
                    <?php $address = explode(',', $address); ?>
                    <li><?= $address[0]; ?></li>
                    <li><?= $address[1] . ', ' . $address[2] . ', ' . $address[3]; ?></li>

                    <?php $phone = explode(';', $telephone); ?>
                    <?php $email = explode(';', $fax); ?>
                    <?php foreach ($phone as $ph) { ?>
                        <li><a href="tel:<?= $ph; ?>"><?= $ph; ?></a></li>
                    <?php } ?>
                    <?php foreach ($email as $em) { ?>
                        <li><a href="mailto:<?= $em; ?>"><?= $em; ?></a></li>
                    <?php } ?>

                </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <div class="wr-social">
                    <div class="name-social"><?= $join_us; ?></div>
                    <ul class="social d-flex justify-content-between">
                        <?php $soc = explode(';', $comment); ?>
                        <li><a href="#" class="icon-soc fa fa-vk hidli" rel="<?= $soc[0]; ?>" title="vk"></a></li>
                        <li><a href="#" class="icon-soc fa fa-facebook hidli" rel="<?= $soc[1]; ?>" title="facebook"></a></li>
                        <li><a href="#" class="icon-soc fa fa-twitter hidli" rel="<?= $soc[2]; ?>" title="twitter"></a></li>
                        <li><a href="#" class="icon-soc fa fa-google-plus hidli" rel="<?= $soc[3]; ?>" title="google-plus"></a></li>
                        <li><a href="#" class="icon-soc fa fa-instagram hidli" rel="<?= $soc[4]; ?>" title="instagram"></a></li>
                        <li><a href="#" class="icon-soc fa fa-youtube-play hidli" rel="<?= $soc[5]; ?>" title="youtube"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //echo $text_newsletter_text;?>
<!--МЫ РАБОТАЕМ 24/7 подробнее   Почему лучше купить семена конопли в -->
<style>
    #arrow_hide:after {
        pointer-events: all;
        content: "\f107";
        font-family: 'FontAwesome';
        color: #fff;
        /*font-size: 40px;*/
        font-size: 28px;
        font-weight: 900;
        line-height: 17px;
        transition: transform linear 0.5s;
        display: inline-block;
    }

    #arrow_hide.toggled:after {
        content: "\f106";
    }

    #arrow_hide2:after {
        pointer-events: all;
        content: "\f107";
        font-family: 'FontAwesome';
        color: #fff;
        /*font-size: 40px;*/
        font-size: 28px;
        font-weight: 900;
        line-height: 17px;
        transition: transform linear 0.5s;
        display: inline-block;
    }

    #arrow_hide2.toggled:after {
        content: "\f106";
    }


</style>

<script>
    $('#arrow_hide').click(function () {
        $(this).toggleClass('toggled');
//        $('.hour_hide.text-center').toggle('slow');
    });

    $('#arrow_hide2').click(function () {
        $(this).toggleClass('toggled');
//        $('.hour_hide2.mt-5').toggle('slow');
    });
</script>
<!--END  МЫ РАБОТАЕМ 24/7 подробнее   Почему лучше купить семена конопли в -->



<!--Подписаться-->
<script type="text/javascript" language="javascript">

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }

    $('#newsletter').on('click', function () {
        var email = $('input[name="email"]').val();

        console.log(email);
        if (email == '') {
            var error = "<?php echo $text_newsletter_text;?>";
        }

        if (!validateEmail(email)) {
            var error = "<?php echo $text_email_not_validate;?>";
        }
        if (error != null) {
            $('#error-email').text(error).css({'color': "red", 'display': 'block'});
            setTimeout(function () {
                $('#error-email').text(error).css({'color': "red", 'display': 'none'});
            }, 2000);
        } else {
            $.ajax({
                url: 'index.php?route=module/newsletters/news',
                type: 'post',
                data: 'email=' + email,
                dataType: 'json',

                success: function (json) {
                    $('input[name="email"]').val('');

                    if (json.message == 0) {
                        $('#error-email').text("<?php echo $text_error_subcribe;?>").css({
                            'display': 'block',
                            'color': 'red'
                        });
                    } else {
                        $('#error-email').text("<?php echo $text_success_subcribe;?>").css({
                            'display': 'block',
                            'color': 'green'
                        });
                    }
                    setTimeout(function () {
                        $('#error-email').text('').css('display', 'none');
                    }, 3000);
                }

            });
            return false;
        }
    });
</script>
<!--END Подписаться-->

<!--Категории-->
<script>
    $(".wr-category-block").on("click" , function(){
        $('.block-cat.toggle').removeClass('active');
        for (var i = 1; i < 6; i++){
            $('.' + 'slide'+i + '.cont-wrap').css({'display':'none'});
        }
    });

    $(document).on("click", ".block-cat.toggle", function(){
        var data = $(this).data('class');
        for (var i = 1; i < 6; i++){
            $('.' + 'slide'+i + '.cont-wrap').css({'display':'none'});
            $('.block-cat.toggle').removeClass('active');
        }
        $('.' + data + '.cont-wrap').css({'display':'block'});
        $(this).addClass('active');

//        $('html,body').animate({scrollTop:$('.' + data + '.cont-wrap').offset().top - 400}, 600);

        if(device.mobile()){
            $('html,body').animate({scrollTop:$('.' + data + '.cont-wrap').offset().top - 150}, 600);
        }else{
            $('html,body').animate({scrollTop:$('.' + data + '.cont-wrap').offset().top - 400}, 600);
        }

    });
    $(document).on("click", ".block-cat.toggle.active", function(){
        $('.block-cat.toggle').removeClass('active');
    });
</script>
<!--END Категории-->

<!--Показать меню Авторизации-->
<script>

    $('#user_account_x').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('toggled');
        $('.dropdown-menu.dropdown-menu-right').toggle('slow');
    });
</script>


<script>
    $('.btn-all-news').on('click', function () {
        $('.bal-box-next').trigger('click');
        return false;
    });
</script>


<script type='text/javascript' id='map' charset='utf-8' data-lang='ru' apiKey='f2c14f967be6d83fe8a3d502812bdfd2'
        data-town='city-not-default' data-town-name='undefined' data-town-id='undefined'
        src='https://apimgmtstorelinmtekiynqw.blob.core.windows.net/content/MediaLibrary/Widget/Map/dist/map.min.js'></script>


<!-- Модалка отзывы-->

<style>
    input[type="radio"] {
        display: none;
    }
    input[type="radio"] + label span {
        display: inline-block;
        width: 35px;
        height: 40px;
        margin: -1px 4px 0 0;
        vertical-align: middle;
        background:url(/image/grey-icon.png) left top no-repeat;
        cursor: pointer;

    }
    input[type="radio"] + label span:hover {
        background:url(/image/green-icon.png) left top no-repeat;
    }
    input[type="radio"]:checked + label span {
        background:url(/image/green-icon.png) left top no-repeat;
    }

    input[type="radio"] + label.AAA span {
        background:url(/image/green-icon.png) left top no-repeat;
    }


</style>

<div class="modal fade" id="exampleModal">
    <div class="modal-dialog" role="document">
        <div class="wrapper bg-black-page">
            <div class="" style="border:none;text-align: center">
                <div class="modal-title" style="color: #82b523; font-size: 27px"><?= $title_feedback; ?></div>
                <button type="button" style="display: none" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body feedback" style="border:none;text-align: center">
                <div class="wr-reg-page col-8 px-3 mx-auto">
                    <div class="form-group ">
<!--                        <label for="formName" class="color-label">--><?php //echo $rating; ?><!--</label>-->
                        &nbsp;&nbsp;
                    </div>
                    <div class="form-group " id="rev-rating">
                        <input type="radio" id="c1" name="rating" value="1"/>
                        &nbsp;<label id="c1-label" data-value="1" for="c1" class="rating"><span></span></label>
                        <input type="radio" id="c2" name="rating" value="2"/>
                        &nbsp;<label id="c2-label" data-value="2" for="c2" class="rating"><span></span></label>
                        <input type="radio" id="c3" name="rating" value="3"/>
                        &nbsp;<label id="c3-label" data-value="3" for="c3" class="rating"><span></span></label>
                        <input  type="radio" id="c4" name="rating" value="4"/>
                        &nbsp;<label id="c4-label" data-value="4" for="c4" class="rating"><span></span></label>
                        <input type="radio" id="c5" name="rating" value="5"/>
                        <label id="c5-label" data-value="5" for="c5" class="rating"><span></span></label>


                        &nbsp;
                    </div>
                    <p class="aler_rating" style="border-bottom: 1px solid red;margin-top: -8px;display: none;"></p>

                    <div class="form-group ">
                        <label for="rev-name" class="color-label"><?php echo $entry_firstname; ?></label>
                        <input type="text" name="rev-name" value="<?php echo $user_info['name']; ?>"
                               class="form-control trans-input form-control-success" id="formName" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="rev-email" class="color-label"><?php echo $entry_email; ?></label>
                        <input type="text" name="rev-email" class="form-control trans-input form-control-success"
                               value="<?php echo $user_info['email']; ?>" id="formEmail"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="rev-comment" class="color-label"><?php echo $entry_comment; ?></label>
                        <input type="text" name="rev-comment" class="form-control trans-input form-control-success" value=""
                               id="formEmail" placeholder="">
                    </div>
                    <input type="hidden" name="product_id" value=""><br/>
                    <input type="hidden" name="review_id" value=""><br/>
                </div>
                <div class="g-recaptcha" data-sitekey="<?=GOOGLE_FRONT?>" id="rev-captcha" data-callback="removeError"></div>
            </div>
            <div class="modal-footer" style="border:none">
                <button type="button" id="new-reviews-x" class="form-btn btn-reg"><?= $button_modal_save; ?></button>
            </div>
        </div>
    </div>
</div>



<!-- Модалка для выбора опции в товарах-->
<div class="modal fade" id="modalcat">
    <div class="modal-dialog" role="document" style="padding-top: 15%;">

        <div class="wrapper bg-black-page" style="background: #212121;">

            <div class="modal-header" style="border:none;text-align: center;display: none">
                <h5 class="modal-title"></h5>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display: none">×</button>
            <div class="modal-body qwerty" style="">
            </div>
        </div>
    </div>
</div>


<!-- Модалка для уведомления о поступлении-->
<div class="modal fade" id="modalentrance">
    <div class="modal-dialog" role="document" style="padding-top: 15%;">
        <div class="wrapper bg-black-page" style="background: #212121;">
            <div class="modal-head">
                <div class="modal-title"><?php echo $entrance_title; ?></div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body entrance col-8 mx-auto" style="">
              <div class="form-group">
                <label for="nr-email" class="color-label"><?php echo $entry_email; ?></label>
                <input type="text" name="nr_email" class="form-control trans-input" value="<?=isset($user_info['email']) ? $user_info['email'] : '';?>" id="nr-email" placeholder="">
              </div>
              <div class="form-group">
                <label for="nr-quantity" class="color-label"><?php echo $entry_quantity; ?></label>
                <input type="text" name="nr_quantity" class="form-control trans-input" value="1" id="nr-quantity" placeholder="">
              </div>
              <div class="form-group">
                <label class="color-label"><?php echo $entry_date_expired; ?></label>
                <div class="row" id="nr-date-expired">
                  <div class="col-12 justify-content-around row">
                    <label for="nr-day" class="color-label"><?php echo $entry_day; ?></label>
                    <label for="nr-month" class="color-label"><?php echo $entry_month; ?></label>
                    <label for="nr-year" class="color-label"><?php echo $entry_year; ?></label>
                    </div>
                    <div class="input-group input-date col-12 col-12">
                      <input type="text" class="form-control trans-input" name="nr_day" value="<?=date('d', strtotime('+1 month'))?>" id="nr-day"/>
                      <input type="text" class="form-control trans-input" name="nr_month" value="<?=date('m', strtotime('+1 month'))?>" id="nr-month"/>
                      <input type="text" class="form-control trans-input" name="nr_year" value="<?=date('Y', strtotime('+1 month'))?>" id="nr-year"/>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer" style="border:none">
                <button type="button" id="entrance" class="form-btn btn-reg"><?php echo $entrance_btn; ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-message">
    <div class="modal-dialog" role="document" style="padding-top: 15%;">
        <div class="wrapper bg-black-page" style="background: #212121;">
            <div class="modal-body message col-8 mx-auto" style="">
            </div>
        </div>
    </div>
</div>




<script>
$('.form-group label.rating').on('click', function () {
     $('label[for=rev-rating]').remove();
    for (var i = 1; i < 6; i++ ){
        var lab = $('#c'+i+'-label' );
        lab.removeClass('AAA');
    }
    for (var i = 1; i < $(this).data('value'); i++ ){
        var lab = $('#c'+i+'-label' );
        lab.addClass('AAA');
    }
});
function accordion() {
    $(".accordion .accordion_title").click(function () {

        var $content = $(this).next();
        if ($content.is(":visible")) {
              $content.slideUp(500, function () {
            });
            $(this).children().removeClass("active");
            $(this).removeClass("active");
        } else {
            $(".accordion .accordion_content").slideUp("slow");
            $(".accordion .accordion_title").children()
                .removeClass("active");
            $(".accordion_title").removeClass("active");
            $content.slideToggle("slow");
            $(this).children().addClass("active");
            $(this).addClass("active");
        }
    });
}
    function pagescrollup() {
        $('html, body').animate({scrollTop: 0}, 500);
    }
</script>

<script>
    $(document).ready(function () {

        $('button.btn.btn-default.btn-block').css({'display':'none'});
        $('.ocfilter-option').css({'display':'block','border':'none'});
        $('div#ocfilter-hidden-options').css({'display':'block'});



        accordion();

        pagescrollup();

        var z = $(".nav-link a[href='" + window.location.hash + "']");
        $('.tab-pane').removeClass('active');
        $('.nav-link').removeClass('active');
        $('.nav-link').each(function () {
            if (window.location.hash == $(this).attr('href')) {
                $(this).addClass('active');
            }
        });
        $(window.location.hash).addClass('active');


        setTimeout(function () {
            $('.alert.alert-success').fadeOut()
        }, 2000);




    });


</script>
<script>$('a.hidli').click(function(){window.open($(this).attr("rel"));return false;});</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.4/jquery.sticky.min.js"></script>
<script>
    $(document).ready(function () {
        if ($(window).width() > 1024) {
            $("#sticker").sticky({topSpacing: 0});
        }
    });
</script>
<script>
    $(".nav-tabs .nav-link").click(function () {
        var $content = $(".accordion .accordion_title");
        if ($content.is(":visible")) {
          $(".accordion .accordion_content").slideUp("slow");
          $content.children().removeClass("active");
          $content.removeClass("active");
        }
    });
    $(".opis-desc").text(function(i, text) {
    if (text.length >= 140) {
      text = text.substring(0, 140);
      var lastIndex = text.lastIndexOf(" ");       // позиция последнего пробела
      text = text.substring(0, lastIndex) + '...'; // обрезаем до последнего слова
    }
    $(this).text(text);
  });
</script>
</body>
</html>
