<form action="/<?php echo (isset($lang_code) ? $lang_code . '/' : ''); ?>search" id="search">
  <fieldset>
    <div class="input-group">
      <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg search-autocomplete" autocomplete="off">
      <span class="input-group-btn">
        <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
      </span>
      <ul class="autocomplete-hide result-search-autocomplete"></ul>
    </div>
  </fieldset>
</form>
