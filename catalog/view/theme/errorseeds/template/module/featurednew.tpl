<div class="new-prod-block">
  <div class="container">
    <div class="name-prod-name text-center">
      <?php if (isset($categories[5])) { ?>
        <a href="<?=$categories[5]['href'];?>"><?=$heading_title; ?></a>
      <?php } else {  echo $heading_title; } ?>
    </div>
    <div class="produckt-slider owl-carousel">
      <?php foreach ($products as $product) { ?>
      <div class="product-wrap">
        <div class="foto-prod">
          <a href="<?php echo $product['href']; ?>">
            <div class="label-prod">
              <img src="image/label.png" alt="">
            </div>
            <img src="<?= $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-fluid">
            <div class="hiden-opt">
              <div class="dop-opt">
                <?php foreach ($product['attributes'] as $group) { ?>
                <?php foreach ($group['attribute'] as $key => $attribute) { ?>
                  <?php if ($key <= 7){ ?>
                <div class="wr-opt-prd">
                  <div class="opt-prd">
                    <?php if (isset($attribute["svg"])){ ?>
                      <?php echo $attribute["svg"]; ?>
                    <?php }else{ ?>
                      <?php if(isset($attribute["image"])){ ?>
                        <img src="<?php echo $attribute["image"] ?>" alt="">
                      <?php } ?>
                    <?php } ?>
                    <span><?=$attribute['name'].' '.$attribute['text'];?></span>
                  </div>
                </div>
              <?php }else{} ?>
                <?php } ?>
                <?php } ?>
              </div>
              <div class="dop-prod-btn d-flex">
                <?php if ($product['in_compare']) {?>
                  <button type="button" style="border: none;cursor: pointer; z-index: 99999"
                          data-toggle="tooltip" class="sr-btn-prod active"
                          title="<?php echo $text_in_compare; ?>"
                          onclick="window.location.href='<?=$product['in_compare']?>';return false;"><i class="fas fa-balance-scale"></i></button>
                <?php } else { ?>
                  <button type="button" style="border: none;cursor: pointer; z-index: 99999"
                          data-toggle="tooltip" class="sr-btn-prod"
                          title="<?php echo $button_compare; ?>"
                          onclick="compare.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="fas fa-balance-scale"></i></button>
                <?php } ?>
                <?php if ($product['in_wishlist']) { ?>
                  <button type="button" style="border: none;cursor: pointer"
                          data-toggle="tooltip" class="wish-btn-prod active"
                          title="<?php echo $text_in_wishlist; ?>"
                          onclick="window.location.href='<?=$product['in_wishlist']?>';return false;"><i class="far fa-heart"></i></button>
                <?php } else { ?>
                  <button type="button" style="border: none;cursor: pointer"
                          data-toggle="tooltip" class="wish-btn-prod"
                          title="<?php echo $button_wishlist; ?>"
                          onclick="wishlist.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="far fa-heart"></i></button>
                <?php } ?>
              </div>
            </div>
          </a>
        </div>
        <a style="text-decoration: none" href="<?php echo $product['href']; ?>">
          <div class="name-prod">
            <?php echo $product['name']; ?>
          </div>
        </a>
        <div class="d-flex flex-wrap justify-content-center align-items-center btn-mar">
          <?php if ($product['special']) { $new_price = $product['special']; ?>
          <div class="old-price">
            <?php echo $product['price']; ?>
          </div>
          <?php } else { $new_price = $product['price']; } ?>
          <?php if ($product['quantity'] > 0) { ?>
            <a href="<?php echo $product['href']; ?>" class="prod-btn" data-id="<?php echo $product['product_id']; ?>" onclick="return false;" data-toggle="modal" data-target="#modalcat">
              <div class="d-flex align-items-center">
                <div class="icon-prod-btn"></div>
                <div class="text-prod-btn">
                  <?php echo $new_price; ?> <span class="modal-add-prod" data-id="<?php echo $product['product_id']; ?>" onclick="return false;" data-toggle="modal" data-target="#modalcat"><?php echo $button_cart; ?></span>

                </div>
              </div>
            </a>
          <?php } else { ?>
            <button type="button" class="entrance-btn xtz_button"
                    data-id="<?php echo $product['product_id']; ?>" onclick="modalEntrance(<?php echo $product['product_id']; ?>)"  data-toggle="modal" data-target="#modalentrance" >
                <div class="d-flex align-items-center">
                    <div class="text-prod-btn">
                        <span><?php echo $button_entrance; ?></span>
                    </div>
                </div>
            </button>
          <?php } ?>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>



<script>
  $(document).ready(function() {
    $('.prod-btn').click(function() {
      var id = $(this).attr('data-id');
      $.ajax({
        url: '/index.php?route=product/qproduct',
        type: 'get',
        data: 'product_id=' + id,
        success: function(data) {
          console.log(data);
          $('.qwerty').html(data);
        }
      });
    });
  });
</script>
