<div class="wr-slider">
    <div id="slideshow<?php echo $module; ?>" class="slider owl-carousel">
        <?php foreach ($banners as $banner) { ?>
        <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" class="img-fluid" alt=""></a>
        <?php } ?>
    </div>
</div>
<script type="text/javascript"><!--
//    $('#slideshow<?php //echo $module; ?>//').owlCarousel({
//        items: 6,
//        autoPlay: 3000,
//        singleItem: true,
//        navigation: true,
//        navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
//        pagination: true
//    });
    --></script>