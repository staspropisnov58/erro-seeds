<?php //if(isset($products)){ ?>
<!--<div>-->
<!--<h3>--><?php //echo $heading_title; ?><!--</h3>-->
<!--  --><?php //foreach ($products as $product) { ?>
<!--  <div class="product-thumb">-->
<!--	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">-->
<!--		<a href="--><?php //echo $product['href']; ?><!--"><img src="--><?php //echo $product['thumb']; ?><!--" /></a>-->
<!--	</div>-->
<!--	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">-->
<!--		<a href="--><?php //echo $product['href']; ?><!--">--><?php //echo $product['name']; ?><!--</a>-->
<!---->
<!--		--><?php //if ($product['rating']) { ?>
<!--        <div class="rating">-->
<!--          --><?php //for ($i = 1; $i <= 5; $i++) { ?>
<!--          --><?php //if ($product['rating'] < $i) { ?>
<!--          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>-->
<!--          --><?php //} else { ?>
<!--          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>-->
<!--          --><?php //} ?>
<!--          --><?php //} ?>
<!--        </div>-->
<!--        --><?php //} ?>
<!---->
<!--		--><?php //if ($product['price']) { ?>
<!--        <p class="price">-->
<!--          --><?php //if (!$product['special']) { ?>
<!--          --><?php //echo $product['price']; ?>
<!--          --><?php //} else { ?>
<!--          <span class="price-new">--><?php //echo $product['special']; ?><!--</span> <span class="price-old">--><?php //echo $product['price']; ?><!--</span>-->
<!--          --><?php //} ?>
<!--        </p>-->
<!--        --><?php //} ?>
<!--	</div>-->
<!--  </div>-->
<!--  --><?php //} ?>
<!--</div>-->
<?php //} ?>




<?php if(isset($products)){ ?>

<div class="bg-grey block-see">
    <div class="container">
        <div class="name-prod-name text-left"><?php echo $heading_title; ?></div>
        <div class="produckt-slider2 owl-carousel">
              <?php foreach ($products as $product) { ?>
<!--                  --><?php //echo "<pre style='color: white'>";?>
            <div class="product-wrap">
                <div class="foto-prod">
                    <a href="<?php echo $product['href']; ?>">
                    <div class="label-prod">
                        <img src="image/label.png" alt="">
                    </div>
                    <img src="<?php echo $product['thumb']; ?>" alt="" class="img-fluid d-block w-100">
                    <div class="hiden-opt">
                        <div class="dop-opt">
                          <?php if (isset($product['attribute_groups'][0])) { ?>
                            <?php $k = 0;
                            foreach ($product['attribute_groups'][0]["attribute"] as $attribute) {
                                $k++;
                                ?>
                                <div class="wr-opt-prd">
                                    <div class="opt-prd">
                                        <img src="/image/forattributes/ti<?= $k; ?>.png" alt="">
                                        <span><?= $attribute["text"]; ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                          <?php } ?>
                        </div>
                        <div class="dop-prod-btn d-flex">
                            <button type="button" style="border: none;cursor: pointer"
                                    data-toggle="tooltip" class="sr-btn-prod"

                                    onclick="compare.add('<?php echo $product['product_id']; ?>');"></button>
                            <button type="button" style="border: none;cursor: pointer"
                                    data-toggle="tooltip" class="wish-btn-prod"

                                    onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></button>
                        </div>
                    </div>
                    </a>
                </div>
                <div class="name-prod"><?php echo $product['name']; ?></div>
                <button type="button" class="prod-btn" style="background: #2a2a2a;cursor: pointer"
                        data-id="<?php echo $product['product_id']; ?>" onclick="return false;"  data-toggle="modal" data-target="#modalcat" >
                    <div class="d-flex align-items-center">
                        <div class="icon-prod-btn"></div>
                        <div class="text-prod-btn">
                            <?php if ($product['special']) {
                                $sum = $product['special'];
                            }else{
                                $sum = $product['price'];
                            }?>

                            <?php echo mb_substr( $sum, 1) ; ?>
                            <span><?php echo $button_cart; ?></span>
                        </div>
                    </div>
                </button>
            </div>
           <?php } ?>
        </div>
    </div>
</div>

<?php } ?>

<script>
    $(document).ready(function () {
        $('.prod-btn').click(function () {
            var id = $(this).attr('data-id');
            $.ajax({
                url: '/index.php?route=product/qproduct',
                type: 'get',
                data: 'product_id='+id,
                success: function (data) {
                    $('.qwerty').html(data);
                }
            });
        });
    });




</script>
