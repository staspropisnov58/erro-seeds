<div class="wr-block-news">
    <div class="news-name text-center">
        <h3><?= $heading_title; ?></h3>
    </div>
    <div class="container d-flex justify-content-between flex-wrap">
        <?php $i = 0;
        foreach ($all_news as $news) {
            $i++ ?>
            <?php if ($i > 4) {
                break;
            } ?>
            <div class="news d-flex">
                <div class="d-flex justify-content-between flex-wrap">
                    <div class="col-12 col-md-4 px-0">
                        <a href="<?php echo $news['view']; ?>" class="read-more"> <img
                                    src="  <?php echo $news['image']; ?>" alt="" class="foto-block-news"></a>
                    </div>

                    <div class="col-12 col-md-8 wr-opis-desc align-self-center align-self-stretch">
                        <a href="<?php echo $news['view']; ?>" class="read-more"
                           style="color: #333333;text-decoration: none"><?php echo $news['title']; ?></a>
                        <div class="opis-desc">
                            <?php echo $news['description']; ?>
                        </div>
                        <div class="read-more-wr">
                            <a href="<?php echo $news['view']; ?>" class="read-more"><?= $read_more; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
    <div class="wr-all-news">
        <a href="/news/" class="btn-all-news_home_x"><?= $read_all_news ?></a>
    </div>
</div>
