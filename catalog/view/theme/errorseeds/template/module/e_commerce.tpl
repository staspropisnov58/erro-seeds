<?php if (isset($order_id) && isset($products)) { ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(["_setAccount", "CODE HERE"]);
  _gaq.push(["_trackPageview"]);
  _gaq.push(["_addTrans",
    "<?=$order_id?>",
    "<?=$store_url?>",
    "<?=$total?>",
    "",
    "<?=$shipping_method?>",
    "<?=$city?>",
    "<?=$zone?>",
    "<?=$country?>"
  ]);

  _gaq.push(["_set", "currencyCode", "<?=$currency?>"]);

   <?php foreach ($products as $product) { ?>
     _gaq.push(["_addItem",
       "<?=$order_id?>",
       "<?=$product['model']?>",
       "<?=$product['name']?>",
       "<?=$product['option']?>",
       "<?=$product['price']?>",
       "<?=$product['quantity']?>"
     ]);
    <?php } ?>
  _gaq.push(["_trackTrans"]);

  (function() {
    var ga = document.createElement("script"); ga.type = "text/javascript"; ga.async = true;
    ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php } ?>
