<style>

    .list-group-item{
        padding: 15px;

    }
    .list-group > a {
        color: #666;
    }
    .list-group > a:hover {
        text-decoration: none;
        color: #82b523;

    }

    .exit_x{
        border: 1px solid #ff3334;
        padding-top: 5px;
        padding-bottom: 5px;
        display: block;
        width: 100%;
        background: #ff3334;
        color: #fff;
    }
</style>

<div class="list-group">
    <span style="padding: 13px;background: #82b523;font-size: 17px;font-family: 'OpenSansBold';"><?=$text_my_account;?> </span>
    <?php if (!$logged) { ?>
        <a href="<?php echo $login; ?>" class="list-group-item"><?php echo $text_login; ?></a>
        <a href="<?php echo $register; ?>" class="list-group-item"><?php echo $text_register; ?></a>
        <a href="<?php echo $forgotten; ?>" class="list-group-item"><?php echo $text_forgotten; ?></a>
    <?php } ?>
    <a  href="<?php echo $account; ?>" class="list-group-item"><?php echo $text_account; ?></a>
    <?php if ($logged) { ?>
        <a href="<?php echo $edit; ?>" class="list-group-item"><?php echo $text_edit; ?></a>
        <a href="<?php echo $password; ?>" class="list-group-item"><?php echo $text_password; ?></a>
    <?php } ?>
    <a href="<?php echo $address; ?>" class="list-group-item"><?php echo $text_address; ?></a>
    <a href="<?php echo $wishlist; ?>" class="list-group-item"><?php echo $text_wishlist; ?></a>
    <a href="<?php echo $order; ?>" class="list-group-item"><?php echo $text_order; ?></a>
    <!--<a href="<?php /*echo $download; */ ?>" class="list-group-item"><?php /*echo $text_download; */ ?></a>-->
    <!--<a href="<?php /*echo $reward; */ ?>" class="list-group-item"><?php /*echo $text_reward; */ ?></a> <a href="<?php /*echo $return; */ ?>" class="list-group-item"><?php /*echo $text_return; */ ?></a> <a href="<?php /*echo $transaction; */ ?>" class="list-group-item"><?php /*echo $text_transaction; */ ?></a>-->
    <a href="<?php echo $newsletter; ?>" class="list-group-item"><?php echo $text_newsletter; ?></a>
    <!--<a href="<?php /*echo $recurring; */ ?>" class="list-group-item"><?php /*echo $text_recurring; */ ?></a>-->

</div>
<?php if($tuning == 1){ ?>
<div class="list-group affiliate">
  <a href="<?php echo $affiliate; ?>" class="list-group-item"><?php echo $text_affiliate; ?></a>
  <?php if (isset($affiliate_id) && !empty($affiliate_id)){ ?>
  <a href="<?php echo $bonuses; ?>" class="list-group-item"><?php echo $text_bonuses; ?></a>
  <a href="<?php echo $payouts; ?>" class="list-group-item"><?php echo $text_payouts; ?></a>
  <a href="<?php echo $unclosed_orders ?>" class="list-group-item"><?php echo $text_unclosed_orders; ?></a>
  <?php } ?>
</div>
<?php } ?>
<?php if ($logged) { ?>
    <a href="<?php echo $logout; ?>" style="border-bottom-style: none;text-align: center;"><b class="exit_x"><i class="fa fa-power-off" style="color: white;font-size: 18px;padding-right: 4px"></i><?php echo $text_logout; ?></b></a>
<?php } ?>
