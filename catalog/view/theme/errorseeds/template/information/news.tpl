<?php echo $header; ?>
<style>
    body{
        background:#fff ;
    }
    .align-self-center{
        color: #333131;
        text-decoration: none;
        text-transform: uppercase;
        font-size: 20px;
    }

    #show_all_reviews{
        text-decoration: none;
        border-bottom: 1px dashed #000;
        color: #000;
        font-size: 15px;
    }
    .fa-refresh{
        color: #000;
    }
    .wr-all-otzivi{
        margin-bottom: 30px
    }
    .action-desc{
        box-shadow: 0 0 10px rgb(117, 113, 113);
    }
    .action{
        margin-bottom: 110px;
    }
    .back-cat a{
        color: #7d7d7d;
        text-decoration: none;
        text-transform: uppercase;
        font-size: 14px;
    }
    .back-cat {color:#7d7d7d;}
</style>
<div class="wrapper">
    <div class="container">
        <div class="sort-wr">
            <div class="sort row flex-wrap justify-content-between">
                <div class="back-cat back-cat2 col-12 d-flex">
                  <i class="fa fa-arrow-left" aria-hidden="true"></i><?php foreach ($breadcrumbs as $breadcrumb) { ?>
                  <?php if (isset($breadcrumb['href'])) { ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' / '; ?></a></li>
                  <?php } else {
                    echo $breadcrumb['text'] . ' / ';
                  }
                } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="wr-action-page container">
        <div class="action">
            <img src="<?=$image_2;?>" alt="" class="img-fluid">
            <div class="action-desc">
                <h1 ><?php echo $heading_title; ?></h1>
                <div style="padding-bottom: 20px"></div>
                <?php echo $description; ?>
                <div class="action-bottom d-flex flex-wrap justify-content-between align-items-center">
                    <div class="col-12 col-md-6 dop-action">
                        <div class="time-action color-theme"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $date_added; ?></div>
                    </div>

                    <?php
                    if ($next_news == 1){
                    $style_next = "pointer-events: none";
                    }else{
                     $style_next = "pointer-events: auto";
                    }

                    if ( $prev_news == 1 ){
                    $style_prev = "pointer-events: none";
                    }else{
                     $style_prev = "pointer-events: auto";
                    }

                    ?>
                    <div class="col-12 col-md-6 dop-action text-right">
                      <?php if ( $next_news != 1 ){ ?>
                        <a href="<?=$next_news;?>" style="<?=$style_next;?>" class="blog-inner-btn blog-inner-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                      <?php } ?>
                      <?php if ( $prev_news != 1 ){ ?>
                        <a href="<?=$prev_news;?>" style="<?=$style_prev;?>" class="blog-inner-btn blog-inner-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                      <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-inner-comment">
        <div class="container">
            <div class="wr-otzivi">
                <div class="sort-otzivi row">

                    <div class="back-cat col-5 d-flex align-self-center">Комментарии</div>
                    <div class="col-7 d-flex justify-content-end">
                        <a href="" class="btn-my btn-all-otzivi"><img src="image/fly.png" alt=""> Написать отзыв</a>
                        <!--                        <div class="input-group-sm">-->
                        <!--                            <select id="input-sort" class="form-control" onchange="location = this.value;">-->
                        <!--                                <option value="" selected="selected">Сортировать</option>-->
                        <!--                                <option value="">по дате</option>-->
                        <!--                                <option value="">самые полезные</option>-->
                        <!--                                <option value="">от купивших этот товар</option>-->
                        <!--                            </select>-->
                        <!--                        </div>-->
                    </div>
                </div>
            </div>

            <div class="all-wr-otzivi row my-5">
                <?php foreach ($all_rew as $rew) { ?>
                    <?php if ($rew['rew_id'] == 0) { ?>
                        <?php if ($rew['customer_id'] == 1) {
                            $class = 'admin';
                        } else {
                            $class = '';
                        } ?>
                        <div class="otziv col-10 qrew">

                            <div class="top-otziv d-flex mb-2">
                                <div class="otz-name color-theme"><?= $rew["author"]; ?>
                                    <span class="otz-date color-theme">(<?= $rew["date_added"]; ?>) </span>
                                </div>
                                <div class="wr-rating">
                                    <div class="rating">
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($rew['rating'] < $i) { ?>
                                                <span class="fa fa-stack"><img src="image/grey-icon.png"></span>
                                            <?php } else { ?>
                                                <span class="fa fa-stack"><img src="image/green-icon.png"></span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="desc-otz mb-2"><?= $rew['text']; ?></div>
                            <div class="bottom-otziv d-flex mb-2">
                                <div class="reply-btn color-theme">
                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                    <a href="" data-review-id-xtz="<?= $rew["review_id"] ?>" class="color-theme">Ответить</a>
                                </div>
                                <div class="btn-ot-group d-flex">
                                    <div class="btn-ot-cool">
                                        <a href="#" class="like-dislike" data-review-id="<?= $rew["review_id"] ?>"
                                           data-like="1"></a>
                                        <span><?= $rew["clike"]; ?></span>
                                    </div>
                                    <div class="btn-ot-pool">
                                        <a href="#" class="like-dislike" data-review-id="<?= $rew["review_id"] ?>"
                                           data-like="0"></a>
                                        <span><?= $rew['cdislike'] ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($all_rew as $rew1) {
                            if ($rew['review_id'] == $rew1['rew_id']) {
                                ?>
                                <?php if ($rew1['customer_id'] == 1) {
                                    $class = 'admin';
                                } else {
                                    $class = 'user';
                                } ?>
                                <div class="otziv col-10 <?= $class ?>">
                                    <div class="wr-ad-bg">
                                        <div class="top-otziv d-flex mb-2">
                                            <div class="otz-name-admin"><?= $rew1["author"]; ?></div>
                                        </div>
                                        <div class="desc-otz mb-2"><?= $rew1['text']; ?></div>
                                    </div>
                                    <div class="bottom-otziv d-flex mb-2">
                                        <div class="reply-btn color-theme">
                                            <i class="fa fa-reply" aria-hidden="true"></i>
                                            <a href="" class="color-theme"
                                               data-review-id-xtz="<?= $rew1["review_id"] ?>">Ответить</a>
                                        </div>
                                        <div class="btn-ot-group d-flex">
                                            <div class="btn-ot-cool">
                                                <a href="#" class="like-dislike"
                                                   data-review-id="<?= $rew1["review_id"] ?>"
                                                   data-like="1"></a>
                                                <span><?= $rew1["clike"]; ?></span>
                                            </div>
                                            <div class="btn-ot-pool">
                                                <a href="#" class="like-dislike"
                                                   data-review-id="<?= $rew1["review_id"] ?>"
                                                   data-like="0"></a>
                                                <span><?= $rew1['cdislike'] ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($all_rew as $rew2) {
                                    if ($rew1['review_id'] == $rew2['rew_id']) { ?>
                                        <?php if ($rew2['customer_id'] == 1) {
                                            $class = 'admin';
                                        } else {
                                            $class = 'user';
                                        } ?>
                                        <div class="otziv col-10 <?= $class ?>">
                                            <div class="wr-ad-bg">
                                                <div class="top-otziv d-flex mb-2">
                                                    <div class="otz-name-admin"><?= $rew2["author"]; ?></div>
                                                </div>
                                                <div class="desc-otz mb-2"><?= $rew2['text']; ?></div>
                                            </div>
                                            <div class="bottom-otziv d-flex mb-2">
                                                <div class="reply-btn color-theme">
                                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                                    <a href="" class="color-theme"
                                                       data-review-id-xtz="<?= $rew2["review_id"] ?>">Ответить</a>
                                                </div>
                                                <div class="btn-ot-group d-flex">
                                                    <div class="btn-ot-cool">
                                                        <a href="#" class="like-dislike"
                                                           data-review-id="<?= $rew2["review_id"] ?>"
                                                           data-like="1"></a>
                                                        <span><?= $rew2["clike"]; ?></span>
                                                    </div>
                                                    <div class="btn-ot-pool">
                                                        <a href="#" class="like-dislike"
                                                           data-review-id="<?= $rew2["review_id"] ?>"
                                                           data-like="0"></a>
                                                        <span><?= $rew2['cdislike'] ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php foreach ($all_rew as $rew3) {
                                            if ($rew2['review_id'] == $rew3['rew_id']) { ?>
                                                <?php if ($rew2['customer_id'] == 1) {
                                                    $class = 'admin';
                                                } else {
                                                    $class = 'user-2';
                                                } ?>
                                                <div class="otziv col-10 user <?= $class ?>">
                                                    <div class="wr-ad-bg">
                                                        <div class="top-otziv d-flex mb-2">
                                                            <div class="otz-name-admin"><?= $rew3["author"]; ?></div>
                                                        </div>
                                                        <div class="desc-otz mb-2"><?= $rew3["text"]; ?></div>
                                                    </div>
                                                    <div class="bottom-otziv d-flex mb-2">
                                                        <div class="reply-btn color-theme">
                                                            <i class="fa fa-reply" aria-hidden="true"></i>
                                                            <a href="" class="color-theme"
                                                               data-review-id-xtz="<?= $rew3["review_id"] ?>">Ответить</a>
                                                        </div>
                                                        <div class="btn-ot-group d-flex">
                                                            <div class="btn-ot-cool">
                                                                <a href="#" class="like-dislike"
                                                                   data-review-id="<?= $rew3["review_id"] ?>"
                                                                   data-like="1"></a>
                                                                <span><?= $rew3["clike"]; ?></span>
                                                            </div>
                                                            <div class="btn-ot-pool">
                                                                <a href="#" class="like-dislike"
                                                                   data-review-id="<?= $rew3["review_id"] ?>"
                                                                   data-like="0"></a>
                                                                <span><?= $rew3['cdislike'] ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php foreach ($all_rew as $rew4) {
                                                    if ($rew3['review_id'] == $rew4['rew_id']) { ?>
                                                        <?php if ($rew2['customer_id'] == 1) {
                                                            $class = 'admin';
                                                        } else {
                                                            $class = 'user-2';
                                                        } ?>
                                                        <div class="otziv col-10 user <?= $class ?>">
                                                            <div class="wr-ad-bg">
                                                                <div class="top-otziv d-flex mb-2">
                                                                    <div class="otz-name-admin"><?= $rew4["author"]; ?></div>
                                                                </div>
                                                                <div class="desc-otz mb-2"><?= $rew4["text"]; ?></div>
                                                            </div>
                                                            <div class="bottom-otziv d-flex mb-2">
                                                                <div class="reply-btn color-theme">
                                                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                                                    <!--                                                        <a href="" class="color-theme">Ответить</a>-->
                                                                </div>
                                                                <div class="btn-ot-group d-flex">
                                                                    <div class="btn-ot-cool">
                                                                        <a href="#" class="like-dislike"
                                                                           data-review-id="<?= $rew4["review_id"] ?>"
                                                                           data-like="1"></a>
                                                                        <span><?= $rew4["clike"]; ?></span>
                                                                    </div>
                                                                    <div class="btn-ot-pool">
                                                                        <a href="#" class="like-dislike"
                                                                           data-review-id="<?= $rew4["review_id"] ?>"
                                                                           data-like="0"></a>
                                                                        <span><?= $rew4['cdislike'] ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
            </div>
            <?php if (!empty ($quantity_comments)){ ?>
              <?php if ($quantity_comments >= 5){ ?>
            <div class="col-12">
                <div class="wr-refresh d-flex justify-content-center mb-3">
                    <span class="fa fa-refresh"></span>
                    <a href="#" id="show_all_reviews">Посмотреть все отзывы</a>
                </div>
            </div>
          <?php } ?>
          <?php if ($quantity_comments >=3){ ?>)

            <div class="wr-all-otzivi">
                <a href="" class="btn-my btn-all-otzivi"><img src="image/fly.png" alt=""> Написать отзыв</a>

            </div>
          <?php } ?>
        <?php } ?>
        </div>
    </div>
</div>
<input type="button" style="display: none" value="QEWQWEQWEQWEQWE" id="trigger-modal" data-toggle="modal"
       data-target="#exampleModal">

<input type="hidden" style="display: none" id="news-add-rew" value="1">
<input type="hidden" style="display: none" name="product_id" value="<?=$data['product_id']?>">


<script>

    // Модалка для добавления отзыва

    $('.btn-my.btn-all-otzivi').on('click', function (e) {
        e.preventDefault();
        $('#trigger-modal').trigger('click');
        $('input[name="product_id"]').val(<?=$data['product_id']?>);
        $('input[name="review_id"]').val(0);
        $('#news-add-rew').val();
    });

    $('a.color-theme').on('click', function (e) {
        e.preventDefault();
        $('#trigger-modal').trigger('click');
        var review_id = $(this).data('reviewIdXtz');
        $('input[name="review_id"]').val(review_id);
    });

    // лайки
    $('.like-dislike').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('reviewId');
        var like = $(this).data('like');
        var x = $(this).parent().find('span').text();
        $(this).parent().find('span').text(++x);
        $(this).parent().parent().css({'cursor': 'default', 'pointer-events': 'none'});
        $.ajax({
            url: 'index.php?route=information/news/update_like',
            type: 'post',
            data: {'review_id': id, 'like': like},
            success: function (json) {
                console.log(json);
            },
            error: function (jqXHR, textStatus, errorThrown) {

                console.log('ERRORS: ' + textStatus);
            }
        });
    });

    $('#show_all_reviews').on('click',function (e) {
        e.preventDefault();
        $('.qrew').each(function () {

            $(this).css('display', 'block');
            $('.user').css('display', 'block');

        });
    });

    $(document).ready(function () {
        var count = 1;
        $('.qrew').each(function () {
            if (count > 5) {
                $(this).css('display', 'none');
                $('.user').css('display', 'none');
            }
            count++;
        });
    });

</script>
<?php echo $footer; ?>
