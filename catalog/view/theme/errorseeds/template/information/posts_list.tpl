<?php echo $header; ?>
<style>
    /*body{*/
    /*background:#fff ;*/
    /*}*/
    .container{
        padding-left: 0px;
        padding-right: 0px;
    }
    .action-name {
        color: #333333;
    }
    .image_for_news_list{
        background:#fff url(image/blog.jpg) ;
        height: 300px;
    }
    .h1-all-news{
        display: block;
        /*color: #7d7d7d;*/
        color: white;
        z-index: 99999999;
        text-align: center;
        padding-bottom: 20px
    }
</style>

    <div class="image_for_news_list"><p style="opacity: 0">asd</p></div>

<div class="wrapper">
    <div class="container">
        <div class="sort-wr">
            <div class="sort row flex-wrap justify-content-between">
                <div class="back-cat back-cat2 col-12 d-flex">

                    <?php $i = 0;
                    foreach ($breadcrumbs as $breadcrumb) {
                        $i++; ?>
                        <?php if ($i == 1) {
                            $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                        } else {
                            $class = '';
                        } ?>
                        <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                            <span> /</span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="h1-all-news">
            <h1><?=$text_h1_posts?></h1>
        </div>

        <div class="wr-action-page">
            <?php foreach ($all_posts as $posts) { ?>
                <div class="action">
                    <a href="<?php echo $posts['view']; ?>">
                        <img src="<?= $posts['image']; ?>" alt="" class="img-fluid">
                        <div class="action-desc">
                            <div class="action-name"><?php echo $posts['title']; ?></div>
                            <?php echo $posts['description']; ?>
                            <div class="action-bottom d-flex flex-wrap justify-content-between">
                                <div class="col-12 col-md-6 dop-action">
                                    <div class="time-action color-theme"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $posts['date_added']; ?></div>
                                </div>
                                <div class="col-12 col-md-6 dop-action text-right">
                                    <div class="comment-action color-theme"><i class="fa fa-comment-o" aria-hidden="true"></i> <?= $read_more; ?></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>

        </div>


        <div class="pag-all-page d-flex flex-wrap justify-content-center">
            <nav aria-label="Page navigation example">
                <?php echo $pagination; ?>
            </nav>
        </div>
    </div>
</div>


<?php echo $footer; ?>
