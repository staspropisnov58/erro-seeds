<?php echo $header; ?>

<div class="container">
    <div class="sort-wr">
        <div class="sort row flex-wrap justify-content-between">
            <div class="back-cat col-12 col-md-5 align-self-center">
                <?php $i = 0;
                foreach ($breadcrumbs as $breadcrumb) {
                    $i++; ?>
                    <?php if ($i == 1) {
                        $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                    } else {
                        $class = '';
                    } ?>
                    <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                        <span> /</span></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php echo $description; ?>

<script>
    $(document).ready(function(){

        setTimeout(function () {
//            $("a[href$='#h5']").trigger('click');
            $("a[href$='#h0']").trigger('click');
        }, 100);


    });
</script>

<?php echo $footer; ?>
