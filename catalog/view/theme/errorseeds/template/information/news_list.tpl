<?php echo $header; ?>
<style>
    body{
        background:#fcfcfc;
        /*background-size: contain!important;*/
    }
    .wrapper {
        background: #fcfcfc;
    }

    .page-link {
        color: #7d7d7d;
        border: 2px solid #7d7d7d;
    }

    .action-name {
        color: #333333;
    }
    .back-cat a{
        color: #7d7d7d;
        text-decoration: none;
        text-transform: uppercase;
        font-size: 14px;
    }
  .back-cat {color: #7d7d7d}
    .container{
        padding-left: 0px;
        padding-right: 0px;
    }
    .action-desc{
        box-shadow: 0 0 10px rgb(117, 113, 113);
    }
    .image_for_news_list{
        background:#fff url(image/blog.jpg) ;
        height: 300px;
    }
    #sticker-sticky-wrapper{
        height: 121px!important;
        margin-top: -1px!important;
    }
    .sort-wr{
        width: 200px;
    }
</style>
<div class="image_for_news_list"><p style="opacity: 0">asd</p></div>

<div class="wrapper">
    <div class="container">
        <div class="sort-wr">

                <div class="back-cat">

                    <?php $i = 0;
                    foreach ($breadcrumbs as $breadcrumb) {
                        $i++; ?>
                        <?php if ($i == 1) {
                            $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                        } else {
                            $class = '';
                        } ?>
                        <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                            <span> /</span></a>
                    <?php } ?>
                </div>

        </div>
        <div class="h1-all-news" style="display: block;color: #7d7d7d;z-index: 99999999;text-align: center;padding-bottom: 20px">
            <h1><?=$h1_news?></h1>
        </div>
        <div class="wr-action-page">
            <?php foreach ($all_news as $news) { ?>
            <div class="action">
                <a href="<?php echo $news['view']; ?>">
                    <img src="<?= $news['image_2']; ?>" alt="" class="img-fluid">
                    <div class="action-desc">
                        <div class="action-name"><?php echo $news['title']; ?></div>
                        <?php echo $news['description']; ?>
                        <div class="action-bottom d-flex flex-wrap justify-content-between">
                            <div class="col-12 col-md-6 dop-action">
                                <div class="time-action color-theme"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $news['date_added']; ?></div>
                            </div>
                            <div class="col-12 col-md-6 dop-action text-right">
                                <div class="comment-action color-theme"><i class="fa fa-comment-o" aria-hidden="true"></i> <?= $read_more; ?></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?>

        </div>
        <div class="pag-all-page d-flex flex-wrap justify-content-center" style="display: none">
            <nav aria-label="Page navigation example">
                <?php echo $pagination; ?>
            </nav>
<!--            <a href="" class="all-vis-tovar">показать все</a>-->
        </div>
<!--        <div class="wr-all-action">-->
<!--            <a href="" class="btn-all-news">Показать больше</a>-->
<!--        </div>-->
    </div>
</div>

<?php echo $footer; ?>
