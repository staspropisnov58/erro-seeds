<?php echo $header; ?>

<div class="wrapper">
    <div class="container">
        <div class="sort-wr">
            <div class="sort row flex-wrap justify-content-between">
                <div class="back-cat col-12 col-md-5 align-self-center">
                    <?php $i = 0;
                    foreach ($breadcrumbs as $breadcrumb) {
                        $i++; ?>
                        <?php if ($i == 1) {
                            $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                        } else {
                            $class = '';
                        } ?>
                        <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                            <span> /</span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="information-page">
            <p><strong><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $address; ?></strong></p>
            <div class="map mb-3">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2564.3162203240663!2d36.22220441571529!3d50.00542797941644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a11e3510c145%3A0x95c4bd2cdf464fb6!2z0L_RgNC-0YHQv9C10LrRgiDQndC10LfQsNC70LXQttC90L7RgdGC0ZYsIDEsINCl0LDRgNC60ZbQsiwg0KXQsNGA0LrRltCy0YHRjNC60LAg0L7QsdC70LDRgdGC0Yw!5e0!3m2!1sru!2sua!4v1509262488905"
                        style="width:100%;height:380px;" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="h1-contacts-x" style="padding-top: 10px;padding-bottom: 20px;">
                <h1><?=$text_h1_contacts;?></h1>
            </div>
            <div class="contact-page-bottom clearfix">
                <div class="col-xs-12 col-md-4 ferst_block_contact">
                    <div>
                        <div class="ic-con-page"><i class="fa fa-phone" aria-hidden="true"></i></div>
                        <div class="tel-con-page">
                            <?php $phone = explode(';', $telephone); ?>
                            <p>  <a href="tel:<?= $phone[0] ? $phone[0] : ''; ?>"><?= $phone[0] ? $phone[0] : ''; ?></a><p>
                            <p><a href="tel:<?= $phone[1] ? $phone[1] : ''; ?>"><?= $phone[1] ? $phone[1] : ''; ?></a><p>
                            <p> <a href="tel:<?= $phone[2] ? $phone[2] : ''; ?>"><?= $phone[2] ? $phone[2] : ''; ?></a><p>
<!--                            <p>--><?//= $phone[0] ? $phone[0] : ''; ?><!--<p>-->
<!--                            <p>--><?//= $phone[1] ? $phone[1] : ''; ?><!--<p>-->
<!--                            <p>--><?//= $phone[2] ? $phone[2] : ''; ?><!--<p>-->
                            <p>
                            </p>

                            <p></p>
                            <p> <a href="tel:<?= $phone[3] ? $phone[3] : ''; ?>"><?php echo $text_telephone; ?>: <br> <?= $phone[3] ? $phone[3] : ''; ?></a><p>
<!--                            <p>--><?php //echo $text_telephone; ?><!--: --><?//= $phone[3] ? $phone[3] : ''; ?><!--</p>-->
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 second_block_contact">
                    <div>
                        <div class="ic-con-page"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                        <div class="tel-con-page">
                            <?php $mail = explode(';', $fax); ?>
                            <p><a href="mailto:<?= $mail[0] ? $mail[0] : ''; ?>"><?= $mail[0] ? $mail[0] : ''; ?></a>
                            </p>
                            <p><a href="mailto:<?= $mail[1] ? $mail[1] : ''; ?>"><?= $mail[1] ? $mail[1] : ''; ?></a>
                            </p>
                            <p></p>
                            <p id="contact_us_xtz"><span class="btn-cont-page" style="cursor: pointer;" ><?php echo $text_address; ?><!--Напишите нам--></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 third_block_contact">
                  <?php echo $column_right; ?>
                </div>
            </div>
        </div>

        <div class="reg-page row" id="contact_us">
            <div class="wr-reg-page col-4 px-3 mx-auto">

                <form class="form-my" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">


                    <div class="form-group">
                        <?php if ($error_name) { ?>
                        <p style="color: #ff3333;font-size: 18px;"><?php echo $error_name; ?></p>
                        <?php } ?>

                        <label for="formEmail" class="color-label"><?php echo $entry_name; ?></label>
                        <input type="text" name="name" value="<?php echo $name; ?>"
                               placeholder="<?php echo $entry_name; ?>" id="input-name"
                               class="form-control trans-input form-control-success"/>
                    </div>


                    <div class="form-group">
                        <?php if ($error_email) { ?>
                            <p style="color: #ff3333;font-size: 18px;"><?php echo $error_email; ?></p>
                        <?php } ?>
                        <label for="formEmail" class="color-label"><?php echo $entry_email; ?></label>
                        <input type="text" name="email" value="<?php echo $email; ?>"
                               placeholder="<?php echo $entry_email; ?>" id="formEmail"
                               class="form-control trans-input form-control-success"/>
                    </div>



                    <div class="form-group">
                        <?php if ($error_enquiry) { ?>
                            <p style="color: #ff3333;font-size: 18px;"><?php echo $error_enquiry; ?></p>
                        <?php } ?>
                        <label class="color-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
                        <textarea name="enquiry" rows="10" id="input-enquiry"
                                  class="form-control trans-input form-control-success"><?php echo $enquiry; ?></textarea>

                    </div>

                    <?php if ($error_g_recaptcha_response_from_server) { ?>
                        <p style="color: #ff3333;font-size: 18px;"><?php echo $error_g_recaptcha_response_from_server; ?></p>
                    <?php } ?>
                    <div class="g-recaptcha" data-sitekey="6LenwzkUAAAAAK2lXYbR83UQnFgtXyxgihhddZt5"></div>



                    <button class="form-btn btn-reg"><?php echo $button_submit; ?></button>
                </form>

            </div>
        </div>
    </div>
</div>




<script>

    $('#contact_us_xtz').click(function () {
        $(this).toggleClass('toggled');
        $('#contact_us').toggle(900);
    });


</script>
<script>
    $('input').on('change',function () {
        if($(this).val()){

            $(this).parent().addClass('has-success')
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');

        }
    });
    $('textarea').on('change',function () {
        if($(this).val()){

            $(this).parent().addClass('has-success')
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');

        }
    });
</script>
<?php echo $footer; ?>
