<?php echo $header; ?>
<div class="container">
  <div class="back-cat col-12 col-md-5 align-self-center">
      <?php  foreach ($breadcrumbs as $key => $breadcrumb) {
        if ($key == 0) {
              $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
          } else {
              $class = '';
          } ?>
          <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>

              <span> /</span></a>
      <?php } ?>
  </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> sitemap_wraper"><?php echo $content_top; ?>
          <div class="row">
            <div class="col-xs-12 sitemap_block">
              <?php foreach ($map as $level1) { ?>
          <div class="col-xs-12 col-sm-6">
            <ul>
              <?php if (isset($level1['href']) && isset($level1['name'])) { ?>
                <li>
                  <h3><a href="<?=$level1['href']?>"><?=$level1['name']?></a></h3></li>
              <?php } ?>
              <?php if (isset($level1['content'])) { ?>
                <li>
                <ul class="sitemap_line2">
                  <?php foreach ($level1['content'] as $level2) { ?>
                    <li>
                  <a href="<?=$level2['href']?>">
                    <?=$level2['name']?>
                  </a>
                </li>
                <?php } ?>
                </ul>
              </li>
              <?php } ?>
            </ul>
          </div>

        <?php } ?>

          </div>
         </div>
        </div>

        <?php echo $column_right; ?></div>
        <div class="pagination__wrap">
            <?php echo $pagination; ?>
        </div>
</div>
<?php echo $footer; ?>
