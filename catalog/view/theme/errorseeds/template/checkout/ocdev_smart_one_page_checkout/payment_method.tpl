<div class="step-cont" id="payment-method">
    <div class="color-label"><?php echo $heading_payment_block; ?>:</div>

        <?php if ($error_warning_payment) { ?>
            <div class="smopc-alert alert smopc-alert-warning"><i
                        class="fa fa-exclamation-circle"></i> <?php echo $error_warning_payment; ?></div>
        <?php } ?>
        <?php if ($payment_methods) { ?>
<!--            <p>--><?php //echo $text_payment_method; ?><!--</p>-->
            <?php if ($display_payment_type == 1) { ?>
                <?php $i = 0; foreach ($payment_methods as $payment_method) { $i++;?>
                    <?php $i == 1 ? $img = 'privat.jpg' : $img = 'nalog.png'; ?>
                    <label class="block-dost d-flex block-check-n">

                            <?php if ($payment_method['code'] == $code_p || !$code_p) { ?>
                                <?php $code_p = $payment_method['code']; ?>
                                <input style="display: none" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>"
                                       checked="checked"
                                       onchange="save_payment('#payment-method', '#payment-method', 'onload');"/>
                            <?php } else { ?>
                                <input style="display: none" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>"
                                       onchange="save_payment('#payment-method', '#payment-method', 'onload');"/>
                            <?php } ?>
<!--                            --><?php //echo $payment_method['title']; ?>
                            <?php if ($payment_method['terms']) { ?>
                                (<?php echo $payment_method['terms']; ?>)
                            <?php } ?>



                            <div class="block-check-n-img col-3 p-0">
                                <img src="image/<?=$img;?>" alt="">
                            </div>
                            <div class="block-check-n-check align-self-center col-2">
                                <?php if ($payment_method['code'] == $code_p || !$code_p) { ?>
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                <?php } ?>
                            </div>
                            <div class="block-check-n-text align-self-center col-7 p-0">
                                <?php echo $payment_method['title']; ?>
                            </div>



                    </label>
                <?php } ?>
            <?php } else { ?>
                <select name="payment_method" class="smopc-form-control"
                        onchange="save_payment('#payment-method', '#payment-method', 'onload');">
                    <?php foreach ($payment_methods as $payment_method) { ?>
                        <?php if ($payment_method['code'] == $code_p || !$code_p) { ?>
                            <?php $code_p = $payment_method['code']; ?>
                            <option value="<?php echo $payment_method['code']; ?>"
                                    selected="selected"><?php echo $payment_method['title']; ?><?php if ($payment_method['terms']) { ?>(<?php echo $payment_method['terms']; ?>)<?php } ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?><?php if ($payment_method['terms']) { ?>(<?php echo $payment_method['terms']; ?>)<?php } ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            <?php } ?>
        <?php } ?>

    <?php if ($text_agree) { ?>
        <div class="smopc-panel-footer">
            <input type="hidden" name="skip_payment_agree" value="1"/>
            <input type="checkbox" name="payment_agree"
                   value="1" <?php echo ($payment_agree) ? 'checked="checked"' : ''; ?>
                   onchange="save_payment('#payment-method', '#payment-method', 'onload');"/>
            <?php echo $text_agree; ?>
        </div>
    <?php } ?>


    <script type="text/javascript"><!--
        // multi-function for save payment data start
        function save_payment(block, refresh, type) {
            masked(refresh, true);
            $('#checkout-buttons-bottom').show();
            $('input[data-button-type=\'checkout\']').button('reset');
            if (type == "onload") {
                $.ajax({
                    url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/payment_save',
                    type: 'post',
                    data: $(block + ' input[type=\'radio\']:checked, ' + block + ' input[type=\'hidden\'], ' + block + ' input[type=\'checkbox\']:checked, ' + block + ' select'),
                    dataType: 'json',
                    success: function (json) {
                        $(block + ' .smopc-alert, ' + block + ' .smopc-text-danger').remove();

                        if (json['error']) {
                            masked(refresh, false);
                            if (json['error']['warning']) {
                                $(block + ' .smopc-panel-body').prepend('<div class="smopc-alert alert smopc-alert-warning">' + json['error']['warning'] + '<button type="button" class="smopc-close" data-dismiss="alert">&times;</button></div>');
                            }
                            $('html, body').animate({scrollTop: 0}, 'slow');
                        } else {
                            $.ajax({
                                url: 'index.php?route=checkout/ocdev_smart_one_page_checkout',
                                dataType: 'html',
                                success: function (data) {
                                    $(block).html($(data).find(block + ' > *'));
                                    $('#bottom-payment-block').html($(data).find('#bottom-payment-block > *'));
                                    masked(refresh, false);
                                }
                            });
                        }
                    }
                });
            } else {
                $.ajax({
                    url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/payment_save',
                    type: 'post',
                    data: $(block + ' input[type=\'radio\']:checked, ' + block + ' input[type=\'checkbox\']:checked, ' + block + ' select'),
                    dataType: 'json',
                    beforeSend: function () {
                        $('input[data-button-type=\'checkout\']').button('loading');
                    },
                    complete: function () {
                        $('input[data-button-type=\'checkout\']').button('reset');
                    },
                    success: function (json) {
                        $(block + ' .smopc-alert, ' + block + ' .smopc-text-danger').remove();

                        if (json['error']) {
                            masked(refresh, false);
                            $('input[data-button-type=\'checkout\']').button('reset');

                            if (json['error']['warning']) {
                                $(block + ' .smopc-panel-body').prepend('<div class="smopc-alert alert smopc-alert-warning">' + json['error']['warning'] + '<button type="button" class="smopc-close" data-dismiss="alert">&times;</button></div>');
                            }
                            $('html, body').animate({scrollTop: 0}, 'slow');
                        } else {
                            $.ajax({
                                url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/confirm',
                                dataType: 'html',
                                success: function (data) {
                                    $('#bottom-payment-block').show();
                                    $('input[data-button-type=\'checkout\']').button('loading');
                                    $('#payment').html(data);
                                    <?php if ($cancel_button == 2 || $cancel_button == 3) { ?>
                                    $('#payment').find('.buttons').last().append('<a href="<?php echo $continue; ?>" class="smopc-btn smopc-btn-default"><?php echo $button_cancel; ?></a>');
                                    <?php } ?>
                                    $('#checkout-buttons-bottom').hide();
                                    var pay_form = $('#payment form').html();
                                    if (typeof(pay_form) === "undefined" || pay_form == "") {
                                        $('#button-confirm').click();
                                    }
                                    masked(refresh, false);
                                }
                            });
                        }
                    }
                });
            }
        }

        // multi-function for save payment data end
        //--></script>