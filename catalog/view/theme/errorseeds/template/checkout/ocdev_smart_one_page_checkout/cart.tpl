<style>

    .name-checkout-basket.mb-2 {
        font-size: 14px;
        line-height: initial;
        font-family: "OpenSansRegular";
    }
    /*.smopc-btn.smopc-btn-primary.xtz{*/
    /*background: none;*/
    /*border-radius: 26px;*/
    /*border-color: #292b2c;*/
    /*}*/
    .smopc-btn.smopc-btn-primary.xtz_minus , .smopc-btn.smopc-btn-primary.xtz_plus{
        border: 2px solid #292b2c;
        border-radius: 50%;

        font-size: 18px;
        line-height: initial;
        cursor: pointer;
        padding: 0px 7px;
        display: inline-block;
        text-align: center;

        margin: 0 6px;
        width: 28px;
    }

    .smopc-btn-primary{
        background-image: none;
    }


    .smopc-btn.smopc-btn-primary:hover{
        background: #82b020;
    }

    .smopc-form-control{
        border: none!important;
        font-size: 22px;
        font-family: "OpenSansBold";
        color: #666;
        z-index: -1;
    }
    .smopc-form-control:focus{
        border:none!important;
        background-color: white;
    }

    .smopc-input-group-btn{
        padding-right: 13px;
    }

    .product_name_x{
        text-align: left;
        font-size: 15px;
    }
    .total_x{
        font-size: 16px;
        font-family: 'OpenSansBold';
        color: #666;
    }
    .mb-3{
        margin-bottom: 0rem !important;
    }
    .cart_item>div{
      padding: 0 5px;
    }
    @media (max-width:767px) {
      .cart_item>div{
        text-align: center;
            margin: 10px auto;
      }
      .cart_item img{width: 50%}
      .smopc-input-group{margin: 0 auto;}
      .cart_item{
        border-bottom: 1px dashed #000
      }
    }
    /*.basket-checkout{*/
        /*position: fixed;*/
        /*z-index: 9999;*/
    /*}*/
</style>
<!-- <head> -->
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!-- </head> -->

<div class="basket-checkout-wr" id="checkout-cart">

    <div class="smopc-panel-heading">
        <div class="basket-checkout-name"><i class="fa fa-shopping-basket"
                                             aria-hidden="true"></i> <?php echo $heading_cart_block; ?></div>
    </div>


    <?php if ($attention_cart) { ?>
        <div class="smopc-alert alert smopc-alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention_cart; ?>
            <button type="button" class="smopc-close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <?php if ($error_warning_cart) { ?>
        <div class="smopc-alert alert smopc-alert-danger"><i
                    class="fa fa-exclamation-circle"></i> <?php echo $error_warning_cart; ?>
            <button type="button" class="smopc-close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>

    <div class="cart_xtz">
      <div>
          <!-- <thead>
          <tr style="display: none">
              <?php if ($hide_main_img) { ?>
                  <td class="smopc-text-center"><?php echo $column_image; ?></td><?php } ?>
              <td class="smopc-text-left"><?php echo $column_name; ?></td>
              <td class="smopc-text-left" style="width:21%;"><?php echo $column_quantity; ?></td>
              <td class="smopc-text-right"><?php echo $column_price; ?></td>
              <td class="smopc-text-right"><?php echo $column_total; ?></td>
              <td class="smopc-text-center"><?php echo $column_remove; ?></td>
          </tr>
          </thead> -->
          <div class="col-12">
            <div class="row">
          <?php foreach ($products as $product) { ?>
              <div class="d-flex justify-content-center align-items-center cart_item flex-wrap">
                  <?php if ($hide_main_img) { ?>
                      <div class="col-12 col-md-3"><?php if ($product['thumb']) { ?>
                              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>"
                                                                             alt="<?php echo $product['name']; ?>"
                                                                             title="<?php echo $product['name']; ?>"/></a>
                          <?php } ?>
                      </div>
                  <?php } ?>
                  <div class="smopc-text-center product_name_x col-12 col-md-3" style="">

                      <div class="name-checkout-basket mb-2">  <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                      <?php if (!$product['stock']) { ?>
                          <span class="smopc-text-danger">***</span>
                      <?php } ?>
                      <?php if ($product['option']) { ?>
                          <?php foreach ($product['option'] as $option) {
                              $x = explode('-',$option['value']);
                              ?>

                              <div class="fas-checkout mb-3"><?php echo $packing; ?>: <?php echo $x[0].' '.$pieces; ?></div>
                          <?php }  ?>
                      <?php } ?>
                      <div class="del-checkout"><i class="fas fa-trash" aria-hidden="true"></i><a style="cursor: pointer"  data-id="<?php echo $product['product_id']; ?>" data-prod_key="<?php echo $product['key']; ?>" onclick="update_cart(this, 'remove', <?php echo $product['product_id']; ?>);"><?php echo $text_delete_from_cart; ?></a></div>
                      <?php if ($product['reward']) { ?>
                          <br/>
                          <small><?php echo $product['reward']; ?></small>
                      <?php } ?>
                      <?php if ($product['recurring']) { ?>
                          <br/>
                          <span class="label label-info"><?php echo $text_recurring_item; ?></span>
                          <small><?php echo $product['recurring']; ?></small>
                      <?php } ?>
                  </div>
                  <div class="smopc-text-left col-12 col-md-3">
                      <input name="product_id_q" value="<?php echo $product['product_id']; ?>" style="display: none;"
                             type="hidden"/>
                      <input name="product_id" value="<?php echo $product['key']; ?>" style="display: none;"
                             type="hidden"/>


                      <!-- <div class="smopc-input-group smopc-btn-block" >
                  <span class="smopc-input-group-btn asd">
                  <a onclick="$(this).parent().next().val(~~$(this).parent().next().val()-1); update_cart(this, 'update');"
                                         class="smopc-btn smopc-btn-primary xtz_minus">-</a>
                  </span>
                      <input style="display: block" type="text" name="quantity" value="<?php /*echo $product['quantity']; */?>" size="1"
                             class="smopc-form-control"
                             onchange="update_cart_input(this, '<?php /*echo $product['key']; */?>'); return validate_input(this);"
                             onkeyup="update_cart_input(this, '<?php /*echo $product['key']; */?>'); return validate_input(this);"/>
                      <span class="count">x<span class="ng-binding"><?php /*echo $product['quantity']; */?></span></span>
                      <span class="smopc-input-group-btn">
                  <a onclick="$(this).parent().prev().val(~~$(this).parent().prev().val()+1); update_cart(this, 'update');"
                                         class="smopc-btn smopc-btn-primary xtz_plus">+</a>
                  </span>
                  </div>
-->
                      <div class="smopc-input-group smopc-btn-block col" style="max-width: 50px;">
                  <span class="smopc-input-group-btn">
                  <a onclick="$(this).parent().next().val(~~$(this).parent().next().val()-1); update_cart(this, 'update');"
                                         class="smopc-btn smopc-btn-primary xtz_minus">-</a>
                  </span>


                          <input style="font-size: 22px;font-family: 'OpenSansBold';color: #666;background: white;" type="text" name="quantity" disabled="disabled" value="<?php echo $product['quantity']; ?>" size="1"
                                 class="smopc-form-control"
                                 onchange="update_cart_input(this, '<?php echo $product['key']; ?>'); return validate_input(this);"
                                 onkeyup="update_cart_input(this, '<?php echo $product['key']; ?>'); return validate_input(this);"/>
                          <span class="smopc-input-group-btn">
                  <a onclick="$(this).parent().prev().val(~~$(this).parent().prev().val()+1); update_cart(this, 'update');"
                                         class="smopc-btn smopc-btn-primary xtz_plus">+</a>
                  </span>
                      </div>

                      <!--<div class="smopc-input-group smopc-btn-block" style="max-width: 50px;">
                  <span class="smopc-input-group-btn">
                  <a onclick="$(this).parent().next().val(~~$(this).parent().next().val()-1); update_cart(this, 'update');"
                                         class="smopc-btn smopc-btn-primary"><i class="fa fa-minus"></i></a>
                  </span>
                      <input type="text" name="quantity" value="<?php /*echo $product['quantity']; */?>" size="1"
                             class="smopc-form-control"
                             onchange="update_cart_input(this, '<?php /*echo $product['key']; */?>'); return validate_input(this);"
                             onkeyup="update_cart_input(this, '<?php /*echo $product['key']; */?>'); return validate_input(this);"/>
                      <span class="smopc-input-group-btn">
                  <a onclick="$(this).parent().prev().val(~~$(this).parent().prev().val()+1); update_cart(this, 'update');"
                                         class="smopc-btn smopc-btn-primary"><i class="fa fa-plus"></i></a>
                  </span>
                  </div>-->


                </div>
                  <div style="opacity: 0" class="smopc-text-right col-12 col-md-1"><?php echo $product['price']; ?></div>
                  <div class="smopc-text-right total_x col-12 col-md-2"><?php echo $product['total']; ?></div>
                  <div class="smopc-text-center col" style="display: none"><a data-id="<?php echo $product['product_id']; ?>" onclick="update_cart(this, 'remove', <?php echo $product['product_id']; ?>);"
                                                                         class="smopc-btn smopc-btn-danger"><i
                                  class="fas fa-times-circle"></i></a></div>
              </div>
          <?php } ?>

          </div>
        </div>
          <div style="color: #666;font-family: 'OpenSansBold'; font-size: 14px; text-align: right; margin-top: 25px;">
          <!--        --><?php //echo "<pre style='color: black'>";?>
          <?php $i = 0; foreach ($totals as $total) { $i++;?>
              <?php  if ($i == 4){
                  $color = '#ff3333';
              }elseif ($i == 3){
                  $color = '#ff3333';
              }else{
                  $color='';
              }?>
              <div>
                  <p class="smopc-text-right">
                      <strong><?php echo $total['title']; ?>:</strong>
                  <span class="smopc-text-right" id="total-<?=$total['code']?>" style="color: <?=$color;?>;"> <?php echo $total['text']; ?></span>
                  <span class="smopc-text-left"></span></p>
              </div>
          <?php } ?>
        </div>
      </div>
    </div>
</div>


<!--СТАНДАРТ-->
<script type="text/javascript"><!--
    // update cart finctions start
    function update_cart(target, status) {
        masked('#checkout-cart', true);
        var input_val    = $(target).parent().parent().children('input[name=quantity]').val(),
            quantity     = parseInt(input_val),
            product_id   = $(target).parent().parent().parent().children('input[name=product_id]').val(),
            product_id_q = $(target).parent().parent().parent().children('input[name=product_id_q]').val(),
            product_key  = $(target).parent().prev().prev().prev().children('input[name=product_id]').val(),
            urls         = null;

        if (quantity <= 0) {
            masked('#checkout-cart', false);
            quantity = $(target).parent().parent().children('input[name=quantity]').val(1);
            return;
        }

        if (status == 'update') {
            urls = 'index.php?route=checkout/ocdev_smart_one_page_checkout&update=' + product_id + '&quantity=' + quantity;
        } else if (status == 'add') {
            urls = 'index.php?route=checkout/ocdev_smart_one_page_checkout&add=' + target + '&quantity=1';
        } else {
            urls = 'index.php?route=checkout/ocdev_smart_one_page_checkout&remove=' + product_key;
        }

        $.ajax({
            url: urls,
            type: 'get',
            dataType: 'html',
            beforeSend: function() {
                $(target).html('<i class="fa fa-refresh fa-spin"></i>');
            },
            success: function(data) {
                $.ajax({
                    url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/status_cart',
                    type: 'get',
                    dataType: 'json',
                    success: function(json) {
                        masked('#checkout-cart', false);
                        if (json['total']) {
                            setTimeout(function () {
                                $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                            }, 100);
                            $('#cart > ul').load('index.php?route=common/cart/info ul li');
                        }
                        if (!json['redirect']) {
                            $('input[data-button-type=\'checkout\']').removeClass('disabled');

                            <?php if (isset($checkout_blocks) && in_array('payment', $checkout_blocks)) { ?>save_payment('#payment-method', '#payment-method', "onload");<?php } ?>
                            $('#checkout-cart').html($(data).find('#checkout-cart > *'));
                            $('#shipping-method').html($(data).find('#shipping-method > *'));
                            <?php if (isset($checkout_blocks) && in_array('shipping', $checkout_blocks)) { ?>save_shipping('#shipping-method', '#shipping-method, #checkout-cart', "onload");<?php } ?>
                        } else {
                            location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function update_cart_input(target, product_id) {
        masked('#checkout-cart', true);
        var input_val = $(target).val(),
            quantity  = parseInt(input_val);

        if (quantity <= 0) {
            masked('#checkout-cart', false);
            quantity = $(target).val(1);
            return;
        }

        $.ajax({
            url: 'index.php?route=checkout/ocdev_smart_one_page_checkout&update=' + product_id + '&quantity=' + quantity,
            type: 'get',
            dataType: 'html',
            success: function(data) {
                $.ajax({
                    url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/status_cart',
                    type: 'get',
                    dataType: 'json',
                    success: function(json) {
                        masked('#checkout-cart', false);
                        if (json['total']) {
                            setTimeout(function () {
                                $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                            }, 100);
                            $('#cart > ul').load('index.php?route=common/cart/info ul li');
                        }
                        if (!json['redirect']) {
                            $('input[data-button-type=\'checkout\']').removeClass('disabled');
                            <?php if (isset($checkout_blocks) && in_array('shipping', $checkout_blocks)) { ?>save_shipping('#shipping-method', '#shipping-method, #checkout-cart', "onload");<?php } ?>
                            <?php if (isset($checkout_blocks) && in_array('payment', $checkout_blocks)) { ?>save_payment('#payment-method', '#payment-method', "onload");<?php } ?>
                            $('#checkout-cart').html($(data).find('#checkout-cart > *'));
                            $('#shipping-method').html($(data).find('#shipping-method > *'));
                        } else {
                            location = json['redirect'];
                        }
                    }
                });
            }
        });
    }
    // update cart finctions end
    //--></script>


<!--МОИ-->
<script type="text/javascript"><!--

    //    $(document).on('click','.remove_product_xtz',function (e) {
    //        e.preventDefault();
    //      var x =  $(this).parent();
    //      console.log(x);
    //    });


    // update cart finctions start
    function update_cart(target, status, prod = 0) {



        masked('#checkout-cart', true);
        var input_val = $(target).parent().parent().children('input[name=quantity]').val(),
            quantity = parseInt(input_val),
            product_id = $(target).parent().parent().parent().children('input[name=product_id]').val(),
            product_id_q = $(target).parent().parent().parent().children('input[name=product_id_q]').val(),
            product_key = $(target).parent().prev().prev().prev().children('input[name=product_id]').val(),
            product_key_xtz = $(target).data('prod_key'),
            urls = null;

        var key = '';

        if (product_key_xtz === undefined){
            key = product_key;
        }else{
            key = product_key_xtz;
        }





        if (quantity <= 0) {
            masked('#checkout-cart', false);
            quantity = $(target).parent().parent().children('input[name=quantity]').val(1);
            return;
        }


        var my_product_key = $('.ASD').val();
        //var my_product_key = prod;


        if (status == 'update') {
            urls = 'index.php?route=checkout/ocdev_smart_one_page_checkout&update=' + product_id + '&quantity=' + quantity;
        } else if (status == 'add') {
            urls = 'index.php?route=checkout/ocdev_smart_one_page_checkout&add=' + target + '&quantity=1';
        } else {
//            urls = 'index.php?route=checkout/ocdev_smart_one_page_checkout&remove=' + product_key;
            urls = 'index.php?route=checkout/ocdev_smart_one_page_checkout&remove=' + key;

        }

        $.ajax({
            url: urls,
            type: 'get',
            dataType: 'html',
            beforeSend: function () {
                $(target).html('<i class="fa fa-refresh fa-spin"></i>');
            },
            success: function (data) {

                $.ajax({
                    url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/status_cart',
                    type: 'get',
                    dataType: 'json',
                    success: function (json) {

                        masked('#checkout-cart', false);
                        if (json['total']) {
                            setTimeout(function () {

                                $('#cart-total').html(' <img src="image/basket.png"> <div class="count-top-header-bl">'+json['total']+'</div>');

                                $('#cart-total-xtz-left').text(json['total']);

                            }, 100);
                            $('#cart > ul').load('index.php?route=common/cart/info ul li');
                        }
                        if (!json['redirect']) {
                            $('input[data-button-type=\'checkout\']').removeClass('disabled');

                            <?php if (isset($checkout_blocks) && in_array('payment', $checkout_blocks)) { ?>save_payment('#payment-method', '#payment-method', "onload");<?php } ?>
                            $('#checkout-cart').html($(data).find('#checkout-cart > *'));
                            $('#shipping-method').html($(data).find('#shipping-method > *'));
                            <?php if (isset($checkout_blocks) && in_array('shipping', $checkout_blocks)) { ?>save_shipping('#shipping-method', '#shipping-method, #checkout-cart', "onload");<?php } ?>
                        } else {
                            location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    function update_cart_input(target, product_id) {
        masked('#checkout-cart', true);
        var input_val = $(target).val(),
            quantity = parseInt(input_val);




        if (quantity <= 0) {
            masked('#checkout-cart', false);
            quantity = $(target).val(1);
            return;
        }

        $.ajax({
            url: 'index.php?route=checkout/ocdev_smart_one_page_checkout&update=' + product_id + '&quantity=' + quantity,
            type: 'get',
            dataType: 'html',
            success: function (data) {
                $.ajax({
                    url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/status_cart',
                    type: 'get',
                    dataType: 'json',
                    success: function (json) {
                        masked('#checkout-cart', false);
                        if (json['total']) {
                            setTimeout(function () {
                                $('#cart-total').html(' <img src="image/basket.png"> <div class="count-top-header-bl">'+json['total']+'</div>');

                                $('#cart-total-xtz-left').text(json['total']);
                            }, 100);
                            $('#cart > ul').load('index.php?route=common/cart/info ul li');
                        }
                        if (!json['redirect']) {
                            $('input[data-button-type=\'checkout\']').removeClass('disabled');
                            <?php if (isset($checkout_blocks) && in_array('shipping', $checkout_blocks)) { ?>save_shipping('#shipping-method', '#shipping-method, #checkout-cart', "onload");<?php } ?>
                            <?php if (isset($checkout_blocks) && in_array('payment', $checkout_blocks)) { ?>save_payment('#payment-method', '#payment-method', "onload");<?php } ?>
                            $('#checkout-cart').html($(data).find('#checkout-cart > *'));
                            $('#shipping-method').html($(data).find('#shipping-method > *'));
                        } else {
                            location = json['redirect'];
                        }
                    }
                });
            }
        });
    }

    // update cart finctions end
    //--></script>
