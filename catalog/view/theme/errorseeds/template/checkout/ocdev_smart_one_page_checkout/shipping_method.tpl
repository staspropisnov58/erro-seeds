<div id="shipping_method_xtz_style">
<div class="step2 d-flex justify-content-start align-items-center">
    <div class="number-step">2</div>
    <div class="text-step"><?php echo $heading_shipping_block; ?></div>
</div>
</div>




			<div class="step-cont" id="shipping-method">
				<?php if ($error_warning_shipping) { ?>
				<div class="smopc-alert alert smopc-alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning_shipping; ?></div>
				<?php } ?>
				<?php if ($shipping_methods) { ?>
<!--					<p>--><?php //echo $text_shipping_method; ?><!--</p>-->
					<?php if ($display_shipping_type == 1) { ?>
						<?php foreach ($shipping_methods as $shipping_method) { ?>
							<?php if ($display_shipping_heading) { ?> <div class="color-label"><?php echo $shipping_method['title']; ?>:</div><?php } ?>
							<?php if (!$shipping_method['error']) { ?>
							<?php $i=0; foreach ($shipping_method['quote'] as $quote) { $i++;?>
                <label  class="block-dost d-flex block-check-n">
<!--							<div class="block-dost d-flex block-check-n">-->

									<?php if ($quote['code'] == $code_s || !$code_s) { ?>
									<?php $code_s = $quote['code']; ?>
									<input style="display: none" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked" onchange="save_shipping('#shipping-method', '#shipping-method, #checkout-cart', 'onload');" />
									<?php } else { ?>
									<input style="display: none" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" onchange="save_shipping('#shipping-method', '#shipping-method, #checkout-cart', 'onload');" />
									<?php } ?>
<!--									--><?php //echo $quote['title']; ?><!-- - --><?php //echo $quote['text']; ?>

                                    <?php $delivery_name = explode('#' ,$quote['title'] );?>

                                    <?php $i == 4 ? $img = 'novr-pochta.jpg' : $img = 'ukr-pochta.jpg'; ?>
                                    <div class="block-check-n-img col-3 p-0">
                                        <img src="image/<?=$img;?>" alt="">
                                    </div>
                                    <div class="block-check-n-check align-self-center col-2 gal" data-delivery_x="<?=$i;?>">


                                    <?php if ($quote['code'] == $code_s || !$code_s) { ?>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <?php } ?>
<!--                                        <i class="fa fa-check" aria-hidden="true"></i>-->
                                    </div>
                                    <div class="block-check-n-text align-self-center col-7 p-0">
                                        <?= $delivery_name[0];?><span>( <?= $delivery_name[1];?>)</span>
                                    </div>



<!--							</div>-->
                </label>
							<?php } ?>
							<?php } else { ?>
							<div class="smopc-alert alert smopc-alert-danger"><?php echo $shipping_method['error']; ?></div>
							<?php } ?>
						<?php } ?>
					<?php } else { ?>
						<select name="shipping_method" class="smopc-form-control" onchange="save_shipping('#shipping-method', '#shipping-method, #checkout-cart', 'onload');">
							<?php foreach ($shipping_methods as $shipping_method) { ?>
								<?php if ($display_shipping_heading) { ?><optgroup label="<?php echo $shipping_method['title']; ?>" <?php echo ($shipping_method['error']) ? 'disabled' : ''; ?>><?php } ?>
								<?php if (!$shipping_method['error']) { ?>
								<?php foreach ($shipping_method['quote'] as $quote) { ?>
								<?php if ($quote['code'] == $code_s || !$code_s) { ?>
									<?php $code_s = $quote['code']; ?>
									<option style="display:none;" value="<?php echo $quote['code']; ?>" selected="selected" <?php echo ($shipping_method['error']) ? 'disabled' : ''; ?>><?php echo $quote['title']; ?> - <?php echo $quote['text']; ?></option>
								<?php } else { ?>
									<option style="display: none" value="<?php echo $quote['code']; ?>" <?php echo ($shipping_method['error']) ? 'disabled' : ''; ?>><?php echo $quote['title']; ?> - <?php echo $quote['text']; ?></option>
								<?php } ?>
								<?php } ?>
								<?php } ?>
								<?php if ($display_shipping_heading) { ?></optgroup><?php } ?>
							<?php } ?>
						</select>
					<?php } ?>
				<?php } ?>
        <?php if ($additions) { ?>
            <?php foreach ($additions as $addition) { ?>
              <div class="color-label"><?=$addition['title']?>:</div>
              <?php foreach ($addition['products'] as $product) { ?>
                <label class="block-dost d-flex block-check-n">
                    <div class="block-check-n-img col-3 p-0">
                        <img src="<?=$product['thumb']?>" alt="">
                    </div>
                    <div class="block-check-n-check align-self-center col-2 gal_xx">
                    </div>
                    <input type="radio" name="additions" value="<?=$product['product_id']?>" style="display: none">
                    <div class="block-check-n-text align-self-center col-7 p-0">
                      <?=$product['name']?>
                      <span>
                        <?php if ($product['special']) {
                          echo $product['special'];
                        } else {
                          echo $product['price'];
                        } ?>
                      </span>
                    </div>
                </label>
              <?php } ?>
            <?php } ?>
        <?php } ?>
			</div>
			<?php if ($text_agree) { ?>
			<div class="smopc-panel-footer">
				<input type="hidden" name="skip_shipping_agree" value="1" />
				<input type="checkbox" name="shipping_agree" value="1" <?php echo ($shipping_agree) ? 'checked="checked"' : ''; ?> onchange="save_shipping('#shipping-method', '#shipping-method', 'onload');" />
				<?php echo $text_agree; ?>
			</div>
			<?php } ?>


<script type="text/javascript"><!--
    $(document).ready(function () {
       $('body').on('change', 'input[name="shipping_method"]', function () {
           $('.gal').html('');
           $(this).parent().find('.gal').html('<i class="fa fa-check" aria-hidden="true"></i>');

        var data_delivery_x =   $(this).parent().find('.gal').attr('data-delivery_x');

        if (data_delivery_x == 4){
            $('.nov-pochta').fadeIn('slow');
        }else{
            $('.nov-pochta').fadeOut('slow');
        }
       });


        $('#formLastname').keyup(function () {
            var text = $('#formLastname').val();
            console.log(text);
            $('input[name="lastname"]').val(text);

        });

    });




//    if($('input[name="shipping_method"]:checked')){
//        //console.log(refresh);
//        $(this).parent().find('.gal').html('<i class="fa fa-check" aria-hidden="true"></i>');
//    }

// multi-function for save shipping data start
function save_shipping(block, refresh, type) {


	masked(refresh, true);
	if (type == "onload") {
		$.ajax({
			url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/shipping_save',
			type: 'post',
			data: $(block + ' input[type=\'radio\']:checked, ' + block + ' input[type=\'hidden\'], ' + block + ' input[type=\'checkbox\']:checked, ' + block + ' select'),
			dataType: 'json',
			success: function(json) {

				$(block + ' .smopc-alert, ' + block + ' .smopc-text-danger').remove();

				if (json['error']) {
					masked(refresh, false);
					if (json['error']['warning']) {
						$(block + ' .smopc-panel-body').prepend('<div class="smopc-alert alert smopc-alert-warning">' + json['error']['warning'] + '<button type="button" class="smopc-close" data-dismiss="alert">&times;</button></div>');
					}
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				} else {
					$.ajax({
						url: 'index.php?route=checkout/ocdev_smart_one_page_checkout',
						dataType: 'html',
						success: function(data) {

							$(block).html($(data).find(block + ' > *'));
							<?php if (isset($checkout_blocks) && in_array('cart', $checkout_blocks)) { ?>$('#checkout-cart').html($(data).find('#checkout-cart > *'));<?php } ?>
              <?php if (isset($checkout_blocks) && in_array('payment', $checkout_blocks)) { ?>$('#payment-method').html($(data).find('#payment-method > *'));<?php } ?>
							masked(refresh, false);
						}
					});
				}
			}
		});
	} else {
		$.ajax({
			url: 'index.php?route=checkout/ocdev_smart_one_page_checkout/shipping_save',
			type: 'post',
			data: $(block + ' input[type=\'radio\']:checked, ' + block + ' input[type=\'checkbox\']:checked, ' + block + ' select'),
			dataType: 'json',
			beforeSend: function() {
				$('input[data-button-type=\'checkout\']').button('loading');
			},
			complete: function() {
				$('input[data-button-type=\'checkout\']').button('reset');
			},
			success: function(json) {

				$(block + ' .smopc-alert, ' + block + ' .smopc-text-danger').remove();

				$('input[data-button-type=\'checkout\']').button('reset');

				if (json['error']) {
					masked(refresh, false);
					$('input[data-button-type=\'checkout\']').button('reset');

					if (json['error']['warning']) {
						$(block + ' .smopc-panel-body').prepend('<div class="smopc-alert alert smopc-alert-warning">' + json['error']['warning'] + '<button type="button" class="smopc-close" data-dismiss="alert">&times;</button></div>');
					}
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				} else {
					$.ajax({
						url: 'index.php?route=checkout/ocdev_smart_one_page_checkout',
						dataType: 'html',
						success: function(data) {
							$(block).html($(data).find(block + ' > *'));
							masked(refresh, false);
						}
					});
				}
			}
		});
	}

}
// multi-function for save shipping data end
// update shipping data start
function update_shipping() {
	var country_id = $('#guest select[name=\'country_id\']').val(),
			zone_id = $('#guest select[name=\'zone_id\']').val();

	masked('#shipping-method', true);
	$.ajax({
		url: 'index.php?route=checkout/ocdev_smart_one_page_checkout&country_id=' + ((typeof(country_id) != 'undefined' || country_id != "") ? country_id : 0) + '&zone_id=' + ((typeof(zone_id) != 'undefined' || zone_id != "") ? zone_id : 0),
		dataType: 'html',
		success: function(data) {
			masked('#shipping-method', false);
			$('#shipping-method').html($(data).find('#shipping-method > *'));
		}
	});
}
function update_guest_shipping() {
	var country_id = $('#guest-shipping select[name=\'country_id\']').val(),
			zone_id = $('#guest-shipping select[name=\'zone_id\']').val();

	masked('#shipping-method', true);
	$.ajax({
		url: 'index.php?route=checkout/ocdev_smart_one_page_checkout&country_id=' + ((typeof(country_id) != 'undefined' || country_id != "") ? country_id : 0) + '&zone_id=' + ((typeof(zone_id) != 'undefined' || zone_id != "") ? zone_id : 0),
		dataType: 'html',
		success: function(data) {
			masked('#shipping-method', false);
			$('#shipping-method').html($(data).find('#shipping-method > *'));
		}
	});
}
// update shipping data end
//--></script>
