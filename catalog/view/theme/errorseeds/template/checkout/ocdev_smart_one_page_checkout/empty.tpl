<?php echo $header; ?>
<div class="wrapper bg-black-page" style="display: block">
    <div class="container">
        <div class="sort-wr">
            <div class="sort row flex-wrap justify-content-between">
                <div class="back-cat col-12 col-md-5 align-self-center">
                    <!--                        <a href=""><i class="fa fa-arrow-left" aria-hidden="true"></i> Регистрация</a>-->
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><i class="fa fa-arrow-left"
                                                                        aria-hidden="true"></i><?php echo $breadcrumb['text']; ?>
                        </a>
                        <?php break;
                    } ?>
                </div>
            </div>
        </div>
        <div class="smopc-row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'smopc-col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'smopc-col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'smopc-col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                <p><?php echo $text_empty; ?></p>
                <div class="smopc-buttons">
                    <div class="smopc-pull-right"><a href="<?php echo $continue; ?>"
                                                     class="smopc-btn smopc-btn-primary"><?php echo $button_continue; ?></a>
                    </div>
                </div>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $footer; ?>
