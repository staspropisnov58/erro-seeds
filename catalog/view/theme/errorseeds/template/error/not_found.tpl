<?php echo $header; ?>
<div id="wrapper">
  <div id="main">
    <div class="container">
      <div class="row">
          <div class="col-12 error-pages clearfix">
            <div class="error-form-left">
              <?=$text_error;?>
            </div>
            <div class="error-form-center img-holder">
              <img src="/catalog/view/theme/errorseeds/images/404.png" alt="404">
            </div>
            <div class="error-form-right error-form-text">
              <?php if (isset($text_page_not_found)) { ?>
                <?=$text_page_not_found;?>
              <?php } ?>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<?php echo $footer; ?>
