<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
</head>
<body>
  <div style=" font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #000000; width: 100%; background-image: url(https://lh3.googleusercontent.com/UsqbrlOa_jnS3Pzk-aXlKpgMOenwBu_p367U_xhmQnPfjSox8RdiG9BXZh-3XMQVwNh3q3nKVUNlrW5m_i3Goa7gLnC4sasubIKwjsHo8ueD7dCpPxwhBLPuDQThf08DH16FpPg-uaobIFis-cAf9tkc_EBm1v4M67QyMFSn7agv4gIgW2dPaaMaSBM-BTyNRk7nuEMebMCdhhsfmmv2zonrh4wIGZ-jfwa0P9yJQlS4mVKG-qQ8OqdIVstSXXgHigA76pmgEJvH3qy2QncOVg3czobPtzcrePCtFG3iTD-IXnDt4VaGr4fMI_MGwYDNFKlgE6ixZKQWYf9bqHTRkSSuj90R_u8Bk1j6IjUkGMrU3RHNzXvieD2gOqL3r1gnVrg1_fMRlLw8xQaNIRQMYF4wOACMSsZXCrjO8lsEnlSxzsgwEDlRsNnlqIpA9v48FIFgnon_cw4Vqm-AtZDGcG3fqnkK_EctpF5wvMUbaPIzSEEqkYB9zRKYqyCPuRsokExTeXiIoyRs3mWEShXZMtJBNUm_D4pxXkqICBBcSbanD6jgf29AaQcHVxbGH77kj0Iq5rH0Z-pAd9V6wUMTzl2bpacRa2CzX2kESKLD5YU7mUdn4wTqFEKV=w1298-h950-no);">
    <center>
      <table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px;">
        <tbody>
          <tr>
            <td>
              <table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px;">
                <tbody>
                  <tr>
                    <td>
                      <a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>">
                        <img src="https://bit.ly/2LKIV9c" alt="" style="border: none; width: 100%; max-width: 600px">
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
              <table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px;">
                <tbody>
                  <tr>
                    <td>
                      <div style="font-size: 25px; padding: 15px 10px; background-color: #ffffff; margin-top: 20px;">
                        <p style="margin: 0; font-weight: 600"><?php echo $text_greeting; ?></p>
                      </div>
                      <div>
                        <table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px; background-color: #ffffff; margin-top: 10px; border-top: 2px solid #bfbfbf; font-size: 15px; width: 100%; ">
                          <thead style="background-color: #e4e4e4; font-size: 18px;">
                            <tr>
                              <td style="padding: 15px 5px;"><?php echo $text_product; ?></td>
                              <td style="padding: 15px 5px;"><?php echo $text_model; ?></td>
                              <td style="padding: 15px 5px;"><?php echo $text_quantity; ?></td>
                              <td style="padding: 15px 5px;"><?php echo $text_price; ?></td>
                              <td style="padding: 15px 5px;"><?php echo $text_total; ?></td>
                            </tr>
                          </thead>
                          <tbody style="text-align: center;">
                            <?php foreach ($products as $product) { ?>
                            <tr>
                              <td style="padding: 5px"><?php echo $product['name']; ?>

                                <?php foreach ($product['option'] as $option) { ?>
                                <br />
                                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                <?php } ?></td>
                              <td style="padding: 10px 5px"><?php echo $product['model']; ?></td>
                              <td style="padding: 10px 0"><?php echo $product['quantity']; ?></td>
                              <td style="padding: 10px 0"><?php echo $product['price']; ?></td>
                              <td style="padding: 10px 5px"><?php echo $product['total']; ?></td>
                            </tr>
                            <?php } ?>
                            <?php foreach ($vouchers as $voucher) { ?>
                            <tr>
                              <td style="padding: 10px 0"><?php echo $voucher['description']; ?></td>
                              <td style="padding: 10px 0"></td>
                              <td style="padding: 10px 0">1</td>
                              <td style="padding: 10px 0"><?php echo $voucher['amount']; ?></td>
                              <td style="padding: 10px 0"><?php echo $voucher['amount']; ?></td>
                            </tr>
                            <?php } ?>
                          </tbody>
                          <tfoot>
                            <?php foreach ($totals as $total) { ?>
                            <tr>
                              <td style="padding: 5px; text-align: right;" colspan="4"><b><?php echo $total['title']; ?></b></td>
                              <td style="padding: 5px"><?php echo $total['text']; ?></td>
                            </tr>
                            <?php } ?>
                          </tfoot>
                        </table>
                      </div>
                      <div style="padding: 10px; background-color: #ffffff; margin-top: 10px;     line-height: 23px;">
                        <p style="font-size: 22px; margin-top: 0; border-bottom: 2px solid #b9b9b9;"><?php echo $text_order_detail; ?></p>
                        <span style="font-weight: 600;"><?php echo $text_order_id; ?></span> <?php echo $order_id; ?><br />
                        <span style="font-weight: 600;"><?php echo $text_date_added; ?></span> <?php echo $date_added; ?> <br />
                        <span style="font-weight: 600;"><?php echo $text_payment_method; ?></span> <?php echo $payment_method; ?><br />
                        <?php if ($shipping_method) { ?>
                          <span style="font-weight: 600;"><?php echo $text_shipping_method; ?></span> <span><?php echo $shipping_method; ?></span><br>
                        <?php } ?>
                         <span style="font-weight: 600;"><?php echo $text_email; ?></span> <?php echo $email; ?><br />
                        <span style="font-weight: 600;"><?php echo $text_telephone; ?></span> <?php echo $telephone; ?><br />

                      </div>
                      <?php if ($comment) { ?>
                        <div style="padding: 10px; background-color: #ffffff; margin-top: 10px">
                          <p style="font-size: 22px; margin-top: 0; border-bottom: 2px solid #b9b9b9;"><?php echo $text_instruction; ?></p>
                          <p><?php echo $comment; ?></p>
                        </div>
                      <?php } ?>
                      <div style="padding: 10px; background-color: #ffffff; margin-top: 10px">
                        <p style="font-size: 22px; margin-top: 0; border-bottom: 2px solid #b9b9b9;"><?php echo $text_payment_address; ?></p>
                        <p><?php echo $payment_address; ?></p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div style="padding: 10px; background-color: #ffffff; margin-top: 10px">
                <p style=""><?php echo $text_footer; ?></p>
              </div>
              <table border="0" cellpadding="0" cellspacing="0" style="max-width: 600px; margin-bottom: 50px; text-align: center; width: 100%">
                <tbody>
                  <tr>
                    <td>
                      <p>© Errors seeds сувениры из Европы 2009-2018</p>
                      <div>
                        <a href="https://www.facebook.com/esuacom/" style="text-decoration:none;display:inline-block;width:50px" target="_blank">
                          <img src="https://bit.ly/2wADhAX" alt="facebook" style="padding:5px; width:30px">
                        </a>
                        <a href="https://www.instagram.com/errorsseeds_ua/" style="text-decoration:none;display:inline-block;width:50px" target="_blank">
                          <img src="https://bit.ly/2MHJeXB" alt="instagram" style="padding:5px;  width:30px">
                        </a>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </center>
  </div>
</body>
</html>
