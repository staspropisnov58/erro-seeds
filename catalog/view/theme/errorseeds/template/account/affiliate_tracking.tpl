<?php echo $header; ?>
<div class="wrapper bg-black-page" style="display: block">
  <div class="container">
    <div class="sort-wr">
        <div class="sort row flex-wrap justify-content-between">
            <div class="back-cat col-12 col-md-5 align-self-center">
                <?php $i = 0;
                foreach ($breadcrumbs as $breadcrumb) {
                    $i++; ?>
                    <?php if ($i == 1) {
                        $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                    } else {
                        $class = '';
                    } ?>
                    <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                        <span> /</span></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
      <div id="content" class="col-12 affiliate_block">
        <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
        <div class="col-12 col-md-9">
          <?php if(!empty($affiliate_id)){ ?>
            <div class="col-sm-12 affiliate_id">
              <?php echo $column_right; ?>
            </div>
          <h1><?php echo $heading_title; ?></h1>
          <p><?php echo $text_description; ?></p>
          <form class="wr-reg-page col-12 col-sm-6 px-3 mx-auto">
            <div class="form-group">
              <label class="color-label" for="input-code"><?php echo $entry_code; ?></label>
                <textarea readonly cols="40" rows="1" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control trans-input form-control-success"><?php echo $code; ?></textarea>

            </div>
            <div class="form-group">
              <label class="color-label" for="input-code"><?php echo $text_affiliate_comission; ?></label>
                <textarea readonly cols="40" rows="1" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control trans-input form-control-success"><?php echo $commission; ?></textarea>
            </div>
            <div class="form-group">
              <label class="color-label" for="input-generator"><span data-toggle="tooltip" title="<?php echo $help_generator; ?>"><?php echo $entry_generator; ?></span></label>
              <input type="text" name="product" value="" placeholder="<?php echo $help_generator; ?>" id="input-generator" class="form-control trans-input form-control-success" />
            </div>
            <div class="form-group">
              <label class="color-label" for="input-link"><?php echo $entry_link; ?></label>
                <textarea name="link" cols="40" rows="5" placeholder="<?php echo $entry_link; ?>" id="input-link" class="form-control trans-input form-control-success"></textarea>
            </div>
          </form>
          <div class="buttons row justify-content-center">
            <div class="col-2"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
          </div>
        <?php }else{ ?>
          <div class="col-sm-12 affiliate_id2">
            <?php echo $column_right; ?>
          </div>
            <form method="post" enctype="multipart/form-data" class="wr-reg-page col-12 col-sm-10 px-3 mx-auto">
            <div class="form-group" style="text-align:center;">
              <label for="affiliate" class="color-label">
                <input type="checkbox" name="affiliate" id="affiliate"> <?php echo $text_new_my_affiliate; ?>
              </label>
            </div>
              <button type="submit" formmethod="post" class="form-btn btn-reg" style="cursor: pointer"><?php echo $text_new_affiliate_button;?></button>
            </form>
          <?php } ?>

        </div>
        <?php echo $content_bottom; ?>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=account/affiliate_tracking/autocomplete&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['link']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'product\']').val(item['label']);
		$('textarea[name=\'link\']').val(item['value']);
	}
});
//--></script>
<?php echo $footer; ?>
