<?php echo $header; ?>

    <div class="wrapper bg-black-page">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">
                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
            <?php } ?>
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
            <?php } ?>
            <div class="reg-page row">
                <div class="wr-reg-page col-4 px-3 mx-auto">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="a-reg"><?php echo $heading_title; ?></div>
                        <div class="a-reg color-theme">
                            <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a><span class="ic fa fa-user-o"></span>
                        </div>
                    </div>
                    <form class="form-my" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

                        <div class="form-group">

                            <?php if ($error_warning) { ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_warning; ?></p>
                            <?php }?>

                            <label for="formEmail" class="color-label"><?php echo $entry_email; ?></label>
                            <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo  $entry_email; ?>" id="formEmail" class="form-control trans-input form-control-success" />
                        </div>

                        <div class="form-group">
                            <label for="inputPassword" class="color-label"><?php echo $entry_password; ?></label>
                            <input type="password" id="inputPassword" name="password" class="form-control trans-input" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>">
                        </div>
                        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                        <button class="form-btn btn-reg">Войти</button>
                    </form>
                    <div class="block-soc-vhod" style="display: none">
                        <div class="block-soc-vhod-title">Войти с помощью</div>
                        <div class="block-soc-vhod-wr d-flex justify-content-center">
                            <a href="" class="fa fa-vk"></a>
                            <a href="" class="fa fa-facebook"></a>
                            <a href="" class="fa fa-twitter"></a>
                            <a href="" class="fa fa-google-plus"></a>
                            <a href="" class="fa fa-instagram"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $('input').on('change',function () {
        if($(this).val()){

            $(this).parent().addClass('has-success')
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');

        }
    });
</script>
<?php echo $footer; ?>
