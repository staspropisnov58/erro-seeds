<?php echo $header; ?>
    <div class="wrapper bg-black-page" style="display: block">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">

                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                </div>
            <?php } ?>
            <div class="row">
              <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
                <div id="content" class="col-12 col-md-9"><?php echo $content_top; ?>
                    <h1 style="text-align: center;color: #85b325"><?php echo $heading_title; ?></h1>
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                          class="wr-reg-page col-4 px-3 mx-auto">
                        <fieldset>

                            <div class="form-group"> <!--has-success-->
                                <?php if ($error_firstname){ ?>
                                    <p style="color: #ff3333;font-size: 18px;"><?php echo $error_firstname; ?></p>
                                <?php }else{ ?>
                                    <label for="formEmail" class="color-label"><?php echo $entry_firstname; ?></label>
                                <?php } ?>
                                <input type="text" name="firstname" class="form-control trans-input form-control-success" value="<?php echo $firstname; ?>" id="formEmail" placeholder="<?php echo $entry_firstname; ?>" >
                            </div>


                            <div class="form-group"> <!--has-success-->
                                <?php if ($error_lastname){ ?>
                                    <p style="color: #ff3333;font-size: 18px;"><?php echo $error_lastname; ?></p>
                                <?php }else{ ?>
                                    <label for="formEmail" class="color-label"><?php echo $entry_lastname; ?></label>
                                <?php } ?>
                                <input type="text" name="lastname" class="form-control trans-input form-control-success" value="<?php echo $lastname; ?>" id="formEmail" placeholder="<?php echo $entry_lastname; ?>" >
                            </div>


                            <div class="form-group"> <!--has-success-->
                                <?php if ($error_email){ ?>
                                    <p style="color: #ff3333;font-size: 18px;"><?php echo $error_email; ?></p>
                                <?php } ?>
                                    <label for="formEmail" class="color-label"><?php echo $entry_email; ?></label>

                                <input type="text" name="email" class="form-control trans-input form-control-success" value="<?php echo $email; ?>" id="formEmail" placeholder="<?php echo $entry_email; ?>" >
                            </div>





                            <div class="form-group">
                                <?php if ($error_telephone){ ?>
                                    <p style="color: #ff3333;font-size: 18px;"><?php echo $error_telephone; ?></p>
                                <?php } ?>
                                    <label for="formPhone" class="color-label"><?php echo $entry_telephone; ?></label>
                                <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="form-control trans-input" id="formPhone" placeholder="+ _ _   _ _ _   _ _ _  _ _  _ _" >
                            </div>








                            <?php foreach ($custom_fields as $custom_field) { ?>
                                <?php if ($custom_field['location'] == 'account') { ?>
                                    <?php if ($custom_field['type'] == 'select') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"
                                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                        id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                        class="form-control">
                                                    <option value=""><?php echo $text_select; ?></option>
                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                        <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                                            <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                                    selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'radio') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <div>
                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                        <div class="radio">
                                                            <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                                                <label>
                                                                    <input type="radio"
                                                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                                           value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                                           checked="checked"/>
                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                            <?php } else { ?>
                                                                <label>
                                                                    <input type="radio"
                                                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                                           value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'checkbox') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <div>
                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                        <div class="checkbox">
                                                            <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $account_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                <label>
                                                                    <input type="checkbox"
                                                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                                           value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                                           checked="checked"/>
                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                            <?php } else { ?>
                                                                <label>
                                                                    <input type="checkbox"
                                                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                                           value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'text') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"
                                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo(isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                       placeholder="<?php echo $custom_field['name']; ?>"
                                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                       class="form-control"/>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'textarea') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"
                                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <textarea
                                                        name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                        rows="5" placeholder="<?php echo $custom_field['name']; ?>"
                                                        id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                        class="form-control"><?php echo(isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'file') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <button type="button"
                                                        id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                        data-loading-text="<?php echo $text_loading; ?>"
                                                        class="btn btn-default"><i
                                                            class="fa fa-upload"></i> <?php echo $button_upload; ?>
                                                </button>
                                                <input type="hidden"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo(isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : ''); ?>"/>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'date') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"
                                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <div class="input-group date">
                                                    <input type="text"
                                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                           value="<?php echo(isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                           placeholder="<?php echo $custom_field['name']; ?>"
                                                           data-date-format="YYYY-MM-DD"
                                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                           class="form-control"/>
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'time') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"
                                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <div class="input-group time">
                                                    <input type="text"
                                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                           value="<?php echo(isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                           placeholder="<?php echo $custom_field['name']; ?>"
                                                           data-date-format="HH:mm"
                                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                           class="form-control"/>
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($custom_field['type'] == 'datetime') { ?>
                                        <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                             data-sort="<?php echo $custom_field['sort_order']; ?>">
                                            <label class="col-sm-2 control-label"
                                                   for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                            <div class="col-sm-10">
                                                <div class="input-group datetime">
                                                    <input type="text"
                                                           name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                           value="<?php echo(isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                           placeholder="<?php echo $custom_field['name']; ?>"
                                                           data-date-format="YYYY-MM-DD HH:mm"
                                                           id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                           class="form-control"/>
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </fieldset>
                        <div class="buttons clearfix">
                            <div class="pull-left" style="display: none"><a href="<?php echo $back; ?>"
                                                      class="btn btn-default"><?php echo $button_back; ?></a></div>
                            <div class="pull-centr">
                                <input type="submit" value="<?php echo $button_continue; ?>" class="form-btn btn-reg"/>
                            </div>
                        </div>
                    </form>
                    <?php echo $content_bottom; ?></div>
                <?php echo $column_right; ?></div>
        </div>
    </div>
    <script type="text/javascript"><!--
        // Sort the custom fields
        $('.form-group[data-sort]').detach().each(function () {
            if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
                $('.form-group').eq($(this).attr('data-sort')).before(this);
            }

            if ($(this).attr('data-sort') > $('.form-group').length) {
                $('.form-group:last').after(this);
            }

            if ($(this).attr('data-sort') < -$('.form-group').length) {
                $('.form-group:first').before(this);
            }
        });
        //--></script>
    <script type="text/javascript"><!--


        $('input').on('change',function () {
            if($(this).val()){

                $(this).parent().addClass('has-success');
                $(this).closest('.form-group')
                    .find('p')
                    .fadeOut('slow');
                $(this).closest('.form-group')
                    .find('label')
                    .fadeIn('slow');
            }
        });

        $('button[id^=\'button-custom-field\']').on('click', function () {
            var node = this;

            $('#form-upload').remove();

            $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

            $('#form-upload input[name=\'file\']').trigger('click');

            timer = setInterval(function () {
                if ($('#form-upload input[name=\'file\']').val() != '') {
                    clearInterval(timer);

                    $.ajax({
                        url: 'index.php?route=tool/upload',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData($('#form-upload')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            $(node).button('loading');
                        },
                        complete: function () {
                            $(node).button('reset');
                        },
                        success: function (json) {
                            $(node).parent().find('.text-danger').remove();

                            if (json['error']) {
                                $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                            }

                            if (json['success']) {
                                alert(json['success']);

                                $(node).parent().find('input').attr('value', json['code']);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });
        //--></script>
    <script type="text/javascript"><!--
        $('.date').datetimepicker({
            pickTime: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });

        $('.time').datetimepicker({
            pickDate: false
        });
        //--></script>
<?php echo $footer; ?>
