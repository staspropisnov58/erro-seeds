<?php echo $header; ?>
<style>
   .list-unstyled > li > a {
        color: white;
        border-bottom: 1px dashed white;
    }
   .list-unstyled > li > a:hover {
        text-decoration: none;
        color: #82b523;
        border-bottom: 1px dashed #82b523;
    }
   .list-unstyled > li{
       margin-bottom: 10px;
   }
</style>
    <div class="wrapper bg-black-page" style="display: block">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">
                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                              <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
            <?php } ?>
            <div class="row">
              <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
                <div id="content" style="color: #85b325" class="col-12 col-md-9"><?php echo $content_top; ?>
                    <h2><?php echo $text_my_account; ?></h2>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
                        <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
                        <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
                        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                    </ul>
                    <h2><?php echo $text_my_orders; ?></h2>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <!--        <li><a href="--><?php //echo $download; ?><!--">-->
                        <?php //echo $text_download; ?><!--</a></li>-->
                        <!--        --><?php //if ($reward) { ?>
                        <!--        <li><a href="--><?php //echo $reward; ?><!--">-->
                        <?php //echo $text_reward; ?><!--</a></li>-->
                        <!--        --><?php //} ?>
                        <!--        <li><a href="--><?php //echo $return; ?><!--">-->
                        <?php //echo $text_return; ?><!--</a></li>-->
                        <!--        <li><a href="--><?php //echo $transaction; ?><!--">-->
                        <?php //echo $text_transaction; ?><!--</a></li>-->
                        <!-- <li><a href="<?//php echo $recurring; ?>"><?//php echo $text_recurring; ?></a></li> -->
                    </ul>
                    <h2><?php echo $text_my_newsletter; ?></h2>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                    </ul>
                    <?php if ($tuning == 1){ ?>
                    <h2><?php echo $text_affiliate; ?></h2>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $information; ?>"><?php echo $text_affiliate_information; ?></a></li>
                    </ul>
                    <?php if (!empty($affiliate_id)){ ?>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $bonuses; ?>"><?php echo $text_bonuses_only_affiliate; ?></a></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $payouts; ?>"><?php echo $text_payouts_affiliate; ?></a></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $unclosed_orders; ?>"><?php echo $text_unclosed_orders; ?></a></li>
                    </ul>

                    <?php }else{ ?>
                    <!--  <form method="post" enctype="multipart/form-data" class="form-horizontal">
                      <div class="form-group">
                        <label for="affiliate" class="color-label"><input type="checkbox" name="affiliate" id="affiliate"> <?php echo $text_new_my_affiliate; ?> </label>
                      </div>
                        <button type="submit" formmethod="post" class="form-btn btn-reg" style="cursor: pointer"><?php echo $text_new_affiliate_button;?></button>
                      </form> -->
                      <?php } ?>
                    <?php } ?>
                    <?php echo $content_bottom; ?></div>
                <?php echo $column_right; ?></div>
        </div>
    </div>
<?php echo $footer; ?>
