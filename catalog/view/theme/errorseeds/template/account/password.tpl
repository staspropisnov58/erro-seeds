<?php echo $header; ?>
<div class="wrapper bg-black-page" style="display: block">
    <div class="container">
      <div class="sort-wr">
          <div class="sort row flex-wrap justify-content-between">
              <div class="back-cat col-12 col-md-5 align-self-center">
                  <?php $i = 0;
                  foreach ($breadcrumbs as $breadcrumb) {
                      $i++; ?>
                      <?php if ($i == 1) {
                          $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                      } else {
                          $class = '';
                      } ?>
                        <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                          <span> /</span></a>
                  <?php } ?>
              </div>
          </div>
      </div>
        <div class="row">
          <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
            <div id="content" style="color: #85b325" class="col-12 col-md-9"><?php echo $content_top; ?>
                <h1 style="text-align: center"><?php echo $heading_title; ?></h1>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      class="wr-reg-page col-4 px-3 mx-auto">
                    <fieldset>

                        <div class="form-group">
                            <?php if ($error_password){ ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_password; ?></p>
                            <?php } ?>
                                <label for="inputPassword" class="color-label"><?php echo $entry_password; ?></label>
                            <input type="password" name="password" id="input-password" class="form-control trans-input" >
                        </div>

                        <div class="form-group">
                        <?php if ($error_confirm){ ?>
                            <p style="color: #ff3333;font-size: 18px;"><?php echo $error_confirm; ?></p>
                        <?php } ?>
                            <label for="inputPassword2" class="color-label"><?php echo $entry_confirm; ?></label>
                            <input type="password" name="confirm" id="input-confirm" class="form-control trans-input" >
                        </div>


                    </fieldset>
                    <div class="buttons clearfix">

                        <div class="pull-centr">
                            <input type="submit" value="<?php echo $button_continue; ?>" class="form-btn btn-reg"/>
                        </div>
                    </div>
                </form>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
</div>
<script>
    $('input').on('change', function () {
        if ($(this).val()) {

            $(this).parent().addClass('has-success');
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');
            $(this).closest('.form-group')
                .find('label')
                .fadeIn('slow');
        }
    });
</script>
<?php echo $footer; ?>
