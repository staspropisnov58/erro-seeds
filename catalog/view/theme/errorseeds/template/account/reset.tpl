<?php echo $header; ?>
<style>
  legend{
    color:#fff;
    font-size: 18px;
  }
  #content{}
  h1{text-align: center; color:#85b325}
</style>
    <div class="wrapper bg-black-page" style="display: block">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">
                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>

                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="content" class="col-md-10 mx-auto col-12 col-lg-6"><?php echo $content_top; ?>
                  <h1><?php echo $heading_title; ?></h1>
                  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="wr-reg-page col-md-8 px-3 mx-auto col-12">
                    <fieldset>
                      <legend><?php echo $text_password; ?></legend>
                      <div class="form-group">
                        <label class="color-label" for="input-password"><?php echo $entry_password; ?></label>
                        <div class="">
                          <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control trans-input form-control-success" />
                          <?php if ($error_password) { ?>
                          <div class="text-danger"><?php echo $error_password; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="color-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                        <div class="">
                          <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="form-control trans-input form-control-success" />
                          <?php if ($error_confirm) { ?>
                          <div class="text-danger"><?php echo $error_confirm; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                    </fieldset>
                    <div class="buttons ">
                      <button type="submit" class="form-btn btn-reg"><?php echo $button_continue; ?></button>
                    </div>
                  </form>

                    <?php echo $content_bottom; ?></div>
                <?php echo $column_right; ?></div>
        </div>
    </div>
<?php echo $footer; ?>
