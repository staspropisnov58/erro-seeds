<?php echo $header; ?>
    <div class="container" style="display: none">
      <div class="sort-wr">
          <div class="sort row flex-wrap justify-content-between">
              <div class="back-cat col-12 col-md-5 align-self-center">

                  <?php $i = 0;
                  foreach ($breadcrumbs as $breadcrumb) {
                      $i++; ?>
                      <?php if ($i == 1) {
                          $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                      } else {
                          $class = '';
                      } ?>
                      <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                          <span> /</span></a>
                  <?php } ?>
              </div>
          </div>
      </div>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row">
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <p><?php echo $text_email; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>
          <legend><?php echo $text_your_email; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="email" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
          <div class="pull-right">
            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
          </div>
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>



    <div class="wrapper bg-black-page">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">
                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="reg-page row">
                <div class="wr-reg-page col-4 px-3 mx-auto">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="a-reg"><?php echo $heading_title; ?></div>
                        <div class="a-reg color-theme">
                            <a href="<?php echo $back; ?>"><?php echo $button_back; ?></a><span class="ic fa fa-user-o"></span>
                        </div>
                    </div>
                    <form class="form-my" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">


                            <?php if ($error_warning) { ?>

                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_warning; ?></p>
                            <?php } ?>


                            <label for="formEmail" class="color-label"><?php echo $entry_email; ?></label>
                            <input type="text" name="email" class="form-control trans-input form-control-success" id="formEmail" placeholder="<?php echo $entry_email; ?>">
                        </div>

                        <button class="form-btn btn-reg"><?php echo $button_continue; ?></button>
                    </form>
                    <div class="block-soc-vhod" style="display: none">
                        <div class="block-soc-vhod-title">Войти с помощью</div>
                        <div class="block-soc-vhod-wr d-flex justify-content-center">
                            <a href="" class="fa fa-vk"></a>
                            <a href="" class="fa fa-facebook"></a>
                            <a href="" class="fa fa-twitter"></a>
                            <a href="" class="fa fa-google-plus"></a>
                            <a href="" class="fa fa-instagram"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('input').on('change',function () {
            if($(this).val()){

                $(this).parent().addClass('has-success')
                $(this).closest('.form-group')
                    .find('p')
                    .fadeOut('slow');

            }
        });
    </script>
<?php echo $footer; ?>
