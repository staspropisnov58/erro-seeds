<?php echo $header; ?>
<div class="wrapper bg-black-page" style="display: block">
  <!--asd-->
  <div class="container">
    <div class="sort-wr">
      <div class="sort row flex-wrap justify-content-between">
        <div class="back-cat col-12 col-md-5 align-self-center">

          <?php $i = 0;
          foreach ($breadcrumbs as $breadcrumb) {
              $i++; ?>
              <?php if ($i == 1) {
                  $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
              } else {
                  $class = '';
              } ?>
              <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                  <span> /</span></a>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
      <div id="content" class="col-12 col-md-9">
        <?php echo $content_top; ?>
        <h1 style="text-align: center;color: #85b325"><?php echo $heading_title; ?></h1>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="wr-reg-page col-xs-12 col-md-10 px-3 mx-auto newsletter_block">
          <fieldset style="margin: 20px 0;">
            <div class="row">
              <div class="col-12 clearfix">
                <div class="row">
                  <p class="col-10 flleft"><label class="newsletter_label"><?php echo $entry_newsletter; ?></label></p>
                  <div class="col-2 flleft">
                    <div class="switch_block">
                      <?php if ($newsletter == 1) { ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter1" value="1" checked>
                      <label for="newsletter1" class="switch-label switch-label-off"></label>
                    <?php }else{ ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter1" value="1">
                      <label for="newsletter1" class="switch-label switch-label-off"></label>
                    <?php } ?>
                      <?php if ($newsletter == 0) { ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter2" value="0" checked>
                      <label for="newsletter2" class="switch-label switch-label-on"></label>
                    <?php }else{ ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter2" value="0">
                      <label for="newsletter2" class="switch-label switch-label-on"></label>
                    <?php } ?>
                      <span class="switch-selection"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <div class="thead col-12 clearfix">
                    <div class="row">
                      <p class="text-center col-12">
                        <?php echo $text_notification_settings; ?>
                      </p>
                      <p class="col-8 flleft">
                        <?php echo $text_notification_title; ?>
                      </p>
                      <p class="col-2 flleft">
                        <?php echo $text_notification_email; ?>
                      </p>
                      <?php  if (isset($sms_customer_notification)){ ?>
                      <p class="col-2 flleft">
                        <?php echo $text_notification_sms; ?>
                      </p>
                    <?php } ?>
                    </div>
                  </div>
                  <?php foreach ($notifications as $notification){ ?>
                  <div class="tbody col-12 clearfix">
                    <div class="row">
                      <div class="text-left col-8 flleft"><label class="newsletter_label"><?php echo $notification['entry']; ?></label></div>
                      <div class="text-left col-2 flleft">
                        <div class="form-group" style="text-align: center;">
                          <div class="switch_block">
                            <?php if ($notification['email_value'] == 1){ ?>

                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>1" value="1" checked>
                            <label for="email_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>1" value="1">
                            <label for="email_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php } ?>
                            <?php if ($notification['email_value'] == 0){ ?>
                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>0" value="0" checked>
                            <label for="email_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>0" value="0">
                            <label for="email_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php } ?>
                            <span class="switch-selection"></span>
                          </div>
                        </div>
                      </div>
                    <?php  if (isset($sms_customer_notification)){ ?>
                      <div class="text-right col-2 flleft">
                        <div class="form-group" style="text-align: center;">
                          <div class="switch_block">
                            <?php if ($notification['sms_value'] == 1){ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>1" value="1" checked>
                            <label for="sms_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>1" value="1">
                            <label for="sms_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php } ?>
                            <?php if ($notification['sms_value'] == 0){ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>0" value="0" checked>
                            <label for="sms_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>0" value="0">
                            <label for="sms_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php } ?>
                            <span class="switch-selection"></span>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </fieldset>
          <div class="col-12">
            <div class="row">
              <label for="default"><?php echo $text_for_default; ?>
                <input type="checkbox" id="default" name="default"/>
              <label>
            </div>
          </div>
          <div class="buttons clearfix">
            <div class="pull-left" style="display: none">
              <a href="<?php echo $back; ?>" class="btn btn-danger"><?php echo $button_back; ?></a>
            </div>
            <div class="pull-centr">
                <input type="submit" value="<?php echo $button_continue; ?>" class="form-btn btn-reg"/>
            </div>
          </div>
        </form>
        <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
      </div>
    </div>
<?php echo $footer; ?>
