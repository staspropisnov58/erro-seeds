<?php echo $header; ?>
<div class="wrapper bg-black-page" style="display: block">
    <div class="container">
        <div class="sort-wr">
            <div class="sort row flex-wrap justify-content-between">
                <div class="back-cat col-12 col-md-5 align-self-center">
                    <?php $i = 0;
                    foreach ($breadcrumbs as $breadcrumb) {
                        $i++; ?>
                        <?php if ($i == 1) {
                            $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                        } else {
                            $class = '';
                        } ?>
                        <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                            <span> /</span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
            <div id="content"  class="col-12 col-md-9"> <?php echo $content_top; ?>
                <h2 style="color: #85b325; text-align: center"><?php echo $text_edit_address; ?></h2>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                      class="wr-reg-page col-4 px-3 mx-auto">
                    <fieldset>

                        <div class="form-group"> <!--has-success-->
                            <?php if ($error_firstname){ ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_firstname; ?></p>
                            <?php } ?>
                                <label for="formEmail" class="color-label"><?php echo $entry_firstname; ?></label>

                            <input type="text" name="firstname" class="form-control trans-input form-control-success" value="<?php echo $firstname; ?>" id="formEmail" placeholder="<?php echo $entry_firstname; ?>" >
                        </div>

                        <div class="form-group"> <!--has-success-->
                            <?php if ($error_lastname){ ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_lastname; ?></p>
                            <?php } ?>
                                <label for="formEmail" class="color-label"><?php echo $entry_lastname; ?></label>

                            <input type="text" name="lastname" class="form-control trans-input form-control-success" value="<?php echo $lastname; ?>" id="formEmail" placeholder="<?php echo $entry_lastname; ?>" >
                        </div>


<!--                        <div class="form-group">-->
<!--                            <label class="col-sm-2 control-label"-->
<!--                                   for="input-company">--><?php //echo $entry_company; ?><!--</label>-->
<!--                            <div class="col-sm-10">-->
<!--                                <input type="text" name="company" value="--><?php //echo $company; ?><!--"-->
<!--                                       placeholder="--><?php //echo $entry_company; ?><!--" id="input-company"-->
<!--                                       class="form-control"/>-->
<!--                            </div>-->
<!--                        </div>-->


                        <div class="form-group"> <!--has-success-->
                            <?php if ($error_address_1){ ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_address_1; ?></p>
                            <?php } ?>
                                <label for="formEmail" class="color-label"><?php echo $entry_address_1; ?></label>

                            <input type="text" name="address_1" class="form-control trans-input form-control-success" value="<?php echo $address_1; ?>" id="formEmail" placeholder="<?php echo $entry_address_1; ?>" >
                        </div>


<!--                        <div class="form-group">-->
<!--                            <label class="col-sm-2 control-label"-->
<!--                                   for="input-address-2">--><?php //echo $entry_address_2; ?><!--</label>-->
<!--                            <div class="col-sm-10">-->
<!--                                <input type="text" name="address_2" value="--><?php //echo $address_2; ?><!--"-->
<!--                                       placeholder="--><?php //echo $entry_address_2; ?><!--" id="input-address-2"-->
<!--                                       class="form-control"/>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group required">-->
<!--                            <label class="col-sm-2 control-label" for="input-city">--><?php //echo $entry_city; ?><!--</label>-->
<!--                            <div class="col-sm-10">-->
<!--                                <input type="text" name="city" value="--><?php //echo $city; ?><!--"-->
<!--                                       placeholder="--><?php //echo $entry_city; ?><!--" id="input-city" class="form-control"/>-->
<!--                                --><?php //if ($error_city) { ?>
<!--                                    <div class="text-danger">--><?php //echo $error_city; ?><!--</div>-->
<!--                                --><?php //} ?>
<!--                            </div>-->
<!--                        </div>-->

                        <div class="form-group"> <!--has-success-->
                            <?php if ($error_city){ ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_city; ?></p>
                            <?php } ?>
                                <label for="formEmail" class="color-label"><?php echo $entry_city; ?></label>

                            <input type="text" name="city" class="form-control trans-input form-control-success" value="<?php echo $city; ?>" id="formEmail" placeholder="<?php echo $entry_city; ?>" >
                        </div>


<!--                        <div class="form-group required">-->
<!--                            <label class="col-sm-2 control-label"-->
<!--                                   for="input-postcode">--><?php //echo $entry_postcode; ?><!--</label>-->
<!--                            <div class="col-sm-10">-->
<!--                                <input type="text" name="postcode" value="--><?php //echo $postcode; ?><!--"-->
<!--                                       placeholder="--><?php //echo $entry_postcode; ?><!--" id="input-postcode"-->
<!--                                       class="form-control"/>-->
<!--                                --><?php //if ($error_postcode) { ?>
<!--                                    <div class="text-danger">--><?php //echo $error_postcode; ?><!--</div>-->
<!--                                --><?php //} ?>
<!--                            </div>-->
<!--                        </div>-->
                        <div class="form-group">
                            <?php if ($error_country){ ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_country; ?></p>
                            <?php } ?>
                            <label class="color-label"
                                   for="input-country"><?php echo $entry_country; ?></label>

                                <select name="country_id" id="input-country" class="form-control trans-input form-control-success">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($countries as $country) { ?>
                                        <?php if ($country['country_id'] == $country_id) { ?>
                                            <option value="<?php echo $country['country_id']; ?>"
                                                    selected="selected"><?php echo $country['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>


                        </div>
                        <div class="form-group">
                            <?php if ($error_zone){ ?>
                                <p style="color: #ff3333;font-size: 18px;"><?php echo $error_zone; ?></p>
                            <?php } ?>
                            <label class="color-label" for="input-zone"><?php echo $entry_zone; ?></label>

                                <select name="zone_id" id="input-zone" class="form-control trans-input form-control-success">
                                </select>


                        </div>
                        <?php foreach ($custom_fields as $custom_field) { ?>
                            <?php if ($custom_field['location'] == 'address') { ?>
                                <?php if ($custom_field['type'] == 'select') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                    id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                    class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                    <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address_custom_field[$custom_field['custom_field_id']]) { ?>
                                                        <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                                selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'radio') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <div>
                                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                    <div class="radio">
                                                        <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address_custom_field[$custom_field['custom_field_id']]) { ?>
                                                            <label>
                                                                <input type="radio" style="display: block"
                                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                                       value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                                       checked="checked"/>
                                                                <?php echo $custom_field_value['name']; ?></label>
                                                        <?php } else { ?>
                                                            <label>
                                                                <input type="radio" style="display: block"
                                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                                       value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                                                <?php echo $custom_field_value['name']; ?></label>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'checkbox') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <div>
                                                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                    <div class="checkbox">
                                                        <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $address_custom_field[$custom_field['custom_field_id']])) { ?>
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                                       value="<?php echo $custom_field_value['custom_field_value_id']; ?>"
                                                                       checked="checked"/>
                                                                <?php echo $custom_field_value['name']; ?></label>
                                                        <?php } else { ?>
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]"
                                                                       value="<?php echo $custom_field_value['custom_field_value_id']; ?>"/>
                                                                <?php echo $custom_field_value['name']; ?></label>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'text') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo(isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                   placeholder="<?php echo $custom_field['name']; ?>"
                                                   id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                   class="form-control"/>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'textarea') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <textarea
                                                    name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                    rows="5" placeholder="<?php echo $custom_field['name']; ?>"
                                                    id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                    class="form-control"><?php echo(isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'file') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <button type="button"
                                                    id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                    data-loading-text="<?php echo $text_loading; ?>"
                                                    class="btn btn-default"><i
                                                        class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden"
                                                   name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                   value="<?php echo(isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : ''); ?>"/>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'date') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <div class="input-group date">
                                                <input type="text"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo(isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                       placeholder="<?php echo $custom_field['name']; ?>"
                                                       data-date-format="YYYY-MM-DD"
                                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'time') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <div class="input-group time">
                                                <input type="text"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo(isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                       placeholder="<?php echo $custom_field['name']; ?>"
                                                       data-date-format="HH:mm"
                                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($custom_field['type'] == 'datetime') { ?>
                                    <div class="form-group<?php echo($custom_field['required'] ? ' required' : ''); ?> custom-field"
                                         data-sort="<?php echo $custom_field['sort_order']; ?>">
                                        <label class="col-sm-2 control-label"
                                               for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                        <div class="col-sm-10">
                                            <div class="input-group datetime">
                                                <input type="text"
                                                       name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]"
                                                       value="<?php echo(isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>"
                                                       placeholder="<?php echo $custom_field['name']; ?>"
                                                       data-date-format="YYYY-MM-DD HH:mm"
                                                       id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"
                                                       class="form-control"/>
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <div class="form-group" style="text-align: center;">
                            <label class="color-label"><?php echo $entry_default; ?></label>
                            <div class="col-sm-10">
                                <?php if ($default) { ?>
                                    <label class="color-label">
                                        <input type="radio" style="display: block" name="default" value="1" checked="checked"/>
                                        <?php echo $text_yes; ?></label>
                                    <label class="color-label">
                                        <input type="radio" style="display: block" name="default" value="0"/>
                                        <?php echo $text_no; ?></label>
                                <?php } else { ?>
                                    <label class="color-label">
                                        <input type="radio" style="display: block" name="default" value="1"/>
                                        <?php echo $text_yes; ?></label>
                                    <label class="color-label">
                                        <input type="radio" style="display: block" name="default" value="0" checked="checked"/>
                                        <?php echo $text_no; ?></label>
                                <?php } ?>
                            </div>
                        </div>
                    </fieldset>
                    <div class="buttons clearfix">
                        <div class="pull-left" style="display: none"><a href="<?php echo $back; ?>"
                                                  class="btn btn-danger"><?php echo $button_back; ?></a></div>
                        <div class="pull-center" style="text-align: center">
                            <input type="submit" value="<?php echo $button_continue; ?>" class="form-btn btn-reg"/>
                        </div>
                    </div>
                </form>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
</div>
<script type="text/javascript"><!--


    $('input').on('change',function () {
        if($(this).val()){

            $(this).parent().addClass('has-success');
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');
            $(this).closest('.form-group')
                .find('label')
                .fadeIn('slow');
        }
    });
    $('select').on('change',function () {
        if($(this).val()){

            $(this).parent().addClass('has-success');
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');
            $(this).closest('.form-group')
                .find('label')
                .fadeIn('slow');
        }
    });


    // Sort the custom fields
    $('.form-group[data-sort]').detach().each(function () {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
            $('.form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('.form-group').length) {
            $('.form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('.form-group').length) {
            $('.form-group:first').before(this);
        }
    });
    //--></script>
<script type="text/javascript"><!--
    $('button[id^=\'button-custom-field\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });
    //--></script>
<script type="text/javascript"><!--
    $('select[name=\'country_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=account/account/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                $('.fa-spin').remove();

                if (json['postcode_required'] == '1') {
                    $('input[name=\'postcode\']').parent().parent().addClass('required');
                } else {
                    $('input[name=\'postcode\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');
    //--></script>
<?php echo $footer; ?>
