<?php echo $header; ?>
<style>
    .table-hover > tbody > tr:hover{
        background-color:black;
    }
    .td_1_x{
        /*float: left;*/
        /*padding-top: 5%;*/
        /*padding-left: 15%;*/
        /*max-width: 40%;*/

    }
    .td_2_x{
        /*float: right;*/
        /*max-width: 45%;*/
        /*padding-right: 18%;*/

    }
    .table.table-bordered.table-hover {
        border-collapse: collapse;
        border-spacing: 0;
    }

    td {
        border: none;
    }
    .address > .td_2_x{
        display: none;
    }
    .address_x > .td_1_x{
        display: none;
    }
</style>
    <div class="wrapper bg-black-page" style="display: block">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">

                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
            <?php } ?>
            <?php if ($error_warning) { ?>
                <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                </div>
            <?php } ?>
            <div class="row">
              <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
                <div id="content" class="col-12 col-md-9"><?php echo $content_top; ?>
                    <h2 style="text-align: center;color: #85b325"><?php echo $text_address_book; ?></h2>
                    <?php if ($addresses) { ?>

                        <table>
                            <?php $i = 0; foreach ($addresses as $result) { $i++;
                                if ($i % 2 == 0){
                                    $color = '#141313';
                                    }else{
                                    $color = '#0f0e0f';
                                    } ?>

                                <tr style="background: <?=$color;?>">
                                    <td align="left" class="address"><?php echo $result['address']; ?></td>
                                    <td align="left" class="address_x"><?php echo $result['address_x']; ?></td>
                                    <td align="centr" style="width: 70px;padding-left: 15px;">

                                         <a href="<?php echo $result['update']; ?>"><?php  $button_edit; ?><i class="fa fa-pencil-square-o" style="color: #6b6e13"></i></a>
                                         <a href="<?php echo $result['delete']; ?>"><i class="fa fa-trash" aria-hidden="true" style="color: #e3332e;margin-left: 10px;"></i><?php  ' '.$button_delete; ?></a>
                                    </td>
                                </tr>

                            <?php } ?>
                        </table>
                      <?php } else { ?>
                        <p><?php echo $text_empty; ?></p>
                    <?php } ?>
                    <div class="buttons clearfix">
                        <div class="pull-left"><a href="<?php echo $back; ?>"
                                                  class="btn btn-default"><?php echo $button_back; ?></a></div>
                        <div class="pull-right"><a href="<?php echo $add; ?>"
                                                   class="btn btn-primary"><?php echo $button_new_address; ?></a></div>
                    </div>
                    <?php echo $content_bottom; ?></div>
                <?php echo $column_right; ?></div>
        </div>
    </div>


<?php echo $footer; ?>
