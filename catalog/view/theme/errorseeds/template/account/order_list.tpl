<?php echo $header; ?>
    <style>
        td, tr {
            border: none;
        }
        .text-left{
            padding-right: 25px;
            padding-bottom: 30px;
        }
    </style>
    <div class="wrapper bg-black-page" style="display: block">
        <div class="container">
            <div class="sort-wr">
                <div class="sort row flex-wrap justify-content-between">
                    <div class="back-cat col-12 col-md-5 align-self-center">

                        <?php $i = 0;
                        foreach ($breadcrumbs as $breadcrumb) {
                            $i++; ?>
                            <?php if ($i == 1) {
                                $class = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
                            } else {
                                $class = '';
                            } ?>
                            <a <?php if(isset($breadcrumb['href'])){ ?> href="<?php echo $breadcrumb['href'];?><?php } ?>"><?= $class; ?><?php echo $breadcrumb['text']; ?>
                                <span> /</span></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-3"><?php echo $column_left; ?></div>
                <div id="content" class="col-12 col-md-9"><?php echo $content_top; ?>
                    <h1 style="text-align: center;color: #85b325"><?php echo $heading_title; ?></h1>
                    <?php if ($orders) { ?>
                        <div class="table-responsive">
                            <table class=" ">
                                <thead>
                                <tr>
                                    <td class="text-left"><?php echo $column_order_id; ?></td>
                                    <td class="text-left"><?php echo $column_status; ?></td>
                                    <td class="text-left"><?php echo $column_date_added; ?></td>
                                    <td class="text-left"><?php echo $column_product; ?></td>
                                    <td class="text-left"><?php echo $column_customer; ?></td>
                                    <td class="text-left"><?php echo $column_total; ?></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i=0; foreach ($orders as $order) {
                                    $i++;
                                    if ($i % 2 == 0){
                                        $color = '#141313';
                                    }else{
                                        $color = '#0f0e0f';
                                    }

                                    ?>


                                    <?php if ($order['show_in_order_list'] != 0) { ?>
                                        <tr style="background: <?=$color;?>">
                                            <td class="text-left">#<?php echo $order['order_id']; ?></td>
                                            <td class="text-left"><?php echo $order['status']; ?></td>
                                            <td class="text-left"><?php echo $order['date_added']; ?></td>
                                            <td class="text-left"><?php echo $order['products']; ?></td>
                                            <td class="text-left"><?php echo $order['name']; ?></td>
                                            <td class="text-left"><?php echo $order['total']; ?></td>
                                            <td class="text-left"><a href="<?php echo $order['href']; ?>"
                                                                     data-toggle="tooltip"
                                                                     title="<?php echo $button_view; ?>"><i class="fa fa-eye" style="color: #6b6e13"></i></a>
                                            </td>
                                            <td class="text-left"><a href="#" class="delete_order" data-toggle="tooltip"
                                                                     data-order-id="<?php echo $order['order_id']; ?>"><i class="fa fa-trash" aria-hidden="true" style="color: #e3332e;margin-left: 10px;"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right"><?php echo $pagination; ?></div>
                    <?php } else { ?>
                        <p><?php echo $text_empty; ?></p>
                    <?php } ?>
                    <div class="buttons clearfix" style="display: none">
                        <div class="pull-right"><a href="<?php echo $continue; ?>"
                                                   class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                    </div>
                    <?php echo $content_bottom; ?></div>
                <?php echo $column_right; ?></div>
        </div>
    </div>

    <script>

        $('.delete_order').on('click', function (e) {
            e.preventDefault();
            var order_id = $(this).data('orderId');

            console.log(order_id)
            $.ajax({
                url: 'index.php?route=account/order/delete_order_history',
                type: 'post',
                data: {'order_id': order_id},
                success: function (json) {
                    console.log(json)
                    if (json == 1) {
                        location.reload();
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    console.log('ERRORS: ' + textStatus);
                }
            });
        });


    </script>

<?php echo $footer; ?>
