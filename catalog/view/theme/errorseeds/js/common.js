$(document).ready(function(){

    $('.menu-all').slicknav({
        // 'label' : 'Меню',
        prependTo:'.menu'
    });
  $('.produckt-slider-hits').owlCarousel({
    loop:true,
    margin:20,
    dots: false,
    nav:true,
    items:1,
    navText: ["<i class='fa fa-chevron-left size-nav'></i>","<i class='fa fa-chevron-right size-nav'></i>"]
  });
  $(".slider").owlCarousel({
  	loop:true,
    margin:0,
    responsiveClass:true,
    navText: ["<i class='fa fa-chevron-left size-nav'></i>","<i class='fa fa-chevron-right size-nav'></i>"],
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true
        }
    }
  });
  $('.produckt-slider').owlCarousel({
    loop:true,
    margin:20,
    dots: false,
    navText: ["<i class='fa fa-chevron-left size-nav'></i>","<i class='fa fa-chevron-right size-nav'></i>"],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        768:{
            items:2,
            nav:true
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
  });
  $('.produckt-slider2').owlCarousel({
    loop:true,
    margin:20,
    dots: false,
    navText: ["<i class='fa fa-chevron-left size-nav'></i>","<i class='fa fa-chevron-right size-nav'></i>"],
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        768:{
            items:2,
            nav:false
        },
        1000:{
            items:4,
            nav:true,
            loop:false
        }
    }
  });

    $('.owl-carousel2').owlCarousel({
        thumbs: true,
        items:1,
        dots: false,
        nav: false,
        loop:true,
        thumbsPrerendered: true,
        responsiveClass:true
    });

  $( "#arrow_hide" ).click(function() {
    $( ".hour_hide" ).toggle( "slow", function() {
        // Animation complete.
    });
  });
  $( "#arrow_hide2" ).click(function() {
    $( ".hour_hide2" ).toggle( "slow", function() {
        // Animation complete.
    });
  });
  // $(".toggle").click(function () {
  //   var tog = $(this).attr("data-class");
  //     $( ".cont-wrap" ).toggleClass( "selected" );
  //     $("." + tog).toggle("fast", function () {
  //     });
  // });
  $('.btn-close').click(function(){
      $( ".cont-wrap" ).removeClass( "selected" );
      $('.cont-wrap').hide('fast', function() {
      });
    });

    $('#comment-close').on('click', function(e) {
      e.preventDefault();
      clearAndHide();
      $('#exampleModal label.error').remove();
      $('#exampleModal').modal('hide');
    });

    $('#entrance').on('click', function(){
      var data = {};
      var date_expired = {};
      data.email = $('#nr-email').val();
      data.quantity = $('#nr-quantity').val();
      date_expired.year = $('#nr-year').val();
      date_expired.month = $('#nr-month').val();
      date_expired.day = $('#nr-day').val();
      data.date_expired = date_expired;
      $.ajax({
          url: 'index.php?route=module/notification_request/saveRequest&product_id=' + $('input[name=nr-product-id]').val(),
          type: 'post',
          data: data,
          success: function (json) {
              if (json.success) {
                $('#modalentrance').modal('hide');
                $('#modal-message').modal('show');
                $('#modal-message .modal-body').html('<div class="message message-success">' + '<i class="fas fa-check"></i>' + json.success + '</div><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');
              } else if (json.warning) {
                $('#modalentrance').modal('hide');
                $('#modal-message').modal('show');
                $('#modal-message .modal-body').html('<div class="message message-warning">' + '<i class="fas fa-exclamation"></i>' + json.warning + '</div><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');
              } else if (json.errors) {
                $.map(json.errors, function(text, field) {
                  var field_id = 'nr-' + field.replace('_', '-');
                  $('#' + field_id).after('<label for="' + field_id + '" class="error color-label">' + text + '</label>');
                  $('#' + field_id).on('focus', function() {
                    $('label[for=' + field_id +'].error').remove();
                  });
                });
              }
          },
          error: function (jqXHR, textStatus, errorThrown) {

              console.log('ERRORS: ' + textStatus);
          }
      });
    });

    $('#new-reviews-x').on('click', function (e) {
        e.preventDefault();
        var data = {};
        data.rating = $('input[name="rating"]:checked').val() ? $('input[name="rating"]:checked').val() : 0;
        data.name = $('input[name="name"]').val();
        data.email = $('input[name="email"]').val();
        data.comment = $('input[name="comment"]').val();
        data.product_id = $('input[name="product_id"]').val();
        data.review_id = $('input[name="review_id"]').val();
        data.g_recaptcha_response = grecaptcha.getResponse();

        var url_action = '';
        if ($('#news-add-rew').val() == 1) {
            url_action = 'index.php?route=information/news/addReviews'
        } else if($('#posts-add-rew').val() == 1) {
            url_action = 'index.php?route=information/posts/addReviews'
        }else {
            url_action = 'index.php?route=product/product/addReviews'
        }

        $.ajax({
            url: url_action,
            type: 'post',
            data: data,
            success: function (json) {
              if (json.success) {
                clearAndHide();
                $('#modal-message').modal('show');
                $('#modal-message .modal-body').html('<div class="message message-success">' + '<i class="fas fa-check"></i>' + json.success + '</div><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');
              } else if (json.errors) {
                $.map(json.errors, function(text, field) {
                  var field_id = 'rev-' + field.replace('_', '-');
                  $('#' + field_id).after('<label for="' + field_id + '" class="error color-label">' + text + '</label>');
                  $('#' + field_id).on('focus', function() {
                    $('label[for=' + field_id +'].error').remove();
                  });
                });
                grecaptcha.reset();
              }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('ERRORS: ' + textStatus);
            }
        });
      });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function () {
        url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;
    });

    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    var width_search = document.getElementById("search").offsetWidth;
    // $('.result-search-autocomplete').css({"width":width_search});
    $('.search-autocomplete').keyup(function(event) {
      /* Act on the event */
      $('.result-search-autocomplete').css({
        "overflow-x": "hidden"
      });
      var search = $('input[name=search]').val();
      var category_id = $('select[name=category_id]').val();
      $.ajax({
        method: "GET",
        url: "/index.php?route=product/json",
        data: {
          search: search,
          category_id: category_id
        }
      }).done(function(result) {
        var html = '';
        if (result && search != '') {
          var count = 0
          $.each(JSON.parse(result), function(index, value) {

            html += '<li>';
            html += '<a href="' + value.href.replace('amp;', '') + '">';
            html += value.name;
            html += '</a>';
            html += '</li>';
            count++;
          });
          $('.result-search-autocomplete').css({
            "display": "block"
          }).addClass('activate');
          if (count > 5) {
            $('.result-search-autocomplete').css({
              "overflow": "scroll"
            });
          } else {
            $('.result-search-autocomplete').css({
              "overflow": "hidden"
            });
          }
        } else {
          html = '';
          $('.result-search-autocomplete').css({
            "display": "none"
          }).removeClass('activate');
        }
        $('.result-search-autocomplete').html(html);
      });
    });
});
function removeError() {
  $('label[for=rev-captcha').remove();
}
function clearAndHide() {
  grecaptcha.reset();
  $('input[name="name"], input[name="email"], input[name="comment"]').val('');
  $('input[name="rating"]').removeAttr('checked');
  $('label.rating').removeClass('AAA');
  $('#exampleModal').modal('hide');
}
function modalEntrance(productId) {
  $('#modalentrance').modal('show');
  if ($('input[name=nr-product-id]').val()) {
    $('input[name=nr-product-id]').val(productId);
  } else {
    $('#modalentrance .modal-body').append('<input type="hidden" name="nr-product-id" value="' + productId + '">');
  }
}
// function OpenJivoChat(e) {
//     0 != e && "undefined" != e || (e = "15px"), $("#jivo-iframe-container"), $("#jivo-iframe-container").attr("data-jivo", "true");
//     try {
//         jivo_api.open()
//     } catch (e) {
//         console.log("jivochat problem" + e.message)
//     }
// }
$(document).click(function(e) {
    var target = e.target;

    if (!$(target).is('.result-search-autocomplete') && !$(target).parents().is('.result-search-autocomplete')) {
        $('.result-search-autocomplete').hide();
    }
});
