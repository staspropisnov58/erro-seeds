<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($products) { ?>
      <table class="table table-bordered">
        <thead>
          <tr>
            <td colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $text_product; ?></strong></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?php echo $text_name; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><a href="<?php echo $products[$product['product_id']]['href']; ?>"><strong><?php echo $products[$product['product_id']]['name']; ?></strong></a></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_image; ?></td>
            <?php foreach ($products as $product) { ?>
            <td class="text-center"><?php if ($products[$product['product_id']]['thumb']) { ?>
              <img src="<?php echo $products[$product['product_id']]['thumb']; ?>" alt="<?php echo $products[$product['product_id']]['name']; ?>" title="<?php echo $products[$product['product_id']]['name']; ?>" class="img-thumbnail" />
              <?php } ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_price; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php if ($products[$product['product_id']]['price']) { ?>
              <?php if (!$products[$product['product_id']]['special']) { ?>
              <?php echo $products[$product['product_id']]['price']; ?>
              <?php } else { ?>
              <span class="price-old"><?php echo $products[$product['product_id']]['price']; ?> </span> <span class="price-new"> <?php echo $products[$product['product_id']]['special']; ?> </span>
              <?php } ?>
              <?php } ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_model; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php echo $products[$product['product_id']]['model']; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_manufacturer; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php echo $products[$product['product_id']]['manufacturer']; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_availability; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php echo $products[$product['product_id']]['availability']; ?></td>
            <?php } ?>
          </tr>
          <?php if ($review_status) { ?>
          <tr>
            <td><?php echo $text_rating; ?></td>
            <?php foreach ($products as $product) { ?>
            <td class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($products[$product['product_id']]['rating'] < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>

              <br />
              <?php echo $products[$product['product_id']]['reviews']; ?></td>
            <?php } ?>
          </tr>
          <?php } ?>
          <tr>
            <td><?php echo $text_summary; ?></td>
            <?php foreach ($products as $product) { ?>
            <td class="description"><?php echo $products[$product['product_id']]['description']; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_weight; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php echo $products[$product['product_id']]['weight']; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_dimension; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php echo $products[$product['product_id']]['length']; ?> x <?php echo $products[$product['product_id']]['width']; ?> x <?php echo $products[$product['product_id']]['height']; ?></td>
            <?php } ?>
          </tr>
        </tbody>
        <?php foreach ($attribute_groups as $attribute_group) { ?>
        <thead>
          <tr>
            <td colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $attribute_group['name']; ?></strong></td>
          </tr>
        </thead>
        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
        <tbody>
          <tr>
            <td><?php echo $attribute['name']; ?></td>
            <?php foreach ($products as $product) { ?>
            <?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
            <td><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>
            <?php } else { ?>
            <td></td>
            <?php } ?>
            <?php } ?>
          </tr>
        </tbody>
        <?php } ?>
        <?php } ?>
        <tr>
          <td></td>
          <?php foreach ($products as $product) { ?>
          <td><input type="button" value="<?php echo $button_cart; ?>" class="btn btn-primary btn-block" onclick="cart.add('<?php echo $product['product_id']; ?>');" />
            <a href="<?php echo $product['remove']; ?>" class="btn btn-danger btn-block"><?php echo $button_remove; ?></a></td>
          <?php } ?>
        </tr>
      </table>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>


<!--
  <link rel="stylesheet" href="/catalog/view/javascript/calendarJs/css/eventCalendar.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
  <script src="/catalog/view/javascript/calendarJs/moment.js"></script>
  <script src="/catalog/view/javascript/calendarJs/jquery.eventCalendar.min.js"></script>

  <div id="eventCalendar" style="width: 500px; margin: 150px auto;"></div>

<script>
  $(function(){
    var data = [
      { "date": "2017-09-21 10:15:20", "title": "Событие 1", "description": "Анонс меоприятия", "url": "" },
      { "date": "2018-09-21 10:15:20", "title": "Событие 2", "description": "Анонс меоприятия", "url": "" },
      { "date": "2019-09-01 10:15:20", "title": "Событие 3", "description": "Анонс меоприятия", "url": "" },
      { "date": "2020-10-21 10:15:20", "title": "Событие 4", "description": "Анонс меоприятия", "url": "" },
      { "date": "2021-08-22 10:15:20", "title": "Событие 5", "description": "Анонс меоприятия", "url": "" },
      { "date": "2021-04-23 10:15:20", "title": "Событие 6", "description": "Анонс меоприятия", "url": "" },
      { "date": "2021-08-04 10:15:20", "title": "Событие 7", "description": "Анонс меоприятия", "url": "" },
      { "date": "2021-12-25 10:15:20", "title": "Событие 8", "description": "Анонс меоприятия", "url": "" },
      { "date": "2021-11-26 10:15:20", "title": "Событие 9", "description": "Анонс меоприятия", "url": "" },
      { "date": "2021-09-27 10:15:20", "title": "Событие 10", "description": "Анонс меоприятия", "url": "" },
      { "date": "2021-01-28 10:15:20", "title": "Событие 11", "description": "Анонс меоприятия", "url": "" }
    ];
    $('#eventCalendar').eventCalendar({
      jsonData: data,
      eventsjson: 'data.json',
      jsonDateFormat: 'human',
      startWeekOnMonday: false,
      openEventInNewWindow: true,
      dateFormat: 'DD-MM-YYYY',
      showDescription: false,
      locales: {
        locale: "ru",
        txt_noEvents: "Нет запланированных событий",
        txt_SpecificEvents_prev: "",
        txt_SpecificEvents_after: "события:",
        txt_NextEvents: "Следующие события:",
        txt_GoToEventUrl: "Смотреть",
        moment: {
          "months" : [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
          "monthsShort" : [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн",
            "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
          "weekdays" : [ "Воскресенье", "Понедельник","Вторник","Среда","Четверг",
            "Пятница","Суббота" ],
          "weekdaysShort" : [ "Вс","Пн","Вт","Ср","Чт",
            "Пт","Сб" ],
          "weekdaysMin" : [ "Вс","Пн","Вт","Ср","Чт",
            "Пт","Сб" ]
        }
      }
    });
  });
</script>

  <style>
    .eventCalendar-wrap {
      border:none;
      margin-bottom:20px;
      margin-top:-20px;
      background-color:#fff;
      color:#807E7E;
    }
    .eventCalendar-wrap .eventCalendar-arrow {
      text-decoration:none;
      color:#fff;
      padding:0 5px;
      line-height:28px;
      top:-6px;
      padding:18px 10px;
    }
    .eventCalendar-wrap .eventCalendar-arrow.prev {

    }
    .eventCalendar-wrap .eventCalendar-arrow:hover { opacity:0.7;}
    .eventCalendar-wrap .eventCalendar-arrow span {
      height: 0;
      width: 0;
      font-size: 0;
      line-height: 0;
      border-top: 6px solid transparent;
      border-bottom: 6px solid transparent;
      border-left: 6px solid #fff;
      float:left;
      text-indent:-5000px;
    }
    .eventCalendar-wrap .eventCalendar-arrow.eventCalendar-prev span {
      border-left-width:0;
      border-right: 6px solid #fff;
    }
    .eventCalendar-slider { height:80px;}

    .eventCalendar-monthWrap {
      top:0px;
      left:0px;
    }
    .eventCalendar-currentTitle {
      line-height:35px;
      background-color:#33a7b5;
      outline:1px solid #33a7b5;
    }
    .eventCalendar-currentTitle .eventCalendar-monthTitle {
      font-size:120%;
      text-decoration:none;
      color:#fff;
    }

    .eventCalendar-daysList {
      zoom: 1;
      padding:0;
      width:100%;

    }
    .eventCalendar-daysList.eventCalendar-showAsWeek {
      margin:10px 0px;
      width:auto;
      border-bottom-width:0;
      border-radius:0;
      background-color:#fff;

    }
    .eventCalendar-daysList.showDayNames.eventCalendar-showAsWeek {

      border-radius:none;
    }
    .eventCalendar-daysList:before, .eventCalendar-daysList:after { content:""; display:table; }
    .eventCalendar-daysList:after { clear: both; }
    .eventCalendar-day-header {
      text-transform:lowercase;
      text-align:center;
      font-size:15px;
      border-bottom:solid 1px #e3e3e3;
    }
    .eventCalendar-daysList.eventCalendar-showAsWeek li {
      height:auto; margin:0;
    }
    .eventCalendar-daysList.eventCalendar-showAsWeek li.eventCalendar-empty {
      background-color: #e3e3e3;
      min-height:29px;
    }
    .eventCalendar-day a {
      text-decoration:none;
      font-size:10px;
      color:#424242;
    }
    .eventCalendar-day {
      border-left:none;
    }
    .eventCalendar-day a  {
      border:none;
    }
    .eventCalendar-showAsWeek .eventCalendar-day { border-left-width:0;}
    .eventCalendar-showAsWeek .eventCalendar-day a  {
      border:solid 1px #e3e3e3;
      border-color:#fff #e3e3e3 #e3e3e3 #e3e3e3;
      line-height:27px;
      font-size:12px;

    }
    .eventCalendar-day a:hover {
      background-color:#E4E4E4;
      /*	box-shadow:inset 5px 5px 10px #C1C1C1;
		  text-shadow: 2px 2px 2px #C1C1C1;*/
    }
    .eventCalendar-daysList li.today a {
      color:#fff;
      background:#33a7b5;
      /*	box-shadow:inset 5px 5px 10px #777;
		  text-shadow: 2px 2px 2px #777;*/
    }
    li.eventCalendar-day.today a:hover {
      background-color:#9cdce4;
      /*box-shadow:inset 5px 5px 10px #999;*/
    }

    .eventCalendar-daysList li.eventCalendar-dayWithEvents a {
      background:#f77a11;
      color:#fff;
    }
    li.eventCalendar-day.eventCalendar-dayWithEvents a:hover {
      background-color:#f9a45d;
    }


    .eventCalendar-daysList li.current a {
      color:#fff;
      background:#449FB2;
    }
    li.eventCalendar-day.current a:hover {
      background-color:#79BDCC;
    }
    .eventCalendar-loading {
      margin:0 auto;
      padding:0px;
      background-color:#ccc;
      color:#fff;
      text-align:center;
      position:absolute;
      z-index:4;
      top:38px;
      left:0px;
    }
    .eventCalendar-loading.error {
      background-color:red;
    }

    .eventCalendar-subtitle { padding-top:10px;}
    .eventCalendar-list-wrap {
      min-height:100px;
      position:relative;
    }
    .eventCalendar-list-content.scrollable {

      height:100px;
      overflow-y:auto;
      margin:0 5px 5px 0;
    }
    .eventCalendar-list {
      margin:0; padding:0; list-style-type:none;
    }
    .eventCalendar-list li {
      padding:0 0px 5px;
      margin:0;
      margin-bottom:20px;
      clear:both;
      border-bottom:1px dashed #ebebeb;
    }
    .eventCalendar-list li time {
      font-size:11px;
      line-height:15px;
    }
    .eventCalendar-list li time em {
      float:left;
      font-style:normal;
      background-color:#767676;
      color:#fff;
      padding:2px 5px 2px 5px;
    }
    .eventCalendar-list li time small {
      font-size:11px;
      float:left;
      background-color:#f77a11;
      color:#fff;
      padding:2px 5px 2px 5px;
      margin:0 0 0 3px;
    }
    .eventCalendar-list li .eventCalendar-eventTitle {
      display:block;
      clear:both;
      font-size:150%;
      text-decoration:none;
    }
    .eventCalendar-list li a.eventCalendar-eventTitle {
      color:#33a7b5;
    }
    .eventCalendar-list li a.eventCalendar-eventTitle:hover { text-decoration:underline;}
    .eventCalendar-list li .eventDesc {
      clear: both;
      margin:0 0 5px 0;
      font-size:80%;
      line-height:1.2em;

    }
    .eventCalendar-list .eventCalendar-noEvents {
      font-size:120%;
      padding:5px;
      background-color:#f77a11;
      border-bottom: none;
      color:#fff;
      text-align:center;
    }

    .bt {
      font-size:15px;
      display:block;
      clear:both;
      text-align: center;
      margin-top:10px;
      padding: 11px 34px 12px;
      text-decoration: none;
      line-height: 1;
      color: #ffffff !important;
      background-color: #33a7b5;
      text-shadow: none;
      box-shadow: none;
      -webkit-transition: 0.1s linear all;
      -moz-transition: 0.1s linear all;
      -ms-transition: 0.1s linear all;
      -o-transition: 0.1s linear all;
      transition: 0.1s linear all;
    }
    .bt:hover {
      background-color: #f77a11;
      text-decoration: none;
    }
  </style>
  -->


</div>
<?php echo $footer; ?>