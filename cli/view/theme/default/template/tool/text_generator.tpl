  <div itemscope="" itemtype="https://schema.org/FAQPage">
    <h2><?php echo $text_heading_question; ?></h2>
    <div itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
      <h3 itemprop="name">&#128181; <?php echo $text_small_price_question; ?></h3>
      <div itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <div itemprop="text">
          <?php echo $text_small_price_subtitle; ?>
          <ul>
            <?php foreach ($category['product_small_price'] as $product_small){?>
            <li><a href="<?php echo $product_small['href']; ?>"> <?php echo $product_small['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
    <div itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
      <h3 itemprop="name"> <?php echo $text_heading_popular_question;?> </h3>
      <div itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <div itemprop="text">
          <?php echo $text_heading_popular; ?>
          <ul>
            <?php foreach ($category['popular_product'] as $popular_product){?>
            <li><a href="<?php echo $popular_product['href']; ?>"> <?php echo $popular_product['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
    <div itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
      <h3 itemprop="name">&#129351; <?php echo $text_big_price_question; ?></h3>
      <div itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <div itemprop="text">
          <?php echo $text_big_price_subtitle; ?>
          <ul>
            <?php foreach ($category['product_big_price'] as $product_big_price){?>
            <li><a href="<?php echo $product_big_price['href']; ?>"> <?php echo $product_big_price['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <h2><?php echo $text_products_subtitle; ?></h2>
  <table>
    <tr>
      <th><?php echo $text_product; ?></th>
      <th><?php echo $text_price; ?></th>
    </tr>
    <?php foreach ($category['product_data'] as $product_data){?>
    <tr>
      <td><?php echo $text_seeds; ?> <?php echo $product_data['name']; ?></td>
      <td><?php echo $product_data['price']; ?> </td>
    </tr>
    <?php } ?>
  </table>
