<?php
// Version
define('VERSION', '2.0.1.1');

// Configuration
if (is_file('config.php')) {
    require_once('config.php');
}
if((isset($_GET['cli_token']) && $_GET['cli_token'] === CLI_TOKEN) || (isset($argv) && in_array(CLI_TOKEN, $argv))){
  // Install
  if (!defined('DIR_APPLICATION')) {
      header('Location: install/index.php');
      exit;
  }

  // Startup
  require_once(DIR_SYSTEM . 'startup.php');

  // Registry
  $registry = new Registry();

  // Loader
  $loader = new Loader($registry);
  $registry->set('load', $loader);

  // Config
  $config = new Config();
  $registry->set('config', $config);

  // Database
  $db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
  $registry->set('db', $db);

  // Url
  $url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
  $registry->set('url', $url);


  // Store
  if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
      $store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  } else {
      $store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  }

  if ($store_query->num_rows) {
      $config->set('config_store_id', $store_query->row['store_id']);
  } else {
      $config->set('config_store_id', 0);
  }

  // Settings
  $query = $db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int)$config->get('config_store_id') . "' ORDER BY store_id ASC");

  foreach ($query->rows as $result) {
      if (!$result['serialized']) {
          $config->set($result['key'], $result['value']);
      } else {
          $config->set($result['key'], unserialize($result['value']));
      }
  }

  // Currency
  $registry->set('currency', new Currency($registry));

  // Tax
  $registry->set('tax', new Tax($registry));

  // Log
  $log = new Log($config->get('config_error_filename'));
  $registry->set('log', $log);

  if ($config->get('config_datetime_zone') !== null) {
      date_default_timezone_set($config->get('config_datetime_zone'));
      $date_now = new DateTime();
      $db->query("SET time_zone = '+" . ((int)$date_now->getOffset() / 60 / 60) . ":00'");
  }



  function error_handler($errno, $errstr, $errfile, $errline)
  {
      global $log, $config;

      // error suppressed with @
      if (error_reporting() === 0) {
          return false;
      }

      switch ($errno) {
          case E_NOTICE:
          case E_USER_NOTICE:
              $error = 'Notice';
              break;
          case E_WARNING:
          case E_USER_WARNING:
              $error = 'Warning';
              break;
          case E_ERROR:
          case E_USER_ERROR:
              $error = 'Fatal Error';
              break;
          default:
              $error = 'Unknown';
              break;
      }

      if ($config->get('config_error_display')) {
          echo '<b>' . $error . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
      }

      if ($config->get('config_error_log')) {
          $log->write('PHP ' . $error . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
      }

      return true;
  }

  $query = $db->query("SELECT * FROM `" . DB_PREFIX . "language`");

  foreach ($query->rows as $result) {
      $languages[$result['code']] = $result;
  }

  $config->set('config_language_id', $languages[$config->get('config_admin_language')]['language_id']);


  // Error Handler
  set_error_handler('error_handler');

  //Cli logs
  $event_log = new Log('event.log');
  $registry->set('event_log', $event_log);
  $event_log->write('start');

  $error_log = new Log('error.log');
  $registry->set('error_log', $error_log);

  // Cache
  $cache = new Cache('file');
  $registry->set('cache', $cache);

  // Session
  $session = new Session();
  $registry->set('session', $session);

  // Request
  $request = new Request();
  $registry->set('request', $request);

  $controller = new Front($registry);


  //Get action from db here

  if(isset($argv) && in_array(CLI_TOKEN, $argv)){

    $query = $db->query("SELECT * FROM " . DB_PREFIX . "task WHERE datetime_run LIKE '%" . date("Y-m-d H:i") ."%'");

    foreach ($query->rows as $task) {
      $action = new Action($task['action'], [$task['args']]);
        $controller->dispatch($action, new Action('error/not_found'));

        $action2 = new Action('tool/task', $task['task_id']);

        $controller->dispatch($action2, new Action('error/not_found'));

    }
  }elseif(isset($_GET['cli_token']) && $_GET['cli_token'] === CLI_TOKEN){
    $query = $db->query("SELECT * FROM " . DB_PREFIX . "task WHERE task_id = '" . $request->get['task_id'] . "'");

    $action = new Action($query->row['action'], [unserialize($query->row['args'])]);

    $controller->dispatch($action, new Action('error/not_found'));
  }
}
