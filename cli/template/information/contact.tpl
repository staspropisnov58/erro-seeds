<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div class="container mb-5">
  <div class="main_content_text">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <div class="contact_us desctop">
      <div class="d-flex flex-column mx-auto">
        <div class="clouds d-flex flex-row">
          <div class="telephone">
            <img src="/catalog/view/theme/old/images/cloud1.png" alt="" class="cloud">
            <div>
              <a href="tel:+380960000849">
                <svg width="20" height="20" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M5.77725 2V4.68067V2Z" fill="black"></path><path d="M5.77725 2V4.68067" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
                  <path d="M10.7004 5.57685L8.15094 6.40522L10.7004 5.57685Z" fill="black"></path><path d="M10.7004 5.57685L8.15094 6.40522" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
                  <path d="M8.81998 11.3642L7.24432 9.1955L8.81998 11.3642Z" fill="black"></path><path d="M8.81998 11.3642L7.24432 9.1955" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
                  <path d="M2.73465 11.3643L4.31031 9.1956L2.73465 11.3643Z" fill="black"></path><path d="M2.73465 11.3643L4.31031 9.1956" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
                  <path d="M0.854216 5.57696L3.40369 6.40534L0.854216 5.57696Z" fill="black"></path><path d="M0.854216 5.57696L3.40369 6.40534" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
                </svg>
                +38 096 00 00 849</a>
              <a href="tel:+380950000849">
                <svg width="20" height="20" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">                    <path d="M3.79006 0.422042C5.29622 -0.177075 7.05066 -0.135652 8.51668 0.560214C8.09893 0.496876 7.67403 0.544633 7.26053 0.6144C6.1375 0.826076 5.07026 1.35343 4.23605 2.13689C3.43618 2.92156 2.87567 3.96268 2.71736 5.07655C2.61264 5.86581 2.70971 6.69364 3.06457 7.41202C3.43025 8.16574 4.07393 8.78393 4.84975 9.10296C5.59733 9.4187 6.4645 9.41522 7.22172 9.13339C8.35789 8.71726 9.17423 7.59364 9.26759 6.39316C9.32632 5.60637 9.13457 4.76939 8.60493 4.16593C8.09931 3.57524 7.35982 3.24987 6.6198 3.06467C6.58013 2.33628 6.92398 1.6167 7.46927 1.14299C7.77209 0.870476 8.14789 0.696771 8.53651 0.585613L8.56602 0.575447C9.67849 1.10787 10.6238 1.98596 11.226 3.06394C11.7421 3.98171 12.0112 5.0393 11.9845 6.09278C11.9794 7.45506 11.4692 8.80384 10.5946 9.84547C9.76759 10.8388 8.61086 11.5534 7.35014 11.8395C6.08607 12.1306 4.72606 12.005 3.54302 11.4689C2.38544 10.9529 1.40473 10.0559 0.780842 8.95253C0.261864 8.03561 -0.0126822 6.9784 0.0101014 5.9237C0.0125765 4.60955 0.480149 3.30555 1.29652 2.27792C1.94861 1.45737 2.81369 0.804921 3.79006 0.422042Z" fill="#E60000"></path>
                  <path d="M7.2606 0.614536C7.67407 0.544769 8.099 0.497044 8.51675 0.560382L8.57672 0.570516L8.53658 0.585717C8.14796 0.696939 7.77216 0.870613 7.46934 1.14316C6.92405 1.61683 6.5802 2.33645 6.61987 3.06484C7.35986 3.25001 8.09938 3.57535 8.605 4.16607C9.13464 4.76953 9.32639 5.60651 9.26766 6.3933C9.1743 7.59375 8.35796 8.7174 7.2218 9.13353C6.46457 9.41535 5.5974 9.41881 4.84982 9.10313C4.07397 8.78406 3.43032 8.16591 3.06464 7.41215C2.70978 6.69377 2.61271 5.86594 2.71746 5.07668C2.87574 3.96282 3.43625 2.92169 4.23616 2.13703C5.07033 1.35357 6.13757 0.826213 7.2606 0.614536Z" fill="white"></path>
                </svg> +38 095 00 00 849</a>
              <a href="tel:+380930000849">
                <svg width="20" height="20" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">                    <path fill-rule="evenodd" clip-rule="evenodd" d="M5.975 11.95C9.2749 11.95 11.95 9.2749 11.95 5.975C11.95 2.6751 9.2749 0 5.975 0C2.6751 0 0 2.6751 0 5.975C0 9.2749 2.6751 11.95 5.975 11.95Z" fill="white"></path>
                  <path d="M0.00976562 5.98538C0.00976562 2.68294 2.68294 0.00976562 5.98538 0.00976562C9.28781 0.00976562 11.961 2.68294 11.961 5.98538C11.961 9.28781 9.28781 11.961 5.98538 11.961C5.33172 11.961 4.70733 11.8585 4.12196 11.6634C4.54635 9.19513 6.10245 7.18538 8.16098 6.33172C8.40489 6.61464 8.76586 6.79025 9.16586 6.79025C9.89757 6.79025 10.4927 6.19513 10.4927 5.46342C10.4927 4.73172 9.89757 4.13659 9.16586 4.13659C8.43416 4.13659 7.83903 4.73172 7.83903 5.46342C5.21464 6.25367 3.08294 8.08781 2.00489 10.4439C1.66342 10.1366 1.35611 9.79025 1.09269 9.41464C1.15611 7.12196 2.28781 5.11708 3.99025 3.93172C4.23416 4.20489 4.59025 4.3805 4.98538 4.3805C5.71708 4.3805 6.3122 3.78538 6.3122 3.04879C6.3122 2.3122 5.71708 1.72196 4.98538 1.72196C4.24879 1.72196 3.65855 2.31708 3.65855 3.04879C3.65855 3.08781 3.66342 3.12684 3.66342 3.16099C2.07806 3.71708 0.780497 4.81464 0.0195217 6.21464C0.00976562 6.14147 0.00976562 6.06342 0.00976562 5.98538Z" fill="#FFC10E"></path>
                </svg> +38 093 00 00 849</a>
            </div>
          </div>
          <div class="socials">
            <img src="/catalog/view/theme/old/images/cloud2.png" alt="">
            <div>
              <a href="https://www.facebook.com/esuacom/"><img src="/catalog/view/theme/old/images/facebook.svg" alt="facebook"> @ESseeds</a>
              <a href="https://t.me/Errors_Seeds"><img src="/catalog/view/theme/old/images/telegram.svg" alt="telegram"> /Errors_Seeds</a>
              <a href="https://www.instagram.com/errorsseeds_ua/"><img src="/catalog/view/theme/old/images/instagram.svg" alt="instagram"> @errorsseeds_ua</a>
              <a href="https://www.youtube.com/channel/UCWk-ZUoQ82Y2cJWkTS_3kLQ"><img src="/catalog/view/theme/old/images/youtube.svg" alt="youtube"> /ErrorsSeedsbank</a>
            </div>
          </div>
          <div class="mail">
            <img src="/catalog/view/theme/old/images/cloud3.png" alt="">
            <div>
              <a href="tel:380930000849"><img src="/catalog/view/theme/old/images/viber.svg" alt="facebook"> 0930000849</a>
              <a href="https://t.me/ErrorsSeeds"><img src="/catalog/view/theme/old/images/telegram.svg" alt="telegram"> @ErrorsSeeds</a>
              <a href="mailto:autoryder@gmail.com"><i class="fas fa-envelope"></i> autoryder@gmail.com</a>
            </div>
          </div>
        </div>
        <div class="contact_form">
          <div class="cann"></div>
          <div class="pot">
            <p>
              <?php echo $text_comment; ?>
            </p>
            <?php if ($form) { ?>
            <form action="<?php echo $form['action']; ?>" name="<?php echo $form['name']; ?>" class="d-flex flex-column" class="protected" method="post">
              <?php foreach ($form['fields'] as $field_name => $field) { ?>
                <?php if ($field['error']) { ?>
                <lable class="error">
                  <?php echo $field['error']; ?>
                </lable>
                <?php } ?>
                <?php if ($field['type'] === 'textarea') { ?>
                <textarea name="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>" class="form-control trans-input form-control-success"><?php echo $field['value']; ?></textarea>
                <?php } else { ?>
                <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>" class="form-control trans-input form-control-success" value="<?php echo $field['value']; ?>">
                <?php } ?>
              <?php } ?>
                  <button type="submit" class="g-recaptcha green_button" id="submit_contact">Отправить</button>
                </form>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="contact_us mobile">
      <div class="telephone mb-4">
        <div>
          <a href="tel:+380960000849">
            <svg width="20" height="20" viewBox="0 0 13 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M5.77725 2V4.68067V2Z" fill="black"></path><path d="M5.77725 2V4.68067" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
              <path d="M10.7004 5.57685L8.15094 6.40522L10.7004 5.57685Z" fill="black"></path><path d="M10.7004 5.57685L8.15094 6.40522" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
              <path d="M8.81998 11.3642L7.24432 9.1955L8.81998 11.3642Z" fill="black"></path><path d="M8.81998 11.3642L7.24432 9.1955" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
              <path d="M2.73465 11.3643L4.31031 9.1956L2.73465 11.3643Z" fill="black"></path><path d="M2.73465 11.3643L4.31031 9.1956" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
              <path d="M0.854216 5.57696L3.40369 6.40534L0.854216 5.57696Z" fill="black"></path><path d="M0.854216 5.57696L3.40369 6.40534" stroke="#00A0FF" stroke-width="2.5" stroke-linecap="round"></path>
            </svg>
            +38 096 00 00 849</a>
          <a href="tel:+380950000849">
            <svg width="20" height="20" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">                    <path d="M3.79006 0.422042C5.29622 -0.177075 7.05066 -0.135652 8.51668 0.560214C8.09893 0.496876 7.67403 0.544633 7.26053 0.6144C6.1375 0.826076 5.07026 1.35343 4.23605 2.13689C3.43618 2.92156 2.87567 3.96268 2.71736 5.07655C2.61264 5.86581 2.70971 6.69364 3.06457 7.41202C3.43025 8.16574 4.07393 8.78393 4.84975 9.10296C5.59733 9.4187 6.4645 9.41522 7.22172 9.13339C8.35789 8.71726 9.17423 7.59364 9.26759 6.39316C9.32632 5.60637 9.13457 4.76939 8.60493 4.16593C8.09931 3.57524 7.35982 3.24987 6.6198 3.06467C6.58013 2.33628 6.92398 1.6167 7.46927 1.14299C7.77209 0.870476 8.14789 0.696771 8.53651 0.585613L8.56602 0.575447C9.67849 1.10787 10.6238 1.98596 11.226 3.06394C11.7421 3.98171 12.0112 5.0393 11.9845 6.09278C11.9794 7.45506 11.4692 8.80384 10.5946 9.84547C9.76759 10.8388 8.61086 11.5534 7.35014 11.8395C6.08607 12.1306 4.72606 12.005 3.54302 11.4689C2.38544 10.9529 1.40473 10.0559 0.780842 8.95253C0.261864 8.03561 -0.0126822 6.9784 0.0101014 5.9237C0.0125765 4.60955 0.480149 3.30555 1.29652 2.27792C1.94861 1.45737 2.81369 0.804921 3.79006 0.422042Z" fill="#E60000"></path>
              <path d="M7.2606 0.614536C7.67407 0.544769 8.099 0.497044 8.51675 0.560382L8.57672 0.570516L8.53658 0.585717C8.14796 0.696939 7.77216 0.870613 7.46934 1.14316C6.92405 1.61683 6.5802 2.33645 6.61987 3.06484C7.35986 3.25001 8.09938 3.57535 8.605 4.16607C9.13464 4.76953 9.32639 5.60651 9.26766 6.3933C9.1743 7.59375 8.35796 8.7174 7.2218 9.13353C6.46457 9.41535 5.5974 9.41881 4.84982 9.10313C4.07397 8.78406 3.43032 8.16591 3.06464 7.41215C2.70978 6.69377 2.61271 5.86594 2.71746 5.07668C2.87574 3.96282 3.43625 2.92169 4.23616 2.13703C5.07033 1.35357 6.13757 0.826213 7.2606 0.614536Z" fill="white"></path>
            </svg> +38 095 00 00 849</a>
          <a href="tel:+380930000849">
            <svg width="20" height="20" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">                    <path fill-rule="evenodd" clip-rule="evenodd" d="M5.975 11.95C9.2749 11.95 11.95 9.2749 11.95 5.975C11.95 2.6751 9.2749 0 5.975 0C2.6751 0 0 2.6751 0 5.975C0 9.2749 2.6751 11.95 5.975 11.95Z" fill="white"></path>
              <path d="M0.00976562 5.98538C0.00976562 2.68294 2.68294 0.00976562 5.98538 0.00976562C9.28781 0.00976562 11.961 2.68294 11.961 5.98538C11.961 9.28781 9.28781 11.961 5.98538 11.961C5.33172 11.961 4.70733 11.8585 4.12196 11.6634C4.54635 9.19513 6.10245 7.18538 8.16098 6.33172C8.40489 6.61464 8.76586 6.79025 9.16586 6.79025C9.89757 6.79025 10.4927 6.19513 10.4927 5.46342C10.4927 4.73172 9.89757 4.13659 9.16586 4.13659C8.43416 4.13659 7.83903 4.73172 7.83903 5.46342C5.21464 6.25367 3.08294 8.08781 2.00489 10.4439C1.66342 10.1366 1.35611 9.79025 1.09269 9.41464C1.15611 7.12196 2.28781 5.11708 3.99025 3.93172C4.23416 4.20489 4.59025 4.3805 4.98538 4.3805C5.71708 4.3805 6.3122 3.78538 6.3122 3.04879C6.3122 2.3122 5.71708 1.72196 4.98538 1.72196C4.24879 1.72196 3.65855 2.31708 3.65855 3.04879C3.65855 3.08781 3.66342 3.12684 3.66342 3.16099C2.07806 3.71708 0.780497 4.81464 0.0195217 6.21464C0.00976562 6.14147 0.00976562 6.06342 0.00976562 5.98538Z" fill="#FFC10E"></path>
            </svg> +38 093 00 00 849</a>
        </div>
      </div>
      <div class="socials mb-4">
        <div>
          <a href="https://www.facebook.com/ESUA420/ "><img src="/catalog/view/theme/old/images/facebook.svg" alt="facebook"> @ESseeds</a>
          <a href="https://t.me/Errors_Seeds"><img src="/catalog/view/theme/old/images/telegram.svg" alt="telegram"> /Errors_Seeds</a>
          <a href="https://www.instagram.com/errorsseeds_ua/"><img src="/catalog/view/theme/old/images/instagram.svg" alt="instagram"> @errorsseeds_ua</a>
          <a href="https://www.youtube.com/channel/UCo1z2GSthMqIOhvTU5EgZtg"><img src="/catalog/view/theme/old/images/youtube.svg" alt="youtube"> /ErrorsSeedsbank</a>
        </div>
      </div>
      <div class="mail mb-4">
        <div>
          <a href="tel:380930000849"><img src="/catalog/view/theme/old/images/viber.svg" alt="facebook"> 0930000849</a>
          <a href="https://t.me/ErrorsSeeds"><img src="/catalog/view/theme/old/images/telegram.svg" alt="telegram"> @ErrorsSeeds</a>
          <a href="mailto:autoryder@gmail.com"><i class="fas fa-envelope"></i> autoryder@gmail.com</a>
        </div>
      </div>
      <div class="contact_form mb-4" >
          <p>
            <?php echo $text_comment; ?>
          </p>
          <?php if ($form) { ?>
          <form action="<?php echo $form['action']; ?>" name="<?php echo $form['name']; ?>" class="d-flex flex-column" class="protected" method="post">
            <?php foreach ($form['fields'] as $field_name => $field) { ?>
            <?php if ($field['error']) { ?>
            <lable class="error" style="color:#e60000">
              <?php echo $field['error']; ?>
            </lable>
            <?php } ?>
            <?php if ($field['type'] === 'textarea') { ?>
            <textarea name="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>" class="form-control trans-input form-control-success"><?php echo $field['value']; ?></textarea>
            <?php } else { ?>
            <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" placeholder="<?php echo $field['placeholder']; ?>" class="form-control trans-input form-control-success" value="<?php echo $field['value']; ?>">
            <?php } ?>

            <?php } ?>
            <button type="submit" class="g-recaptcha green_button" id="submit_contact">Отправить</button>
          </form>
          <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
