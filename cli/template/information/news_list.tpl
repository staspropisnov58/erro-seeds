<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div class="container">
  <div class="main_content_text">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <div class="post_page">
      <div class="h1">
        <h1><?php echo $text_news; ?>
        </h1>
      </div>
      <div class="d-flex justify-content-between align-items-stretch flex-wrap news-list">
        <?php foreach ($all_news as $news) { ?>
        <a href="<?php echo $news['view']; ?>" class="post_block">
          <div class="img_block mb-2"><img alt="<?php echo $news['title']; ?>" src="<?= $news['image_2']; ?>"></div>
          <h2 class="post_title mb-2">
            <?php echo $news['title']; ?>
          </h2>
          <div class="post_date">
            <?php echo $news['date_added']; ?>
          </div>
        </a>
        <?php } ?>
      </div>
      <div>
        <div class="navigator_product mt-3 mb-3">
          <?php echo $pagination; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
