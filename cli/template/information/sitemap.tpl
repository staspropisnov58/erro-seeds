<head>
  <meta charset="UTF-8">
  <base href="<?php echo $base; ?>" />
  <title>
    <?php echo $heading_title; ?>
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="shortcut icon" href="/logo_favicon.png">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/bootstrap.min.css" type="text/css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/slinky.min.css" type="text/css">  
  <link rel="stylesheet" href="/catalog/view/theme/old/css/main.min.css">
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>

  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>" rel="icon" />
  <?php } ?>

  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>

  <?php if ($robots) { ?>
  <meta name="robots" content="<?php echo $robots; ?>" />
  <?php } ?>
  <?php if (isset($linkes)){ ?>
  <?php foreach($linkes['hreflang'] as $key => $linkeses){ ?>
  <link rel="alternate" hreflang="<?php echo $linkeses['hreflang']; ?>" href="<?php echo $linkes['href'][$key]; ?>" />
  <?php } ?>
  <?php } ?>
  <script type="text/javascript" src="/catalog/view/theme/old/js/jquery-3.3.1.min.js"></script>
  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>

  <script src="/published/SC/html/scripts/js/slides.min.jquery.js" type="text/javascript"></script>
  <script src="/published/SC/html/scripts/js/test.js" type="text/javascript"></script>
  <script type="text/javascript" src="/catalog/view/theme/old/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="/catalog/view/theme/old/js/slinky.min.js"></script>
  <script src="/published/publicdata/WEBASYST/attachments/SC/themes/classic/jquery.validate.js" type="text/javascript"></script>
  <script src="/published/publicdata/WEBASYST/attachments/SC/themes/classic/validate.register.js" type="text/javascript"></script>
  <script type="text/javascript" src="/published/publicdata/WEBASYST/attachments/SC/themes/classic/jquery.countdown.js"></script>
  <script type="text/javascript" src="/published/publicdata/WEBASYST/attachments/SC/themes/classic/head.js"></script>

  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T36VTNM');
  </script>
  <!-- End Google Tag Manager -->
  <meta name="yandex-verification" content="3940d507abba79b6" />
</head>
<div class="container" id="sitemap_wrapper">
  <a href="/"><?php echo $text_back_home; ?></a>
  <div class="sitemap_page d-flex flex-wrap flex-row">
    <?php foreach ($map as $level1) { ?>
    <div class="text_block">
      <?php if (isset($level1['href']) && isset($level1['name'])) { ?>
      <p class="title"><a href="<?=$level1['href']?>"><?=$level1['name']?></a></p>
      <?php } ?>
      <ul class="sitemap_line2">
        <?php if (isset($level1['content'])) { ?>
          <?php foreach ($level1['content'] as $level2) { ?>
            <li>
              <a href="<?=$level2['href']?>"><?=$level2['name']?></a>
            </li>
          <?php } ?>
        <?php } ?>
      </ul>
    </div>
    <?php } ?>
  </div>
  <div class="navigator_product mb-3">
    <?php echo $pagination; ?>
  </div>
</div>
