<div class="cat_list" >
  <?php foreach($products as $product){ ?>
  <div class="list">
      <form class="product_brief_block d-flex flex-row" method="post" rel="<?php echo $product['product_id']; ?>"
        <?php if ($product['form']) { ?> action="<?php echo $product['form']['action']; ?>" name="<?php echo $product['form']['name']; ?>"
        <?php } else { ?> action="<?php echo $product['href']; ?>" <?php }?>>
        <input name="action" value="add_product" type="hidden">
        <input type="hidden" name="product_id" value="<?php echo $product['product_id']; ?>">
        <input name="quantity" value="1" type="hidden">
        <input class="product_price" value="<?php echo $product['price']; ?>" type="hidden">
        <noscript>
            <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
        </noscript>
      <div class="img_block">
        <a href="<?php echo $product['href']; ?>">
          <?php if ($product['stickers']) { ?>
            <div class="stickers-block">
            <?php foreach ($product['stickers'] as $sticker){ ?>
                <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
            <?php } ?>
          </div>
          <?php } ?>
          <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['alt_image']; ?>" title="<?php echo $product['title_image']; ?>">
        </a>
      </div>
      <div class="descript_list d-flex flex-column justify-content-between">
        <div class="prdbrief_name">
          <a href="<?php echo $product['href'] ?>" ><?php echo $product['name']; ?></a>
          <span><?php echo $product['manufacturer']; ?></span>
        </div>
        <div class="prdbrief_price">
          <!-- Класс переименован для того чтобы отключить js -->
          <div class="_totalPrice d-flex">
            <?php if ($product['special']){ ?>
              <span class="old_price"><?php echo $product['price']; ?></span>
              <span class="new_price"><?php echo $product['special']; ?></span>
            <?php } else { ?>
            <span class="new_price"><?php echo $product['price']; ?></span>
            <?php } ?>
          </div>
        </div>
        <div class="d-flex flex-column">
          <div class="item-rating">
            <?php for($r=1; $r<=5; $r++): ?>
              <?php if($r <= $product['rating']){ ?>
                <span class="active">
                  <svg viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.162695 5.23494C0.115422 5.22136 0.0645553 5.23619 0.0319787 5.27304C-0.000597835 5.30989 -0.00907458 5.36219 0.0101953 5.40744C0.910195 7.40494 2.4977 9.54994 4.4977 10.7499C3.22494 10.7317 1.96667 11.0217 0.830195 11.5949C0.778899 11.6299 0.7482 11.6879 0.7482 11.7499C0.7482 11.812 0.778899 11.87 0.830195 11.9049C2.1277 12.5424 3.6927 12.9049 5.1477 12.6799C4.82674 13.1911 4.60614 13.7587 4.4977 14.3524C4.49131 14.3995 4.51064 14.4464 4.54835 14.4754C4.58605 14.5043 4.63636 14.5108 4.6802 14.4924C5.6127 14.1999 6.5627 13.5899 6.9952 12.6799V15.9104C6.9952 16.0484 7.10712 16.1604 7.2452 16.1604C7.38327 16.1604 7.4952 16.0484 7.4952 15.9104V12.6799C7.9277 13.5874 8.8777 14.1999 9.81019 14.4924C9.85403 14.5108 9.90434 14.5043 9.94204 14.4754C9.97975 14.4464 9.99908 14.3995 9.99269 14.3524C9.88502 13.7589 9.66527 13.1913 9.34519 12.6799C10.7977 12.9074 12.3627 12.5424 13.6627 11.9049C13.714 11.87 13.7447 11.812 13.7447 11.7499C13.7447 11.6879 13.714 11.6299 13.6627 11.5949C12.527 11.022 11.2696 10.7321 9.99769 10.7499C11.9777 9.55744 13.5802 7.41494 14.4977 5.40744C14.517 5.36219 14.5085 5.30989 14.4759 5.27304C14.4433 5.23619 14.3925 5.22136 14.3452 5.23494C12.1727 5.94494 9.81769 7.34994 8.4177 9.26244H8.4052C9.1927 6.31244 8.5777 2.80244 7.3727 0.0774412C7.35334 0.0325185 7.30911 0.00341797 7.2602 0.00341797C7.21128 0.00341797 7.16705 0.0325185 7.1477 0.0774412C5.9402 2.82744 5.3302 6.32744 6.1152 9.26244H6.1027C4.7027 7.36744 2.3552 5.94994 0.162695 5.23494Z" fill="#65BD00"/>
                  </svg>
                </span>
               <?php } else { ?>
                <span>
                  <svg viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.162695 5.23494C0.115422 5.22136 0.0645553 5.23619 0.0319787 5.27304C-0.000597835 5.30989 -0.00907458 5.36219 0.0101953 5.40744C0.910195 7.40494 2.4977 9.54994 4.4977 10.7499C3.22494 10.7317 1.96667 11.0217 0.830195 11.5949C0.778899 11.6299 0.7482 11.6879 0.7482 11.7499C0.7482 11.812 0.778899 11.87 0.830195 11.9049C2.1277 12.5424 3.6927 12.9049 5.1477 12.6799C4.82674 13.1911 4.60614 13.7587 4.4977 14.3524C4.49131 14.3995 4.51064 14.4464 4.54835 14.4754C4.58605 14.5043 4.63636 14.5108 4.6802 14.4924C5.6127 14.1999 6.5627 13.5899 6.9952 12.6799V15.9104C6.9952 16.0484 7.10712 16.1604 7.2452 16.1604C7.38327 16.1604 7.4952 16.0484 7.4952 15.9104V12.6799C7.9277 13.5874 8.8777 14.1999 9.81019 14.4924C9.85403 14.5108 9.90434 14.5043 9.94204 14.4754C9.97975 14.4464 9.99908 14.3995 9.99269 14.3524C9.88502 13.7589 9.66527 13.1913 9.34519 12.6799C10.7977 12.9074 12.3627 12.5424 13.6627 11.9049C13.714 11.87 13.7447 11.812 13.7447 11.7499C13.7447 11.6879 13.714 11.6299 13.6627 11.5949C12.527 11.022 11.2696 10.7321 9.99769 10.7499C11.9777 9.55744 13.5802 7.41494 14.4977 5.40744C14.517 5.36219 14.5085 5.30989 14.4759 5.27304C14.4433 5.23619 14.3925 5.22136 14.3452 5.23494C12.1727 5.94494 9.81769 7.34994 8.4177 9.26244H8.4052C9.1927 6.31244 8.5777 2.80244 7.3727 0.0774412C7.35334 0.0325185 7.30911 0.00341797 7.2602 0.00341797C7.21128 0.00341797 7.16705 0.0325185 7.1477 0.0774412C5.9402 2.82744 5.3302 6.32744 6.1152 9.26244H6.1027C4.7027 7.36744 2.3552 5.94994 0.162695 5.23494Z" fill="#686D74"/>
                  </svg>
                </span>
              <?php } ?>
            <?php  endfor; ?>
          </div>
          <p class="in_stock">
            <?php echo $product['stock']; ?>
          </p>
        </div>
        <?php if ($product['option']) { ?>
          <div class="d-flex flex-column">
            <input type="hidden" name="option[<?php echo $product['option']['product_option_id']; ?>]" value="<?php echo $product['option']['product_option_value_id']; ?>">
            <span><?php echo $product['option']['name']; ?>: <?php echo $product['option']['value']; ?></span>
          </div>
        <?php } ?>
        <div class="prdbrief_add2cart">
            <?php if ($product['quantity'] > 0) { ?>
              <button class="green_button btn" type="submit" data-toggle="model" onclick="<?php echo $product['onclick']; ?>"><?php echo $text_buy; ?></button>
            <?php } else { ?>
              <button class="entrance-btn btn" type="submit" data-toggle="model" onclick="<?php echo $product['onclick']; ?>"><?php echo $button_entrance; ?></button>
            <?php } ?>
        </div>
      </div>
      <!-- если товар снят с производства -->
      <!-- <div class="descript_list d-flex flex-column justify-content-between">
        <div class="prdbrief_name">
          <a href="<?php echo $product['href'] ?>" ><?php echo $product['name']; ?></a>
          <span><?php echo $product['manufacturer']; ?></span>
        </div>
        <p class="discontinued">Товар снят с производства</p>
      </div> -->
      <!--  -->

      <!-- если снят с производства, нужно убрать блок list-options -->
      <div class="flex-column list-options">
        <?php foreach ($product['attribute_groups'] as $group) { ?>
          <?php foreach ($group['attribute'] as $key => $attribute) { ?>
            <p><span><?php echo $attribute['name']; ?>:</span> <?php echo $attribute['text']; ?></p>
          <?php } ?>
        <?php } ?>
      </div>
    </form>
  </div>
<?php } ?>
</div>
