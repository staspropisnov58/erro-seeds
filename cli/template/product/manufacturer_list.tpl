<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div id="content" class="container seedbank">
  <h1><?php echo $heading_title; ?></h1>
  <div class="d-flex flex-row align-items-center justify-content-between">
    <div id="cat_product_sort" class="d-flex flex-row  align-items-center">
      <?php echo $column_right; ?>
        <?php echo $text_filter; ?>
        <div class="navigator_product">
          <?php echo $pagination; ?>
        </div>
    </div>
  </div>
  <div class="hr"></div>
  <?php if ($categories) { ?>
  <?php foreach ($categories as $category) { ?>
  <?php if ($category['manufacturer']) { ?>
  <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
  <div class="row">
    <?php foreach ($manufacturers as $manufacturer) { ?>
    <div class="seedbank_list col-12">
      <div class="seedbank_item d-flex flex-row flex-wrap">
        <div class="seedbank_image col-12 col-sm-4">
          <a href="<?php echo $manufacturer['href']; ?>">
                      <img src="<?php echo $manufacturer['image']; ?>" alt="<?php echo $manufacturer['name']; ?>" title="<?php echo $manufacturer['name']; ?>">
                    </a>
        </div>
        <div class="seedbank_text col-12 col-sm-8">
          <div class="seedbank_title">
            <a href="<?php echo $manufacturer['href']; ?>"><h2><?php echo $manufacturer['name']; ?></h2></a>
          </div>
          <div class="seedbank_description">
            <?php echo $manufacturer['description_2']; ?>
          </div>
          <div class="seedbank_link">
            <a href="<?php echo $manufacturer['href']; ?>">
              <?php echo $show_products; ?>
            </a>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <div class="navigator_product">
    <?php echo $pagination; ?>
  </div>
  <?php } else { ?>
  <p>
    <?php echo $text_empty; ?>
  </p>
  <div class="buttons clearfix">
    <div class="pull-right">
      <a href="<?php echo $continue; ?>" class="btn btn-primary">
        <?php echo $button_continue; ?>
      </a>
    </div>
  </div>
  <?php } ?>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
