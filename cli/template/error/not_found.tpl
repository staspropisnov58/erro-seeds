<?php echo $header; ?>
<div id="wrapper">
  <div id="main">
    <div class="container">
      <div class="error-pages mb-3 mt-3">
        <div class="error-form-left">
          <?=$text_error;?>
        </div>
        <div class="error-form-center img-holder">
          <p>404</p>
          <img src="/catalog/view/theme/old/images/icon-404.svg" alt="404">
        </div>
        <div class="error-form-right error-form-text">
          <?php if (isset($text_page_not_found)) { ?>
          <?=$text_page_not_found;?>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
