<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <base href="<?php echo $base; ?>" />
  <meta charset="UTF-8" />
  <title>
    <?php echo $title; ?>
  </title>
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>
  <?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>" rel="icon" />
  <?php } ?>
  <link rel="preconnect" crossorigin="crossorigin" href="https://stats.g.doubleclick.net">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.google.com">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.google.com.ua">
  <link rel="preconnect" crossorigin="crossorigin" href="https://mc.webvisor.org">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.google-analytics.com">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.gstatic.com">
  <link rel="preconnect" crossorigin="crossorigin" href="https://cdn.jsdelivr.net">
  <link rel="preconnect" crossorigin="crossorigin" href="https://www.googletagmanager.com">
  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T36VTNM');
  </script>
  <!-- End Google Tag Manager -->
  <?php foreach ($links as $link) { ?>
  <?php  if($link['hreflang']){ ?>
  <link rel="<?=$link['rel']?>" hreflang="<?=$link['hreflang']?>" href="<?=$link['href']?>">
  <?php }else{ ?>
  <link href="<?=$link['href']?>" rel="<?=$link['rel']?>">
  <?php } ?>
  <?php } ?>
  <?php if ($robots) { ?>
  <meta name="robots" content="<?php echo $robots; ?>" />
  <?php } ?>

  <?php if ($microdata){
        echo $microdata;
      } ?>

      <?php if($opengraph){ ?>
        <?php foreach($opengraph['opengraph'] as $key => $value){ ?>
          <meta property="<?php echo $key; ?>" content="<?php echo $value; ?>">
        <?php } ?>
      <?php } ?>


  <?php if ($e_commerce){
        echo $e_commerce;
      } ?>
  <meta name="yandex-verification" content="3940d507abba79b6" />
  <link rel="shortcut icon" href="/logo_favicon.png">
  <script type='text/javascript'>
  (function() {
    var widget_id = '<?php echo $jivochate_code; ?>';
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = '//code.jivosite.com/script/widget/' + widget_id;
    var ss = document.getElementsByTagName('script')[0];
    ss.parentNode.insertBefore(s, ss);
  })();
</script>
  <link rel="stylesheet" href="/catalog/view/theme/old/css/bootstrap.min.css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/main.min.css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/checkout/style_new_checkout.css" type="text/css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/animate.css" type="text/css">
  <link rel="stylesheet" href="/catalog/view/theme/old/css/font_aw.min.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Oswald:400,600&display=swap&subset=cyrillic" rel="stylesheet">
  <?php foreach ($styles as $style) { ?>
  <link rel="stylesheet" href="<?php echo $style['href']; ?>">
  <?php } ?>
  <script src="/catalog/view/theme/old/js/jquery-3.3.1.min.js"></script>
  <script defer src="/catalog/view/theme/old/js/bootstrap.min.js"></script>
  <script defer src="/catalog/view/theme/old/js/fontawesome.min.js" defer data-search-pseudo-elements searchPseudoElements></script>
  <?php foreach ($scripts as $script) { ?>
  <script defer src="<?php echo $script; ?>"></script>
  <?php } ?>
</head>

<body>
  <!--  BODY -->
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T36VTNM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <?php if($agreement_popup) { ?>
  <div class="modal fade bd-example-modal-lg" id="modalagreement" tabindex="-1" data-backdrop="static" role="dialog" data-keyboard="false" aria-labelledby="modalagreement" aria-hidden="true">
    <div class="modal-dialog modal-lg agr_wrap" role="document">
      <div class="modal-content">
        <div class="agr_text">
          <h4 class="modal-title"><?=$agreement_popup['title']?></h4>
          <?=$agreement_popup['description']?>
        </div>
        <div class="choise">
          <button type="button" id="agreement-yes" class="agr_buttons"><?=$agreement_popup['button_yes']?></button>
          <a id="agreement-no" class="agr_buttons" href="https://www.google.com/">
            <?=$agreement_popup['button_no']?>
          </a>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "ImageObject",
      "author": "Errors Seeds",
      "contentUrl": "/published/publicdata/WEBASYST/attachments/SC/products_pictures/semena-fotoperiodichnogo-feminizirovannogo-sorta-Somango-Feminised.jpg"
    }
  </script>
  <script type="text/text/javascript" async src="https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js"></script>
  <div class="bg_header bg_header_mob">
    <div class="container d-flex flex-column">
      <div class="header header_mob">
        <button type="button" class="menu_button">
          <svg viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect opacity="0.01" width="22" height="22" fill="#D8D8D8"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M20.0001 5.9001H2.0001C1.50304 5.9001 1.1001 5.49715 1.1001 5.0001C1.1001 4.50304 1.50304 4.1001 2.0001 4.1001H20.0001C20.4972 4.1001 20.9001 4.50304 20.9001 5.0001C20.9001 5.49715 20.4972 5.9001 20.0001 5.9001ZM2.0001 12.4001H14.0001C14.4972 12.4001 14.9001 11.9972 14.9001 11.5001C14.9001 11.003 14.4972 10.6001 14.0001 10.6001H2.0001C1.50304 10.6001 1.1001 11.003 1.1001 11.5001C1.1001 11.9972 1.50304 12.4001 2.0001 12.4001ZM2.0001 18.9001H17.5001C17.9972 18.9001 18.4001 18.4972 18.4001 18.0001C18.4001 17.503 17.9972 17.1001 17.5001 17.1001H2.0001C1.50304 17.1001 1.1001 17.503 1.1001 18.0001C1.1001 18.4972 1.50304 18.9001 2.0001 18.9001Z" fill="white"/>
          </svg>
        </button>
        <div class="mob_menu" id="mobile_menu">
          <button type="button" class="close">
            <svg width="20" height="20" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0.209705 0.387101L0.292893 0.292893C0.653377 -0.0675907 1.22061 -0.0953203 1.6129 0.209705L1.70711 0.292893L8.5 7.085L15.2929 0.292893C15.6534 -0.0675907 16.2206 -0.0953203 16.6129 0.209705L16.7071 0.292893C17.0676 0.653377 17.0953 1.22061 16.7903 1.6129L16.7071 1.70711L9.915 8.5L16.7071 15.2929C17.0676 15.6534 17.0953 16.2206 16.7903 16.6129L16.7071 16.7071C16.3466 17.0676 15.7794 17.0953 15.3871 16.7903L15.2929 16.7071L8.5 9.915L1.70711 16.7071C1.34662 17.0676 0.779392 17.0953 0.387101 16.7903L0.292893 16.7071C-0.0675907 16.3466 -0.0953203 15.7794 0.209705 15.3871L0.292893 15.2929L7.085 8.5L0.292893 1.70711C-0.0675907 1.34662 -0.0953203 0.779392 0.209705 0.387101Z" fill="white"/>
            </svg>
          </button>
          <div class="d-flex flex-column mod_block">
            <div class="mob_menu_item">
              <div class="mob_language">
                <div class="language"><?php echo $language; ?></div>
              </div>
            </div>
            <div class="mob_menu_item">
              <div class="mob_currency">
                <div class="currency"><?php echo $currency; ?></div>
              </div>
            </div>
            <div class="mob_menu_item">
              <div class="mob_main_menu ">
                <div class="mmenu"><?php echo $header_menu; ?></div>
              </div>
            </div>
            <div class="mob_menu_item">
              <div class="links">
                <a href="/reports/"><?php echo $text_about_us_feedbacs; ?></a>
              </div>
            </div>
            <div class="mob_menu_item">
              <div class="links">
                <a href="/auxpage_law/"><?php echo $text_law; ?></a>
              </div>
            </div>
          </div>
        </div>
        <a href="<?php echo $home; ?>" class="logo small_logo">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            	 viewBox="0 0 223.3 150" style="fill: #65bd00;" xml:space="preserve">
            <g>
            	<path class="st0" d="M93.7,134.7H22V90.6h64.4c4.8,0,8.7-3.6,8.7-8.3c0-4.8-3.8-8.5-8.7-8.5H36.1H22c-11.3,0-18.4,2.7-18.4,15.2
            		v53.7c0,5,4,8.9,9.1,8.9h80.9c4.8,0,8.7-3.6,8.7-8.3C102.3,138.5,98.5,134.7,93.7,134.7L93.7,134.7z M22,29.9h14.1h57.6
            		c4.8,0,8.7-3.6,8.7-8.3c0-4.8-3.8-8.5-8.7-8.5H12.7c-5,0-9.1,4.2-9.1,9.1C3.7,31.9,16.1,30,22,29.9L22,29.9L22,29.9z"/>
            	<path class="st0" d="M138.2,50.3c0-14.9,15.5-22.6,35.9-22.6c12.3,0,27,8.3,35.1,12.4c1,0.5,2.2,0.8,3.2,0.8c5,0,8.1-4.4,8.1-8.3
            		c0-4.8-2.9-6.7-5.6-7.9C205.2,20.2,192,11,172,11c-34.1,0-52.3,18.7-52.3,40.7c0,48.8,85.8,32.6,85.8,62
            		c0,17.3-15.3,22.8-35.3,22.8c-17.4,0-30-12.9-35.9-16.3c-1.3-0.8-2.6-0.8-3.8-0.8c-5,0-8.3,4-8.3,8.3c0,3.6,3,6.3,5.6,8.1
            		c10.1,7.3,24,17.7,44.2,17.7c31.3,0,51.7-16.3,51.7-38.7C223.7,62.4,138.2,84.7,138.2,50.3L138.2,50.3L138.2,50.3z"/>
                </g>
            </svg>
        </a>
        <a href="<?php echo $home; ?>" class="logo big_logo">
          <svg version="1.1" xmlns:x="&ns_extend;" xmlns:i="&ns_ai;" xmlns:graph="&ns_graphs;"
          	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 328.5 34.2"
          	 style="enable-background:new 0 0 328.5 34.2;" xml:space="preserve">
            <switch>
            	<foreignObject requiredExtensions="&ns_ai;" x="0" y="0" width="1" height="1">
            		<i:pgfRef  xlink:href="#adobe_illustrator_pgf">
            		</i:pgfRef>
            	</foreignObject>
            	<g i:extraneous="self">
            		<g>
            			<path d="M105.7,4.6c-8.2,0-13.4,5.3-13.5,12.7c0.2,7.4,5.3,12.8,13.5,12.8c8.2,0,13.4-5.3,13.5-12.8
            				C119.1,10,113.9,4.6,105.7,4.6L105.7,4.6z M105.7,27.1c-6.4,0-10-4.3-10-9.7c0-5.5,3.6-9.7,10-9.7c6.4,0,10,4.3,10,9.7
            				C115.7,22.9,112.1,27.1,105.7,27.1L105.7,27.1z"/>
            			<path d="M142.6,19c1.1-0.2,1.7-0.5,1.8-0.5c2.9-1.1,4.4-3.7,4.4-6.3c0-4.1-3-7.2-9.8-7.2h-8.4
            				c-0.9,0-1.6,0.7-1.6,1.6v21.7c0,0.9,0.7,1.6,1.6,1.6c0.9,0,1.7-0.7,1.7-1.6v-8.2h7.5c0.1,0.2,0.3,0.5,0.4,0.7l4.7,8.1l0,0
            				l0.1,0.2l0.3,0.4c0.5,0.6,1.3,0.7,2,0.3c0.7-0.5,1-1.4,0.5-2.1l-0.3-0.4L142.6,19L142.6,19z M138.7,17.1h-6.5v-9h6.4
            				c5.3,0,6.9,1.8,6.9,4.4C145.5,15.5,142.8,17.1,138.7,17.1L138.7,17.1z"/>
            			<path d="M160.8,11.7c0-2.7,2.8-4,6.4-4c2.2,0,4.8,1.5,6.3,2.2c0.2,0.1,0.4,0.1,0.6,0.1c0.9,0,1.4-0.8,1.4-1.5
            				c0-0.9-0.5-1.2-1-1.4c-1.7-0.8-4.1-2.4-7.6-2.4c-6.1,0-9.3,3.3-9.3,7.3c0,8.7,15.3,5.8,15.3,11.1c0,3.1-2.7,4.1-6.3,4.1
            				c-3.1,0-5.4-2.3-6.4-2.9c-0.2-0.1-0.5-0.1-0.7-0.1c-0.9,0-1.5,0.7-1.5,1.5c0,0.6,0.5,1.1,1,1.5c1.8,1.3,4.3,3.2,7.9,3.2
            				c5.6,0,9.2-2.9,9.2-6.9C176.1,13.8,160.8,17.8,160.8,11.7L160.8,11.7L160.8,11.7z"/>
            			<path d="M243,26.8h-12.8v-7.9h11.5c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5h-11.5V8H243c0.9,0,1.6-0.6,1.6-1.5
            				c0-0.9-0.7-1.5-1.6-1.5h-14.5c-0.9,0-1.6,0.7-1.6,1.6v21.6c0,0.9,0.7,1.6,1.6,1.6H243c0.9,0,1.6-0.6,1.6-1.5
            				C244.5,27.5,243.9,26.8,243,26.8L243,26.8z"/>
            			<path d="M267.4,26.8h-12.8v-7.9h11.5c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5h-11.5V8h12.8
            				c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5H253c-0.9,0-1.6,0.7-1.6,1.6v21.6c0,0.9,0.7,1.6,1.6,1.6h14.5
            				c0.9,0,1.6-0.6,1.6-1.5C269,27.5,268.3,26.8,267.4,26.8L267.4,26.8z"/>
            			<path d="M284.2,5h-6.7c-0.8,0-1.5,0.7-1.5,1.6v21.6c0,0.9,0.7,1.6,1.5,1.6h6.7c7.9,0,11.7-6.4,11.7-12.4
            				C295.9,11.4,292.1,5,284.2,5L284.2,5z M283.8,26.8h-4.8V8h4.8c5.5,0,9.1,3.8,9.1,9.4C292.9,22.9,289.4,26.8,283.8,26.8
            				L283.8,26.8z"/>
            			<path d="M77.5,19c1.1-0.2,1.7-0.5,1.8-0.5c2.9-1.1,4.4-3.7,4.4-6.3c0-4.1-3-7.2-9.8-7.2h-8.4c-0.9,0-1.6,0.7-1.6,1.6
            				v21.7c0,0.9,0.7,1.6,1.6,1.6c0.9,0,1.7-0.7,1.7-1.6v-8.2h7.5c0.1,0.2,0.3,0.5,0.4,0.7l4.7,8.1l0,0l0.1,0.2l0.3,0.4
            				c0.5,0.6,1.3,0.7,2,0.3c0.7-0.5,1-1.4,0.5-2.1l-0.3-0.4L77.5,19L77.5,19z M73.6,17.1h-6.5v-9h6.4c5.3,0,6.9,1.8,6.9,4.4
            				C80.4,15.5,77.7,17.1,73.6,17.1L73.6,17.1z"/>
            			<path d="M47.3,19c1.1-0.2,1.7-0.5,1.8-0.5c2.9-1.1,4.4-3.7,4.4-6.3c0-4.1-3-7.2-9.8-7.2h-8.4c-0.9,0-1.6,0.7-1.6,1.6
            				v21.7c0,0.9,0.7,1.6,1.6,1.6s1.7-0.7,1.7-1.6v-8.2h7.5c0.1,0.2,0.3,0.5,0.4,0.7l4.7,8.1l0,0l0.1,0.2l0.3,0.4
            				c0.5,0.6,1.3,0.7,2,0.3c0.7-0.5,1-1.4,0.5-2.1l-0.3-0.4L47.3,19L47.3,19z M43.5,17.1h-6.5v-9h6.4c5.3,0,6.9,1.8,6.9,4.4
            				C50.2,15.5,47.5,17.1,43.5,17.1L43.5,17.1z"/>
            			<path d="M204.1,11.7c0-2.7,2.8-4,6.4-4c2.2,0,4.8,1.5,6.3,2.2c0.2,0.1,0.4,0.1,0.6,0.1c0.9,0,1.4-0.8,1.4-1.5
            				c0-0.9-0.5-1.2-1-1.4c-1.7-0.8-4.1-2.4-7.6-2.4c-6.1,0-9.3,3.3-9.3,7.3c0,8.7,15.3,5.8,15.3,11.1c0,3.1-2.7,4.1-6.3,4.1
            				c-3.1,0-5.4-2.3-6.4-2.9c-0.2-0.1-0.5-0.1-0.7-0.1c-0.9,0-1.5,0.7-1.5,1.5c0,0.6,0.5,1.1,1,1.5c1.8,1.3,4.3,3.2,7.9,3.2
            				c5.6,0,9.2-2.9,9.2-6.9C219.4,13.8,204.1,17.8,204.1,11.7L204.1,11.7L204.1,11.7z"/>
            			<path d="M305.8,11.7c0-2.7,2.8-4,6.4-4c2.2,0,4.8,1.5,6.3,2.2c0.2,0.1,0.4,0.1,0.6,0.1c0.9,0,1.4-0.8,1.4-1.5
            				c0-0.9-0.5-1.2-1-1.4c-1.7-0.8-4.1-2.4-7.6-2.4c-6.1,0-9.3,3.3-9.3,7.3c0,8.7,15.3,5.8,15.3,11.1c0,3.1-2.7,4.1-6.3,4.1
            				c-3.1,0-5.4-2.3-6.4-2.9c-0.2-0.1-0.5-0.1-0.7-0.1c-0.9,0-1.5,0.7-1.5,1.5c0,0.6,0.5,1.1,1,1.5c1.8,1.3,4.3,3.2,7.9,3.2
            				c5.6,0,9.2-2.9,9.2-6.9C321.1,13.8,305.8,17.8,305.8,11.7L305.8,11.7L305.8,11.7z"/>
            			<path d="M21.2,26.8H8.4v-7.9h11.5c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5h-9H8.4c-2,0-3.3,0.5-3.3,2.7v9.6
            				c0,0.9,0.7,1.6,1.6,1.6h14.5c0.9,0,1.6-0.6,1.6-1.5C22.8,27.5,22.1,26.8,21.2,26.8L21.2,26.8z M8.4,8h2.5h10.3
            				c0.9,0,1.6-0.6,1.6-1.5c0-0.9-0.7-1.5-1.6-1.5H6.8C5.8,5,5.1,5.8,5.1,6.6C5.1,8.4,7.4,8,8.4,8L8.4,8L8.4,8z"/>
            		</g>
            	</g>
            </switch>
            </svg>
        </a>
        <div class="right_block d-flex flex-row">
          <div class="language_currency">
            <div class="language"><?php echo $language; ?></div>
            <div class="currency"><?php echo $currency; ?></div>
          </div>
          <div class="fast_order">
            <a href="https://docs.google.com/forms/d/e/1FAIpQLScVy9xO7r0wmuavHw5pDUUfxqPfEu6eF6jOcL7YuummY1n87g/viewform"  class="menu_item" target="_blank">
              <i class="far fa-clock"></i>
              <span><?php echo $text_fast_order; ?></span>
            </a>
          </div>
          <div class="contact">
            <button type="button" class="menu_item" data-toggle="modal" data-target="#modalcontact">
              <svg viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.01" x="1" width="22" height="22" fill="#D8D8D8"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.4499 5.3707C8.4499 7.52461 6.70381 9.2707 4.5499 9.2707C4.14797 9.2707 3.76025 9.2099 3.39541 9.097C5.07832 13.4091 8.38339 16.8382 12.5 18.6101C12.4037 18.271 12.3521 17.913 12.3521 17.5429C12.3521 15.389 14.0982 13.6429 16.2521 13.6429C18.406 13.6429 20.1521 15.389 20.1521 17.5429C20.1521 19.6968 18.406 21.4429 16.2521 21.4429C16.1444 21.4429 16.0378 21.4386 15.9323 21.43C15.8298 21.4477 15.7225 21.4479 15.6143 21.4277C8.67001 20.1364 3.02766 14.8458 1.09666 7.88252C0.939524 7.31587 0.807829 6.74104 0.702147 6.15947C0.688162 6.08251 0.684411 6.00606 0.689925 5.93152C0.663552 5.74839 0.649902 5.56114 0.649902 5.3707C0.649902 3.21679 2.39599 1.4707 4.5499 1.4707C6.70381 1.4707 8.4499 3.21679 8.4499 5.3707ZM2.4499 5.3707C2.4499 6.5305 3.3901 7.4707 4.5499 7.4707C5.7097 7.4707 6.6499 6.5305 6.6499 5.3707C6.6499 4.21091 5.7097 3.2707 4.5499 3.2707C3.3901 3.2707 2.4499 4.21091 2.4499 5.3707ZM16.2521 19.6429C15.0923 19.6429 14.1521 18.7027 14.1521 17.5429C14.1521 16.3831 15.0923 15.4429 16.2521 15.4429C17.4119 15.4429 18.3521 16.3831 18.3521 17.5429C18.3521 18.7027 17.4119 19.6429 16.2521 19.6429Z" fill="white"/>
                <path d="M17.9001 9.0001C17.9001 6.2939 15.7063 4.1001 13.0001 4.1001C12.503 4.1001 12.1001 4.50304 12.1001 5.0001C12.1001 5.49715 12.503 5.9001 13.0001 5.9001C14.7122 5.9001 16.1001 7.28801 16.1001 9.0001C16.1001 9.49715 16.503 9.9001 17.0001 9.9001C17.4972 9.9001 17.9001 9.49715 17.9001 9.0001Z" fill="#65BD00"/>
                <path d="M21.9001 9.0001C21.9001 4.08476 17.9154 0.100098 13.0001 0.100098C12.503 0.100098 12.1001 0.503041 12.1001 1.0001C12.1001 1.49715 12.503 1.9001 13.0001 1.9001C16.9213 1.9001 20.1001 5.07888 20.1001 9.0001C20.1001 9.49715 20.503 9.9001 21.0001 9.9001C21.4972 9.9001 21.9001 9.49715 21.9001 9.0001Z" fill="#65BD00"/>
              </svg>
            </button>
          </div>
          <div class="login">
            <div class="dropdown_block">
              <button id="drop_menu_acc" class="menu_item" type="button" >
                <?php if($logged != null){ ?>
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 34 33.4" style="enable-background:new 0 0 34 33.4;" xml:space="preserve">
                    <g><path style="fill:none;stroke:#FFFFFF;stroke-width:2.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M22.9,30.1c-1.7,0.8-3.7,1.2-5.7,1.2c-7.7,0-14-6.3-14-14s6.3-14,14-14s14,6.3,14,14c0,1.3-0.2,2.6-0.5,3.8"/>
                      <circle class="st1" style="fill:none;stroke:#65BD00;stroke-width:2.5;stroke-miterlimit:10;" cx="17.3" cy="12.4" r="2.9"/>
                      <path style="fill:none;stroke:#FFFFFF;stroke-width:2.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" d="M7.4,27.1c0,0,3.5-8.1,9.8-8.1c1.2,0,2.3,0.3,3.3,0.7c0.9,0.4,1.7,0.9,2.4,1.5"/></g>
                    <g><line style="fill:none;stroke:#65BD00;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="27" y1="21.8" x2="27" y2="29.8"/>
                      <line style="fill:none;stroke:#65BD00;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" x1="23" y1="25.8" x2="31" y2="25.8"/>
                    </g>
                  </svg>
                  <?php }else{ ?>
                    <svg viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect opacity="0.01" y="1" width="22" height="22" fill="#D8D8D8"/>
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M14.6501 8.2501C14.6501 9.98979 13.2398 11.4001 11.5001 11.4001C9.7604 11.4001 8.3501 9.98979 8.3501 8.2501C8.3501 6.5104 9.7604 5.1001 11.5001 5.1001C13.2398 5.1001 14.6501 6.5104 14.6501 8.2501ZM10.1501 8.2501C10.1501 8.99568 10.7545 9.6001 11.5001 9.6001C12.2457 9.6001 12.8501 8.99568 12.8501 8.2501C12.8501 7.50451 12.2457 6.9001 11.5001 6.9001C10.7545 6.9001 10.1501 7.50451 10.1501 8.2501Z" fill="#65BD00"/>
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M11.5001 22.9001C17.7961 22.9001 22.9001 17.7961 22.9001 11.5001C22.9001 5.20405 17.7961 0.100098 11.5001 0.100098C5.20405 0.100098 0.100098 5.20405 0.100098 11.5001C0.100098 17.7961 5.20405 22.9001 11.5001 22.9001ZM4.38819 17.9485C2.84213 16.2444 1.9001 13.9823 1.9001 11.5001C1.9001 6.19816 6.19816 1.9001 11.5001 1.9001C16.802 1.9001 21.1001 6.19816 21.1001 11.5001C21.1001 13.9823 20.1581 16.2444 18.612 17.9485C17.7226 14.8596 14.8752 12.6001 11.5001 12.6001C8.125 12.6001 5.27764 14.8596 4.38819 17.9485ZM5.94013 19.3271C7.50898 20.4435 9.42786 21.1001 11.5001 21.1001C13.5723 21.1001 15.4912 20.4435 17.0601 19.3271C16.7276 16.5516 14.3651 14.4001 11.5001 14.4001C8.63508 14.4001 6.27258 16.5516 5.94013 19.3271Z" fill="white"/>
                    </svg>
                <?php } ?>
              </button>
              <div class="drop_menu_acc hidden_menu">
                <?php if($logged != null){ ?>
                <a href="<?php echo $account; ?>" class="green_button dropdown-item">
                  <?php echo $text_account; ?>
                </a>
                <a href="<?php echo $logout; ?>" class="logout dropdown-item">
                  <?php echo $text_logout; ?>
                </a>
                <?php }else{ ?>
                <a class="input green_button dropdown-item" href="<?php echo $login; ?>">
                  <?php echo $text_login; ?>
                </a>
                <a href="<?php echo $register; ?>" class="registration green_button dropdown-item">
                  <?php echo $text_register; ?>
                </a>
                <?php } ?>
              </div>
            </div>
          </div>
          <div class="cart d-flex">
            <?php echo $cart; ?>
          </div>
        </div>
        <div class="menu_desc">
          <div class="mmenu"><?php echo $header_menu; ?></div>
        </div>
      </div>
      <div class="top d-flex flex-row justify-content-between">
        <div class="search d-flex">
          <form action="/search/" method="get" id="autosearch">
            <input type="text" id="search" name="search" autocomplete="off">
            <ul class="result-search-autocomplete"></ul>
            <button type="submit" class="button_search">
                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect opacity="0.01" x="1" y="0.0454102" width="21" height="21" fill="#D8D8D8"/>
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M17.4878 16.2728C18.9205 14.566 19.7833 12.3648 19.7833 9.96208C19.7833 4.53798 15.3862 0.140869 9.96212 0.140869C4.53802 0.140869 0.140911 4.53798 0.140911 9.96208C0.140911 15.3862 4.53802 19.7833 9.96212 19.7833C12.3649 19.7833 14.5661 18.9205 16.2728 17.4877L19.7381 20.953C20.0736 21.2885 20.6176 21.2885 20.9531 20.953C21.2886 20.6175 21.2886 20.0736 20.9531 19.7381L17.4878 16.2728ZM15.8592 15.5194C17.2269 14.0687 18.0652 12.1133 18.0652 9.96208C18.0652 5.4869 14.4373 1.85905 9.96212 1.85905C5.48694 1.85905 1.85909 5.4869 1.85909 9.96208C1.85909 14.4373 5.48694 18.0651 9.96212 18.0651C12.1133 18.0651 14.0687 17.2268 15.5195 15.8591C15.5577 15.7896 15.6063 15.7242 15.6653 15.6652C15.7242 15.6063 15.7896 15.5577 15.8592 15.5194Z"/>
                </svg>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  function headerResize() {
    if ($(window).width() > '991') {
        $('.header.header_mob').removeClass('header_mob').addClass('header_desc');
        $('.bg_header').removeClass('bg_header_mob');
      }else{
        $('.header.header_desc').removeClass('header_desc').addClass('header_mob');
        $('.bg_header').addClass('bg_header_mob');
      };
  };
  headerResize();
  $(window).on('resize', headerResize);
</script>
