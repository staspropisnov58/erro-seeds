<div class="f_title text-right"><?php echo $text_currency; ?></div>

<div class="cpt_currency_selection dropdown_block">
    <div id="curr_trig" class="drop_trig curr counter_right">
    <?php foreach ($currencies as $currency) { ?>
          <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?>
             <label><?php echo $currency['code']; ?></label>
          <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?>
              <label><?php echo $currency['code']; ?></label>
          <?php } ?>
      <?php } ?>
    </div>
    <div class="all_var hidden_menu" aria-labelledby="curr_trig">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="currency" class="d-flex">
          <?php foreach ($currencies as $currency){ ?>
            <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?>
              <div class="curr active counter_right">
                <label><input type="radio" value="<?php echo $currency['code']; ?>" name="currency" checked >
                <?php echo $currency['code']; ?></label>
              </div>
            <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?>
              <div class="curr active counter_right">
                <label><input type="radio" value="<?php echo $currency['code']; ?>" name="currency" checked>
                <?php echo $currency['code']; ?></label>
              </div>
            <?php }else{ ?>
              <div class="curr counter_right">
                <label><input type="radio" value="<?php echo $currency['code']; ?>" name="currency">
                <?php echo $currency['code']; ?></label>
              </div>
            <?php } ?>
          <?php } ?>
        <input type="hidden" name="code"></input>
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
      </form>
    </div>
  </div>
  <div class="exchange">
    <div class="texc">
      <?php foreach($currencies as $currency){ ?>
        <?php if($config_currency == $currency['code']){ ?>
        <?php }else{ ?>
      <div>
        <img src = "catalog/view/theme/old/images/<?php echo $currency['code'] . '.png'; ?>" onkeypress="">
        <span class = "<?php echo $currency['code']; ?>">
        </span>
        <?php echo '1 Грн = ' . $currency['value'] .' '. $currency['code']; ?>
      </div>
    <?php } ?>
  <?php } ?>
    </div>
  </div>
