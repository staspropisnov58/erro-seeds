<?php echo $header; ?>
<div class="container">
  <div class="success_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div id="content" class="mx-auto w-100">
        <div class="text-center">
          <div class="col-12 mx-auto">
            <div class="success_block">
              <?php echo $text_message; ?>
              <?php if(isset($text_link)){ ?>
              <?php echo $text_link; ?>
              <?php } ?>
            </div>
            <div class="d-flex align-items-center justify-content-center">
              <a href="<?php echo $continue; ?>" class="green_button"><?php echo $button_continue; ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
