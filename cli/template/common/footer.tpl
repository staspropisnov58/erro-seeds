<div class="footer">
  <div class="copyright container d-flex flex-column">
    <div class="row mb-sm-4">
      <div class="footer_sub col-12 col-md-3">
        <div class="language d-flex flex-wrap ">
          <?php echo $language; ?>
        </div>
      </div>
      <div class="footer_sub col-12 col-md-4">
        <div class="currency d-flex align-items-md-end flex-column ">
          <?php echo $currency; ?>
        </div>
      </div>
      <div class="footer_sub col-12 col-md-5 flex-column align-items-md-end">
        <div class="f_title">Мы в соцсетях</div>
        <div class="">
          <div class="block_counter d-flex flex-row align-items-lg-end">
            <a href="#" id="instagram" class="counter_right hidli" target="_blank" rel="https://www.instagram.com/errorsseeds_ua/ ">
              <img src="/catalog/view/theme/old/images/instagram.svg" alt="instagram">
            </a>
            <a href="viber://tel:80930000849" class="counter_right">
              <img src="/catalog/view/theme/old/images/viber.svg" alt="viber">
            </a>
            <a target="_blank" href="#" class="counter_right hidli" rel="https://www.facebook.com/esuacom/">
              <img src="/catalog/view/theme/old/images/facebook.svg" alt="ES facebook">
            </a>
            <a target="_blank" href="#" class="counter_right hidli" rel="https://vk.com/esinfoo ">
              <img src="/catalog/view/theme/old/images/vk.svg" alt="ES vk">
            </a>
            <a target="_blank" href="#" class="counter_right hidli" rel="https://t.me/Errors_Seeds">
              <img src="/catalog/view/theme/old/images/telegram.svg" alt="ES telegram">
            </a>
            <a target="_blank" href="#" class="counter_right hidli" rel="https://www.youtube.com/channel/UCWk-ZUoQ82Y2cJWkTS_3kLQ">
              <img src="/catalog/view/theme/old/images/youtube.svg" alt="ES YouTube">
            </a>
            <div></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="footer_sub col-12">
        <p>
          <?php echo $text_footer; ?>
        </p>
        <p>
          <?php echo $text_ES . date('Y')?>
          </a>
        </p>
      </div>
    </div>
  </div>
</div>

<p id="back-top">
  <a href="#top"><i class="fas fa-chevron-up"></i></a>
</p>

<div class="modal fade" id="modalform" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
  </div>
</div>
<div class="modal fade" id="modalmessage" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-body message col-8 mx-auto" style="">
    </div>
  </div>
</div>
</body>
</html>
