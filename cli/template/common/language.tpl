<div class="f_title"><?php echo $text_language; ?></div>
<div class="cpt_currency_selection dropdown_block">
    <div id="lang_trig" class="drop_trig lang counter_right">
      <?php foreach ($languages as $language){ ?>
        <?php if ($language['code'] === $code) { ?>
          <img src="catalog/language/<?php echo $language['directory']; ?>/<?php echo $language['image']; ?>">
        <?php } ?>
      <?php } ?>
    </div>
    <div class="all_var hidden_menu" aria-labelledby="lang_trig">
    <?php foreach ($languages as $language){ ?>
      <div class="select-lang">
        <?php if ($language['code'] === $code) { ?>
          <div class="lang active counter_right">
            <img src="catalog/language/<?php echo $language['directory']; ?>/<?php echo $language['image']; ?>">
          </div>
        <?php } else { ?>
          <div class="lang counter_right">
            <a href="<?php echo $redirects[$language['code']]; ?>">
              <img src="catalog/language/<?php echo $language['directory']; ?>/<?php echo $language['image']; ?>">
            </a>
          </div>
        <?php } ?>
      </div>
    <?php } ?>
    <input type="hidden" name="code"></input>
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
  </div>
</div>
