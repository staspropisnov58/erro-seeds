<div id="slides">
    <div class="slides_container owl-carousel">
      <?php foreach ($banners as $banner) { ?>
      <div class="slide">
        <a href="<?php echo $banner['link']; ?>" target="_blank">
          <img alt="<?php echo $banner['title'];?>"  data-src="<?php echo $banner['image']; ?>" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 2'%3E%3C/svg%3E" class="owl-lazy" height="224px"/>
        </a>
      </div>
    <?php } ?>
    </div>
  </div>
