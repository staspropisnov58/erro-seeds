<?php if ($poll_question) { ?>
<div id="pesky-poll" class="p-2 dialog-window pesky-window">
  <img src="<?php echo $image; ?>">
  <div class="d-flex flex-row justify-content-between">
    <p class="pesky-title"><?php echo $poll_question; ?></p>
  </div>
  <div class="d-flex flex-row p-3 justify-content-around" id="pesky-buttons">
    <button type="button" class="green_button pesky-button" onclick="votePoll('index.php?route=module/pesky_window/votePoll&vote=yes');"><?php echo $poll_yes; ?></button>
    <button type="button" class="green_button pesky-button" onclick="votePoll('index.php?route=module/pesky_window/votePoll&vote=no');"><?php echo $poll_no; ?></button>
  </div>
</div>
<?php } ?>
<div id="pesky-product" class="d-flex flex-row p-2 dialog-window pesky-window"
data-min-timeout="<?php echo $min_timeout; ?>" data-max-timeout="<?php echo $max_timeout; ?>"
data-first-timeout="<?php echo $first_timeout; ?>">
  <div class="col-3 p-0">
    <img src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 3'%3E%3C/svg%3E" alt="pesky-product"
    width="<?php echo $img_width; ?>" height="<?php echo $img_height; ?>">
  </div>
  <div class="col-9 justify-content-between">
    <button type="button" class="close" data-dismiss="dialog-window" aria-label="Close">
      <svg width="10" height="10" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">            <path d="M0.209705 0.387101L0.292893 0.292893C0.653377 -0.0675907 1.22061 -0.0953203 1.6129 0.209705L1.70711 0.292893L8.5 7.085L15.2929 0.292893C15.6534 -0.0675907 16.2206 -0.0953203 16.6129 0.209705L16.7071 0.292893C17.0676 0.653377 17.0953 1.22061 16.7903 1.6129L16.7071 1.70711L9.915 8.5L16.7071 15.2929C17.0676 15.6534 17.0953 16.2206 16.7903 16.6129L16.7071 16.7071C16.3466 17.0676 15.7794 17.0953 15.3871 16.7903L15.2929 16.7071L8.5 9.915L1.70711 16.7071C1.34662 17.0676 0.779392 17.0953 0.387101 16.7903L0.292893 16.7071C-0.0675907 16.3466 -0.0953203 15.7794 0.209705 15.3871L0.292893 15.2929L7.085 8.5L0.292893 1.70711C-0.0675907 1.34662 -0.0953203 0.779392 0.209705 0.387101Z" fill="#fff"></path>
          </svg>
    </button>
    <div class="d-flex flex-column">
      <p class="pesky-title"><?php echo $heading_title; ?></p>
      <div class="pesky-product-link">
        <a></a>
      </div>
    </div>
  </div>
</div>

