<div class="menu">
    <ul class="header_menu">
        <li><a href="/ua/">Головна</a></li>
        <li>
        	  <p class="drop">Про магазин <i class="fas fa-chevron-down"></i></p>
            <ul class="drop_hid">
              <div class="m_it">
                <li><a href="/ua/auxpage_about/">Про Нас</a></li>
                <li><a href="/ua/feedback/">Контакти</a></li>
                <li><a href="/ua/auxpage_schedule/">Графік роботи</a></li>
                <li><a href="/ua/auxpage_faq/">Часті питання</a></li>
              </div>
              <div class="m_it">
                <li><a href="/ua/auxpage_shipping/">Оплата</a></li>
                <li><a href="/ua/auxpage_delivery/">Доставка</a></li>                
                <li><a href="/ua/reports/">Відгуки</a></li>
                <li><a href="https://jahforum.net/">Форум</a></li>
              </div>
              <div class="m_it">
                <li><a href="/ua/auxpage_kak-kupit-semena-konopli/">Як купити насiння коноплi</a></li>
                <li>
                	<div class="cont" style="cursor: default;">Полiтика замiни та повернення<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                    <ul class="accordion_block">
                      <li><a href="/ua/auxpage_indiv/">Для приватних осiб</a></li>
                      <li><a href="/ua/auxpage_for-shop/">Для магазинiв</a></li>
                    </ul>
                </li>
              </div>
            </ul>
        </li>
        <li>
        	  <p class="drop">Семена <i class="fas fa-chevron-down"></i></p>
            <ul class="drop_hid">
              <div class="m_it">
                <li>
                	<div class="cont"><a href="/ua/category/errors-seeds/">Errors Seeds Silver</a><div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                    <ul class="accordion_block">
                        <li><a href="/ua/category/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
                        <li><a href="/ua/category/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
                        <li><a href="/ua/category/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
                        <li><a href="/ua/category/errors-seeds-auto-fem/">Errors Seeds Silver Auto Fem</a></li>
                        <li><a href="/ua/category/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
                    </ul>
                </li>
                <li>
                	<div class="cont"><a href="/ua/category/errors-seeds-import/">Errors Seeds Gold</a><div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                    <ul class="accordion_block">
                        <li><a href="/ua/category/errors-seeds-import-feminised/">Errors Seeds Gold Feminised</a></li>
                        <li><a href="/ua/category/errors-seeds-import-auto-fem/">Errors Seeds Gold Auto Fem</a></li>
                    </ul>
                </li>
                <li>
                  <div class="cont">
                    <a href="/ua/category/komplekty-es/">Комплекти Errors Seeds</a>
                  </div>
                </li>
              </div>
              <div class="m_it">
                <li>
                  <div class="cont">
                    <a href="/ua/category/cbd-sorta/">CBD сорти</a>
                  </div>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">Іспанія<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/ua/category/victory-seeds/">Victory Seeds</a></li>
                    <li><a href="/ua/category/sweet-seeds/">Sweet Seeds</a></li>
                    <li><a href="/ua/category/mandala-seeds-/">Mandala Seeds</a> </li>
                    <li><a href="/ua/category/kannabia/">Kannabia</a> </li>
                    <li><a href="/ua/category/pyramid-seeds/">Pyramid Seeds</a> </li>
                    <li><a href="/ua/category/drugie/dinafem/">Dinafem</a> </li>
                  </ul>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">Англія<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/ua/category/bulk-seed-bank/">Bulk Seed Bank</a></li>
                  </ul>
                </li>
              </div>
              <div class="m_it">
                <li>
                  <div class="cont"><a href="/ua/category/semena-iz-gollandii/">Голандія</a><div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/ua/category/dutch-passion-seeds/">Dutch Passion seeds</a></li>
                    <li><a href="/ua/category/green-house-seeds/">Green House Seeds</a></li>
                    <li><a href="/ua/category/vision-seeds/">Vision Seeds</a></li>
                    <li><a href="/ua/category/neuroseeds/">Neuroseeds</a></li>
                    <li><a href="/ua/category/barneys-farm/">Barneys Farm</a> </li>
                  </ul>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">США<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/ua/category/humboldt/">Humboldt</a></li>
                  </ul>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">Чехія<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/ua/category/carpathians-seeds/">Carpathians seeds</a></li>
                  </ul>
                </li>
              </div>
            </ul>
        </li>
        <li>
          <p class="drop">Обладнання <i class="fas fa-chevron-down"></i></p>
          <ul class="drop_hid">
            <div class="m_it">
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/">Все обладнання</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/ventilyaciya/">Вентиляція</a></li>
            </div>
            <div class="m_it">
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/izmeritelnaya-tehnika/">Вимірювальна техніка</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/hidroponnue-sistemu/">Гідропонні системи</a></li>
            </div>
            <div class="m_it">
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/osveshenie/">Освітлення</a></li>
              <li><a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/upravleniye-i-avtomatika/">Управління та автоматика</a></li>
            </div>
          </ul>
        </li>
      	<li>
          <p class="drop">Добрива <i class="fas fa-chevron-down"></i></p>
          <ul class="drop_hid">
            <div class="m_it">
              <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/">Всі добрива</a></li>
            </div>
            <div class="m_it">
              <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/well-grow-by-errors-seeds/">Well Grow by Errors Seeds</a></li>
            </div>
            <div class="m_it">
              <li><a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/biostimulyatoru/">БІОСтимулятори</a></li>
            </div>
          </ul>
        </li>
        <li><a href="/ua/auxpage_action_list/"><span style="color: red; font-weight: bold;">SALE</span></a></li>
        <li><a rel="nofollow" href="https://errors-seeds.com.ua/blog/">Новини</a></li>
        <li>
        	  <p class="drop">Співробітництво <i class="fas fa-chevron-down"></i></p>
            <ul class="drop_hid">
              <div class="m_it">
                <li><a href="/ua/auxpage_sotrudnichestvo/">О співробітництві</a></li>
                <li><a href="/ua/auxpage_referalnaya_programma/">Реферальна програма</a></li>
                <li><a href="/ua/auxpage_opt/">Опт</a></li>
              </div>
              <div class="m_it">
                <li><a href="/ua/auxpage_dropshipping/">Дропшиппінг</a></li>
                <li><a href="/ua/auxpage_interesnyy_kontent/">Цікавий контент</a></li>
                <li><a href="/ua/auxpage_smm/">SMM </a></li>
              </div>
              <div class="m_it">
                <li><a href="/ua/auxpage_distribjucija/">Дистриб'юція</a></li>
                <li><a href="/ua/auxpage_vakansii/">Вакансії</a></li>
              </div>
            </ul>
        </li>
    </ul>
</div>
