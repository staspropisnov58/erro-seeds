<div class="menu desktop_menu order-5">
  <ul class="sf-menu header_menu d-flex flex-md-row flex-column">
    <li class="current"> <a href="/ua/"> Головна </a></li>
    <li class="current"> <a href="#">Про магазин</a>
      <ul>
        <li> <a href="/ua/auxpage_about/"> Про нас </a></li>
		<li> <a rel="nofollow" href="/ua/blog/"> Новини </a></li>
        <li> <a href="/ua/auxpage_faq/"> Часті питання </a></li>
        <li> <a href="/ua/auxpage_schedule/"> Графік роботи </a></li>
        <li> <a href="/ua/auxpage_delivery/"> Доставка </a></li>
        <li> <a href="/ua/auxpage_shipping/"> Оплата </a></li>
        <li> <a href="/ua/auxpage_adress/"> Як нас знайти </a></li>
        <li> <a href="/ua/auxpage_kak-kupit-semena-konopli/"> Як купити насіння коноплі </a></li>
        <li> <a href="/ua/feedback/"> Контакти </a></li>
        <li>
          <a href="/ua/auxpage_nashy-distribjutory/">Наші дистриб'ютори<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/auxpage_master-seed/">Master Seed</a></li>
            <li><a href="/ua/auxpage_seed-bank-shop/">Seed Bank</a></li>
            <li><a href="/ua/auxpage_good-seed/">Good Seed</a></li>
            <li><a href="/ua/auxpage_ligalaiz-groop/">Ligalaiz Groop</a></li>
            <li><a href="/ua/auxpage_sunny-seeds/">Sunny Seeds</a></li>
  <li><a href="/ua/auxpage_tengo-seed/">Tengo Seed</a></li>
          </ul>
        </li>
        <li> <a href="/ua/auxpage_polit/"> Політика заміни і повернення<span class="arrow">►</span></a>
          <ul>
            <li> <a href="/ua/auxpage_indiv/"> Для приватних осіб </a></li>
            <li> <a href="/ua/auxpage_for-shop/"> Для магазинів </a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li> <a href="#"> Насіння </a>
      <ul>
        <li> <a href="/ua/category/errors-seeds/">Errors Seeds Silver<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/category/errors-seeds/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
            <li><a href="/ua/category/errors-seeds/errors-seeds-auto-fem/">Errors Seeds Silver Auto Fem</a></li>
            <li><a href="/ua/category/errors-seeds/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
			<li><a href="/ua/category/errors-seeds/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
			<li><a href="/ua/category/errors-seeds/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/errors-seeds-import/">Errors Seeds Gold<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/category/errors-seeds-import/errors-seeds-import-feminised/">Errors Seeds Gold Feminised</a></li>
            <li><a href="/ua/category/errors-seeds-import/errors-seeds-import-auto-fem/">Errors Seeds Gold Auto Fem</a></li>
          </ul>
        </li>
		<li><a href="/ua/category/komplekty-es/">Комплекти насіння від Errors Seeds</a></li>
        <li> <a href="/ua/category/carpathians-seeds/">Carpathians seeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/category/carpathians-seeds/carpathians-seeds-auto-fem/">Carpathians seeds Auto Fem</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/neuroseeds/">Neuroseeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/category/neuroseeds/neuroseeds-auto-fem/">Neuroseeds auto fem</a></li>
            <li><a href="/ua/category/neuroseeds/neuroseeds-fem/">Neuroseeds fem</a></li>
          </ul>
        </li>
        <li><a href="/ua/category/cbd-sorta/">CBD сорти</a></li>
        <li> <a href="/ua/category/victory-seeds/">Victory Seeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/category/victory-seeds/victory-seeds-auto-fem/">Victory Seeds Auto Fem</a></li>
            <li><a href="/ua/category/victory-seeds/victory-seeds-fem/">Victory Seeds Fem</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/bulk-seed-bank/">Bulk Seed Bank<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/category/bulk-seed-bank/bulk-seed-bank-auto-fem/">Bulk Seed Bank Auto Fem</a></li>
            <li><a href="/ua/category/bulk-seed-bank/bulk-seed-bank-fem/">Bulk Seed Bank Fem</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/drugie/green-house-seeds/">Green House Seeds<span class="arrow">►</span></a>
          <ul>
            <li><a href="/ua/category/drugie/green-house-seeds/green-house-seeds-auto-fem/">Green House Seeds Auto Fem</a></li>
            <li><a href="/ua/category/drugie/green-house-seeds/green-house-seeds-feminised/">Green House Seeds Feminised</a></li>
          </ul>
        </li>
		<li><a href="/ua/category/buddha-seeds/">Buddha Seeds</a></li>
        <li><a href="/ua/category/mandala-seeds-/">Mandala Seeds</a></li>
        <li><a href="/ua/category/kannabia/">Kannabia</a></li>
        <li><a href="/ua/category/pyramid-seeds/">Pyramid Seeds</a></li>
        <li><a href="/ua/category/humboldt/">Humboldt</a></li>
        <li><a href="/ua/category/gsr/">Green Silk Road Seeds</a>
      </li>
         <li><a href="/ua/category/dinafem/">Dinafem</a>
      </li>
           <li>
        <a href="/ua/category/semena-iz-gollandii/">Насіння з Голландії<span class="arrow">►</span></a>
        <ul>
          <li><a href="/ua/category/semena-iz-gollandii/barneys-farm/">Barneys Farm</a></li>
          <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/">Dutch Passion<span class="arrow">►</span></a>
            <ul>
              <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/dutch-passion-seeds-auto-fem/">Dutch Passion Seeds Auto Fem</a></li>
            </ul>
          </li>
          <li> <a href="/ua/category/semena-iz-gollandii/vision-seeds/">Vision Seeds <span class="arrow">►</span></a>
            <ul>
              <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/vision-seeds-auto-fem/">Vision Seeds Auto Fem</a></li>
              <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/vision-seeds-feminised/">Vision Seeds Feminised</a></li>
            </ul>
          </li>
        </ul>
      </li>
        <li> <a href="/ua/category/drugie/">Інші<span class="arrow">►</span></a>
          <ul>
            <li> <a href="/ua/category/drugie/shortstuff-seeds/">Shortstuff Seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/ua/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-fem/">Shortstuff Seeds fem</a></li>
                <li><a href="/ua/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-reg/">Shortstuff Seeds reg</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/sweet-seeds/">Sweet Seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/ua/category/sweet-seeds/sweet-seeds-auto-fem/">Sweet Seeds Auto Fem</a></li>
                <li><a href="/ua/category/sweet-seeds/sweet-seeds-feminised/">Sweet Seeds Feminised</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/flash-seeds/">Flash-seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/ua/category/drugie/flash-seeds/flash-seeds-auto-fem/">Flash-seeds Auto Fem</a></li>
                <li><a href="/ua/category/drugie/flash-seeds/flash-seeds-regular/">Flash-seeds Regular</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/grass-o-matic-seeds/">Grass-o-matic seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/ua/category/drugie/grass-o-matic-seeds/grass-o-maitc-seeds-auto-fem/">Grass-o-matic seeds Auto Fem</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/joint-doctor-seeds/">Joint Doctor seeds<span class="arrow">►</span></a>
              <ul>
                <li><a href="/ua/category/drugie/joint-doctor-seeds/joint-doctor-seeds-regular/">Joint Doctor seeds Regular</a></li>
                <li><a href="/ua/category/drugie/joint-doctor-seeds/joint-doctor-seeds-auto-fem/">Joint Doctor seeds Auto Fem</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/seedsman/">Seedsman<span class="arrow">►</span></a>
              <ul>
                <li><a href="/ua/category/drugie/seedsman/seedsman-auto-fem/">Seedsman Auto Fem</a></li>
                <li><a href="/ua/category/drugie/seedsman/seedsman-regular/">Seedsman Regular</a></li>
                <li><a href="/ua/category/drugie/seedsman/seedsman-feminised/">Seedsman Feminised</a></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li> <a href="/ua/auxpage_action_list"> Акції </a></li>
    <li> <a rel="nofollow" href="/ua/blog/"> Новини </a></li>
    <li> <a href="/ua/auxpage_sotrudnichestvo/"> Співпраця </a>
      <ul>
        <li><a href="/ua/auxpage_referalnaya_programma/">Реферальна програма</a></li>
        <li><a href="/ua/auxpage_opt/">Опт</a></li>
        <li><a href="/ua/auxpage_dropshipping/">Дропшиппінг</a></li>
        <li><a href="/ua/auxpage_interesnyy_kontent/">Цікавий контент</a></li>
        <li><a href="/ua/auxpage_smm/">SMM </a></li>
        <li><a href="/ua/auxpage_distribjucija/">Дистриб'юція</a></li>
        <li><a href="/ua/auxpage_vakansii/">Вакансії</a></li>
        <li><a href="/ua/pricelist/?in_stock=yes">Прайс-лист</a></li>
      </ul>
    </li>
  </ul>
</div>
<div class="menu mobile_menu order-1 col-1">
  <div class="navBurger" role="navigation" id="navToggle"></div>
  <div class="overlay">
    <nav class="overlayMenu js-menu">
      <ul role="menu">
        <li> <a href="/ua/auxpage_about/"> Про нас </a></li>
        <li> <a href="/ua/auxpage_faq/"> Часті питання </a></li>
        <li> <a href="/ua/auxpage_schedule/"> Графік роботи </a></li>
        <li> <a href="/ua/auxpage_delivery/"> Доставка </a></li>
        <li> <a href="/ua/auxpage_shipping/"> Оплата </a></li>
        <li> <a href="/ua/auxpage_adress/"> Як нас знайти </a></li>
        <li> <a href="/ua/auxpage_kak-kupit-semena-konopli/"> Як купити насіння коноплі </a></li>
        <li> <a href="/ua/feedback/"> Контакти </a></li>
        <li> <a href="/ua/auxpage_polit/"> Політика заміни і повернення </a>
          <ul>
            <li> <a href="/ua/auxpage_indiv/"> Для приватних осіб </a></li>
            <li> <a href="/ua/auxpage_for-shop/"> Для магазинів </a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li> <a href="#"> Насіння </a>
      <ul>
        <li> <a href="/ua/category/errors-seeds/">Errors Seeds Silver</a>
          <ul>
            <li><a href="/ua/category/errors-seeds/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
            <li><a href="/ua/category/errors-seeds/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
            <li><a href="/ua/category/errors-seeds/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
            <li><a href="/ua/category/errors-seeds/errors-seeds-auto-fem/">Errors Seeds Silver Auto Fem</a></li>
            <li><a href="/ua/category/errors-seeds/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/errors-seeds-import/">Errors Seeds Gold</a>
          <ul>
            <li><a href="/ua/category/errors-seeds-import/errors-seeds-import-feminised/">Errors Seeds Gold Feminised</a></li>
            <li><a href="/ua/category/errors-seeds-import/errors-seeds-import-auto-fem/">Errors Seeds Gold Auto Fem</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/carpathians-seeds/">Carpathians seeds</a>
          <ul>
            <li><a href="/ua/category/carpathians-seeds/carpathians-seeds-auto-fem/">Carpathians seeds Auto Fem</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/neuroseeds/">Neuroseeds</a>
          <ul>
            <li><a href="/ua/category/neuroseeds/neuroseeds-auto-fem/">Neuroseeds auto fem</a></li>
            <li><a href="/ua/category/neuroseeds/neuroseeds-fem/">Neuroseeds fem</a></li>
          </ul>
        </li>
        <li><a href="/ua/category/cbd-sorta/">CBD сорти</a></li>
        <li> <a href="/ua/category/victory-seeds/">Victory Seeds</a>
          <ul>
            <li><a href="/ua/category/victory-seeds/victory-seeds-auto-fem/">Victory Seeds Auto Fem</a></li>
            <li><a href="/ua/category/victory-seeds/victory-seeds-fem/">Victory Seeds Fem</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/bulk-seed-bank/">Bulk Seed Bank</a>
          <ul>
            <li><a href="/ua/category/bulk-seed-bank/bulk-seed-bank-auto-fem/">Bulk Seed Bank Auto Fem</a></li>
            <li><a href="/ua/category/bulk-seed-bank/bulk-seed-bank-fem/">Bulk Seed Bank Fem</a></li>
          </ul>
        </li>
        <li> <a href="/ua/category/drugie/green-house-seeds/">Green House Seeds</a>
          <ul>
            <li><a href="/ua/category/drugie/green-house-seeds/green-house-seeds-auto-fem/">Green House Seeds Auto Fem</a></li>
            <li><a href="/ua/category/drugie/green-house-seeds/green-house-seeds-feminised/">Green House Seeds Feminised</a></li>
          </ul>
        </li>
        <li><a href="/ua/category/mandala-seeds-/">Mandala Seeds</a></li>
        <li><a href="/ua/category/kannabia/">Kannabia</a></li>
        <li><a href="/ua/category/pyramid-seeds/">Pyramid Seeds</a></li>
        <li><a href="/ua/category/humboldt/">Humboldt</a></li>
        <li><a href="/ua/category/gsr/">Green Silk Road Seeds</a>
      </li>
         <li><a href="/ua/category/dinafem/">Dinafem</a>
      </li>
           <li>
        <a href="/ua/category/semena-iz-gollandii/">Насіння з Голландії</a>
        <ul>
          <li><a href="/ua/category/semena-iz-gollandii/barneys-farm/">Barneys Farm</a></li>
          <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/">Dutch Passion</a>
            <ul>
              <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/dutch-passion-seeds-auto-fem/">Dutch Passion Seeds Auto Fem</a></li>
            </ul>
          </li>
          <li> <a href="/ua/category/semena-iz-gollandii/vision-seeds/">Vision Seeds</a>
            <ul>
              <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/vision-seeds-auto-fem/">Vision Seeds Auto Fem</a></li>
              <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/vision-seeds-feminised/">Vision Seeds Feminised</a></li>
            </ul>
          </li>
        </ul>
      </li>
        <li> <a href="/ua/category/drugie/">Інші</a>
          <ul>
            <li> <a href="/ua/category/drugie/shortstuff-seeds/">Shortstuff Seeds</a>
              <ul>
                <li><a href="/ua/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-fem/">Shortstuff Seeds fem</a></li>
                <li><a href="/ua/category/drugie/shortstuff-seeds/shortstuff-seeds-auto-reg/">Shortstuff Seeds reg</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/sweet-seeds/">Sweet Seeds</a>
              <ul>
                <li><a href="/ua/category/sweet-seeds/sweet-seeds-auto-fem/">Sweet Seeds Auto Fem</a></li>
                <li><a href="/ua/category/sweet-seeds/sweet-seeds-feminised/">Sweet Seeds Feminised</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/flash-seeds/">Flash-seeds</a>
              <ul>
                <li><a href="/ua/category/drugie/flash-seeds/flash-seeds-auto-fem/">Flash-seeds Auto Fem</a></li>
                <li><a href="/ua/category/drugie/flash-seeds/flash-seeds-regular/">Flash-seeds Regular</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/grass-o-matic-seeds/">Grass-o-matic seeds</a>
              <ul>
                <li><a href="/ua/category/drugie/grass-o-matic-seeds/grass-o-maitc-seeds-auto-fem/">Grass-o-matic seeds Auto Fem</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/joint-doctor-seeds/">Joint Doctor seeds</a>
              <ul>
                <li><a href="/ua/category/drugie/joint-doctor-seeds/joint-doctor-seeds-regular/">Joint Doctor seeds Regular</a></li>
                <li><a href="/ua/category/drugie/joint-doctor-seeds/joint-doctor-seeds-auto-fem/">Joint Doctor seeds Auto Fem</a></li>
              </ul>
            </li>
            <li> <a href="/ua/category/drugie/seedsman/">Seedsman</a>
              <ul>
                <li><a href="/ua/category/drugie/seedsman/seedsman-auto-fem/">Seedsman Auto Fem</a></li>
                <li><a href="/ua/category/drugie/seedsman/seedsman-regular/">Seedsman Regular</a></li>
                <li><a href="/ua/category/drugie/seedsman/seedsman-feminised/">Seedsman Feminised</a></li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
	<li> <a href="/ua/category/oborydovanie-i-udobreniya/oborydovanie/"> Обладнання </a></li>
	  <li> <a href="/ua/category/oborydovanie-i-udobreniya/udobreniya/"> Добрива </a></li>
    <li> <a href="/ua/auxpage_action_list"> <span style="color: red; font-weight: bold;"> SALE</span> </a></li>
    <li> <a href="/ua/auxpage_sotrudnichestvo/"> Співпраця </a>
      <ul>
        <li><a href="/ua/auxpage_referalnaya_programma/">Реферальна програма</a></li>
        <li><a href="/ua/auxpage_opt/">Опт</a></li>
        <li><a href="/ua/auxpage_dropshipping/">Дропшиппінг</a></li>
        <li><a href="/ua/auxpage_interesnyy_kontent/">Цікавий контент</a></li>
        <li><a href="/ua/auxpage_smm/">SMM </a></li>
        <li><a href="/ua/auxpage_distribjucija/">Дистриб'юція</a></li>
        <li><a href="/ua/auxpage_vakansii/">Вакансії</a></li>
        <li><a href="/ua/pricelist/?in_stock=yes">Прайс-лист</a></li>
      </ul>
    </li>
      </ul>
    </nav>
  </div>
</div>
