<div class="footer_top">
<div class="container">
  <div class="row mb-lg-4">
    <div class="footer_sub col-12 col-md-5">
      <div class="f_sub_head f_title">Про магазин <i class="fas fa-chevron-down"></i></div>
      <div class="f_sub_cont ">
        <ul class="sf-menu">
          <li>
            <li> <a href="/ua/blog/">Блог</a></li>
            <li> <a href="/ua/auxpage_about/"> Про нас </a></li>
            <li> <a href="/ua/feedback/"> Контакти </a></li>
            <li> <a href="/ua/auxpage_shipping/"> Оплата </a></li>
            <li> <a href="/ua/auxpage_delivery/"> Доставка </a></li>
            <li> <a href="/ua/sitemap/"> Мапа сайта </a></li>
            <li> <a href="/ua/auxpage_polit-konf/"> Політика конфіденційності </a></li>
          </li>
        </ul>
      </div>
    </div>
    <div class="footer_sub col-12 col-md-4">
      <div class="f_sub_head f_title">Насiння <i class="fas fa-chevron-down"></i></div>
      <div class="f_sub_cont">
        <ul class="sf-menu">
          <li><a href="/ua/category/errors-seeds/">Errors Seeds Silver</a></li>
          <li><a href="/ua/category/errors-seeds-import/">Errors Seeds Gold</a></li>
          <li><a href="/ua/category/carpathians-seeds/">Carpathians Seeds</a></li>
          <li><a href="/ua/category/drugie/green-house-seeds/">Green House Seeds</a></li>
          <li><a href="/ua/category/drugie/dinafem/">Dinafem</a></li>
          <li><a href="/ua/category/sweet-seeds/">Sweet Seeds</a></li>
          <li><a href="/ua/category/barneys-farm/">Barneys Farm</a></li>
        </ul>
      </div>
    </div>
    <div class="footer_sub col-12 col-md-3">
      <div class="payment mb-3"><img src="/image/payment.png"></div>
      <ul class="sf-menu">
        <li>
          <a rel="http://growmarket.com.ua/catalog/osveschenie" href="#" target="_blank" class="hidli">
            Лампи для рослин
          </a>
        </li>
        <li>
          <a rel="http://medcannabis.info/" href="#" target="_blank" class="hidli">
            Медицина</a>
        </li>
        <li>
          <a rel="https://jahforum.net/" href="#" target="_blank" class="hidli">
            Форум</a>
        </li>
      </ul>
      <button type="button" name="director" class="director_btn">Звя`зок з директором</button>
    </div>
  </div>
</div>
</div>
