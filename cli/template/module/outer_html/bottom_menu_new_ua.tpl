<div class="container-fluid" style="background:#363636;margin-top: 20px;padding: 15px 0;">
  <div class="container">
    <div class="d-flex col-12 m-auto col-lg-9 p-0 col-12 flex-column flex-md-row">
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> НОВІ ТА ПОПУЛЯРНІ</div>
        <ul class="sf-menu">
          <li> <a href="/ua/202/">Ура! Errors Seeds 9 років! </a> <a href="/ua/198/">Імпортне оновлення асортименту</a> <a href="/ua/197/">Свято 4:20 з Errors Seeds </a> <a href="/ua/196/">Скоро свято! Не прогав! </a> <a href="/ua/195/">Оновлення
              асортименту до нового аутдорного сезону 2018 </a> <a href="/ua/194/">Стартує передзамовлення на 8-й випуск журналу JahPub! </a> <a href="/ua/193/">Замовив Green House — отримав подарунок!</a> <a href="/ua/192/">Абсолютно легально!</a></li>
        </ul>
      </div>
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> ДОПОМОГА</div>
        <ul class="sf-menu">
          <li> <a href="/ua/auxpage_faq/"> Часті питання </a></li>
          <li> <a href="/ua/auxpage_kak-kupit-semena-konopli/"> Як купити насіння коноплі </a></li>
          <li> <a href="/ua/feedback/"> Контакти </a></li>
          <li> <a href="/ua/auxpage_delivery/"> Доставка </a></li>
          <li> <a href="/ua/auxpage_shipping/"> Оплата </a></li>
        </ul>
      </div>
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> НАСIННЯ</div>
        <ul class="sf-menu">
          <li><a href="/ua/category/errors-seeds/">Errors-seeds</a></li>
          <li><a href="/ua/category/errors-seeds-import/">Errors-seeds імпорт</a></li>
          <li><a href="/ua/category/carpathians-seeds/">Carpathians seeds</a></li>
          <li><a href="/ua/category/drugie/green-house-seeds/">Green House Seeds</a></li>
          <li><a href="/ua/category/semena-iz-gollandii/vision-seeds/">Vision Seeds</a></li>
          <li><a href="/ua/category/drugie/shortstuff-seeds/">Shortstuff Seeds</a></li>
          <li><a href="/ua/category/sweet-seeds/">Sweet Seeds</a></li>
          <li><a href="/ua/category/semena-iz-gollandii/dutch-passion-seeds/">Dutch Passion seeds</a></li>
          <li><a href="/ua/category/drugie/flash-seeds/">Flash-seeds</a></li>
          <li><a href="/ua/category/drugie/grass-o-matic-seeds/">Grass-o-matic seeds</a></li>
          <li><a href="/ua/category/drugie/joint-doctor-seeds/">Joint Doctor seeds</a></li>
          <li><a href="/ua/category/drugie/seedsman/">Seedsman</a></li>
        </ul>
      </div>
      <div class="footer_sub col-12 p-0 col-md-3">
        <div class="f_sub_head"> О МАГАЗИНІ</div>
        <ul class="sf-menu">
          <li> <a href="/ua/auxpage_about/"> Про нас </a></li>
          <li> <a href="/ua/auxpage_schedule/"> Графік роботи </a></li>
          <li> <a href="/ua/blog/"> Блог </a></li>
          <li> <a href="/ua/sitemap/"> Карта сайту </a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
