<div class="menu">
    <ul class="header_menu">
        <li><a href="/">Главная</a></li>
        <li>
        	  <p class="drop">О магазине <i class="fas fa-chevron-down"></i></p>
            <ul class="drop_hid">
              <div class="m_it">
                <li><a href="/auxpage_about/">О Нас</a></li>
                <li><a href="/feedback/">Контакты</a></li>
                <li><a href="/auxpage_schedule/">График работы</a></li>
                <li><a href="/auxpage_faq/">Часто задаваемые вопросы</a></li>
              </div>
              <div class="m_it">
                <li><a href="/auxpage_shipping/">Оплата</a></li>
                <li><a href="/auxpage_delivery/">Доставка</a></li>
                <li><a href="/reports/">Отзывы</a></li>
                <li><a href="https://jahforum.net/">Форум</a></li>
              </div>
              <div class="m_it">
                <li><a href="/auxpage_kak-kupit-semena-konopli/">Как купить семена конопли</a></li>
                <li>
                	<div class="cont" style="cursor: default;">Политика замены и возврата<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                    <ul class="accordion_block">
                        <li><a href="/auxpage_indiv/">Для частных лиц</a></li>
                        <li><a href="/auxpage_for-shop/">Для магазинов</a></li>
                    </ul>
                </li>
              </div>
            </ul>
        </li>
        <li>
        	  <p class="drop">Семена <i class="fas fa-chevron-down"></i></p>
            <ul class="drop_hid">
              <div class="m_it">
                <li>
                	<div class="cont"><a href="/category/errors-seeds/">Errors Seeds Silver</a><div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                    <ul class="accordion_block">
                        <li><a href="/category/errors-seeds-auto/">Errors Seeds Silver Auto</a></li>
                        <li><a href="/category/errors-seeds-regular/">Errors Seeds Silver Regular</a></li>
                        <li><a href="/category/errors-seeds-feminised/">Errors Seeds Silver Feminised</a></li>
                        <li><a href="/category/errors-seeds-auto-fem/">Errors Seeds Silver Auto Fem</a></li>
                        <li><a href="/category/errors-seeds-cup/">Errors Seeds Silver Cup</a></li>
                    </ul>
                </li>
                <li>
                	<div class="cont"><a href="/category/errors-seeds-import/">Errors Seeds Gold</a><div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                    <ul class="accordion_block">
                        <li><a href="/category/errors-seeds-import-feminised/">Errors Seeds Gold Feminised</a></li>
                        <li><a href="/category/errors-seeds-import-auto-fem/">Errors Seeds Gold Auto Fem</a></li>
                    </ul>
                </li>
                <li>
                  <div class="cont">
                    <a href="/category/komplekty-es/">Комплекты Errors Seeds</a>
                  </div>
                </li>
              </div>
              <div class="m_it">
                <li>
                  <div class="cont">
                    <a href="/category/cbd-sorta/">CBD сорта</a>
                  </div>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">Испания<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/category/victory-seeds/">Victory Seeds</a></li>
                    <li><a href="/category/sweet-seeds/">Sweet Seeds</a></li>
                    <li><a href="/category/mandala-seeds-/">Mandala Seeds</a> </li>
                    <li><a href="/category/kannabia/">Kannabia</a> </li>
                    <li><a href="/category/pyramid-seeds/">Pyramid Seeds</a> </li>
                    <li><a href="/category/drugie/dinafem/">Dinafem</a> </li>
                    <li><a href="/category/buddha-seeds/">Buddha Seeds</a> </li>
                  </ul>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">Англия<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/category/bulk-seed-bank/">Bulk Seed Bank</a></li>
                  </ul>
                </li>
              </div>
              <div class="m_it">
                <li>
                  <div class="cont"><a href="/category/semena-iz-gollandii/">Голландия</a><div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/category/dutch-passion-seeds/">Dutch Passion seeds</a></li>
                    <li><a href="/category/green-house-seeds/">Green House Seeds</a></li>
                    <li><a href="/category/vision-seeds/">Vision Seeds</a></li>
                    <li><a href="/category/neuroseeds/">Neuroseeds</a></li>
                    <li><a href="/category/barneys-farm/">Barneys Farm</a></li>
                  </ul>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">США<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/category/humboldt/">Humboldt</a></li>
                  </ul>
                </li>
                <li>
                  <div class="cont" style="cursor: default;">Чехия<div class="accordion_header"><i class="fas fa-chevron-down"></i></div></div>
                  <ul class="accordion_block">
                    <li><a href="/category/carpathians-seeds/">Carpathians seeds</a></li>
                  </ul>
                </li>
              </div>
            </ul>
        </li>
        <li>
          <p class="drop">Оборудование <i class="fas fa-chevron-down"></i></p>
          <ul class="drop_hid">
            <div class="m_it">
              <li><a href="/category/oborydovanie-i-udobreniya/oborydovanie/">Все оборудование</a></li>
              <li><a href="/category/oborydovanie-i-udobreniya/oborydovanie/ventilyaciya/">Вентиляция</a></li>
            </div>
            <div class="m_it">
              <li><a href="/category/oborydovanie-i-udobreniya/oborydovanie/izmeritelnaya-tehnika/">Измерительная техника</a></li>
              <li><a href="/category/oborydovanie-i-udobreniya/oborydovanie/hidroponnue-sistemu/">Гидропонные системы</a></li>
            </div>
            <div class="m_it">
              <li><a href="/category/oborydovanie-i-udobreniya/oborydovanie/osveshenie/">Освещение</a></li>
              <li><a href="/category/oborydovanie-i-udobreniya/oborydovanie/upravleniye-i-avtomatika/">Управление и автоматика</a></li>
            </div>
          </ul>
        </li>
      	<li>
          <p class="drop">Удобрения <i class="fas fa-chevron-down"></i></p>
          <ul class="drop_hid">
            <div class="m_it">
              <li><a href="/category/oborydovanie-i-udobreniya/udobreniya/">Все удобрения</a></li>
            </div>
            <div class="m_it">
              <li><a href="/category/oborydovanie-i-udobreniya/udobreniya/well-grow-by-errors-seeds/">Well Grow by Errors Seeds</a></li>
            </div>
            <div class="m_it">
              <li><a href="/category/oborydovanie-i-udobreniya/udobreniya/biostimulyatoru/">БИОСтимуляторы</a></li>
            </div>
          </ul>
        </li>
        <li><a href="/auxpage_action_list"><span style="color: red; font-weight: bold;">SALE</span></a></li>
        <li><a rel="nofollow" href="/blog/">Новости</a></li>
        <li>
        	  <p class="drop">Сотрудничество <i class="fas fa-chevron-down"></i></p>
            <ul class="drop_hid">
              <div class="m_it">
                <li><a href="/auxpage_sotrudnichestvo/">О сотрудничестве</a></li>
                <li><a href="/auxpage_referalnaya_programma">Реферальная программа</a></li>
                <li><a href="/auxpage_opt">Опт</a></li>
              </div>
              <div class="m_it">
                <li><a href="/auxpage_dropshipping">Дропшиппинг</a></li>
                <li><a href="/auxpage_interesnyy_kontent">Интересный контент</a></li>
                <li><a href="/auxpage_smm">SMM </a></li>
              </div>
              <div class="m_it">
                <li><a href="/auxpage_distribjucija">Дистрибьюция</a></li>
                <li><a href="/auxpage_vakansii">Вакансии</a></li>
              </div>
            </ul>
        </li>
    </ul>
</div>
