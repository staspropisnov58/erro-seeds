<nav class="navbar navbar-expand-md navbarNavaccount p-0">
  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNavaccount" aria-controls="navbarNavaccount" aria-expanded="false" aria-label="Toggle navigation">
    <span>Меню</span>
    <span class="acc_arrow"><i class="fas fa-chevron-up"></i></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavaccount">
    <div class="navbar-nav w-100">
      <?php foreach ($menu as $group) { ?>
        <div class="list-group <?php echo $group['class']; ?>">
          <?php foreach ($group['links'] as $link) { ?>
            <a href="<?php echo $link['link']; ?>" class="list-group-item <?php echo $link['class']; ?>"><?php echo $link['text']; ?></a>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
  </div>
</nav>
<?php if ($logged) { ?>
<a href="<?php echo $logout; ?>" class="logout"><?php echo $text_logout; ?></a>
<?php } ?>
