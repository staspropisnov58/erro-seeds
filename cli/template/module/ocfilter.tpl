<?php if ($options || $show_price) { ?>
<div class="panel ocfilter panel-default" id="ocfilter" <?php if (isset($for)) {?> data-for="
  <?=$for?>"
    <?php } ?>>
    <?php /*?>
    <div class="panel-heading ">
      <?php echo $heading_title; ?>
    </div>
    <?php */?>
    <div class="mt-lg-2" id="ocfilter-button">
      <button class="show_sorts green_button" data-loading-text="<i class='fas fa-sync-alt fa-spin'></i> Загрузка.."></button>
    </div>
    <div class="list-group" id="ocfilter-content">
      <?php if ($selecteds) { # Selected options ?>
      <div class="list-group-item selected-options">
        <?php foreach ($selecteds as $option) { ?>
        <div class="ocfilter-option">
          <span><?php echo $option['name']; ?>:</span>
          <?php foreach ($option['values'] as $value) { ?>
          <button type="button" onclick="location = '<?php echo $value['href']; ?>';" class="dellete-one"><i class="fa fa-times"></i> <?php echo $value['name']; ?></button>
          <?php } ?>
        </div>
        <?php } ?>
        <?php $count = count($selecteds); $selected = $selecteds; $first = array_shift($selected); ?>
        <?php if ($count > 1 || count($first['values']) > 1) { ?>
        <button type="button" onclick="location = '<?php echo $link; ?>';" class="btn btn-block  dellete-all" style=""><i class="fa fa-times-circle"></i> <?php echo $text_cancel_all; ?></button>
        <?php } ?>
      </div>
      <?php } ?>

      <?php if ($show_price) { # Price filtering ?>
      <div class="list-group-item ocfilter-option" data-toggle="popover-price">
        <div class="option-name">
          <?php echo $text_price; ?>&nbsp;
          <?php echo $symbol_left; ?>
          <span id="price-from"><?php echo $min_price_get; ?></span>&nbsp;-&nbsp;<span id="price-to"><?php echo $max_price_get; ?></span>
          <?php echo $symbol_right; ?>
        </div>

        <div class="option-values">
          <div id="scale-price" class="scale ocf-target" data-option-id="p" data-start-min="<?php echo $min_price_get; ?>" data-start-max="<?php echo $max_price_get; ?>" data-range-min="<?php echo $min_price; ?>" data-range-max="<?php echo $max_price; ?>" data-element-min="#price-from"
            data-element-max="#price-to" data-control-min="#min-price-value" data-control-max="#max-price-value"></div>

          <?php if ($diagram) { ?>
          <div class="price-diagram">
            <div class="diagram-field">
              <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="40px">
                                    <?php foreach ($diagram['circles'] as $circle) { ?>
                                        <?php if ($circle['count']) { ?>
                                            <circle cx="<?php echo $circle['x']; ?>" cy="<?php echo $circle['y']; ?>" r="1" fill="#037a9c" stroke="#037a9c" stroke-width="1" />
                                        <?php } else { ?>
                                            <circle cx="<?php echo $circle['x']; ?>" cy="<?php echo $circle['y']; ?>" r="1" fill="#BF0000" stroke="#BF0000" stroke-width="1" />
                                        <?php } ?>
                                    <?php } ?>
                                    <path fill="#037a9c" stroke="#037a9c" d="<?php echo $diagram['path']; ?>" stroke-width="1" opacity="0.25" stroke-opacity="1" />
                                </svg>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php } # Price filtering end ?>

      <?php foreach ($options as $option) { ?>
      <div class="list-group-item ocfilter-option" id="option-<?php echo $option['option_id']; ?>">
        <div class="option-name">
          <?php echo $option['name']; ?>
          <i class="fas fa-chevron-down"></i>
          <?php if ($option['type'] == 'slide' || $option['type'] == 'slide_dual') { ?>
          <span id="left-value-<?php echo $option['option_id']; ?>"><?php echo $option['slide_value_min_get']; ?></span>
          <?php if ($option['type'] == 'slide_dual') { ?> -&nbsp;
          <span id="right-value-<?php echo $option['option_id']; ?>"><?php echo $option['slide_value_max_get']; ?></span>
          <?php } ?>
          <?php echo $option['postfix']; ?>
          <?php } ?>
        </div>

        <div class="option-values">
          <?php if ($option['selectbox'] && ($option['type'] == 'radio' || $option['type'] == 'checkbox')) { # Selectbox wrapper start ?>
          <div class="dropdown">
            <button class="btn btn-block btn-<?php echo (isset($selecteds[$option['option_id']]) ? 'warning' : 'default'); ?> dropdown-toggle" type="button" id="ocfilter-selectbox-<?php echo $option['option_id']; ?>" data-toggle="dropdown" aria-expanded="true">
                                <i class="pull-right fa fa-angle-down"></i>
                                <span class="pull-left text-left">
                	<?php if (isset($selecteds[$option['option_id']])) { ?>
                        <?php foreach ($selecteds[$option['option_id']]['values'] as $value) { ?>
                            <?php echo $value['name']; ?><br />
                        <?php } ?>
                    <?php } else { ?>
                        <?php echo $text_any; ?>
                    <?php } ?>
                </span>
                            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="ocfilter-selectbox-<?php echo $option['option_id']; ?>">
              <li role="presentation">
                <?php } ?>

                <?php if ($option['type'] == 'select') { # Select type start ?>

                <select class="form-control ocf-target<?php echo ($option['selected'] ? ' selected' : ''); ?>">
                                            <?php foreach ($option['values'] as $value) { ?>
                                                <?php if ($value['selected']) { ?>
                                                    <option value="<?php echo $value['href']; ?>" id="v-<?php echo $value['id']; ?>" selected="selected"><?php echo $value['name']; ?></option>
                                                <?php } elseif ($value['count']) { ?>
                                                    <option value="<?php echo $value['href']; ?>" id="v-<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                <?php } else { ?>
                                                    <option value="" id="v-<?php echo $value['id']; ?>" disabled="disabled"><?php echo $value['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>

                <?php } elseif ($option['type'] == 'slide' || $option['type'] == 'slide_dual') { # Slide type start ?>
                <div id="scale-<?php echo $option['option_id']; ?>" class="scale ocf-target" data-option-id="<?php echo $option['option_id']; ?>" data-start-min="<?php echo $option['slide_value_min_get']; ?>" data-start-max="<?php echo $option['slide_value_max_get']; ?>"
                  data-range-min="<?php echo $option['slide_value_min']; ?>" data-range-max="<?php echo $option['slide_value_max']; ?>" data-element-min="#left-value-<?php echo $option['option_id']; ?>" data-element-max="#right-value-<?php echo $option['option_id']; ?>"></div>

                <?php } elseif ($option['type'] == 'radio' || $option['type'] == 'checkbox') { # Radio and Checkbox types start ?>

                <?php foreach ($option['values'] as $key => $value) { ?>

                <?php if ($show_values_limit && $show_values_limit == $key) { ?>
                <p>
                  <a href="#ocfilter-hidden-values-<?php echo $option['option_id']; ?>" data-toggle="collapse" aria-expanded="false" aria-controls="ocfilter-hidden-values">
                    <?php echo $text_show_all; ?> <i class="fa fa-angle-down"></i></a>
                </p>
                <div class="collapse" id="ocfilter-hidden-values-<?php echo $option['option_id']; ?>">
                  <?php } ?>

                  <?php if ($option['color']) { ?>
                  <div class="color" style="background-color: #<?php echo $value['color']; ?>;"></div>
                  <?php } ?>

                  <?php if ($option['image']) { ?>
                  <div class="image" style="background-image: url(<?php echo $value['image']; ?>);"></div>
                  <?php } ?>

                  <?php if ($value['selected']) { ?>
                  <label id="v-<?php echo $value['id']; ?>" class="selected">
                                                    <input type="<?php echo $option['type']; ?>" name="ocfilter_filter[<?php echo $option['option_id']; ?>]" value="<?php echo $value['href']; ?>" checked="checked" class="ocf-target" />
                                                  <div class="d-flex">
                                                    <span class="custom_input"></span>
                                                    <a href="<?php echo $value['href']; ?>"><?php echo $value['name']; ?></a>
                                                  </div>
                                                </label>
                  <?php } elseif ($value['count']) { ?>
                  <label id="v-<?php echo $value['id']; ?>">
                                                    <input type="<?php echo $option['type']; ?>" name="ocfilter_filter[<?php echo $option['option_id']; ?>]" value="<?php echo $value['href']; ?>" class="ocf-target" />
                                                  <div class="d-flex">
                                                    <span class="custom_input"></span>
                                                    <a href="<?php echo $value['href']; ?>"><?php echo $value['name']; ?></a>
                                                  </div>
                                                    <?php if ($show_counter) { ?>
                                                        <small class="badge">(<?php echo $value['count']; ?>)</small>
                                                    <?php } ?>
                                                </label>
                  <?php } else { ?>
                  <label id="v-<?php echo $value['id']; ?>" class="disabled">
                                                    <input type="<?php echo $option['type']; ?>" name="ocfilter_filter[<?php echo $option['option_id']; ?>]" value="" disabled="disabled" class="ocf-target" />
                                                    <div class="d-flex"><span class="custom_input"></span>
                                                    <?php echo $value['name']; ?></div>
                                                    <?php if ($show_counter) { ?>
                                                        <small class="badge">(0)</small>
                                                    <?php } ?>
                                                </label>
                  <?php } ?>

                  <?php if ($show_values_limit && $show_values_limit < count($option['values']) && $key + 1 == count($option['values'])) { ?>
                </div>
                <?php } ?>

                <?php } ?>

                <?php } # End type switcher ?>

                <?php if ($option['selectbox'] && ($option['type'] == 'radio' || $option['type'] == 'checkbox')) { # Selectbox wrapper end ?>
              </li>
            </ul>
          </div>
          <?php } ?>
        </div>
      </div>

      <?php if ($show_options_limit && $option['index'] == count($options)) { # Closing the hidden options box ?>
      <?php } ?>

      <?php } # End "foreach $options" ?>
    </div>
</div>
<button type="button" class="close">
  <svg width="20" height="20" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M0.209705 0.387101L0.292893 0.292893C0.653377 -0.0675907 1.22061 -0.0953203 1.6129 0.209705L1.70711 0.292893L8.5 7.085L15.2929 0.292893C15.6534 -0.0675907 16.2206 -0.0953203 16.6129 0.209705L16.7071 0.292893C17.0676 0.653377 17.0953 1.22061 16.7903 1.6129L16.7071 1.70711L9.915 8.5L16.7071 15.2929C17.0676 15.6534 17.0953 16.2206 16.7903 16.6129L16.7071 16.7071C16.3466 17.0676 15.7794 17.0953 15.3871 16.7903L15.2929 16.7071L8.5 9.915L1.70711 16.7071C1.34662 17.0676 0.779392 17.0953 0.387101 16.7903L0.292893 16.7071C-0.0675907 16.3466 -0.0953203 15.7794 0.209705 15.3871L0.292893 15.2929L7.085 8.5L0.292893 1.70711C-0.0675907 1.34662 -0.0953203 0.779392 0.209705 0.387101Z" fill="white"></path>
  </svg>
</button>
<script type="text/javascript" defer>
  <!--
  var options = {
    mobile: false,
    php: {
      showPrice: <?php echo $show_price; ?>,
      showCounter: <?php echo $show_counter; ?>,
      manualPrice: <?php echo $manual_price; ?>,
      link: '<?php echo $link; ?>',
      path: '<?php echo $path; ?>',
      params: '<?php echo $params; ?>',
      index: '<?php echo $index; ?>'
    },
    text: {
      show_all: '<?php echo $text_show_all; ?>',
      hide: '<?php echo $text_hide; ?>',
      load: '<?php echo $text_load; ?>',
      any: '<?php echo $text_any; ?>',
      select: '<?php echo $button_select; ?>'
    }
  };
  $(function() {
    if ($('#ocfilter').is(':hidden')) {
      $('#navbar-ocfilter').html($('#ocfilter').remove().get(0).outerHTML);

      var html = $('#ocfilter-mobile').remove().get(0).outerHTML;

      $('.breadcrumb').after(html);

      options['mobile'] = true;
    }

    setTimeout(function() {
      $('#ocfilter').ocfilter(options);
    }, 1);
  });

  $('.my_class_x').on('click', function() {
    $('.ocfilter').css({
      'opacity': '1',
      'height': 'auto'
    });
    $('.my_class_x_2').css({
      'display': 'block'
    });
    $('.my_class_x').css({
      'display': 'none'
    });
  });

  $('.my_class_x_2').on('click', function() {
    $('.ocfilter').css({
      'opacity': '0',
      'height': '1'
    });
    $('.my_class_x_2').css({
      'display': 'none'
    });
    $('.my_class_x').css({
      'display': 'block'
    });
  });

  //-->
</script>
<?php } ?>
