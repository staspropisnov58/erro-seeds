<?php if (isset($products)) { ?>
<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
 'ecommerce': {
   'currencyCode': '<?php echo $currency_code; ?>',
   'checkout': {
     'actionField': {'step': <?php echo $current_step_id; ?>, 'option': '<?php echo $option; ?>'},
     'products': [
       <?php foreach ($products as $product) { ?>
         {'name': '<?=$product['name']?>',
         'id': '<?=$product['product_id']?>',
         'price': '<?=$product['price']?>',
         'brand': '<?=$product['manufacturer']?>',
         'category': '<?=$product['category']?>',
         'variant': '<?=$product['option']?>',
         'quantity': <?=$product['quantity']?>,
         'coupon': ''},
        <?php } ?>
     ]
   }
 },
 'event': 'gtm-ee-event',
 'gtm-ee-event-category': 'Enhanced Ecommerce',
 'gtm-ee-event-action': 'Checkout Step <?php echo $current_step_id; ?>',
 'gtm-ee-event-non-interaction': 'False',
});
</script>
<?php } ?>
