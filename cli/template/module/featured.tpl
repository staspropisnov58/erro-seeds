<div class="produckt-slider">
  <div class="container">
    <div class="name-prod-name">
      <?php if (isset($categories[6])) { ?>
        <a href="<?=$categories[6]['href'];?>" ><?=$heading_title; ?></a>
      <?php } else {  echo $heading_title; } ?>
    </div>
    <div class="related_slider owl-carousel">
      <?php foreach ($products as $product) { ?>
      <div class="product-wrap">
        <div class="product_img">
          <a href="<?php echo $product['href']; ?>">
            <?php if ($stickers) { ?>
              <div class="stickers-block">
              <?php foreach ($stickers as $sticker){ ?>
                  <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
              <?php } ?>
              </div>
            <?php } ?>
            <img data-src="<?= $product['thumb']; ?>" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 3'%3E%3C/svg%3E" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-fluid owl-lazy">
          </a>
        </div>
        <a href="<?php echo $product['href']; ?>" class="product_name related_product">
            <?php echo $product['name']; ?>
        </a>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
