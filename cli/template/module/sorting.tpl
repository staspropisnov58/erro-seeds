<?php if($sorting_status == 1){ ?>
  <div class="d-flex align-items-center custom-sel-wrapper">
    <div class="custom-sel" id="input-sort" onchange="location = this.value;">
        <div class="custom-sel__trigger">
          <span>
            <i class="fas fa-sort" style="padding-right:5px;color:#65bd00;font-size:17px"></i>
            <?php echo $sort_order; ?>
          </span>
          <i class="fas fa-chevron-down"></i>
        </div>
        <div class="custom-options">
          <?php if(!isset($url_string)){ ?>
            <span class="custom-option selected" data-value="<?php echo $sort_order_href; ?>"><?php echo $sort_order; ?></span>
          <?php }else{ ?>
            <span class="custom-option" data-value="<?php echo $sort_order_href; ?>"><?php echo $sort_order; ?></span>
          <?php } ?>
          <?php foreach ($sortings as $key => $sorting) { ?>
          <?php  if( isset($url_string) && $url_string == $sorting){ ?>
              <span class="custom-option selected" data-value="<?php echo $sorting; ?>"><?php echo $$key; ?></span>
          <?php  }else{ ?>
                <span class="custom-option" data-value="<?php echo $sorting; ?>"><?php echo $$key; ?></span>
              <?php  } ?>
          <?php } ?>
        </div>
      </div>
    </div>
  <?php } ?>
