<?php if ($categories) { ?>
  <div class="h1"><h1><?php echo $text_category; ?></h1></div>
  <div class="cpt_maincontent">
      <table border="0" cellpadding="0" class="toggle_xs category_table">
        <tbody>
          <?php foreach ($categories as $categories_row) { ?>
            <tr>
              <?php foreach ($categories_row as $category) { ?>
                <td align="center" class="cat_image">
                  <a href="<?php echo $category['href']; ?>">
                    <?php if ($category['image']) { ?>
                    <?php if ($category['image']['extension'] === 'svg') { ?>
                    <?php echo $category['image']['value']; ?>
                    <?php } else { ?>
                    <img src="<?php echo $category['image']['value']; ?>" alt="<?php echo $category['name']; ?> - image" title="<?php echo $category['name']; ?>">
                    <?php } ?>
                    <?php } ?>
                    <span><?php echo $category['name']; ?></span>
                  </a>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
        </tbody>
      </table>
      <span class="show_all"><?php echo $text_show_all; ?></span>
  </div>
<?php } ?>
