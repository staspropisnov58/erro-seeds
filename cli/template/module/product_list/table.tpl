<div class="cpt_product_lists 1">
  <div class="name-prod-name">
    <?php if ($href) { ?>
      <a href="<?php echo $href; ?>"><div class="h1" style="margin-top:25px;"><h2><?php echo $heading_title; ?></h2></div></a>
    <?php } else { ?>
      <span><?php echo $heading_title; ?></span>
    <?php } ?>
  </div>
  <div class="product_home">
    <ul>
      <?php foreach($products as $key => $product){ ?>
        <?php if ($key <= 19) {?>
        <li>
          <div class="product_img">
            <!-- <div class="sale_icon"></div> -->
            <div class="stickers-block">
              <?php foreach ($product['stickers'] as $sticker){ ?>
                <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
              <?php } ?>
            </div>
            <a href=" <?php echo $product['href']; ?>">
              <img src="<?php echo $product['thumb']; ?>"  alt = "<?php echo $product['alt_image'];?>" title = "<?php echo $product['title_image']; ?>"/>
            </a>
          </div>
          <a href="<?php echo $product['href'] ?>" class="product_name"><?php echo $product['name']; ?> </a>
          <p class="product_price"><?php echo $product['price']; ?></p>
        </li>
      <?php }else{} ?>
      <?php } ?>
    </ul>
  </div>
</div>
