<?php if ($products) { ?>
  <div class="produckt-slider">
    <div class="container">
      <div class="name-prod-name">
        <?php if ($href) { ?>
          <a href="<?php echo $href; ?>"><span><?php echo $heading_title; ?></span></a>
        <?php } else { ?>
          <span><?php echo $heading_title; ?></span>
        <?php } ?>
      </div>
      <div class="related_slider owl-carousel">
        <?php foreach ($products as $product) { ?>
        <div class="product-wrap">
          <div class="product_img">
            <a href="<?php echo $product['href']; ?>">
              <?php if ($product['stickers']) { ?>
                <div class="stickers-block">
                <?php foreach ($product['stickers'] as $sticker){ ?>
                    <span class="<?php echo $sticker['class']; ?>"> <?php echo $sticker['text']; ?> </span>
                <?php } ?>
                </div>
              <?php } ?>
              <img data-src="<?= $product['thumb']; ?>" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 3'%3E%3C/svg%3E" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-fluid owl-lazy">
            </a>            
          </div>
          <a href="<?php echo $product['href'] ?>" class="product_name"><?php echo $product['name']; ?> </a>
          <p class="product_price"><?php echo $product['price']; ?></p>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php } ?>
