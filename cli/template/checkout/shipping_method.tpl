<div class="d-flex justify-content-sm-between flex-column flex-sm-row checkout_content_top align-items-sm-end">
  <h2><?php echo $text_h2; ?>:</h2>
</div>
<form class="mt-5 d-flex flex-column" id="shipping_method" name="<?php echo $form_name; ?>" method="post" action="<?php echo $form_action; ?>">
  <?php if ($shipping_methods) { ?>
    <?php foreach ($shipping_methods as $code => $shipping_method) { ?>
      <div class="shipping_item d-flex justify-content-between flex-column align-items-start">
        <div class="d-flex flex-row w-100 align-items-center justify-content-between">
          <input type="radio" name="shipping" class="checkout_checkbox" id="<?php echo $code; ?>" value="<?php echo $code; ?>"
          onclick="checkout.selectShipping(this)"
          <?php if ($shipping_method['checked']) { ?>checked<?php } ?>>
          <label class="for_checkbox shipping_name" for="<?php echo $code; ?>"><?php echo $shipping_method['title']; ?></label>
          <div class="shipping_cost text-right">
            <?php echo $shipping_method['text']; ?>
          </div>
        </div>
        <div class="shipping_info">
          <div class="shipping_description">
            <?php echo $shipping_method['note']; ?>
          </div>
        </div>
        <?php if($shipping_method['additions']) { ?>
          <?php foreach ($shipping_method['additions'] as $addition_id => $addition) { ?>
            <?php if ($addition['products']) { ?>
              <div id="additions_<?php echo $code; ?>" class="shipping_additions <?php if ($shipping_method['checked']) { ?>shown<?php } else { ?>hidden<?php } ?>" data-count-children="<?php echo count($addition['products']); ?>">
                <div class="d-flex justify-content-sm-between flex-column flex-sm-row checkout_content_top align-items-sm-end mt-5">
                  <h3><?php echo $addition['title']; ?></h3>
                </div>
                <?php foreach ($addition['products'] as $product_id => $product) { ?>
                  <div class="d-flex flex-row w-100 align-items-center justify-content-between">
                    <input type="checkbox" name="additions[<?php echo $addition_id; ?>]" class="checkout_checkbox addition" id="product<?php echo $product_id; ?>"
                    onclick="checkout.saveSelected(this)"
                    <?php if ($product['checked']) { ?>checked<?php } ?>
                    value="<?php echo $product_id; ?>" class="addition">
                    <label for="product<?php echo $product_id; ?>" class="for_checkbox shipping_name"><?php echo $product['name']; ?></label>
                    <div class="shipping_cost text-right">
                      <?php echo $product['cost']; ?>
                    </div>
                  </div>
                  <div class="shipping_info">
                    <div class="shipping_description">
                      <?php echo $product['description']; ?>
                    </div>
                  </div>
                <?php } ?>
              </div>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      </div>
    <?php } ?>
  <?php } ?>
  <div class="checkout_buttons ">
    <div id="cart_index" class="d-flex flex-row justify-content-between align-items-end">
      <div class="button_step_back">
        <?php if ($prev_step) { ?>
        <a href="<?php echo $prev_step_href; ?>" onclick="loadStep('<?php echo $prev_step; ?>', event)"><?php echo $text_prev_step; ?></a>
        <?php } else { ?>
          <a href="/">На главную</a>
          <?php } ?>
      </div>
    <div class="d-flex justify-content-end w-50">
      <button class="button_order active" type="submit" onclick="checkout.sendForm(event)"><?php echo $text_next_step; ?></button>
    </div>
    </div>
  </div>
</form>
