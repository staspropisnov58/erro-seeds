<?php if ($success_coupon) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $success_coupon; ?></div>
<?php } ?>
<?php if ($text_edit_coupon) { ?>
  <style>
    label.coupon_block{cursor: pointer; color: #82b523;}
    div.coupon_block{display: none;}
    #coupon_block:checked + div.coupon_block{
      display: block;
    }
  </style>
  <label for="coupon_block" class="coupon_block"><?php echo $text_edit_coupon; ?></label>
  <input class="coupon_block" name="coupon_block" id="coupon_block" type="checkbox" <?php if(!$text_edit_coupon || $error_coupon) { ?>checked<?php } ?>>
<?php } ?>

<div class="coupon_block">
  <form class="d-flex flex-column" action="<?php echo $coupon_action; ?>" method="post">
    <label class="mb-0 w-100 ">
      <?php echo $entry_coupon; ?>
      <div class="d-flex justify-content-between">
        <input type="text" name="coupon" value="" placeholder="" id="input-coupon" class="form-control w-50">
        <button class="button_coupon" onclick="checkout_cart.coupon(event)"><?php echo $button_coupon; ?></button>
      </div>
    </label>
    <noscript>
        <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
    </noscript>
    <?php if ($error_coupon) { ?>
      <span class="label_error w-100 d-flex"><?php echo $error_coupon; ?></span>
    <?php } ?>
  </form>
</div>
