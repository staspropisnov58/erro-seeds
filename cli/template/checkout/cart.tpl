<div class="checkout_right_block flex-column d-flex justify-content-between" id="cart-content">
  <div class="top_cart">
    <div class="checkout_prod_title d-flex justify-content-between align-items-center">
      <p class="title"><?php echo $text_items; ?></p>
      <?php if ($start_checkout) { ?>
      <a class="close" href="<?php echo $current_page; ?>" onclick="cart.close(event);">
        <span aria-hidden="true">&times;</span>
      </a>
      <?php } else { ?>
      <a class="close close_in_checkout" href="/">
        <span aria-hidden="true">&times;</span>
      </a>
      <button class="close close_cart" onclick="checkout.closeincheckout();">
        <span aria-hidden="true">&times;</span>
      </button>
        <?php } ?>
    </div>
    <?php if ($products) { ?>
    <?php if ($error_min_order) { ?>
    <div class="cash_on_dilivery">
      <?php echo $error_min_order; ?>
    </div>
    <?php } ?>
    <?php if ($error_payment_min_order) { ?>
    <div class="cash_on_dilivery">
      <?php echo $error_payment_min_order; ?>
    </div>
    <?php } ?>
    <div class="checkout_prod_block">
      <?php foreach ($products as $product) { ?>
        <div class="checkout_product_item flex-column">
          <div class="d-flex flex-row justify-content-between mb-2">
            <div class="product_name w-75">
              <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a><?php if (!$product['stock']) { ?>***<?php } ?>
                <span class="manufac"><?php echo $product['model']; ?></span>
            </div>
            <div class="product_price"><?php echo $product['total']; ?></div>
          </div>
          <div class="justify-content-between flex-row d-flex">
            <div class="">
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="checkout_prod_img">
            </div>
            <div class="d-flex flex-row ml-3 align-items-stretch w-100">
              <div class="d-flex flex-column justify-content-between flex-wrap w-75">
                <?php echo $product['sku']; ?>
                <div class="d-flex flex-row justify-content-between align-items-center">
                  <?php if ($product['option']) { ?>
                  <div class="product_option d-flex flex-row flex-wrap">
                    <span>
                      <?php foreach ($product['option'] as $option) { ?>
                      <span><?php echo $option['name']; ?>: <span><?php echo $option['value']; ?></span></span><br>
                      <?php } ?>
                    </span>
                  </div>
                  <?php } ?>
                </div>
                <div class="product_quantity d-flex flex-wrap flex-row align-items-center justify-content-start js-quantity-container">
                  <form class="d-flex align-items-center flex-row cart-action" action="<?php echo $edit_action; ?>" name="edit" method="post" onsubmit="checkout_cart.edit(this, event)">
                    <span class="minus" onclick="checkout_cart.plus('-1', event)">-</span>
                    <input type="tel" class="quantity" value="<?php echo $product['quantity']; ?>" name="quantity[<?php echo $product['key']; ?>]" onchange="checkout_cart.edit(this.form, event)">
                    <span class="plus" onclick="checkout_cart.plus('1', event)">+</span>
                    <noscript>
                      <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
                      <button name="refresh"><?php echo $button_update; ?></button>
                    </noscript>
                  </form>
                </div>
              </div>
              <div class="d-flex align-items-center justify-content-end delete_block w-25">
                <form class="d-flex flex-column cart-action" action="<?php echo $remove_action; ?>" name="remove" method="post">
                  <input type="hidden" name="key" value="<?php echo $product['key']; ?>">
                  <noscript>
                    <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
                  </noscript>
                  <button class="product_delete" onclick="checkout_cart.remove('<?php echo $product['key']; ?>', event)"><i class="far fa-trash-alt"></i></button>
                </form>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <?php if ($cart_gifts) { ?>
      <?php foreach ($cart_gifts['gift_programs'] as $gift_program_is => $program) { ?>
      <?php if (count($program['gifts']) === 1) { ?>
      <div class="checkout_product_item justify-content-between flex-row">
        <div class="">
          <img src="<?php echo $program['gifts'][0]['image']; ?>" alt="<?php echo $program['gifts'][0]['name']; ?>" title="<?php echo $program['gifts'][0]['name']; ?>" class="checkout_prod_img">
        </div>
        <div class="d-flex flex-column w-100 ml-3">
          <div class="d-flex flex-row justify-content-between">
            <div class="product_name w-75">
              <p class="bonus"><?php echo $cart_gifts['text_cart_gift']; ?></p>
              <a href="<?php echo $program['gifts'][0]['href']; ?>"><?php echo $program['gifts'][0]['name']; ?></a>
            </div>
          </div>
          <div class="d-flex flex-row justify-content-between mt-2 flex-wrap align-items-center">
            <?php echo $program['gifts'][0]['model']; ?>
            <div class="product_quantity d-flex flex-wrap flex-row align-items-center justify-content-start">
              <span><?php echo $cart_gifts['text_quantity']; ?>: <?php echo $program['gifts'][0]['quantity']; ?></span>

            </div>

          </div>
        </div>
      </div>
      <?php } ?>
      <?php } ?>
      <?php } ?>
    </div>
    <?php if ($subtotals) { ?>
    <div class="checkout_prices d-flex flex-column">
      <?php foreach ($subtotals as $subtotal) { ?>
      <div class="d-flex flex-row justify-content-between">
        <span class="price_text"><?php echo $subtotal['title']; ?>:</span>
        <div class="d-flex">
          <?php if ($subtotal['remove']) { ?>
          <form class="d-flex flex-column cart-action" action="<?php echo $subtotal['remove']; ?>" name="remove_<?php echo $subtotal['code']; ?>" method="post">
            <noscript>
              <input type="hidden" name="redirect" value="<?php echo $current_page; ?>">
            </noscript>
            <button class="product_delete" onclick="checkout_cart.remove_<?php echo $subtotal['code']; ?>(event)"><?php echo $button_remove; ?></button>
          </form>
          <?php } ?>
          <span class="price"><?php echo $subtotal['text']; ?></span>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
    <div class="checkout_total_price">
      <div class="d-flex flex-row justify-content-between">
        <span class="price_text"><?php echo $total['title']; ?>:</span>
        <span class="price"><?php echo $total['text']; ?></span>
      </div>
    </div>
    <?php echo $coupon; ?>
  </div>
  <?php if (!$error_min_order) { ?>
  <?php if ($start_checkout) { ?>
  <div class="checkout_buttons ">
    <div id="cart_index" class="d-flex flex-row justify-content-between align-items-end">
      <?php if ($start_checkout) { ?>
      <?php if ($fast_order) { ?>
      <div class="button_step_back w-50 mr-2 d-flex justify-content-start">
        <a id="fast_order" href="<?php echo $fast_order; ?>"><?php echo $text_quick_order; ?></a>
      </div>
      <?php } ?>
      <div class="d-flex justify-content-end w-50">
        <a class="button_order" href="<?php echo $start_checkout; ?>"><?php echo $text_next_step; ?></a>
      </div>
      <?php } ?>
    </div>
  </div>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
  <div class="checkout_no_product">
    <div class="d-flex justify-content-between flex-row align-items-center">
      <figure>
        <svg aria-hidden="true" data-prefix="fas" data-icon="frown-open" class="svg-inline--fa fa-frown-open fa-w-16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512">
          <path fill="currentColor"
            d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zM136 208c0-17.7 14.3-32 32-32s32 14.3 32 32-14.3 32-32 32-32-14.3-32-32zm187.3 183.3c-31.2-9.6-59.4-15.3-75.3-15.3s-44.1 5.7-75.3 15.3c-11.5 3.5-22.5-6.3-20.5-18.1 7-40 60.1-61.2 95.8-61.2s88.8 21.3 95.8 61.2c2 11.9-9.1 21.6-20.5 18.1zM328 240c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z" />
          </svg>
        <figcaption>
          <?php echo $text_empty_cart; ?>
        </figcaption>
      </figure>
    </div>
  </div>
  </div>
  <?php }?>
</div>
