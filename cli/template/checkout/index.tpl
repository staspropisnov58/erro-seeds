<!DOCTYPE html>
<html>
<head>
	<title><?php echo $heading_title; ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T36VTNM');
  </script>
	<?php echo $e_commerce; ?>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/catalog/view/theme/old/css/checkout/style_new_checkout.css">
	<link href="https://fonts.googleapis.com/css?family=Exo+2:400,700,900&amp;subset=cyrillic" rel="stylesheet">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous" defer></script>
	<script src="/catalog/view/theme/old/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/catalog/view/theme/old/js/common.js"></script>
	<script type="text/javascript" defer src="/catalog/view/theme/old/js/checkout/cart.js"></script>
	<script type="text/javascript" defer src="/catalog/view/theme/old/js/checkout/checkout.js?v=1.2.0"></script>
	<style>
	html, body {
    width: 100%;
    height: 100%;
	}
	.hidden, .shown {
	    max-height: 0;
	    overflow-y: hidden;
	    -webkit-transition: max-height 0.8s ease-in-out;
	    -moz-transition: max-height 0.8s ease-in-out;
	    -o-transition: max-height 0.8s ease-in-out;
	    transition: max-height 0.8s ease-in-out;
	}
	.shown {
	    max-height: 50em;
	}
	</style>
	<noscript>
	  <style>
	  .hidden {
	      max-height: 50em;
	  }
	  </style>
	</noscript>
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T36VTNM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
	<div class="d-flex justify-content-between mb-3">
		<div class="col-12 d-flex flex-row align-items-stretch checkout_left_block <?php if($cart) { ?>col-lg-7 flex-column flex-lg-row<?php } ?>">
			<div class="checkout_logo">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 223.3 200" style="fill: #65bd00;margin: 10px 0;" xml:space="preserve">
					<g>
						<path class="st0" d="M93.7,134.7H22V90.6h64.4c4.8,0,8.7-3.6,8.7-8.3c0-4.8-3.8-8.5-8.7-8.5H36.1H22c-11.3,0-18.4,2.7-18.4,15.2
							v53.7c0,5,4,8.9,9.1,8.9h80.9c4.8,0,8.7-3.6,8.7-8.3C102.3,138.5,98.5,134.7,93.7,134.7L93.7,134.7z M22,29.9h14.1h57.6
							c4.8,0,8.7-3.6,8.7-8.3c0-4.8-3.8-8.5-8.7-8.5H12.7c-5,0-9.1,4.2-9.1,9.1C3.7,31.9,16.1,30,22,29.9L22,29.9L22,29.9z"/>
						<path class="st0" d="M138.2,50.3c0-14.9,15.5-22.6,35.9-22.6c12.3,0,27,8.3,35.1,12.4c1,0.5,2.2,0.8,3.2,0.8c5,0,8.1-4.4,8.1-8.3
							c0-4.8-2.9-6.7-5.6-7.9C205.2,20.2,192,11,172,11c-34.1,0-52.3,18.7-52.3,40.7c0,48.8,85.8,32.6,85.8,62
							c0,17.3-15.3,22.8-35.3,22.8c-17.4,0-30-12.9-35.9-16.3c-1.3-0.8-2.6-0.8-3.8-0.8c-5,0-8.3,4-8.3,8.3c0,3.6,3,6.3,5.6,8.1
							c10.1,7.3,24,17.7,44.2,17.7c31.3,0,51.7-16.3,51.7-38.7C223.7,62.4,138.2,84.7,138.2,50.3L138.2,50.3L138.2,50.3z"/>
							</g>
					</svg>
			</div>
			<div class="d-flex flex-column checkout_center ">
				<div class="checkout_top d-flex flex-row justify-content-between align-items-start">
					<div class="checkout_head w-100">
						<div class="checkout_title mb-4 d-flex justify-content-between align-items-center">
							<a href="/" class="small_logo"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 223.3 150" style="fill: #65bd00;" xml:space="preserve">
								<g>
									<path d="M93.7,134.7H22V90.6h64.4c4.8,0,8.7-3.6,8.7-8.3c0-4.8-3.8-8.5-8.7-8.5H36.1H22c-11.3,0-18.4,2.7-18.4,15.2
									            		v53.7c0,5,4,8.9,9.1,8.9h80.9c4.8,0,8.7-3.6,8.7-8.3C102.3,138.5,98.5,134.7,93.7,134.7L93.7,134.7z M22,29.9h14.1h57.6
									            		c4.8,0,8.7-3.6,8.7-8.3c0-4.8-3.8-8.5-8.7-8.5H12.7c-5,0-9.1,4.2-9.1,9.1C3.7,31.9,16.1,30,22,29.9L22,29.9L22,29.9z"></path>
									<path d="M138.2,50.3c0-14.9,15.5-22.6,35.9-22.6c12.3,0,27,8.3,35.1,12.4c1,0.5,2.2,0.8,3.2,0.8c5,0,8.1-4.4,8.1-8.3
									            		c0-4.8-2.9-6.7-5.6-7.9C205.2,20.2,192,11,172,11c-34.1,0-52.3,18.7-52.3,40.7c0,48.8,85.8,32.6,85.8,62
									            		c0,17.3-15.3,22.8-35.3,22.8c-17.4,0-30-12.9-35.9-16.3c-1.3-0.8-2.6-0.8-3.8-0.8c-5,0-8.3,4-8.3,8.3c0,3.6,3,6.3,5.6,8.1
									            		c10.1,7.3,24,17.7,44.2,17.7c31.3,0,51.7-16.3,51.7-38.7C223.7,62.4,138.2,84.7,138.2,50.3L138.2,50.3L138.2,50.3z"></path>
									</g>
								</svg>
							</a>
							<h1><?php echo $text_h1; ?></h1>
							<button id="cart_edit_button" onclick="checkout.openCart()">
								<svg viewBox="0 0 23 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" clip-rule="evenodd" d="M1.92639 2.47358H1.46323C0.966176 2.47358 0.563232 2.07064 0.563232 1.57358C0.563232 1.07653 0.966176 0.673584 1.46323 0.673584H1.92639C3.51011 0.673584 4.80739 1.77545 5.13303 3.39616L5.14164 3.45609H19.9982C21.4661 3.45609 22.58 4.73418 22.3933 6.21235L21.6833 10.8581C21.2253 12.6732 19.6822 13.9475 17.8001 13.9475H6.64857L6.88394 15.5862C6.9429 16.112 7.43712 16.5076 8.07376 16.5076H19.6527C20.1498 16.5076 20.5527 16.9105 20.5527 17.4076C20.5527 17.9047 20.1498 18.3076 19.6527 18.3076H8.07376C6.56002 18.3076 5.26125 17.2681 5.0987 15.8145L4.83069 13.9475H4.82863L4.29024 10.1826L3.35997 3.70221C3.20982 2.95828 2.63917 2.47358 1.92639 2.47358ZM6.39003 12.1475H17.8001C18.8358 12.1475 19.6768 11.4531 19.9208 10.5028L20.6106 5.96454C20.6592 5.57907 20.3777 5.25609 19.9982 5.25609H5.40404L6.27774 11.3658L6.39003 12.1475Z" fill="white"></path>
									<path d="M17.1053 23.0611C18.0645 23.0611 18.8421 22.2905 18.8421 21.34C18.8421 20.3895 18.0645 19.6189 17.1053 19.6189C16.146 19.6189 15.3684 20.3895 15.3684 21.34C15.3684 22.2905 16.146 23.0611 17.1053 23.0611Z" fill="#65BD00"></path>
									<path d="M8.26321 23.0611C9.22244 23.0611 10.0001 22.2905 10.0001 21.34C10.0001 20.3895 9.22244 19.6189 8.26321 19.6189C7.30398 19.6189 6.52637 20.3895 6.52637 21.34C6.52637 22.2905 7.30398 23.0611 8.26321 23.0611Z" fill="#65BD00"></path>
								</svg>
								<i class="fas fa-chevron-right"></i>
							</button>
						</div>
            <?php if ($steps) { ?>
              <div class="checkout_step d-md-flex flex-row align-items-center mb-5 flex-wrap d-none ">
								<?php foreach ($steps as $step) { ?>
									<span class="label_step <?php echo $step['class']; ?>" id="to_<?php echo $step['name']; ?>">
									<?php if ($step['href']) { ?>
										<a href="<?php echo $step['href']; ?>" onclick="checkout.loadStep('<?php echo $step['name']; ?>', event)">
										<?php echo $step['text']; ?>
										</a>
									<?php } else { ?>
										<?php echo $step['text']; ?>
									<?php } ?>
								</span>
								<?php } ?>
              </div>
            <?php } ?>
						<?php if ($messages) { ?>
							<div id="messages">
								<?php foreach ($messages as $message) { ?>
									<div class="alert alert-<?php echo $message['type']; ?> col-12">
										<?php echo $message['text']; ?>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="checkout_content" id="checkout_content">
					<div id="step_<?php echo $current_step; ?>">
						<?php echo $content; ?>
					</div>
				</div>
			</div>
			<?php echo $cart; ?>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('#customselect').SelectCustomizer();
	});
</script>

</html>
