<?php if ($gift_programs) { ?>
  <h2 class="mt-4"><?php echo $text_gift; ?></h2>
  <?php foreach ($gift_programs as $gift_program_id => $program) { ?>
    <div class="gift_block">
      <?php $checked = 'checked'; ?>
      <?php foreach ($program['gifts'] as $gift) { ?>
      <div class="gift_item">
        <input type="radio" <?php echo $checked; ?> name="gifts[<?php echo $gift_program_id; ?>][]" value="<?php echo $gift['product_id']; ?>" id="gift-product<?php echo $gift['product_id']; ?>">
        <?php $checked = ''; ?>
        <label for="gift-product<?php echo $gift['product_id']; ?>">
          <img src="<?php echo $gift['image']; ?>" alt="<?php echo $gift['name']; ?>" title="<?php echo $gift['name']; ?>">
          <p><?php echo $gift['name']; ?></p>
          <p><?php echo $text_quantity; ?>: <?php echo $gift['quantity']; ?></p>
          <a href="<?php echo $gift['href']; ?>">
            <?php echo $text_more; ?>
          </a>
        </label>
      </div>
      <?php } ?>
    </div>
  <?php } ?>
<?php } ?>
