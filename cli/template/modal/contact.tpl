<div class="modal-content">
  <div class="modal-header">
    <p class="modal-title" id="ModalCenterTitle"><?php echo $title; ?></p>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <svg width="20" height="20" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">            <path d="M0.209705 0.387101L0.292893 0.292893C0.653377 -0.0675907 1.22061 -0.0953203 1.6129 0.209705L1.70711 0.292893L8.5 7.085L15.2929 0.292893C15.6534 -0.0675907 16.2206 -0.0953203 16.6129 0.209705L16.7071 0.292893C17.0676 0.653377 17.0953 1.22061 16.7903 1.6129L16.7071 1.70711L9.915 8.5L16.7071 15.2929C17.0676 15.6534 17.0953 16.2206 16.7903 16.6129L16.7071 16.7071C16.3466 17.0676 15.7794 17.0953 15.3871 16.7903L15.2929 16.7071L8.5 9.915L1.70711 16.7071C1.34662 17.0676 0.779392 17.0953 0.387101 16.7903L0.292893 16.7071C-0.0675907 16.3466 -0.0953203 15.7794 0.209705 15.3871L0.292893 15.2929L7.085 8.5L0.292893 1.70711C-0.0675907 1.34662 -0.0953203 0.779392 0.209705 0.387101Z" fill="#000"></path>
          </svg>
    </button>
  </div>
  <div class="modal-body" id="contact">
    <form method="post" id="contact_form" <?php if ($form) { ?> action="<?php echo $form['action']; ?>" name="<?php echo $form['name']; ?>" <?php } ?>>
      <?php if ($form) { ?>
        <div class="form_review">
          <?php foreach ($form['fields'] as $field_name => $field) { ?>
            <div class="d-flex flex-row justify-content-center mt-2">
              <div class="form-field w-100">
              <?php if ($field['type'] === 'textarea') { ?>
                <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label']; ?></label>
                <textarea name="<?php echo $field_name; ?>" class="form-control"
                value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder=""></textarea>
              <?php } else { ?>
                  <label for="<?php echo $field_name; ?>" class="for_input"><?php echo $field['label']; ?></label>
                  <input type="<?php echo $field['type']; ?>" name="<?php echo $field_name; ?>" class="form-control"
                  value="<?php echo $field['value']; ?>" id="<?php echo $field_name; ?>" placeholder="">
              <?php } ?>
            </div>
            </div>
          <?php } ?>
          <button type="submit" alt="<?php echo $button_submit; ?>" title="" rel="" onclick="sendContact(event)" class="cpt_product_add2cart_button green_button" id="submit_contact">
            <?php echo $button_submit; ?>
          </button>
        </div>
      <?php } ?>
    </form>
  </div>
  <div class="modal-footer">
  </div>
</div>
