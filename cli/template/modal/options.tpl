<div class="modal-content">
  <div class="modal-header">
    <span class="cpt_product_name"><?php echo $name; ?></span>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <svg width="20" height="20" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">            <path d="M0.209705 0.387101L0.292893 0.292893C0.653377 -0.0675907 1.22061 -0.0953203 1.6129 0.209705L1.70711 0.292893L8.5 7.085L15.2929 0.292893C15.6534 -0.0675907 16.2206 -0.0953203 16.6129 0.209705L16.7071 0.292893C17.0676 0.653377 17.0953 1.22061 16.7903 1.6129L16.7071 1.70711L9.915 8.5L16.7071 15.2929C17.0676 15.6534 17.0953 16.2206 16.7903 16.6129L16.7071 16.7071C16.3466 17.0676 15.7794 17.0953 15.3871 16.7903L15.2929 16.7071L8.5 9.915L1.70711 16.7071C1.34662 17.0676 0.779392 17.0953 0.387101 16.7903L0.292893 16.7071C-0.0675907 16.3466 -0.0953203 15.7794 0.209705 15.3871L0.292893 15.2929L7.085 8.5L0.292893 1.70711C-0.0675907 1.34662 -0.0953203 0.779392 0.209705 0.387101Z" fill="#000"></path>
          </svg>
    </button>
  </div>
  <div class="modal-body" id="product">
    <form id="product_form">
      <input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
      <div>
        <?php	 if(!empty($price)) {
       if(!empty($special) ) {?>
        <span id="totalprice" class="totalPrice price_sale new_price"><?=$special?></span>
        <span class="regularPrice price_sale old_price"><?=$price?></span>
        <p id="saving_text"><?php echo $text_economy; ?>&nbsp;<span class="saving_price"><?php echo $saving; ?></span></p>
        <?php } else { ?>
          <span id="totalprice" class="totalPrice new_price"><?=$price?></span>
          <?php } ?>
        <?php } ?>
      </div>
      <?php if ($options) { ?>
      <div class="product_options">
        <?php foreach ($options as $option) { ?>
          <?php if (is_array($option)) { ?>
        <div class="cpt_product_params_selectable">
          <span><?php echo $option['name']; ?>:</span>

          <div class="option_select">
            <?php $checked = 'checked'; ?>
            <?php foreach ($option['product_option_value'] as $option_value) { ?>
            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"
            id="<?php echo $option_value['product_option_value_id']; ?>" data-price="<?php echo $option_value['price']; ?>"
            <?php if($option_value['old_price']) { ?>data-old-price="<?php echo $option_value['old_price']; ?>"<?php } ?>
            <?php echo $checked; ?>
            <?php if ($option_value['trigger_image']) { ?>data-image="<?php echo $option_value['trigger_image']; ?>"<?php } ?>
            onchange="addOption(this)">

            <label for="<?php echo $option_value['product_option_value_id']; ?>">
              <?php if ($option_value['image']) { ?>
                <?php if ($option_value['image']['extension'] === 'svg') { ?>
                  <?php echo $option_value['image']['value']; ?>
                <?php } else { ?>
                  <img src="<?php echo $option_value['image']['value']; ?>" alt="<?php echo $option_value['name']; ?>" title="<?php echo $option_value['name']; ?>">
                <?php } ?>
              <?php } else { ?>
                <?php echo $option_value['name']; ?>
              <?php } ?>
              </label>
              <?php $checked = ''; ?>
              <?php  }?>
          </div>

        </div>
      <?php } ?>
      <?php } ?>
        <div class="d-flex flex-wrap justify-content-between align-items-end">
          <div class="product_quantity">
            <span><?php echo $entry_qty; ?>:</span>
            <div>
              <input type="button" onclick="plus(this, '-1')" value=" - " class="count-icon-m">
              <input name="quantity" id="acpro_inp_1777" class="product_qty" type="text" data-quantity="1" value="1" onchange="recount(this)">
              <input type="button" onclick="plus(this, '1')" value=" + " class="count-icon-p">
            </div>
          </div>
          <!-- <p class="advantage"></p> -->
        </div>
      </div>

    <?php } ?>
    <button type="submit" data-toggle="model" onclick="cart.add(<?php echo $product_id; ?>, 1, event)" class="cpt_product_add2cart_button green_button"><?php echo $button_cart; ?></button>
    </form>
  </div>
  <div class="modal-footer"></div>
</div>
