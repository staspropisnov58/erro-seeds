<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
        <div id="content" class="col-12 col-md-8 order-1 order-md-2">
        <div class="h1"><h1><?php echo $heading_title; ?></h1></div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="wr-reg-page col-12 newsletter_block p-0">
          <fieldset style="margin: 20px 0;">
            <div>
              <div class="col-12 col-12 p-0">
                <div class="d-flex p-0 align-items-center">
                  <p class="col-10 flleft"><label class="newsletter_label"><?php echo $entry_newsletter; ?></label></p>
                  <div class="col-2 flleft">
                    <div class="switch_block mx-auto">
                      <?php if ($newsletter == 1) { ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter1" value="1" checked>
                      <label for="newsletter1" class="switch-label switch-label-off"></label>
                    <?php }else{ ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter1" value="1">
                      <label for="newsletter1" class="switch-label switch-label-off"></label>
                    <?php } ?>
                      <?php if ($newsletter == 0) { ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter2" value="0" checked>
                      <label for="newsletter2" class="switch-label switch-label-on"></label>
                    <?php }else{ ?>
                      <input type="radio" class="switch-input" name="newsletter" id="newsletter2" value="0">
                      <label for="newsletter2" class="switch-label switch-label-on"></label>
                    <?php } ?>
                      <span class="switch-selection"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 col-12 p-0">
                <div class="d-flex p-0 flex-column">
                  <div class="thead col-12 clearfix">
                    <div class="row">
                      <p class=" col-12">
                        <?php echo $text_notification_settings; ?>
                      </p>
                      <p class="col-8 flleft">
                        <?php echo $text_notification_title; ?>
                      </p>
                      <p class="col-2 text-center">
                        <?php echo $text_notification_email; ?>
                      </p>
                      <?php  if (isset($sms_customer_notification)){ ?>
                      <p class="col-2 text-center">
                        <?php echo $text_notification_sms; ?>
                      </p>
                    <?php } ?>
                    </div>
                  </div>
                  <?php foreach ($notifications as $notification){ ?>
                  <div class="tbody col-12 clearfix">
                    <div class="d-flex flex-row align-items-center">
                      <div class="text-left col-8 flleft p-0"><label class="newsletter_label"><?php echo $notification['entry']; ?></label></div>
                      <div class="text-left col-2 flleft ">
                        <div class="form-group" style="text-align: center;">
                          <div class="switch_block mx-auto">
                            <?php if ($notification['email_value'] == 1){ ?>

                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>1" value="1" checked>
                            <label for="email_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>1" value="1">
                            <label for="email_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php } ?>
                            <?php if ($notification['email_value'] == 0){ ?>
                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>0" value="0" checked>
                            <label for="email_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="email_<?php echo $notification['name'] ?>" id="email_<?php echo $notification['name'] ?>0" value="0">
                            <label for="email_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php } ?>
                            <span class="switch-selection"></span>
                          </div>
                        </div>
                      </div>
                    <?php  if (isset($sms_customer_notification)){ ?>
                      <div class="text-right col-2 flleft ">
                        <div class="form-group" style="text-align: center;">
                          <div class="switch_block mx-auto">
                            <?php if ($notification['sms_value'] == 1){ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>1" value="1" checked>
                            <label for="sms_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>1" value="1">
                            <label for="sms_<?php echo $notification['name'] ?>1" class="switch-label switch-label-off"></label>
                            <?php } ?>
                            <?php if ($notification['sms_value'] == 0){ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>0" value="0" checked>
                            <label for="sms_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php }else{ ?>
                            <input type="radio" class="switch-input" name="sms_<?php echo $notification['name'] ?>" id="sms_<?php echo $notification['name'] ?>0" value="0">
                            <label for="sms_<?php echo $notification['name'] ?>0" class="switch-label switch-label-on"></label>
                            <?php } ?>
                            <span class="switch-selection"></span>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </fieldset>
          <div class="col-12">
            <div class="d-flex p-0">
              <input type="checkbox" id="default" name="default" class="custom_checkbox"/>
              <label for="default" class="for_checkbox"><?php echo $text_for_default; ?><label>
            </div>
          </div>
          <div class="buttons d-flex justify-content-between">
            <button class="form-btn green_button d-flex mx-auto" style="cursor: pointer"><?=$button_continue?></button>

            <!-- <div><a href="<?php echo $continue; ?>" class="green_button btn"><?php echo $button_continue; ?></a></div> -->
          </div>
        </form>
      </div>
        <?php echo $column_right; ?></div>
      </div>
    </div>
    <?php echo $content_bottom; ?>
<?php echo $footer; ?>
