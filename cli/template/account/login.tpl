<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <?php if ($success) { ?>
    <div class="alert alert-success col-12"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger col-12"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="wr-reg-page">
      <div class="d-flex align-items-center header_acc vhod col-12 col-sm-8 col-md-8 col-lg-7 mx-auto">
        <div class="d-flex flex-row w-100 justify-content-start">
          <div class="a-reg"><?php echo $heading_title; ?> </div>
          <span class="slesh">/</span>
          <div class="a-reg">
            <a href="<?php echo $register; ?>" class="color-theme"><?php echo $text_register; ?></a>
          </div>
        </div>
      </div>
      <form class="col-12 col-sm-8 col-md-8 col-lg-7 mx-auto" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <?php if ($error_warning) { ?>
          <p class="error"><?php echo $error_warning; ?></p>
          <?php }?>
          <label for="formEmail" class="white_label"><?php echo $entry_email; ?></label>
          <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo  $entry_email; ?>" id="formEmail" class="form-control trans-input form-control-success" />
        </div>
        <div class="form-group">
          <label for="inputPassword" class="white_label"><?php echo $entry_password; ?></label>
          <input type="password" id="inputPassword" name="password" class="form-control trans-input" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>">
        </div>
        <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
        <button class="green_button d-flex"><?php echo $text_login; ?></button>
      </form>

      </div>
    </div>
  </div>
</div>
<script>
    $('input').on('change',function () {
        if($(this).val()){

            $(this).parent().addClass('has-success')
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');

        }
    });
</script>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
