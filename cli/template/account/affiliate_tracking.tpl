<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
        <div id="content" class="col-12 col-md-8 order-1 order-md-2">
          <?php if ($success) { ?>
              <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
          <?php } ?>
        <div class="col-12">
          <?php if(!empty($affiliate_id)){ ?>
            <div class="col-sm-12 affiliate_id">
              <?php echo $column_right; ?>
            </div>
          <div class="h1"><h1><?php echo $heading_title; ?></h1></div>
          <p><?php echo $text_description; ?></p>
          <form class="wr-reg-page col-12">
            <div class="form-group">
              <label class="white_label" for="input-code"><?php echo $entry_code; ?></label>
                <textarea readonly cols="40" rows="1" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control trans-input form-control-success"><?php echo $code; ?></textarea>

            </div>
            <div class="form-group">
              <label class="white_label" for="input-code"><?php echo $text_affiliate_comission; ?></label>
                <textarea readonly cols="40" rows="1" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control trans-input form-control-success"><?php echo $commission; ?></textarea>
            </div>
            <div class="form-group">
              <label class="white_label" for="input-generator"><span data-toggle="tooltip" title="<?php echo $help_generator; ?>"><?php echo $entry_generator; ?></span></label>
              <input type="text" name="product" value="" placeholder="<?php echo $help_generator; ?>" id="input-generator" class="form-control trans-input form-control-success" />
            </div>
            <div class="form-group">
              <label class="white_label" for="input-link"><?php echo $entry_link; ?></label>
                <textarea name="link" cols="40" rows="5" placeholder="<?php echo $entry_link; ?>" id="input-link" class="form-control trans-input form-control-success"></textarea>
            </div>
          </form>
          <div class="buttons">
            <div><a href="<?php echo $continue; ?>" class="green_button btn"><?php echo $button_continue; ?></a></div>
          </div>
        <?php }else{ ?>
          <div class="col-sm-12 affiliate_id2">
            <?php echo $column_right; ?>
          </div>
            <form method="post" enctype="multipart/form-data" class="wr-reg-page">
            <div class="form-group">
              <input type="checkbox" name="affiliate" id="affiliate" class="custom_checkbox">
              <label for="affiliate" class="white_label for_checkbox">
                <?php echo $text_new_my_affiliate; ?>
              </label>
            </div>
              <button type="submit" formmethod="post" class="green_button" ><?php echo $text_new_affiliate_button;?></button>
            </form>
          <?php } ?>

        </div>
      </div>

    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
