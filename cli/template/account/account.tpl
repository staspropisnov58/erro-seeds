<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
      <div id="content" class="col-12 col-md-8 order-1 order-md-2">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <h2><?php echo $text_my_account; ?></h2>
        <ul class="list-unstyled">
          <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
          <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
          <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
        </ul>
        <h2><?php echo $text_my_orders; ?></h2>
        <ul class="list-unstyled">
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        </ul>
        <h2><?php echo $text_my_newsletter; ?></h2>
        <ul class="list-unstyled">
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
        <?php if ($tuning == 1){ ?>
        <h2><?php echo $text_affiliate; ?></h2>
        <ul class="list-unstyled">
          <li><a href="<?php echo $information; ?>"><?php echo $text_affiliate_information; ?></a></li>
        </ul>
        <?php if (!empty($affiliate_id)){ ?>
        <ul class="list-unstyled">
          <li><a href="<?php echo $bonuses; ?>"><?php echo $text_bonuses_only_affiliate; ?></a></li>
        </ul>
        <ul class="list-unstyled">
          <li><a href="<?php echo $payouts; ?>"><?php echo $text_payouts_affiliate; ?></a></li>
        </ul>
        <ul class="list-unstyled">
          <li><a href="<?php echo $unclosed_orders; ?>"><?php echo $text_unclosed_orders; ?></a></li>
        </ul>

        <?php }else{ ?>
        <?php } ?>
        <?php } ?>
      </div>
      <?php echo $column_right; ?>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
