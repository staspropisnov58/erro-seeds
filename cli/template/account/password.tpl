<?php echo $header; ?>
<div class="container">
  <div class="account_page">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <?php echo $content_top; ?>
    <div class="col-12 p-0 d-flex flex-wrap">
      <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
      <div id="content" class="col-12 col-md-8 order-1 order-md-2">
        <div class="h1"><h1><?php echo $heading_title; ?></h1></div>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="wr-reg-page col-10 p-0">
          <fieldset>
            <div class="form-group">
              <label for="inputPassword" class="white_label"><?php echo $entry_password; ?></label>
              <input type="password" name="password" id="input-password" class="form-control trans-input">
              <?php if ($error_password){ ?>
              <label for="inputPassword" class="error"><?php echo $error_password; ?></label>
              <?php } ?>
            </div>
            <!-- <div class="form-group">
                        <?php // if ($error_confirm){ ?>
                            <p style="color: #ff3333;font-size: 18px;"><?php echo $error_confirm; ?></p>
                        <?php // } ?>
                            <label for="inputPassword2" class="white_label"><?php // echo $entry_confirm; ?></label>
                            <input type="password" name="confirm" id="input-confirm" class="form-control trans-input" >
                        </div> -->


          </fieldset>
          <div class="buttons clearfix">

            <div>
              <input type="submit" value="<?php echo $button_continue; ?>" class="green_button btn" />
            </div>
          </div>
        </form>
      </div>
      <?php echo $column_right; ?>
    </div>
  </div>
</div>
<script>
    $('input').on('change', function () {
        if ($(this).val()) {

            $(this).parent().addClass('has-success');
            $(this).closest('.form-group')
                .find('p')
                .fadeOut('slow');
            $(this).closest('.form-group')
                .find('label')
                .fadeIn('slow');
        }
    });
</script>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
