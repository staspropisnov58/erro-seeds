<?php echo $header; ?>
<style>
   .wish-btn-prod.active{
     background: #ffb0b0fa;
     color: red;
   }
</style>

    <div class="container">
      <div class="row col-12 m-auto col-lg-10 p-0 col-12">
        <div class="back-cat ">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
          <?php if (isset($breadcrumb['href'])) { ?>
          <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
          <?php } else { echo $breadcrumb['text'] . '  '; }
          } ?>
        </div>
        <?php echo $content_top; ?>
        <div class="col-12 p-0 d-flex flex-wrap">
          <div class="col-12 col-md-4 p-0 order-2 order-md-1"><?php echo $column_left; ?></div>
            <div id="content" class="col-12 col-md-8 order-1 order-md-2">
                <div class="wrapper-wwr row flex-wrap justify-content-between">
                    <div class="col-12 col-md-12 content-wrapper">
                        <div class="catalog-item row">
                            <?php
                            $i = 0;
                            foreach ($products as $product) {
                                $i++;
                                ?>
                                <div class="col-12 col-md-6 col-lg-3 wr-page-product-wrap">
                                    <div class="product-wrap">
                                        <div class="foto-prod">
                                            <img src="<?php echo $product['thumb']; ?>" alt="" class="img-fluid d-block w-100">
                                            <div class="hiden-opt">
                                                <div class="dop-opt">
                                                    <?php $k = 0;
                                                    foreach ($product['attribute_groups'][0]["attribute"] as $attribute) {
                                                        $k++;
                                                        ?>
                                                        <div class="wr-opt-prd">
                                                            <div class="opt-prd">
                                                              <?php if (isset($attribute["svg"])){ ?>
                                                                <?php echo $attribute["svg"]; ?>
                                                              <?php }else{ ?>
                                                                  <?php if(isset($attribute["image"])){ ?>
                                                                    <img src="<?php echo $attribute["image"] ?>" alt="">
                                                                  <?php } ?>
                                                              <?php } ?>
                                                              <span><?= $attribute["text"]; ?></span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="dop-prod-btn d-flex">
                                                  <?php if ($product['in_compare']) {?>
                                                    <button type="button" style="border: none;cursor: pointer"
                                                            data-toggle="tooltip" class="sr-btn-prod active"
                                                            title="<?php echo $text_in_compare; ?>"
                                                            onclick="window.location.href='<?=$product['in_compare']?>';return false;"><i class="fas fa-balance-scale"></i></button>
                                                  <?php } else { ?>
                                                    <button type="button" style="border: none;cursor: pointer"
                                                            data-toggle="tooltip" class="sr-btn-prod"
                                                            title="<?php echo $button_compare; ?>"
                                                            onclick="compare.add('<?php echo $product['product_id']; ?>', this);return false;"><i class="fas fa-balance-scale"></i></button>
                                                  <?php } ?>
                                                    <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip"
                                                       title="<?php echo $button_remove; ?>" class="wish-btn-prod active d-flex align-items-center justify-content-center">
                                                       <i class="fas fa-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="name-prod"><?php echo $product['name']; ?></div>
                                        <div class="d-flex flex-wrap justify-content-center align-items-center btn-mar">
                                            <button type="button" class="prod-btn"
                                                    style="background: #2a2a2a;cursor: pointer"
                                                    onclick="cart.add('<?php echo $product['product_id']; ?>');">
                                                <div class="d-flex align-items-center">
                                                    <div class="icon-prod-btn"><i class="fas fa-shopping-cart"></i></div>
                                                    <div class="text-prod-btn">
                                                        <?php echo mb_substr($product['price'], 1); ?>
                                                        <span><?php echo $button_cart; ?></span>
                                                    </div>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>
    </div>
<?php echo $content_bottom; ?>

    <?php echo $footer; ?>
