<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!--[if !mso]><!-->
	<meta http-equiv="X-Ua-Compatible" content="IE=edge">
	<!--<![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="GENERaTOR" content="MSHTML 11.00.9600.18666">
	<style type="text/css">
		.img {
			width: 30%
		}

		.inline1,
		.inline2 {
			width: 33%
		}

		b {
			color: #82b523
		}

		.recom {
			width: 24%;
			display: inline-block;
		}

		@media screen and (max-width: 600px) {
			.img {
				width: 40%
			}

			.inline1 {
				width: 57%
			}

			.inline2 {
				width: 100%
			}

			.recom {
				width: 49%;
				margin-bottom: 5px
			}
		}
	</style>
</head>

<body>
	<div class="container"
		style="width: 100%; background-image: url(https://res.cloudinary.com/seoboost/image/upload/v1570713055/email_border_bg/order_bg.jpg); ">
		<center>
			<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px; font-size: 18px; font-family: Arial, Arial, Helvetica, sans-serif;">
				<tbody>
					<tr>
						<th height="40"></th>
					</tr>
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px; padding: 0 10px; background-color: #82b523">
								<tbody>
									<tr>
										<td style="width: 100%;max-width: 600px">
											<a href="" style="text-decoration: none;" target="_blank"><img style="" src="https://bit.ly/2wV9Nhw" alt=""></a>
										</td>
									</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="background-color: #FFFFFF;width: 100%; max-width: 600px; padding: 15px">
								<tbody>
									<tr>
										<td>
											<p style="font-size: 18px;line-height: 30px; margin-top: 0;">
												<?php echo $text_admission; ?>
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px;">
												<tbody>
													<?php foreach ($products as $product){ ?>


													<tr>
														<td>
															<a href="<?php echo $product['href']; ?> " target="_blank" style="display: block;text-decoration: none; color: #181818;padding: 5px;background-color: #2e2e2e;color: #FFFFFF;border-radius: 5px">
																<div style="margin-right: 1%;display: inline-block;    vertical-align: middle;" class="img">
																	<img src="<?php echo $product['image']; ?>" alt="" style="display: block; width: 100%; border-radius: 5px">
																</div>
																<div style="display: inline-block; vertical-align: top;" class="inline1">
																	<p style="font-size: 22px; color: #82b523;margin:0 0 10px"><?php echo $product['name']; ?></p>
																	<small style="display: block;margin: 10px 0"><?php echo $product['manufacturer']; ?></small>

													<tr>
														<td>
															<a href="<?php echo $product['href']; ?> " target="_blank" style="display: block;text-decoration: none; color: #181818;padding: 5px;background-color: #2e2e2e;color: #FFFFFF;border-radius: 5px">
																<div style="margin-right: 1%;display: inline-block;    vertical-align: middle;" class="img">
																	<img src="<?php echo $product['image']; ?>" alt="" style="display: block; width: 100%; border-radius: 5px">
																</div>
																<div style="display: inline-block; vertical-align: top;" class="inline1">
																	<p style="font-size: 22px; color: #82b523;margin:0 0 10px"><?php echo $product['name']; ?></p>
																	<small style="display: block;margin: 10px 0"><?php echo $product['manufacturer']; ?></small>

																	<!-- если скидка -->
																	<?php if((int)$product['product_special_id'] !== 0 ){ ?>
																	<span style="color: #b12525;font-size: 22px;font-weight: 600;display: block;"><?php echo $product['special']; ?></span>
																	<span style="text-decoration: line-through;display: block;"><?php echo $product['price']; ?></span>
																	<?php }else{ ?>
																	<span style="font-size: 22px;font-weight: 600;display: block;"><?php echo $product['price']; ?></span>
																	<?php } ?>
																	<!--  -->
																</div>
																<div style="display: inline-block; font-size: 14px; line-height: 22px;vertical-align: top;" class="inline2">
																	<span style="display: block;"><b><?php echo $text_manufacturer; ?> </b> <?php echo $product['manufacturer']; ?></span>
																	<?php $count = 0; ?>
																	<?php foreach ($product['option'] as $option){ ?>
																	<?php foreach($option['attribute'] as $attribute){ ?>
																	<?php if($count < 4){ ?>
																	<span style="display: block;"><b> <?php echo $attribute['name']; ?>: </b> <?php echo $attribute['text']; ?> </span>
																	<?php } ?>
																	<?php $count++ ?>

																	<?php } ?>
																	<?php } ?>

																</div>
															</a>
														</td>
													</tr>
													<tr>
														<td style="height: 10px"></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td style="height: 10px"></td>
									</tr>
									<tr>
										<td style="padding: 15px">
											<hr>
											<p style="font-size: 18px;line-height: 30px;">
												<?php echo $test_related; ?>
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px;">
												<tbody>
													<tr>
														<td style="width: 100%;">
															<?php $count = 0; ?>
															<?php foreach ($related_products as $related_product){ ?>
															<?php if($count <= 4){ ?>
															<div class="recom">
																<a href="<?php echo $related_product['href']; ?>" target="_blank" style="display: block;text-decoration: none; color: #FFFFFF;text-align: center;">
																	<div style="max-height: 177px;overflow: hidden;">
																		<img src="<?php echo $related_product['image'];?>" alt="" style="min-width: 80px; border-radius: 5px 5px 0 0;width: 100%; display: block;">
																	</div>
																	<div style="background-color: #2e2e2e; border-radius: 0 0 5px 5px;">
																		<p style="padding: 15px 5px;margin: 0;font-size: 15px;font-weight: 600;"><?php echo $related_product['name']; ?></p>
																		<?php if((int)$related_product['product_special_id'] !== 0 ){ ?>
																		<span style="color: #b12525;display: block;text-align: center;padding: 5px 5px 10px;"><?php echo $related_product['special']; ?></span>
																		<span style="text-decoration: line-through;display: block;text-align: center;padding: 5px 5px 10px;"><?php echo $related_product['price']; ?></span>
																		<?php }else{ ?>
																		<span style="display: block;text-align: center;padding: 5px 5px 10px;"><?php echo $related_product['price']; ?></span>
																		<?php } ?>
																	</div>
																</a>
															</div>
															<?php } ?>
															<?php $count++; ?>
															<?php } ?>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px; padding-bottom: 50px; font-family: Arial, Arial, Helvetica, sans-serif; ">
								<tbody>
									<tr>
										<td style="width: 100%; text-align: center;">
											<a href="<?php echo $store_url; ?>" target="_blank" style="text-decoration: none;">
												<p style="color: #656464">© Errors Seeds 2009 - <?=date('Y');?></p>
										</td>
									</tr>
									<tr>
										<td style="width: 100%;">
											<center>
												<div>
													<a href="https://www.facebook.com/ESseeds" style="text-decoration:none;display:inline-block;width:50px" target="_blank">
														<img src="https://res.cloudinary.com/seoboost/image/upload/v1570697967/email_icon/unnamed_1.png" alt="facebook" style="padding:5px; width:30px">
													</a>
													<a href="https://www.instagram.com/errorsseeds_ua/" style="text-decoration:none;display:inline-block;width:50px" target="_blank">
														<img src="https://res.cloudinary.com/seoboost/image/upload/v1570697969/email_icon/unnamed_2.png" alt="instagram" style="padding:5px;  width:30px">
													</a>
												</div>
											</center>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</center>
	</div>
</body>

</html>
