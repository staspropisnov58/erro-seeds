<?php echo $header; ?>
<div class="slider_top container">
  <?php echo $content_top; ?>
</div>
<div class="container">
  <div class="main_content_text" id="main_reports">
    <div class="back-cat ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php if (isset($breadcrumb['href'])) { ?>
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text'] . ' <i class="fas fa-chevron-right"></i> '; ?></a></li>
      <?php } else { echo $breadcrumb['text'] . '  '; }
      } ?>
    </div>
    <div class="h1">
      <h1><?php echo $text_testimonials; ?></h1></div>
    <?php echo $description; ?>
    <div class="testimonial_top">
      <div id="cat_product_sort" class="d-flex flex-row justify-content-between align-items-center flex-wrap">
        <div class="navigator_product">
          <?php echo $pagination; ?>
        </div>
        <div>
          <button type="button" onclick="<?php echo $write_review; ?>" class="green_button review_btn"><?php echo $text_write; ?></button>
        </div>
      </div>
    </div>
    <div class="testimonial reviews review_block">
      <?php foreach ($reviews as $review) { ?>
      <div class="review_item">
        <div class="review">
          <div class="review_date">
            <?php echo $review['author']; ?>&nbsp;<small>(<?php echo $review['date_added']; ?>)</small>
          </div>
          <?php if ($review['title']) { ?>
          <h3 class="review_title"><?php echo $review['title']; ?></h3>
          <?php } ?>
          <div class="review_rating">
            <?php for ($i = 1; $i <= 5; $i++) { ?>
            <?php if ($review['rating'] < $i) { ?>
            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.47942 1.42285L9.018 4.54041C9.12499 4.75724 9.33186 4.90748 9.57117 4.9422L13.0117 5.44216C13.6144 5.5298 13.8548 6.27021 13.4189 6.69504L10.9293 9.12171C10.7563 9.29045 10.6772 9.53374 10.7182 9.77191L11.3058 13.1985C11.4088 13.7986 10.7788 14.2562 10.2399 13.9731L7.16271 12.3554C6.94873 12.243 6.69292 12.243 6.47893 12.3554L3.40178 13.9731C2.86283 14.2565 2.23283 13.7986 2.33584 13.1985L2.92344 9.77191C2.96442 9.53374 2.88531 9.29045 2.71231 9.12171L0.222754 6.69504C-0.213181 6.26992 0.0272657 5.52951 0.629949 5.44216L4.07048 4.9422C4.30979 4.90748 4.51666 4.75724 4.62365 4.54041L6.16223 1.42285C6.43141 0.876791 7.20995 0.876791 7.47942 1.42285Z" stroke="white"/>
                      </svg>
            <?php } else { ?>
            <svg width="15" height="15" viewBox="0 0 15 15" fill="#65bd00" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.47942 1.42285L9.018 4.54041C9.12499 4.75724 9.33186 4.90748 9.57117 4.9422L13.0117 5.44216C13.6144 5.5298 13.8548 6.27021 13.4189 6.69504L10.9293 9.12171C10.7563 9.29045 10.6772 9.53374 10.7182 9.77191L11.3058 13.1985C11.4088 13.7986 10.7788 14.2562 10.2399 13.9731L7.16271 12.3554C6.94873 12.243 6.69292 12.243 6.47893 12.3554L3.40178 13.9731C2.86283 14.2565 2.23283 13.7986 2.33584 13.1985L2.92344 9.77191C2.96442 9.53374 2.88531 9.29045 2.71231 9.12171L0.222754 6.69504C-0.213181 6.26992 0.0272657 5.52951 0.629949 5.44216L4.07048 4.9422C4.30979 4.90748 4.51666 4.75724 4.62365 4.54041L6.16223 1.42285C6.43141 0.876791 7.20995 0.876791 7.47942 1.42285Z" stroke="white"/>
                      </svg>
            <?php } ?>
            <?php } ?>
          </div>
          <div class="review_content">
            <?php echo $review['text']; ?>
          </div>
          <div class="actions">
            <!-- <a href="#" class="review-reply"><i class="fas fa-reply-all"></i> ответить</a> -->
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="d-flex flex-row justify-content-between align-items-center flex-wrap">
        <div class="navigator_product">
          <?php echo $pagination; ?>
        </div>
        <div>
          <button type="button" onclick="<?php echo $write_review; ?>" class="green_button review_btn"><?php echo $text_write; ?></button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
