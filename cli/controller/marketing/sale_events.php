<?php
class ControllerMarketingSaleEvents extends Controller
{
    public function enableSaleEvent($sale_event_id = 0)
    {

      $this->event_log->write($sale_event_id);

        if ($sale_event_id) {
            $sale_event_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON (se.sale_event_id = sed.sale_event_id) WHERE se.sale_event_id = '" . (int)$sale_event_id . "' AND sed.language = 2");

            if ($sale_event_query->num_rows) {
                $this->event_log->write('Старт включения распродажи ' . $sale_event_query->row['name'] . ' id = ' . $sale_event_id);


                if (isset($sale_event_id) && $sale_event_id && $sale_event_id != 0) {

          //Включаем скидки

                    $this->db->query("UPDATE " . DB_PREFIX . "product_special SET status = 1 WHERE sale_event_id = '" . (int)$sale_event_id . "'");


                    $this->event_log->write('Скидки включены');


                    //Включаем новость

                    $this->db->query("UPDATE " . DB_PREFIX . "news SET status = 1 WHERE sale_event_id = '" . (int)$sale_event_id . "'");

                    $this->event_log->write('Новость включена');


                    //Включаем баннеры

                    $this->db->query("UPDATE " . DB_PREFIX . "banner_image SET status = 1 WHERE sale_event_id = '" . (int)$sale_event_id . "'");

                    $this->event_log->write('Баннер включен');



                    // Отключаем купоны

                    if ($sale_event_query->row['disable_coupon']!='') {
                        $disabled_coupons = unserialize($sale_event_query->row['disable_coupon']);

                        foreach ($disabled_coupons as $coupon_id) {
                            $this->db->query("UPDATE " . DB_PREFIX . "coupon SET status = 0 WHERE coupon_id = '" . (int)$coupon_id . "'");
                        }

                        $this->event_log->write('Купоны отключены');
                    }

                    $this->event_log->write('Распродажа ' . $sale_event_query->row['name'] . ' id = ' . $sale_event_id . ' включена');
                }
            } else {
                $this->error_log->write('Распродажа с id = ' . $sale_event_id . ' не найдена');
            }
        } else {
            $this->error_log->write('Передан id распродажи 0 или не передан вообще.');
        }
    }



    public function disableSaleEvent($sale_event_id = 0)
    {
        if ($sale_event_id) {
            $this->event_log->write("SELECT * FROM " . DB_PREFIX . "sale_event se LEFT JOIN " . DB_PREFIX . "sale_event_description sed ON (se.sale_event_id = sed.sale_event_id) WHERE se.sale_event_id = '" . (int)$sale_event_id . "' AND sed.language = '" . $this->config->get('config_language_id') . "'");

            if ($sale_event_query->num_rows) {
                $this->event_log->write('Старт выключения распродажи ' . $sale_event_query->row['name'] . ' id = ' . $sale_event_id);

                //Выключаем баннеры

                $this->db->query("UPDATE " . DB_PREFIX . "banner_image SET status = 0 WHERE sale_event_id = '" . (int)$sale_event_id . "'");

                $this->event_log->write('Баннер выключен');

                //Включаем купоны

                if ($sale_event_query->row['disable_coupon']!='') {
                    $disabled_coupons = unserialize($sale_event_query->row['disable_coupon']);

                    foreach ($disabled_coupons as $coupon_id) {
                        $this->db->query("UPDATE " . DB_PREFIX . "coupon SET status = 1 WHERE coupon_id = '" . (int)$coupon_id . "'");
                    }

                    $this->event_log->write('Купоны включены');

                    $this->event_log->write('Распродажа ' . $sale_event_query->row['name'] . ' id = ' . $sale_event_id . ' выключена');
                }
            } else {
                $this->error_log->write('Распродажа с id = ' . $sale_event_id . ' не найдена');
            }
        } else {
            $this->error_log->write('Передан id распродажи 0 или не передан вообще.');
        }
    }
}
