<?php
class ControllerToolTextGenerator extends Controller
{
  public function generateSeoCategoryDescription(){

    $this->load->model('localisation/language');
    $this->load->model('catalog/categories');
    $this->load->model('tool/url_builder');

    $year = date('Y');


    $languages = $this->model_localisation_language->getLanguages();
    $this->load->model('localisation/language');



    foreach ($languages as $language_array) {


      $language = new Language($language_array['directory']);

      $language->load('tool/text_generator');

      $filter_data = array(
        'language_id' => $language_array['language_id'],
        'product_type' => 'seeds',
      );

      $this->load->model('catalog/products');

      $categories = $this->model_catalog_categories->getCategories($filter_data);

      foreach ($categories as $category) {
        $data = array();
        $data['text_product'] = $language->get('text_product');
        $month = $language->get('text_in_' . strtolower(date('F')));
        $data['text_price'] = $language->get('text_price');
        $data['text_seeds'] = $language->get('text_seeds');
        $data['text_heading_question'] = sprintf($language->get('text_heading_question'), $category['name']);
        $data['text_small_price_question'] = sprintf($language->get('text_small_price_question'), $category['name']);
        $data['text_small_price_subtitle'] = sprintf($language->get('text_small_price_subtitle'), $category['name']);
        $data['text_heading_popular_question'] = sprintf($language->get('text_heading_popular_question'), $category['name'], $month, $year);
        $data['text_heading_popular'] = sprintf($language->get('text_heading_popular'), $category['name'], $month, $year);
        $data['text_big_price_question'] = sprintf($language->get('text_big_price_question'), $category['name']);
        $data['text_big_price_subtitle'] = sprintf($language->get('text_big_price_subtitle'), $category['name']);
        $data['text_products_subtitle']  = sprintf($language->get('text_products_subtitle'), $category['name'], $month, $year);

        $popular_products_data = array();
        $popular_products = array();

        $popular_products_data = $this->model_catalog_products->getPopularProducts(['limit'=>5, 'category_id'=>$category['category_id'], 'language_id'=>$language_array['language_id'], 'product_type'=>'seeds']);

        foreach ($popular_products_data as $popular_product) {
          $popular_products[] = array(
            'product_id' => $popular_product['product_id'],
            'href' => parse_url($this->model_tool_url_builder->createLink('product/product', 'product_id=' . $popular_product['product_id'], true))['path'],
            'name' => $popular_product['name'],
            'price' => isset($product_small_price['special']) ? $this->currency->format($this->tax->calculate($popular_product['special'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight() : $this->currency->format($this->tax->calculate($popular_product['price'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight(),

          );
        }



        $products_small_price_data = array();
        $products_small_price = array();

        $products_small_price_data = $this->model_catalog_products->getProducts(['limit'=>5, 'category_id'=>$category['category_id'], 'language_id'=>$language_array['language_id'], 'sort'=>'p.price', 'order'=>'ASC', 'product_type'=>'seeds', 'avaliable' => true]);


        foreach ($products_small_price_data as $product_small_price) {
          $products_small_price[] = array(
            'product_id' => $product_small_price['product_id'],
            'href' => parse_url($this->model_tool_url_builder->createLink('product/product', 'product_id=' . $product_small_price['product_id'], true))['path'],
            'name' => $product_small_price['name'],
            'price' => isset($product_small_price['special']) ? $this->currency->format($this->tax->calculate($product_small_price['special'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight() : $this->currency->format($this->tax->calculate($product_small_price['price'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight(),

          );
        }



        $products_big_price_data = array();
        $products_big_price = array();

        $products_big_price_data = $this->model_catalog_products->getProducts(['limit'=>5, 'category_id'=>$category['category_id'], 'language_id'=>$language_array['language_id'], 'sort'=>'p.price', 'order'=>'DESC','product_type'=>'seeds', 'avaliable' => true]);

        foreach ($products_big_price_data as $product_big_price) {
          $products_big_price[] = array(
            'product_id' => $product_big_price['product_id'],
            'href' => parse_url($this->model_tool_url_builder->createLink('product/product', 'product_id=' . $product_big_price['product_id'], true))['path'],
            'name' => $product_big_price['name'],
            'price' => isset($product['special']) ? $this->currency->format($this->tax->calculate($product_big_price['special'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight() : $this->currency->format($this->tax->calculate($product_big_price['price'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight(),
          );
        }


        $products_data = array();
        $products = array();

        $products_data = $this->model_catalog_products->getProducts(['limit'=>5, 'category_id'=>$category['category_id'], 'language_id'=>$language_array['language_id'], 'sort'=>'p.product_id', 'order'=>'ASC', 'product_type'=>'seeds', 'avaliable' => true]);

        foreach ($products_data as $product) {
          $products[] = array(
            'product_id' => $product['product_id'],
            'href' => parse_url($this->model_tool_url_builder->createLink('product/product', 'product_id=' . $product['product_id'], true))['path'],
            'name' => $product['name'],
            'price' => isset($product['special']) ? $this->currency->format($this->tax->calculate($product['special'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight() : $this->currency->format($this->tax->calculate($product['price'], $this->config->get('config_tax')), '', '', false) . ' ' . $this->currency->getSymbolRight(),
          );
        }



        $data['category'] = array(
          'category_id' => $category['category_id'],
          'name'  =>  $category['name'],
          'popular_product'  => $popular_products,
          'product_small_price'  => $products_small_price,
          'product_big_price'  => $products_big_price,
          'product_data'  => $products,
        );

        $seo_text = $this->load->view('default/template/tool/text_generator.tpl', $data);

        $data_category = array(
          'category_id' => $category['category_id'],
          'language_id' => $language_array['language_id'],
          'description' => $seo_text,
        );

        $this->model_catalog_categories->addSeoDescription($data_category);

      }

    }



  }


}
