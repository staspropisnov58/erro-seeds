<?php
class ControllerFeedYML extends Controller
{
  private const FILE_PATH = '/var/www/www-root/data/www/errors-seeds.com.ua/feed/';

  public function update($args = [])
  {
      $xmlstr = '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd">';
      $xmlstr .= '<yml_catalog date="' . date('Y-m-d H:i:s') . '"></yml_catalog>';

      $this->load->model('localisation/language');
      $languages = $this->model_localisation_language->getLanguages();
      $default_language_id = $this->config->get('config_language_id');
      foreach ($languages as $language) {
        $this->config->set('config_language_id', $language['language_id']);
        $this->yml = new SimpleXMLElement($xmlstr);
        $this->shop = $this->yml->addChild('shop', '');

        $this->event_log->write('setShopInfo');
        $this->setShopInfo();
        $this->event_log->write('addCurrencies');
        $this->addCurrencies();
        $this->event_log->write('addCategories');
        $this->addCategories();
        $this->event_log->write('addOffers');
        $this->addOffers();

        $this->yml->asXML(self::FILE_PATH . 'feed_' . $language['code'] . '.xml');
      }

      $this->config->set('config_language_id', $default_language_id);

      $next_time = new DateTime('tomorrow');
      $next_time->modify('+4 hour');
      $this->load->model('tool/task');
      $this->model_tool_task->addTask('feed/yml/update', $next_time);
  }

  private function setShopInfo()
  {
    $this->shop->addChild('name', $this->config->get('config_name'));
    $this->shop->addChild('company', $this->config->get('config_owner'));
    $this->shop->addChild('url', HTTPS_CATALOG);
    $this->shop->addChild('phone', $this->config->get('config_telephone'));
    $this->shop->addChild('platform', 'Yandex.YML for OpenCart (seoboost)');
    $this->shop->addChild('version', '1.0');
  }

  private function addCurrencies()
  {
    $currencies_node = $this->shop->addChild('currencies', '');
    $this->load->model('localisation/currency');
    $currencies = $this->model_localisation_currency->getCurrencies();

    foreach ($currencies as $currency) {
      $currency_node = $currencies_node->addChild('currency', '');
      $currency_node->addAttribute('id', $currency['code']);
      $currency_node->addAttribute('rate', $currency['value']);
    }
  }

  private function addCategories()
  {
    $categories_node = $this->shop->addChild('categories', '');
    $this->load->model('catalog/categories');
    $categories = $this->model_catalog_categories->getCategories();

    foreach ($categories as $category) {
      $category_node = $categories_node->addChild('category', $category['name']);
      $category_node->addAttribute('id', $category['category_id']);
      if ($category['parent_id']) {
        $category_node->addAttribute('parentId', $category['parent_id']);
      }
    }
  }

  private function addOffers()
  {
    $offers_node = $this->shop->addChild('offers', '');

    $this->load->model('catalog/products');
    $this->load->model('catalog/product');
    $this->load->model('catalog/option');

    $products = $this->model_catalog_products->getProducts();

    foreach ($products as $product) {
      $offer_node = $offers_node->addChild('offer', '');
      $offer_node->addAttribute('id', $product['product_id']);
      $available = $product['quantity'] > 0 ? 'true' : 'false';
      $offer_node->addAttribute('available', $available);

      $this->load->model('tool/url_builder');
      $offer_node->addChild('url', $this->model_tool_url_builder->createLink('product/product', 'product_id=' . $product['product_id'], true));

      $price = $this->setPrice($product);

      $offer_node->addChild('price', $price->getPrice());
      $offer_node->addChild('oldprice', $price->getOldPrice());

      $offer_node->addChild('currencyId', $this->config->get('config_currency'));
      $offer_node->addChild('categoryId', $product['category_id']);

      $offer_node->addChild('picture', HTTPS_CATALOG . 'image/' . $product['image']);
      $this->setAdditionalPictures($offer_node, $product['product_id']);

      $offer_node->addChild('delivery', $available);

      $offer_node->addChild('name', $product['name']);
      $offer_node->addChild('vendor', $product['manufacturer']);
      $offer_node->addChild('vendorCode', '');
      $offer_node->addChild('description', '<![CDATA[' . $product['description'] . ']]>');
      $offer_node->addChild('country_of_origin', '');

      $this->setOptionsAsParams($offer_node, $product['product_id'], $price);
      $this->setAttributesAsParams($offer_node, $product['product_id']);
    }
  }

  private function setPrice(array $product)
  {
    $price = new Price($product['price']);

    $price->setSpecial($this->model_catalog_product->getProductSpecial($product['product_id']));

    //TODO: нужно поправить disount для 1 штуки
    $first_option_prices = $this->model_catalog_option->getFirstAvailableOptionsPrices($price, $product['product_id']);
    if ($first_option_prices) {
      foreach ($first_option_prices as $option_price) {
        $price->addOptionPrice($option_price);
      }
    }

    return $price;
  }

  private function setAdditionalPictures(&$offer_node, int $product_id)
  {
    $images = $this->model_catalog_product->getProductImages($product_id);

    foreach ($images as $image) {
      $offer_node->addChild('picture', HTTPS_CATALOG . 'image/' . $image['image']);
    }
  }

  private function setOptionsAsParams(&$offer_node, int $product_id, Price $price)
  {
    $options = $this->model_catalog_option->getOptionsWithPreparedPrices($price, $product_id);

    foreach ($options as $option) {
      foreach ($option['product_option_value'] as $value) {
        $value_node = $offer_node->addChild('param', $price->getPrice() + (float)($value['price_prefix'] . $value['price']));
        $value_node->addAttribute('name', $option['name'] . ': ' . $value['name']);
      }
    }
  }

  private function setAttributesAsParams(&$offer_node, int $product_id)
  {
    $attributes = $this->model_catalog_product->getProductAttributes($product_id);

    foreach ($attributes as $attribute_group) {
      foreach ($attribute_group['attribute'] as $attribute) {
        $attribute_node = $offer_node->addChild('param', $attribute['text']);
        $attribute_node->addAttribute('name', $attribute['name']);
      }
    }
  }
}
