<?php
class ModelCatalogCategories extends Model
{
  public function getCategories($data=array()) {
    $sql = "SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id)";

    if(isset($data['language_id']) && $data['language_id'] != ''){
      $sql .= " WHERE cd.language_id = '" . (int)$data['language_id'] . "' ";

    }else{
      $sql .= " WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";
    }

    if(isset($data['product_type']) && $data['product_type'] != ''){
      $sql .= " AND c.product_type = '" . $data['product_type'] . "'";
    }

     $sql .= " AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";

		$query = $this->db->query($sql);

		return $query->rows;
	}

  public function addSeoDescription($data){
    $this->db->query("UPDATE " . DB_PREFIX . "category_description SET description_2 = '" . $this->db->escape($data['description']) . "' WHERE language_id= '" . (int)$data['language_id'] . "' AND category_id = '" . (int)$data['category_id'] . "'");
  }
}
