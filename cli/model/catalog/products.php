<?php
class ModelCatalogProducts extends Model
{
  public function getProducts($data = array()) {
		$sql = "SELECT p.product_id,
                   p.price,
                   p.quantity,
                   p.image,
                   pd.name,
                   pd.description,
                   m.name AS manufacturer,
                   ptc.category_id,
                   (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))	ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
            FROM " . DB_PREFIX . "product p";

    $sort_data = array(
			'p.product_id',
			'p.price'
		);

		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)";

    $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category ptc ON p.product_id = ptc.product_id ";
    $sql .= " LEFT JOIN " . DB_PREFIX . "manufacturer m ON p.manufacturer_id = m.manufacturer_id WHERE p.status = 1 AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

    if(isset($data['language_id']) && $data['language_id'] != ''){
      $sql.= " AND pd.language_id = '" . (int)$data['language_id'] . "'";
    }else{
      $sql .= " AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
    }

    if(isset($data['category_id']) && $data['category_id'] != ''){

      $sql .= " AND ptc.category_id = '" . (int)$data['category_id'] . "'";
    }else{
      $sql .= " AND ptc.main_category = 1 ";
    }

    if(isset($data['product_type']) && $data['product_type'] != ''){
      $sql.= "  AND p.product_type = '" . $data['product_type'] . "'";
    }

    if (isset($data['avaliable']) && $data['avaliable']) {
      $sql .= " AND p.quantity > 0 ";
    }

		$sql .= " GROUP BY p.product_id";

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

      $sql.= " ORDER BY '" . $data['sort'] . "'";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
    if(isset($data['limit']) && $data['limit'] != ''){
      $sql .= " LIMIT ". (int)$data['limit'];
    }


		$query = $this->db->query($sql);

		return $query->rows;
	}

  public function getPopularProducts($data){

    $sql = "SELECT p.product_id, p.price, pd.name, p.tax_class_id, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))	ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id=pd.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0";

    if(isset($data['category_id']) && $data['category_id'] != ''){
      $sql .= " AND p2c.category_id = '" . (int)$data['category_id'] . "' ";
    }
    if(isset($data['language_id']) && $data['language_id'] != ''){
      $sql .= " AND pd.language_id='" . (int)$data['language_id'] . "' ";
    }

    if(isset($data['product_type']) && $data['product_type'] != ''){
      $sql .= " AND p.product_type ='" . $data['product_type'] . "'";
    }

    if (isset($data['avaliable']) && $data['avaliable']) {
      $sql .= " AND p.quantity > 0 ";
    }

    $sql .= " ORDER BY p.viewed DESC, p.date_added DESC ";

    if(isset($data['product_type']) && $data['product_type'] != ''){
      $sql .= " LIMIT ". (int)$data['limit'];
    }

    $query = $this->db->query($sql);

    return $query->rows;
  }


}
