<?php
class ModelToolTask extends Model
{
  public function addTask(string $action, DateTime $datetime_run, array $args = [])
  {
    $this->db->query("INSERT INTO " . DB_PREFIX . "task SET
                      datetime_run = '" . $datetime_run->format('Y-m-d H:i:s') . "',
                      action = '" . $this->db->escape($action) . "',
                      args = '" . serialize($args) . "'");
  }

  public function updateTask($task_id){

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "task WHERE task_id= '" . (int)$task_id . "'");

    if($query->num_rows){

      if($query->row['frequency'] === 'once'){
        $this->db->query("DELETE FROM " . DB_PREFIX . "task WHERE task_id = '" . (int)$task_id . "'");
      }else{

        $date = new DateTime($query->row['datetime_run']);

        $date->modify('+1 '. $query->row['frequency']);

        $new_datetime_run = $date->format('Y-m-d H:i:s');

        $this->db->query("UPDATE " . DB_PREFIX . "task SET datetime_run = '" . $new_datetime_run . "' WHERE task_id='" . (int)$task_id . "'");

      }
    }
  }
}
