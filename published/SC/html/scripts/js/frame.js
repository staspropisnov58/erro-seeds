function open_printable_version(d) {
  var c = "menubar=no,location=no,resizable=yes,scrollbars=yes";
  newWin = window.open(d, "printableWin", c);
  if (newWin) {
    newWin.focus()
  }
}

function confirmUnsubscribe() {
  temp = window.confirm(translate.cnfrm_unsubscribe);
  if (temp) {
    window.location = "index.php?killuser=yes"
  }
}

function validate() {
  if (document.subscription_form.email.value.length < 1) {
    alert(translate.err_input_email);
    return false
  }
  if (document.subscription_form.email.value == "Email") {
    alert(translate.err_input_email);
    return false
  }
  return true
}

function validate_disc() {
  if (document.formD.nick.value.length < 1) {
    alert(translate.err_input_nickname);
    return false
  }
  if (document.formD.topic.value.length < 1) {
    alert(translate.err_input_message_subject);
    return false
  }
  return true
}

function validate_search() {
  if (document.Sform.price1.value != "" && ((document.Sform.price1.value < 0) || isNaN(document.Sform.price1.value))) {
    alert(translate.err_input_price);
    return false
  }
  if (document.Sform.price2.value != "" && ((document.Sform.price2.value < 0) || isNaN(document.Sform.price2.value))) {
    alert(translate.err_input_price);
    return false
  }
  return true
}

function validate_input_digit(k) {
  var i;
  var j;
  var l;
  try {
    if (window.event) {
      i = window.event.keyCode
    } else {
      if (k.which) {
        i = k.which
      }
    }
  } catch (e) {
    alert(e.message)
  }
  i = parseInt(i);
  if (i == 13) {
    return false
  }
  if (i >= 33 && i <= 40) {
    return true
  }
  if (i == 8) {
    return true
  }
  if (i == 17) {
    return true
  }
  if (i == 45) {
    return true
  }
  if (i == 46) {
    return true
  }
  if (i >= 96 && i <= 105) {
    i -= 48
  }
  j = String.fromCharCode(i);
  l = /\d/;
  return l.test(j);
  var h = l.test(j);
  return h
}
Behaviour.register({
  "input.input_message": function(b) {
    b.onfocus = function() {
      this.className = this.className.replace(/input_message/, "") + " input_message_focus";
      var a = this.getAttribute("rel");
      if (!a) {
        a = this.getAttribute("title")
      }
      if (this.value != a) {
        return
      }
      this.value = ""
    };
    b.onblur = function() {
      if (this.value != "") {
        return
      }
      this.className = this.className.replace(/input_message_focus/, "") + " input_message";
      var a = this.getAttribute("rel");
      if (!a) {
        a = this.getAttribute("title")
      }
      this.value = a
    }
  },
  ".add2cart_handler": function(b) {
    b.onclick = function() {
      var h = getFormByElem(this);
      if (!h) {
        return true
      }
      var l = getElementsByClass("product_option", h);
      var i = "";
      for (var j = l.length - 1; j >= 0; j--) {
        if (!parseInt(l[j].value)) {
          continue
        }
        if (l[j].name) {
          i += "&" + l[j].name + "=" + parseInt(l[j].value)
        }
      }
      var a = getElementByClass("product_qty", h);
      if (a) {
        a = parseInt(a.value);
        if (a > 1) {
          i += "&product_qty=" + a
        }
      }
      var k = ORIG_LANG_URL + set_query("?ukey=cart&view=noframe&action=add_product&" + i + "&productID=" + h.getAttribute("rel"), "");
      openFadeIFrame(k);
      return false
    }
  },
  ".hndl_proceed_checkout": function(b) {
    b.onclick = function() {
      openFadeIFrame(ORIG_LANG_URL + set_query("?ukey=cart&view=noframe"));
      return false
    }
  },
  "input.goto": function(b) {
    b.onclick = function() {
      if (this.className.search(/confirm/) !== -1 && !window.confirm(this.getAttribute("title"))) {
        return
      }
      document.location.href = this.getAttribute("rel")
    }
  },
  ".gofromfade": function(b) {
    b.onclick = function() {
      parent.document.location.href = this.href;
      parent.closeFadeIFrame();
      return false
    }
  },
  ".notification_request_handler": function(b) {
    b.onclick = function() {
      var d = getFormByElem(this);
      if (!d) {
        return true
      }
      var a = ORIG_LANG_URL + set_query("?ukey=notification_request&ProductID=" + d.getAttribute("rel") + "&view=noframe", "");
      openFadeIFrame(a);
      return false
    }
  },
  ".callback": function(b) {
    b.onclick = function() {
      var a = ORIG_LANG_URL + set_query("?ukey=feedback&view=noframe&type=callback", "");
      openFadeIFrame(a);
      return false
    }
  },
  "input.digit": function(b) {
    b.onkeydown = function(a) {
      return validate_input_digit(a)
    }
  }
});
Behaviour.addLoadEvent(function() {
  setTimeout(function() {
    if (formatPrice && defaultCurrency && defaultCurrency.getView) {
      var t = getElementsByClass("totalPrice");
      for (var s = t.length - 1; s >= 0; s--) {
        var n = getFormByElem(t[s]);
        if (!n) {
          continue
        }
        var m = getElementsByClass("product_option", n);
        var o = parseFloat(getElementByClass("product_price", n).value.replace(/,/, "."));
        var k = 0;
        var r = getElementByClass("product_list_price", n);
        if (r) {
          k = parseFloat(r.value.replace(/,/, "."))
        }
        for (var q = m.length - 1; q >= 0; q--) {
          var p = select_getCurrOption(m[q]);
          if (!p) {
            continue
          }
          o += parseFloat(p.getAttribute("rel"));
          k += parseFloat(p.getAttribute("rel"))
        }
        var i = ("" + formatPrice(o));
        if (i.length > 0) {
          t[s].innerHTML = i
        }
        var r = getElementByClass("regularPrice", n);
        if (r) {
          r.innerHTML = formatPrice(k)
        }
        var r = getElementByClass("youSavePrice", n);
        if (r) {
          r.innerHTML = formatPrice(k - o) + " (" + Math.round((k - o) / k * 100, 2) + "%)"
        }
      }
    }
  }, 500)
});
