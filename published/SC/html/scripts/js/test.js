var checkboxHeight = "25";
var radioHeight = "25";
var selectWidth = "90";
document.write('<style type="text/css">input.styled { display: none; } select.styled { position: relative; width: ' + selectWidth + "px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>");
var Custom = {
  init: function() {
    var e = document.getElementsByTagName("input"),
      g = Array(),
      c, f, h;
    for (a = 0; a < e.length; a++) {
      if ((e[a].type == "checkbox" || e[a].type == "radio") && e[a].className == "styled") {
        g[a] = document.createElement("span");
        g[a].className = e[a].type;
        if (e[a].checked == true) {
          if (e[a].type == "checkbox") {
            position = "0 -" + (checkboxHeight * 2) + "px";
            g[a].style.backgroundPosition = position
          } else {
            position = "0 -" + (radioHeight * 2) + "px";
            g[a].style.backgroundPosition = position
          }
        }
        e[a].parentNode.insertBefore(g[a], e[a]);
        e[a].onchange = Custom.clear;
        if (!e[a].getAttribute("disabled")) {
          g[a].onmousedown = Custom.pushed;
          g[a].onmouseup = Custom.check
        } else {
          g[a].className = g[a].className += " disabled"
        }
      }
    }
    e = document.getElementsByTagName("select");
    for (a = 0; a < e.length; a++) {
      if (e[a].className == "styled") {
        f = e[a].getElementsByTagName("option");
        h = f[0].childNodes[0].nodeValue;
        c = document.createTextNode(h);
        for (b = 0; b < f.length; b++) {
          if (f[b].selected == true) {
            c = document.createTextNode(f[b].childNodes[0].nodeValue)
          }
        }
        g[a] = document.createElement("span");
        g[a].className = "select";
        g[a].id = "select" + e[a].name;
        g[a].appendChild(c);
        e[a].parentNode.insertBefore(g[a], e[a]);
        if (!e[a].getAttribute("disabled")) {
          e[a].onchange = Custom.choose
        } else {
          e[a].previousSibling.className = e[a].previousSibling.className += " disabled"
        }
      }
    }
    document.onmouseup = Custom.clear
  },
  pushed: function() {
    element = this.nextSibling;
    if (element.checked == true && element.type == "checkbox") {
      this.style.backgroundPosition = "0 -" + checkboxHeight * 3 + "px"
    } else {
      if (element.checked == true && element.type == "radio") {
        this.style.backgroundPosition = "0 -" + radioHeight * 3 + "px"
      } else {
        if (element.checked != true && element.type == "checkbox") {
          this.style.backgroundPosition = "0 -" + checkboxHeight + "px"
        } else {
          this.style.backgroundPosition = "0 -" + radioHeight + "px"
        }
      }
    }
  },
  check: function() {
    element = this.nextSibling;
    if (element.checked == true && element.type == "checkbox") {
      this.style.backgroundPosition = "0 0";
      element.checked = false
    } else {
      if (element.type == "checkbox") {
        this.style.backgroundPosition = "0 -" + checkboxHeight * 2 + "px"
      } else {
        this.style.backgroundPosition = "0 -" + radioHeight * 2 + "px";
        group = this.nextSibling.name;
        inputs = document.getElementsByTagName("input");
        for (a = 0; a < inputs.length; a++) {
          if (inputs[a].name == group && inputs[a] != this.nextSibling) {
            inputs[a].previousSibling.style.backgroundPosition = "0 0"
          }
        }
      }
      element.checked = true
    }
  },
  clear: function() {
    inputs = document.getElementsByTagName("input");
    for (var c = 0; c < inputs.length; c++) {
      if (inputs[c].type == "checkbox" && inputs[c].checked == true && inputs[c].className == "styled") {
        inputs[c].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight * 2 + "px"
      } else {
        if (inputs[c].type == "checkbox" && inputs[c].className == "styled") {
          inputs[c].previousSibling.style.backgroundPosition = "0 0"
        } else {
          if (inputs[c].type == "radio" && inputs[c].checked == true && inputs[c].className == "styled") {
            inputs[c].previousSibling.style.backgroundPosition = "0 -" + radioHeight * 2 + "px"
          } else {
            if (inputs[c].type == "radio" && inputs[c].className == "styled") {
              inputs[c].previousSibling.style.backgroundPosition = "0 0"
            }
          }
        }
      }
    }
  },
  choose: function() {
    option = this.getElementsByTagName("option");
    for (d = 0; d < option.length; d++) {
      if (option[d].selected == true) {
        document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue
      }
    }
  }
};
window.onload = Custom.init;
$(document).ready(function() {
  $("#left_tab").hover(function() {
    $(this).stop(true).delay(150).animate({
      left: "-1px"
    }, 800)
  }, function() {
    $(this).stop(true).animate({
      left: "-287px"
    }, 800)
  })
});
$(document).ready(function accordion() {
  $(".accordion .accordion_title").click(function() {
    var c = $(this).next();
    if (c.is(":visible")) {
      c.slideUp(600, function() {});
      $(this).children().removeClass("active");
      $(this).removeClass("active")
    } else {
      $(".accordion .accordion_content").slideUp();
      $(".accordion .accordion_title").children().removeClass("active");
      $(".accordion_title").removeClass("active");
      c.slideToggle();
      $(this).children().addClass("active");
      $(this).addClass("active")
    }
  })
});
$(document).ready(function() {
  $(".image_referal").click(function() {
    var c = $(this);
    var e = c.attr("src");
    $("body").append("<div class='popup_referal'><div class='popup_bg'></div><img src='" + e + "' class='popup_img' /></div>");
    $(".popup_referal").fadeIn(200);
    $(".popup_bg").click(function() {
      $(".popup_referal").fadeOut(200);
      setTimeout(function() {
        $(".popup_referal").remove()
      }, 200)
    })
  })
});
$(document).ready(function() {
  $("#login_ref").click(function() {
    $("#trigg").trigger("click")
  })
});
$(document).ready(function() {
  $.validator.methods.email = function( value, element ) {
    return this.optional(element) || /^[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&\'*+\/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9]))*\.{1}(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/.test(value);
  }
  $.validator.addMethod("validName", function(value, element) {
    return this.optional(element) || /^[a-zA-ZА-ЯёЁа-я0-9  \'-]+$/.test(value);
  });
  $.validator.addMethod("validMime", function(value, element, params) {
    var file = element.files[0];
    return $.inArray(file.type, params) !== -1;
  });
  $.validator.addMethod("validSize", function(value, element) {
    var file = element.files[0];
    return file.size < 1000000;
  });
  var c = $("#consultation_form");
  var f = $("#consultation_form form");
  var choice = '';
  f.validate();
  $("#consultation_form input[name=name]").rules("add", {
    required: true,
    rangelength: [3, 30],
    validName: true,
    messages: {
      required: "Введите своё имя",
      rangelength: jQuery.validator.format("Длина имени может быть от {0} до {1} символов"),
      validName: "Имя содержит недопустимые символы"
    }
  });
  $("#consultation").click(function() {
    $("#order_a_cons").attr("data-theme", $(this).attr("data-theme"));
    c.fadeIn(200)
  });
  $('#choice_tel').click(function() {
    choice = "tel";
    $("#consultation_email").hide();
    $("#consultation_form input[name=mail]").rules("remove");
    $(".consultation_wrapper form, #consultation_tel, #order_a_cons").show();
    $("#consultation_form input[name=tel]").rules("add", {
      required: true,
      rangelength: [9, 14],
      digits: true,
      messages: {
        required: "Введите свой телефон",
        rangelength: jQuery.validator.format("Длина телефона может быть от {0} до {1} символов"),
        digits: "Только цифры!"
      }
    });
  });
  $('#choice_email').click(function() {
    choice = "mail";
    $("#consultation_tel").hide();
    $("#consultation_form input[name=tel]").rules("remove");
    $(".consultation_wrapper form, #consultation_email, #order_a_cons").show();
    $("#consultation_form input[name=mail]").rules("add", {
      required: true,
      rangelength: [4, 50],
      email: true,
      messages: {
        required: "Введите свой email",
        rangelength: jQuery.validator.format("Длина email может быть от {0} до {1} символов"),
        email: "Email содержит недопустимые символы"
      }
    });
  });
  $("#consultation_form .close_form").click(function() {
    $("#order_a_cons").attr("data-theme", "");
    $("#consultation_form form").hide().trigger("reset");
    choice = "";
    c.fadeOut(200);
  });
  $("#order_a_cons").click(function() {
    f.valid();
    var data = {};
    data.callback = "yes";
    data.choice = choice;
    data.name = $("#consultation_form input[name=name]").val();
    data.connect_to = $("#consultation_form input[name=" + choice + "]").val();
    data.theme = $(this).attr("data-theme");
    $.ajax({
      url: "https://errors-seeds.com.ua/feedback",
      method: "POST",
      data: data,
      success: function(json) {
        $(".consultation_wrapper span").empty();
        $(".consultation_wrapper form").hide();
        $(".consultation_wrapper .choice_of_consultation").hide();
        $(".consultation_wrapper span").html("Спасибо. С вами свяжутся в ближайшее время");
        c.delay(4000).fadeOut(200)
      },
      error: function(f, h, g) {
        console.log("ERRORS: " + h)
      }
    });
  });
});
$(document).ready(function() {
  var hr = $("#hr_form");
  var hr_f = $("#hr_form form");
  $("#hr_button").click(function() {
    hr.fadeIn(200);
  });
  hr_f.validate({
    rules: {
      name: {
        required: true,
        rangelength: [3, 30],
        validName: true
      },
      tel: {
        required: true,
        rangelength: [9, 14],
        digits: true
      },
      mail: {
        required: true,
        rangelength: [4, 50],
        email: true
      },
      file: {
        required: true,
        validMime: ["application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword", ],
        validSize: true
      }
    },
    messages: {
      name: {
        required: "Введите своё имя",
        rangelength: jQuery.validator.format("Длина имени может быть от {0} до {1} символов"),
        validName: "Имя содержит недопустимые символы"
      },
      tel: {
        required: "Введите свой телефон",
        rangelength: jQuery.validator.format("Длина телефона может быть от {0} до {1} символов"),
        digits: "Только цифры!"
      },
      mail: {
        required: "Введите свой email",
        rangelength: jQuery.validator.format("Длина email может быть от {0} до {1} символов"),
        email: "Email содержит недопустимые символы"
      },
      file: {
        required: "Загрузите резюме в формате doc, docx или pdf",
        validMime: "Вы можете загрузить только файлы в формате doc, docx или pdf",
        validSize: "Максимальный размер файла - 1MB"
      }
    }
  });

  $("#hr_form .close_form").click(function() {
    hr.fadeOut(200);
  });
  $("#send_cv").click(function() {
    // hr_f.valid();
    var data = {};
    data.cv = "yes";
    data.name = $("#hr_form input[name=name]").val();
    data.tel = $("#hr_form input[name=tel]").val();
    data.mail = $("#hr_form input[name=mail]").val();
    var file_field = $("#hr_form input[name=file]");
    // var file = file_field[0].files[0];
    data.theme = $(this).attr("data-theme");
    var form_data = new FormData();
    jQuery.each(file_field[0].files, function(i, file) {
        console.log(file);
        form_data.append('file-'+i, file);
    });
    console.log(form_data);
    console.log(data);
    $.ajax({
      url: "https://errors-seeds.com.ua/formsender/index.php",
      method: "POST",
      data: form_data,
      constentType: false,
      processData: false,
      cache: false,
      success: function(f) {
        console.log(f);
        $(".hr_wrapper span").empty();
        $(".hr_wrapper form").hide();
        $(".hr_wrapper span").html("Спасибо. С вами свяжутся в ближайшее время");
        hr.delay(4000).fadeOut(200)
      },
      error: function(f, h, g) {
        console.log("ERRORS: " + h)
      }
    });
  })
});
$("a.hidli").click(function() {
  window.open($(this).attr("rel"));
  return false
});
$(document).ready(function() {
  $("#arrow_hide2").click(function() {
    var c = $(this).next();
    if (c.is(":visible")) {
      c.slideUp(600, function() {});
      $(this).children().removeClass("active");
    } else {
      c.slideToggle();
      $(this).children().addClass("active");
    }
  });
});
// $(document).ready(function() {
//         $(function() {
//             $(".vertical .carousel").jCarouselLite({
//                 btnNext: ".vertical .down",
//                 btnPrev: ".vertical .up",
//                 vertical: true
//             });
//         });
// });
$(document).ready(function(){
  $(".header_menu li:has(ul)").addClass("dropdown_menu")
});
$(document).ready(function(){

	// hide #back-top first
	$("#back-top").hide();

	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

  var offset = $("#feedback").offset();
  var height_b = screen.height;
  var topPadding = height_b/2;
  $(window).scroll(function() {
    if ($(window).scrollTop() > offset.top) {
      $("#feedback").stop().animate({
        marginTop: $(window).scrollTop() - offset.top + topPadding
      });
    } else {
      $("#feedback").stop().animate({
        marginTop: 0
      });
    }
  });

  $(".animated-icon").click(function () {
    $(this).toggleClass("open")
  })
});
$(document).ready(function(){
$("#drop_user").click(function(){
  $(this).toggleClass('active');
  $(".drop_user_menu").toggleClass('active');
});
$("#drop_menu_acc").click(function(){
  $(this).toggleClass('active');
  $(".drop_menu_acc").toggleClass('active');
});
$(".ocfilter button").click(function(){
  $(".fa-angle-down").toggleClass('active');
});
});
$(document).ready(function(){
  $(".navbarNavaccount button").click(function() {
      $('html, body').animate({
          scrollTop: $("#column-left").offset().top
      }, 2000);
  });
  });

  $(document).ready(function() {
    if ($(window).width() <= '767'){
    $(".f_sub_head").addClass("mobi");
    $(".f_sub_head.mobi").click(function() {
      $(this).toggleClass("active");
      var c = $(this).next();
      if (c.is(":visible")) {
        c.slideUp(600, function() {});
        $(this).children().removeClass("active");
      } else {
        c.slideToggle();
        $(this).children().addClass("active");
      }
    });
  }
  });
