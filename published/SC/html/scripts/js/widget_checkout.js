if (!sswgt_CartManager) {
  var sswgt_CartManager = {
    prefix: "my_",
    mode: "frame",
    add2cart: function(a) {
      this.shop_url = a.getAttribute("rel");
      sizes = getPageSize();
      this.show(a.href + "&widgets=1", sizes[2] * 0.7, sizes[3] * 0.7);
      return false
    },
    go2cart: function(a) {
      this.shop_url = a.getAttribute("rel");
      sizes = getPageSize();
      this.show(a.href, sizes[2] * 0.7, sizes[3] * 0.7);
      return false
    },
    _show: function(width, height) {
      this.hide(true);
      // hideSelectBoxes();
      // hideFlash();
      this.div();
      this.base();
      if (this.mode == "frame") {
        this.frame(this.params.url, width, height)
      } else {
        if (this.mode == "layer") {
          this.layer(this.params.layer_id, width, height)
        }
      }
      showMessage();
      this.border(this.objMain);
      this.closeButton(this.objBorder);
      var old_onresize = window.onresize;
      window.onresize = function() {
        with(sswgt_CartManager) {
          if (!(objOverlay && objDiv)) {
            return
          }
          var arrayPageSize = getPageSize();
          objOverlay.style.width = arrayPageSize[2] + "px";
          objOverlay.style.height = arrayPageSize[3] + "px";
          objBase.style.width = arrayPageSize[2] + "px";
          objBase.style.height = arrayPageSize[3] + "px";
          if (sswgt_CartManager.mode == "frame") {
            sswgt_CartManager.frame(null, frameWidth, frameHeight)
          } else {
            if (sswgt_CartManager.mode == "layer") {
              this.layer(sswgt_CartManager.params.layer_id, frameWidth, frameHeight)
            }
          }
          border(sswgt_CartManager.objMain);
          closeButton(objBorder);
        }
        if (old_onresize) {
          old_onresize()
        }
      };
      var old_onkeydown = document.onkeydown;
      document.onkeydown = function(event) {
        event = event ? event : window.event;
        if (event) {
          switch (event.keyCode ? event.keyCode : event.which ? event.which : null) {
            case 27:
              sswgt_CartManager.hide(true)
          }
        }
        if (old_onkeydown) {
          old_onkeydown()
        }
      }
    },
    showLayer: function(c, b, a) {
      this.mode = "layer";
      this.params = {
        layer_id: c
      };
      this._show(b, a);
      showSelectBoxes()
    },
    show: function(b, c, a) {
      this.mode = "frame";
      this.params = {
        url: b
      };
      this._show(c, a)
    },
    resizeFrame: function(b, a) {
      if (b != null) {
        this.frameWidth = b
      }
      if (a != null) {
        this.frameHeight = a
      }
      this.frame(null, this.frameWidth, this.frameHeight);
      this.border(this.objFrame);
      this.closeButton(this.objBorder)
    },
    div: function() {
      this.objDiv = document.createElement("div");
      setStyle(this.objDiv, "display:none; zIndex:100; position: absolute; left:0; top: 0;");
      var a = document.getElementsByTagName("body").item(0);
      a.appendChild(this.objDiv);
      this.objDiv.style.backgroundAttachment = "fixed"
    },
    layer: function(f, b, a) {
      var g = getLayer(f);
      setStyle(g, "visibility:hidden; display: block");
      if (!b) {
        b = g.offsetWidth
      }
      if (!a) {
        a = g.offsetHeight
      }
      this.frameWidth = b;
      this.frameHeight = a;
      var c = getPageSize();
      var e = (c[2] - b) / 2;
      if (e < 0) {
        e = 0
      }
      var d = (c[3] - a) / 2;
      if (d < 10) {
        d = 10
      }
      this.objBase.appendChild(g);
      setStyle(g, "top:" + d + "px; left:" + e + "px; width:" + b + "px; height:" + a + "px;overflow: auto;zIndex:121; position:absolute;visibility:visible");
      this.objBase.style.visibility = "visible";
      this.objMain = g
    },
    frame: function(a, b, m) {
      this.frameWidth = b;
      this.frameHeight = m;
      var f = getPageSize();
      var d = parseInt((f[2] - b) / 2);
      if (d < 0) {
        d = 0
      }
      var h = parseInt((f[3] - m) / 2);
      if (h < 10) {
        h = 10
      }
      var k = document.getElementById(this.prefix + "frame");
      if (!k) {
        var j = document.createElement("div");
        j.style.display = "none";
        j.innerHTML = '<iframe id="' + this.prefix + 'frame" frameborder="0"></iframe>';
        this.objBase.appendChild(j);
        k = document.getElementById(this.prefix + "frame");
        this.objBase.appendChild(k);
        this.objBase.removeChild(j);
        setStyle(k, "zIndex:120; position:absolute; backgroundColor:#ffffff");
        var l = document.createElement("div");
        this.objBase.appendChild(l);
        setStyle(l, "backgroundColor:#ffffff; zIndex:121; position:absolute; padding:20px; left:" + parseInt(f[2] / 2 - 50 + 20) + "px; top:" + parseInt(f[3] / 2 - 50) + "px;visibility: visible;");
        // var g = document.createElement("img");
        // g.src = ((window.CONF_ON_WEBASYST || (this.shop_url.search("webasyst.net") != -1)) ? (this.shop_url.replace(/shop\//, "") + "shop/") : (this.shop_url + "published/SC/html/scripts/")) + "images_common/loading.gif";
        // l.appendChild(g);
        setTimeout(function() {
          k.src = a
        }, 100);
        var e = this.objBase;

        function c() {
          if (k.style.visibility != "visible") {
            var o = l;
            var n = e;
            setTimeout(function() {
              if (o && o.parentNode) {
                o.parentNode.removeChild(o);
                o = null
              }
              // n.style.visibility = "visible"
            }, 800)
          }
        }
        if (k.addEventListener) {
          k.addEventListener("load", c, false)
        } else {
          if (k.attachEvent) {
            k.attachEvent("onload", c)
          }
        }
        this.objFrame = k;
        this.objMain = this.objFrame
      }
      setStyle(k, "top:" + h + "px; left:" + d + "px; width:" + b + "px; height:" + m + "px")
    },
    base: function() {
      if (!/MSIE/.test(navigator.userAgent)) {
        var a = document.createElement("div");
        a.style.visibility = "hidden"
      } else {
        this.objDiv.innerHTML += '<div id="myBase" style=\'z-index:95; position:absolute; visibility:hidden; top: expression(parseInt(document.documentElement.scrollTop || document.body.scrollTop, 10)+"px"); left: expression(parseInt(document.documentElement.scrollLeft || document.body.scrollLeft, 10)+"px");\'></div>';
        var a = document.getElementById("myBase")
      }
      setStyle(a, "zIndex:95");
      if (!/MSIE/.test(navigator.userAgent)) {
        setStyle(a, "top:0; left:0; position: fixed")
      }
      var b = getPageSize();
      a.style.width = b[2] + "px";
      a.style.height = b[3] + "px";
      a.onclick = function(c) {
        if (getEventObject(c).target.id && getEventObject(c).target.id == this.id) {
          sswgt_CartManager.hide(true)
        }
      };
      this.objDiv.insertBefore(a, this.objDiv.firstChild);
      this.objBase = a;
      this.overlay()
    },
    overlay: function() {
      var c = document.getElementById(this.prefix + "overlay");
      if (!c) {
        c = document.createElement("div");
        c.id = this.prefix + "overlay";
        this.objBase.appendChild(c)
      }
      var e = 0;
      var d = 0;
      var b = this.objBase.offsetWidth;
      var a = this.objBase.offsetHeight;
      setStyle(c, "position:absolute; visibility: visible; top:" + d + "; left:" + e + "; width:" + b + "px; height:" + a + "px; backgroundColor:#000000");
      setOpacity(c, 0.7);
      c.onclick = function(f) {
        if (getEventObject(f).target.id && getEventObject(f).target.id == this.id) {
          sswgt_CartManager.hide(true)
        }
      };
      this.objOverlay = c
    },
    hide: function(a) {
      if (this.objDiv && this.objDiv.parentNode) {
        if (this.objOverlay) {
          this.objDiv.appendChild(this.objOverlay)
        }
        this.objDiv.style.display = "none";
        if (a) {
          if (this.mode == "layer") {
            setStyle(this.objMain, "display:none");
            setStyle(this.objMain, "visibility:hidden");
            document.body.appendChild(this.objMain)
          }
          this.objDiv.parentNode.removeChild(this.objDiv);
          this.objDiv = null;
          this.objOverlay = null
        }
        showSelectBoxes();
        showFlash()
      }
    },
    closeButton: function(parentObject) {
      with(this) {
        var objCloseButton = document.getElementById(prefix + "closeButton");
        if (!objCloseButton) {
          objCloseButton = document.createElement("img");
          objCloseButton.id = prefix + "closeButton"
        }
        var objFrame = this.objFrame || document.getElementById(this.prefix + "frame");
        if (objFrame) {
          var left = objFrame.offsetLeft + this.objFrame.offsetWidth - 22;
          var top = objFrame.offsetTop - 25;
          setStyle(objCloseButton, "position:absolute; top:" + top + "px; left:" + left + "px; cursor:hand;");
          objCloseButton.src = ((window.CONF_ON_WEBASYST || (this.shop_url.search("webasyst.net") != -1)) ? (this.shop_url.replace(/shop\//, "") + "shop/") : (this.shop_url + "published/SC/html/scripts/")) + "images_common/close.gif";
          objCloseButton.onclick = function() {
            sswgt_CartManager.hide(true);
            return false
          };
          objBase.appendChild(objCloseButton)
        }
      }
    },
    border: function(e) {
      var b = document.getElementById(this.prefix + "border");
      if (!b) {
        b = document.createElement("div");
        b.id = this.prefix + "border";
        this.objBase.appendChild(b)
      }
      var c = 0;
      var g = parseInt(e.style.left, 10) - c;
      var f = parseInt(e.style.top, 10) - c;
      var d = e.offsetWidth;
      var a = e.offsetHeight;
      if (/MSIE/.test(navigator.userAgent)) {
        d += c * 2;
        a += c * 2
      }
      setStyle(b, "position:absolute; top:" + f + "; left:" + g + "; width:" + d + "px; height:" + a + "px; border: " + c + "px solid #efefef");
      this.objBorder = b
    }
  };

  function showSelectBoxes() {
    var a = document.getElementsByTagName("select");
    for (i = 0; i != a.length; i++) {
      a[i].style.visibility = "visible"
    }
  }

  function hideSelectBoxes() {
    var a = document.getElementsByTagName("select");
    for (i = 0; i != a.length; i++) {
      if (!a[i].className.match(/div_fade_select/)) {
        a[i].style.visibility = "hidden"
      }
    }
  }

  function showFlash() {
    var b = document.getElementsByTagName("object");
    for (i = 0; i < b.length; i++) {
      b[i].style.visibility = "visible"
    }
    var a = document.getElementsByTagName("embed");
    for (i = 0; i < a.length; i++) {
      a[i].style.visibility = "visible"
    }
  }

  function hideFlash() {
    var b = document.getElementsByTagName("object");
    for (i = 0; i < b.length; i++) {
      b[i].style.visibility = "hidden"
    }
    var a = document.getElementsByTagName("embed");
    for (i = 0; i < a.length; i++) {
      a[i].style.visibility = "hidden"
    }
  }

  function getPageSize() {
    var c, a;
    if (window.innerHeight && window.scrollMaxY) {
      c = window.innerWidth + window.scrollMaxX;
      a = window.innerHeight + window.scrollMaxY
    } else {
      if (document.body.scrollHeight > document.body.offsetHeight) {
        c = document.body.scrollWidth;
        a = document.body.scrollHeight
      } else {
        c = document.body.offsetWidth;
        a = document.body.offsetHeight
      }
    }
    var b, d;
    if (self.innerHeight) {
      if (document.documentElement.clientWidth) {
        b = document.documentElement.clientWidth
      } else {
        b = self.innerWidth
      }
      d = self.innerHeight
    } else {
      if (document.documentElement && document.documentElement.clientHeight) {
        b = document.documentElement.clientWidth;
        d = document.documentElement.clientHeight
      } else {
        if (document.body) {
          b = document.body.clientWidth;
          d = document.body.clientHeight
        }
      }
    }
    if (a < d) {
      pageHeight = d
    } else {
      pageHeight = a
    }
    if (c < b) {
      pageWidth = c
    } else {
      pageWidth = b
    }
    arrayPageSize = new Array(pageWidth, pageHeight, b, d);
    return arrayPageSize
  }

  function setOpacity(a, b) {
    if (b == 1) {
      a.style.opacity = (/Gecko/.test(navigator.userAgent) && !/Konqueror|Safari|KHTML/.test(navigator.userAgent)) ? 0.999999 : null;
      if (/MSIE/.test(navigator.userAgent)) {
        a.style.filter = a.style.filter.replace(/alpha\([^\)]*\)/gi, "")
      }
    } else {
      if (b < 0.00001) {
        b = 0
      }
      a.style.opacity = b;
      if (/MSIE/.test(navigator.userAgent)) {
        a.style.filter = a.style.filter.replace(/alpha\([^\)]*\)/gi, "") + "alpha(opacity=" + b * 100 + ")"
      }
    }
  }

  function setStyle(obj, style_str) {
    var styles = style_str.split(";");
    with(obj) {
      for (var k = styles.length - 1; k >= 0; k--) {
        var _style = styles[k].split(":", 2);
        if (!_style[1]) {
          continue
        }
        _style[0] = _style[0].replace(/^\s|\s$/, "");
        _style[1] = _style[1].replace(/^\s|\s$/, "");
        style[_style[0]] = _style[1]
      }
    }
  }

  function getEventObject(b) {
    var a = {};
    b = b ? b : window.event;
    if (b.srcElement) {
      a.target = b.srcElement
    } else {
      a.target = b.target
    }
    a.ev = b;
    return a
  }
};
function showMessage() {
  console.log('in cart');
   $("#alert-cart").show().delay(5000).hide('slow');
  }
