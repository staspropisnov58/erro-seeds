 $(function() {
    


     var endDate = new Date(Date.parse("2016-11-24T15:30:00.0Z"));

     $('.countdown.styled').countdown({
         date: endDate,
         render: function(data) {
             $(this.el).html("<div><div class='backtimer'>"
                + this.leadingZeros(data.days, 2) 
                +"</div><p>дней</p></div><div><div class='backtimer'>" + 
                this.leadingZeros(data.hours, 2) + 
                " </div><p>часов</p></div><div><div class='backtimer'>" + 
                this.leadingZeros(data.min, 2) + "</div><p>минут</p></div><div><div class='backtimer'>"
                + this.leadingZeros(data.sec, 2) + "</div><p>секунд</p></div>");
         }
     });

     // End time for diff purposes
     var endTimeDiff = new Date().getTime() + 15000;
     // This is server's time
     var timeThere = new Date();
     // This is client's time (delayed)
     var timeHere = new Date(timeThere.getTime() - 5434);
     // Get the difference between client time and server time
     var diff_ms = timeHere.getTime() - timeThere.getTime();
     // Get the rounded difference in seconds
     var diff_s = diff_ms / 1000 | 0;

     var notice = [];
     notice.push('Server time: ' + timeThere.toDateString() + ' ' + timeThere.toTimeString());
     notice.push('Your time: ' + timeHere.toDateString() + ' ' + timeHere.toTimeString());
     notice.push('Time difference: ' + diff_s + ' seconds (' + diff_ms + ' milliseconds to be precise). Your time is a bit behind.');

 });

 window.setInterval(function(){
    var current = new Date();
    var expiry  = new Date(Date.parse("2016-11-24T15:30:00.0Z"));
    
    if(current.getTime()>expiry.getTime()){
      $('.timer_container').hide();
      $('.blackFriday_link').hide();
      $('.blackFriday_banner_link').show();
    }
},0);
window.setInterval(function(){
    var current = new Date();
    var expiry2 = new Date(Date.parse("2016-11-25T22:00:00.0Z"));
    
    if(current.getTime()>expiry2.getTime()){
      $('.count-down-block').css('visibility','hidden');
    }
},0);
