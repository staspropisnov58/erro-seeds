<?php
class Price
{
  private $price = 0.0;
  private $special = 0.0;
  private $special_id = 0;
  private $discount = 0.0;
  private $quantity = 1;
  private $option_price = 0.0;

  function __construct($price)
  {
    $this->price = $price;
    $this->special = $price;
    $this->discount = $price;
  }

  public function setSpecial($special_info)
  {
    if ($special_info) {
      $this->special = $special_info['price'] ? $special_info['price'] : $this->special;
      $this->special_id = $special_info['product_special_id'] ? $special_info['product_special_id'] : $this->special_id;
    }
  }

  public function setDiscount($discount)
  {
    $this->discount = $discount ? $discount : $this->price;
  }

  public function setQuantity($quantity = 1)
  {
    $this->quantity = $quantity;
  }

  public function getPrice()
  {
    return (float)min([$this->price, $this->special, $this->discount]) * $this->quantity;
  }

  public function getOldPrice()
  {
    return (float)$this->price * $this->quantity;
  }

  public function getSpecialId()
  {
    return $this->special_id;
  }

  public function addOptionPrice(Price $option_price)
  {
    $this->special += $option_price->getPrice();
    $this->price += $option_price->getOldPrice();
    $this->discount += $option_price->getOldPrice();
  }
}
