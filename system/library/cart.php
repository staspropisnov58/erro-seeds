<?php
class Cart {
	private $config;
	private $db;
	private $data = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');

		if (!isset($this->session->data['cart']) || !is_array($this->session->data['cart'])) {
			$this->session->data['cart'] = array();
		}
	}

	public function getProducts() {
		if (!$this->data) {
			$order_id = isset($this->session->data['order_id']) ? $this->session->data['order_id'] : 0;
			foreach ($this->session->data['cart'] as $key => $quantity) {
				$quantity_in_stock = 0;
				$quantity_in_order = $quantity;
				$subtract = 1;

				$i = 0;
				$product = unserialize(base64_decode($key));

				$product_id = $product['product_id'];

				$stock = true;


				// Options
				if (!empty($product['option'])) {
					$options = $product['option'];

					foreach ($product['option'] as $product_option_id => $product_option_value_id) {
						$product_option_value_id = $product_option_value_id;
						$product_option_id = $product_option_id;
						$quantity_to_subtract_option = $this->db->query("SELECT quantity_to_subtract_option FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . (int)$product_option_id . "'");
						$quantity_to_subtract_option = $quantity_to_subtract_option->row['quantity_to_subtract_option'];
						if($quantity_to_subtract_option == 'one_to_one'){
					    $quantity_subtract = 1;
					  }elseif($quantity_to_subtract_option == 'one_to_many'){
							$quantity_subtract = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . $product_option_value_id . "'" );
					    $quantity_subtract = (int)$quantity_subtract->row['quantity'];
							$subtract = (int)$quantity_subtract;

					  }else{
							$quantity_subtract = 1;
						}

						$quantity_array[] = array(
							'quantity' => $quantity,
							'quantity_to_subtract_option' => $quantity_subtract,
						);
					}

				} else {
					$options = array();

					$quantity_array[] = array(
						'quantity' => $quantity,
						'quantity_to_subtract_option' => $subtract,
					);
				}

				// Profile
				if (!empty($product['recurring_id'])) {
					$recurring_id = $product['recurring_id'];
				} else {
					$recurring_id = 0;
				}

				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
				AND p.date_available <= NOW() AND p.status = '1'");



				if ($product_query->num_rows) {



					$price = $product_query->row['price'];
					$quantity_in_stock = $product_query->row['quantity'];

                    if($this->customer->isLogged() && $this->customer->getGroupId()==3  ){

                        $sale_price_category = $this->getCategoriesMain($product_id);
                        if($sale_price_category['sale_price']>0){
                            $pr = $this->percent($product_query->row['price'],$sale_price_category['sale_price']);
                            $product_query->row['price'] = $pr;
                            $price=$pr;
                        }else {
                            $summ_sale = $this->customer->getSummeSale();
                            $summ_sale_group = $this->customer->getSummeSaleGroup();
                            $procent_sale = $this->customer->getProcentSale();
                            $procent_sale_group = $this->customer->getProcentSaleGroup();
                            $procent_sale_bonus = $this->customer->getProcentSaleBonus();
                            $customer_balance = $this->customer->getSummBalance();
                            if ($summ_sale < $customer_balance && $summ_sale != 0) {
                                $pr = $this->percent($product_query->row['price'], $procent_sale);
                                $product_query->row['price'] = $pr;
                                $price = $pr;
                            } elseif ($summ_sale_group < $customer_balance && $summ_sale_group != 0) {
                                $pr = $this->percent($product_query->row['price'], $procent_sale_group);
                                $product_query->row['price'] = $pr;
                                $price = $pr;
                            }
                        }



                    }




					// Product Discounts
					$discount_quantity = 0;
					$quantity_in_cart = 0;

					$maximum = (int)$product_query->row['maximum'];
					$maximum_exeeded = false;


					foreach ($this->session->data['cart'] as $key_2 => $quantity_2) {
						$product_2 = (array)unserialize(base64_decode($key_2));

						if ($product_2['product_id'] == $product_id) {
							$discount_quantity += $quantity_2;
						}
					}

					$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

					if ($product_discount_query->num_rows) {
						$price = $product_discount_query->row['price'];
					}


					// Product Specials
					$sql = "SELECT price, product_special_id, maximum FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW()))";

					if($this->config->get('sale_events_status')!== NULL && (int)$this->config->get('sale_events_status') !== 0){
						$sql .= " AND status = 1 ";
					}

					$sql .= " ORDER BY priority ASC, price ASC LIMIT 1";

					$product_special_query = $this->db->query($sql);

					if ($product_special_query->num_rows) {
						$price = $product_special_query->row['price'];
						$product_special_id = $product_special_query->row['product_special_id'];

						$maximum = (int)$product_special_query->row['maximum'] && ((int)$product_special_query->row['maximum'] < $maximum || !(int)$maximum) ? (int)$product_special_query->row['maximum'] : $maximum;
					}

					$option_points = 0;
					$option_weight = 0;
					$option_price = 0;

					$option_data = array();

					foreach ($options as $product_option_id => $value) {

						$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

						if ($option_query->num_rows) {
							if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");


                                if($summ_sale < $customer_balance && $summ_sale!=0){
                                    $pr_opt = $this->percent($option_value_query->row['price'],$procent_sale);
                                    $option_value_query->row['price'] = $pr_opt;

                                }elseif ($summ_sale_group < $customer_balance && $summ_sale_group!=0){
                                    $pr_opt = $this->percent($option_value_query->row['price'],$procent_sale_group);
                                    $option_value_query->row['price'] = $pr_opt;


                                }

								if ($option_value_query->num_rows) {
									$current_option_price = $option_value_query->row['price'];
									if (isset($product_special_id)) {
										$option_special_query = $this->db->query("SELECT price, maximum FROM " . DB_PREFIX . "product_option_special
																															WHERE product_option_value_id = '" . (int)$value . "' AND special_id = '" . (int)$product_special_id . "'");

								if(isset($option_special_query->row['price'])){
									$current_option_price = $option_special_query->row['price'];
											if((int)$option_special_query->row['price'] === 0){
												$price = (int)$option_value_query->row['price'] ? $product_query->row['price'] : $price;
												$current_option_price = $option_value_query->row['price'];
											}
										}else{
											$price = (int)$option_value_query->row['price'] ? $product_query->row['price'] : $price;
											$current_option_price = $option_value_query->row['price'];
										}
									} else {
									}
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $current_option_price;
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $current_option_price;
									}

									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}

									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}

									if ($option_value_query->row['subtract']) {
										if($quantity_to_subtract_option == 'one_to_many'){
											$quantity_in_order = $quantity * $option_value_query->row['quantity'];

											$maximum_counted = ($maximum - ($maximum % (int)$option_value_query->row['quantity'])) / (int)$option_value_query->row['quantity'];
										} else {
											$quantity_in_stock = min([$quantity, $option_value_query->row['quantity']]);
										}
									}



									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $value,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'value'                   => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix']
									);
								}
							} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
								foreach ($value as $product_option_value_id) {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                                    if($summ_sale < $customer_balance && $summ_sale!=0){
                                        $pr_opt = $this->percent($option_value_query->row['price'],$procent_sale);
                                        $option_value_query->row['price'] = $pr_opt;

                                    }elseif ($summ_sale_group < $customer_balance && $summ_sale_group!=0){
                                        $pr_opt = $this->percent($option_value_query->row['price'],$procent_sale_group);
                                        $option_value_query->row['price'] = $pr_opt;
                                    }

									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											$option_price += $option_value_query->row['price'];
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}


										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}

										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}

										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {

											$stock = false;
										}

										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => $product_option_value_id,
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => $option_value_query->row['option_value_id'],
											'name'                    => $option_query->row['name'],
											'value'                   => $option_value_query->row['name'],
											'type'                    => $option_query->row['type'],
											'quantity'                => $option_value_query->row['quantity'],
											'subtract'                => $option_value_query->row['subtract'],
											'price'                   => $option_value_query->row['price'],
											'price_prefix'            => $option_value_query->row['price_prefix'],
											'points'                  => $option_value_query->row['points'],
											'points_prefix'           => $option_value_query->row['points_prefix'],
											'weight'                  => $option_value_query->row['weight'],
											'weight_prefix'           => $option_value_query->row['weight_prefix']
										);
									}
								}

							} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => '',
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => '',
									'name'                    => $option_query->row['name'],
									'value'                   => $value,
									'type'                    => $option_query->row['type'],
									'quantity'                => '',
									'subtract'                => '',
									'price'                   => '',
									'price_prefix'            => '',
									'points'                  => '',
									'points_prefix'           => '',
									'weight'                  => '',
									'weight_prefix'           => ''
								);
							}
						}
					}

					// Reward Points
					$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'");

					if ($product_reward_query->num_rows) {
						$reward = $product_reward_query->row['points'];
					} else {
						$reward = 0;
					}

					// Downloads
					$download_data = array();

					$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$product_id . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

					foreach ($download_query->rows as $download) {
						$download_data[] = array(
							'download_id' => $download['download_id'],
							'name'        => $download['name'],
							'filename'    => $download['filename'],
							'mask'        => $download['mask']
						);
					}


					// Stock


					if($order_id){
						$order_data = $this->db->query("SELECT * , (SELECT order_status_id FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "' ) AS ordrer_status_id FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX ."order_option oo ON (op.order_id = oo.order_id) WHERE op.order_id = '" . (int)$order_id . "'");
						foreach ($order_data->rows as $order_data){

						    if(!isset($order_data['order_status_id'])){$order_data['order_status_id']=2;}
							if((int)$order_data['product_id'] === (int)$product_query->row['product_id'] && in_array($order_data['order_status_id'], $this->config->get('config_processing_status')) && $quantity_to_subtract_option == 'one_to_many'){
								if((int)$option_data['option_value_id'] === (int)$order_data['product_option_value_id']){
									$qunatity_in_order -= $order_data['quantity'] * $subtract;
								}
							}
						}
					}

					if ($maximum && $maximum < $quantity_in_order) {
						$maximum_exeeded = true;
					} else {
						$maximum_exeeded = false;
					}

					if (!$quantity_in_stock || ($quantity_in_stock < $quantity_in_order)) {
						$stock = false;
					}



					$recurring_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "recurring` `p` JOIN `" . DB_PREFIX . "product_recurring` `pp` ON `pp`.`recurring_id` = `p`.`recurring_id` AND `pp`.`product_id` = " . (int)$product_query->row['product_id'] . " JOIN `" . DB_PREFIX . "recurring_description` `pd` ON `pd`.`recurring_id` = `p`.`recurring_id` AND `pd`.`language_id` = " . (int)$this->config->get('config_language_id') . " WHERE `pp`.`recurring_id` = " . (int)$recurring_id . " AND `status` = 1 AND `pp`.`customer_group_id` = " . (int)$this->config->get('config_customer_group_id'));

					if ($recurring_query->num_rows) {
						$recurring = array(
							'recurring_id'    => $recurring_id,
							'name'            => $recurring_query->row['name'],
							'frequency'       => $recurring_query->row['frequency'],
							'price'           => $recurring_query->row['price'],
							'cycle'           => $recurring_query->row['cycle'],
							'duration'        => $recurring_query->row['duration'],
							'trial'           => $recurring_query->row['trial_status'],
							'trial_frequency' => $recurring_query->row['trial_frequency'],
							'trial_price'     => $recurring_query->row['trial_price'],
							'trial_cycle'     => $recurring_query->row['trial_cycle'],
							'trial_duration'  => $recurring_query->row['trial_duration']
						);
					} else {
						$recurring = false;
					}


					$this->data[$key] = array(
						'key'             => $key,
						'product_id'      => $product_query->row['product_id'],
						'name'            => $product_query->row['name'],
						'model'           => $product_query->row['model'],
						'sku'             => $product_query->row['sku'],
						'shipping'        => $product_query->row['shipping'],
						'image'           => $product_query->row['image'],
						'option'          => $option_data,
						'download'        => $download_data,
						'quantity'        => $quantity,
						'minimum'         => $product_query->row['minimum'],
						'maximum'         => isset($maximum_counted) ? $maximum_counted : $maximum,
						'maximum_exeeded' => $maximum_exeeded,
						'subtract'        => $product_query->row['subtract'],
						'stock'           => $stock,
						'price'           => ($price + $option_price),
						'total'           => ($price + $option_price) * $quantity,
						'reward'          => $reward * $quantity,
						'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $quantity : 0),
						'tax_class_id'    => $product_query->row['tax_class_id'],
						'weight'          => ($product_query->row['weight'] + $option_weight) * $quantity,
						'weight_class_id' => $product_query->row['weight_class_id'],
						'length'          => $product_query->row['length'],
						'width'           => $product_query->row['width'],
						'height'          => $product_query->row['height'],
						'length_class_id' => $product_query->row['length_class_id'],
						'recurring'       => $recurring,
					);
				} else {
					$this->remove($key);
				}
				$i = $i++;

			}
		}

		return $this->data;
	}

    public function getCategoriesMain($product_id) {
        $query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' and main_category=1 ");
        if($query->row['category_id'] > 0){
            $query = $this->db->query("SELECT sale_price FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$query->row['category_id'] . "' ");
        }
        return $query->row;
    }

    public function percent($number,$procent) {

        $percent = $procent; // Необходимый процент
        $number_percent = $number / 100 * $percent;

        return $number-$number_percent;

    }
	public function getRecurringProducts() {
		$recurring_products = array();

		foreach ($this->getProducts() as $key => $value) {
			if ($value['recurring']) {
				$recurring_products[$key] = $value;
			}
		}

		return $recurring_products;
	}

	public function add($product_id, $qty = 1, $option = array(), $recurring_id = 0) {
		$this->data = array();

		$product['product_id'] = (int)$product_id;

		if ($option) {
			$product['option'] = $option;

			foreach ($product['option'] as $product_option_id => $product_option_value_id) {
				$quantity_to_subtract_option = $this->db->query("SELECT quantity_to_subtract_option FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . (int)$product_option_id . "'");
				$quantity_to_subtract_option = $quantity_to_subtract_option->row['quantity_to_subtract_option'];
				if($quantity_to_subtract_option == 'one_to_one'){
			    $quantity_subtract = 1;
			  }elseif($quantity_to_subtract_option == 'one_to_many'){
					$quantity_subtract = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . $product_option_value_id . "'" );
			    $quantity_subtract = (int)$quantity_subtract->row['quantity'];
			  }else{
					$quantity_subtract = 1;
				}

				$quantity_array[] = array(
					'quantity' => $qty,
					'quantity_to_subtract_option' => $quantity_subtract,
				);
			}
		}


		if ($recurring_id) {
			$product['recurring_id'] = (int)$recurring_id;
		}

		$key = base64_encode(serialize($product));

		if ((int)$qty && ((int)$qty > 0)) {
			$key = base64_encode(serialize($product));

			if (!isset($this->session->data['cart'][$key])) {
				$this->session->data['cart'][$key] = (int)$qty;
			} else {
				$this->session->data['cart'][$key] += (int)$qty;
			}
		} else {
			$key = '';
		}

		return $key;
	}

	public function update($key, $qty) {
		$this->data = array();

		if ((int)$qty && ((int)$qty > 0) && isset($this->session->data['cart'][$key])) {
			$this->session->data['cart'][$key] = (int)$qty;
		} else {
			$this->remove($key);
		}
	}

	public function remove($key) {
		$this->data = array();

		unset($this->session->data['cart'][$key]);
	}

	public function clear() {
		$this->data = array();

		$this->session->data['cart'] = array();
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function getTaxes() {
		$tax_data = array();

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		return $tax_data;
	}

	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts() {
		return count($this->session->data['cart']);
	}

	public function hasRecurringProducts() {
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {

				$stock = false;

			}
		}

		return $stock;
	}

	public function hasShipping() {
		$shipping = false;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$shipping = true;

				break;
			}
		}

		return $shipping;
	}

	public function hasDownload() {
		$download = false;

		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				$download = true;

				break;
			}
		}

		return $download;
	}
}
