<?php
include '../../../config.php';

$dbh = dbConnect();
$get_brands = $dbh->prepare('SELECT DISTINCT manufacturer_id, name FROM oc_manufacturer WHERE manufacturer_id IN(
  SELECT manufacturer_id FROM oc_manufacturer_to_store WHERE store_id = 0)');
$get_brands->execute();

while ($brand = $get_brands->fetch(PDO::FETCH_ASSOC)) {
  productsToXML($brand);
}

function productsToXML($brand) {
  $xmlstr = '<?xml version="1.0" encoding="UTF-8"?><document></document>';
  $products = new SimpleXMLElement($xmlstr);
  $products->addChild('created', date('Y-m-d H:i:s'));

  $dbh = dbConnect();
  $get_products = $dbh->prepare('SELECT p.product_id, p.sku, m.name AS brand, p.price, p.quantity, p.date_modified, p.image
    FROM oc_product p
    INNER JOIN oc_manufacturer m ON p.manufacturer_id = m.manufacturer_id
    LEFT JOIN oc_product_to_store p2s ON p.product_id = p2s.product_id
    WHERE p.manufacturer_id = :manufacturer_id
        AND p.status = 1
        AND p.date_available <= NOW()
        AND p2s.store_id = 0
    ORDER BY p.quantity DESC');

    $get_products->bindParam(':manufacturer_id', $brand['manufacturer_id'], PDO::PARAM_STR);
    $get_products->execute();

    $languages = getLanguages();

    while ($product = $get_products->fetch(PDO::FETCH_ASSOC)) {
      foreach ($languages as $language) {
        $product['description'][$language['code']] = getProductDescription($product['product_id'], $language['language_id']);
      }

      $product['special'] = getProductSpecial($product['product_id']);
      $product['attributes'] = getProductAttributes($product['product_id'], $languages);
      $product['categories'] = getProductCategories($product['product_id'], $languages);
      $product['options'] = getProductOptions($product['product_id'], $product['price'], $product['special'], $languages);
      $product['images'] = getProductImages($product['product_id']);
      $product['images'][] = HTTPS_SERVER . 'image/' . $product['image'];
      createProductNode($products, $product);
    }

  $file_name = '/var/www/www-root/data/www/errors-seeds.com.ua/product_xml/' . mb_strtolower(str_replace(' ', '_', $brand['name'])) . '.xml';

  $products->asXML($file_name);
}

function createProductNode(&$products, $product) {
  $node = $products->addChild('product', '');
  $node->addChild('sku', $product['sku']);
  $node->addChild('brand', $product['brand']);

  if ($product['special']) {
    $special = $product['special']['price'];
  } else {
    $special = 0;
  }

  $node->addChild('price', number_format($product['price'], 2, '.', ''));
  if ($special) {
    $node->addChild('special', number_format($special, 2, '.', ''));
  }

  $in_stock = $product['quantity'] > 0 ? 1 : 0;
  $node->addChild('in_stock', $in_stock);

  $node->addChild('date_modified', $product['date_modified']);

  foreach ($product['description'] as $language_code => $description) {
    $node->addChild('name', htmlspecialchars(trim($description['name'])))->addAttribute('language', $language_code);
    $node->addChild('description', '<![CDATA[' .  iconv("UTF-8", "UTF-8*//TRANSLIT", $description['description']) . ']]>')->addAttribute('language', $language_code);
  }

  $categories = $node->addChild('categories', '');
  foreach ($product['categories'] as $category) {
    $category_node = $categories->addChild('category', '');
    foreach ($category as $language_code => $name) {
      $category_node->addChild('name', htmlspecialchars(trim($name)))->addAttribute('language', $language_code);
    }
  }

  $images = $node->addChild('images', '');
  foreach ($product['images'] as $sort_order => $image) {
    $images->addChild('image', trim($image))->addAttribute('sort_order', $sort_order);
  }

  $attributes = $node->addChild('attributes', '');
  foreach ($product['attributes'] as $attribute) {
    $attribute_node = $attributes->addChild('attribute', '');
    foreach ($attribute as $language_code => $content) {
      $attribute_node->addChild('name', htmlspecialchars(trim($content['name'])))->addAttribute('language', $language_code);
      $attribute_node->addChild('text', htmlspecialchars(trim($content['text'])))->addAttribute('language', $language_code);
    }
  }

  $options = $node->addChild('options', '');
  foreach ($product['options'] as $option) {
    $option_node = $options->addChild('option', '');
    foreach ($option['name'] as $language_code => $content) {
      $option_node->addChild('name', htmlspecialchars(trim($content)))->addAttribute('language', $language_code);
    }

    $values_node = $option_node->addChild('values', '');
    foreach ($option['values'] as $value) {
      $value_node = $values_node->addChild('value', '');
      foreach ($value['value'] as $language_code => $content) {
        $value_node->addChild('name', htmlspecialchars(trim($content)))->addAttribute('language', $language_code);
      }

      $value_node->addChild('price', number_format($value['option_price'], 2, '.', ''));

      if (isset($value['option_special'])) {
        $value_node->addChild('special', number_format($value['option_special'], 2, '.', ''));
      }

      if ($option['quantity_to_subtract_option'] === 'one_to_many' && $product['quantity'] - $value['quantity'] < 0) {
        $in_stock = 0;
      } else {
        $in_stock = $value['quantity'] > 0 ? 1 : 0;
      }

      $value_node->addChild('in_stock', $in_stock);
    }
  }
}

function dbConnect() {
  return new PDO('mysql:host=localhost;dbname=' . DB_DATABASE . ";charset=utf8", DB_USERNAME, DB_PASSWORD);
}

function getProductDescription($product_id, $language_id) {
  $dbh = dbConnect();
  $description = [];

  $get_product_description = $dbh->prepare('SELECT name, description
    FROM oc_product_description
    WHERE product_id = :product_id AND language_id = :language_id');

  $get_product_description->bindParam(':product_id', $product_id, PDO::PARAM_INT);
  $get_product_description->bindParam(':language_id', $language_id, PDO::PARAM_INT);
  $get_product_description->execute();

  return $get_product_description->fetch(PDO::FETCH_ASSOC);
}

function getProductSpecial($product_id) {
  $dbh = dbConnect();
  $special = [];

  $get_product_special = $dbh->prepare('SELECT product_special_id, price, percent
    FROM oc_product_special
    WHERE product_id = :product_id
        AND customer_group_id = 1
        AND ((date_start = "000-00-00" OR date_start < NOW())
        AND (date_end = "0000-00-00" OR date_end > NOW()))
        ORDER BY priority ASC, price ASC LIMIT 1');

  $get_product_special->bindParam(':product_id', $product_id, PDO::PARAM_INT);
  $get_product_special->execute();

  return $get_product_special->fetch(PDO::FETCH_ASSOC);
}

function getProductAttributes($product_id, $languages) {
    $dbh = dbConnect();
    $attributes = [];

    $get_product_attributes = $dbh->prepare('SELECT DISTINCT attribute_id
      FROM oc_product_attribute WHERE product_id = :product_id');

    $get_product_attributes_description = $dbh->prepare('SELECT a.name, pa.text
      FROM oc_product_attribute pa
      INNER JOIN oc_attribute_description a ON pa.attribute_id = a.attribute_id
      WHERE pa.product_id = :product_id
      AND pa.language_id = :language_id
      AND a.language_id = pa.language_id
      AND a.attribute_id = :attribute_id');

    $get_product_attributes->bindParam(':product_id', $product_id, PDO::PARAM_INT);
    $get_product_attributes->execute();

    while ($attribute = $get_product_attributes->fetch(PDO::FETCH_ASSOC)) {
      foreach ($languages as $language) {
        $get_product_attributes_description->bindParam(':product_id', $product_id, PDO::PARAM_INT);
        $get_product_attributes_description->bindParam(':language_id', $language['language_id'], PDO::PARAM_INT);
        $get_product_attributes_description->bindParam(':attribute_id', $attribute['attribute_id'], PDO::PARAM_INT);
        $get_product_attributes_description->execute();

        while ($attribute_description = $get_product_attributes_description->fetch(PDO::FETCH_ASSOC)) {
          $attributes[$attribute['attribute_id']][$language['code']] = $attribute_description;
        }
      }
    }

    return $attributes;
}

function getProductCategories($product_id, $languages) {
  $dbh = dbConnect();
  $categories = [];

  $get_product_categories = $dbh->prepare('SELECT category_id
    FROM oc_product_to_category
    WHERE category_id IN(SELECT category_id FROM oc_category WHERE status = 1)
    AND product_id = :product_id');

  $get_product_categories_description = $dbh->prepare('SELECT name
    FROM oc_category_description
    WHERE category_id = :category_id AND language_id = :language_id');

  $get_product_categories->bindParam(':product_id', $product_id, PDO::PARAM_INT);
  $get_product_categories->execute();

  while ($category = $get_product_categories->fetch(PDO::FETCH_ASSOC)) {
    foreach ($languages as $language) {
      $get_product_categories_description->bindParam(':category_id', $category['category_id'], PDO::PARAM_INT);
      $get_product_categories_description->bindParam(':language_id', $language['language_id'], PDO::PARAM_INT);
      $get_product_categories_description->execute();

      while ($category_description = $get_product_categories_description->fetch(PDO::FETCH_ASSOC)) {
        $categories[$category['category_id']][$language['code']] = $category_description['name'];
      }
    }
  }

  return $categories;
}

function getProductOptions($product_id, $price, $special, $languages) {
  $dbh = dbConnect();
  $options = [];

  $get_product_options = $dbh->prepare('SELECT option_id, product_option_id, quantity_to_subtract_option
    FROM oc_product_option
    WHERE product_id = :product_id');

  $get_product_options_description = $dbh->prepare('SELECT name
    FROM oc_option_description
    WHERE option_id = :option_id AND language_id = :language_id');

  $get_product_options->bindParam(':product_id', $product_id, PDO::PARAM_INT);
  $get_product_options->execute();

  while ($option = $get_product_options->fetch(PDO::FETCH_ASSOC)) {
    $options[$option['option_id']] = $option;
    if (isset($option['product_option_id'])) {
      $options[$option['option_id']]['values'] = getOptionValues($option['product_option_id'], $price, $special, $languages);
      foreach ($languages as $language) {
        $get_product_options_description->bindParam(':option_id', $option['option_id'], PDO::PARAM_INT);
        $get_product_options_description->bindParam(':language_id', $language['language_id'], PDO::PARAM_INT);
        $get_product_options_description->execute();

        while ($option_description = $get_product_options_description->fetch(PDO::FETCH_ASSOC)) {
          $options[$option['option_id']]['name'][$language['code']] = $option_description['name'];
        }
      }
    }
  }

  return $options;
}

function getOptionValues($product_option_id, $price, $special, $languages) {
  $dbh = dbConnect();

  $option_values = [];

  $get_product_option_values_string = 'SELECT pov.option_value_id, pov.quantity,
    (pov.price + :price) AS option_price';

  if ($special) {
    $get_product_option_values_string .= ', (pos.price + :special) AS option_special ';
  }

  $get_product_option_values_string .= ' FROM oc_product_option_value pov ';

  if ($special) {
    $get_product_option_values_string .= ' INNER JOIN oc_product_option_special pos
    ON pov.product_option_value_id = pos.product_option_value_id ';
  }

  $get_product_option_values_string .= ' WHERE pov.product_option_id = :product_option_id AND pov.status = 1';

  if ($special) {
    $get_product_option_values_string .= ' AND special_id = :special_id ';
  }

  $get_product_option_values_string .= ' ORDER BY pov.price ASC';

  $get_product_option_values = $dbh->prepare($get_product_option_values_string);


  $get_product_option_values->bindParam(':price', $price, PDO::PARAM_STR);
  $get_product_option_values->bindParam(':product_option_id', $product_option_id, PDO::PARAM_INT);
  if ($special) {
    $get_product_option_values->bindParam(':special_id', $special['product_special_id'], PDO::PARAM_STR);
    $get_product_option_values->bindParam(':special', $special['price'], PDO::PARAM_STR);
  }
  $get_product_option_values->execute();

  $get_product_option_values_description = $dbh->prepare('SELECT ovd.name AS value
  FROM oc_option_value_description ovd
  WHERE ovd.option_value_id = :option_value_id
  AND language_id = :language_id');

  while ($option_value = $get_product_option_values->fetch(PDO::FETCH_ASSOC)) {
    $option_values[$option_value['option_value_id']] = $option_value;
    foreach ($languages as $language) {
      $get_product_option_values_description->bindParam(':option_value_id', $option_value['option_value_id'], PDO::PARAM_INT);
      $get_product_option_values_description->bindParam(':language_id', $language['language_id'], PDO::PARAM_INT);
      $get_product_option_values_description->execute();

      while ($option_value_description = $get_product_option_values_description->fetch(PDO::FETCH_ASSOC)) {
        $option_values[$option_value['option_value_id']]['value'][$language['code']] = $option_value_description['value'];
      }
    }
  }

  return $option_values;
}

function getProductImages($product_id) {
  $dbh = dbConnect();
  $images = [];

  $get_product_images = $dbh->prepare('SELECT CONCAT(:image_path, image) AS image
    FROM oc_product_image WHERE product_id = :product_id ORDER BY sort_order ASC');

  $image_path = HTTPS_SERVER . 'image/';
  $get_product_images->bindParam(':image_path', $image_path, PDO::PARAM_STR);
  $get_product_images->bindParam(':product_id', $product_id, PDO::PARAM_INT);
  $get_product_images->execute();

  while ($image = $get_product_images->fetch(PDO::FETCH_ASSOC)) {
    $images[] = $image['image'];
  }

  return $images;
}

function getLanguages() {
  $dbh = dbConnect();
  $get_languages = $dbh->prepare('SELECT language_id, code FROM oc_language');
  $get_languages->execute();

  return $get_languages->fetchAll();
}
