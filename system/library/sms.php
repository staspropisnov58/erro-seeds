<?php
final class Sms {

	private $smssender;

	public function __construct($gate, $options = array()) {
		if (! defined('DIR_SMSSENDER')) {
			define('DIR_SMSSENDER', DIR_SYSTEM . 'smssender/');
		}

		if (! $gate) $gate = 'testsms';
		if (file_exists(DIR_SMSSENDER . $gate . '.php')) {
			$gate_path = DIR_SMSSENDER . $gate . '.php';
			require_once($gate_path);
		} else {
			trigger_error('Error: Could not load database file ' . $gate . '!');
			exit();
		}

		$this->smssender = new $gate($options);
	}

	public function __set($key, $value) {
		$this->smssender->{$key} = $value;
	}

	public function __get($key) {
		return $this->smssender->{$key};
	}

	public function has($key) {
		return $this->smssender->has($key);
	}

	public function send() {
		return $this->smssender->send();
	}
}

class SmsSender {

	private $data = array();

	public function __construct($options = array()) {
		if (is_array($options)) $this->data = $options;
	}

	public function __set($key, $value) {
		$this->data[$key] = $value;
	}

	public function __get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : NULL);
	}

	public function has($key) {
    	return isset($this->data[$key]);
  	}

	public function send() { }
}

?>
