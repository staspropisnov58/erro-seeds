<?php
class Log {
	private $handle;
	private $filename;

	public function __construct($filename) {
		$this->filename = $filename;
		$this->handle = fopen(DIR_LOGS . $filename, 'a');
	}

	public function write($message) {
		fwrite($this->handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true) . "\n");
	}

	public function __destruct() {
		fclose($this->handle);
		chmod(DIR_LOGS . $this->filename, 0777);
	}
}
