<?php
interface CheckoutSteps
{
  public function load($errors);
  public function save($data);
}
