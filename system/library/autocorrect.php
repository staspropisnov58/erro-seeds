<?php
trait Autocorrect
{
  public function correctValuesOf(&$form)
  {
    foreach ($form as $name => &$value) {
      switch ($name) {
        case 'email':
          $value = $this->correctEmail($value);
          break;
        case 'telephone':
          $value = $this->correctTelephone($value);
          break;
        case 'password':
          //password shouldn't be autocorrected
          break;
        default:
          $value = $this->correctText($value);
          break;
      }
    }
  }

  protected function correctEmail($value)
  {
    return strtolower($this->correctText($value));
  }

  protected function correctText($value)
  {
    return trim($value);
  }

  protected function correctTelephone($value)
  {
    return strtolower($this->correctText($value));
  }
}
