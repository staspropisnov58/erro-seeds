<?php
class Opengraph
{

/*
<meta property="og:type" content="website">
<meta property="og:site_name" content="Errors Seeds Україна">
<meta property="og:url" content="https://errors-seeds.com.ua/">
<meta property="og:title" content="Amnesium Feminised семена конопли от Victory Seeds: купить семена марихуаны поштучно с доставкой по Украине - Errors-Seeds">
<meta property="og:description" content="Семена конопли Amnesium Feminised поштучно - ? высококачественная медицинская конопля для снятия стресса и беспокойства ☀️ Errors-Seeds: ☑️ Конфиденциальность ☑️ Хорошая проращиваемость ☑️ Безопасная доставка">
<meta property="og:image" content="https://errors-seeds.com.ua/image/catalog/og_logo.png">
<meta property="og:image:width" content="698">
<meta property="og:image:height" content="469">
<meta property="og:image:type" content="image/png">
<meta property="og:image:alt" content="Errors Seeds Україна">
*/
  private $data;


  public function __construct($site_name, $height, $width)
  {
    $this->data['opengraph']['og:type'] = 'website';
    $this->data['opengraph']['og:site_name'] = $site_name;
    $this->data['opengraph']['og:image:width'] = $width;
    $this->data['opengraph']['og:image:height'] = $height;
  }


  public function setType($type = 'website'){
    $this->data['opengraph']['og:type'] = $type;
  }

  public function SetSiteName($name){
    $this->data['opengraph']['og:site_name'] = $name;
  }

  public function SetUrl($url){
    $this->data['opengraph']['og:url'] = $url;
  }

  public function SetTitle($title){
    $this->data['opengraph']['og:title'] = $title;
  }

  public function SetDescription($description){
    $this->data['opengraph']['og:description'] = $description;
  }

  public function SetImage($image){
    $this->data['opengraph']['og:image'] = $image;
  }

  public function SetImageWidth($image_width){
    $this->data['opengraph']['og:image:width'] = $image_width;
  }

  public function SetImageHeight($image_height){
    $this->data['opengraph']['og:image:height'] = $image_height;
  }

  public function SetImageType($image_type){
    $this->data['opengraph']['og:image:type'] = $image_type;
  }

  public function SetImageAlt($image_alt){
    $this->data['opengraph']['og:image:alt'] = $image_alt;
  }

  public function getData()
  {
    return $this->data;
  }
}
