<?php
class Customer {
	private $customer_id;
	private $bonus_sale;
	private $summ_sale;
	private $procent_sale_bonus;
	private $procent_sale;
	private $firstname = '';
	private $lastname = '';
	private $email = '';
	private $telephone = '';
	private $fax = '';
	private $newsletter = 0;
	private $customer_group_id = 0;
	private $address_id = 0;
	private $affiliate_id = 0;
	private $affiliate_code = '';

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['customer_id'])) {
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->session->data['customer_id'] . "' AND status = '1'");

			if ($customer_query->num_rows) {
				$this->customer_id = $customer_query->row['customer_id'];
				$this->firstname = $customer_query->row['firstname'];
				$this->lastname = $customer_query->row['lastname'];
				$this->summ_sale = $customer_query->row['summ_sale'];
				$this->procent_sale_bonus = $customer_query->row['procent_sale_bonus'];
				$this->procent_sale = $customer_query->row['procent_sale'];
				$this->email = $customer_query->row['email'];
				$this->telephone = $customer_query->row['telephone'];
				$this->fax = $customer_query->row['fax'];
				$this->newsletter = $customer_query->row['newsletter'];
				$this->customer_group_id = $customer_query->row['customer_group_id'];
				$this->bonus_sale = $customer_query->row['bonus_sale'];
				$this->address_id = $customer_query->row['address_id'];

				if(!empty($customer_query->row['affiliate_id'])){
					$this->affiliate_id = $customer_query->row['affiliate_id'];

					$affiliate_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE affiliate_id = '" . $this->affiliate_id . "' AND status = '1'");

					if(!empty($affiliate_query->row['code'])){
						$this->affiliate_code = $affiliate_query->row['code'];
					}
				}

				$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "', wishlist = '" . $this->db->escape(isset($this->session->data['wishlist']) ? serialize($this->session->data['wishlist']) : '') . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int)$this->session->data['customer_id'] . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

				if (!$query->num_rows) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "customer_ip SET customer_id = '" . (int)$this->session->data['customer_id'] . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', date_added = NOW()");
				}
			} else {
				$this->logout();
			}
		}
	}

	public function login($email, $password, $override = false) {
		if ($override) {
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND status = '1'");
		} else {
			$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1' AND approved = '1'");
		}

		if ($customer_query->num_rows) {
			$this->session->data['customer_id'] = $customer_query->row['customer_id'];

			if ($customer_query->row['cart'] && is_string($customer_query->row['cart'])) {
				$cart = unserialize($customer_query->row['cart']);

				foreach ($cart as $key => $value) {
					if (!array_key_exists($key, $this->session->data['cart'])) {
						$this->session->data['cart'][$key] = $value;
					} else {
						$this->session->data['cart'][$key] += $value;
					}
				}
			}

			if ($customer_query->row['wishlist'] && is_string($customer_query->row['wishlist'])) {
				if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = array();
				}

				$wishlist = unserialize($customer_query->row['wishlist']);

				foreach ($wishlist as $product_id) {
					if (!in_array($product_id, $this->session->data['wishlist'])) {
						$this->session->data['wishlist'][] = $product_id;
					}
				}
			}

			$this->customer_id = $customer_query->row['customer_id'];
			$this->firstname = $customer_query->row['firstname'];
			$this->lastname = $customer_query->row['lastname'];
			$this->summ_sale = $customer_query->row['summ_sale'];
			$this->procent_sale_bonus = $customer_query->row['procent_sale_bonus'];
			$this->procent_sale = $customer_query->row['procent_sale'];
			$this->email = $customer_query->row['email'];
			$this->telephone = $customer_query->row['telephone'];
			$this->fax = $customer_query->row['fax'];
			$this->newsletter = $customer_query->row['newsletter'];
			$this->customer_group_id = $customer_query->row['customer_group_id'];
			$this->address_id = $customer_query->row['address_id'];

			if(!empty($customer_query->row['affiliate_id'])){
				$this->affiliate_id = $customer_query->row['affiliate_id'];

				$affiliate_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "affiliate WHERE affiliate_id = '" . $this->affiliate_id . "' AND status = '1'");

				if(!empty($affiliate_query->row['code'])){
					$this->affiliate_code = $affiliate_query->row['code'];
				}
			}


			$this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

			return true;
		} else {
			return false;
		}
	}

	public function logout() {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = '" . $this->db->escape(isset($this->session->data['cart']) ? serialize($this->session->data['cart']) : '') . "', wishlist = '" . $this->db->escape(isset($this->session->data['wishlist']) ? serialize($this->session->data['wishlist']) : '') . "' WHERE customer_id = '" . (int)$this->customer_id . "'");

		unset($this->session->data['customer_id']);

		$this->customer_id = '';
		$this->firstname = '';
		$this->lastname = '';
		$this->summ_sale = '';
		$this->procent_sale_bonus = '';
		$this->procent_sale = '';
		$this->email = '';
		$this->telephone = '';
		$this->fax = '';
		$this->newsletter = '';
		$this->customer_group_id = '';
		$this->address_id = '';
		$this->affiliate_id = '';
		$this->affiliate_code = '';
	}

	public function isLogged() {
		return $this->customer_id;
	}

	public function getId() {
		return $this->customer_id;
	}

	public function getSummeSale() {
		return $this->summ_sale;
	}

	public function getProcentSale() {
		return $this->procent_sale;
	}

	public function getProcentSaleBonus() {
		return $this->procent_sale_bonus;
	}
	public function getBonus() {
		return $this->bonus_sale;
	}

	public function getFirstName() {
		return $this->firstname;
	}

	public function getLastName() {
		return $this->lastname;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getTelephone() {
		return $this->telephone;
	}

	public function getFax() {
		return $this->fax;
	}

	public function getNewsletter() {
		return $this->newsletter;
	}

	public function getGroupId() {
		return $this->customer_group_id;
	}

	public function getAffiliateId(){
		return $this->affiliate_id;
	}

	public function getAffiliateCode(){
		return $this->affiliate_code;
	}

	public function getAddressId() {
		return $this->address_id;
	}

	public function getGroupName() {
		$query = $this->db->query("SELECT `name` FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$this->customer_group_id . "' and language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row['name'];
	}
	public function getProcentSaleBonusGroup() {
		$query = $this->db->query("SELECT `procent_sale_bonus` FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$this->customer_group_id . "' ");

		return $query->row['procent_sale_bonus'];
	}
	public function getBalance() {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$this->customer_id . "'");

		return $query->row['total'];
	}
	public function getProcentSaleGroup() {
		$query = $this->db->query("SELECT procent_sale FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$this->customer_group_id . "'");

		return $query->row['procent_sale'];
	}
	public function getUserBonus() {
		$query = $this->db->query("SELECT custom_bonus FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$this->customer_id . "'");

		return $query->row['custom_bonus'];
	}
	public function getSummeSaleGroup() {
		$query = $this->db->query("SELECT summ_sale FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$this->customer_group_id . "'");

		return $query->row['summ_sale'];
	}

    public function getSummBalance() {
        $query = $this->db->query("SELECT SUM(total) AS total FROM " . DB_PREFIX . "order WHERE customer_id = '" . (int)$this->customer_id . "'");

        return $query->row['total'];
    }

	public function getRewardPoints() {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$this->customer_id . "'");

		return $query->row['total'];
	}
}
