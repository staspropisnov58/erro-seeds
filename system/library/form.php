<?php
class Form
{
  private $name = '';
  private $action = '';
  private $fields = [];
  private $sort_order = [];
  public $load_countries = false;

	public function __construct($name = '', $action = '') {
    $this->name = $name;
    $this->action = $action;
  }

  public function addField($field_data)
  {
    $this->fields[$field_data['name']]['label']               = $field_data['label'];
    $this->fields[$field_data['name']]['placeholder']         = $field_data['placeholder'];
    $this->fields[$field_data['name']]['value']               = $field_data['value'];
    $this->fields[$field_data['name']]['type']                = $field_data['type'] ? $field_data['type'] : '';
    $this->fields[$field_data['name']]['options']             = isset($field_data['options']) ? $field_data['options'] : [];
    $this->fields[$field_data['name']]['dataset']             = isset($field_data['dataset']) ? $this->toHtmlString($field_data['dataset']) : '';
    $this->fields[$field_data['name']]['event_listeners']     = isset($field_data['event_listeners']) ? $this->toHtmlString($field_data['event_listeners']) : '';
    $this->fields[$field_data['name']]['is_group']    = false;

    $this->sort_order[$field_data['name']]['sort_order']  = $field_data['sort_order'];
  }

  public function setFieldsValues($values)
  {
    foreach ($this->fields as $field_name => &$field) {
      if (isset($values[$field_name])) {
        $field['value'] = $values[$field_name];
      }
    }
  }

  public function setFieldsErrors($errors)
  {
    foreach ($this->fields as $field_name => &$field) {
      if (isset($errors[$field_name])) {
        $field['error'] = $errors[$field_name];
      } else {
        $field['error'] = '';
      }
    }
  }

  public function build()
  {
    $form = array();

    if ($this->fields) {
      array_multisort($this->sort_order, SORT_ASC, $this->fields);
    }

    $form['name'] = $this->name;
    $form['action'] = $this->action;
    $form['fields'] = $this->fields;

    return $form;
  }

  /**
  * It works for fieldsets and groupped filds both
  */

  public function groupFields($group_name, ...$fields_name)
  {
    $group      = [];
    $label      = isset($this->fields[$group_name]['label']) ? $this->fields[$group_name]['label'] : '';
    $sort_order = $this->sort_order[$group_name];

    foreach ($fields_name as $field_name) {
      if (isset($this->fields[$field_name]) && ($this->fields[$field_name]['is_group'] || $this->fields[$field_name]['type'] !== '')) {
        $group[$field_name] = $this->fields[$field_name];
      } else {

      }
      unset($this->fields[$field_name]);
      unset($this->sort_order[$field_name]);
    }

    $this->fields[$group_name]['is_group']  = true;
    $this->fields[$group_name]['label']     = $label;
    $this->fields[$group_name]['fields']    = $group;

    $this->sort_order[$group_name] = $sort_order;
  }

  protected function toHtmlString($array)
  {
    $string = '';

    if ($array) {
      foreach ($array as $key => $value) {
        $string .= $key;
        $string .= '="' . $value . '"  ';
      }
    }

    return $string;
  }
}
