<?php
final class Smsc extends SmsSender{

  private $balance = 'https://smsc.ua/sys/balance.php?login={sms_login}&psw={sms_password}';

  public function send(){

    $params = array(
			'to'	=> $this->to,
			'text'	=> $this->message,
      'from'  => $this->from,
      'login' => $this->login,
      'password'  => $this->password,
      'request' => $this->request,
		);

    $params['to'] = preg_replace("/[^0-9+]/", '', $params['to']);


    $meanings = array("{sms_login}", "{sms_password}", "{sms_from}","{sms_to}","{sms_message}");
    $change   = array(html_entity_decode($params['login'], ENT_QUOTES, 'UTF-8'), html_entity_decode($params['password'], ENT_QUOTES, 'UTF-8'), html_entity_decode($params['from'], ENT_QUOTES, 'UTF-8'), html_entity_decode($params['to'], ENT_QUOTES, 'UTF-8'), urlencode(html_entity_decode($params['text'], ENT_QUOTES, 'UTF-8')));

    $sms_request= html_entity_decode(str_replace($meanings, $change, $params['request']),ENT_QUOTES, 'UTF-8');

    $meanings = array("{sms_login}", "{sms_password}");
    $change   = array(html_entity_decode($params['login'], ENT_QUOTES, 'UTF-8'), html_entity_decode($params['password'], ENT_QUOTES, 'UTF-8'));
    $balance_request = html_entity_decode(str_replace($meanings, $change, $this->balance),ENT_QUOTES, 'UTF-8');

    $balanse = file_get_contents($balance_request);

    $balance = (int)$balanse;

    $answer = array();


    if((int)$balance < 5){
      $answer['low_balance'] = (int)$balance;
    }

    $ret = $this->sendSms($sms_request);

    preg_match("/ERROR/", $ret, $match);

    if($match){

      preg_match('#\((.*?)\)#', $ret, $error);

      $answer['message'] = $error[1];

    }

    return $answer;

  }

  public function sendSms($sms_request){
    return file_get_contents($sms_request);

  }
}
