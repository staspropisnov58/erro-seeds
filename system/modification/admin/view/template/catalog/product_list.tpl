<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="submit" form="form-product" formaction="<?php echo $copy; ?>" data-toggle="tooltip" title="<?php echo $button_copy; ?>" class="btn btn-default"><i class="fa fa-copy"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-model"><?php echo $entry_sku; ?></label>
                <input type="text" name="filter_sku" value="<?php echo $filter_sku; ?>" placeholder="<?php echo $entry_sku; ?>" id="input-sku" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-price"><?php echo $entry_price; ?></label>
                <input type="text" name="filter_price" value="<?php echo $filter_price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
              </div>
              <?php if($product_stickers_status){ ?>
              <div class="form-group">
                <label class="control-label" for="input-main_sticker"><?php echo $entry_sticker; ?></label>
                <select name="filter_main_sticker" id="input-main_sticker" class="form-control">
                  <option value="*"></option>
                  <?php if($filter_main_sticker == 'hit'){ ?>
                    <option selected value = "hit"> <?php echo $text_main_sticker_hit; ?></option>
                  <?php }else{ ?>
                    <option value = "hit"> <?php echo $text_main_sticker_hit; ?></option>
                  <?php } ?>
                  <?php if($filter_main_sticker == 'new'){ ?>
                    <option selected value = "new"> <?php echo $text_main_sticker_new; ?></option>
                  <?php }else{ ?>
                    <option value = "new"> <?php echo $text_main_sticker_new; ?></option>
                  <?php } ?>
                  <?php if($filter_main_sticker == 'recommended'){ ?>
                    <option selected value = "recommended"> <?php echo $text_main_sticker_recommended; ?></option>
                  <?php }else{ ?>
                    <option value = "recommended"> <?php echo $text_main_sticker_recommended; ?></option>
                  <?php } ?>
                  <?php if($filter_main_sticker == 'with_stickers'){ ?>
                    <option selected value = "with_stickers"> <?php echo $text_main_with_stickers; ?></option>
                  <?php }else{ ?>
                    <option value = "with_stickers"> <?php echo $text_main_with_stickers; ?></option>
                  <?php } ?>
                  <?php if($filter_main_sticker == 'without_stickers'){ ?>
                    <option selected value = "without_stickers"> <?php echo $text_main_without_stickers; ?></option>
                  <?php }else{ ?>
                    <option value = "without_stickers"> <?php echo $text_main_without_stickers; ?></option>
                  <?php } ?>
                </select>
              </div>
            <?php } ?>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-special"><?php echo $entry_special; ?></label>
                <select name="filter_special" id="input-special" class="form-control">
                  <option value="*"></option>
                  <?php if($filter_special == 'not_specials'){ ?>
                    <option selected value = "not_specials"> <?php echo $text_not_specials; ?></option>
                  <?php }else{ ?>
                    <option value = "not_specials"> <?php echo $text_not_specials; ?></option>
                  <?php } ?>
                  <?php if($filter_special == 'only_specials'){ ?>
                    <option selected value = "only_specials"> <?php echo $text_only_specials; ?></option>
                  <?php }else{ ?>
                    <option value = "only_specials"> <?php echo $text_only_specials; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '0-10'){ ?>
                    <option selected value = "0-10"> <?php echo $text_option_10; ?></option>
                  <?php }else{ ?>
                    <option value = "0-10"> <?php echo $text_option_10; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '10-20'){ ?>
                    <option selected value = "10-20"> <?php echo $text_option_10_20; ?></option>
                  <?php }else{ ?>
                    <option value = "10-20"> <?php echo $text_option_10_20; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '20-30'){ ?>
                    <option selected value = "20-30"> <?php echo $text_option_20_30; ?></option>
                  <?php }else{ ?>
                    <option value = "20-30"> <?php echo $text_option_20_30; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '30-40'){ ?>
                    <option selected value = "30-40"> <?php echo $text_option_30_40; ?></option>
                  <?php }else{ ?>
                    <option value = "30-40"> <?php echo $text_option_30_40; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '40-50'){ ?>
                    <option selected value = "40-50"> <?php echo $text_option_40_50; ?></option>
                  <?php }else{ ?>
                    <option value = "40-50"> <?php echo $text_option_40_50; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '50-60'){ ?>
                    <option selected value = "50-60"> <?php echo $text_option_50_60; ?></option>
                  <?php }else{ ?>
                    <option value = "50-60"> <?php echo $text_option_50_60; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '60-70'){ ?>
                    <option selected value = "60-70"> <?php echo $text_option_60_70; ?></option>
                  <?php }else{ ?>
                    <option value = "60-70"> <?php echo $text_option_60_70; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '70-80'){ ?>
                    <option selected value = "70-80"> <?php echo $text_option_70_80; ?></option>
                  <?php }else{ ?>
                    <option value = "70-80"> <?php echo $text_option_70_80; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '80-90'){ ?>
                    <option selected value = "80-90"> <?php echo $text_option_80_90; ?></option>
                  <?php }else{ ?>
                    <option value = "80-90"> <?php echo $text_option_80_90; ?></option>
                  <?php } ?>
                  <?php if($filter_special == '90-100'){ ?>
                    <option selected value = "90-100"> <?php echo $text_option_90_100; ?></option>
                  <?php }else{ ?>
                    <option value = "90-100"> <?php echo $text_option_90_100; ?></option>
                  <?php } ?>
                </select>
              </div>



          <div class="form-group">
            <label class="control-label" for="input-import-batch"><?php echo 'Import'; ?></label>
            <select name="filter_import_batch" id="input-import-batch" class="form-control">
              <option value="*"></option>
              <?php foreach ($importLabels as $importLabel) { ?>
              <option value="<?php echo $importLabel; ?>" <?php if ($filter_import_batch == $importLabel) echo 'selected="selected"'; ?>><?php echo $importLabel; ?></option>
              <?php } ?>
            </select>
          </div>
      
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-center"><?php echo $column_image; ?></td>
                  <td class="text-left"><?php if ($sort == 'p.sku') { ?>
                    <a href="<?php echo $sort_sku; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sku; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sku; ?>"><?php echo $column_sku; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.model') { ?>
                    <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'p.quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'special') { ?>
                    <a href="<?php echo $sort_special; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_special; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_special; ?>"><?php echo $column_special; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-center"><?php if ($product['image']) { ?>
                    <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" />
                    <?php } else { ?>
                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                    <?php } ?></td>
                  <td class="text-left"><input data-product="<?php echo $product['product_id']; ?>" name="sku" type="text" value="<?php echo $product['sku']; ?>" /></td>
                  <td class="text-left">
                    <?php echo $product['name']; ?>
                    <?php if($product_stickers_status){ ?>
                    <?php foreach ($product['main_stickers'] as $main_sticker){ ?>
                      <?php if($main_sticker === 'hit'){ ?>
                        <span class="label label-danger"><?php echo $main_sticker; ?></span>
                      <?php }elseif($main_sticker === 'new'){ ?>
                        <span class="label label-success"><?php echo $main_sticker; ?></span>
                      <?php }elseif($main_sticker === 'recommended'){ ?>
                        <span class="label label-primary"><?php echo $main_sticker; ?></span>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                  </td>
                  <td class="text-left"><?php echo $product['model']; ?></td>
                  <td class="text-left">
                    <?php echo $product['price']; ?>
                  </td>
                  <td class="text-right"><?php if ($product['quantity'] <= 0) { ?>
                    <span class="label label-warning"><input data-product="<?php echo $product['product_id']; ?>" name="quantity" type="text" value="<?php echo $product['quantity']; ?>" /></span>
                    <?php } elseif ($product['quantity'] <= 5) { ?>
                    <span class="label label-danger"><input data-product="<?php echo $product['product_id']; ?>" name="quantity" type="text" value="<?php echo $product['quantity']; ?>" /></span>
                    <?php } else { ?>
                    <span class="label label-success"><input data-product="<?php echo $product['product_id']; ?>" name="quantity" type="text" value="<?php echo $product['quantity']; ?>" /></span>
                    <?php } ?></td>
                  <td class="text-left"><?php echo $product['status']; ?></td>
                  <td class="text-left">
                    <?php if(!empty($product['special'])){ ?>
                    <?php foreach($product['special'] as $special){ ?>
                      <p>
                        <?php if($special['price']){ ?>
                          <?php echo $special['price'] . ' '; ?>
                        <?php } ?>
                        <?php if($special['group_name']){ ?>
                          <?php echo $special['group_name']; ?>
                        <?php } ?>
                      </p>
                      <p>
                        <?php if($special['date_start']){ ?>
                          <?php echo $special['date_start'] . ' - '; ?>
                        <?php } ?>
                        <?php if($special['date_end']){ ?>
                           <?php $special['date_end']; ?>
                         <?php } ?>
                      </p>
                    <?php } ?>
                  <?php } ?>
                  </td>
                  <td class="text-right"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

  <style>
    .label-success input
    {
      color:#8fbb6c;
    }
    .label-warning input
    {
      color:#f38733;
    }    .label-danger input
    {
      color:#f56b6b;
    }
  </style>

<script>
  $(document).ready(function () {

  $('input[name=sku]').on('keyup',function (){
var val = $(this).val();
var product_id = $(this).data('product');
$.post('index.php?route=catalog/product/updateSku&token=<?php echo $token; ?>', 'sku='+val+'&product_id='+product_id)
  })
    $('input[name=quantity]').on('keyup',function (){
var val = Number($(this).val());
var product_id = $(this).data('product');


$.post('index.php?route=catalog/product/updateQa&token=<?php echo $token; ?>', 'quantity='+val+'&product_id='+product_id)
  })

  })
</script>

  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

 var filter_import_batch = $('select[name=\'filter_import_batch\']').val();

  if (filter_import_batch != '*') {
		url += '&filter_import_batch=' + encodeURIComponent(filter_import_batch);
	}
      
	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

  var filter_sku = $('input[name=\'filter_sku\']').val();

	if (filter_sku) {
		url += '&filter_sku=' + encodeURIComponent(filter_sku);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

  var filter_special = $('select[name=\'filter_special\']').val();

	if (filter_special != '*') {
		url += '&filter_special=' + encodeURIComponent(filter_special);
	}

  var filter_main_sticker = $('select[name=\'filter_main_sticker\']').val();

	if (filter_main_sticker != '*') {
		url += '&filter_main_sticker=' + encodeURIComponent(filter_main_sticker);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});

$('input[name=\'filter_sku\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_sku=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['sku'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_sku\']').val(item['label']);
	}
});
//--></script></div>
<?php echo $footer; ?>
