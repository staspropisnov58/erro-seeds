<?php
class Document {
	private $title;
	private $description;
	private $keywords;

  // OCFilter start
  private $noindex = false;
  // OCFilter end
      
	private $links = array();
	private $robots = array();
	private $styles = array();
	private $scripts = array();
	private $e_commerce = '';
	private $microdata = array();
	private $opengraph = array();



  // OCFilter start
  public function setNoindex($state = false) {
  	$this->noindex = $state;
  }

	public function isNoindex() {
		return $this->noindex;
	}
  // OCFilter end
      
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setRobots($robots) {
		$this->robots = $robots;
	}

	public function getRobots() {
		return $this->robots;
	}

	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	public function getKeywords() {
		return $this->keywords;
	}

	public function addLink($href, $rel, $hreflang = '') {
		$this->links[$href . $rel . $hreflang] = array(
				'href' => $href,
				'rel'  => $rel,
				'hreflang' => $hreflang
			);
	}

	public function getLinks() {
		return $this->links;
	}

	public function addMicrodata($microdates){

		foreach($microdates as $value){
			$microdata[] = '<script type="application/ld+json">'. "\n" . json_encode($value, JSON_UNESCAPED_UNICODE) . "\n" . '</script>' . "\n";
		}

		$microdata = implode($microdata);

		$this->microdata = $microdata;
	}

	public function getMicrodata(){
		return $this->microdata;
	}

	public function addStyle($href, $rel = 'stylesheet', $media = 'screen') {
		$this->styles[$href] = array(
			'href'  => $href,
			'rel'   => $rel,
			'media' => $media
		);
	}

	public function addOpengraph($opengraph){
		$this->opengraph = $opengraph;
	}

	public function getOpengraph(){
		return $this->opengraph;
	}

	public function getStyles() {
		return $this->styles;
	}

	public function addScript($script) {
		$this->scripts[md5($script)] = $script;
	}

	public function getScripts() {
		return $this->scripts;
	}

	public function addEcommerce($code) {
		$this->e_commerce = $code;
	}

	public function getEcommerce() {
		return $this->e_commerce;
	}
}
