<?php
class DB {

	private $sc_jetcache_query_count = 0;

	private $db;

	public function __construct($driver, $hostname, $username, $password, $database) {
		$class = 'DB\\' . $driver;

		if (class_exists($class)) {
			$this->db = new $class($hostname, $username, $password, $database);
		} else {
			exit('Error: Could not load database driver ' . $driver . '!');
		}
	}


	public function get_sc_jetcache_query_count() {
		return $this->sc_jetcache_query_count;
	}

	public function query($sql) {
	$this->sc_jetcache_query_count++;


		return $this->db->query($sql);
	}

	public function escape($value) {
		return $this->db->escape($value);
	}

	public function countAffected() {
		return $this->db->countAffected();
	}

	public function getLastId() {
		return $this->db->getLastId();
	}

	public function getConnectionLink() {
		return $this->db->getConnectionLink();
	}
}
