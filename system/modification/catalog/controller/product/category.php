<?php
class ControllerProductCategory extends Controller {
	public function index() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$no_description = false;

		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
			$no_description = true;
		} else {
			$filter = '';
		}

		if (isset($this->request->get['filter_ocfilter'])) {
			$no_description = true;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		}
		 else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			$no_description = true;
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}


		// OCFilter start
    if (isset($this->request->get['filter_ocfilter'])) {
      $filter_ocfilter = $this->request->get['filter_ocfilter'];
    } else {
      $filter_ocfilter = '';
    }
		// OCFilter end
      
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home'),
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);




			// foreach ($parts as $path_id) {
			// 	if (!$path) {
			// 		$path = (int)$path_id;
			// 	} else {
			// 		$path .= '_' . (int)$path_id;
			// 	}
      //
			// 	$category_info = $this->model_catalog_category->getCategory($path_id);
      //
      //
      //
			// 	if ($category_info) {
			// 		$data['breadcrumbs'][] = array(
			// 			'text' => $category_info['name'],
			// 			'href' => $this->url->link('product/category', 'path=' . $path . $url)
			// 		);
			// 	}
			// }
		} else {
			$category_id = 0;
		}
		$category_info = $this->model_catalog_category->getCategory($category_id);



		if ($category_info) {
			$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/category.js?v=2.1.5');

			if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

				$this->load->model('tool/opengraph');

				$this->model_tool_opengraph->addOpengraphForCategory($category_info, $this->request->get['path']);
			}

			$meta_page = ($page != 1) ? unicode_ucfirst($this->language->get('text_page')) . ' ' . $page . ' - ' : '';

			$this->document->setTitle($meta_page . $category_info['meta_title']);
			$this->document->setDescription($meta_page . $category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);
			//$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path']), 'canonical');
			if ($category_info['meta_robots'] !== ''){
				$this->document->setRobots($category_info['meta_robots']);
			}

			$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/category.js?v=2.1.5');



			$data['heading_title'] = $category_info['name'];

			$data['text_refine'] = $this->language->get('text_refine');
			$data['sender_text'] = $this->language->get('sender_text');
			$data['entry_pre_order'] = $this->language->get('entry_pre_order');
			$data['send_title_hed'] = $this->language->get('send_title_hed');
			$data['send_quantity_prod'] = $this->language->get('send_quantity_prod');
			$data['entry_tel'] = $this->language->get('entry_tel');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');
			$data['text_file_download'] = $this->language->get('text_file_download');
			$data['text_in_compare'] = $this->language->get('text_in_compare');
			$data['text_in_wishlist'] = $this->language->get('text_in_wishlist');
			$data['text_buy'] = $this->language->get('text_buy');
			$data['text_show_all'] = $this->language->get('text_show_all');
			$data['full_text'] = $this->language->get('full_text');
			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_entrance'] = $this->language->get('button_entrance');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');
			$data['text_all_category_x'] = $this->language->get('text_all_category_x');
			$data['text_filter'] = $this->language->get('text_filter');
			$data['category_id'] = $category_id;
			$data['category_name'] = $category_info["name"];

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'microdata' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
			);

			foreach ($data['breadcrumbs'] as $key =>  $value){
				$breadcrumbs_array[$key] = array(
					'@type' => "ListItem",
					"position" => $key,
					"item" => array(
						'id' => $value['microdata'],
						'name' => $value['text'],
					),
				);
			}

			$breadcrumbs = array(
				'@context' => 'https://schema.org',
				'@type' => 'BreadcrumbList',
				'itemListElement'=> $breadcrumbs_array,
			);

			$microdates = array($breadcrumbs);

			$this->document->addMicrodata($microdates);


			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			} else {
				$data['thumb'] = '';
			}



			if ($category_info['image_2']) {
				$data['image_2'] = $this->model_tool_image->resize($category_info['image_2'], 500, 228);
			} else {
				$data['image_2'] = '';
			}

			$data['category_name'] = $category_info['name'];
			$data['description'] = $no_description ? '' : html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['description_2'] = $no_description ? '' : html_entity_decode($category_info['description_2'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['categories'] = array();

			$results = $this->model_catalog_category->getCategories($category_id);

			foreach ($results as $result) {
				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);

				if ($result['image'] && is_file(DIR_IMAGE . $result['image'])) {
					$image['extension'] = pathinfo($result['image'], PATHINFO_EXTENSION);

					if($image['extension'] === 'svg') {
						$image['value'] = $this->model_tool_image->renderSVG($result['image']);
					} else {
						$image['value'] = $this->model_tool_image->resize($result['image'], 126, 42);
					}
				} else {
					$image['extension'] = 'png';
					$image['value'] = $this->model_tool_image->resize('small-placeholder.png', 126, 42);
				}


				$data['categories'][] = array(
					'name'  => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href'  => $this->url->link('product/category', 'path=' . $result['category_id']),
					'image' => $image,
				);
			}



			$data['products'] = array();

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit,
				'path'							 => str_replace('/','_',$this->request->get['route'])
			);

			$filter_data['filter_option'] = (int)$category_id === 173 ? '53, 96' : '';


  		// OCFilter start
  		$filter_data['filter_ocfilter'] = $filter_ocfilter;
  		// OCFilter end
      
			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$data['config_limit'] = $this->config->get('config_product_limit');
			$data['product_total'] = $product_total;

			$display_settings['display'] = isset($this->request->cookie['display_list']) ? 'list' : 'grid';
			$display_settings['filter_data'] = $filter_data;

			$data['products'] = $this->load->controller('product/product/displayProducts', $display_settings);

			$data['filter_data'] = json_encode($filter_data);


			$url = '';


      // OCFilter start
			if (isset($this->request->get['filter_ocfilter'])) {
				$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
			}
      // OCFilter end
      
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}


			/*$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);*/

			$url = '';


      // OCFilter start
			if (isset($this->request->get['filter_ocfilter'])) {
				$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
			}
      // OCFilter end
      
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			$data['limits'] = array();

			$limits = array_unique(array($this->config->get('config_product_limit'), 18, 36, 72/*, 100*/));

			sort($limits);

			foreach($limits as $value) {
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value),
                );
			}

			$url = '';


      // OCFilter start
			if (isset($this->request->get['filter_ocfilter'])) {
				$url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
			}
      // OCFilter end
      
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

						//Canonization

			if( 1 >= ceil($product_total/$limit)){
			$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path']), 'canonical');
			}else{
			if(($page == 1) ){
				$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path']), 'canonical');
				$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page=2'), 'next');
			}else{
				if($page == ceil($product_total/$limit)){
					$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page=' . ($page-1)), 'prev');
					$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page='.$page), 'canonical');
				}else{
					if($page === '2'){
						$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path']), 'prev');
						$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page='.$page), 'canonical');
						$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page='. ($page+1)), 'next');
					}else{
						$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page=' . ($page-1)), 'prev');
						$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page='.$page), 'canonical');
						$this->document->addLink($this->url->link('product/category', 'path=' . $this->request->get['path'] . '&page='.($page+1)), 'next');
					}
				}
			}
			}

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

      // OCFilter Start
      $ocfilter_page_info = $this->load->controller('module/ocfilter/getPageInfo');

      if ($ocfilter_page_info) {
        $this->document->setTitle($ocfilter_page_info['meta_title']);

        if ($ocfilter_page_info['meta_description']) {
			    $this->document->setDescription($ocfilter_page_info['meta_description']);
        }

        if ($ocfilter_page_info['meta_keyword']) {
			    $this->document->setKeywords($ocfilter_page_info['meta_keyword']);
        }

			  $data['heading_title'] = $ocfilter_page_info['title'];

        if ($ocfilter_page_info['description'] && !isset($this->request->get['page']) && !isset($this->request->get['sort']) && !isset($this->request->get['order']) && !isset($this->request->get['search']) && !isset($this->request->get['limit'])) {
        	$data['description'] = html_entity_decode($ocfilter_page_info['description'], ENT_QUOTES, 'UTF-8');
        }
      } else {
        $meta_title = $this->document->getTitle();
        $meta_description = $this->document->getDescription();
        $meta_keyword = $this->document->getKeywords();

        $filter_title = $this->load->controller('module/ocfilter/getSelectedsFilterTitle');

        if ($filter_title) {
          if (false !== strpos($meta_title, '{filter}')) {
            $meta_title = trim(str_replace('{filter}', $filter_title, $meta_title));
          } else {
            $meta_title .= ' ' . $filter_title;
          }

          $this->document->setTitle($meta_title);

          if ($meta_description) {
            if (false !== strpos($meta_description, '{filter}')) {
              $meta_description = trim(str_replace('{filter}', $filter_title, $meta_description));
            } else {
              $meta_description .= ' ' . $filter_title;
            }

  			    $this->document->setDescription($meta_description);
          }

          if ($meta_keyword) {
            if (false !== strpos($meta_keyword, '{filter}')) {
              $meta_keyword = trim(str_replace('{filter}', $filter_title, $meta_keyword));
            } else {
              $meta_keyword .= ' ' . $filter_title;
            }

           	$this->document->setKeywords($meta_keyword);
          }

          $heading_title = $data['heading_title'];

          if (false !== strpos($heading_title, '{filter}')) {
            $heading_title = trim(str_replace('{filter}', $filter_title, $heading_title));
          } else {
            $heading_title .= ' ' . $filter_title;
          }

          $data['heading_title'] = $heading_title;

          $data['description'] = '';
        } else {
          $this->document->setTitle(trim(str_replace('{filter}', '', $meta_title)));
          $this->document->setDescription(trim(str_replace('{filter}', '', $meta_description)));
          $this->document->setKeywords(trim(str_replace('{filter}', '', $meta_keyword)));

          $data['heading_title'] = trim(str_replace('{filter}', '', $data['heading_title']));
        }
      }
      // OCFilter End
      
			$data['all'] = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $product_total);
			$data['all_products'] =  $product_total;
			$data['page'] = $page;


			$data['continue'] = $this->url->link('common/home');

            $data['lincks_cat']='';


            $lincks_hed = $this->cache->get('lincks_cat_.' . (int)$this->config->get('config_language_id').$category_id);
            if (!$lincks_hed) {
                $data['lincks_cat'] = $this->PereLinkCategory();
                $this->cache->set('lincks_cat_.' . (int)$this->config->get('config_language_id').$category_id, $data['lincks_cat']);
            }else{
                $data['lincks_cat'] = $this->cache->get('lincks_cat_.' . (int)$this->config->get('config_language_id').$category_id);
            }


			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/category.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');


			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

    public function PereLinkCategory(){
        $data = file("https://errors-seeds.com.ua/published/perelink/file/link-Category.txt");
        $total = count($data);
        srand((double)microtime()*1000000);
        $mn = 5; // сколько выводить минимум
        $mx = 5; // сколько выводить максимум
        $crosslinks = "";
        for($i=0; $i<mt_rand($mn,$mx); $i++){
            $s = mt_rand(0,$total-1);
            $crosslinks .= "".$data[$s];
        }

        return "$crosslinks";
    }
}
