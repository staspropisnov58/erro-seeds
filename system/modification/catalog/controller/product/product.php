<?php
class ControllerProductProduct extends Controller {
	private $limit = 5;
	private $error = array();

	public function index() {

			if(isset($this->session->data['products_id']))
			{
				if(!in_array($this->request->get['product_id'], $this->session->data['products_id']))
					$this->session->data['products_id'][] = $this->request->get['product_id'];
			}
			else $this->session->data['products_id'][] = $this->request->get['product_id'];
            
		$this->load->language('product/product');

		$this->session->data['products_id'][$this->request->get['product_id']] = $this->request->get['product_id'];

		$this->load->model('module/ajaxzoom');
		$data['ajaxzoom'] = $this->model_module_ajaxzoom->hookProduct($this->request->get['product_id']);
		$data['ajaxzoom'] = '';

		if ($this->config->get('convead_status')) {
			$this->load->model('module/convead');
			if ($this->request->get['product_id']) $this->model_module_convead->productViewed($this->request->get['product_id']);
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home'),
			'microdata' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path),
						'microdata' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url),
					'microdata' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)

				);
			}
		}

		$main_category = $this->model_catalog_category->getMainCategory($this->request->get['product_id']);
		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer'),
				'microdata' => $this->url->link('product/manufacturer')

			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url),
					'microdata' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)

				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url),
				'microdata' => $this->url->link('product/search', $url)

			);
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'microdata' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			if($this->config->get('opengraph_status') !== NULL && $this->config->get('opengraph_status')){

				$this->load->model('tool/opengraph');

				$this->model_tool_opengraph->addOpengraphForProduct($product_info);
			}


			$this->document->setTitle($product_info['meta_title']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/lightbox.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
			$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/product.js');


			if ($product_info['meta_robots'] !== ''){
				$this->document->setRobots($product_info['meta_robots']);
			}

			$data['heading_title'] = $product_info['name'];


			$data['text_products_reviews'] = $this->language->get('text_products_reviews');
			$data['text_reviews_warning'] = $this->language->get('text_reviews_warning');
			$data['text_select'] = $this->language->get('text_select');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_stock'] = $this->language->get('text_stock');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_write'] = $this->language->get('text_write');
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
			$data['text_note'] = $this->language->get('text_note');
			$data['text_tags'] = $this->language->get('text_tags');
			$data['text_related'] = $this->language->get('text_related');
            $data['text_recomended'] = $this->language->get('text_recomended');
			$data['text_loading'] = $this->language->get('text_loading');
			$data['text_in_compare'] = $this->language->get('text_in_compare');
			$data['text_in_wishlist'] = $this->language->get('text_in_wishlist');

			$data['entry_qty'] = $this->language->get('entry_qty');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['entry_bad'] = $this->language->get('entry_bad');
			$data['entry_captcha'] = $this->language->get('entry_captcha');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_upload'] = $this->language->get('button_upload');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['text_economy'] = $this->language->get('text_economy');
			$data['text_no_reviews'] = $this->language->get('text_no_reviews');
			$data['entry_pre_order'] = $this->language->get('entry_pre_order');
			$this->load->model('catalog/review');

			$data['tab_description'] = $this->language->get('tab_description');
			$data['tab_attribute'] = $this->language->get('tab_attribute');
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['manufacturer'] = $product_info['manufacturer'];
			$data['alt_image'] = $product_info['alt_image'];
			$data['title_image'] = $product_info['title_image'];
			$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['model'] = $product_info['model'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];

			$data['current_page'] = $this->url->link('product/product', 'product_id=' . $product_id);
			$data['form'] = '';
            $data['entry_pre_order'] = $this->language->get('entry_pre_order');
            $data['entry_tel'] = $this->language->get('entry_tel');

			if ($product_info['quantity'] <= 0) {
				// TODO: get notification request form
				$data['stock'] = $product_info['stock_status'];

				if ($this->config->get('notification_request_status')) {
					$this->load->language('module/notification_request');

					$product_data = [];
					$product_data['product_id'] = $product_id;
					$product_data['name'] = $product_info['name'];
					$data = array_merge($data, $this->load->controller('module/notification_request/renderForm', $product_data));
				}
			} else {
				$form = new Form('add_to_cart', $this->url->link('checkout/cart/add', '', 'SSL'));
				$data['form'] = $form->build();

				$data['stock'] = $this->language->get('text_instock');
			}


			$data['quantity'] = $product_info['quantity'];
			$data['order_pre'] = $product_info['order_pre'];

			$this->load->model('tool/image');
			$data['images'] = array();

			if ($product_info['image']) {
				$data['images'][0]['popup'] = '/image/' . $product_info['image'];
			} else {
				$data['images'][0]['popup'] = '';
			}

			if ($product_info['image']) {
				$data['images'][0]['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
			} else {
				$data['images'][0]['thumb'] = '';
			}

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => '/image/' . $result['image'],
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'))
				);
			}

			$data['options'] = $this->getProductOptions($product_info);
			if (array_key_exists('update', $data['options'])) {
				foreach($data['options']['update'] as $key => $value) {
					$product_info[$key] = $value;
				}
				unset($data['options']['update']);
			}

			// var_dump($this->config->get('config_tax'));

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));

			} else {
				$data['price'] = false;
			}

			if ((float)$product_info['special']) {
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				$data['saving'] = $this->currency->format($this->tax->calculate((float)$product_info['price'] - (float)$product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['special'] = false;
				$data['saving'] = '';
			}

			$data['stickers'] = $this->model_catalog_product->getStickers($product_info);
//            if($this->customer->isLogged() && $this->customer->getGroupId()==3  ){
//                $data['stickers'][] =array(
//                  'class' => "sticker sticker-sale",
//                  'text' => "11%",
//
//                );
//               //print_r($data['stickers']);
//            }
			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}

			$data['in_compare'] = isset($this->session->data['compare']) && in_array($this->request->get['product_id'], $this->session->data['compare']) ? $this->url->link('product/compare') : '';
			$data['in_wishlist'] = isset($this->session->data['wishlist']) && in_array($this->request->get['product_id'], $this->session->data['wishlist']) ? $this->url->link('account/wishlist') : '';

			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['rating'] = (int)$product_info['rating'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$data['seo_text'] = html_entity_decode($product_info['seo_text'], ENT_QUOTES, 'UTF-8');
			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

			$this->load->model('tool/image');

			foreach ($data['attribute_groups'] as $key => &$attribute_groups) {
				foreach ($attribute_groups['attribute'] as &$attribute_group) {
					$image = [];

					if ($attribute_group['image'] && is_file(DIR_IMAGE . $attribute_group['image'])) {
						$image['extension'] = pathinfo($attribute_group['image'], PATHINFO_EXTENSION);

						if($image['extension'] === 'svg') {
							$image['value'] = $this->model_tool_image->renderSVG($attribute_group['image']);
						} else {
							$image['value'] = $this->model_tool_image->resize($attribute_group['image'], $this->config->get('config_image_attribute_product_width'), $this->config->get('config_image_attribute_product_height'));
						}
					} else {
						$image['extension'] = 'png';
						$image['value'] = $this->model_tool_image->resize('small-placeholder.png', $this->config->get('config_image_attribute_product_width'), $this->config->get('config_image_attribute_product_height'));
					}

					$attribute_group['image'] = $image;

					if($attribute_group['filter_link'] != ''){
						$attribute_group['filter_link'] = $this->url->link('product/category', 'path=' . $main_category['category_path_id']) . $attribute_group['filter_link'];
					}
				}
			}
//					    echo '<pre style="display: none">';
//					    print_r($data['options']);
//					    echo '</pre>';
			$data['related_products'] = '';

			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
			$related_data['products'] = [];


			foreach ($results as $result) {
				$stickers = $this->model_catalog_product->getStickers($result);

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$related_data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'stickers'		=> $stickers,
					'tax'         => $tax,
					'rating'      => $rating,
          'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

			$related_data['href'] = '';
			$related_data['heading_title'] = $this->language->get('text_related_product');

			$data['related_products'] = $this->load->view($this->config->get('config_template') . '/template/module/product_list/horizontal_slider.tpl', $related_data);


            $data['recomended_products'] = '';
            if($this->config->get('recomended_products_status') !== NULL && $this->config->get('recomended_products_status')){
                $results = $this->model_catalog_product->getProductRecomended($this->request->get['product_id']);
            }else{
                $results = array();
            }

            $recomended_data['products'] = [];

            foreach ($results as $result) {
                $stickers = $this->model_catalog_product->getStickers($result);

                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 172, 172);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 172,172);
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }

                $recomended_data['products'][] = array(
                    'product_id'  => $result['product_id'],
                    'thumb'       => $image,
                    'name'        => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'stickers'		=> $stickers,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
            }

            $recomended_data['href'] = '';
            $recomended_data['heading_title'] = $this->language->get('text_recomended_product');
            $recomended_data['setting_prod'] = true;


            $data['recomended_products'] = $this->load->view($this->config->get('config_template') . '/template/module/product_list/horizontal_slider.tpl', $recomended_data);


            $data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}

			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

            // start rew
      $this->load->model('catalog/review');
      $data['reviews'] = array();

      $review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

			$reviews = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id']);

			foreach ($reviews as $review) {
					$data['reviews'][] = array(
							'author'     => $review['author'],
							'title'      => $review['title'],
							'text'       => nl2br($review['text']),
							'rating'     => (int)$review['rating'],
							'review_id'    => $review['review_id'],
							'date_added' => date($this->language->get('date_format_short'), strtotime($review['date_added']))
					);
			}


			$data['write_review'] = 'getModal(\'modal=review&product_id=' . $product_id . '\', event)';

			$aggregateRating = array(
			              '@type' => 'AggregateRating',
			              'ratingValue' => $product_info['rating'],
			            );

			            if((int)$product_info['reviews'] !== 0){
			              $aggregateRating['reviewCount'] = $product_info['reviews'];
			            }else{
			              $aggregateRating['ratingCount'] = 10;
			            }

			            if($product_info['quantity'] > 0){
			              $availability = 'https://schema.org/InStock';
			            }else{
			              $availability = 'https://schema.org/OutOfStock';
			            }

			            if(!empty($data['reviews'])){
			              $last_review = end($data['reviews']);

			              $review = array(
			                '@type' => 'Review',
			                'author' => $last_review['author'],
			                'ratingValue' => $last_review['rating'],
			                'bestRating' => 5,
			                'datePublished' => $last_review['date_added'],
			                'reviewBody' => $last_review['text']
			              );

			            }else{
			              $review = array();
			            }


			            $offers = array(
			              '@type' => 'Offer',
			              'availability' => $availability,
			              'price' => number_format((float)$product_info['price'], 2, '.', ''),
			              'priceCurrency' => $this->config->get('config_currency'),
			              'priceValidUntil' => date('Y-m-d', strtotime('+1 year')),
			              'url' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']),
			            );

			            $brand = array(
			              '@type' => 'Thing',
			              'name' => $product_info['model']
			            );

			            $microdata = array(
			              '@type' => 'Product',
			              '@context' => 'https://schema.org',
			              'aggregateRating'=> $aggregateRating,
			              'description' => html_entity_decode($product_info['meta_description']),
			              'name' => $product_info['name'],
			              'image' => $product_info['image'],
			              'offers' => $offers,
			              'url' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']),
			              'brand' => $brand,
			              'sku' => $product_info['sku']
			            );

			            if(!empty($review)){
			              $microdata['review'] = $review;
			            }

			            foreach ($data['breadcrumbs'] as $key =>  $value){
			              $breadcrumbs_array[$key] = array(
			                '@type' => "ListItem",
			                "position" => $key,
			                "item" => array(
			                  'id' => $value['microdata'],
			                  'name' => $value['text'],
			                ),
			              );
			            }

			            $breadcrumbs = array(
			              '@context' => 'https://schema.org',
			              '@type' => 'BreadcrumbList',
			              'itemListElement'=> $breadcrumbs_array,
			            );

			            $microdates = array($breadcrumbs, $microdata);

			            $this->document->addMicrodata($microdates);

						$data['quantity_comments'] = count($data['reviews']);

            $data['all_rew'] =  $data['reviews'];

						$data['limit'] = $this->limit;


            $data['lincks_prod']='';


                $lincks_hed = $this->cache->get('lincks_prod_.' . (int)$this->config->get('config_language_id').$product_info['product_id']);
                if (!$lincks_hed) {
                    $data['lincks_prod'] = $this->PereLinkProduct();
                    $this->cache->set('lincks_prod_.' . (int)$this->config->get('config_language_id').$product_info['product_id'], $data['lincks_prod']);
                }else{
                    $data['lincks_prod'] = $this->cache->get('lincks_prod_.' . (int)$this->config->get('config_language_id').$product_info['product_id']);
                }




			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/product.tpl', $data));
			}
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}



			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error')
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

            $this->load->language('product/product');

            $this->load->model('catalog/review');

            $data['text_no_reviews'] = $this->language->get('text_no_reviews');

            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }





			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}


	public function review() {
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = $this->limit;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/review.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/review.tpl', $data));
		}
	}

	public function getRecurringDescription() {
		$this->language->load('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function update_like(){
        $this->load->model('catalog/review');
        $like = $_POST['like'];
        $this->model_catalog_review->updateLike((int)$_POST['review_id'],$like);
        echo 1;
    }

		public function getProductOptions($product_info) {
			$this->load->model('catalog/product');

			$options = array();
            if(isset($product_info['product_id'])) {
                $product_price = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'];



                $product_old_price = $product_info['price'];
                $options['required'] = 0;
                $first_option = 0;
                foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                    $product_option_value_data = array();
                    foreach ($option['product_option_value'] as $key => $option_value) {
                        if (!empty($product_info['show_with_bulk_option'])) {
                            if (in_array((int)$option_value['option_value_id'], $product_info['show_with_bulk_option'])) {
                                $first_option = $key;
                            } else {
                                continue;
                            }
                        }

                        if ($option_value['subtract'] && $option_value['quantity'] <= 0) {
                            $first_option++;
                            continue;
                        } else {
                            $price = false;
                            $old_price = false;

                            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                                $option_price = (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price;
                                $option_old_price = 0;

                                if ($product_info['product_special_id']) {
                                    $special = $this->model_catalog_product->getProductOptionSpecial($option_value['product_option_value_id'], $product_info['product_special_id']);

                                    if ($special) {
                                        if ((int)$special['price']) {
                                            $option_price = (float)($option_value['price_prefix'] . $special['price']) + (float)$product_price;
                                            $option_old_price = (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price;
                                        } else {
                                            $option_price = (float)$option_value['price'] ? (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price : $product_price;
                                            $option_old_price = (float)$option_value['price'] ? 0 : $product_old_price;
                                        }
                                    } else {
                                        $option_price = (float)$option_value['price'] ? (float)($option_value['price_prefix'] . $option_value['price']) + (float)$product_old_price : $product_price;
                                        $option_old_price = (float)$option_value['price'] ? 0 : $product_old_price;
                                    }
                                } else {

                                }

                                $price = $this->currency->format($this->tax->calculate($option_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                if ($option_old_price) {
                                    $old_price = $this->currency->format($this->tax->calculate($option_old_price, $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                }

                                if ($key === $first_option && $option_value['price']) {
                                    if ($product_info['special'] && $option_old_price) {
                                        $options['update']['special'] = $option_price;
                                        $options['update']['price'] = $option_old_price;
                                    } else {
                                        $options['update']['special'] = 0;
                                        $options['update']['price'] = $option_price;
                                    }
                                } else {
                                }
                            }

                            $image = [];
                            $trigger_image = '';

                            if ($option_value['image']) {
                                $image['extension'] = pathinfo($option_value['image'], PATHINFO_EXTENSION);

                                if ($image['extension'] === 'svg') {
                                    $image['value'] = $this->model_tool_image->renderSVG($option_value['image']);
                                } else {
                                    $image['value'] = $this->model_tool_image->resize($option_value['image'], 60, 36);
                                }

                                $trigger_image = pathinfo($option_value['image'], PATHINFO_FILENAME);
                            }

                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'quantity' => $option_value['quantity'],
                                'image' => $image,
                                'trigger_image' => $trigger_image,
                                'price' => $price,
                                'price_dev' => $option_value['price_dev'],
                                'old_price' => isset($old_price) ? $old_price : '',
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }

                    if ($product_option_value_data) {
                        $options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required']
                        );
                        if ($option['required']) {
                            $options['required'] = 1;
                        }
                    }
                }
            }

			return $options;
		}

		public function displayProducts($display_settings)
		{
			$this->load->language('product/category');
			$this->load->model('catalog/product');
			$this->load->model('tool/image');

			$data['current_page'] = $this->request->server['REQUEST_URI'];

			$path = isset($display_settings['filter_data']['path']) ? $display_settings['filter_data']['path'] : '';

			if ($display_settings['display'] === 'list') {
				setcookie('display_list', 1, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
			} else {
				setcookie('display_list', '', time() - 3600, '/', $this->request->server['HTTP_HOST']);
			}

			$data['text_in_compare'] = $this->language->get('text_in_compare');
			$data['text_in_wishlist'] = $this->language->get('text_in_wishlist');
			$data['text_buy'] = $this->language->get('text_buy');
			$data['alt'] = $this->language->get('alt');
			$data['title'] = $this->language->get('title');


			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_entrance'] = $this->language->get('button_entrance');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');

			$data['entry_pre_order'] = $this->language->get('entry_pre_order');
			$data['entry_tel'] = $this->language->get('entry_tel');

			$data['products'] = array();
             $data['cat_id'] = '';
			$show_with_bulk_option = [];
			if (isset($display_settings['filter_data']['filter_category_id'])) {
                $data['cat_id'] = (int)$display_settings['filter_data']['filter_category_id'];
				$show_with_bulk_option = (int)$display_settings['filter_data']['filter_category_id'] === 173 ? [53, 96]: [];
				$display_settings['filter_data']['filter_option'] = implode(', ', $show_with_bulk_option);
			}

			if ($path === 'product_special') {
				$results = $this->model_catalog_product->getProductSpecials($display_settings['filter_data']);
			} else {
				$results = $this->model_catalog_product->getProducts($display_settings['filter_data']);
			}

			foreach ($results as $result) {
				$result['show_with_bulk_option'] = $show_with_bulk_option;

				$stickers = $this->model_catalog_product->getStickers($result);
				$options = $this->getProductOptions($result);
				if (array_key_exists('update', $options)) {
					foreach($options['update'] as $key => $value) {
						$result[$key] = $value;
					}
				}


				$option = [];

				if ($show_with_bulk_option) {
					$option = [
					    'name' => $options[0]['name'],
					    'price_dev' => $options[0]['product_option_value'][0]['price_dev'],
				        'value' => $options[0]['product_option_value'][0]['name'],
									   'product_option_id' => $options[0]['product_option_id'],
									   'product_option_value_id' => $options[0]['product_option_value'][0]['product_option_value_id']];
				}

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));




				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$attribute_groups = $this->model_catalog_product->getProductAttributes($result['product_id']);

					if ($options['required'] && $option === []) {
						$onclick = 'getModal(\'modal=options&product_id=' . $result['product_id'] . '\', event)';
						$product_form = [];
					} else {
						$onclick = 'cart.add(' . $result['product_id'] . ', 1, event)';
						$form = new Form('add_to_cart', $this->url->link('checkout/cart/add', '', 'SSL'));
						$product_form = $form->build();
					}

					if ($result['quantity'] <= 0) {
						$onclick = 'getModal(\'modal=notification_request&product_id=' . $result['product_id'] . '\', event)';
						$stock = $result['stock_status'];
						$product_form = [];
					} else {
						$stock = $this->language->get('text_instock');
					}
					if ((int)$display_settings['filter_data']['filter_category_id'] === 173){

                        if($option['price_dev']>0){
                            $special = $price;
                            $price = $this->currency->format($this->tax->calculate($option['price_dev'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));

                        }
                    }

				$data['products'][] = array(
					'product_id'  			=> $result['product_id'],
					'cat_id'  			=> $data['cat_id'],
					'quantity'    			=> $result['quantity'],
					'order_pre'    			=> $result['order_pre'],
					'model'							=> $result['model'],
					'thumb'       			=> $image,
					'name'        			=> $result['name'],
					'alt_image'					=> $result['alt_image'],
					'title_image' 			=> $result['title_image'],
					'description' 			=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'attribute_groups'  => $attribute_groups,
					'price'       			=> $price,
					'stickers'					=> $stickers,
					'manufacturer'			=> $result['manufacturer'],
					'special'     			=> $special,
					'tax'         			=> $tax,
					'rating'      			=> $result['rating'],
					'href'        			=> $this->url->link('product/product', 'path=' . $path . '&product_id=' . $result['product_id']),
					'alt' 							=> sprintf($data['alt'], $result['name']),
					'title' 						=> sprintf($data['title'], $result['name']),
					'in_compare'				=> isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare']) ? $this->url->link('product/compare') : '',
					'in_wishlist' 			=> isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist']) ? $this->url->link('account/wishlist') : '',
					'onclick'     			=> $onclick,
					'option'            => $option,
					'form'              => $product_form,
					'stock'             => $stock,
				);
			}

	    return $data['products'] ? $this->load->view($this->config->get('config_template') . '/template/product/product_' . $display_settings['display'] . '.tpl', $data) : '';
		}

		public function autocomplete()
		{
			$json = [];

			if (isset($this->request->get['search'])) {
				$this->load->model('catalog/product');

				if (isset($this->request->get['search'])) {
					$filter_name = $this->request->get['search'];
					$filter_model = $this->request->get['search'];
					$filter_sku = $this->request->get['search'];
				} else {
					$filter_name = $filter_model = $filter_sku = '';
				}

				if (isset($this->request->get['limit'])) {
					$limit = $this->request->get['limit'];
				} else {
					$limit = 5;
				}

				$filter_data = array(
					'filter_name'  => $filter_name,
					'filter_model' => $filter_model,
					'filter_sku' => $filter_sku,
					'start'        => 0,
					'limit'        => $limit,
					'path'         => 'product_search',
					'sort'         => 'default',
				);

				$results = $this->model_catalog_product->getProducts($filter_data);
				$json['products'] = [];

				foreach ($results as $result) {
                    $result['show_with_bulk_option'] = [];

                    $option_data = array();
                    if ($result['product_id']) {
                        $options = $this->getProductOptions($result);
                        if (array_key_exists('update', $options)) {
                            foreach ($options['update'] as $key => $value) {
                                $result[$key] = $value;
                            }
                        }

                        if ($result['image']) {
                            $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                        }

                        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                            $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                        } else {
                            $price = false;
                        }

                        if ((float)$result['special']) {
                            $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                        } else {
                            $special = false;
                        }

                        $json['products'][] = array(
                            'product_id' => $result['product_id'],
                            'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                            'model' => $result['model'],
                            'price' => $price,
                            'special' => $special,
                            'image' => $image,
                            'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                            'sku' => $result['sku']
                        );
                    }
                }
			}

			$this->load->language('product/search');
			if ($json['products']) {
				$json['href'] = $this->url->link('product/search', '&search=' . $this->request->get['search']);
				$json['text_search'] = $this->language->get('text_search');
			} else {
				$json['text_empty'] = $this->language->get('text_empty');
			}

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}

    public function PereLinkProduct(){
        $data = file("https://errors-seeds.com.ua/published/perelink/file/link-Product.txt");
        $total = count($data);
        srand((double)microtime()*1000000);
        $mn = 5; // сколько выводить минимум
        $mx = 5; // сколько выводить максимум
        $crosslinks = "";
        for($i=0; $i<mt_rand($mn,$mx); $i++){
            $s = mt_rand(0,$total-1);
            $crosslinks .= "".$data[$s];
        }

        return "$crosslinks";
    }
}
