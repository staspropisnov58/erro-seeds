<?php
class ModelCatalogProduct extends Model {
	private $sort_strings = ['name' => 'LCASE(pd.name)',
													 'price' => '(CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)',
													 'quantity' => 'in_stock',
													 'rating' => 'rating',
													 'model' => 'LCASE(p.model)',
													 'date_added' => 'p.date_added',
													 'sort_order' => 'p.sort_order',
												  'date_start' => 'ISNULL(ps.date_start)'];
	public function updateViewed($product_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = (viewed + 1) WHERE product_id = '" . (int)$product_id . "'");
	}

	public function getProduct($product_id) {

		$sql = "SELECT DISTINCT *,";

		if($this->config->get('product_stickers_status')){
			$sql .= " p.main_stickers,";
		}

		$sql .= " p.meta_robots, pd.name AS name, ";

		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL && $this->config->get('opengraph_status') !== NULL && (int)$this->config->get('opengraph_status') === 1){
			$sql .=" pd.og_title, pd.og_description, pd.og_image, ";
		}

		$sql .=" p.image, m.name AS manufacturer,
			(SELECT price FROM " . DB_PREFIX . "product_discount pd2
				WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
				AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW())
				AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW()))
				ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1)
			AS discount,
			(SELECT price FROM " . DB_PREFIX . "product_special ps
				WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
				AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))
				ORDER BY ps.priority ASC, ps.price ASC LIMIT 1)
			AS special,
			(SELECT points FROM " . DB_PREFIX . "product_reward pr
				WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "')
			AS reward,
			(SELECT ss.name FROM " . DB_PREFIX . "stock_status ss
				WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "')
			AS stock_status,
			(SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd
				WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "')
			AS weight_class,
			(SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd
				WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "')
			AS length_class,
			(SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1
				WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id)
			AS rating,
			(SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2
				WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id)
			AS reviews,
			p.sort_order
			FROM " . DB_PREFIX . "product p
			LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
			LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id)
			WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'
			AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

			$query = $this->db->query($sql);

		if(isset($query->row['main_stickers'])){
			$main_stickers = unserialize($query->row['main_stickers']);
		}else{
			$main_stickers = array();
		}

		if ($query->num_rows && !$this->isAddition($product_id)) {
			$product_special = $this->getProductSpecial($product_id);
			$customers_rating = isset($query->row['customers_rating']) ? $query->row['customers_rating'] : 0;


            if($this->customer->isLogged() && $this->customer->getGroupId()==3  ){


            $sale_price_category = $this->getCategoriesMain($product_id);

            if($sale_price_category['sale_price']>0){
                $pr = $this->percent($query->row['price'],$sale_price_category['sale_price']);
               $product_special['price'] = $pr;
            }else{
                $summ_sale = $this->customer->getSummeSale();
                $summ_sale_group = $this->customer->getSummeSaleGroup();
                $procent_sale = $this->customer->getProcentSale();
                $procent_sale_group = $this->customer->getProcentSaleGroup();
                $procent_sale_bonus = $this->customer->getProcentSaleBonus();
                $customer_balance = $this->customer->getSummBalance();
                if($summ_sale < $customer_balance && $summ_sale !=0 && !isset($product_special['price'])){
                    $pr = $this->percent($query->row['price'],$procent_sale);
                    $product_special['price'] = $pr;
                }elseif ($summ_sale_group < $customer_balance && $summ_sale_group!=0 && !isset($product_special['price'])){

                    $pr = $this->percent($query->row['price'],$procent_sale_group);
                    $product_special['price'] = $pr;

                }
            }




            }

			$product_array = array(
				'product_id'       => $query->row['product_id'],
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'order_pre'      => $query->row['order_pre'],
				'alt_image'				 => $query->row['alt_image'],
				'main_stickers'		 => $main_stickers,
				'title_image'			 => $query->row['title_image'],
				'meta_title'       => $query->row['meta_title'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'meta_robots'			 => $query->row['meta_robots'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $product_special ? $product_special['price'] : 0,
				'product_special_id' => $product_special ? $product_special['product_special_id'] : 0,
				'sale_event' => isset($product_special['sale_event']) ? $product_special['sale_event'] : '',
				'discount'         => $query->row['discount'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating'] + $customers_rating),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed'],
				'seo_text'         => $query->row['seo_text'],
		);


		if($this->config->get('opengraph_use_meta_when_no_og') !== NULL && $this->config->get('opengraph_status') !== NULL && (int)$this->config->get('opengraph_status') === 1){
			$product_array['og_title'] = $query->row['og_title'];
			$product_array['og_description'] = $query->row['og_description'];
			$product_array['og_image'] = $query->row['og_image'];
		}
			return $product_array;
		} else {
			return false;
		}
	}
    public function percent($number,$procent) {

        $percent = $procent; // Необходимый процент
        $number_percent = $number / 100 * $percent;

        return $number-$number_percent;

    }
	public function getProductsOLD($data = array()) {
		$sql = "SELECT p.product_id, (CASE WHEN p.quantity > 0 THEN 1 ELSE 0 END) AS in_stock, ps.date_start, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}

			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}

		$sql .= " LEFT JOIN (SELECT date_start, product_special_id, product_id FROM " . DB_PREFIX . "product_special WHERE customer_group_id = '" . $this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW()))) ps ON (p.product_id = ps.product_id)";

		if (!empty($data['filter_option'])) {
			$sql .= " INNER JOIN oc_product_option_value pov ON p.product_id = pov.product_id ";
		}

		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
			}

			if (!empty($data['filter_filter'])) {
				$implode = array();

				$filters = explode(',', $data['filter_filter']);

				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}

				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		} else {
			$data['filter_category_id'] = 0;
		}

		if (!empty($data['filter_option'])) {
			$sql .= " AND pov.option_value_id IN(". $data['filter_option'] .") AND p.quantity > pov.quantity AND pov.status = 1 ";
		}

		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";

			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}

			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}

			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			$sql .= ")";
		}


		// OCFilter start
		if (!empty($data['filter_ocfilter'])) {
    	$this->load->model('catalog/ocfilter');

      $ocfilter_product_sql = $this->model_catalog_ocfilter->getProductSQL($data['filter_ocfilter']);

			if ($ocfilter_product_sql) {
			  $sql .= $ocfilter_product_sql;
			}
		}
		// OCFilter end
      
		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}

		$sql .= " GROUP BY p.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.quantity',
			'p.price',
			'rating',
			'p.sort_order',
			'p.date_added',
			'ps.product_special'
		);

		$config_sorts = $this->config->get('sorting_status') && $this->config->get('sorting_' . $data['path']) ? $this->config->get('sorting_' . $data['path']) : [];

		if ($data['sort'] === 'default' && count($config_sorts['default']) === 2 && !in_array((int)$data['filter_category_id'], [98, 63, 221])) {

				$first_order = substr($config_sorts['default']['first'], strrpos($config_sorts['default']['first'], "_", -0) + 1);
				$first_sort = $this->sort_strings[stristr($config_sorts['default']['first'], '_' . $first_order, true)];

				$second_order = substr($config_sorts['default']['second'], strrpos($config_sorts['default']['second'], "_", -0) + 1);
				$second_sort = $this->sort_strings[stristr($config_sorts['default']['second'], '_' . $second_order, true)];

				$sql .= " ORDER BY " . $first_sort ." ". $first_order . ", " . $second_sort ." ". $second_order;
		} else {
				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
						if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
								$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
						} elseif ($data['sort'] == 'p.price') {
								$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
						} elseif ($data['sort'] == 'p.quantity') {
								$sql .= " ORDER BY in_stock ";
						} else {
								$sql .= " ORDER BY " . $data['sort'];
						}
				} else {
						$sql .= " ORDER BY p.sort_order";
				}

				if (isset($data['order']) && ($data['order'] == 'DESC')|| isset($data['order']) && ($data['order'] == 'desc')) {
						$sql .= " DESC, LCASE(pd.name) ASC";
				} else {
						$sql .= " ASC, LCASE(pd.name) ASC";
				}
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$product_data = array();

		$query = $this->db->query($sql);

//		echo '<pre style="display:none">';
//		print_r($query);
//		echo '</pre>';

		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}

		return $product_data;
	}


    public function getProducts($data = array()) {
        $sql = "SELECT pd.name,p.product_id, ps.date_start, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
            } else {
                $sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
            } else {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
            }
        } else {
            $sql .= " FROM " . DB_PREFIX . "product p";
        }

        $sql .= " LEFT JOIN (SELECT date_start, product_special_id, product_id FROM " . DB_PREFIX . "product_special WHERE customer_group_id = '" . $this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW()))) ps ON (p.product_id = ps.product_id)";

        if (!empty($data['filter_option'])) {
            $sql .= " INNER JOIN oc_product_option_value pov ON p.product_id = pov.product_id ";
        }

        $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ";

        if (!empty($data['filter_category_id'])) {
            if (!empty($data['filter_sub_category'])) {
                $sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
            } else {
                $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
            }

            if (!empty($data['filter_filter'])) {
                $implode = array();

                $filters = explode(',', $data['filter_filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int)$filter_id;
                }

                $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
            }
        } else {
            $data['filter_category_id'] = 0;
        }

        if (!empty($data['filter_option'])) {
            $sql .= " AND pov.option_value_id IN(". $data['filter_option'] .") AND p.quantity > pov.quantity AND pov.status = 1 ";
        }

        if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
            $sql .= " AND (";

            if (!empty($data['filter_name'])) {
                $implode = array();

                $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                foreach ($words as $word) {
                    $implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
                }

                if ($implode) {
                    $sql .= " " . implode(" AND ", $implode) . "";
                }

                if (!empty($data['filter_description'])) {
                    $sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                }
            }

            if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                $sql .= " OR ";
            }

            if (!empty($data['filter_tag'])) {
                $sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
            }

            if (!empty($data['filter_name'])) {
                $sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                $sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
            }

            $sql .= ")";
        }


		// OCFilter start
		if (!empty($data['filter_ocfilter'])) {
    	$this->load->model('catalog/ocfilter');

      $ocfilter_product_sql = $this->model_catalog_ocfilter->getProductSQL($data['filter_ocfilter']);

			if ($ocfilter_product_sql) {
			  $sql .= $ocfilter_product_sql;
			}
		}
		// OCFilter end
      
        if (!empty($data['filter_manufacturer_id'])) {
            $sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
        }

        $sql .= " GROUP BY p.product_id ";

        $sort_data = array(
            'pd.name',
            'p.model',
            'p.quantity',
            'p.price',
            'rating',
            'p.sort_order',
            'p.date_added',
            'ps.product_special'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
            } elseif ($data['sort'] == 'p.price') {
                $sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
            } else {
                $sql .= " ORDER BY " . $data['sort'];
            }
        } else {
            if ($data['sort'] == 'default'
                && $this->config->get('sorting_status')
                && $this->config->get('config_sorts_radio_'. $data['path'])
                && (int)$data['filter_category_id'] !== 98
            ){
                $config_sorts_radio = $this->config->get('config_sorts_radio_'. $data['path']);
                foreach ($config_sorts_radio as $key => &$sorting) {

                    if(substr_count($sorting, '_') === 1){							$str = str_replace("_",", ", $sorting);
                        $sorts[$key] = substr(stristr($sorting, '_'),1);
                    }else{
                        $sorts[$key] = substr($sorting, strrpos($sorting, "_")+1);
                    }

                    $str = strrev(implode(strrev(", "), explode(strrev("_"), strrev($sorting), 2)));

                    if(strstr($str,'name')){
                        $sorting = 'pd.name';
                    }elseif(strstr($str,'price')){
                        $sorting = 'p.price';
                    }elseif(strstr($str,'quantity')){
                        $sorting = 'case p.quantity > 0 WHEN 0 THEN 1 END ';
                    }elseif(strstr($str,'date_start')){
                        $sorting = 'ps.date_start';
                    }else{
                        $sorting = stristr($sorting, '_',true);
                    }
                }
               // $sorts['first']='DESC';
                if($config_sorts_radio['second'] === 'ps.date_start'){
             //    $sql .= " ORDER BY special DESC," . $config_sorts_radio['first'] ." ". $sorts['first'] . ", ISNULL(" . $config_sorts_radio['second'] . "), " . $config_sorts_radio['second'] ." ". $sorts['second'];
                //    $sql .= " ORDER BY case p2c.category_id = 123 WHEN 0 THEN 1 END ASC, special DESC," . $config_sorts_radio['first'] ." ". $sorts['first'] . " ";
             //       $sql .= " ORDER BY case p2c.category_id = 122 WHEN 0 THEN 1 END DESC ";
                    if($data['filter_category_id']==74||$data['filter_category_id']==73){
                        $sql .= " ORDER BY p.product_id=2977 DESC, special DESC," . $config_sorts_radio['first'] ." ". $sorts['first'] . ",case p.model = 'Errors Seeds Silver' WHEN 0 THEN 1 END ASC,case p.model = 'Errors Seeds Gold' WHEN 0 THEN 1 END ASC ";
                    }else{
                        $sql .= " ORDER BY p.product_id=2977 DESC, special DESC," . $config_sorts_radio['first'] ." ". $sorts['first'] . " ";
                    }

                }elseif($config_sorts_radio['first'] === 'ps.date_start'){
                    $sql .= " ORDER BY ISNULL(" . $config_sorts_radio['first'] . ") " . $config_sorts_radio['first'] ." ". $sorts['first'] . ", " . $config_sorts_radio['second'] ." ". $sorts['second'];
                }else{
                    $sql .= " ORDER BY " . $config_sorts_radio['first'] ." ". $sorts['first'] . ", " . $config_sorts_radio['second'] ." ". $sorts['second'];
                }

                if((int)count($config_sorts_radio) === (int)2 && !in_array("pd.name", $config_sorts_radio)){
                  $sql .= ", LCASE(pd.name) ASC";
                }

            }else{
                $sql .= " ORDER BY p.sort_order";
            }
        }
        if ($data['sort'] != 'default'){
            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC, LCASE(pd.name) DESC";
            } else {
                $sql .= " ASC, LCASE(pd.name) ASC";
            }
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $product_data = array();

        $query = $this->db->query($sql);

      //  sort($query->rows);
//		echo '<pre style="display:none">';
//		print_r($sql);
//		echo '</pre>';
        foreach ($query->rows as $result) {
            $product_data[] = $this->getProduct($result['product_id']);
        }

        return $product_data;
    }



	public function getProductSpecial($product_id) {
		$sql = "SELECT product_special_id, sale_event, price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW()))";

		if($this->config->get('sale_events_status')!== NULL && (int)$this->config->get('sale_events_status') !== 0){
			$sql .= " AND status = 1 ";
		}

		$sql .=" ORDER BY priority ASC, price ASC LIMIT 1";

		$query = $this->db->query($sql);

		return $query->row;
	}

	public function getProductOptionSpecial($product_option_value_id, $special_id){
		$query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_option_special WHERE product_option_value_id='" . $product_option_value_id . "' AND special_id='" . (int)$special_id . "'");
			return $query->row;
	}

	public function getProductSpecials($data = array()) {
		$sql = "SELECT DISTINCT ps.product_id, ps.price AS special, NULL as discount, (CASE WHEN p.quantity > 0 THEN 1 ELSE 0 END) AS in_stock, (SELECT AVG(rating) FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = ps.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) RIGHT JOIN oc_product_special psdef ON ps.product_id = psdef.product_id WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) AND psdef.customer_group_id = '1' AND ps.date_start = psdef.date_start AND ps.date_end = psdef.date_end";

		if($this->config->get('sale_events_status')!== NULL && (int)$this->config->get('sale_events_status') !== 0){
			$sql .= " AND ps.status = 1 ";
		}

		// OCFilter start
		if (!empty($data['filter_ocfilter'])) {
			$this->load->model('catalog/ocfilter');

			$ocfilter_product_sql = $this->model_catalog_ocfilter->getProductSQL($data['filter_ocfilter']);

			if ($ocfilter_product_sql) {
				$sql .= $ocfilter_product_sql;
			}
		}

		$sql .= " GROUP BY ps.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.quantity',
			'p.price',
			'rating',
			'p.sort_order',
			'p.date_added',
			'ps.date_start'
		);


		$config_sorts = $this->config->get('sorting_status') && $this->config->get('sorting_' . $data['path']) ? $this->config->get('sorting_' . $data['path']) : [];

		if ($data['sort'] === 'default' && count($config_sorts['default']) === 2) {

				$first_order = substr($config_sorts['default']['first'], strrpos($config_sorts['default']['first'], "_", -0) + 1);
				$first_sort = $this->sort_strings[stristr($config_sorts['default']['first'], '_' . $first_order, true)];

				$second_order = substr($config_sorts['default']['second'], strrpos($config_sorts['default']['second'], "_", -0) + 1);
				$second_sort = $this->sort_strings[stristr($config_sorts['default']['second'], '_' . $second_order, true)];

				$sql .= " ORDER BY " . $first_sort ." ". $first_order . ", " . $second_sort ." ". $second_order;
		} else {
				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
						if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
								$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
						} elseif ($data['sort'] == 'p.price') {
								$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
						} elseif ($data['sort'] == 'p.quantity') {
								$sql .= " ORDER BY in_stock ";
						} else {
								$sql .= " ORDER BY " . $data['sort'];
						}
				} else {
						$sql .= " ORDER BY p.sort_order";
				}

				if (isset($data['order']) && ($data['order'] == 'DESC')|| isset($data['order']) && ($data['order'] == 'desc')) {
						$sql .= " DESC, LCASE(pd.name) ASC";
				} else {
						$sql .= " ASC, LCASE(pd.name) ASC";
				}
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$product_data = array();

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}

		return $product_data;
	}

	public function getLatestProducts($limit) {
		$product_data = $this->cache->get('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit);

		if (!$product_data) {
			$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.date_added DESC LIMIT " . (int)$limit);

			foreach ($query->rows as $result) {
				$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
			}

			$this->cache->set('product.latest.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit, $product_data);
		}

		return $product_data;
	}

	public function getPopularProducts($limit) {
		$product_data = array();

		$query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.viewed DESC, p.date_added DESC LIMIT " . (int)$limit);

		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}

		return $product_data;
	}

	public function getBestSellerProducts($limit) {
		$product_data = $this->cache->get('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit);

		if (!$product_data) {
			$product_data = array();

			$query = $this->db->query("SELECT op.product_id, SUM(op.quantity) AS total FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE o.order_status_id > '0' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' GROUP BY op.product_id ORDER BY total DESC LIMIT " . (int)$limit);

			foreach ($query->rows as $result) {
				$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
			}

			$this->cache->set('product.bestseller.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int)$limit, $product_data);
		}

		return $product_data;
	}

	public function getProductAttributes($product_id) {
		$product_attribute_group_data = array();

		$product_attribute_group_query = $this->db->query("SELECT ag.attribute_group_id, agd.name FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE pa.product_id = '" . (int)$product_id . "' AND agd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name");

		foreach ($product_attribute_group_query->rows as $product_attribute_group) {
			$product_attribute_data = array();

			$product_attribute_query = $this->db->query("SELECT a.attribute_id, ad.name, ad.short_name, pa.text, a.show_grid, a.image, pa.filters_id, a.filter_group_id FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.product_id = '" . (int)$product_id . "' AND a.attribute_group_id = '" . (int)$product_attribute_group['attribute_group_id'] . "' AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY a.sort_order, ad.name");


			foreach ($product_attribute_query->rows as $product_attribute) {

				if(!empty($product_attribute['filters_id']) && !empty($product_attribute['filter_group_id'])){
					$url = '';

					$filter_group_keyword = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "ocfilter_option WHERE option_id = '" . (int)$product_attribute['filter_group_id'] . "'");

					$url .= $filter_group_keyword->row['keyword'] . '/';

					$urls = $this->db->query("SELECT * FROM " . DB_PREFIX . "ocfilter_option_value WHERE value_id IN(" . $product_attribute['filters_id'] . ") AND option_id = '" . (int)$product_attribute['filter_group_id'] . "'");

					foreach ($urls->rows as $keywords) {
						$url .= $keywords['keyword'] . '/';
					}
				}else{
					$url = '';
				}

if(preg_match("/[\d]+/", $product_attribute['text']) === 0 && preg_match("/[\!]+/", $product_attribute['text']) === 0){					$product_attribute['show_grid'] = 0;
				}


				$product_attribute_data[] = array(
					'attribute_id' 		=> $product_attribute['attribute_id'],
					'show_grid'		 		=> $product_attribute['show_grid'],
					'short_name' 	 		=> $product_attribute['short_name'],
					'name'         		=> $product_attribute['name'],
					'text'         		=> $product_attribute['text'],
					'image'				 		=> $product_attribute['image'],
					'filters_id'   		=> $product_attribute['filters_id'],
					'filter_group_id' => $product_attribute['filter_group_id'],
					'filter_link' 		=> $url,
				);


			}

			$product_attribute_group_data[] = array(
				'attribute_group_id' => $product_attribute_group['attribute_group_id'],
				'name'               => $product_attribute_group['name'],
				'attribute'          => $product_attribute_data
			);
		}

		return $product_attribute_group_data;
	}

	public function getProductOptions($product_id) {
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.sort_order");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_id = '" . (int)$product_option['product_option_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND pov.status = 1 ORDER BY ov.sort_order"); //тут исправил status = 1

            $summ_sale=0;
            $summ_sale_group=0;
            $customer_balance=0;
            if($this->customer->isLogged() && $this->customer->getGroupId()==3  ){
                $summ_sale_group = $this->customer->getSummeSaleGroup();
                $procent_sale_group = $this->customer->getProcentSaleGroup();
                $summ_sale = $this->customer->getSummeSale();
                $procent_sale = $this->customer->getProcentSale();
                $procent_sale_bonus = $this->customer->getProcentSaleBonus();
                $customer_balance = $this->customer->getSummBalance();

            }
			foreach ($product_option_value_query->rows as $product_option_value) {

                if($summ_sale < $customer_balance && $summ_sale!=0){
                    $pr_opt = $this->percent($product_option_value['price'],$procent_sale);
                    $product_option_value['price'] = $pr_opt;

                }elseif ($summ_sale_group < $customer_balance && $summ_sale_group!=0){
                    $pr_opt = $this->percent($product_option_value['price'],$procent_sale_group);
                    $product_option_value['price'] = $pr_opt;
                }

				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'name'                    => $product_option_value['name'],
					'image'                   => $product_option_value['image'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_dev'                   => $product_option_value['price_dev'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix'],
					'status'				  => $product_option_value['status'], // тут исправил добавил status в массив
				);
			}

			if ($product_option_value_data) {
				$product_option_data[] = array(
					'product_option_id'    => $product_option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $product_option['option_id'],
					'name'                 => $product_option['name'],
					'type'                 => $product_option['type'],
					'value'                => $product_option['value'],
					'required'             => $product_option['required']
				);
			}
		}

		return $product_option_data;
	}

	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");

		return $query->rows;
	}

	public function getProductImages($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getProductRelated($product_id) {
		$product_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pr.product_id = '" . (int)$product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		foreach ($query->rows as $result) {
			$product_data[$result['related_id']] = $this->getProduct($result['related_id']);
		}

		return $product_data;
	}

	public function getProductsRelated($products_id){

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related pr
															LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id)
															LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
															WHERE pr.product_id IN(" . $products_id . ") AND p.status = '1'
															AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
															LIMIT " . $this->config->get('config_limit_recommended'));

															$product_data = array();

															foreach ($query->rows as $result) {
																$product_data[] = $this->getProduct($result['related_id']);
															}

															return $product_data;

	}

    public function getProductRecomended($product_id) {
        $product_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_recomended pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.recomended_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pr.product_id = '" . (int)$product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

        foreach ($query->rows as $result) {
            $product_data[$result['recomended_id']] = $this->getProduct($result['recomended_id']);
        }

        return $product_data;
    }

    public function getProductsRecomended($products_id){

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_recomended pr
															LEFT JOIN " . DB_PREFIX . "product p ON (pr.recomended_id = p.product_id)
															LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
															WHERE pr.product_id IN(" . $products_id . ") AND p.status = '1'
															AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
															LIMIT " . $this->config->get('config_limit_recommended'));

        $product_data = array();

        foreach ($query->rows as $result) {
            $product_data[] = $this->getProduct($result['recomended_id']);
        }

        return $product_data;

    }

	public function getProductLayoutId($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getCategories($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		return $query->rows;
	}

    public function getCategoriesMain($product_id) {
        $query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' and main_category=1 ");
   if($query->row['category_id'] > 0){
       $query = $this->db->query("SELECT sale_price FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$query->row['category_id'] . "' ");
   }
        return $query->row;
    }

	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}

			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}

		if (!empty($data['filter_option'])) {
			$sql .= " INNER JOIN oc_product_option_value pov ON p.product_id = pov.product_id ";
		}

		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
			}

			if (!empty($data['filter_filter'])) {
				$implode = array();

				$filters = explode(',', $data['filter_filter']);

				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}

				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		}

		if (!empty($data['filter_option'])) {
			$sql .= " AND pov.option_value_id IN(". $data['filter_option'] .") AND p.quantity > pov.quantity AND pov.status = 1 ";
		}

		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";

			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}

			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}

			if (!empty($data['filter_tag'])) {
				$sql .= "pd.tag LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_tag'])) . "%'";
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			$sql .= ")";
		}


		// OCFilter start
		if (!empty($data['filter_ocfilter'])) {
    	$this->load->model('catalog/ocfilter');

      $ocfilter_product_sql = $this->model_catalog_ocfilter->getProductSQL($data['filter_ocfilter']);

			if ($ocfilter_product_sql) {
			  $sql .= $ocfilter_product_sql;
			}
		}
		// OCFilter end
      
		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getProfiles($product_id) {
		return $this->db->query("SELECT `pd`.* FROM `" . DB_PREFIX . "product_recurring` `pp` JOIN `" . DB_PREFIX . "recurring_description` `pd` ON `pd`.`language_id` = " . (int)$this->config->get('config_language_id') . " AND `pd`.`recurring_id` = `pp`.`recurring_id` JOIN `" . DB_PREFIX . "recurring` `p` ON `p`.`recurring_id` = `pd`.`recurring_id` WHERE `product_id` = " . (int)$product_id . " AND `status` = 1 AND `customer_group_id` = " . (int)$this->config->get('config_customer_group_id') . " ORDER BY `sort_order` ASC")->rows;
	}

	public function getProfile($product_id, $recurring_id) {
		return $this->db->query("SELECT * FROM `" . DB_PREFIX . "recurring` `p` JOIN `" . DB_PREFIX . "product_recurring` `pp` ON `pp`.`recurring_id` = `p`.`recurring_id` AND `pp`.`product_id` = " . (int)$product_id . " WHERE `pp`.`recurring_id` = " . (int)$recurring_id . " AND `status` = 1 AND `pp`.`customer_group_id` = " . (int)$this->config->get('config_customer_group_id'))->row;
	}

	public function getTotalProductSpecials($data=array()) {
		$sql = "SELECT COUNT(DISTINCT ps.product_id) AS total FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) RIGHT JOIN oc_product_special psdef ON ps.product_id = psdef.product_id WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) AND psdef.customer_group_id = '1' AND ps.date_start = psdef.date_start AND ps.date_end = psdef.date_end";
		// OCFilter start
		if (!empty($data['filter_ocfilter'])) {
			$this->load->model('catalog/ocfilter');

			$ocfilter_product_sql = $this->model_catalog_ocfilter->getProductSQL($data['filter_ocfilter']);

			if ($ocfilter_product_sql) {
				$sql .= $ocfilter_product_sql;
			}
		}
		// OCFilter end

		$query = $this->db->query($sql);

		if (isset($query->row['total'])) {
			return $query->row['total'];
		} else {
			return 0;
		}
	}

	public function getIdsForSpecial() {
		$for = '';
		$query = $this->db->query("SELECT DISTINCT ps.product_id FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id");
		if ($query->num_rows) {
			foreach ($query->rows as $result) {
				$ids[] = $result['product_id'];
			}
			$for = implode(',', $ids);
		}

		return $for;
	}

	public function getIdsForBestseller() {
		$products = $this->getBestSellerProducts(25);
		return implode(',', array_column($products, 'product_id'));
	}

	public function isAddition($product_id) {
		$query = $this->db->query("SELECT addition_product_id FROM " . DB_PREFIX . "addition_product WHERE product_id = " . (int)$product_id);
		return $query->row;
	}

	public function getQuantityToSubtractOption($product_option_id){
		$query = $this->db->query("SELECT quantity_to_subtract_option FROM " . DB_PREFIX . "product_option WHERE product_option_id = '" . (int)$product_option_id . "'");

		return $query->row['quantity_to_subtract_option'];
	}

	public function getQuantityProductOptionValue($product_option_value_id){
		$query = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . $product_option_value_id . "'" );

		return $query->row['quantity'];
	}

	public function getStickers($product_info) {
		$stickers = [];

        if (isset($product_info['special'])&&(float)$product_info['special']) {
			// $sticker_class = $query->row['sale_event'] ? 'sticker-sale-' . $query->row['sale_event'] : 'sticker-sale';
			$sticker_class = 'sticker-sale';

			$percent = round(100 - ((float)$product_info['special'] * 100 / (float)$product_info['price']));
			$stickers[] = array('class' => 'sticker ' . $sticker_class, 'text' => '-' . $percent . '%');
		}

		if($this->config->get('product_stickers_status')){
            if (isset($product_info['main_stickers'])) {
                foreach ($product_info['main_stickers'] as $main_stickers) {
                    if ($main_stickers === 'recommended') {
                        $stickers[] = array('class' => 'sticker sticker-top', 'text' => 'TOP');
                    } else {
                        $stickers[] = array('class' => 'sticker sticker-' . $main_stickers, 'text' => mb_strtoupper($main_stickers));
                    }
                }
            }
		}
			return($stickers);
		}

}
