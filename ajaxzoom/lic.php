<?php
/**
* @version     2.0
* @module      Product 3D/360 Viewer for OpenCart
* @author      AJAX-ZOOM
* @copyright   Copyright (C) 2016 AJAX-ZOOM. All rights reserved.
* @license     http://www.ajax-zoom.com/index.php?cid=download
*/

if (!isset($zoom)) /* variable $zoom is defined in the external APP */
	exit;

require __DIR__ . '/../config.php';
error_reporting(0);
$lic = array();
if (function_exists('mysqli_connect')){
	$mysqli = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	$value = $mysqli->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` = 'ajaxzoom_LICENSE'")->fetch_object()->value;
	$lic = json_decode($value, true);
}
elseif (function_exists('mysql_connect')){
	$db_connect = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
	$db = mysql_select_db(DB_DATABASE);
	$data_query = mysql_query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` = 'ajaxzoom_LICENSE'");
	$data = mysql_fetch_array($data_query);
	$lic = json_decode($data['value'], true);
 	mysql_close($db_connect);
}

if (!empty($lic)) {
	foreach ($lic as $line) {
		$domain = trim($line['domain']);
		$type = trim($line['type']);
		$key = trim($line['key']);
		$error200 = trim($line['error200']);
		$error300 = trim($line['error300']);
		if(!empty($domain) && !empty($key)) {
			$zoom['config']['licenses'][$domain] = array(
				'licenceType' => $type,
				'licenceKey' => $key,
				'error200' => $error200,
				'error300' => $error300
			);
		}
	}
}
?>